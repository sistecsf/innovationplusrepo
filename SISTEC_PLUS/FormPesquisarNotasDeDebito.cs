﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS
{
    public partial class FormPesquisarNotasDeDebito : MetroFramework.Forms.MetroForm
    {
        SqlConnection Conexao = new SqlConnection(Variavel.Conexao);
        public FormPesquisarNotasDeDebito()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd = new SqlCommand(getQueryDeNotasDeDebito(), Conexao);

                Conexao.Open();
                SqlDataReader res = cmd.ExecuteReader();

                if (res.HasRows)
                {
                    while (res.Read())
                    {
                        metroGridNotasDeDebito.Rows.Add(new string[] { res["dataCria"].ToString(), res["coddoc"].ToString(), res["descrdoc"].ToString(), res["numdoc"].ToString(), res["codentidade"].ToString(), res["cliente"].ToString(), res["status"].ToString(), res["idUnico"].ToString(), res["nomeUtilizador"].ToString(), res["nomeloja"].ToString(), res["codpagamento"].ToString() });
                    }
                }
                else
                {
                    MessageBox.Show("Não retornou dados da pesquisa!", "Innovation - Informação");
                }
            }
            catch(Exception erro)
            {
                MessageBox.Show("Erro ao tentar retornar as notas de debito. ", "Innovation - Erro");
            }
            finally
            {
                Conexao.Close();
            }
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }
        private string getDataNoFormatoAmericano(DateTime data)
        {
            string dia = "0"+data.Day;
            string mes = "0"+data.Month;
            int ano = data.Year;
            string dataNoFormatoAmericano = ano+"-"+mes+"-"+dia;
            return dataNoFormatoAmericano;
        }

        /*
         * 
         * MÉTODOS DA REGRA DE NEGÓCIO
         * 
         * 
         */
        //Funcao   que retorna a query para pesquisar as notas de debito
        public string getQueryDeNotasDeDebito()
        {
            //sqlbase
            string sqlBase = " SELECT    CONVERT(VARCHAR(10), CAST(DataCria AS DATETIME), 105) as dataCria, CODDOC, DESCRDOC,  NUMDOC, CODENTIDADE,      CLIENTE, STATUS, IDUNICO, NOMEUTILIZADOR, NOMELOJA, CODPAGAMENTO FROM VW_POS_RECIBOS  WITH (NOLOCK)   WHERE DATACRIA IS NOT NULL ";
            DateTime hoje = DateTime.Now;
            DateTime dataInicial = Convert.ToDateTime(dTPDataInicial.Text).Date;
            DateTime dataFinal = Convert.ToDateTime(dTPDataFinal.Text).Date;

            sqlBase += " AND LEFT(DATACRIA, 10) >=  '" + getDataNoFormatoAmericano(dataFinal) + "' and LEFT(DATACRIA, 10) <= '" + getDataNoFormatoAmericano(dataInicial) + "'";
            
            if (!string.IsNullOrEmpty(txtNumeroDoCliente.Text))
            {
                sqlBase += " AND CODENTIDADE  LIKE '" + txtNumeroDoCliente.Text + "%'";
            }

            if (!string.IsNullOrEmpty(txtNomeDoCliente.Text))
            {
                sqlBase += " AND NOME LIKE '" +txtNomeDoCliente.Text+ "%'";
            }

            if (!string.IsNullOrEmpty(txtcodigoDoDocumento.Text))
            {
                sqlBase += "  AND CODDOC LIKE '" + txtcodigoDoDocumento.Text + "%'";
            }

            if (!string.IsNullOrEmpty(txtNumeroDoDocumento.Text))
            {
                sqlBase += "  AND NUMDOC LIKE '" + txtNumeroDoDocumento.Text + "%'";
            }

            if (!string.IsNullOrEmpty(txtDescricao.Text))
            {
                sqlBase += "   AND DESCRDOC LIKE     '" + txtDescricao.Text + "%'";
            }

            if (!string.IsNullOrEmpty(txtIdUnico.Text))
            {
                sqlBase += "   AND IDUNICO LIKE     '" + txtIdUnico.Text + "%'";
            }

            sqlBase += " Order By CONVERT(VARCHAR(10), CAST(DATACRIA AS DATETIME), 105), NUMDOC, CLIENTE";

            return sqlBase;
        }

        private void metroGridNotasDeDebito_RowDividerDoubleClick(object sender, DataGridViewRowDividerDoubleClickEventArgs e)
        {
            
        }

        private void metroGridNotasDeDebito_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (metroGridNotasDeDebito.CurrentRow != null)
            {
                string idUnicoParaReemissaoDaNotaDeDebito = metroGridNotasDeDebito.CurrentRow.Cells[7].Value.ToString();

                //nova reemissao
                reportViewReciboPagamentoDeFactura reemissao = new reportViewReciboPagamentoDeFactura(idUnicoParaReemissaoDaNotaDeDebito);
                reemissao.ShowDialog();
            }
        }
    }
}
