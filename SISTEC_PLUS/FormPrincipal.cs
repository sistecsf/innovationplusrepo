﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using SISTEC_PLUS.OPS;




namespace SISTEC_PLUS
{
    public partial class FormPrincipal : MetroFramework.Forms.MetroForm
    {
        public FormPrincipal()
        {
            InitializeComponent();

            // Carrega dados dos report viewer dashboards main
            ESTATISTICATableAdapter.Fill(DataSet1.ESTATISTICA);
            ESTATISTICA02TableAdapter.Fill(nrtrabalhadoresds.ESTATISTICA02);
            ESTATISTICA03TableAdapter.Fill(this.nrfobmifds.ESTATISTICA03);


        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {
            /*
            // TODO: This line of code loads data into the 'nrtrabalhadoresds.ESTATISTICA02' table. You can move, or remove it, as needed.
            this.ESTATISTICA02TableAdapter.Fill(this.nrtrabalhadoresds.ESTATISTICA02);
            // TODO: This line of code loads data into the 'DataSet1.ESTATISTICA' table. You can move, or remove it, as needed.
            this.ESTATISTICATableAdapter.Fill(this.DataSet1.ESTATISTICA);
            // TODO: This line of code loads data into the 'nrfobmifds.ESTATISTICA03' table. You can move, or remove it, as needed.
            this.ESTATISTICA03TableAdapter.Fill(this.nrfobmifds.ESTATISTICA03);
            */
            // mostra dashboards main
            reportViewer1.RefreshReport();
            reportViewer2.RefreshReport();
            reportViewer3.RefreshReport();



        }

        private void button5_Click(object sender, EventArgs e)
        {
            FormMovomentoVenda venda = new FormMovomentoVenda();
            venda.ShowDialog();
        }
         // BOTAO DO MODULO CRM
        private void button2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Users\adriano.amaral\Documents\Visual Studio 2013\Projects\innovationplusrepo\Sistec_v2\Sistec_v2\bin\Debug\Sistec_v2.exe");
        }
    }
}
