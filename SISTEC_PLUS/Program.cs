﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SISTEC_PLUS
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FormLogin _frmLogin = new FormLogin();
            Application.Run(_frmLogin);
            if (_frmLogin.DialogResult == DialogResult.OK)
            Application.Run(new FormPrincipal());
        }
    }
}
