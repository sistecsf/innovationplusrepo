﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace SISTEC_PLUS
{
    public partial class FormMessagem : MetroFramework.Forms.MetroForm
    {
        public FormMessagem(string messagem)
        {
            InitializeComponent();
            txtMessagem.Text = messagem;
        }

        private void FormMessagem_Load(object sender, EventArgs e)
        {

        }

        private void btCliente_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
