﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.CRM.Modelo
{
    class AFENTIDADE
    {
        public AFENTIDADE()
        {
           // this.AFCONTACTOes = new List<AFCONTACTO>();
           // this.ASENCOMENDAS = new List<ASENCOMENDA>();
            //this.ATRECIBOes = new List<ATRECIBO>();
            //this.XXMOVIMENTOS = new List<XXMOVIMENTO>();
        }

        public string CodEntidade { get; set; }
        public string CodFamilia { get; set; }
        public string Status { get; set; }
        public string NomeEntidade { get; set; }
        public string SITIO { get; set; }
        public string Morada { get; set; }
        public string CxPostal { get; set; }
        public string NumCONTRIB { get; set; }
        public Nullable<int> DESC1 { get; set; }
        public Nullable<int> DESC2 { get; set; }
        public Nullable<int> DESC3 { get; set; }
        public string OBS { get; set; }
        public Nullable<int> NIVELDESC { get; set; }
        public string DATA_CONFIRMOU { get; set; }
        public string CONFIRMOU { get; set; }
        public string Apelido { get; set; }
        public string TELEX { get; set; }
        public string DataCria { get; set; }
        public string ALTEROU { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD1 { get; set; }
        public string DATACRIADO { get; set; }
        public string LOGINCOUNTER { get; set; }
        public string EMAIL { get; set; }
        public string FONE { get; set; }
        public string FAX { get; set; }
        public string CodConta { get; set; }
        public string CodUtilizador { get; set; }
        public string CodLocal { get; set; }
        public string CodTipoCli { get; set; }
        public string CodigoINT { get; set; }
        public string CodPais { get; set; }
        public string Data_Alteracao { get; set; }
        public string Alterado_Por { get; set; }
        public string BI { get; set; }
        public Nullable<decimal> Valor_Tx { get; set; }
        public string Display_ContraValor { get; set; }
        public string Localizacao_Fisica { get; set; }
        public string Inactivo { get; set; }
       // public virtual ICollection<AFCONTACTO> AFCONTACTOes { get; set; }
       // public virtual AFLocalid AFLocalid { get; set; }
      //  public virtual AFPais AFPais { get; set; }
      //  public virtual ICollection<ASENCOMENDA> ASENCOMENDAS { get; set; }
      //  public virtual ICollection<ATRECIBO> ATRECIBOes { get; set; }
       // public virtual ICollection<XXMOVIMENTO> XXMOVIMENTOS { get; set; }

    }
}
