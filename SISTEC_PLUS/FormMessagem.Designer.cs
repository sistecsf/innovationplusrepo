﻿namespace SISTEC_PLUS
{
    partial class FormMessagem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.txtMessagem = new System.Windows.Forms.TextBox();
            this.btOk = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(23, 74);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(457, 77);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtMessagem
            // 
            this.txtMessagem.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtMessagem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMessagem.Enabled = false;
            this.txtMessagem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMessagem.Location = new System.Drawing.Point(23, 63);
            this.txtMessagem.Multiline = true;
            this.txtMessagem.Name = "txtMessagem";
            this.txtMessagem.Size = new System.Drawing.Size(457, 77);
            this.txtMessagem.TabIndex = 1;
            this.txtMessagem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btOk
            // 
            this.btOk.Location = new System.Drawing.Point(203, 157);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(82, 32);
            this.btOk.TabIndex = 101;
            this.btOk.Text = "OK";
            this.btOk.UseSelectable = true;
            this.btOk.Click += new System.EventHandler(this.btCliente_Click);
            // 
            // FormMessagem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 203);
            this.ControlBox = false;
            this.Controls.Add(this.btOk);
            this.Controls.Add(this.txtMessagem);
            this.Controls.Add(this.textBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMessagem";
            this.Text = "Messagem";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormMessagem_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtMessagem;
        private MetroFramework.Controls.MetroButton btOk;
    }
}