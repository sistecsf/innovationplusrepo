﻿namespace SISTEC_PLUS
{
    partial class FormMessagem2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtMessagem = new System.Windows.Forms.TextBox();
            this.btSim = new MetroFramework.Controls.MetroButton();
            this.btNao = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // txtMessagem
            // 
            this.txtMessagem.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtMessagem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMessagem.Enabled = false;
            this.txtMessagem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMessagem.Location = new System.Drawing.Point(21, 67);
            this.txtMessagem.Multiline = true;
            this.txtMessagem.Name = "txtMessagem";
            this.txtMessagem.Size = new System.Drawing.Size(457, 77);
            this.txtMessagem.TabIndex = 2;
            this.txtMessagem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btSim
            // 
            this.btSim.Location = new System.Drawing.Point(151, 165);
            this.btSim.Name = "btSim";
            this.btSim.Size = new System.Drawing.Size(82, 32);
            this.btSim.TabIndex = 102;
            this.btSim.Text = "Sim";
            this.btSim.UseSelectable = true;
            this.btSim.Click += new System.EventHandler(this.btSim_Click);
            // 
            // btNao
            // 
            this.btNao.Location = new System.Drawing.Point(253, 165);
            this.btNao.Name = "btNao";
            this.btNao.Size = new System.Drawing.Size(82, 32);
            this.btNao.TabIndex = 103;
            this.btNao.Text = "Não";
            this.btNao.UseSelectable = true;
            this.btNao.Click += new System.EventHandler(this.btNao_Click);
            // 
            // FormMessagem2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 211);
            this.ControlBox = false;
            this.Controls.Add(this.btNao);
            this.Controls.Add(this.btSim);
            this.Controls.Add(this.txtMessagem);
            this.Name = "FormMessagem2";
            this.Text = "Messagem";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormMessagem2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtMessagem;
        private MetroFramework.Controls.MetroButton btSim;
        private MetroFramework.Controls.MetroButton btNao;
    }
}