﻿namespace SISTEC_PLUS
{
    partial class FormPesquisarNotasDeDebito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dTPDataFinal = new System.Windows.Forms.DateTimePicker();
            this.dTPDataInicial = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtNumeroDoCliente = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNomeDoCliente = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtIdUnico = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtcodigoDoDocumento = new System.Windows.Forms.TextBox();
            this.txtNumeroDoDocumento = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDescricao = new System.Windows.Forms.TextBox();
            this.metroGridNotasDeDebito = new MetroFramework.Controls.MetroGrid();
            this.data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoDoDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tipoDoDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numeroDoDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigodoCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idUnico = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codigoDoPagamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridNotasDeDebito)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dTPDataFinal);
            this.groupBox1.Controls.Add(this.dTPDataInicial);
            this.groupBox1.Location = new System.Drawing.Point(23, 108);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(667, 58);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(352, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "à:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "De: ";
            // 
            // dTPDataFinal
            // 
            this.dTPDataFinal.Location = new System.Drawing.Point(374, 22);
            this.dTPDataFinal.Name = "dTPDataFinal";
            this.dTPDataFinal.Size = new System.Drawing.Size(271, 20);
            this.dTPDataFinal.TabIndex = 3;
            // 
            // dTPDataInicial
            // 
            this.dTPDataInicial.Location = new System.Drawing.Point(93, 22);
            this.dTPDataInicial.Name = "dTPDataInicial";
            this.dTPDataInicial.Size = new System.Drawing.Size(211, 20);
            this.dTPDataInicial.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(422, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Sair";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(228, 63);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(188, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Pesquisar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtNumeroDoCliente);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtNomeDoCliente);
            this.groupBox2.Location = new System.Drawing.Point(23, 172);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(667, 57);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cliente";
            // 
            // txtNumeroDoCliente
            // 
            this.txtNumeroDoCliente.Location = new System.Drawing.Point(73, 23);
            this.txtNumeroDoCliente.Mask = "00000000000000";
            this.txtNumeroDoCliente.Name = "txtNumeroDoCliente";
            this.txtNumeroDoCliente.Size = new System.Drawing.Size(98, 20);
            this.txtNumeroDoCliente.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(266, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nome:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Número:";
            // 
            // txtNomeDoCliente
            // 
            this.txtNomeDoCliente.Location = new System.Drawing.Point(316, 20);
            this.txtNomeDoCliente.Name = "txtNomeDoCliente";
            this.txtNomeDoCliente.Size = new System.Drawing.Size(329, 20);
            this.txtNomeDoCliente.TabIndex = 1;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtIdUnico);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtcodigoDoDocumento);
            this.groupBox3.Controls.Add(this.txtNumeroDoDocumento);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtDescricao);
            this.groupBox3.Location = new System.Drawing.Point(23, 235);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(667, 53);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Documento";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(559, 25);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "ID:";
            // 
            // txtIdUnico
            // 
            this.txtIdUnico.Location = new System.Drawing.Point(586, 21);
            this.txtIdUnico.Name = "txtIdUnico";
            this.txtIdUnico.Size = new System.Drawing.Size(59, 20);
            this.txtIdUnico.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(416, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Número: ";
            // 
            // txtcodigoDoDocumento
            // 
            this.txtcodigoDoDocumento.Location = new System.Drawing.Point(73, 21);
            this.txtcodigoDoDocumento.Name = "txtcodigoDoDocumento";
            this.txtcodigoDoDocumento.Size = new System.Drawing.Size(98, 20);
            this.txtcodigoDoDocumento.TabIndex = 10;
            // 
            // txtNumeroDoDocumento
            // 
            this.txtNumeroDoDocumento.Location = new System.Drawing.Point(472, 21);
            this.txtNumeroDoDocumento.Mask = "00000000000000";
            this.txtNumeroDoDocumento.Name = "txtNumeroDoDocumento";
            this.txtNumeroDoDocumento.Size = new System.Drawing.Size(81, 20);
            this.txtNumeroDoDocumento.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(186, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Descrição: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Código:";
            // 
            // txtDescricao
            // 
            this.txtDescricao.Location = new System.Drawing.Point(253, 21);
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(140, 20);
            this.txtDescricao.TabIndex = 1;
            // 
            // metroGridNotasDeDebito
            // 
            this.metroGridNotasDeDebito.AllowUserToResizeRows = false;
            this.metroGridNotasDeDebito.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridNotasDeDebito.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGridNotasDeDebito.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGridNotasDeDebito.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridNotasDeDebito.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGridNotasDeDebito.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGridNotasDeDebito.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.data,
            this.codigoDoDocumento,
            this.tipoDoDocumento,
            this.numeroDoDocumento,
            this.codigodoCliente,
            this.cliente,
            this.estado,
            this.idUnico,
            this.codigoDoPagamento});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGridNotasDeDebito.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGridNotasDeDebito.EnableHeadersVisualStyles = false;
            this.metroGridNotasDeDebito.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGridNotasDeDebito.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridNotasDeDebito.Location = new System.Drawing.Point(23, 305);
            this.metroGridNotasDeDebito.Name = "metroGridNotasDeDebito";
            this.metroGridNotasDeDebito.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridNotasDeDebito.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGridNotasDeDebito.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGridNotasDeDebito.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGridNotasDeDebito.Size = new System.Drawing.Size(667, 149);
            this.metroGridNotasDeDebito.TabIndex = 58;
            this.metroGridNotasDeDebito.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid1_CellContentClick);
            this.metroGridNotasDeDebito.RowDividerDoubleClick += new System.Windows.Forms.DataGridViewRowDividerDoubleClickEventHandler(this.metroGridNotasDeDebito_RowDividerDoubleClick);
            this.metroGridNotasDeDebito.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.metroGridNotasDeDebito_RowHeaderMouseDoubleClick);
            // 
            // data
            // 
            this.data.HeaderText = "Data";
            this.data.Name = "data";
            // 
            // codigoDoDocumento
            // 
            this.codigoDoDocumento.HeaderText = "Código do documento";
            this.codigoDoDocumento.Name = "codigoDoDocumento";
            // 
            // tipoDoDocumento
            // 
            this.tipoDoDocumento.HeaderText = "Tipo do documento";
            this.tipoDoDocumento.Name = "tipoDoDocumento";
            // 
            // numeroDoDocumento
            // 
            this.numeroDoDocumento.HeaderText = "Nº Documento";
            this.numeroDoDocumento.Name = "numeroDoDocumento";
            // 
            // codigodoCliente
            // 
            this.codigodoCliente.HeaderText = "Código do cliente";
            this.codigodoCliente.Name = "codigodoCliente";
            // 
            // cliente
            // 
            this.cliente.HeaderText = "Cliente";
            this.cliente.Name = "cliente";
            // 
            // estado
            // 
            this.estado.HeaderText = "Estado";
            this.estado.Name = "estado";
            // 
            // idUnico
            // 
            this.idUnico.HeaderText = "ID";
            this.idUnico.Name = "idUnico";
            // 
            // codigoDoPagamento
            // 
            this.codigoDoPagamento.HeaderText = "Código do pagamento";
            this.codigoDoPagamento.Name = "codigoDoPagamento";
            // 
            // FormPesquisarNotasDeDebito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 466);
            this.Controls.Add(this.metroGridNotasDeDebito);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Name = "FormPesquisarNotasDeDebito";
            this.Text = "Pesquisar notas de débito";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridNotasDeDebito)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dTPDataFinal;
        private System.Windows.Forms.DateTimePicker dTPDataInicial;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNomeDoCliente;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDescricao;
        private System.Windows.Forms.MaskedTextBox txtNumeroDoCliente;
        private System.Windows.Forms.MaskedTextBox txtNumeroDoDocumento;
        private MetroFramework.Controls.MetroGrid metroGridNotasDeDebito;

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtIdUnico;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtcodigoDoDocumento;

        private System.Windows.Forms.DataGridViewTextBoxColumn data;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoDoDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoDoDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroDoDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigodoCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn cliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn idUnico;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigoDoPagamento;

    }
}