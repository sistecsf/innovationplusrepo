﻿namespace SISTEC_PLUS
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.metroButton5 = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.txtUtilizador = new MetroFramework.Controls.MetroTextBox();
            this.txtPalavraPasse = new MetroFramework.Controls.MetroTextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(607, 407);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(88, 19);
            this.metroLabel2.TabIndex = 17;
            this.metroLabel2.Text = "Palavra-Passe";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(607, 378);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(65, 19);
            this.metroLabel1.TabIndex = 16;
            this.metroLabel1.Text = "Utilizador";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(30)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(3, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1294, 21);
            this.panel1.TabIndex = 20;
            this.panel1.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Login ou Senha Incorrete";
            // 
            // metroButton5
            // 
            this.metroButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.metroButton5.Location = new System.Drawing.Point(714, 443);
            this.metroButton5.Name = "metroButton5";
            this.metroButton5.Size = new System.Drawing.Size(59, 25);
            this.metroButton5.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroButton5.TabIndex = 15;
            this.metroButton5.Text = "OK";
            this.metroButton5.UseSelectable = true;
            this.metroButton5.Click += new System.EventHandler(this.metroButton5_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.metroButton1.Location = new System.Drawing.Point(779, 443);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(59, 25);
            this.metroButton1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroButton1.TabIndex = 21;
            this.metroButton1.Text = "Cancelar";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // txtUtilizador
            // 
            this.txtUtilizador.Lines = new string[0];
            this.txtUtilizador.Location = new System.Drawing.Point(714, 374);
            this.txtUtilizador.MaxLength = 32767;
            this.txtUtilizador.Name = "txtUtilizador";
            this.txtUtilizador.PasswordChar = '\0';
            this.txtUtilizador.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUtilizador.SelectedText = "";
            this.txtUtilizador.Size = new System.Drawing.Size(153, 23);
            this.txtUtilizador.TabIndex = 22;
            this.txtUtilizador.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtUtilizador.UseSelectable = true;
            // 
            // txtPalavraPasse
            // 
            this.txtPalavraPasse.Lines = new string[0];
            this.txtPalavraPasse.Location = new System.Drawing.Point(714, 403);
            this.txtPalavraPasse.MaxLength = 32767;
            this.txtPalavraPasse.Name = "txtPalavraPasse";
            this.txtPalavraPasse.PasswordChar = '*';
            this.txtPalavraPasse.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPalavraPasse.SelectedText = "";
            this.txtPalavraPasse.Size = new System.Drawing.Size(153, 23);
            this.txtPalavraPasse.TabIndex = 23;
            this.txtPalavraPasse.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPalavraPasse.UseSelectable = true;
            this.txtPalavraPasse.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPalavraPasse_KeyPress_1);
            // 
            // FormLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 639);
            this.Controls.Add(this.txtPalavraPasse);
            this.Controls.Add(this.txtUtilizador);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroButton5);
            this.Name = "FormLogin";
            this.Text = "Innovation Plus";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton metroButton5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroTextBox txtUtilizador;
        private MetroFramework.Controls.MetroTextBox txtPalavraPasse;

    }
}

