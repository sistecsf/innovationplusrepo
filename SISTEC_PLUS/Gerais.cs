﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace SISTEC_PLUS
{
    class Gerais
    {
        SqlConnection conexao = new SqlConnection(@"Data Source=sisdbs1b;Initial Catalog=INNOVUIGE;User ID=admin;Password=admin"); // conexão com o banco

        public string GetParam(string pr){
            string parametro;
            SqlCommand cmd = new SqlCommand("Select RTRIM(LTRIM(Valor)) FROM AAPARAMETRO (NOLOCK) WHERE CODPARAM = '"+pr+"'", conexao);

                try
                {
                    conexao.Open();
                    parametro = (string)cmd.ExecuteScalar();
                }
                catch (Exception) { parametro = ""; }
                finally { conexao.Close(); }

            return parametro;
        }
        public static string PARAM_CODDOC_DV, PARAM_CODCLI_EMP, PARAM_CODCLI_GERAL, PARAM_CODCLI_GERAL_EQUIP, PARAM_COD_ACCAO;
        public static string PARAM_COD_HOTEL, PARAM_COD_MOBRA, PARAM_COD_NSERIE, PARAM_COD_FRN, PARAM_COD_CLI, PARAM_COD_TIPOCLI, PARAM_COD_VALIDADE_PWD, PARAM_COD_AVISO_EXP_PWD, PARAM_COD_VALIDADE_APP;
        public static string PARAM_COD_DATAINST_APP, PARAM_COD_PERCENT_CUSTOS, PARAM_COD_TRA, PARAM_COD_DESLOC1, PARAM_COD_DESLOC2, PARAM_CODDOC_EA, PARAM_CODDOC_IF, PARAM_CODDOC_EM, PARAM_CODDOC_EI,  PARAM_CODDOC_ET, PARAM_CODDOC_FA, PARAM_CODDOC_FC;
        public static string PARAM_CODDOC_FD, PARAM_CODDOC_FH, PARAM_CODDOC_FM, PARAM_CODDOC_FP, PARAM_CODDOC_GR, PARAM_CODDOC_GE, PARAM_CODDOC_SE, PARAM_CODDOC_GI, PARAM_CODDOC_EG, PARAM_CODDOC_MN;
        public static string PARAM_CODDOC_CH, PARAM_CODDOC_MC, PARAM_CODDOC_CT, PARAM_CODDOC_FT, PARAM_CODDOC_NC, PARAM_CODDOC_CC, PARAM_CODDOC_ND, PARAM_CODDOC_NP, PARAM_CODDOC_PA;
        public static string PARAM_CODDOC_PD, PARAM_CODDOC_PN, PARAM_CODDOC_PP, PARAM_CODDOC_PC, PARAM_CODDOC_RV, PARAM_CODDOC_SA, PARAM_CODDOC_ST, PARAM_CODDOC_SR, PARAM_CODDOC_ER;
        public static string PARAM_CODDOC_TK, PARAM_CODDOC_TP, PARAM_CODDOC_TT, PARAM_CODDOC_VC, PARAM_CODDOC_VD, PARAM_CODDOC_VP, PARAM_CODDOC_RI, PARAM_CODDOC_RF, PARAM_CODLOJA_GER;
        public static string PARAM_CODARMZ_GER, PARAM_CONTA_CLI, PARAM_CONTA_CLI_GERAL, PARAM_CONTA_CX, PARAM_CONTA_FRN, PARAM_CONTA_STOCK, PARAM_CONTA_VENDAS, PARAM_CONTA_SERVICO, PARAM_INVAL_CHAR, PARAM_MAX_DESC, PARAM_MOEDA_NAC;
        public static string PARAM_MOEDA_PDR, PARAM_NRDIAS_FP, PARAM_NRDIAS_NP, PARAM_NRDIAS_FA, PARAM_NRDIAS_FC, PARAM_NRDIAS_FD, PARAM_NRDIAS_FM, PARAM_NRDIAS_PEM, PARAM_SAB_DIA_UTIL, PARAM_TIPO_FACTURA, PARAM_TIPO_RECIBO, PARAM_DOM_DIA_UTIL, PARAM_VALMIN_FC;
        public static string PARAM_LOGOTIPO_LOJA, PARAM_GAVETA_ABERTA, PARAM_GAVETA_PORTA, PARAM_GAVETA_DEFINICOES, PARAM_LEASING, PARAM_CODDOC_SC, PARAM_CODDOC_EC, PARAM_CODDOC_OS, PARAM_CODDOC_FF;

        public void lerParametro(){
            PARAM_CODDOC_DV = GetParam("CODDOC_DV");
            PARAM_CODCLI_EMP = GetParam("CODCLI_EMP");
            PARAM_CODCLI_GERAL = GetParam("CODCLI_GERAL");
            PARAM_CODCLI_GERAL_EQUIP = GetParam("CODCLI_GERAL_EQUIP");
            PARAM_COD_ACCAO = GetParam("COD_ACCAO");
            PARAM_COD_HOTEL = GetParam("COD_HOTEL");
            PARAM_COD_MOBRA = GetParam("COD_MOBRA");
            PARAM_COD_NSERIE = GetParam("COD_NSERIE");
            PARAM_COD_FRN = GetParam("COD_FRN");
            PARAM_COD_CLI = GetParam("COD_CLI");
            PARAM_COD_TIPOCLI = GetParam("COD_TIPOCLI");
            PARAM_COD_VALIDADE_PWD = GetParam("COD_VALIDADE_PWD");
            PARAM_COD_AVISO_EXP_PWD = GetParam("COD_AVISO_EXP_PWD");
            PARAM_COD_VALIDADE_APP = GetParam("COD_VALIDADE_APP");
            PARAM_COD_DATAINST_APP = GetParam("COD_DATAINST_APP");
            PARAM_COD_PERCENT_CUSTOS = GetParam("COD_PERCENT_CUSTOS");
            PARAM_COD_TRA = GetParam("COD_TRA");
            PARAM_COD_DESLOC1 = GetParam("COD_DESLOC1");
            PARAM_COD_DESLOC2 = GetParam("COD_DESLOC2");
            PARAM_CODDOC_EA = GetParam("CODDOC_EA");
            PARAM_CODDOC_IF = GetParam("CODDOC_IF");
            PARAM_CODDOC_EM = GetParam("CODDOC_EM");
            PARAM_CODDOC_EI = GetParam("CODDOC_EI");
            PARAM_CODDOC_ET = GetParam("CODDOC_ET");
            PARAM_CODDOC_FA = GetParam("CODDOC_FA");
            PARAM_CODDOC_FC = GetParam("CODDOC_FC");
            PARAM_CODDOC_FD = GetParam("CODDOC_FD");
            PARAM_CODDOC_FH = GetParam("CODDOC_FH");
            PARAM_CODDOC_FM = GetParam("CODDOC_FM");
            PARAM_CODDOC_FP = GetParam("CODDOC_FP");
            PARAM_CODDOC_GR = GetParam("CODDOC_GR");
            PARAM_CODDOC_GE = GetParam("CODDOC_GE");
            PARAM_CODDOC_SE = GetParam("CODDOC_SE");
            PARAM_CODDOC_GI = GetParam("CODDOC_GI");
            PARAM_CODDOC_EG = GetParam("CODDOC_EG");
            PARAM_CODDOC_MN = GetParam("CODDOC_MN");
            PARAM_CODDOC_CH = GetParam("CODDOC_CH");
            PARAM_CODDOC_MC = GetParam("CODDOC_MC");
            PARAM_CODDOC_CT = GetParam("CODDOC_CT");
            PARAM_CODDOC_FT = GetParam("CODDOC_FT");
            PARAM_CODDOC_NC = GetParam("CODDOC_NC");
            PARAM_CODDOC_CC = GetParam("CODDOC_CC");
            PARAM_CODDOC_ND = GetParam("CODDOC_ND");
            PARAM_CODDOC_NP = GetParam("CODDOC_NP");
            PARAM_CODDOC_PA = GetParam("CODDOC_PA");
            PARAM_CODDOC_PD = GetParam("CODDOC_PD");
            PARAM_CODDOC_PN = GetParam("CODDOC_PN");
            PARAM_CODDOC_PP = GetParam("CODDOC_PP");
            PARAM_CODDOC_PC = GetParam("CODDOC_PC");
            PARAM_CODDOC_RV = GetParam("CODDOC_RV");
            PARAM_CODDOC_SA = GetParam("CODDOC_SA");
            PARAM_CODDOC_ST = GetParam("CODDOC_ST");
            PARAM_CODDOC_SR = GetParam("CODDOC_SR");
            PARAM_CODDOC_ER = GetParam("CODDOC_ER");
            PARAM_CODDOC_TK = GetParam("CODDOC_TK");
            PARAM_CODDOC_TP = GetParam("CODDOC_TP");
            PARAM_CODDOC_TT = GetParam("CODDOC_TT");
            PARAM_CODDOC_VC = GetParam("CODDOC_VC");
            PARAM_CODDOC_VD = GetParam("CODDOC_VD");
            PARAM_CODDOC_VP = GetParam("CODDOC_VP");
            PARAM_CODDOC_RI = GetParam("CODDOC_RI");
            PARAM_CODDOC_RF = GetParam("CODDOC_RF");
            PARAM_CODLOJA_GER = GetParam("CODLOJA_GER");
            PARAM_CODARMZ_GER = GetParam("CODARMZ_GER");
            PARAM_CONTA_CLI = GetParam("CONTA_CLI");
            PARAM_CONTA_CLI_GERAL = GetParam("CONTA_CLI_GERAL");
            PARAM_CONTA_CX = GetParam("CONTA_CX");
            PARAM_CONTA_FRN = GetParam("CONTA_FRN");
            PARAM_CONTA_STOCK = GetParam("CONTA_STOCK");
            PARAM_CONTA_VENDAS = GetParam("CONTA_VENDAS");
            PARAM_CONTA_SERVICO = GetParam("CONTA_SERVICO");
            PARAM_INVAL_CHAR = GetParam("INVAL_CHAR");
            PARAM_MAX_DESC = GetParam("MAX_DESC");
            PARAM_MOEDA_NAC = GetParam("MOEDA_NAC");
            PARAM_MOEDA_PDR = GetParam("MOEDA_PDR");
            PARAM_NRDIAS_FP = GetParam("NRDIAS_FP");
            PARAM_NRDIAS_NP = GetParam("NRDIAS_NP");
            PARAM_NRDIAS_FA = GetParam("NRDIAS_FA");
            PARAM_NRDIAS_FC = GetParam("NRDIAS_FC");
            PARAM_NRDIAS_FD = GetParam("NRDIAS_FD");
            PARAM_NRDIAS_FM = GetParam("NRDIAS_FM");
            PARAM_NRDIAS_PEM = GetParam("NRDIAS_PEM");
            PARAM_SAB_DIA_UTIL = GetParam("SAB_DIA_UTIL");
            PARAM_TIPO_FACTURA = GetParam("TIPO_FACTURA");
            PARAM_TIPO_RECIBO = GetParam("TIPO_RECIBO");
            PARAM_SAB_DIA_UTIL = GetParam("SAB_DIA_UTIL");
            PARAM_DOM_DIA_UTIL = GetParam("DOM_DIA_UTIL");
            PARAM_VALMIN_FC = GetParam("VALMIN_FC");
            PARAM_LOGOTIPO_LOJA = GetParam("LOGOTIPO_LOJA");
            PARAM_GAVETA_ABERTA = GetParam("GAVETA_ABERTA");
            PARAM_GAVETA_PORTA = GetParam("GAVETA_PORTA");
            PARAM_GAVETA_DEFINICOES = GetParam("GAVETA_DEFINICOES");
            PARAM_LEASING = GetParam("COD_LEASING");
            PARAM_CODDOC_SC = GetParam("CODDOC_SC");
            PARAM_CODDOC_EC = GetParam("CODDOC_EC");
            PARAM_CODDOC_OS = GetParam("CODDOC_OS");
            PARAM_CODDOC_FF = GetParam("CODDOC_FF");
        }
    }
}
