﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace SISTEC_PLUS.POS.Recibos
{
    public partial class FacturaDiscritiva1 : MetroFramework.Forms.MetroForm
    {
        public FacturaDiscritiva1(string[] cod, string[] dis, string[] qtd, string[] pr, string[] vr, string[] imp, int tamanho)
        {
            InitializeComponent();
            var contatos = FacturaDescritiva2.recibo(cod, dis, qtd, pr, vr, imp, tamanho);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", contatos));
            reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        private void FacturaDiscritiva1_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
