﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;



namespace SISTEC_PLUS.POS.Recibos
{
    public partial class EntradaMercadoriaRecibo : MetroFramework.Forms.MetroForm
    {
        public EntradaMercadoriaRecibo(string Id, string Guia, string Invoice, string Data, string Loja, string Fornecedor, string numDoc, string operador, string NLoja,
           string[] referenc, string[] desc, string[] locali, string[] fob, string[] pca, string[] pv, string[] totalfob,  string[] nserie, string[] mercadoria , string[] qtd , int tamanho)
        {
            InitializeComponent();
            
            // setar os parametros
            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[10];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("Id", Id);
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Guia",Guia);//*

            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Invoice", Invoice);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("Data", Data);
            p[4] = new Microsoft.Reporting.WinForms.ReportParameter("Loja", Loja);
            p[5] = new Microsoft.Reporting.WinForms.ReportParameter("Fornecedor", Fornecedor);
            p[6] = new Microsoft.Reporting.WinForms.ReportParameter("numDoc", numDoc);
            p[7] = new Microsoft.Reporting.WinForms.ReportParameter("operador", operador);
            p[8] = new Microsoft.Reporting.WinForms.ReportParameter("NLoja", "" + NLoja);
            p[9] = new Microsoft.Reporting.WinForms.ReportParameter("itens", ""+tamanho);
     
       
            reportViewer1.LocalReport.SetParameters(p);


            var contatos = EntradaMercRecibo.recibo   ( referenc, desc, locali,  fob,  pca, pv, totalfob,   nserie, mercadoria , qtd , tamanho);
            

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSetEntradaMerc", contatos));
            reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport(); 
          
        }

        private void EntradaMercadoriaRecibo_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
