﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using SISTEC_PLUS.POS;
using SISTEC_PLUS.POS.Recibos;
using MetroFramework;

namespace SISTEC_PLUS.Recibos
{
    public partial class ReciboVendas : MetroFramework.Forms.MetroForm
    {
        string data;
        public ReciboVendas(string hora, string ID, string Moeda, string NomeCliente, string EnderecoCliente,
            string telefoneCliente, string REF, string ValorEx, string contaKZ, string contaUSD,
            string[] cod, string[] dis, string[] qtd, string[] pr, string[] vr, int tamanho) //*/
        {
            InitializeComponent();
            data = "" + Variavel.dataAtual.Day + "-" + Variavel.dataAtual.Month + "-" + Variavel.dataAtual.Year;
            
            // setar os parametros
            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[16];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("nomeLoja", Variavel.nomeLoja);
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("NIF", Empreza.sEmpresaNIF);//*
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("ID", ID);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("Moeda", Moeda);
            p[4] = new Microsoft.Reporting.WinForms.ReportParameter("Data", data);
            p[5] = new Microsoft.Reporting.WinForms.ReportParameter("Hora", hora);
            p[6] = new Microsoft.Reporting.WinForms.ReportParameter("Operador", Variavel.nomeUtilizador);
            p[7] = new Microsoft.Reporting.WinForms.ReportParameter("NomeCliente", NomeCliente);
            p[8] = new Microsoft.Reporting.WinForms.ReportParameter("EnderecoCliente", EnderecoCliente);
            p[9] = new Microsoft.Reporting.WinForms.ReportParameter("telefoneCliente", telefoneCliente);
            p[10] = new Microsoft.Reporting.WinForms.ReportParameter("nomeEmpreza", Empreza.sNomeEmp);
            p[11] = new Microsoft.Reporting.WinForms.ReportParameter("enderecoLoja", Variavel.enderecoLoja);
            p[12] = new Microsoft.Reporting.WinForms.ReportParameter("REF", REF);
            p[13] = new Microsoft.Reporting.WinForms.ReportParameter("ValorEx", ValorEx);
            p[14] = new Microsoft.Reporting.WinForms.ReportParameter("contaKZ", contaKZ);
            p[15] = new Microsoft.Reporting.WinForms.ReportParameter("contaUSD", contaUSD);//*/
            reportViewer1.LocalReport.SetParameters(p);
            
            var contatos = VendaRecib.recibo(cod, dis, qtd, pr, vr, tamanho);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", contatos));
            reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        private void ReciboVendas_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }

        private void ReciboVendas_FormClosing(object sender, FormClosingEventArgs e)
        {
            reportViewer1.LocalReport.ReleaseSandboxAppDomain();
        }
    }
}
