﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Recibos
{
    class VendaClienteRecibo
    {
        public string referencia { get; set; }
        public string descricao { get; set; }
        public string quantidade { get; set; }
        public string valorPadrao { get; set; }
        public string documento { get; set; }
        public string id { get; set; }
        public string numeroDocumento { get; set; }
        public string data { get; set; }
        public string valorDocumento { get; set; }
        public static List<VendaClienteRecibo> recibo(string[] refrenc, string[] descr, string[] qtd, string[] valor, int tamanho, string[] docum, string[] id, string[] nDoc, string[] dat, string[] valorDoc)
        {
            List<VendaClienteRecibo> cont = new List<VendaClienteRecibo>();
            for (int i = 0; i < tamanho; i++)
            {
                VendaClienteRecibo c = new VendaClienteRecibo()
                {
                    referencia = refrenc[i],
                    descricao = descr[i],
                    quantidade = qtd[i],
                    valorPadrao = valor[i],
                    documento = docum[i],
                    id = id[i],
                    numeroDocumento = nDoc[i],
                    data = dat[i],
                    valorDocumento = valorDoc[i]
                };
                cont.Add(c);
            }

            return cont;
        }
    }
}
