﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS.Recibos
{
    class RequisicaoEncomendas
    {
        public static SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public static SqlCommand cmd;
        public string data { get; set; }
        public string numeroDoc { get; set; }
        public string PCLD { get; set; }
        public string codigo { get; set; }
        public string descricao { get; set; }
        public string quantidade { get; set; }
        public string pvp { get; set; }
        public string pvpd { get; set; }
        public string armazem { get; set; }
        public string local { get; set; }
        public string nomeLoja { get; set; }
        public string destino { get; set; }

        public static List<RequisicaoEncomendas> recibo(int id, bool externa, string cliente)
        {
            List<RequisicaoEncomendas> cont = new List<RequisicaoEncomendas>();
            if (externa)
            {
                cmd = new SqlCommand("Select Distinct Cast(F.DataCria As DateTime), F.NumDoc, F.PCLD, RTRIM(F.REFERENC), RTRIM(M.DESCRARTIGO), F.QTD, F.PVP, D.DescrDoc, F.Moeda, F.PVPD, U.NOMEUTILIZADOR, Z.NOMEARZ, Ltrim(Rtrim(LOCALI)), RTrim(LTrim(A.NomeLoja)), DESTINO = CASE When Localizacao_Fisica = 'EXTERNA' THEN RTRIM(LTRIM(C.NomeEntidade))+' - '+RTRIM(LTRIM(C.CodEntidade)) " +
                    "When IsNull(Localizacao_Fisica,'INTERNA') = 'INTERNA' AND F.LOJADESTINO IS NOT NULL THEN RTRIM(LTRIM(L2.NOMELOJA))+' - '+RTRIM(LTRIM(F.LOJADESTINO)) When IsNull(Localizacao_Fisica,'INTERNA')='INTERNA' AND F.LOJADESTINO IS  NULL THEN RTRIM(LTRIM(Z2.NOMEARZ))+' - '+RTRIM(LTRIM(F.ARMZDESTINO)) End From ASMESTRE M WITH (NOLOCK), ASFICMOV1 F WITH (NOLOCK), ASDOCS D WITH (NOLOCK), ASLOJA A WITH (NOLOCK), AFENTIDADE C WITH (NOLOCK) , AREGDOC R  WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK), ASARMAZ Z  WITH (NOLOCK), ASLOJA L2 WITH (NOLOCK), ASARMAZ Z2  WITH (NOLOCK) " +
                    "Where F.REFERENC = M.REFERENC AND F.CODARMZ = M.CODARMZ AND Z.CODARMZ = M.CODARMZ AND F.CODLOJA = M.CODLOJA AND A.CODLOJA = Z.CODLOJA AND R.CODDOC=D.CODDOC AND R.CODENTIDADE = C.CODENTIDADE AND A.CODLOJA=F.CODLOJA AND R.CODLOJA=F.CODLOJA AND R.NUMDOC=F.NUMDOC AND F.CODDOC=R.CODDOC AND R.CODUTILIZADOR =U.CODUTILIZADOR AND R.IDUNICO=F.IDUNICO AND F.IdRequisicao = '" + id + "' And F.CodEntidade= '" + cliente + "' And L2.CODLOJA =* F.LOJADESTINO  And Z2.CODARMZ =* F.ARMZDESTINO ORDER BY F.NUMDOC, Z.NOMEARZ,Ltrim(Rtrim(LOCALI)), RTRIM(M.DESCRARTIGO)", conexao);
            }
            else
            {
                cmd = new SqlCommand("Select Distinct CONVERT(VARCHAR(10), CAST(F.DataCria AS DATETIME), 105), F.NumDoc, F.PCLD, RTRIM(F.REFERENC), RTRIM(M.DESCRARTIGO), F.QTD, F.PVP, D.DescrDoc, F.Moeda, F.PVPD, U.NOMEUTILIZADOR, Z.NOMEARZ, Ltrim(Rtrim(LOCALI)), RTrim(LTrim(A.NomeLoja)), DESTINO = CASE When Localizacao_Fisica = 'EXTERNA' THEN RTRIM(LTRIM(C.NomeEntidade))+' - '+RTRIM(LTRIM(C.CodEntidade)) When IsNull(Localizacao_Fisica,'INTERNA') = 'INTERNA' AND F.LOJADESTINO IS NOT NULL THEN RTRIM(LTRIM(L2.NOMELOJA))+' - '+RTRIM(LTRIM(F.LOJADESTINO))  " +
                    "When IsNull(Localizacao_Fisica,'INTERNA')='INTERNA' AND F.LOJADESTINO IS  NULL THEN RTRIM(LTRIM(Z2.NOMEARZ))+' - '+RTRIM(LTRIM(F.ARMZDESTINO)) End From ASMESTRE M WITH (NOLOCK), ASFICMOV1 F WITH (NOLOCK), ASDOCS D WITH (NOLOCK), ASLOJA A WITH (NOLOCK), AFENTIDADE C WITH (NOLOCK) , AREGDOC R  WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK), ASARMAZ Z  WITH (NOLOCK), ASLOJA L2 WITH (NOLOCK), ASARMAZ Z2  WITH (NOLOCK) " +
                    "Where F.REFERENC = M.REFERENC AND F.CODARMZ = M.CODARMZ AND Z.CODARMZ = M.CODARMZ AND F.CODLOJA = M.CODLOJA AND A.CODLOJA = Z.CODLOJA AND R.CODDOC=D.CODDOC AND R.CODENTIDADE = C.CODENTIDADE AND A.CODLOJA=F.CODLOJA AND R.CODLOJA=F.CODLOJA AND R.NUMDOC=F.NUMDOC AND F.CODDOC=R.CODDOC AND R.CODUTILIZADOR =U.CODUTILIZADOR AND R.IDUNICO=F.IDUNICO AND F.IdRequisicao = '" + id + "' And L2.CODLOJA =* F.LOJADESTINO  And Z2.CODARMZ =* F.ARMZDESTINO ORDER BY F.NUMDOC, Z.NOMEARZ,Ltrim(Rtrim(LOCALI)), RTRIM(M.DESCRARTIGO)", conexao);
            }
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read()) 
                    {
                        RequisicaoEncomendas c = new RequisicaoEncomendas()
                        {
                            data = reader[0].ToString(),
                            numeroDoc = reader["NumDoc"].ToString() + "  ---  " + reader[13].ToString(),
                            PCLD = reader["PCLD"].ToString(),
                            codigo = reader[3].ToString(),
                            descricao = reader[4].ToString(),
                            quantidade = reader["QTD"].ToString(),
                            pvp = reader[6].ToString(),
                            pvpd = reader[9].ToString(),
                            armazem = reader["NOMEARZ"].ToString(),
                            local = reader[11].ToString(),
                            nomeLoja = reader[13].ToString(),
                            destino = reader["DESTINO"].ToString()
                        };
                        cont.Add(c);
                    }
                }
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
            
            return cont;
        }
    }
}
