﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS.Recibos
{
    public partial class FacturaDescritiva : MetroFramework.Forms.MetroForm
    {
        string[] cod = { "a" }, dis = { "a" }, qtd = { "a" }, pr = { "a" }, vr = { "a" };
        public FacturaDescritiva(string nome, string codigo, string morada, string Morada, string telefone, string postal, string ID, string factura, string area, string descricao, string valor, string data)
        {
            InitializeComponent();

            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[14];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("endereco", Morada);
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("telefone", telefone);
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("postal", postal);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("NIF", Empreza.sEmpresaNIF);
            p[4] = new Microsoft.Reporting.WinForms.ReportParameter("email", Empreza.sEmpMail);
            p[5] = new Microsoft.Reporting.WinForms.ReportParameter("factura", factura);
            p[6] = new Microsoft.Reporting.WinForms.ReportParameter("data", data);
            p[7] = new Microsoft.Reporting.WinForms.ReportParameter("area", area);
            p[8] = new Microsoft.Reporting.WinForms.ReportParameter("descricao", descricao);
            p[9] = new Microsoft.Reporting.WinForms.ReportParameter("valor", valor);
            p[10] = new Microsoft.Reporting.WinForms.ReportParameter("ID", ID);
            p[11] = new Microsoft.Reporting.WinForms.ReportParameter("nomeEntidade", nome);
            p[12] = new Microsoft.Reporting.WinForms.ReportParameter("codEntidade", codigo);
            p[13] = new Microsoft.Reporting.WinForms.ReportParameter("morada", morada);

            reportViewer1.LocalReport.SetParameters(p);

            var contatos = VendaRecib.recibo(cod, dis, qtd, pr, vr, 1);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", contatos));
            reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        private void FacturaDescritiva_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }
    }
}
