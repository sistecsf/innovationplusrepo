﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Recibos
{
    class RequisicaoInterna
    {
        public string codigo { get; set; }
        public string designacao { get; set; }
        public string quantidade { get; set; }
        public string stock { get; set; }
        public string armazem { get; set; }
        public string local { get; set; }

        public static List<RequisicaoInterna> recibo(string[] cod, string[] dis, string[] qtd, string[] st, string[] ar, string[] lc, int tamanho)
        {
            List<RequisicaoInterna> cont = new List<RequisicaoInterna>();
            for (int i = 0; i < tamanho; i++)
            {
                RequisicaoInterna c = new RequisicaoInterna()
                {
                    codigo = cod[i],
                    designacao = dis[i],
                    quantidade = qtd[i],
                    stock = st[i],
                    armazem = ar[i],
                    local = lc[i]
                };
                cont.Add(c);
            }

            return cont;
        }
    }
}
