﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace SISTEC_PLUS.POS.Recibos
{
    public partial class VendaCliente : MetroFramework.Forms.MetroForm
    {
        public VendaCliente(string[] refrenc, string[] descr, string[] qtd, string[] valor, string[] docum, string[] id, string[] nDoc, string[] dat, string[] valorDoc, int tamanho, string codCliente, string nomeCliente, string data)
        {
            InitializeComponent();

            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("codCliente", codCliente);
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("nomeCliente", nomeCliente);
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.nomeUtilizador);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("data", ""+Variavel.dataAtual);
            p[4] = new Microsoft.Reporting.WinForms.ReportParameter("data2", data);

            reportViewer1.LocalReport.SetParameters(p);
            
            var contatos = VendaClienteRecibo.recibo(refrenc,descr,qtd,valor,tamanho,docum,id,nDoc,dat,valorDoc);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", contatos));
            reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        private void VendaCliente_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }

        private void VendaCliente_FormClosing(object sender, FormClosingEventArgs e)
        {
            reportViewer1.LocalReport.ReleaseSandboxAppDomain();
        }
    }
}
