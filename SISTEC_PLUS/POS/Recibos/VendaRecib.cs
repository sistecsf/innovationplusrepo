﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Recibos
{
    class VendaRecib
    {
        public string codigo { get; set; }
        public string designacao { get; set; }
        public string quantidade { get; set; }
        public string preco { get; set; }
        public string valor { get; set; }

        public static List<VendaRecib> recibo(string[] cod, string[] dis, string[] qtd, string[] pr, string[] vr, int tamanho)
        {
            List<VendaRecib> cont = new List<VendaRecib>();
            for (int i = 0; i < tamanho; i++)
            {
                VendaRecib c = new VendaRecib()
                {
                    codigo = cod[i],
                    designacao = dis[i],
                    quantidade = qtd[i],
                    preco = pr[i],
                    valor = vr[i]
                };
                cont.Add(c);
            }

            return cont;
        }
    }
}
