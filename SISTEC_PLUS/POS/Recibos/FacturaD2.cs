﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS.POS
{
    public partial class FacturaD2 : MetroFramework.Forms.MetroForm
    {
        public FacturaD2(string telefone, string postal, string Morada, string PRECO, string refe, string ref2, string desc, string imposto, string codCliente, string nome, string morada, string ID, string factura, string moeda, string data)
        {
            InitializeComponent();

            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[17];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("endereco", Morada);
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("telefone", telefone);
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("postal", postal);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("NIF", Empreza.sEmpresaNIF);
            p[4] = new Microsoft.Reporting.WinForms.ReportParameter("email", Empreza.sEmpMail);
            p[5] = new Microsoft.Reporting.WinForms.ReportParameter("factura", factura);
            p[6] = new Microsoft.Reporting.WinForms.ReportParameter("data", data);
            p[7] = new Microsoft.Reporting.WinForms.ReportParameter("moeda", moeda);
            p[8] = new Microsoft.Reporting.WinForms.ReportParameter("ID", ID);
            p[9] = new Microsoft.Reporting.WinForms.ReportParameter("codEntidade", codCliente);
            p[10] = new Microsoft.Reporting.WinForms.ReportParameter("nomeEntidade", nome);
            p[11] = new Microsoft.Reporting.WinForms.ReportParameter("Morada", morada);
            p[12] = new Microsoft.Reporting.WinForms.ReportParameter("desconto", desc);
            p[13] = new Microsoft.Reporting.WinForms.ReportParameter("imposto", imposto);
            p[14] = new Microsoft.Reporting.WinForms.ReportParameter("ref", refe);
            p[15] = new Microsoft.Reporting.WinForms.ReportParameter("ref2", ref2);
            p[16] = new Microsoft.Reporting.WinForms.ReportParameter("preco", PRECO);

            reportViewer1.LocalReport.SetParameters(p);

            //var contatos = FacturaDesc2.recibo(cod, dis, qtd, pr, vr, imp, tamanho);
            var contatos = FacturaDesc2.facturaDescritiva2(ID);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", contatos));
            reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        private void FacturaD2_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
