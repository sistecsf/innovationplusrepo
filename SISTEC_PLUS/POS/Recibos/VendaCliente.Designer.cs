﻿namespace SISTEC_PLUS.POS.Recibos
{
    partial class VendaCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.VendaClienteRecibo2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.VendaClienteReciboBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.VendaClienteRecibo2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendaClienteReciboBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // VendaClienteRecibo2BindingSource
            // 
            this.VendaClienteRecibo2BindingSource.DataSource = typeof(SISTEC_PLUS.POS.Recibos.VendaClienteRecibo2);
            // 
            // VendaClienteReciboBindingSource
            // 
            this.VendaClienteReciboBindingSource.DataSource = typeof(SISTEC_PLUS.POS.Recibos.VendaClienteRecibo);
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.VendaClienteRecibo2BindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "SISTEC_PLUS.POS.Recibos.VendaCliente.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(20, 60);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(723, 385);
            this.reportViewer1.TabIndex = 0;
            // 
            // VendaCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 465);
            this.Controls.Add(this.reportViewer1);
            this.Name = "VendaCliente";
            this.Text = "Recibo";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VendaCliente_FormClosing);
            this.Load += new System.EventHandler(this.VendaCliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.VendaClienteRecibo2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VendaClienteReciboBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource VendaClienteReciboBindingSource;
        private System.Windows.Forms.BindingSource VendaClienteRecibo2BindingSource;
    }
}