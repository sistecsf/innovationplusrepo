﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Recibos
{
    class VendaClienteRecibo2
    {
        public static int i, a;
        public string documento { get; set; }
        public string id { get; set; }
        public string numeroDocumento { get; set; }
        public string valorDocumento { get; set; }
        public static List<VendaClienteRecibo2> recibo(string[] refrenc, string[] descr, string[] qtd, string[] valor, int tamanho, string[] docum, string[] id, string[] nDoc, string[] dat, string[] valorDoc)
        {
            i = a = 0;
            bool ver = false;
            List<VendaClienteRecibo2> cont = new List<VendaClienteRecibo2>();
            while (i < tamanho)
            {
                foreach (VendaClienteRecibo2 aPart in cont)
                {
                    Console.WriteLine(aPart);
                    if (aPart.numeroDocumento == "Nº Doc:" + nDoc[i]) { ver = true; }
                }
                if (!ver)
                {
                        VendaClienteRecibo2 c = new VendaClienteRecibo2()
                          {
                              documento = docum[i],
                              id = "ID: " + id[i] + "      " + "Data: " + dat[i],
                              numeroDocumento = "Nº Doc:" + nDoc[i],
                              valorDocumento = valorDoc[i]
                          };
                           
                        cont.Add(c);
                        VendaClienteRecibo2 c2 = new VendaClienteRecibo2()
                        {
                            documento = refrenc[i],
                            id = descr[i],
                            numeroDocumento = qtd[i],
                            valorDocumento = valor[i]
                        };
                        cont.Add(c2);
                    }
                    else
                    {
                        VendaClienteRecibo2 c = new VendaClienteRecibo2()
                        {
                            documento = refrenc[i],
                            id = descr[i],
                            numeroDocumento = qtd[i],
                            valorDocumento = valor[i]
                        };
                        cont.Add(c);
                    }
                i++;
                a++;
                ver = false;
            }
            return cont;
        }
    }
}
