﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Recibos
{
    class FacturaDescritiva2
    {
        public string item { get; set; }
        public string designacao { get; set; }
        public string quantidade { get; set; }
        public string preco { get; set; }
        public string imposto { get; set; }
        public string valor { get; set; }

        public static List<FacturaDescritiva2> recibo(string[] cod, string[] dis, string[] qtd, string[] pr, string[] imp, string[] vr, int tamanho)
        {
            List<FacturaDescritiva2> cont = new List<FacturaDescritiva2>();
            for (int i = 0; i < tamanho; i++)
            {
                FacturaDescritiva2 c = new FacturaDescritiva2()
                {
                    item = cod[i],
                    designacao = dis[i],
                    quantidade = qtd[i],
                    imposto = imp[i],
                    preco = pr[i],
                    valor = vr[i]
                };
                cont.Add(c);
            }

            return cont;
        }
    }
}
