﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS.Recibos
{
    public partial class reportViewReciboPagamentoDeFactura : MetroFramework.Forms.MetroForm
    {
        string idUnico;
        SqlConnection Conexao = new SqlConnection(Variavel.Conexao);
        //campos do report
        int numeroDoDocumento;
        string numeroDaFacturaRecibo;
        double valorEmMoedaNacional;
        double valorEmMoedaPadrao;
        string codigoDaEntidade;
        string nomeDoCliente;
        string formaDePagamento;
        string descricaoDoRecibo;
        string nomeDoUtilizador;
        string dataDeCriacao;
        string observacao;
        string codigoDoDocumento;
        string diferencaDeDias;


        string nomeDaAreaOrganica;
        string codigoDaAreaOrganica;
        string nomeDaEmpresa;
        string ObsercacaoEmpresa;
        string morada1;
        string morada2;
        string telefone;
        string telemovel;
        string caixaPostal;
        string email;
        string logo;
        string NIF;
        string fax;
        string valorEmMoedaNacionalExtenca;
        string valorEmMoedaPadraoExtenca;

        public reportViewReciboPagamentoDeFactura( string idUnico)
        {
            this.idUnico = idUnico;
            InitializeComponent();
        }

        private void reportViewReciboPagamentoDeFactura_Load(object sender, EventArgs e)
        {
            //Consulta para popular o report
            SqlCommand cmd = new SqlCommand("SELECT "
                                                    + "R.Numdoc as numeroDoDocumento,"
                                                    + " R.Numfact as numeroDaFacturaRecibo,"
                                                    + " R.Preco as valorEmMoedaNacional,"
                                                    + " R.PrecoD as valorEmMoedaPadrao, "
                                                    + " R.codEntidade as codigoDaEntidade, "
                                                    + "C.NomeEntidade as nomeDoCliente,"
                                                    + " R2.Forma_Pagamento as formaDePagamento,"
                                                    + " R2.DescRec as descricaoDoRecibo,"
                                                    + " RTRIM(U.NomeUtilizador) as nomeDoUtilizador, "
                                                    + "I.NomeAO as codigoDaAreoOrganica,"
                                                    + " CONVERT(VARCHAR(10),CAST (R.DATACRIA AS DATETIME), 105) as dataDeCriacao,"
                                                    + " ISNULL ( R.OBS , ' ' ) AS observacao,"
                                                    + " R.CODDOC as codigoDoDocumento, "
                                                    + "DATEDIFF(dd,R1.DATACRIA,R.DATACRIA) as direfencaDeDias  "
                                            + "FROM       AREGDOC R  WITH (NOLOCK) ,"
                                                    + " AREGDOC R1  WITH (NOLOCK) ,"
                                                    + " ATRECIBO R2  WITH (NOLOCK) , "
                                                    + "AFENTIDADE C  WITH (NOLOCK) ,"
                                                    + " UTILIZADORES U  WITH (NOLOCK),"
                                                    + " AIAREAORGANICA I  WITH (NOLOCK),"
                                                    + " ASLOJA L  WITH (NOLOCK), "
                                                    + "ASDOCS D "
                                            + "WHERE   R.Codutilizador = U.Codutilizador "
                                                    + " AND R.Idunico = R2.Idunico "
                                                    + " AND R.CodEntidade = C.CodEntidade "
                                                    + " AND R.CodAO = I.CodAO "
                                                    + " AND R.CodLoja *= L.CodLoja "
                                                    + " AND R.IDORIG = R1.IDUNICO "
                                                    + " AND R.IDORIG IS NOT NULL "
                                                    + " AND R1.CODDOC = D.CODDOC "
                                                    + " AND R.IDUnico = '" + this.idUnico + "' "
                                            + "UNION ALL "
                                            + "SELECT R.Numdoc as numeroDoDocumento,"
                                                    + " R.Numfact as numeroDaFacturaRecibo,"
                                                    + " R.Preco as valorEmMoedaNacional,"
                                                    + " R.PrecoD as valorEmMoedaPadrao,"
                                                     + " R.codEntidade as codigoDaEntidade, "
                                                    + " C.NomeEntidade as nomeDoCliente,"
                                                    + " R2.Forma_Pagamento as formaDePagamento,"
                                                    + " R2.DescRec as descricaoDoRecibo,"
                                                    + " RTRIM(U.NomeUtilizador) as nomeDoUtilizador,"
                                                    + " I.NomeAO as codigoDaAreaOrganica,"
                                                    + " CONVERT(VARCHAR(10),CAST (R.DATACRIA AS DATETIME), 105) as dataDeCriacao,"
                                                    + " R.DESCR AS observacao,"
                                                    + " R.CODDOC as codigoDoDocumento,"
                                                    + " DATEDIFF(dd,R2.DATACRIA,R.DATACRIA) as direfencaDeDias  "
                                            + " FROM AREGDOC R  WITH (NOLOCK),"
                                                    + " ATRECIBO R2  WITH (NOLOCK), "
                                                    + " AFENTIDADE C  WITH (NOLOCK),"
                                                    + " UTILIZADORES U  WITH (NOLOCK),"
                                                    + " AIAREAORGANICA I WITH (NOLOCK), "
                                                    + "ASLOJA L With (NoLock),"
                                                    + " ASDOCS D  WITH (NOLOCK) "
                                            + "WHERE   R.Codutilizador = U.Codutilizador "
                                                    + " AND R.Idunico = R2.Idunico "
                                                    + " AND R.CodEntidade = C.CodEntidade "
                                                    + " AND R.CodAO = I.CodAO "
                                                    + " AND  R.CodLoja *= L.CodLoja "
                                                    + " AND R.Coddoc = D.Coddoc "
                                                    + " AND  R.IDORIG IS NULL "
                                                    + " AND  R.IDUnico = '" + this.idUnico + "'"
                                                    , Conexao);

            SqlCommand cmd2 = new SqlCommand("SELECT  E.NOME as nomeDaEmpresa,"
                                                      + " D.Obs as observacao,"
                                                      + " E.Mora1 as morada1,"
                                                      + " E.TELEF as telefone,"
                                                      + " E.Caixa as caixaPostal,"
                                                      + " E.Email,"
                                                      + " E.Logo,"
                                                      + " E.NIF,"
                                                      + " C.Morada as morada2,"
                                                      + " C.Fone as telemovel, "
                                                      + " C.Fax as fax,"
                                                      + " Convert(Varchar(10), Cast(D.DataCria As DateTime), 105) as dataDeCriacao,  "
                                                      + " I.NomeAO as nomeDaAreaOrganica, "
                                                      + " I.CodAO as codigoDaAreaOrganica "
                                               + " From AREGDOC D  WITH (NOLOCK), "
                                                        + "AFENTIDADE C  WITH (NOLOCK),"
                                                        + "UTILIZADORES U  WITH (NOLOCK),"
                                                        + "AIAREAORGANICA I  WITH (NOLOCK),"
                                                        + "EMPRESA E  WITH (NOLOCK) , "
                                                        + "ASLOJA L  WITH (NOLOCK)  "
                                               + " Where  D.Codutilizador = U.Codutilizador "
                                                        + " and D.CodEntidade = C.CodEntidade "
                                                        + " and D.CodAO = I.CodAO "
                                                        + " and D.CodLoja = L.CodLoja "
                                                        + " and D.IDUnico = '" + this.idUnico + "'", Conexao);


            //leitura dos dados da consulta
            Conexao.Open();
            SqlDataReader res = cmd.ExecuteReader();
            
            while(res.Read())
            {
                this.numeroDoDocumento = Convert.ToInt32(res["numeroDoDocumento"].ToString());
                this.numeroDaFacturaRecibo = res["numeroDaFacturaRecibo"].ToString();
                this.valorEmMoedaNacional = Convert.ToDouble(res["valorEmMoedaNacional"].ToString());
                this.valorEmMoedaPadrao = Convert.ToDouble(res["valoremMoedaPadrao"].ToString());
                this.nomeDoCliente = res["nomeDoCliente"].ToString();
                this.formaDePagamento = res["formaDePagamento"].ToString();
                this.descricaoDoRecibo = res["descricaoDoRecibo"].ToString();
                this.nomeDoUtilizador = res["nomeDoUtilizador"].ToString();
                this.codigoDaEntidade = res["codigoDaEntidade"].ToString();
                this.dataDeCriacao = res["dataDeCriacao"].ToString();
                this.observacao = (res["observacao"].ToString() == "" || res["observacao"].ToString() == null) ? " " : res["observacao"].ToString();
                this.codigoDoDocumento = res["codigoDoDocumento"].ToString();
                this.diferencaDeDias = res["direfencaDeDias"].ToString();  
            }
            res.Close();
            Conexao.Close();

            //leitura do select dos dados da empresa
            Conexao.Open();
            SqlDataReader res2 = cmd2.ExecuteReader();

            //verifica se retornou algum dadO
            while (res2.Read())
            {
                this.nomeDaAreaOrganica = (res2["nomeDaAreaOrganica"].ToString() == "" || res2["nomeDaAreaOrganica"].ToString() == null) ? " " : res2["nomeDaAreaOrganica"].ToString();
                this.codigoDaAreaOrganica = (res2["codigoDaAreaOrganica"].ToString() == "" || res2["codigoDaAreaOrganica"].ToString() == null) ? " " : res2["codigoDaAreaOrganica"].ToString();
                this.nomeDaEmpresa = (res2["nomeDaEmpresa"].ToString() == "" || res2["nomeDaEmpresa"].ToString() == null) ? " " : res2["nomeDaEmpresa"].ToString();
                this.telefone = (res2["telefone"].ToString() == "" || res2["telefone"].ToString() == null) ? " " : res2["telefone"].ToString();
                this.fax = (res2["fax"].ToString() == "" || res2["fax"].ToString() == null) ? " " : res2["fax"].ToString();
                this.morada1 = (res2["morada1"].ToString() == "" || res2["morada1"].ToString() == null) ? " " : res2["morada1"].ToString();
                this.morada2 = (res2["morada2"].ToString() == "" || res2["morada2"].ToString() == null) ? " " : res2["morada2"].ToString();
                this.caixaPostal = (res2["caixaPostal"].ToString() == "" || res2["caixaPostal"].ToString() == null) ? " " : res2["caixaPostal"].ToString();
                this.email = (res2["Email"].ToString() == "" || res2["Email"].ToString() == null) ? " " : res2["Email"].ToString();
                this.logo = (res2["logo"].ToString() == "" || res2["logo"].ToString() == null) ? " " : res2["logo"].ToString();
                this.NIF = (res2["NIF"].ToString() == "" || res2["NIF"].ToString() == null) ? " " : res2["NIF"].ToString();
                this.telemovel = (res2["telemovel"].ToString() == "" || res2["telemovel"].ToString() == null) ? " " : res2["telemovel"].ToString();
            }

            res2.Close();
            Conexao.Close();

            //criacao de parametro para o report
            ReportParameter[] parametros = new ReportParameter[27];
            parametros[0] = new ReportParameter("numeroDoDocumento", this.numeroDoDocumento.ToString());
            parametros[1] = new ReportParameter("numeroDaFacturaRecibo", this.numeroDaFacturaRecibo);
            parametros[2] = new ReportParameter("valorEmMoedaNacional", this.valorEmMoedaNacional.ToString());
            parametros[24] = new ReportParameter("codigoDaEntidade", this.codigoDaEntidade);
            parametros[3] = new ReportParameter("valorEmMoedaPadrao", this.valorEmMoedaPadrao.ToString());
            parametros[4] = new ReportParameter("nomeDoCliente", this.nomeDoCliente);
            parametros[5] = new ReportParameter("formaDePagamento", this.formaDePagamento);
            parametros[23] = new ReportParameter("descricaoDoRecibo", this.descricaoDoRecibo);
            parametros[6] = new ReportParameter("nomeDoUtilizador", this.nomeDoUtilizador);
            parametros[7] = new ReportParameter("dataDeCriacao", this.dataDeCriacao);
            parametros[8] = new ReportParameter("observacao", this.observacao);
            parametros[9] = new ReportParameter("codigoDoDocumento", this.codigoDoDocumento);
            parametros[10] = new ReportParameter("diferencaDeDias", this.diferencaDeDias);

            parametros[11] = new ReportParameter("nomeDaAreaOrganica", this.nomeDaAreaOrganica);
            parametros[12] = new ReportParameter("codigoDaAreaOrganica", this.codigoDaAreaOrganica);
            parametros[13] = new ReportParameter("nomeDaEmpresa", this.nomeDaEmpresa);
            parametros[14] = new ReportParameter("telefone", this.telefone);
            parametros[15] = new ReportParameter("fax", this.fax);
            parametros[16] = new ReportParameter("morada1", this.morada1);
            parametros[17] = new ReportParameter("morada2", this.morada2);
            parametros[18] = new ReportParameter("caixaPostal", this.caixaPostal);
            parametros[19] = new ReportParameter("email", this.email);
            parametros[20] = new ReportParameter("logo", this.logo);
            parametros[21] = new ReportParameter("NIF", this.NIF);
            parametros[22] = new ReportParameter("telemovel", this.telemovel);
            
            //valores por extenco
            this.valorEmMoedaNacionalExtenca = Extenso.toExtenso(this.valorEmMoedaNacional, Extenso.TipoValorExtenso.Decimal);
            this.valorEmMoedaPadraoExtenca = Extenso.toExtenso(this.valorEmMoedaPadrao, Extenso.TipoValorExtenso.Decimal);

            parametros[25] = new ReportParameter("valorEmMoedaNacionalExtenca", this.valorEmMoedaNacionalExtenca);
            parametros[26] = new ReportParameter("valorEmMoedaPadraoExtenca", this.valorEmMoedaPadraoExtenca);

            reportViewer1.LocalReport.SetParameters(parametros);
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.Refresh();

            //Carrega o report
            this.reportViewer1.RefreshReport();
        }
    }
}
