﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS.POS.Recibos
{
    public partial class Requisicao : MetroFramework.Forms.MetroForm
    {
        public Requisicao(int tamanho, string data, string fatura, string id,
            string loja1, string utilizador1, string utilizador2, string status, string lojaDestino)
        {
            InitializeComponent();

            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[8];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("data", data);
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("fatura", fatura);
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("id", id);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja1", loja1);
            p[4] = new Microsoft.Reporting.WinForms.ReportParameter("utilizador1", utilizador1);
            p[5] = new Microsoft.Reporting.WinForms.ReportParameter("utilizador2", utilizador2);
            p[6] = new Microsoft.Reporting.WinForms.ReportParameter("status", status);
            p[7] = new Microsoft.Reporting.WinForms.ReportParameter("lojaDestino", lojaDestino);

            reportViewer1.LocalReport.SetParameters(p);

            //var contatos = FacturaDesc2.recibo(cod, dis, qtd, st, local, arm, tamanho);
            var contatos = FacturaDesc2.requisicao(id);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", contatos));
            reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        private void Requisicao_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }
    }
}
