﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Recibos
{
    class EntradaMercRecibo
    {

        public string referencia { get; set; }
        public string descricao { get; set; }
        public string localizacao { get; set; }
        public string fob{ get; set; }
        public string pca { get; set; }

        public string pv { get; set; }

        public string totalFob { get; set; }

        public string nserie{ get; set; }

        public string mercadoria { get; set; }

        public string qtd { get; set; }

    





        public static List<EntradaMercRecibo> recibo(string[] referenc, string[] desc, string[] locali, string[] fob, string[] pca, string[] pv, string[] totalfob,  string[] nserie, string[] mercadoria , string[] qtd , int tamanho)
        {
            List<EntradaMercRecibo> cont = new List<EntradaMercRecibo>();
            for (int i = 0; i < tamanho; i++)
            {
                EntradaMercRecibo c = new EntradaMercRecibo()
                {
                    referencia = referenc[i],
                    descricao = desc[i],
                    localizacao = locali[i],
                    fob =fob[i],
                    pca = pca[i],
                    pv =  pv[i],
                    totalFob = totalfob[i],
                    nserie =  nserie [i],
                    mercadoria = "MERCADORIA:  "+mercadoria[i],
                    qtd = qtd[i]

                };
                cont.Add(c);
            }

            return cont;
        }

    }
}
