﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS.Recibos
{
    class FacturaDesc2
    {
        public static SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public static SqlCommand cmd;
        public string item { get; set; }
        public string designacao { get; set; }
        public string quantidade { get; set; }
        public string preco { get; set; }
        public string imposto { get; set; }
        public string valor { get; set; }
        public static List<FacturaDesc2> facturaDescritiva2(string id)
        {
            List<FacturaDesc2> cont = new List<FacturaDesc2>();
            cmd = new SqlCommand("SELECT Item, Descricao, Qtd,PrecoUnit, ImpostoConsumo, PrecoTotal FROM ASDETALHE where IDUnico = '" + id + "' AND CodDoc = 'FD'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        FacturaDesc2 c = new FacturaDesc2()
                        {
                            imposto = reader["ImpostoConsumo"].ToString(),
                            item = reader["Item"].ToString(),
                            designacao = reader["Descricao"].ToString(),
                            preco = reader["PrecoUnit"].ToString(),
                            quantidade = reader["Qtd"].ToString(),
                            valor = reader["PrecoTotal"].ToString()
                        };
                        cont.Add(c);
                    }
                }
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }

            return cont;
        }
        public static List<FacturaDesc2> proformaDescritiva(string id)
        {
            List<FacturaDesc2> cont = new List<FacturaDesc2>();
            cmd = new SqlCommand("SELECT Item, Descricao, Qtd,PrecoUnit, ImpostoConsumo, PrecoTotal FROM ASDETALHE where IDUnico = '" + id + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        FacturaDesc2 c = new FacturaDesc2()
                        {
                            imposto = reader["ImpostoConsumo"].ToString(),
                            item = reader["Item"].ToString(),
                            designacao = reader["Descricao"].ToString(),
                            preco = reader["PrecoUnit"].ToString(),
                            quantidade = reader["Qtd"].ToString(),
                            valor = reader["PrecoTotal"].ToString()
                        };
                        cont.Add(c);
                    }
                }
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }

            return cont;
        }

        public static List<FacturaDesc2> recibo(string[] cod, string[] dis, string[] qtd, string[] pr, string[] vr, string[] imp, int tamanho)
        {
            List<FacturaDesc2> cont = new List<FacturaDesc2>();
            for (int i = 0; i < tamanho; i++)
            {
                FacturaDesc2 c = new FacturaDesc2()
                {
                    item = cod[i],
                    designacao = dis[i],
                    quantidade = qtd[i],
                    preco = pr[i],
                    imposto = imp[i],
                    valor = vr[i]
                };
                cont.Add(c);
            }

            return cont;
        }

        public static List<FacturaDesc2> requisicao(string id)
        {
            List<FacturaDesc2> cont = new List<FacturaDesc2>();
            cmd = new SqlCommand("select distinct A.NomeArz, F.Referenc, M.DescrArtigo, F.X1, F.QTD, M.LOCALI from ASFICMOV1 F WITH (NOLOCK), ASARMAZ A WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), ASLOJA L WITH (NOLOCK) "+
                "where F.IDUnico = '" + id + "' AND F.Referenc = M.Referenc AND F.CodArmz = A.CodArmz AND F.LojaDestino = L.CodLoja AND F.CodLoja = A.CodLoja and M.CodArmz = A.CodArmz", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        FacturaDesc2 c = new FacturaDesc2()
                        {
                            imposto = reader["NomeArz"].ToString(),
                            item = reader["Referenc"].ToString(),
                            designacao = reader["DescrArtigo"].ToString(),
                            preco = reader["X1"].ToString(),
                            quantidade = reader["QTD"].ToString(),
                            valor = reader["LOCALI"].ToString()
                        };
                        cont.Add(c);
                    }
                }
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }

            return cont;
        }
    }
}
