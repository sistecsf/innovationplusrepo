﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace SISTEC_PLUS.POS.Recibos
{
    public partial class RequisicaoEncom : MetroFramework.Forms.MetroForm
    {
        public RequisicaoEncom(int tipo, bool externa, string codCliente, string id, string dias, string percentagem)
        {
            InitializeComponent();

            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[3];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("id", id);
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("dias", dias);
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("percentagem", percentagem);

            reportViewer1.LocalReport.SetParameters(p);

            var contatos = RequisicaoEncomendas.recibo(tipo, externa, codCliente);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", contatos));
            reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        private void RequisicaoEncom_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }
    }
}
