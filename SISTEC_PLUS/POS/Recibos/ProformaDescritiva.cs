﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS.POS.Recibos
{
    public partial class ProformaDescritiva : MetroFramework.Forms.MetroForm
    {
        public ProformaDescritiva(string PEM, string INSTALACAO, string GARANTIA, string desconto, string imposto, string total, string NumFatura, string IDUnico, string moeda, string data, string conta, string endereco, string telefone, string cliente)
        {
            InitializeComponent();
            
            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[14];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("NomeCliente", cliente);
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("telefoneCliente", telefone);
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("EnderecoCliente", endereco);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("conta", conta);
            p[4] = new Microsoft.Reporting.WinForms.ReportParameter("NumFatura", NumFatura);
            p[5] = new Microsoft.Reporting.WinForms.ReportParameter("IDUnico", IDUnico);
            p[6] = new Microsoft.Reporting.WinForms.ReportParameter("moeda", moeda);
            p[7] = new Microsoft.Reporting.WinForms.ReportParameter("data", data);
            p[8] = new Microsoft.Reporting.WinForms.ReportParameter("total", total);
            p[9] = new Microsoft.Reporting.WinForms.ReportParameter("imposto", imposto);
            p[10] = new Microsoft.Reporting.WinForms.ReportParameter("desconto", desconto);
            p[11] = new Microsoft.Reporting.WinForms.ReportParameter("GARANTIA", GARANTIA);
            p[12] = new Microsoft.Reporting.WinForms.ReportParameter("INSTALACAO", INSTALACAO);
            p[13] = new Microsoft.Reporting.WinForms.ReportParameter("PEM", PEM);

            reportViewer1.LocalReport.SetParameters(p);

            //var contatos = FacturaDesc2.recibo(cod, dis, qtd, pr, vr, imp, tamanho);
            var contatos = FacturaDesc2.proformaDescritiva(IDUnico);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", contatos));
            reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        private void ProformaDescritiva_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
