﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace SISTEC_PLUS.POS.Recibos
{
    public partial class RequisicaoAtend : MetroFramework.Forms.MetroForm
    {
        string data;
        public RequisicaoAtend(string numero, string dat, string loja, string requisicao, string id, string[] cod, string[] dis, string[] qtd, string[] st, string[] local, string[] arm, int tamanho)
        {
            InitializeComponent();
            
            data = "" + Variavel.dataAtual.Day + "-" + Variavel.dataAtual.Month + "-" + Variavel.dataAtual.Year;

            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[8];
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("id", id);
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("requisicao", requisicao);//*
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("data", dat);
            p[4] = new Microsoft.Reporting.WinForms.ReportParameter("numero", numero);
            p[5] = new Microsoft.Reporting.WinForms.ReportParameter("loja1", Variavel.nomeLoja);
            p[6] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.nomeUtilizador);
            p[7] = new Microsoft.Reporting.WinForms.ReportParameter("dataAtual", data);
            
            reportViewer1.LocalReport.SetParameters(p);

            var contatos = RequisicaoInterna.recibo(cod, dis, qtd, st, arm, local, tamanho);

            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", contatos));
            reportViewer1.LocalReport.Refresh();
            this.reportViewer1.RefreshReport();
        }

        private void RequisicaoAtend_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
