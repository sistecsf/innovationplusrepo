﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormInventarioFisico : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;
        string idUnico, nG, nD, sTipMove, nQtdDif, nF_Kz, nC_Kz, nP_Kz;
        int i = 0, count = 0;
        string[] sCodLoja, sArmaz, sRefere, sLocali, nFob, nPca, nPvp, nQtdSist, nQtdFis, sDataInv, sDataFechoInv;
        SqlCommand comand;
        public FormInventarioFisico()
        {
            InitializeComponent();
        }
        private void gravarAbertura()
        {
            string dataAberto = "";
            const string message = "TEM CERTEZA QUE DESEJA FAZER O FECHO DO INVENTÁRIO?";
            const string caption = "CONFIRMAÇÃO";
            
            if (cbFechoInventario.Checked)
            {
                var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    fAcerto_QTDSistemaX(); //*
                    if (txtADataControle.Text != "")
                        fFecha_InventarioAjustado();
                    else
                        fFecha_InventarioFisico(); //*/
                }
                cbFechoInventario.Checked = false;
            }
            else
            {
                SqlCommand cmd2 = new SqlCommand("SELECT DataCria FROM ASINVENTARIO WHERE DATA_FECHAINV IS NULL AND CODLOJA= '" + lbACodLoja.Text + "'", conexao);
                try
                {
                    conexao.Open();
                    using (SqlDataReader reader = cmd2.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dataAberto = reader[0].ToString();
                        }
                    }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
                finally
                {
                    conexao.Close();
                }
                if (dataAberto != "" && dataAberto != null)
                {
                    MessageBox.Show("A " + cmbAInvertarioLoja.Text + " tem Fecho de Inventário Pendente! Obrigado");
                }
                else
                {
                    conexao.Open();
                    SqlTransaction tran = conexao.BeginTransaction();
                    try
                    {
                        SqlCommand comando = new SqlCommand("UPDATE ASMESTRE SET QTD=QTDMOVIMENTO FROM ASMESTRE M, VW_MOVIMENTO02 F WHERE M.CODLOJA = '" + lbACodLoja.Text + "' AND F.QTDMOVIMENTO!=QTD AND M.CODLOJA=F.CODLOJA AND M.CODARMZ=F.CODARMZ AND M.REFERENC=F.REFERENC AND CONTASTOCKS='S'");
                        comando.Connection = conexao;
                        comando.Transaction = tran;
                        comando.ExecuteNonQuery();

                        SqlCommand cmd1 = new SqlCommand("INSERT INTO ASINVENTARIO (CODLOJA, CODARMZ, REFERENC, LOCALI, FOB, PCA, PVP, CONTASTOCKS, PMC, DATACRIA, QTD_SIST, QuantidadeSistemaX, CODUTILIZADOR, DATA_ABREINV) " +
                             "SELECT CODLOJA, CODARMZ, REFERENC, LOCALI, FOB, PCA, PVP, CONTASTOCKS, PMC, Convert(Varchar(20),GetDate(),120), QTD, QTD, '" + Variavel.codUtilizador + "', Convert(Varchar(10),GetDate(),120) From ASMESTRE Where CODLOJA = '" + lbACodLoja.Text + "' And ContaStocks='S' And Activo='S'");

                        cmd1.Connection = conexao;
                        cmd1.Transaction = tran;
                        cmd1.ExecuteNonQuery();

                        tran.Commit();
                        MessageBox.Show("Abertura de Inventário efectuada com sucesso!");
                        lbAberto.Text = "Inventário Aberto";
                    }
                    catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
                    finally { conexao.Close(); }
                }
            }
        }
        private void gravarRecolhaInventario(DataGridView dataGrid)
        {
            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                for (int i = 0; i < dataGrid.RowCount - 1; i++)
                {
                    SqlCommand comando = new SqlCommand("UPDATE ASINVENTARIO SET QTD_FIS = ISNULL(QTD_FIS,0) + ISNULL('" + dataGrid[5, i].Value + "',0), LOCALI = '" + dataGrid[2, i].Value + "', Contado_Por = '" + Variavel.codUtilizador + "', DataCria = Convert(Varchar(20), GetDate(), 120) Where Data_AbreInv = '" + cmbRDataInventrario.Text + "' And LTRIM(RTRIM(CODLOJA)) = LTRIM(RTRIM('" + lbRLoja.Text + "')) AND LTRIM(RTRIM(CODARMZ)) = LTRIM(RTRIM('" + dataGrid[1, i].Value + "')) AND LTRIM(RTRIM(REFERENC))=LTRIM(RTRIM('" + dataGrid[0, i].Value + "')) AND DATA_FECHAINV IS NULL");
                    comando.Connection = conexao;
                    comando.Transaction = tran;
                    comando.ExecuteNonQuery();
                }
                tran.Commit();
                MessageBox.Show("Gravado com sucesso!");
                dataGrid.Rows.Clear();
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void fAcerto_QTDSistemaX()
        {
            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                SqlCommand comando = new SqlCommand("UPDATE ASINVENTARIO SET QuantidadeSistemaX = QTDMOVIMENTO FROM ASINVENTARIO INV, VW_MOVIMENTO02 F Where Data_AbreInv = '" + cmbAInventarioDia.Text + "' And INV.CODLOJA = '" + lbACodLoja.Text + "' And INV.CODLOJA = F.CODLOJA And INV.CODARMZ = F.CODARMZ And INV.REFERENC = F.REFERENC And CONTASTOCKS = 'S'");
                comando.CommandTimeout = 9000;
                comando.Connection = conexao;
                comando.Transaction = tran;
                comando.ExecuteNonQuery();

                tran.Commit();
                MessageBox.Show("UPDATE REALISADO com sucesso!");
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private string IDUnico()
        {
            string id = "";
            comand = new SqlCommand("SELECT RTRIM(ISNULL(CODLOJA,'" + lbACodLoja.Text + "')) + CAST (ISNULL( MAX( CAST( RIGHT (RTRIM(IDUNICO), LEN(RTRIM(IDUNICO)) - ( LEN(ISNULL(CODLOJA,'" + lbACodLoja.Text + "')) ) )AS INT ) ) , 1) + 1 AS VARCHAR(23)) AS IDUNICO FROM AREGDOC WITH (XLOCK) WHERE CODLOJA = '" + lbACodLoja.Text + "' AND LEFT(IDUNICO, LEN(CODLOJA) ) = '" + lbACodLoja.Text + "' GROUP BY CODLOJA", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        id = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }

            return id;
        }
        private string NG()
        {
            string ng = "";
            comand = new SqlCommand("SELECT ISNULL(MAX(CAST(GUIA AS INT)), 1) + 1 FROM ASFICMOV1 WITH (XLOCK) WHERE CODDOC = 'IF' AND CODLOJA = '" + lbACodLoja.Text + "' AND ISNUMERIC(GUIA)= 1", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ng = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }

            return ng;
        }
        private string ND()
        {
            string nd = "";
            comand = new SqlCommand("SELECT ISNULL(MAX(NUMDOC),0) + 1 FROM AREGDOC WITH (XLOCK) WHERE CODDOC = 'IF' AND CODLOJA = '" + lbACodLoja.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        nd = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            return nd;
        }
        private void fFecha_InventarioAjustado()
        {
            string sEstado_IF = "E", sTName = "T_INVENTARIO_FISICO";

            //*************** SELECT MAXIMOS ***************
            idUnico = IDUnico();
            nG = NG();
            nD = ND();

            comand = new SqlCommand("SELECT CODLOJA, CODARMZ, REFERENC, LOCALI, FOB, PCA, PVP, QuantidadeSistemaX, QuantidadeFisicaX, DATA_ABREINV, DATA_FECHAINV From ASINVENTARIO Where CODLOJA = '" + lbACodLoja.Text + "' And Data_AbreInv = '" + cmbAInventarioDia.Text + "' And QTD_FIS Is Not Null And Data_FechaInv Is Null And Referenc Not In (Select Referenc From Asinventario Where Data_AbreInv = '" + cmbAInventarioDia.Text + "' And QTD_FIS Is Not Null )", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        count = count + 1;
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            sCodLoja = new string[count]; sArmaz = new string[count]; sRefere = new string[count]; sLocali = new string[count]; nFob = new string[count]; nPca = new string[count]; nPvp = new string[count]; nQtdSist = new string[count]; nQtdFis = new string[count]; sDataInv = new string[count]; sDataFechoInv = new string[count];
            //*
            comand = new SqlCommand("SELECT CODLOJA, CODARMZ, REFERENC, LOCALI, FOB, PCA, PVP, QuantidadeSistemaX, QuantidadeFisicaX, DATA_ABREINV, DATA_FECHAINV From ASINVENTARIO Where CODLOJA = '" + lbACodLoja.Text + "' And Data_AbreInv = '" + cmbAInventarioDia.Text + "' And QTD_FIS Is Not Null And Data_FechaInv Is Null And Referenc Not In (Select Referenc From Asinventario Where Data_AbreInv = '" + cmbAInventarioDia.Text + "' And QTD_FIS Is Not Null )", conexao);
            comand.CommandTimeout = 600;
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        sCodLoja[i] = reader["CODLOJA"].ToString();
                        sArmaz[i] = reader["CODARMZ"].ToString();
                        sRefere[i] = reader["REFERENC"].ToString();
                        sLocali[i] = reader["LOCALI"].ToString();
                        nFob[i] = reader["FOB"].ToString();
                        nPca[i] = reader["PCA"].ToString();
                        nPvp[i] = reader["PVP"].ToString();
                        nQtdSist[i] = reader["QuantidadeSistemaX"].ToString();
                        nQtdFis[i] = reader["QuantidadeFisicaX"].ToString();
                        sDataInv[i] = reader["DATA_ABREINV"].ToString();
                        sDataFechoInv[i] = reader["DATA_FECHAINV"].ToString();
                        i++;
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            //*
            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                //*************** INSERT AREGDOC ***************
                SqlCommand comd = new SqlCommand("INSERT INTO AREGDOC(IDUnico, DataCria,               CodUtilizador,                     CodDoc,                          CodEntidade,                        CodLoja,             NumDoc,      NumFact, PrecoD,  Preco,           DATA_LANCAMENTO,                  STATUS) " +
                             "VALUES('" + idUnico + "', CONVERT(VARCHAR(20),GETDATE(),120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_IF + "', '" + Gerais.PARAM_CODCLI_GERAL + "', '" + lbACodLoja.Text + "', '" + nD + "', '" + nD + "', '0',    '0', CONVERT(VARCHAR(10), GETDATE(), 120), '" + sEstado_IF + "')");
                comd.CommandTimeout = 600;
                comd.Connection = conexao;
                comd.Transaction = tran;
                comd.ExecuteNonQuery();

                for (int a = 0; a < count; a++)
                {
                    if (Convert.ToInt32(nQtdFis[a]) > Convert.ToInt32(nQtdSist[a]))
                    {
                        sTipMove = "+";
                        if (Convert.ToInt32(nQtdSist[a]) < 0)
                            nQtdSist[a] = "0";
                        nQtdDif = "" + (Convert.ToInt32(nQtdFis[a]) - Convert.ToInt32(nQtdSist[a]));
                    }
                    else if (Convert.ToInt32(nQtdFis[a]) < Convert.ToInt32(nQtdSist[a]))
                    {
                        sTipMove = "-";
                        if (Convert.ToInt32(nQtdFis[a]) < 0)
                            nQtdFis[a] = "0";
                        nQtdDif = "" + (Convert.ToInt32(nQtdSist[a]) - Convert.ToInt32(nQtdFis[a]));
                    }
                    else
                    {
                        sTipMove = "=";
                        nQtdDif = "0";
                    }
                    nF_Kz = "" + (Convert.ToDouble(nFob[a]) * Convert.ToDouble(Variavel.cambio));
                    nC_Kz = "" + (Convert.ToDouble(nPca[a]) * Convert.ToDouble(Variavel.cambio));
                    nP_Kz = "" + (Convert.ToDouble(nPvp[a]) * Convert.ToDouble(Variavel.cambio));
                    //*************** INSERT ASFICMOV1 ***************
                    SqlCommand cmd1 = new SqlCommand("INSERT INTO ASFICMOV1( IDUNICO, CODLOJA, CODARMZ,       REFERENC,        NUMDOC,                  CODDOC,                   QTD,              FOB,            PCLD,           PVPD,                  DATACRIA,                         CODUTILIZADOR,                       CODENTIDADE,                  TIPMOV,           GUIA,            PCL,            PVP,            DATA_LANCAMENTO,                       DATA_INV,                       QTD_INV,        QTD_SISTEMA) " +
                             "VALUES('" + idUnico + "', '" + lbACodLoja.Text + "', '" + sArmaz + "', '" + sRefere + "', '" + nD + "', '" + Gerais.PARAM_CODDOC_IF + "', '" + nQtdDif + "', '" + nFob + "', '" + nPca + "', '" + nPvp + "', CONVERT(VARCHAR(20),GETDATE(),120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODCLI_GERAL + "', '" + sTipMove + "', '" + nG + "', '" + nC_Kz + "', '" + nP_Kz + "', CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(20),GETDATE(),120), '" + nQtdFis + "', '" + nQtdSist + "')");
                    cmd1.CommandTimeout = 600;
                    cmd1.Connection = conexao;
                    cmd1.Transaction = tran;
                    cmd1.ExecuteNonQuery();
                    //*************** UPDATE ASMESTRE ***************
                    SqlCommand comando = new SqlCommand("UPDATE ASMESTRE SET QTD = '" + nQtdFis + "', DATA_ULT_INV = CONVERT(VARCHAR(20),GETDATE(),120), LOCALI = '" + sLocali + "', CODUTIL_INV = '" + Variavel.codUtilizador + "' WHERE CODLOJA = '" + lbACodLoja.Text + "' AND CODARMZ = '" + sArmaz + "' AND REFERENC= '" + sRefere + "'");
                    comando.CommandTimeout = 600;
                    comando.Connection = conexao;
                    comando.Transaction = tran;
                    comando.ExecuteNonQuery();
                }
                tran.Commit();
                MessageBox.Show("Insert AREGDOC e ASFICMOV1 e UPDATE ASMESTRE realisados com sucesso!");
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); } //*/
            //*
            conexao.Open();
            SqlTransaction transc = conexao.BeginTransaction();
            try
            {
                SqlCommand cmd1 = new SqlCommand("INSERT INTO ASFICMOV1(IDUNICO, CODLOJA, CODARMZ, REFERENC, NUMDOC, CODDOC,QTD, FOB, PCLD, PVPD, DATACRIA, CODUTILIZADOR,CODENTIDADE, TIPMOV, GUIA, PCL, PVP, DATA_LANCAMENTO, DATA_INV, QTD_INV,QTD_SISTEMA) " +
                             "Select '" + idUnico + "', CODLOJA, CODARMZ, REFERENC, '" + nD + "', '" + Gerais.PARAM_CODDOC_IF + "', QTD_FIS, '" + nFob + "', '" + nPca + "', '" + nPvp + "', CONVERT(VARCHAR(20),GETDATE(),120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODCLI_GERAL + "', '+', '" + nG + "', '" + nC_Kz + "', '" + nP_Kz + "', CONVERT(VARCHAR(20),GETDATE(),120), CONVERT(VARCHAR(20),GETDATE(),120), QTD_FIS,QuantidadeSistemaX From ASINVENTARIO Where CODLOJA = '" + lbACodLoja.Text + "' AND DATA_ABREINV = '" + cmbAInventarioDia.Text + "' And QTD_FIS Is Not Null And Data_FechaInv Is Null And Referenc In (Select Referenc From Asinventario Where Data_AbreInv = '" + cmbAInventarioDia.Text + "' And QTD_FIS Is Not Null)");
                cmd1.CommandTimeout = 600;
                cmd1.Connection = conexao;
                cmd1.Transaction = transc;
                cmd1.ExecuteNonQuery();

                SqlCommand comando = new SqlCommand("UPDATE ASINVENTARIO SET DATA_FECHAINV = CONVERT(VARCHAR(10),GETDATE(),120) WHERE CODLOJA = '" + lbACodLoja.Text + "' AND DATA_ABREINV = '" + cmbAInventarioDia.Text + "'");
                comando.CommandTimeout = 600;
                comando.Connection = conexao;
                comando.Transaction = transc;
                comando.ExecuteNonQuery();

                transc.Commit();
                MessageBox.Show("Gravado com sucesso!");
            }
            catch (SqlException ex) { transc.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }

            lbAberto.Text = "Inventário Fechado";

            comand = new SqlCommand("Select Distinct NomeUtilizador From AREGDOC R With (NoLock), UTILIZADORES U With (NoLock) Where IDunico = '" + idUnico + "' And U.CODUTILIZADOR=R.CODUTILIZADOR", conexao);
            comand.CommandTimeout = 600;
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        txtAFechadoPor.Text = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); } //*/
        }
        private void fFecha_InventarioFisico()
        {
            string sEstado_IF = "E";
            string sTName = "T_INVENTARIO_FISICO";
            idUnico = IDUnico();
            nG = NG();
            nD = ND();

            comand = new SqlCommand("SELECT CODLOJA, CODARMZ, REFERENC, LOCALI, FOB, PCA, PVP, QuantidadeSistemaX, QuantidadeFisicaX, DATA_ABREINV, DATA_FECHAINV From ASINVENTARIO Where CODLOJA = '" + lbACodLoja.Text + "' And Data_AbreInv = '" + cmbAInventarioDia.Text + "' And QTD_FIS Is Not Null And Data_FechaInv Is Null And Referenc Not In (Select Referenc From Asinventario Where Data_AbreInv = '" + cmbAInventarioDia.Text + "' And QTD_FIS Is Not Null )", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        count =  count + 1;
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            
            sCodLoja = new string[count]; sArmaz = new string[count]; sRefere = new string[count]; sLocali = new string[count]; nFob = new string[count]; nPca = new string[count]; nPvp = new string[count]; nQtdSist = new string[count]; nQtdFis = new string[count]; sDataInv = new string[count]; sDataFechoInv = new string[count];
            //* 
            comand = new SqlCommand("SELECT CODLOJA, CODARMZ, REFERENC, LOCALI, FOB, PCA, PVP, QuantidadeSistemaX, QuantidadeFisicaX + IsNull(QuantidadeEM,0), DATA_ABREINV, DATA_FECHAINV From ASINVENTARIO Where CODLOJA = '" + lbACodLoja.Text + "' And Data_AbreInv = '" + cmbAInventarioDia.Text + "' And QTD_FIS Is Not Null And Data_FechaInv Is Null And Referenc Not In (Select Referenc From Asinventario Where Data_AbreInv = '" + cmbAInventarioDia.Text + "' And QTD_FIS Is Not Null )", conexao);
            comand.CommandTimeout = 600;
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        sCodLoja[i] = reader["CODLOJA"].ToString();
                        sArmaz[i] = reader["CODARMZ"].ToString();
                        sRefere[i] = reader["REFERENC"].ToString();
                        sLocali[i] = reader["LOCALI"].ToString();
                        nFob[i] = reader["FOB"].ToString();
                        nPca[i] = reader["PCA"].ToString();
                        nPvp[i] = reader["PVP"].ToString();
                        nQtdSist[i] = reader["QuantidadeSistemaX"].ToString();
                        nQtdFis[i] = reader["QuantidadeFisicaX"].ToString();
                        sDataInv[i] = reader["DATA_ABREINV"].ToString();
                        sDataFechoInv[i] = reader["DATA_FECHAINV"].ToString();
                        i++;
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }//*/
            //*
            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                SqlCommand comd = new SqlCommand("INSERT INTO AREGDOC (IDUnico, DataCria,              CodUtilizador,                       CodDoc,                            CodEntidade,                    CodLoja,            NumDoc,       NumFact,  PrecoD, Preco,        DATA_LANCAMENTO,                    STATUS) " +
                             "VALUES('" + idUnico + "', CONVERT(VARCHAR(20),GETDATE(),120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_IF + "', '" + Gerais.PARAM_CODCLI_GERAL + "', '" + lbACodLoja.Text + "', '" + nD + "', '" + nD + "', '0',   '0', CONVERT(VARCHAR(10), GETDATE(), 120), '" + sEstado_IF + "')");
                comd.CommandTimeout = 600;
                comd.Connection = conexao;
                comd.Transaction = tran;
                comd.ExecuteNonQuery();
                
                for (int a = 0; a < count; a++)
                {
                    if(Convert.ToInt32(nQtdFis[a]) > Convert.ToInt32(nQtdSist[a]))
                    {
                        sTipMove = "+";
                        if(Convert.ToInt32(nQtdSist[a]) < 0)
                            nQtdSist[a] = "0";
                        nQtdDif = "" + (Convert.ToInt32(nQtdFis[a]) - Convert.ToInt32(nQtdSist[a]));
                    }
                    else if(Convert.ToInt32(nQtdFis[a]) < Convert.ToInt32(nQtdSist[a]))
                    {
                        sTipMove = "-";
                        if(Convert.ToInt32(nQtdFis[a]) < 0)
                            nQtdFis[a] = "0";
                        nQtdDif = "" + (Convert.ToInt32(nQtdSist[a]) - Convert.ToInt32(nQtdFis[a]));
                    }
                    else
                    {
                        sTipMove = "=";
                        nQtdDif = "0";
                    }
                    nF_Kz = "" + (Convert.ToDouble(nFob[a]) * Convert.ToDouble(Variavel.cambio));
                    nC_Kz = "" + (Convert.ToDouble(nPca[a]) * Convert.ToDouble(Variavel.cambio));
                    nP_Kz = "" + (Convert.ToDouble(nPvp[a]) * Convert.ToDouble(Variavel.cambio));

                    //*************** INSERT ASFICMOV1 ***************
                    SqlCommand cmd1 = new SqlCommand("INSERT INTO ASFICMOV1 (IDUNICO, CODLOJA, CODARMZ,   REFERENC,         NUMDOC,                 CODDOC,                    QTD,              FOB,           PCLD,           PVPD,                  DATACRIA,                          CODUTILIZADOR,                       CODENTIDADE,                  TIPMOV,           GUIA,           PCL,             PVP,                DATA_LANCAMENTO,                     DATA_INV,                       QTD_INV,          QTD_SISTEMA) " +
                             "VALUES('" + idUnico + "', '" + lbACodLoja.Text + "', '" + sArmaz + "', '" + sRefere + "', '" + nD + "', '" + Gerais.PARAM_CODDOC_IF + "', '" + nQtdDif + "', '" + nFob + "', '" + nPca + "', '" + nPvp + "', CONVERT(VARCHAR(20),GETDATE(),120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODCLI_GERAL + "', '" + sTipMove + "', '" + nG + "', '" + nC_Kz + "', '" + nP_Kz + "', CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(20),GETDATE(),120), '" + nQtdFis + "', '" + nQtdSist + "')");
                    cmd1.CommandTimeout = 600;
                    cmd1.Connection = conexao;
                    cmd1.Transaction = tran;
                    cmd1.ExecuteNonQuery();
                    
                    //*************** UPDATE ASMESTRE ***************
                    SqlCommand comando = new SqlCommand("UPDATE ASMESTRE SET QTD = '" + nQtdFis + "', DATA_ULT_INV = CONVERT(VARCHAR(20),GETDATE(),120), LOCALI = '" + sLocali + "', CODUTIL_INV = '" + Variavel.codUtilizador + "' WHERE CODLOJA = '" + lbACodLoja.Text + "' AND CODARMZ = '" + sArmaz + "' AND REFERENC= '" + sRefere + "'");
                    comando.CommandTimeout = 600;
                    comando.Connection = conexao;
                    comando.Transaction = tran;
                    comando.ExecuteNonQuery();
                    
                    if (sTipMove == "+")
                    {
                        SqlCommand comando1 = new SqlCommand("UPDATE ASINVENTARIO SET QuantidadeEntrada = '" + nQtdDif + "' Where Data_AbreInv = '" + cmbRDataInventrario.Text + "' And CODLOJA = '" + lbACodLoja.Text + "' And CODARMZ = '" + sArmaz + "' And REFERENC = '" + sRefere + "' And Data_FechaInv Is Null");
                        comando1.CommandTimeout = 600;
                        comando1.Connection = conexao;
                        comando1.Transaction = tran;
                        comando1.ExecuteNonQuery();
                    }
                    else if (sTipMove == "-")
                    {
                        SqlCommand comando2 = new SqlCommand("UPDATE ASINVENTARIO SET QuantidadeSaida = '" + nQtdDif + "' Where Data_AbreInv = '" + cmbRDataInventrario.Text + "' And CODLOJA = '" + lbACodLoja.Text + "' And CODARMZ = '" + sArmaz + "' And REFERENC = '" + sRefere + "' And Data_FechaInv Is Null");
                        comando2.CommandTimeout = 600;
                        comando2.Connection = conexao;
                        comando2.Transaction = tran;
                        comando2.ExecuteNonQuery();
                    }
                }

                tran.Commit();
                MessageBox.Show("Insert AREGDOC e ASFICMOV1 e UPDATE ASMESTRE realisados com sucesso!");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); } //*/
            //*
            conexao.Open();
            SqlTransaction transc = conexao.BeginTransaction();
            try
            {
                SqlCommand comando = new SqlCommand("UPDATE ASINVENTARIO SET DATA_FECHAINV = CONVERT(VARCHAR(10),GETDATE(),120) WHERE CODLOJA = '" + lbACodLoja.Text + "' AND DATA_ABREINV = '" + cmbAInventarioDia.Text + "'");
                comando.CommandTimeout = 600;
                comando.Connection = conexao;
                comando.Transaction = transc;
                comando.ExecuteNonQuery();
                
                SqlCommand cmad = new SqlCommand("SP_INVENTARIO_RECOLHA_FM", conexao);
                cmad.CommandTimeout = 600;
                cmad.CommandType = CommandType.StoredProcedure;

                cmad.Parameters.Add(new SqlParameter("@p_sCodigoLoja", lbACodLoja.Text));
                cmad.Parameters.Add(new SqlParameter("@p_sData_AbreInv", cmbAInventarioDia.Text));
                cmad.Parameters.Add(new SqlParameter("@p_sCodUtilizador", Variavel.codUtilizador));

                cmad.Transaction = transc;
                cmad.ExecuteNonQuery();

                transc.Commit();
                MessageBox.Show("Gravado com sucesso!");
            }
            catch (SqlException ex) { transc.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }

            lbAberto.Text = "Inventário Fechado";

            comand = new SqlCommand("Select Distinct NomeUtilizador From AREGDOC R With (NoLock), UTILIZADORES U With (NoLock) Where IDunico = '" + idUnico + "' And U.CODUTILIZADOR=R.CODUTILIZADOR", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        txtAFechadoPor.Text = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); } //*/
        }
        private void atualizarZerar(DataGridView dataGrid)
        {
            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                for (int i = 0; i < dataGrid.RowCount - 1; i++)
                {
                    SqlCommand comando = new SqlCommand("UPDATE ASINVENTARIO Set DataCria = Convert(Varchar(20), GetDate(),120), QuantidadeFisicaX = 0, QTD_FIS = 0, Zerado = 'S', Contado_Por = '" + Variavel.codUtilizador + "' Where Data_AbreInv = '" + cmbZData.Text + "' And QTD_FIS Is Null And CONTASTOCKS = 'S' And CODLOJA = '" + lbZLoja.Text + "' And CODARMZ = '" + dataGrid[1, i].Value + "' And REFERENC Not In (Select REFERENC From ASFICMOV1 Where CODLOJA = '" + lbZLoja.Text + "' And CODARMZ = '" + dataGrid[1, i].Value + "' And QTD>0 And Left(DataCria,10)>= '" + cmbZData.Text + "' And CodDoc Not In ('IF', 'RF') And TIPMOV IN ('+', '-'))");
                    comando.Connection = conexao;
                    comando.Transaction = tran;
                    comando.ExecuteNonQuery();
                }

                tran.Commit();
                MessageBox.Show("Operação Realizada com Sucesso!");

                dataGrid.DataSource = null;
                dataGrid.Rows.Clear();
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void atualizarAcerto(DataGridView dataGrid)
        {
            string sAcerto = "S", sZerado = "S", sql = "", codLoja = "";
            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                for (int i = 0; i < dataGrid.RowCount - 1; i++)
                {
                    if (dataGrid[3, i].Value.ToString() == "-")
                        sql = "UPDATE ASINVENTARIO SET QuantidadeFisicaX = ISNULL(QuantidadeFisicaX,0) - '" + dataGrid[2, i].Value + "', Acerto = '" + sAcerto + "' Where DataCria < '" + dataGrid[4, i].Value + "' And LTRIM(RTRIM(CODLOJA))=LTRIM(RTRIM('" + dataGrid[5, i].Value + "')) AND LTRIM(RTRIM(CODARMZ))=LTRIM(RTRIM('" + dataGrid[6, i].Value + "')) And Zerado Is Null And LTRIM(RTRIM(REFERENC))=LTRIM(RTRIM('" + dataGrid[0, i].Value + "')) And Data_AbreInv = '" + cmbAcDataInventario.Text + "'";
                    else if (dataGrid[3, i].Value.ToString() == "+")
                    {
                        if (dataGrid[7, i].Value.ToString() == Gerais.PARAM_CODDOC_EM)
                            sql = "UPDATE ASINVENTARIO SET QuantidadeEM = IsNull(QuantidadeEM,0) + '" + dataGrid[2, i].Value + "', Acerto = '" + sAcerto + "' Where DataCria < '" + dataGrid[4, i].Value + "' And LTRIM(RTRIM(CODLOJA)) = LTRIM(RTRIM('" + dataGrid[5, i].Value + "')) AND LTRIM(RTRIM(CODARMZ))= LTRIM(RTRIM('" + dataGrid[6, i].Value + "')) And Zerado Is Null And LTRIM(RTRIM(REFERENC)) = LTRIM(RTRIM('" + dataGrid[0, i].Value + "')) And Data_AbreInv = '" + cmbAcDataInventario.Text + "'";
                        else
                            sql = "UPDATE ASINVENTARIO SET QuantidadeFisicaX = ISNULL(QuantidadeFisicaX,0) + '" + dataGrid[2, i].Value + "', Acerto = '" + sAcerto + "' Where DataCria < '" + dataGrid[4, i].Value + "' And LTRIM(RTRIM(CODLOJA)) = LTRIM(RTRIM('" + dataGrid[5, i].Value + "')) And LTRIM(RTRIM(CODARMZ))= LTRIM(RTRIM('" + dataGrid[6, i].Value + "')) And Zerado Is Null And LTRIM(RTRIM(REFERENC)) = LTRIM(RTRIM('" + dataGrid[0, i].Value + "')) And Data_AbreInv = '" + cmbAcDataInventario.Text + "'";
                    }

                    SqlCommand comando = new SqlCommand(sql);
                    comando.Connection = conexao;
                    comando.Transaction = tran;
                    comando.ExecuteNonQuery();
                }

                SqlCommand comando1 = new SqlCommand("UPDATE ASINVENTARIO SET QuantidadeSistemaX = QTDMOVIMENTO FROM ASINVENTARIO INV, VW_MOVIMENTO02 F Where Data_AbreInv = '" + cmbAcDataInventario.Text + "' And INV.CODLOJA = '" + lbACLoja.Text + "' And INV.CODLOJA = F.CODLOJA And INV.CODARMZ = F.CODARMZ And INV.REFERENC = F.REFERENC And CONTASTOCKS = 'S'");
                comando1.Connection = conexao;
                comando1.Transaction = tran;
                comando1.ExecuteNonQuery();

                SqlCommand cmd = new SqlCommand("INSERT Into AUDIT_ASINVENTARIO_ACERTO (Data_AbreInv, CodLoja, DataInicial, DataFinal, CodUtilizador, DataCria)" +
                             "Values ('" + cmbAcDataInventario.Text + "', '" + lbACLoja.Text + "', '" + txtDataInicial.Text + "', '" + txtDataFinal.Text + "', '" + Variavel.codUtilizador + "', Convert(Varchar(20),GetDate(),120))");

                cmd.Connection = conexao;
                cmd.Transaction = tran;
                cmd.ExecuteNonQuery();

                tran.Commit();
                MessageBox.Show("Operação Realizada com Sucesso!");

                dataGrid.DataSource = null;
                dataGrid.Rows.Clear();
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }

            SqlCommand cmd2 = new SqlCommand("Select CODLOJA, CODARMZ, REFERENC From ASINVENTARIO With (NoLock) Where QuantidadeFisicaX+IsNull(QuantidadeEM,0)<0 And Data_AbreInv = '" + cmbAcDataInventario.Text + "' And CODLOJA = '" + lbACLoja.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codLoja = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            if (codLoja != "" && codLoja != null)
                MessageBox.Show("A T E N Ç Ã O ! ! ! NÃO PODE FAZER O FECHO DO INVENTÁRIO, SEM REGULARIZAR OS ARTIGOS QUE ESTÃO COM QUANTIDADES FÍSICA NEGATIVAS");

            limparAcerto();
        }
        private void prenxerGriddgFicha(DataGridView dataGrid, string sql)
        {
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dataGrid.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void preenxerComboBox(ComboBox combo, string sql)
        {
            SqlCommand cmd = new SqlCommand(sql);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    combo.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void preencheGridZerar(DataGridView dataGrid)
        {
            string sql = "Select Distinct LTrim(RTrim(A.NomeARZ)), LTrim(RTrim(I.CodArmz)) " +
                "From ASARMAZ A With (NoLock), ASINVENTARIO I With (NoLock) Where Data_AbreInv = '" + cmbZData.Text + "' And I.CodLoja = '" + lbZLoja.Text + "' And " +
                "I.CODLOJA=A.CODLOJA And I.CodArmz=A.CodArmz And Data_FechaInv Is Null And QTD_FIS IS NULL And ContaStocks = 'S' Order BY LTrim(RTrim(A.NomeARZ))";
            prenxerGriddgFicha(dataGrid, sql);
        }
        private void preencheGridAcerto(DataGridView dataGrid)
        {
            string sql = "Select LTrim(RTrim(A.NomeArz)), LTrim(RTrim(F.CodLoja)), LTrim(RTrim(F.CodArmz)), LTrim(RTrim(F.Referenc)), Cast(F.DataCria As DateTime), F.QTD, LTrim(RTrim(F.TipMov)), LTrim(RTrim(F.CodDoc)) As CodDoc " +
                "From ASFICMOV1 F, ASINVENTARIO I, ASARMAZ A, AREGDOC R Where Data_AbreInv = '" + cmbAcDataInventario.Text + "' And QTD_Fis Is Not Null And (F.TipMov='+' Or F.TipMov='-') And F.coddoc <> '" + Gerais.PARAM_CODDOC_IF + "' And Convert(Varchar(10), Cast(F.DataCria As DateTime),120) >= '" + txtDataInicial.Text + "' And " +
                "Convert(Varchar(10), Cast(F.DataCria As DateTime),120) <= '" + txtDataFinal.Text + "' And F.CodLoja = '" + lbACLoja.Text + "' And F.CodLoja=A.CodLoja And F.CodArmz=A.CodArmz And F.CodLoja=I.CodLoja And F.CodArmz=I.CodArmz And F.Referenc=I.Referenc And F.IDunico=R.IDunico And R.STATUS <> 'A'";
            prenxerGriddgFicha(dataGrid, sql);
        }
        private void preenxerComboLojaAbertura()
        {
            string sql = @"SELECT DISTINCT NOMELOJA FROM ASLOJA";
            if(cmbAInventarioDia.Text != "")
                sql = @"SELECT DISTINCT L.NOMELOJA FROM ASLOJA L, ASINVENTARIO R WHERE R.CODLOJA=L.CODLOJA AND R.DATA_ABREINV = '" + cmbAInventarioDia.Text + "' AND R.DATA_FECHAINV IS NULL";
            preenxerComboBox(cmbAInvertarioLoja, sql);
        }
        private void preenxerComboLojaRecolha()
        {
            string sql = @"SELECT DISTINCT NOMELOJA FROM ASLOJA";
            if (cmbRDataInventrario.Text != "")
                sql = @"SELECT DISTINCT L.NOMELOJA FROM ASLOJA L, ASINVENTARIO R WHERE R.CODLOJA=L.CODLOJA AND R.DATA_ABREINV='" + cmbRDataInventrario.Text + "' AND R.DATA_FECHAINV IS NULL";
            preenxerComboBox(cmbRLoja, sql);
        }
        private void preenxerComboLojaZerar()
        {
            string sql = @"Select Distinct L.NomeLoJa From ASLOJA L, ASINVENTARIO R Where R.CodLOJA=L.CodLOJA AND R.Data_AbreInv = '" + cmbZData.Text + "' And R.Data_FechaInv Is Null Order By L.NomeLoJa";
            preenxerComboBox(cmbZLoja, sql);
        }
        private void preenxerComboLojaAcerto()
        {
            string sql = @"Select Distinct L.NomeLoJa From ASLOJA L, ASINVENTARIO R Where R.CodLOJA=L.CodLOJA AND R.Data_AbreInv = '" + cmbAcDataInventario.Text + "' And R.Data_FechaInv Is Null Order By L.NomeLoJa";
            preenxerComboBox(cmbAcSaelecaoLoja, sql);
        }
        private void preenxerComboDataAbertura()
        {
            string sql = @"SELECT DISTINCT DATA_ABREINV FROM ASINVENTARIO WHERE DATA_FECHAINV IS NULL And CodLoja = '"+ lbACodLoja.Text +"' ORDER BY DATA_ABREINV DESC";
            preenxerComboBox(cmbAInventarioDia, sql);
        }
        private void preenxerComboDataRecolha()
        {
            string sql = @"SELECT DISTINCT DATA_ABREINV FROM ASINVENTARIO WHERE DATA_FECHAINV IS NULL ORDER BY DATA_ABREINV DESC";
            preenxerComboBox(cmbRDataInventrario, sql);
        }
        private void preenxerComboDataZerar()
        {
            string sql = @"Select Distinct Data_AbreInv From ASINVENTARIO With (NoLock) Where Data_FechaInv Is Null Order By Data_AbreInv Desc";
            preenxerComboBox(cmbZData, sql);
        }
        private void preenxerComboDataAcerto()
        {
            string sql = @"Select Distinct Data_AbreInv From ASINVENTARIO With (NoLock) Where Data_FechaInv Is Null Order By Data_AbreInv Desc";
            preenxerComboBox(cmbAcDataInventario, sql);
        }
        private void preenxerComboArmazemRecolha()
        {
            string sql = @"SELECT DISTINCT L.NOMEARZ FROM ASARMAZ L, ASINVENTARIO R WHERE R.CODLOJA=L.CODLOJA AND R.CODARMZ=L.CODARMZ AND R.CODLOJA = '"+ lbRLoja.Text +"' AND R.DATA_ABREINV= '" + cmbRDataInventrario.Text + "' AND R.DATA_FECHAINV IS NULL";
            preenxerComboBox(cmbRMercadoria, sql);
        }
        private void preenxerComboReferenciaRecolha()
        {
            string sql = "";
            if(cbRAberturaLoja.Checked)
                sql = @"SELECT DISTINCT REFERENC FROM ASINVENTARIO WHERE DATA_FECHAINV IS NULL AND DATA_ABREINV = '" + cmbRDataInventrario.Text + "' AND CODLOJA = '" + lbRLoja.Text + "' AND CODARMZ= '" + lbRArmazem.Text + "'";
            else
                sql = @"SELECT DISTINCT REFERENC FROM ASINVENTARIO WHERE DATA_FECHAINV IS NULL AND DATA_ABREINV = '" + cmbRDataInventrario.Text + "' AND CODLOJA = '" + lbRLoja.Text + "'";
            preenxerComboBox(cmbRReferencia, sql);
        }
        private void limparAcerto()
        {
            txtDataInicial.Text = txtDataFinal.Text = DateTime.Now.ToShortDateString();
            cmbAcDataInventario.Text = "";
            cmbAcDataInventario.PromptText = "";
            cmbAcSaelecaoLoja.Text = "";
            cmbAcSaelecaoLoja.PromptText = "";
            metroGrid4.DataSource = null;
            metroGrid4.Rows.Clear();
        }
        private void FormInventarioFisico_Load(object sender, EventArgs e)
        {
             metroTabControl1.SelectedTab = metroTabPage1;
             txtDataInicial.Text = txtDataFinal.Text = DateTime.Now.ToShortDateString();
        }

        private void txtDataInicial_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception ex)
            {
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!");
                txtDataInicial.Text = "";
            }
        }

        private void txtDataFinal_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception ex)
            {
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!");
                txtDataFinal.Text = "";
            }
        }

        private void metroTabPage5_Leave(object sender, EventArgs e)
        {
            limparAcerto();
        }

        private void cmbAInvertarioLoja_DropDown(object sender, EventArgs e)
        {
            cmbAInvertarioLoja.Items.Clear();
            preenxerComboLojaAbertura();
        }

        private void cmbAInvertarioLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("SELECT CODLOJA FROM ASLOJA WHERE NOMELOJA = '" + cmbAInvertarioLoja.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        lbACodLoja.Text = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally
            {
                conexao.Close();
            }
        }

        private void cmbAInventarioDia_DropDown(object sender, EventArgs e)
        {
            cmbAInventarioDia.Items.Clear();
            preenxerComboDataAbertura();
        }

        private void cmbAInventarioDia_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("Select Distinct NomeUtilizador From ASINVENTARIO I With (NoLock), UTILIZADORES U With (NoLock) Where Data_AbreInv = '" + cmbAInventarioDia.Text + "' And I.CodLoja= '"+ lbACodLoja.Text +"' And U.CODUTILIZADOR=I.CODUTILIZADOR ", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        txtAAbertoPor.Text = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally
            {
                conexao.Close();
            }
        }

        private void btAGravar_Click(object sender, EventArgs e)
        {
            gravarAbertura();
        }

        private void cmbRDataInventrario_DropDown(object sender, EventArgs e)
        {
            cmbRDataInventrario.Items.Clear();
            preenxerComboDataRecolha();
        }

        private void cmbRLoja_DropDown(object sender, EventArgs e)
        {
            cmbRLoja.Items.Clear();
            preenxerComboLojaRecolha();
        }

        private void cmbRLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("SELECT CODLOJA FROM ASLOJA WHERE NOMELOJA = '" + cmbRLoja.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        lbRLoja.Text = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally
            {
                conexao.Close();
            }
        }

        private void cmbRMercadoria_DropDown(object sender, EventArgs e)
        {
            cmbRMercadoria.Items.Clear();
            preenxerComboArmazemRecolha();
        }

        private void cmbRMercadoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("SELECT CODARMZ FROM ASARMAZ WHERE NOMEARZ = '" + cmbRMercadoria.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        lbRArmazem.Text = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally
            {
                conexao.Close();
            }
        }

        private void cmbRReferencia_DropDown(object sender, EventArgs e)
        {
            cmbRReferencia.Items.Clear();
            preenxerComboReferenciaRecolha();
        }

        private void cmbRReferencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sArmazem = "", nQTD_S = "", nQTD_F = "", sArmaz = "", sLocaliza = "";
            SqlCommand cmd2 = new SqlCommand("Select DESCRARTIGO From ASMESTRE Where CODLOJA = '" + lbRLoja.Text + "' And REFERENC = '" + cmbRReferencia.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        lbRDescricao.Text = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally{conexao.Close();}
            
            SqlCommand comd = new SqlCommand("SELECT DISTINCT A.NOMEARZ FROM ASINVENTARIO M, ASARMAZ A WHERE M.CODARMZ=A.CODARMZ AND M.CODLOJA=A.CODLOJA AND M.CODLOJA = '" + lbRLoja.Text + "' AND REFERENC = '" + cmbRReferencia.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        sArmazem = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            //SELECT M.QTD_SIST, M.QTD_FIS, M.CODARMZ, LTRIM(RTRIM(M.LOCALI)) From ASINVENTARIO M, ASARMAZ A Where Data_AbreInv = '2015-03-24' And Data_FechaInv Is Null AND A.NOMEARZ = 'ANDRE J  GINGA' AND M.CODLOJA = 'INF' AND M.REFERENC = '014633154269'
            SqlCommand comand = new SqlCommand("SELECT M.QTD_SIST, M.QTD_FIS, M.CODARMZ, LTRIM(RTRIM(M.LOCALI)) From ASINVENTARIO M, ASARMAZ A Where Data_AbreInv = '" + cmbRDataInventrario.Text + "' And Data_FechaInv Is Null AND A.NOMEARZ = '" + sArmazem + "' AND M.CODLOJA = '" + lbRLoja.Text + "' AND M.REFERENC = '" + cmbRReferencia.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        nQTD_S = reader[0].ToString(); 
                        nQTD_F = reader[1].ToString(); 
                        sArmaz = reader[2].ToString();
                        sLocaliza = reader[3].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }

            metroGrid1.Rows.Add(cmbRReferencia.Text, sArmazem, sLocaliza, nQTD_S, nQTD_F);
        }

        private void btRGravar_Click(object sender, EventArgs e)
        {
            gravarRecolhaInventario(metroGrid1);
        }

        private void cmbZLoja_DropDown(object sender, EventArgs e)
        {
            cmbZLoja.Items.Clear();
            preenxerComboLojaZerar();
        }

        private void cmbZData_DropDown(object sender, EventArgs e)
        {
            cmbZData.Items.Clear();
            preenxerComboDataZerar();
        }

        private void cmbZLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("SELECT CODLOJA FROM ASLOJA WITH (NOLOCK) WHERE NomeLoja = '" + cmbZLoja.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        lbZLoja.Text = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally
            {
                conexao.Close();
            }
            preencheGridZerar(metroGrid3);
        }

        private void btZGravar_Click(object sender, EventArgs e)
        {
            const string message = "TEM CERTEZA QUE DESEJA EFECTUAR ESTA OPERAÇÃO?";
            const string caption = "CONFIRMAÇÃO";

            var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                atualizarZerar(metroGrid3);
            }
        }

        private void cmbAcDataInventario_DropDown(object sender, EventArgs e)
        {
            cmbAcDataInventario.Items.Clear();
            preenxerComboDataAcerto();
        }

        private void cmbAcSaelecaoLoja_DropDown(object sender, EventArgs e)
        {
            cmbAcSaelecaoLoja.Items.Clear();
            preenxerComboLojaAcerto();
        }

        private void cmbAcSaelecaoLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("SELECT CODLOJA FROM ASLOJA WITH (NOLOCK) Where NomeLoja = '" + cmbAcSaelecaoLoja.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        lbACLoja.Text = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally
            {
                conexao.Close();
            }
            preencheGridAcerto(metroGrid4);
        }

        private void btAcGravar_Click(object sender, EventArgs e)
        {
            atualizarAcerto(metroGrid4);
        }
    }
}
