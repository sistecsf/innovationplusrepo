﻿namespace SISTEC_PLUS.POS
{
    partial class FormSelectAreaOrganica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxAreasOrganicas = new System.Windows.Forms.ComboBox();
            this.buttonConfirmar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBoxAreasOrganicas
            // 
            this.comboBoxAreasOrganicas.FormattingEnabled = true;
            this.comboBoxAreasOrganicas.Location = new System.Drawing.Point(42, 136);
            this.comboBoxAreasOrganicas.Name = "comboBoxAreasOrganicas";
            this.comboBoxAreasOrganicas.Size = new System.Drawing.Size(204, 21);
            this.comboBoxAreasOrganicas.TabIndex = 0;
            this.comboBoxAreasOrganicas.DropDown += new System.EventHandler(this.comboBox1_DropDown);
            // 
            // buttonConfirmar
            // 
            this.buttonConfirmar.Location = new System.Drawing.Point(87, 185);
            this.buttonConfirmar.Name = "buttonConfirmar";
            this.buttonConfirmar.Size = new System.Drawing.Size(105, 23);
            this.buttonConfirmar.TabIndex = 1;
            this.buttonConfirmar.Text = "Confirmar";
            this.buttonConfirmar.UseVisualStyleBackColor = true;
            this.buttonConfirmar.Click += new System.EventHandler(this.buttonConfirmar_Click);
            // 
            // FormSelectAreaOrganica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 300);
            this.Controls.Add(this.buttonConfirmar);
            this.Controls.Add(this.comboBoxAreasOrganicas);
            this.Name = "FormSelectAreaOrganica";
            this.Text = "Areas Organicas";
            this.Load += new System.EventHandler(this.FormSelectAreaOrganica_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxAreasOrganicas;
        private System.Windows.Forms.Button buttonConfirmar;
    }
}