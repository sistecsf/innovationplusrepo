﻿namespace SISTEC_PLUS.POS
{
    partial class FormGestaoDescontos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGestaoDescontos));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btOk = new System.Windows.Forms.ToolStripButton();
            this.btImprimir = new System.Windows.Forms.ToolStripButton();
            this.btSair = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.rbPorMercadoria = new MetroFramework.Controls.MetroRadioButton();
            this.rbPorFamilia = new MetroFramework.Controls.MetroRadioButton();
            this.rbPorSubFamilia = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.cmbArmazem = new MetroFramework.Controls.MetroComboBox();
            this.txtDescontoAplicar = new MetroFramework.Controls.MetroTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btOk,
            this.btImprimir,
            this.btSair});
            this.toolStrip1.Location = new System.Drawing.Point(164, 60);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(164, 25);
            this.toolStrip1.TabIndex = 89;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btOk
            // 
            this.btOk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btOk.Image = ((System.Drawing.Image)(resources.GetObject("btOk.Image")));
            this.btOk.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(42, 22);
            this.btOk.Text = "Ok";
            // 
            // btImprimir
            // 
            this.btImprimir.Image = ((System.Drawing.Image)(resources.GetObject("btImprimir.Image")));
            this.btImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(73, 22);
            this.btImprimir.Text = "Imprimir";
            // 
            // btSair
            // 
            this.btSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btSair.Image = ((System.Drawing.Image)(resources.GetObject("btSair.Image")));
            this.btSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(46, 22);
            this.btSair.Text = "Sair";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(196, 2);
            this.toolStripContainer1.Location = new System.Drawing.Point(164, 60);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(196, 27);
            this.toolStripContainer1.TabIndex = 90;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer1.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer1.TopToolStripPanel.Tag = "XC";
            // 
            // rbPorMercadoria
            // 
            this.rbPorMercadoria.AutoSize = true;
            this.rbPorMercadoria.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbPorMercadoria.Location = new System.Drawing.Point(6, 31);
            this.rbPorMercadoria.Name = "rbPorMercadoria";
            this.rbPorMercadoria.Size = new System.Drawing.Size(104, 15);
            this.rbPorMercadoria.TabIndex = 92;
            this.rbPorMercadoria.Text = "Por Mercadoria";
            this.rbPorMercadoria.UseSelectable = true;
            // 
            // rbPorFamilia
            // 
            this.rbPorFamilia.AutoSize = true;
            this.rbPorFamilia.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbPorFamilia.Location = new System.Drawing.Point(196, 31);
            this.rbPorFamilia.Name = "rbPorFamilia";
            this.rbPorFamilia.Size = new System.Drawing.Size(82, 15);
            this.rbPorFamilia.TabIndex = 93;
            this.rbPorFamilia.Text = "Por Família";
            this.rbPorFamilia.UseSelectable = true;
            // 
            // rbPorSubFamilia
            // 
            this.rbPorSubFamilia.AutoSize = true;
            this.rbPorSubFamilia.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbPorSubFamilia.Location = new System.Drawing.Point(357, 31);
            this.rbPorSubFamilia.Name = "rbPorSubFamilia";
            this.rbPorSubFamilia.Size = new System.Drawing.Size(107, 15);
            this.rbPorSubFamilia.TabIndex = 94;
            this.rbPorSubFamilia.Text = "Por Sub-Família";
            this.rbPorSubFamilia.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(26, 184);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(67, 19);
            this.metroLabel1.TabIndex = 95;
            this.metroLabel1.Text = "Armazém";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(268, 235);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(119, 19);
            this.metroLabel2.TabIndex = 96;
            this.metroLabel2.Text = "Desconto à Aplicar";
            // 
            // cmbArmazem
            // 
            this.cmbArmazem.FormattingEnabled = true;
            this.cmbArmazem.ItemHeight = 23;
            this.cmbArmazem.Location = new System.Drawing.Point(119, 174);
            this.cmbArmazem.Name = "cmbArmazem";
            this.cmbArmazem.Size = new System.Drawing.Size(374, 29);
            this.cmbArmazem.TabIndex = 97;
            this.cmbArmazem.UseSelectable = true;
            // 
            // txtDescontoAplicar
            // 
            this.txtDescontoAplicar.Lines = new string[0];
            this.txtDescontoAplicar.Location = new System.Drawing.Point(396, 231);
            this.txtDescontoAplicar.MaxLength = 32767;
            this.txtDescontoAplicar.Name = "txtDescontoAplicar";
            this.txtDescontoAplicar.PasswordChar = '\0';
            this.txtDescontoAplicar.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDescontoAplicar.SelectedText = "";
            this.txtDescontoAplicar.Size = new System.Drawing.Size(75, 23);
            this.txtDescontoAplicar.TabIndex = 98;
            this.txtDescontoAplicar.UseSelectable = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(477, 241);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 99;
            this.label2.Text = "%";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbPorMercadoria);
            this.groupBox1.Controls.Add(this.rbPorFamilia);
            this.groupBox1.Controls.Add(this.rbPorSubFamilia);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(23, 103);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(470, 52);
            this.groupBox1.TabIndex = 100;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Critério de Atribuição de Descontos";
            // 
            // FormGestaoDescontos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 275);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtDescontoAplicar);
            this.Controls.Add(this.cmbArmazem);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "FormGestaoDescontos";
            this.Text = "Gestao de Descontos de Artigos";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btOk;
        private System.Windows.Forms.ToolStripButton btImprimir;
        private System.Windows.Forms.ToolStripButton btSair;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private MetroFramework.Controls.MetroRadioButton rbPorMercadoria;
        private MetroFramework.Controls.MetroRadioButton rbPorFamilia;
        private MetroFramework.Controls.MetroRadioButton rbPorSubFamilia;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroComboBox cmbArmazem;
        private MetroFramework.Controls.MetroTextBox txtDescontoAplicar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}