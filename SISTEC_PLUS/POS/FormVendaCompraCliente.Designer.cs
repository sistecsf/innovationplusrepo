﻿namespace SISTEC_PLUS.POS
{
    partial class FormVendaCompraCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormVendaCompraCliente));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btImprimir = new System.Windows.Forms.ToolStripButton();
            this.btSair = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.txtNumeroCliente = new MetroFramework.Controls.MetroTextBox();
            this.cmbNomeCliente1 = new MetroFramework.Controls.MetroComboBox();
            this.txtDataInicio = new System.Windows.Forms.MaskedTextBox();
            this.txtDataFim = new System.Windows.Forms.MaskedTextBox();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btImprimir,
            this.btSair});
            this.toolStrip1.Location = new System.Drawing.Point(207, 60);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(122, 25);
            this.toolStrip1.TabIndex = 88;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btImprimir
            // 
            this.btImprimir.Image = ((System.Drawing.Image)(resources.GetObject("btImprimir.Image")));
            this.btImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(73, 22);
            this.btImprimir.Text = "Imprimir";
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // btSair
            // 
            this.btSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btSair.Image = ((System.Drawing.Image)(resources.GetObject("btSair.Image")));
            this.btSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(46, 22);
            this.btSair.Text = "Sair";
            this.btSair.Click += new System.EventHandler(this.btSair_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(151, 2);
            this.toolStripContainer1.Location = new System.Drawing.Point(207, 60);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(151, 27);
            this.toolStripContainer1.TabIndex = 89;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer1.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer1.TopToolStripPanel.Tag = "XC";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 118);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(70, 19);
            this.metroLabel1.TabIndex = 90;
            this.metroLabel1.Text = "Data Início";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(308, 118);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(62, 19);
            this.metroLabel2.TabIndex = 91;
            this.metroLabel2.Text = "Data Fim";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(23, 162);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(122, 19);
            this.metroLabel3.TabIndex = 92;
            this.metroLabel3.Text = "Número do Cliente";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(23, 203);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(110, 19);
            this.metroLabel4.TabIndex = 93;
            this.metroLabel4.Text = "Nome do Cliente";
            // 
            // txtNumeroCliente
            // 
            this.txtNumeroCliente.Lines = new string[0];
            this.txtNumeroCliente.Location = new System.Drawing.Point(160, 158);
            this.txtNumeroCliente.MaxLength = 32767;
            this.txtNumeroCliente.Name = "txtNumeroCliente";
            this.txtNumeroCliente.PasswordChar = '\0';
            this.txtNumeroCliente.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNumeroCliente.SelectedText = "";
            this.txtNumeroCliente.Size = new System.Drawing.Size(77, 23);
            this.txtNumeroCliente.TabIndex = 99;
            this.txtNumeroCliente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNumeroCliente.UseSelectable = true;
            this.txtNumeroCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumeroCliente_KeyPress);
            // 
            // cmbNomeCliente1
            // 
            this.cmbNomeCliente1.FormattingEnabled = true;
            this.cmbNomeCliente1.IntegralHeight = false;
            this.cmbNomeCliente1.ItemHeight = 23;
            this.cmbNomeCliente1.Location = new System.Drawing.Point(160, 193);
            this.cmbNomeCliente1.Name = "cmbNomeCliente1";
            this.cmbNomeCliente1.Size = new System.Drawing.Size(367, 29);
            this.cmbNomeCliente1.TabIndex = 161;
            this.cmbNomeCliente1.UseSelectable = true;
            this.cmbNomeCliente1.SelectedIndexChanged += new System.EventHandler(this.cmbNomeCliente1_SelectedIndexChanged);
            // 
            // txtDataInicio
            // 
            this.txtDataInicio.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtDataInicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDataInicio.Location = new System.Drawing.Point(160, 118);
            this.txtDataInicio.Mask = "00/00/0000";
            this.txtDataInicio.Name = "txtDataInicio";
            this.txtDataInicio.Size = new System.Drawing.Size(77, 20);
            this.txtDataInicio.TabIndex = 162;
            this.txtDataInicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDataInicio.ValidatingType = typeof(System.DateTime);
            this.txtDataInicio.Leave += new System.EventHandler(this.txtDataInicio_Leave);
            // 
            // txtDataFim
            // 
            this.txtDataFim.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtDataFim.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDataFim.Location = new System.Drawing.Point(385, 118);
            this.txtDataFim.Mask = "00/00/0000";
            this.txtDataFim.Name = "txtDataFim";
            this.txtDataFim.Size = new System.Drawing.Size(77, 20);
            this.txtDataFim.TabIndex = 163;
            this.txtDataFim.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDataFim.ValidatingType = typeof(System.DateTime);
            this.txtDataFim.Leave += new System.EventHandler(this.txtDataFim_Leave);
            // 
            // FormVendaCompraCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 244);
            this.Controls.Add(this.txtDataFim);
            this.Controls.Add(this.txtDataInicio);
            this.Controls.Add(this.cmbNomeCliente1);
            this.Controls.Add(this.txtNumeroCliente);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "FormVendaCompraCliente";
            this.Text = "Compra do Cliente";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormVendaCompraCliente_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btImprimir;
        private System.Windows.Forms.ToolStripButton btSair;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox txtNumeroCliente;
        private MetroFramework.Controls.MetroComboBox cmbNomeCliente1;
        private System.Windows.Forms.MaskedTextBox txtDataInicio;
        private System.Windows.Forms.MaskedTextBox txtDataFim;
    }
}