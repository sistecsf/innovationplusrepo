﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using System.IO;
using SISTEC_PLUS;
using SISTEC_PLUS.POS.Recibos;
using System.Text.RegularExpressions;
using SISTEC_PLUS.Recibos;
using Microsoft.Reporting.WinForms;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendas : MetroFramework.Forms.MetroForm
    {
        string sAnulado1, sAnulado, sTipomov;
        int nZero, intPos;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet; 
        string sConta, numeroUnico, sTipoCli, sDisplay_ContraValor, CodAO, sigla, tipoDoc = "", afetaVenda, alterStock, sContaStocks, sPermiteObsArt, sConta_Vendas, sArmzCodAO;
        public static int nArtDesc, nCliDesc, countNS = 0, garantia, indice = -1;
        double cambio;
        string client, nome;
        string STOCKTOTAL, Descricao, prUnitario, impostoConsumo, Desconto, CodFam, CodSubFam, LOCALI, PCA, PVP, FOB, OBS, RegEquip;
        string sNotaEnt;
        string unico, sStatusDoc, TipDocCC, TipDocRef, sAfet, TipoCCorrente, sCarta, sCustopca, sNotaLev, sTipoDocRF, sCodAreaDefault;
        public static string[] strSql, numeroS, strSQL;
        public static string codCliente, nomeCliente, tipoDocumento, codDocumento, numeroDocumento, moeda, valorUDS, idunico, data, utilizador;
        DateTime dData;
        bool bDoASMESTRE, bDoAEFICHAS, bRollback, bModalDialog, bSujou, bEntrega = false;
        int intPosASMESTRE, intPosAEFICHAS, intPosAux, nINS_FICHA, nDos;
        string strGinasio, strHotelBooKing, TipDocStock, strATCAIXA, VendaRemota, sTName, nUnico, dDataLimiteDoc, sCdent, codVendedor, sDesLoja, sStatusDocAux, sDatexpira, TotalUsd, sTipDoc, mlObs;
        public static string lsAnulaMotivo, telefoneCliente, EnderecoCliente, Total, totalkz, sForma_Pagamento, CodArmz, codMarca, codModelo, dataInicio, dataFim, horaInicio, horaFim, referencia, Qtd, strEntrega_Empresa, strEntrega_Morada, strEntrega_PReferencia, strEntrega_Contacto, strEntrega_Telefone, strEntrega_Data, strEntrega_Hora;

        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public FormVendas()
        {
            InitializeComponent();
        }
        private void gravar()
        {
            string pcl, pvp, fob;
             if (lbCodAO.Text == "" || lbCodAO.Visible == false) CodAO = "1";
             SqlCommand comand = conexao.CreateCommand();
             SqlCommand[] comd = new SqlCommand[metroGrid1.RowCount - 1];
             //MessageBox.Show(idunico);
             for (int i = 0; i < metroGrid1.RowCount - 1; i++)
             {
                 pcl = "" + Convert.ToDouble(metroGrid1[12, i].Value) * Convert.ToDouble(lbCambio.Text);
                 pvp = "" + Convert.ToDouble(metroGrid1[13, i].Value) * Convert.ToDouble(lbCambio.Text);
                 fob = "" + Convert.ToDouble(metroGrid1[14, i].Value);

                 comd[i] = conexao.CreateCommand();
                 //comd[i].CommandText = "INSERT INTO adm.AAMOVIMENTOS (IDUNICO, NUMDOC,                     CODARMZ,                    CODLOJA,                          REFERENC,                     PCL,                               DATACRIA     ,                           PVP,                                   PVPD,                                                                     PCLD,                                          FOB,                               ALTSTOK,                            QTD,                                                X1,                        DESCONT,                  CODENTIDADE,                          CODDOC,            TIPMOV,           POSTO,       CODVEND,                CODUTILIZADOR,                   MOEDA,                               CODAO,               DATA_LANCAMENTO,                         Obs)" +
                        //"values ('" + idunico + "','" + Convert.ToDouble(numeroUnico) + "', '" + metroGrid1[9, i].Value + "','" + Variavel.codLoja + "',    '" + metroGrid1[0, i].Value + "', '" + pcl.Replace(',', '.') + "', Convert(varchar(20),GetDate(), 120) , '" + pvp.Replace(',', '.') + "', '" + metroGrid1[13, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[12, i].Value.ToString().Replace(',', '.') + "', '" + fob.Replace(',', '.') + "', '" + metroGrid1[16, i].Value + "', '" + metroGrid1[3, i].Value + "', '" + Convert.ToDouble(metroGrid1[18, i].Value) + "', '" + lbDesconto.Text + "', '" + txtCodigo.Text + "', '" + lbTopo.Text + "',     '-',                0 , '" + codVendedor + "', '" + Variavel.codUtilizador + "', '" + lbMoeda.Text + "', '" + metroGrid1[17, i].Value + "', '" + dataLancamente() + "', '" + metroGrid1[15, i].Value + "')";

                 comd[i].CommandText = "INSERT INTO ASFICMOV1 (CodLoja,ALTSTOK,DataCria,                    Data_Lancamento,                  MOEDA,                          FOB,                                        PCLD,                                                              PVPD,                                       PCL,                                       PVP,                 CodEntidade,                CodUtilizador,                         CodAO,                 IDunico,                NumDoc,                    CodArmz,                          Referenc,                            QTD,                     CodDoc,       TIPMOV,                          X1)" +
                        "values ('" + Variavel.codLoja + "','S',Convert(Varchar(20),GetDate(),120),Convert(Varchar(10),GetDate(),120), '" + lbMoeda.Text + "', '" + fob.Replace(',', '.') + "','" + metroGrid1[12, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[13, i].Value.ToString().Replace(',', '.') + "', '" + pcl.Replace(',', '.') + "', '" + pvp.Replace(',', '.') + "', '" + txtCodigo.Text + "', '" + Variavel.codUtilizador + "', '" + metroGrid1[17, i].Value + "','" + idunico + "','" + numeroUnico + "', '" + metroGrid1[9, i].Value + "', '" + metroGrid1[0, i].Value + "', '" + metroGrid1[3, i].Value + "', '" + lbTopo.Text + "', '-', '" + Convert.ToDouble(metroGrid1[18, i].Value) + "')";
             }
             comand.CommandText = "INSERT INTO adm.AADOCUMENTO (IDUNICO,                DataCria,                         CodUtilizador,                 CodEntidade,                 CodLoja,              CodAO,             CodDoc,             Data_Lancamento)" +
                                "values ('" + idunico + "', CONVERT(VARCHAR(20), GETDATE(), 120), '" + Variavel.codUtilizador + "', '" + txtCodigo.Text + "', '" + Variavel.codLoja + "','" + CodAO + "','" + lbTopo.Text + "', '" + dataLancamente() + "')";
             

             conexao.Open();
             SqlTransaction tran = conexao.BeginTransaction();
             try
             {
                 
                 comand.Transaction = tran;
                 comand.ExecuteNonQuery();
                 
                 for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                 {
                     comd[i].Transaction = tran;
                     comd[i].ExecuteNonQuery();
                 }
                 
                 for (int i = 0; i < metroGrid2.RowCount - 1; i++)
                 {
                     SqlCommand cmd = new SqlCommand("INSERT Into adm.AAMOVIMENTOE (IDUNICO, CODDOC, CODMARCA,                 CODMODELO,                          NSERIE,                    CODLOJA,                          REFERENC,                 CODENTIDADE,                   DataCria,                         CodUtilizador)" +
                             "Values ('" + idunico + "', '" + lbTopo.Text + "', '" + metroGrid2[0, i].Value + "', '" + metroGrid2[1, i].Value + "', '" + metroGrid2[3, i].Value + "', '" + Variavel.codLoja + "', '" + metroGrid2[2, i].Value + "', '" + txtCodigo.Text + "', CONVERT(VARCHAR(20),GETDATE(),120), '" + Variavel.codUtilizador + "')");
                     
                     cmd.Connection = conexao;
                     cmd.Transaction = tran;
                     cmd.ExecuteNonQuery();

                     SqlCommand comando = new SqlCommand("update AEFICHAS set Saiu = 'S' where NSERIE = '" + metroGrid2[3, i].Value + "'");
                     comando.Connection = conexao;
                     comando.Transaction = tran;
                     comando.ExecuteNonQuery();
                 }
                 //*
                 SqlCommand cmad = new SqlCommand("SP_DOCUMENTO", conexao);
                 cmad.CommandType = CommandType.StoredProcedure;

                 cmad.Parameters.Add(new SqlParameter("@p_nIDUNICO", idunico));
                 cmad.Parameters.Add(new SqlParameter("@p_sCodDoc", lbTopo.Text));
                 cmad.Parameters.Add(new SqlParameter("@p_sUserCode", Variavel.codUtilizador));
                 cmad.Parameters.Add(new SqlParameter("@p_sCodLoja", Variavel.codLoja));
                 cmad.Parameters.Add(new SqlParameter("@p_sCodEntidade", txtCodigo.Text));
                 cmad.Parameters.Add(new SqlParameter("@p_sREquipamento", RegEquip));
                 cmad.Parameters.Add(new SqlParameter("@p_sCodAO", sCdent));
                 cmad.Parameters.Add(new SqlParameter("@p_sCodConta2", lbConta.Text));
                 cmad.Parameters.Add(new SqlParameter("@p_sCodConta", Variavel.contakz));
                 cmad.Parameters.Add(new SqlParameter("@p_sFormaPag", sForma_Pagamento));
                 cmad.Parameters.Add(new SqlParameter("@p_sImpSelo", "S"));

                 cmad.Transaction = tran;
                 cmad.ExecuteNonQuery();
                 //*/
                 tran.Commit();
                 MessageBox.Show("Gravado com sucesso!");
             }
             catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
             finally { conexao.Close(); }
        }
        private void imprimirFatura()
        {
            int i = 0, count = 0;
            double valorExtenso = 0;
            SqlCommand cmd2 = new SqlCommand("SELECT count(F.Referenc) FROM ASFICMOV1 F WITH (NOLOCK), ASMESTRE M WITH (NOLOCK) where IDunico = '" + idunico + "' AND M.Referenc = F.Referenc", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd2.ExecuteReader())
            {
                while (reader.Read())
                {
                    count = Convert.ToInt32(reader[0].ToString());
                }
            }
            conexao.Close();
            string[] codigo = new string[count], designacao = new string[count], quantidade = new string[count], preco = new string[count], valor = new string[count];
            
            SqlCommand cmd1 = new SqlCommand("SELECT F.Referenc, M.DescrArtigo, F.QTD, F.PVP, F.PVP * F.QTD FROM ASFICMOV1 F WITH (NOLOCK), ASMESTRE M WITH (NOLOCK) where IDunico = '"+ idunico +"' AND M.Referenc = F.Referenc", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd1.ExecuteReader())
            {
                while (reader.Read())
                {
                    codigo[i] = reader[0].ToString();
                    designacao[i] = reader[1].ToString();
                    quantidade[i] = reader[2].ToString();
                    preco[i] = reader[3].ToString();
                    valor[i] = reader[4].ToString();
                    valorExtenso = valorExtenso + Convert.ToDouble(valor[i]);
                    i++;
                }
            }
            conexao.Close();
            valorExtenso = Convert.ToDouble(lbComDesconto.Text);
            DateTime Data = DateTime.Now;
            string hora = "" + Data.Hour + ":" + Data.Minute + ":" + Data.Second;
            Extenso.TipoValorExtenso valorE = Extenso.TipoValorExtenso.Decimal;
            if (cmbNome.Text != "")
                nome = cmbNome.Text + " - " + txtCodigo.Text;
            else if(cmbNome.PromptText != "")
                nome = cmbNome.PromptText + " - " + txtCodigo.Text;
            ReciboVendas formRecibo = new ReciboVendas(hora, idunico, lbMoeda.Text, nome, EnderecoCliente,
                telefoneCliente, lbValorPadrao.Text, Extenso.toExtenso(Convert.ToDecimal(valorExtenso), valorE), lbMoedaNacional.Text, lbValorPadrao.Text,
                codigo, designacao, quantidade, preco, valor, metroGrid1.RowCount - 1);
            formRecibo.Show();
        }
        private void limparCampos()
        {
            txtCodigo.Text = "";
            txtDesconto2.Text = "";
            txtQtd.Text = "";
            cmbArea.Text = "";
            cmbDesignacao.Text = "";
            cmbReferencia.Text = "";
            cmbTipo.Text = "";
            cmbNome.Text = "";
            cmbVendedor.Text = "";
            cmbDesignacao.PromptText = "";
            cmbReferencia.PromptText = "";
            cmbNome.PromptText = "";
            metroGrid1.Rows.Clear();
            lbVendedor.Text = "";
            lbMoedaNacional.Text = "0";
            lbComDesconto.Text = "0";
            lbValorPadrao.Text = "0";
            txtObservacao.Text = "";
            lbPrUnitario.Text = "0";
            lbStock.Text = "0";
            lbCons.Text = "0";
            lbDesc.Text = "0";
            lbTotal.Text = "0";
        }
        private DateTime garantiaM(DateTime data, int meses)
        {
            DateTime da = data.AddMonths(meses);
            string dat = "" + da.Year + "-" + da.Month + "-" + da.Day;
            return Convert.ToDateTime(dat);
        }
        private DateTime ProxDiaUtil(string p_dData, int p_iNrDias ){
            dData = Convert.ToDateTime(p_dData);
            int nDias = 0, nYear, nMonth, nNewDay, nWeekDay;
            if (p_dData == null || p_iNrDias == null)
                return dData;
            else {
                while(nDias < p_iNrDias){
                    nYear = dData.Year;
                    nMonth = dData.Month;
                    nNewDay = dData.Day;
                    nNewDay = nNewDay + 1;
                    //dData = SalDateConstruct(nYear, nMonth, nNewDay, 0, 0, 0);
                    nWeekDay = Convert.ToInt32(dData.DayOfWeek);
                    // ============= Verificar se o dia resultante do incremento calha num Sábado/Domingo ========================
                    if(Gerais.PARAM_SAB_DIA_UTIL != "1"){
                        if(nWeekDay != 0 && nWeekDay != 1)
                            nDias = nDias +1;
                    // ============= Se não for Sábado/Domingo verificar se o dia em questão é um Feriado =======================
                        else{
                            nDias = nDias +1;
                        }
                    }
                    else if (Gerais.PARAM_DOM_DIA_UTIL != "1"){
                        if(nWeekDay != 1)
                            nDias = nDias +1;
                        else
                           nDias = nDias +1;
                    }
                }
                return dData;
            }
        }
        private void prenxarmetroGrid2()
        {
            for (int i = 0; i < countNS; i++)
            {
                metroGrid2.Rows.Add(codMarca, codModelo, referencia, numeroS[i], garantiaM(Variavel.dataAtual, garantia), txtCodigo.Text);
            }
        }
        private void prenxarDgTempo()
        {
            bool b = false;
            int c = -1;
            for (int i = 0; i < dgTempo.RowCount - 1; i++)
            {
                if (cmbReferencia.Text == dgTempo[0, i].Value.ToString())
                {
                    b = true;
                    c = i;
                }
            }

            if (b == false)
            {
                dgTempo.Rows.Add(referencia, dataInicio, dataFim, horaInicio, horaFim);
            }
            else
            {
                this.dgTempo[1, c].Value = dataInicio;
                this.dgTempo[2, c].Value = dataFim;
                this.dgTempo[3, c].Value = horaInicio;
                this.dgTempo[4, c].Value = horaFim;
            }
        }
        private void penxerDataGrid()
        {
            double total = 0;
            bool b = false;
            int c = -1;
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                if (cmbReferencia.Text == metroGrid1[0, i].Value.ToString() && cmbDesignacao.Text == metroGrid1[1, i].Value.ToString())
                {
                    b = true;
                    c = i;
                }
            }

            if (b == true)
            {
                if (strHotelBooKing == "S")
                {
                    this.metroGrid1[3, c].Value = Qtd;
                    this.metroGrid1[6, c].Value = Total;
                }
                else
                {
                    Qtd = "" + (Convert.ToInt32(txtQtd.Text) + Convert.ToInt32(metroGrid1[3, c].Value));
                    Total = "" + (Convert.ToDouble(lbTotal.Text) + Convert.ToDouble(metroGrid1[6, c].Value));
                    if (Convert.ToInt32(Qtd) <= Convert.ToInt32(lbStock.Text))
                    {
                        this.metroGrid1[3, c].Value = Qtd;
                        this.metroGrid1[6, c].Value = Total;
                    }
                    else
                        MessageBox.Show("A Quantidade total do artigo tem de ser menor ou igual ao que tem no Stock");
                }
            }
            else
            {
                metroGrid1.Rows.Add(referencia, Descricao, prUnitario, Qtd, impostoConsumo, Desconto, Total, CodFam, CodSubFam, CodArmz, Variavel.codUtilizador, LOCALI, PCA, PVP, FOB, OBS, sContaStocks, "0", "0");
            }
            total = 0;
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                total = total + Convert.ToDouble(metroGrid1[6, i].Value);
            }
            lbMoedaNacional.Text = totalkz = "" + total;
            lbComDesconto.Text = ""+(total - (total * (Convert.ToDouble(lbDesconto.Text) / 100)));
            lbValorPadrao.Text = "" + Convert.ToDouble(lbComDesconto.Text) / Convert.ToDouble(lbCambio.Text);
        }
        private string pesquisarCliente(string codcliente)
        {
            SqlCommand cmd = new SqlCommand("select NomeEntidade from AFENTIDADE where CodEntidade = '" + codcliente + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    client = reader[0].ToString();
                }
            }
            conexao.Close();
            
            return client;
        }
        private void cmbNomeCliente()
        {
            int tipo = 0;
            SqlCommand cmd = new SqlCommand("SP_ComboEntidade", conexao);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@p_iTipoSeleccao", tipo));
            cmd.Parameters.Add(new SqlParameter("@p_sNivelUSER", Permissoes.nPrgClie));
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbNome.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (Exception ex) { }
            finally{conexao.Close();}
        }
        private void CmbVendedor()
        {
            SqlCommand cmd = new SqlCommand(@"SELECT NomeEntidade FROM AFENTIDADE with (NoLock) WHERE CodTipoCli = 'VEN' ORDER BY NomeEntidade");
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbVendedor.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void cmbTipoDocumento()
        {
            if(Variavel.contaNacional == "" && Variavel.contaPadao == ""){
                SqlCommand cmd = new SqlCommand(@"SELECT DescrDoc FROM ASDOCS WITH (NOLOCK) WHERE UPPER(TipoPos) = 'S' AND ISNULL(TIPODOC, 0) !='R' ORDER BY descrdoc");
                cmd.Connection = conexao;
                try
                {
                    conexao.Open();
                    SqlDataReader leitor = cmd.ExecuteReader();
                    while (leitor.Read())
                    {
                        cmbTipo.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                    }
                    conexao.Close();
                }
                catch (SqlException ex){MessageBox.Show(ex.Message);}
                finally{conexao.Close();}
            }
            else
            {
                SqlCommand cmd = new SqlCommand(@"SELECT descrdoc, CodDoc FROM ASDOCS WITH (NOLOCK) WHERE UPPER(TipoPos) = 'S' ORDER BY descrdoc");
                cmd.Connection = conexao;
                try
                {
                    conexao.Open();
                    SqlDataReader leitor = cmd.ExecuteReader();
                    while (leitor.Read())
                    {
                        cmbTipo.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                    }
                    conexao.Close();
                }
                catch (SqlException ex) { MessageBox.Show(ex.Message); }
                finally { conexao.Close(); }
            }
        }
        private string idUnico()
        {
            string unico;
            SqlCommand cmd2 = new SqlCommand("SELECT RTRIM(ISNULL(CODLOJA,'" + Variavel.codLoja + "')) + CAST (ISNULL( MAX( CAST( RIGHT (RTRIM(IDUNICO), LEN(RTRIM(IDUNICO)) - (LEN(ISNULL(CODLOJA,'" + Variavel.codLoja + "'))))AS INT )), 1) + 1 AS VARCHAR(23)) AS IDUNICO FROM AREGDOC  WITH (XLOCK) WHERE CODLOJA= '" + Variavel.codLoja + "' AND LEFT(IDUNICO, LEN(CODLOJA)) = '" + Variavel.codLoja + "' GROUP BY CODLOJA", conexao);
            conexao.Open();
            try { 
                unico = (string)cmd2.ExecuteScalar();
                if (unico == null || unico == "")
                    unico = Variavel.codLoja + 1;
            }
            catch (Exception) { unico = Variavel.codLoja + 1; }
            finally { conexao.Close(); }
            return unico;
        }
        private string numeroDoc()
        {
            string numero;
            SqlCommand cmd = new SqlCommand("SP_GET_NUMDOC", conexao);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@p_sCodLoja", Variavel.codLoja));
            cmd.Parameters.Add(new SqlParameter("@p_sCodDoc", Gerais.PARAM_CODDOC_VD));

            try
            {
                conexao.Open();
                numero = "" + cmd.ExecuteScalar();
                if (numero == null || numero == "")
                    numero = Variavel.codUtilizador + 1;
            }
            catch (Exception) { numero = Variavel.codUtilizador + 1; }
            finally{ conexao.Close(); }
            return numero;
        }
        private void selecionarCodCliente()
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("SELECT CODENTIDADE, ISNULL(DESC1,0), RTRIM(CODTIPOCLI), RTRIM(LTRIM(Display_ContraValor)), Morada, FONE FROM AFENTIDADE WITH (NOLOCK) WHERE RTRIM(NomeEntidade) = '" + cmbNome.Text + "' ", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codCliente = reader[0].ToString();
                        nCliDesc = Convert.ToInt32(reader[1].ToString());
                        sTipoCli = reader[2].ToString();
                        sDisplay_ContraValor = reader[3].ToString();
                        EnderecoCliente = reader[4].ToString();
                        telefoneCliente = reader[5].ToString();
                    }
                }
                txtCodigo.Text = codCliente;
                cmbNome.PromptText = nomeCliente;
                lbDesconto.Text = "" + nCliDesc;
            }
            catch (Exception) { }
            finally 
            { 
                conexao.Close();
                if (EnderecoCliente == null || EnderecoCliente == "")
                    EnderecoCliente = ".";
                if (telefoneCliente == null || telefoneCliente == "")
                    telefoneCliente = ".";
            }
        }
        private void selecionarNomeCliente(string codigo)
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("SELECT RTRIM(NomeEntidade), ISNULL(DESC1,0), RTRIM(CODTIPOCLI), RTRIM(LTRIM(Display_ContraValor)), Morada, FONE FROM AFENTIDADE WITH (NOLOCK) WHERE RTRIM(CODENTIDADE) = '" + codigo + "' ", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        nomeCliente = reader[0].ToString();
                        nCliDesc = Convert.ToInt32(reader[1].ToString());
                        sTipoCli = reader[2].ToString();
                        sDisplay_ContraValor = reader[3].ToString();
                        EnderecoCliente = reader[4].ToString();
                        telefoneCliente = reader[5].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally
            {
                conexao.Close();
                cmbNome.PromptText = nomeCliente;
                cmbNome.Text = nomeCliente;
                lbDesconto.Text = "" + nCliDesc;
                if (EnderecoCliente == null || EnderecoCliente == "")
                    EnderecoCliente = ".";
                if (telefoneCliente == null || telefoneCliente == "")
                    telefoneCliente = ".";
            }
        }
        private void FormVendas_Load(object sender, EventArgs e)
        {
            btImprimir.Enabled = false;
            cbEmitirCarta.Enabled = false;
            cmbArea.Visible = false;
            lbArea.Visible = false;
            
            CmbVendedor();
            sCodAreaDefault = Variavel.codArea; 
            lbCambio.Text = Variavel.cambio;
            data = ""+Variavel.dataAtual;

            DateTime Data = DateTime.Now;
            strATCAIXA = "ATCAIXA_" + Data.Year;
            SqlCommand cmd2 = new SqlCommand("select VENDAREMOTA from ASLOJA where CodLoja ='"+ Variavel.codLoja +"'", conexao);
            conexao.Open();
            try { VendaRemota = (string)cmd2.ExecuteScalar(); }
            catch (Exception) { VendaRemota = null; }
            finally { conexao.Close(); }
        }
        private void cmbArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("Select CodAO from AIAREAORGANICA WITH (NOLOCK) where NomeAO ='" + cmbArea .Text + "'", conexao);
            cmd.Connection = conexao;
            conexao.Open();
            try {CodAO = (string)cmd.ExecuteScalar(); }
            catch (Exception) { }
            finally { conexao.Close(); }
            lbCodAO.Text = CodAO;
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }


        private void txtQtd_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtQtd.Text) <= Convert.ToInt32(STOCKTOTAL))
                {
                    try
                    {
                        lbTotal.Text = "" + Convert.ToInt32(txtQtd.Text) * Convert.ToDouble(prUnitario);
                    }
                    catch
                    {
                        lbTotal.Text = "" + 0 * Convert.ToDouble(prUnitario);
                    }
                    Total = lbTotal.Text;
                    Qtd = txtQtd.Text;
                }
                else
                {
                    MessageBox.Show("A quantidade tem que ser menor ou igual ao que tem disponivel no Stock");
                    Qtd = txtQtd.Text = "0";
                }
            }
            catch (Exception) { Qtd = "0"; }
        }

        private void txtQtd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (strHotelBooKing == "S")
                {
                    FormVendaHotelBooking form = new FormVendaHotelBooking();
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        prenxarDgTempo();
                        penxerDataGrid();
                    }
                }
                else if (RegEquip == "S")
                {
                    FormVendasNSerie form = new FormVendasNSerie();
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        penxerDataGrid();
                        prenxarmetroGrid2();
                    }
                }
                else
                    penxerDataGrid(); 
            }
        }

        private void btGravar_Click(object sender, EventArgs e)
        {
            idunico = idUnico();
            lbNumero.Text = numeroUnico = numeroDoc();
            if (cmbArea.Text != "" && cmbArea.Visible)
                sCdent = CodAO;
            else
                sCdent = sCodAreaDefault;

            if (cmbNome.Text == "" && cmbNome.PromptText == "") MessageBox.Show("O campo [ Nome do Cliente ] tem que ser preenchido. Por favor reveja o lançamento !");
            else if (cmbTipo.Text == "" && cmbTipo.PromptText == "") MessageBox.Show("O campo [ Tipo Documento ] tem que ser preenchido. Por favor reveja o lançamento !");
            else if (lbTopo.Text.Trim() == "FA" && txtCodigo.Text.Trim() == "G1") MessageBox.Show("O cliente " + txtCodigo.Text + " nao pode emitir uma factura!");
            else
            {
                SqlCommand cmd = new SqlCommand("SELECT UPPER(Emitir_NL) From AIAREAORGANICA WITH (NOLOCK) where CodAO ='" + CodAO + "'", conexao);
                cmd.Connection = conexao;
                conexao.Open();
                try { sNotaEnt = (string)cmd.ExecuteScalar(); }
                catch (Exception) { }
                finally { conexao.Close(); }

                SqlCommand cmd1 = new SqlCommand("SELECT ccorrente, tipoutil, TipoCCorrente, Emitir_CT, Custo_PCA, UPPER(EMITIR_NL), TIPODOC FROM ASDOCS WITH (NOLOCK) where descrdoc = '" + cmbTipo.Text + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TipDocCC = reader[0].ToString();
                        TipDocRef = reader[1].ToString();
                        TipoCCorrente = reader[2].ToString();
                        sCarta = reader[3].ToString();
                        sCustopca = reader[4].ToString();
                        sNotaLev = reader[5].ToString();
                        sTipoDocRF = reader[6].ToString();
                    }
                }
                conexao.Close();

                if (lbTopo.Text == "TK" || lbTopo.Text == "TP" || lbTopo.Text == "VD" || lbTopo.Text == "VP")
                {
                    FormVendasFormaPagamento form = new FormVendasFormaPagamento();
                    if (form.ShowDialog() == DialogResult.OK) { gravar(); }
                }
                else 
                {
                    sForma_Pagamento = "";
                    gravar(); 
                }
                try { imprimirFatura(); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
                limparCampos();
            }
        }
        private void cmbVendedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            conexao.Close();
            SqlCommand cmd = new SqlCommand(@"SELECT CodEntidade FROM AFENTIDADE with (NoLock) where NomeEntidade = '"+cmbVendedor.Text+"'", conexao);
            cmd.Connection = conexao;
            conexao.Open();
            lbVendedor.Text = (string)cmd.ExecuteScalar();
            conexao.Close();
        }
        private string dataLancamente()
        {
            SqlCommand cmd2 = new SqlCommand("select MAX(data_lancamento) from AFLANCAMENTO where CodLoja=@loja ", conexao);
            cmd2.Parameters.Add(new SqlParameter("@loja", Variavel.codLoja));
            string data = "";
            
            try
            {
                conexao.Open();
                data = (string)cmd2.ExecuteScalar();
                //DateTime dt = Convert.ToDateTime(data);
                //data = "" + dt.Year + "-" + dt.Month + "-" + dt.Day;
            }
            catch (Exception ex) { data = "0"; }
            finally { conexao.Close(); }
            return data;
        }
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            //*
            btGravar.Enabled = true;
            btImprimir.Enabled = false;
            metroGrid1.DataSource = null;
            metroGrid1.Rows.Clear();
            metroGrid1.Refresh();
            metroGrid2.Rows.Clear();
            metroGrid2.Refresh();
            dgTempo.Rows.Clear();
            dgTempo.Refresh();
            cmbReferencia.Text = "";
            cmbDesignacao.Text = "";
            cmbReferencia.Items.Clear();
            cmbDesignacao.Items.Clear();
            txtCodigo.Text = cmbNome.Text = "";
            lbNumero.Text = "";
            cmbTipo.Text = "";
            
            
            cmbNome.PromptText = ""; //*/
            
            //cmbNome.Items.Clear();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            FormVendaAnexo form = new FormVendaAnexo();
            form.ShowDialog();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            FormVendasConsultaDocumento form = new FormVendasConsultaDocumento();
            if (form.ShowDialog() == DialogResult.OK)
            {
                btGravar.Enabled = false;
                btImprimir.Enabled = true;
                txtCodigo.Text = codCliente;
                cmbNome.Text = nomeCliente;
                cmbNome.PromptText = nomeCliente;
                lbTopo.Text = codDocumento;
                cmbTipo.Text = tipoDocumento;
                cmbTipo.PromptText = tipoDocumento;
                lbMoeda.Text = moeda;
                lbNumero.Text = numeroDocumento;
                lbValorPadrao.Text = valorUDS;
                lbMoedaNacional.Text = "" + Convert.ToDouble(valorUDS) * Convert.ToDouble(lbCambio.Text);
                lbComDesconto.Text = "" + Convert.ToDouble(valorUDS) * Convert.ToDouble(lbCambio.Text);
                selecionarNomeCliente(codCliente);

                metroGrid1.AutoGenerateColumns = false;
                try
                {
                    conexao.Open();
                    dataAdapter = new SqlDataAdapter(@"select A.Referenc, M.DescrArtigo, A.PVP, A.QTD, A.PVP*A.QTD, A.CodArmz from ASFICMOV1 A with (NOLOCK), ASMESTRE M with (NOLOCK) where IDUNICO = '"+ idunico +"' AND A.Referenc = M.Referenc", conexao);
                    dataSet = new System.Data.DataSet();
                    dataTable = new DataTable("pessoa");
                    dataView = new DataView(dataTable);
                    dataAdapter.Fill(dataTable);
                    metroGrid1.DataSource = dataView;
                }
                catch (Exception ex) { }
                finally { conexao.Close(); }
            }
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (metroGrid1.RowCount > 1)
                imprimirFatura();
            btImprimir.Enabled = false;
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            indice = e.RowIndex;
        }

        private void eliminarLinhaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (indice >= 0)
            {
                if (metroGrid1.Rows[indice].Cells[0].Value != DBNull.Value)
                {
                    metroGrid1.Rows.RemoveAt(indice);
                }
                metroGrid1.Refresh();
            }
            else
                MessageBox.Show("Tem de clicar na linha da tabela que pretende eliminar");
            indice = -1;
        }

        private void anularDocumentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lbNumero.Text == "")
                MessageBox.Show("É preciso Selectionar o Documento à anulado!");
            else
            {
                strSQL = new string[5];
                int b = 1;
                //==== VERIFICAR SE O USER TEM PERMISSÃO PARA A ANULAÇÃO --> APENAS NÍVEL 5====
                SqlCommand cmd = new SqlCommand("SELECT CCorrente, AltStock, TipoUtil, AfetaVend, TipoCCorrente, Emitir_CT, Custo_PCA, TipoDoc From ASDOCS WITH (NOLOCK) WHERE coddoc= '" + lbTopo.Text + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        TipDocCC = reader[0].ToString();
                        TipDocStock = reader[1].ToString();
                        TipDocRef = reader[2].ToString();
                        sAfet = reader[3].ToString();
                        TipoCCorrente = reader[4].ToString();
                        sCarta = reader[5].ToString();
                        sCustopca = reader[6].ToString();
                        sTipoDocRF = reader[7].ToString();
                    }
                }
                conexao.Close();
                string sSta = "", sActual_Contab = "";

                SqlCommand comd = new SqlCommand("SELECT RTRIM(LTRIM(R.STATUS)), C.FLAG From AREGDOC R, ATCAIXA_2015 C WHERE R.CODLOJA = '" + Variavel.codLoja + "' AND R.CODDOC ='" + lbTopo.Text + "' AND R.NUMDOC ='" + lbNumero.Text + "' AND C.NUMDOC =* R.NUMDOC AND C.CODUTILIZADOR =* R.CODUTILIZADOR AND C.DESCRICAO =* R.DESCR AND LTRIM(RTRIM(R.IDUnico)) = '" + idunico + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = comd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        sSta = reader[0].ToString();
                        sActual_Contab = reader[1].ToString();
                    }
                }
                conexao.Close();
                if (sSta == "A")
                    MessageBox.Show("Este Documento já esta Anulado!");
                else if ((sSta == "P" && sTipoDocRF != "R") || (sSta == "F" && sTipoDocRF != "R"))
                    MessageBox.Show("Este Documento já foi Pago!");
                else
                {
                    if (sActual_Contab == "X")
                        MessageBox.Show("Não lhe é permitido alterar o estado deste Documento, porque o mesmo já sofreu Prestação de Contas. Para mais informações dirija-se à Contabilidade. OBRIGADO.");
                    else
                    {
                        if (Convert.ToInt32(Permissoes.nPrgPos) < 4)
                            MessageBox.Show("Não tem permissão para executar a acção seleccionada!");
                        else
                        {
                            //======================== VERIFICAR SE O DOCUMENTO EXISTE OU TEM DADOS ========================
                            if (lbNumero.Text == "" || lbNumero.Text == "0")
                                MessageBox.Show("O documento não existe ou não tem dados!");
                            else
                            {
                                //======= VERIFICAR SE A DATA DE LANÇAMENTO DO DOC É IGUAL À DATA DE ABERTURA DO DIA ===========
                                SqlCommand comad = new SqlCommand("Select Top 1 IDunico From AREGDOC Where IDunico = '" + idunico + "' And Convert(Varchar(10),data_lancamento,112) = Convert(Varchar(10),'" + dataLancamente() + "',112)", conexao);
                                conexao.Open();
                                using (SqlDataReader reader = comad.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        unico = reader[0].ToString();
                                    }
                                }
                                conexao.Close();
                                if (unico == "")
                                {
                                    if (Convert.ToInt32(Permissoes.nPrgPos) < 5)
                                        MessageBox.Show("As Anulações de Documentos apenas são permitidas no Dia de Lançamento!");
                                }
                                else
                                {
                                    const string message = "Confirma a ANULAÇÃO do documento?";
                                    const string caption = "CONFIRMAÇÃO";
                                    var result = MessageBox.Show(message, caption, MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                                    if (result == DialogResult.OK)
                                    {
                                        sTName = "T_UPD_VENDAS";
                                        nZero = 0;
                                        sAnulado1 = " ANULADO(A)";
                                        sAnulado = cmbTipo.Text + sAnulado1;
                                        sAnulado = sAnulado.ToUpper();
                                        sStatusDoc = "A";
                                        sTipomov = "-";
                                        //*************** INÍCIO - EDIÇÃO DAS LINHAS DA TBL P/ TORNÁ-LAS ALTERÁVEIS ***************
                                        //*************** FIM - EDIÇÃO DAS LINHAS DA TBL P/ TORNÁ-LAS ALTERÁVEIS ***************
                                        //=============== INSERÇÃO DOS REGISTOS NA strSQL ===============
                                        intPos = 0;
                                        //************* UPDATE DO STATUS DO DOCUMENTO ORIGINAL NA TABELA AREGDOC *****************
                                        FormVendasAnulaMotivo form = new FormVendasAnulaMotivo();
                                        if (form.ShowDialog() == DialogResult.OK)
                                        {
                                            strSQL[intPos] = "UPDATE AREGDOC SET Status = '" + sStatusDoc + "', Data_Anulacao = CONVERT(VARCHAR(10),'" + dataLancamente() + "',120)," +
                                                            "CodUtil_Anulacao = '" + Variavel.codUtilizador + "', MOTIVO_ANULACAO= '" + lsAnulaMotivo + "' WHERE LTRIM(RTRIM(Idunico)) = '" + idunico + "'";
                                            intPos += 1;
                                            //************* ESTORNAR ( VALOR NEGATIVO ) O DOCUMENTO DO CAIXA *****************
                                            lbConta.Text = lbConta.Text.Trim();
                                            //sConta = sConta.Trim();
                                            //************* ELIMINAR O DOCUMENTO DA CONTA CORRENTE DO CLIENTE *****************
                                            if (lbTopo.Text == Gerais.PARAM_CODDOC_GR)
                                            {
                                                SqlCommand cmad = new SqlCommand("SELECT TOP 1 IDUNICO FROM AFCONCLI WITH (NOLOCK) Where LTRIM(RTRIM(Idunico)) = '" + idunico + "'", conexao);
                                                conexao.Open();
                                                using (SqlDataReader reader = cmad.ExecuteReader())
                                                {
                                                    while (reader.Read())
                                                    {
                                                        unico = reader[0].ToString();
                                                    }
                                                }
                                                conexao.Close();
                                                if (unico != "" || unico != null)
                                                    TipDocCC = "S";
                                            }
                                            if (TipDocCC == "S")
                                            {
                                                strSQL[intPos] = "DELETE FROM AFCONCLI WHERE LTRIM(RTRIM(Idunico)) = '" + idunico + "'";
                                                intPos += 1;
                                                b += 1;
                                            }
                                            if (sActual_Contab != "X")
                                            {
                                                strSQL[intPos] = "DELETE FROM ATSELOS WHERE LTRIM(RTRIM(Idunico)) = '" + idunico + "'";
                                                intPos = intPos + 1;
                                                strSQL[intPos] = "DELETE FROM ATRECIBO WHERE LTRIM(RTRIM(Idunico)) = '" + idunico + "'";
                                                intPos = intPos + 1;
                                                strSQL[intPos] = "DELETE FROM ATCAIXA_2015 WHERE LTRIM(RTRIM(Idunico)) = '" + idunico + "'";
                                                intPos = intPos + 1;
                                                b += 3;
                                            }
                                            //************* EXECUTAR OS STATEMENTS SQL *****************

                                            for (intPos = 0; intPos < b; intPos++)
                                            {
                                                conexao.Open();
                                                SqlCommand comand = new SqlCommand(strSQL[intPos]);
                                                comand.Connection = conexao;
                                                comand.ExecuteNonQuery();
                                                conexao.Close();
                                            }
                                            //************* UPDATE (REPOR) DO STOCK NA TABELA ARTIGOS *****************
                                            if (TipDocStock == "S")
                                            {
                                                conexao.Open();
                                                SqlCommand comand = new SqlCommand("UPDATE ASMESTRE SET ASMESTRE.QTD = M.QTD + F.QTD FROM ASMESTRE M WITH (NOLOCK), ASFICMOV1 F WITH (NOLOCK) WHERE LTRIM(RTRIM(M.CODLOJA)) = LTRIM(RTRIM(F.CODLOJA))" +
                                                    "AND LTRIM(RTRIM(M.CODARMZ)) = LTRIM(RTRIM(F.CODARMZ)) AND LTRIM(RTRIM(M.REFERENC)) = LTRIM(RTRIM(F.REFERENC)) AND LTRIM(RTRIM(F.IDUNICO))= LTrim(RTrim('" + idunico + "'))");
                                                comand.Connection = conexao;
                                                comand.ExecuteNonQuery();

                                                SqlCommand comando = new SqlCommand("UPDATE AEFICHAS SET CODENTIDADE= '" + Gerais.PARAM_CODCLI_GERAL + "', DATA_INSTALACAO=NULL, DATAGARANTIA=NULL, SAIU=NULL, IDUNICO_SAIDA=NULL WHERE LTRIM(RTRIM(IDunico_Saida))=LTrim(RTrim('" + idunico + "'))");
                                                comando.Connection = conexao;
                                                comando.ExecuteNonQuery();
                                                conexao.Close();
                                            }
                                            MessageBox.Show("Documento Anulado com Sucesso!");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void consultaNºSérieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormVendaConsultaNSerie form = new FormVendaConsultaNSerie();
            form.ShowDialog();
        }

        private void consultaDetalhadaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormVendaConsultaDetalhada form = new FormVendaConsultaDetalhada();
            form.ShowDialog();
        }

        private void comprasClientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormVendaCompraCliente form = new FormVendaCompraCliente();
            form.ShowDialog();
        }

        private void txtCodigo_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                selecionarNomeCliente(txtCodigo.Text);
            }
        }

        private void cmbNome_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            selecionarCodCliente();
        }
        private void cmbTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbReferencia.Items.Clear();
            cmbDesignacao.Items.Clear();
            metroGrid1.DataSource = null;
            metroGrid1.Rows.Clear();
            metroGrid1.Refresh();
            metroGrid2.Rows.Clear();
            metroGrid2.Refresh();
            dgTempo.Rows.Clear();
            dgTempo.Refresh();
            Cursor.Current = Cursors.WaitCursor;
            SqlCommand cmd = new SqlCommand("select RTRIM(LTRIM(CodDoc)), ALTSTOCK, afetavend, UniMonet, ALTSTOCK from ASDOCS WITH (NOLOCK) where DescrDoc = '" + cmbTipo.Text + "'", conexao);
            conexao.Close();
            conexao.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    tipoDoc = reader[0].ToString();
                    alterStock = reader[1].ToString();
                    afetaVenda = reader[2].ToString();
                    moeda = reader[3].ToString();
                    TipDocStock = reader[4].ToString();
                }
            }
            conexao.Close();
            lbTopo.Text = tipoDoc;
            if (lbMoeda.Text != moeda)
            {
                MessageBox.Show("A moeda será alterada");
                lbMoeda.Text = moeda;
            }

            if (moeda == "KZ") lbConta.Text = Variavel.contaNacional;
            else lbConta.Text = Variavel.contaPadao;
            Cursor.Current = Cursors.WaitCursor;
            if (afetaVenda == "S")
            {
                SqlCommand cmd2 = new SqlCommand(@"SELECT DISTINCT RTRIM(LTRIM(REFERENC)) FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA='" + Variavel.codLoja + "' And (PVP > 0 Or (PVP < 0 And IsNull(PROMOCAO,'N')='S')) AND ((CONTASTOCKS = 'S' AND QTD > 0 OR (ISNULL(STOCKNEGATIVO,'N')='S')) OR (ISNULL(CONTASTOCKS,'N')='N')) AND ISNULL(Data_Expira,CONVERT(VARCHAR(10),GETDATE(), 120)) >= CONVERT(VARCHAR(10),GETDATE(), 120) AND ACTIVO = 'S' AND CODARMZ IN (SELECT CODARMZ FROM ASARMAZ WHERE CODLOJA ='" + Variavel.codLoja + "' AND UPPER(ARMA_VENDA)='S') ORDER BY RTRIM(LTRIM(REFERENC))");
                cmd2.Connection = conexao;
                try
                {
                    conexao.Open();
                    SqlDataReader leitor = cmd2.ExecuteReader();
                    while (leitor.Read())
                    {
                        cmbReferencia.Items.Add(leitor.GetValue(0));
                    }
                }
                catch (SqlException ex) { MessageBox.Show(ex.Message); }
                finally { conexao.Close(); }

                SqlCommand cmd3 = new SqlCommand(@"SELECT DISTINCT RTRIM(LTRIM(DESCRARTIGO)) FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA='" + Variavel.codLoja + "' And (PVP > 0 Or (PVP < 0 And IsNull(PROMOCAO,'N')='S')) And ((CONTASTOCKS = 'S' AND QTD > 0 OR (ISNULL(STOCKNEGATIVO,'N')='S')) OR (ISNULL(CONTASTOCKS,'N')='N')) And ISNULL(Data_Expira,CONVERT(VARCHAR(10),GETDATE(), 120)) >= CONVERT(VARCHAR(10),GETDATE(), 120) And ACTIVO = 'S' AND CODARMZ IN (SELECT CODARMZ FROM ASARMAZ WHERE CODLOJA ='" + Variavel.codLoja + "' AND UPPER(ARMA_VENDA)='S') ORDER BY RTRIM(LTRIM(DESCRARTIGO))");
                cmd3.Connection = conexao;
                try
                {
                    conexao.Open();
                    SqlDataReader leitor = cmd3.ExecuteReader();
                    while (leitor.Read())
                    {
                        cmbDesignacao.Items.Add(leitor.GetValue(0));
                    }
                }
                catch (SqlException ex) { MessageBox.Show(ex.Message); }
                finally { conexao.Close(); }
            }
            else if (alterStock == "S")
            {
                SqlCommand cmd2 = new SqlCommand(@"SELECT DISTINCT RTRIM(LTRIM(REFERENC)) FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA='" + Variavel.codLoja + "' And (PVP > 0 Or (PVP < 0 And IsNull(PROMOCAO,'N')='S')) AND ((CONTASTOCKS = 'S' AND QTD > 0 ) OR (ISNULL(CONTASTOCKS,'N')='N')) AND ISNULL(Data_Expira,CONVERT(VARCHAR(10),GETDATE(), 120)) >= CONVERT(VARCHAR(10),GETDATE(), 120) AND ACTIVO = 'S' ORDER BY RTRIM(LTRIM(REFERENC))");
                cmd2.Connection = conexao;
                try
                {
                    conexao.Open();
                    SqlDataReader leitor = cmd2.ExecuteReader();
                    while (leitor.Read())
                    {
                        cmbReferencia.Items.Add(leitor.GetValue(0));
                    }
                }
                catch (SqlException ex) { MessageBox.Show(ex.Message); }
                finally { conexao.Close(); }

                SqlCommand cmd3 = new SqlCommand(@"SELECT DISTINCT RTRIM(LTRIM(DESCRARTIGO)) FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA='" + Variavel.codLoja + "' And (PVP > 0 Or (PVP < 0 And IsNull(PROMOCAO,'N')='S')) AND ((CONTASTOCKS = 'S' AND QTD > 0 ) OR (ISNULL(CONTASTOCKS,'N')='N')) AND ISNULL(Data_Expira,CONVERT(VARCHAR(10),GETDATE(), 120)) >= CONVERT(VARCHAR(10),GETDATE(), 120) AND ACTIVO = 'S' ORDER BY RTRIM(LTRIM(DESCRARTIGO))");
                cmd3.Connection = conexao;
                try
                {
                    conexao.Open();
                    SqlDataReader leitor = cmd3.ExecuteReader();
                    while (leitor.Read())
                    {
                        cmbDesignacao.Items.Add(leitor.GetValue(0));
                    }
                }
                catch (SqlException ex) { MessageBox.Show(ex.Message); }
                finally { conexao.Close(); }
            }
            else
            {//*
                SqlCommand cmd2 = new SqlCommand(@"SELECT DISTINCT RTRIM(LTRIM(REFERENC)) FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA='" + Variavel.codLoja + "' And (PVP > 0 Or (PVP < 0 And IsNull(PROMOCAO,'N')='S')) And ACTIVO = 'S' And CODARMZ IN (SELECT CODARMZ FROM ASARMAZ WHERE CODLOJA = '" + Variavel.codLoja + "' AND UPPER(ARMA_VENDA)='S') ORDER BY RTRIM(LTRIM(REFERENC))");
                cmd2.Connection = conexao;
                try
                {
                    conexao.Open();
                    SqlDataReader leitor = cmd2.ExecuteReader();
                    while (leitor.Read())
                    {
                        cmbReferencia.Items.Add(leitor.GetValue(0));
                    }
                }
                catch (SqlException ex) { MessageBox.Show(ex.Message); }
                finally { conexao.Close(); }

                SqlCommand cmd3 = new SqlCommand(@"SELECT DISTINCT RTRIM(LTRIM(DESCRARTIGO)) FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA='" + Variavel.codLoja + "' And (PVP > 0 Or (PVP < 0 And IsNull(PROMOCAO,'N')='S')) And ACTIVO = 'S' And CODARMZ IN (SELECT CODARMZ FROM ASARMAZ WHERE CODLOJA = '" + Variavel.codLoja + "' AND UPPER(ARMA_VENDA)='S') ORDER BY RTRIM(LTRIM(DESCRARTIGO))");
                cmd3.Connection = conexao;
                try
                {
                    conexao.Open();
                    SqlDataReader leitor = cmd3.ExecuteReader();
                    while (leitor.Read())
                    {
                        cmbDesignacao.Items.Add(leitor.GetValue(0));
                    }
                }
                catch (SqlException ex) { MessageBox.Show(ex.Message); }
                finally { conexao.Close(); } //*/
            }

            if (tipoDoc == "GR")
            {
                lbArea.Visible = true;
                cmbArea.Visible = true;
                SqlCommand cmd3 = new SqlCommand(@"Select NomeAO from AIAREAORGANICA WITH (NOLOCK) WHERE ISNULL(TO_DISPLAY,'S') ='S'");
                cmd3.Connection = conexao;
                try
                {
                    conexao.Open();
                    SqlDataReader leitor = cmd3.ExecuteReader();
                    while (leitor.Read())
                    {
                        cmbArea.Items.Add(leitor.GetValue(0));
                    }
                }
                catch (SqlException ex) { MessageBox.Show(ex.Message); }
                finally { conexao.Close(); }
            }
            else
            {
                lbArea.Visible = false;
                cmbArea.Visible = false;
            }
        }

        private void cmbTipo_MouseClick(object sender, MouseEventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;
            cmbTipoDocumento();
        }

        private void cmbReferencia_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            string select;
            int count = 0, result = 1;
            FormVendasArmazem form = new FormVendasArmazem();
            referencia = cmbReferencia.Text;
            SqlCommand comd = new SqlCommand("SELECT CodArmz FROM ASMESTRE WITH (NOLOCK) where REFERENC = '" + cmbReferencia.Text + "' and CodLoja = '" + Variavel.codLoja + "' and QTD > 0", conexao);
            conexao.Open();
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { count = count + 1; } }
            conexao.Close();
            try
            {
                if (count > 1)
                {
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        select = "SELECT FOB, PCA, PVP, QTD, LOCALI, CodArmz, CodFam, CodSubFam, OBS, Desconto, RTRIM(LTRIM(DescrArtigo)), ImpostoConsumo, HotelBooKing, Ginasio, CodMarca, CodModelo, RegEquip, CONTASTOCKS FROM ASMESTRE WITH (NOLOCK) where REFERENC = '" + cmbReferencia.Text + "' AND CodArmz = '" + CodArmz + "'";
                        SqlCommand cmd = new SqlCommand(select, conexao);
                        conexao.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                FOB = reader[0].ToString();
                                PCA = reader[1].ToString();
                                PVP = reader[2].ToString();
                                STOCKTOTAL = reader[3].ToString();
                                LOCALI = reader[4].ToString();
                                CodArmz = reader[5].ToString();
                                CodFam = reader[6].ToString();
                                CodSubFam = reader[7].ToString();
                                OBS = reader[8].ToString();
                                Desconto = reader[9].ToString();
                                Descricao = reader[10].ToString();
                                impostoConsumo = reader[11].ToString();
                                strHotelBooKing = reader[12].ToString();
                                strGinasio = reader[13].ToString();
                                codMarca = reader[14].ToString();
                                codModelo = reader[15].ToString();
                                RegEquip = reader[16].ToString();
                                sContaStocks = reader[17].ToString();
                            }
                        }
                    }
                    else
                        result = 0;
                }
                else
                {
                    select = "SELECT FOB, PCA, PVP, QTD, LOCALI, CodArmz, CodFam, CodSubFam, OBS, Desconto, RTRIM(LTRIM(DescrArtigo)), ImpostoConsumo, HotelBooKing, Ginasio, CodMarca, CodModelo, RegEquip, CONTASTOCKS FROM ASMESTRE WITH (NOLOCK) where REFERENC = '" + cmbReferencia.Text + "'";
                    SqlCommand cmd = new SqlCommand(select, conexao);
                    conexao.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FOB = reader[0].ToString();
                            PCA = reader[1].ToString();
                            PVP = reader[2].ToString();
                            STOCKTOTAL = reader[3].ToString();
                            LOCALI = reader[4].ToString();
                            CodArmz = reader[5].ToString();
                            CodFam = reader[6].ToString();
                            CodSubFam = reader[7].ToString();
                            OBS = reader[8].ToString();
                            Desconto = reader[9].ToString();
                            Descricao = reader[10].ToString();
                            impostoConsumo = reader[11].ToString();
                            strHotelBooKing = reader[12].ToString();
                            strGinasio = reader[13].ToString();
                            codMarca = reader[14].ToString();
                            codModelo = reader[15].ToString();
                            RegEquip = reader[16].ToString();
                            sContaStocks = reader[17].ToString();
                        }
                    }
                }
            }
            catch (Exception) { }
            finally
            {
                conexao.Close();
                if (result == 1)
                {
                    if (Convert.ToInt32(STOCKTOTAL) > 0)
                    {
                        prUnitario = "" + Convert.ToDouble(Variavel.cambio) * Convert.ToDouble(PVP);
                        cmbDesignacao.Text = Descricao;
                        lbStock.Text = STOCKTOTAL;
                        lbPrUnitario.Text = prUnitario;
                        lbCons.Text = impostoConsumo;
                        txtQtd.Text = Qtd = "1";
                        lbTotal.Text = Total = "" + Convert.ToInt32(txtQtd.Text) * Convert.ToDouble(prUnitario);
                        if (Desconto == null || Desconto == "")
                            Desconto = "0";
                        lbDesc.Text = "" + (Convert.ToDouble(Desconto) + Convert.ToDouble(lbDesconto.Text));
                    }
                    else
                        MessageBox.Show("STOCK VASIO");
                }
            }
        }

        private void cmbDesignacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            int count = 0, result = 1;
            Descricao = cmbDesignacao.Text;
            SqlCommand comd = new SqlCommand("SELECT CodArmz FROM ASMESTRE WITH (NOLOCK) where DESCRARTIGO = '" + cmbDesignacao.Text + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { count = count + 1; } }
            conexao.Close();
            try
            {
                if (count > 1)
                {
                    FormVendasArmazem form = new FormVendasArmazem();
                    if (form.ShowDialog() == DialogResult.OK)
                    {
                        SqlCommand cmd = new SqlCommand("SELECT FOB, PCA, PVP, QTD, LOCALI, CodArmz, CodFam, CodSubFam, OBS, Desconto, RTRIM(LTRIM(REFERENC)), ImpostoConsumo, HotelBooKing, Ginasio, CodMarca, CodModelo, RegEquip, CONTASTOCKS FROM ASMESTRE WITH (NOLOCK) where DESCRARTIGO = '" + cmbDesignacao.Text + "' and CodArmz = '" + CodArmz + "'", conexao);
                        conexao.Open();
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                FOB = reader[0].ToString();
                                PCA = reader[1].ToString();
                                PVP = reader[2].ToString();
                                STOCKTOTAL = reader[3].ToString();
                                LOCALI = reader[4].ToString();
                                CodArmz = reader[5].ToString();
                                CodFam = reader[6].ToString();
                                CodSubFam = reader[7].ToString();
                                OBS = reader[8].ToString();
                                Desconto = reader[9].ToString();
                                referencia = reader[10].ToString();
                                impostoConsumo = reader[11].ToString();
                                strHotelBooKing = reader[12].ToString();
                                strGinasio = reader[13].ToString();
                                codMarca = reader[14].ToString();
                                codModelo = reader[15].ToString();
                                RegEquip = reader[16].ToString();
                                sContaStocks = reader[17].ToString();
                            }
                        }
                    }
                    else
                        result = 0;
                }
                else
                {
                    SqlCommand cmd = new SqlCommand("SELECT FOB, PCA, PVP, QTD, LOCALI, CodArmz, CodFam, CodSubFam, OBS, Desconto, RTRIM(LTRIM(REFERENC)), ImpostoConsumo, HotelBooKing, Ginasio, CodMarca, CodModelo, RegEquip, CONTASTOCKS FROM ASMESTRE WITH (NOLOCK) where DESCRARTIGO = '" + cmbDesignacao.Text + "'", conexao);
                    conexao.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            FOB = reader[0].ToString();
                            PCA = reader[1].ToString();
                            PVP = reader[2].ToString();
                            STOCKTOTAL = reader[3].ToString();
                            LOCALI = reader[4].ToString();
                            CodArmz = reader[5].ToString();
                            CodFam = reader[6].ToString();
                            CodSubFam = reader[7].ToString();
                            OBS = reader[8].ToString();
                            Desconto = reader[9].ToString();
                            referencia = reader[10].ToString();
                            impostoConsumo = reader[11].ToString();
                            strHotelBooKing = reader[12].ToString();
                            strGinasio = reader[13].ToString();
                            codMarca = reader[14].ToString();
                            codModelo = reader[15].ToString();
                            RegEquip = reader[16].ToString();
                            sContaStocks = reader[17].ToString();
                        }
                    }
                }
            }
            catch (Exception) { }
            finally
            {
                conexao.Close();
                if (result == 1)
                {
                    if (Convert.ToInt32(STOCKTOTAL) > 0)
                    {
                        prUnitario = "" + Convert.ToDouble(Variavel.cambio) * Convert.ToDouble(PVP);
                        cmbDesignacao.Text = Descricao;
                        lbStock.Text = STOCKTOTAL;
                        lbPrUnitario.Text = prUnitario;
                        lbCons.Text = impostoConsumo;
                        cmbReferencia.Text = referencia;
                        txtQtd.Text = Qtd = "1";
                        lbTotal.Text = Total = "" + Convert.ToInt32(txtQtd.Text) * Convert.ToDouble(prUnitario);
                        try { lbDesc.Text = "" + (Convert.ToDouble(Desconto) + Convert.ToDouble(lbDesconto.Text)); }
                        catch (Exception) { lbDesc.Text = "0"; }
                    }
                    else
                        MessageBox.Show("STOCK VASIO");
                }
            }
        }
        private void btAtualizar_Click(object sender, EventArgs e)
        {
            selecionarNomeCliente(txtCodigo.Text);
        }

        private void cmbNome_DropDown(object sender, EventArgs e)
        {
            if (cmbNome.Items.Count <= 0)
            {
                Cursor.Current = Cursors.WaitCursor;
                cmbNomeCliente();
            }
        }

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            FormVendaEntrega form = new FormVendaEntrega();
            if (form.ShowDialog() == DialogResult.OK)
            {
                bEntrega = true;
            }
        }

        private void FormVendas_Leave(object sender, EventArgs e)
        {
            limparCampos();
        }
    }
}
