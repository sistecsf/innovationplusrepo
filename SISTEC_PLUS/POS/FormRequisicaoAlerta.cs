﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using System.Timers;

namespace SISTEC_PLUS.POS
{
    public partial class FormRequisicaoAlerta : MetroFramework.Forms.MetroForm
    {
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        int count = 0;
        public FormRequisicaoAlerta()
        {
            InitializeComponent();
        }
        private void preencherGrid()
        {
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(@"Select Descricao From ALERTAS WITH (NOLOCK) Where  Convert(varchar(10),Datacria,120)=Convert(varchar(10), GetDate(),120) and CodLoja= '"+ FormRequisicaoInterna.codLojaRI +"'", conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }

        private void FormRequisicaoImportar_Load(object sender, EventArgs e)
        {
            metroGrid1.AutoGenerateColumns = false;
            preencherGrid();
            timer1.Start();
        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            count += 1;
            if(count == 5)
                this.DialogResult = DialogResult.OK;
        }
    }
}
