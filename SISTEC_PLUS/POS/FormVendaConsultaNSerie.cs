﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendaConsultaNSerie : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        string aaa;
        public FormVendaConsultaNSerie()
        {
            InitializeComponent();
        }
        private void consultar()
        {
            try
            {
                SqlCommand cmd1 = new SqlCommand("Select LTRIM(RTRIM(MA.DescrMarca)) MAR, LTRIM(RTRIM(MO.DescrModelo)) MOD, LTrim(RTrim(C.CODENTIDADE)) + ' - ' + LTrim(RTrim(C.NOMEENTIDADE)) CLI, REFERENC, VENDIDO = CASE RTRIM(LTRIM(SAIU)) WHEN 'S' Then 'VENDIDO' ELSE 'DISPONÍVEL' END, IDUNICO_SAIDA, Data_Instalacao, NomeLoja, NomeArz From AEFICHAS F WITH (NOLOCK), AEMODELO MO WITH (NOLOCK), AEMARCA MA WITH (NOLOCK), AFENTIDADE C WITH (NOLOCK), ASLOJA LJ WITH (NOLOCK), ASARMAZ AZ WITH (NOLOCK)" +
                    "Where NSERIE = '" + txtNSeria.Text + "' And F.CodModelo=MO.codmodelo And F.CodMarca=MA.codmarca And F.CodEntidade=C.CodEntidade And F.CodEntidade=C.CodEntidade And F.CodLoja=LJ.CodLoja And  LJ.CodLoja=AZ.CodLoja And F.CODARMZ=AZ.CODARMZ", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        lbMarca.Text = reader[0].ToString();
                        lbModelo.Text = reader[1].ToString();
                        lbCliente.Text = reader[2].ToString();
                        lbReferencia.Text = reader[3].ToString();
                        lbEstado.Text = reader[4].ToString();
                        lbID.Text = reader[5].ToString();
                        lbData.Text = reader[6].ToString();
                        lbLoja.Text = reader[7].ToString();
                        lbMercadoria.Text = reader[8].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }
        private void btConsultar_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            consultar();
        }

        private void txtDesconto2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                Cursor.Current = Cursors.WaitCursor;
                consultar();
            }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
