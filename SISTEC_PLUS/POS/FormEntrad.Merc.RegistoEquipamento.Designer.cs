﻿namespace SISTEC_PLUS.POS
{
    partial class FormEntrad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btOk = new System.Windows.Forms.ToolStripButton();
            this.btExcluir = new System.Windows.Forms.ToolStripButton();
            this.btSair = new System.Windows.Forms.ToolStripButton();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.importar = new MetroFramework.Controls.MetroButton();
            this.RadioButtonCartaoSim = new MetroFramework.Controls.MetroRadioButton();
            this.txtImei = new MetroFramework.Controls.MetroTextBox();
            this.RadioButtonTelefone = new MetroFramework.Controls.MetroRadioButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtNumSer = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtCustoFob = new MetroFramework.Controls.MetroTextBox();
            this.txtQtdRE = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.txtTotalItens = new MetroFramework.Controls.MetroTextBox();
            this.txtMarca = new MetroFramework.Controls.MetroTextBox();
            this.txtDescricao = new MetroFramework.Controls.MetroTextBox();
            this.txtCodigoModelo = new MetroFramework.Controls.MetroTextBox();
            this.txtCodMarca = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.toolStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btOk,
            this.btExcluir,
            this.btSair});
            this.toolStrip1.Location = new System.Drawing.Point(366, 70);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(104, 25);
            this.toolStrip1.TabIndex = 14;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btOk
            // 
            this.btOk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btOk.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(26, 22);
            this.btOk.Text = "Ok";
            this.btOk.Click += new System.EventHandler(this.btOk_Click);
            // 
            // btExcluir
            // 
            this.btExcluir.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btExcluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btExcluir.Name = "btExcluir";
            this.btExcluir.Size = new System.Drawing.Size(45, 22);
            this.btExcluir.Text = "Excluir";
            // 
            // btSair
            // 
            this.btSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(30, 22);
            this.btSair.Text = "Sair";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(72, 160);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(65, 19);
            this.metroLabel5.TabIndex = 117;
            this.metroLabel5.Text = "Descrição";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(12, 53);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(79, 19);
            this.metroLabel7.TabIndex = 116;
            this.metroLabel7.Text = "IMEI / ICCID";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(72, 125);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(46, 19);
            this.metroLabel4.TabIndex = 115;
            this.metroLabel4.Text = "Marca";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(143, 131);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 13);
            this.label4.TabIndex = 120;
            this.label4.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(143, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 13);
            this.label1.TabIndex = 121;
            this.label1.Text = "*";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.importar);
            this.groupBox1.Controls.Add(this.RadioButtonCartaoSim);
            this.groupBox1.Controls.Add(this.txtImei);
            this.groupBox1.Controls.Add(this.RadioButtonTelefone);
            this.groupBox1.Controls.Add(this.metroButton1);
            this.groupBox1.Controls.Add(this.metroLabel7);
            this.groupBox1.Controls.Add(this.metroButton3);
            this.groupBox1.Controls.Add(this.metroLabel6);
            this.groupBox1.Controls.Add(this.txtNumSer);
            this.groupBox1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.groupBox1.Location = new System.Drawing.Point(66, 248);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(726, 123);
            this.groupBox1.TabIndex = 123;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Agentes Telecomunicações";
            // 
            // importar
            // 
            this.importar.Location = new System.Drawing.Point(316, 93);
            this.importar.Name = "importar";
            this.importar.Size = new System.Drawing.Size(75, 24);
            this.importar.TabIndex = 146;
            this.importar.Text = "Importar ";
            this.importar.UseSelectable = true;
            this.importar.Click += new System.EventHandler(this.importar_Click);
            // 
            // RadioButtonCartaoSim
            // 
            this.RadioButtonCartaoSim.AutoSize = true;
            this.RadioButtonCartaoSim.Location = new System.Drawing.Point(403, 19);
            this.RadioButtonCartaoSim.Name = "RadioButtonCartaoSim";
            this.RadioButtonCartaoSim.Size = new System.Drawing.Size(81, 15);
            this.RadioButtonCartaoSim.TabIndex = 145;
            this.RadioButtonCartaoSim.Text = "Cartão Sim";
            this.RadioButtonCartaoSim.UseSelectable = true;
            this.RadioButtonCartaoSim.CheckedChanged += new System.EventHandler(this.RadioButtonCartaoSim_CheckedChanged);
            // 
            // txtImei
            // 
            this.txtImei.Lines = new string[0];
            this.txtImei.Location = new System.Drawing.Point(97, 54);
            this.txtImei.MaxLength = 32767;
            this.txtImei.Name = "txtImei";
            this.txtImei.PasswordChar = '\0';
            this.txtImei.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtImei.SelectedText = "";
            this.txtImei.Size = new System.Drawing.Size(183, 23);
            this.txtImei.TabIndex = 139;
            this.txtImei.UseSelectable = true;
            this.txtImei.Click += new System.EventHandler(this.txtImei_Click);
            // 
            // RadioButtonTelefone
            // 
            this.RadioButtonTelefone.AutoSize = true;
            this.RadioButtonTelefone.Location = new System.Drawing.Point(243, 19);
            this.RadioButtonTelefone.Name = "RadioButtonTelefone";
            this.RadioButtonTelefone.Size = new System.Drawing.Size(69, 15);
            this.RadioButtonTelefone.TabIndex = 144;
            this.RadioButtonTelefone.Text = "Telefone";
            this.RadioButtonTelefone.UseSelectable = true;
            this.RadioButtonTelefone.CheckedChanged += new System.EventHandler(this.RadioButtonTelefone_CheckedChanged);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(316, 53);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(75, 24);
            this.metroButton1.TabIndex = 127;
            this.metroButton1.Text = "Importar";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.Location = new System.Drawing.Point(422, 93);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(75, 24);
            this.metroButton3.TabIndex = 133;
            this.metroButton3.Text = "Adicionar";
            this.metroButton3.UseSelectable = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(12, 91);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(77, 19);
            this.metroLabel6.TabIndex = 128;
            this.metroLabel6.Text = "Nº de Serie";
            // 
            // txtNumSer
            // 
            this.txtNumSer.Lines = new string[0];
            this.txtNumSer.Location = new System.Drawing.Point(97, 93);
            this.txtNumSer.MaxLength = 32767;
            this.txtNumSer.Name = "txtNumSer";
            this.txtNumSer.PasswordChar = '\0';
            this.txtNumSer.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNumSer.SelectedText = "";
            this.txtNumSer.Size = new System.Drawing.Size(183, 23);
            this.txtNumSer.TabIndex = 129;
            this.txtNumSer.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(206, 202);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(69, 19);
            this.metroLabel1.TabIndex = 128;
            this.metroLabel1.Text = "Custo Fob";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(586, 198);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(35, 19);
            this.metroLabel2.TabIndex = 129;
            this.metroLabel2.Text = "QTD";
            // 
            // txtCustoFob
            // 
            this.txtCustoFob.Lines = new string[0];
            this.txtCustoFob.Location = new System.Drawing.Point(281, 202);
            this.txtCustoFob.MaxLength = 32767;
            this.txtCustoFob.Name = "txtCustoFob";
            this.txtCustoFob.PasswordChar = '\0';
            this.txtCustoFob.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCustoFob.SelectedText = "";
            this.txtCustoFob.Size = new System.Drawing.Size(65, 23);
            this.txtCustoFob.TabIndex = 128;
            this.txtCustoFob.UseSelectable = true;
            // 
            // txtQtdRE
            // 
            this.txtQtdRE.Lines = new string[0];
            this.txtQtdRE.Location = new System.Drawing.Point(627, 198);
            this.txtQtdRE.MaxLength = 32767;
            this.txtQtdRE.Name = "txtQtdRE";
            this.txtQtdRE.PasswordChar = '\0';
            this.txtQtdRE.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtQtdRE.SelectedText = "";
            this.txtQtdRE.Size = new System.Drawing.Size(65, 23);
            this.txtQtdRE.TabIndex = 131;
            this.txtQtdRE.UseSelectable = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(66, 556);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(71, 19);
            this.metroLabel8.TabIndex = 135;
            this.metroLabel8.Text = "Total Items";
            // 
            // txtTotalItens
            // 
            this.txtTotalItens.Lines = new string[0];
            this.txtTotalItens.Location = new System.Drawing.Point(154, 556);
            this.txtTotalItens.MaxLength = 32767;
            this.txtTotalItens.Name = "txtTotalItens";
            this.txtTotalItens.PasswordChar = '\0';
            this.txtTotalItens.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTotalItens.SelectedText = "";
            this.txtTotalItens.Size = new System.Drawing.Size(65, 23);
            this.txtTotalItens.TabIndex = 136;
            this.txtTotalItens.UseSelectable = true;
            // 
            // txtMarca
            // 
            this.txtMarca.Lines = new string[0];
            this.txtMarca.Location = new System.Drawing.Point(163, 121);
            this.txtMarca.MaxLength = 32767;
            this.txtMarca.Name = "txtMarca";
            this.txtMarca.PasswordChar = '\0';
            this.txtMarca.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtMarca.SelectedText = "";
            this.txtMarca.Size = new System.Drawing.Size(183, 23);
            this.txtMarca.TabIndex = 137;
            this.txtMarca.UseSelectable = true;
            // 
            // txtDescricao
            // 
            this.txtDescricao.Lines = new string[0];
            this.txtDescricao.Location = new System.Drawing.Point(163, 160);
            this.txtDescricao.MaxLength = 32767;
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.PasswordChar = '\0';
            this.txtDescricao.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDescricao.SelectedText = "";
            this.txtDescricao.Size = new System.Drawing.Size(183, 23);
            this.txtDescricao.TabIndex = 138;
            this.txtDescricao.UseSelectable = true;
            // 
            // txtCodigoModelo
            // 
            this.txtCodigoModelo.Lines = new string[0];
            this.txtCodigoModelo.Location = new System.Drawing.Point(627, 156);
            this.txtCodigoModelo.MaxLength = 32767;
            this.txtCodigoModelo.Name = "txtCodigoModelo";
            this.txtCodigoModelo.PasswordChar = '\0';
            this.txtCodigoModelo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCodigoModelo.SelectedText = "";
            this.txtCodigoModelo.Size = new System.Drawing.Size(65, 23);
            this.txtCodigoModelo.TabIndex = 139;
            this.txtCodigoModelo.UseSelectable = true;
            // 
            // txtCodMarca
            // 
            this.txtCodMarca.BackColor = System.Drawing.Color.White;
            this.txtCodMarca.Lines = new string[0];
            this.txtCodMarca.Location = new System.Drawing.Point(627, 121);
            this.txtCodMarca.MaxLength = 32767;
            this.txtCodMarca.Name = "txtCodMarca";
            this.txtCodMarca.PasswordChar = '\0';
            this.txtCodMarca.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCodMarca.SelectedText = "";
            this.txtCodMarca.Size = new System.Drawing.Size(65, 23);
            this.txtCodMarca.TabIndex = 140;
            this.txtCodMarca.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(508, 121);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(113, 19);
            this.metroLabel3.TabIndex = 141;
            this.metroLabel3.Text = "Codigo da marca";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(490, 156);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(131, 19);
            this.metroLabel9.TabIndex = 142;
            this.metroLabel9.Text = "Codigo da descrição";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "TEL. OU SIM";
            this.Column6.Name = "Column6";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "REFERÊNCIA";
            this.Column5.Name = "Column5";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "MODELO";
            this.Column4.Name = "Column4";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "MARCA";
            this.Column3.Name = "Column3";
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "DOC";
            this.Column2.HeaderText = "IMEI / ICCID";
            this.Column2.Name = "Column2";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "F1";
            this.Column1.HeaderText = "Nº Serie";
            this.Column1.Name = "Column1";
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle5;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(111, 389);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(641, 161);
            this.metroGrid1.TabIndex = 134;
            // 
            // FormEntrad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 602);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.txtCodMarca);
            this.Controls.Add(this.txtCodigoModelo);
            this.Controls.Add(this.txtDescricao);
            this.Controls.Add(this.txtMarca);
            this.Controls.Add(this.txtTotalItens);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroGrid1);
            this.Controls.Add(this.txtQtdRE);
            this.Controls.Add(this.txtCustoFob);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.toolStrip1);
            this.Name = "FormEntrad";
            this.Text = "Registo de Equipamentos ";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormEntrad_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btOk;
        private System.Windows.Forms.ToolStripButton btExcluir;
        private System.Windows.Forms.ToolStripButton btSair;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtCustoFob;
        private MetroFramework.Controls.MetroTextBox txtQtdRE;
        private MetroFramework.Controls.MetroTextBox txtNumSer;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox txtTotalItens;
        private MetroFramework.Controls.MetroTextBox txtMarca;
        private MetroFramework.Controls.MetroTextBox txtDescricao;
        private MetroFramework.Controls.MetroTextBox txtImei;
        private MetroFramework.Controls.MetroTextBox txtCodigoModelo;
        private MetroFramework.Controls.MetroTextBox txtCodMarca;
        private MetroFramework.Controls.MetroRadioButton RadioButtonTelefone;
        private MetroFramework.Controls.MetroRadioButton RadioButtonCartaoSim;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroButton importar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private MetroFramework.Controls.MetroGrid metroGrid1;
    }
}