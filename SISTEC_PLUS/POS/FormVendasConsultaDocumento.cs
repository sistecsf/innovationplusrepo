﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendasConsultaDocumento : MetroFramework.Forms.MetroForm
    {
        public static string dataIniacial, dataFim, sql, tipo, codLoja;
        DateTime data;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public FormVendasConsultaDocumento()
        {
            InitializeComponent();
        }
        private void prenxerGrid()
        {
            sql = @"SELECT DISTINCT CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105), R.coddoc, D.Descrdoc AS TIPODOC, CAST(R.numdoc AS INT) AS NUMDOC, LTrim(RTrim(C.CodEntidade)) + ' - ' +" +
                              "LTrim(RTrim(C.NomeEntidade)) AS CLIENTE, STATUS = CASE R.Status WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO' WHEN 'F' THEN 'PAG. PARCIAL' WHEN 'J' THEN 'ADJUDICADO' WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END," +
                                "RTRIM(R.IDunico), U.NOMEUTILIZADOR, PRECOD FROM AFENTIDADE C WITH (NOLOCK), ASDOCS D WITH (NOLOCK), ASFICMOV1 F WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), AREGDOC R WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK) " +
                                "WHERE R.IDunico = F.IDunico AND R.CodEntidade = C.CodEntidade and D.Coddoc = R.Coddoc AND M.codarmz = F.codarmz AND M.referenc = F.referenc AND M.codloja = F.codloja AND UPPER(D.TipoPos) = 'S' AND F.CODUTILIZADOR = U.CODUTILIZADOR ";
            if(Convert.ToInt32(Permissoes.nPrgPos) < 3)
                sql = sql + "AND R.codutilizador = '"+Variavel.codUtilizador+"'";
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                sql = sql + "AND CONVERT(DATETIME, R.DataCria, 120) > CONVERT(DATETIME, '" + dataIniacial + "', 120) AND CONVERT(DATETIME, R.DataCria, 120) <= CONVERT(DATETIME, '" + dataFim + " 18:59" + "', 120)";
            if (txtNumero.Text != "")
                sql = sql + " AND C.CodEntidade LIKE '"+ txtNumero.Text +"%'";
            if (cmbLoja.Text != "" || cmbLoja.PromptText != "")
                sql = sql + " AND R.codloja = '"+ codLoja +"'";
            if(txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '"+ txtNome.Text +"%'";
            if(txtDoc.Text != "")
                sql = sql + " AND R.coddoc LIKE '"+ txtDoc.Text +"%'";
            if(txtDescDoc.Text != "")
                sql = sql + " AND D.descrdoc LIKE '"+ txtDescDoc.Text +"%'";
            if(txtNrDoc.Text != "")
                sql = sql + " AND R.numdoc = '"+ txtNrDoc.Text +"'";
            if(txtCodArtigo.Text != "")
                sql = sql + " And F.Referenc Like '%"+ txtCodArtigo.Text +"%'";
            if(txtDescArtigo.Text != "")
                sql = sql + " AND M.DescrArtigo LIKE '%"+ txtDescArtigo.Text +"%' ";
            sql = sql + " ORDER BY CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME),105), R.coddoc, NUMDOC, CLIENTE, TIPODOC";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql , conexao);
                dataAdapter.SelectCommand.CommandTimeout = 90000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void prenxerCmbLoja()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeLoja from ASLOJA with (NoLock) order by NomeLoja");
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbLoja.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void gridDuploClick(DataGridView dataGrid)
        {
            FormVendas.data = dataGrid.CurrentRow.Cells[0].Value.ToString();
            FormVendas.codDocumento = dataGrid.CurrentRow.Cells[1].Value.ToString();
            FormVendas.tipoDocumento = dataGrid.CurrentRow.Cells[2].Value.ToString();
            FormVendas.numeroDocumento = dataGrid.CurrentRow.Cells[3].Value.ToString();
            FormVendas.idunico = dataGrid.CurrentRow.Cells[6].Value.ToString();
            FormVendas.utilizador = dataGrid.CurrentRow.Cells[7].Value.ToString();
            FormVendas.valorUDS = dataGrid.CurrentRow.Cells[8].Value.ToString();

            try
            {
                SqlCommand cmd = new SqlCommand("select D.CodEntidade, E.NomeEntidade, A.UniMonet from AREGDOC D WITH (NOLOCK), AFENTIDADE E WITH (NOLOCK), ASDOCS A WITH (NOLOCK) where D.IDUNICO = '" + FormVendas.idunico + "' AND E.CodEntidade = D.CodEntidade AND A.CodDoc = D.CodDoc", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        FormVendas.codCliente = reader[0].ToString();
                        FormVendas.nomeCliente = reader[1].ToString();
                        FormVendas.moeda = reader[2].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }

            this.DialogResult = DialogResult.OK;
        }

        private void FormVendasConsultaDocumento_Load(object sender, EventArgs e)
        {
            metroGrid1.AutoGenerateColumns = false;
            prenxerGrid();
            txtDataInicial.Text = txtDataFinal.Text = DateTime.Now.ToShortDateString();
            prenxerCmbLoja();
            cmbLoja.PromptText = Variavel.nomeLoja;
            codLoja = Variavel.codLoja;
        }
        private void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                data = Convert.ToDateTime(txtDataInicial.Text);
                dataIniacial = "" + data.Year + "-" + data.Month + "-" + data.Day;
                try
                {
                    data = Convert.ToDateTime(txtDataFinal.Text.Trim());
                    dataFim = "" + data.Year + "-" + data.Month + "-" + data.Day;

                    metroGrid1.AutoGenerateColumns = false;
                    Cursor.Current = Cursors.WaitCursor;
                    prenxerGrid();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message + " Data Invalida " + txtDataFinal.Text); }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message + " Data Invalida " + txtDataInicial.Text); }
        }

        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    try { gridDuploClick(metroGrid1); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
        }

        private void cmbLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("select CodLoja from ASLOJA with (nolock) where NomeLoja = '" + cmbLoja.Text + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codLoja = reader[0].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try { gridDuploClick(metroGrid1); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void txtDataInicial_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception ex)
            {
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!");
                txtDataInicial.Text = "";
            }
        }

        private void txtDataFinal_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception ex)
            {
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!");
                txtDataFinal.Text = "";
            }
        }
    }
}
