﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormRequisicaoAtendimento : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        public static string dataIniacial, dataFim, sql, sCodLoja, idunico, codDoc, numeroDoc, operador, data1;
        DateTime data;
        public FormRequisicaoAtendimento()
        {
            InitializeComponent();
        }
        private void prenxerGrid()
        {
            metroGrid1.DataSource = null;
            metroGrid1.Rows.Clear();
            sql = "Select Distinct Convert(Varchar(10), Cast(R.DataCria As DateTime),105), Cast(R.NumDoc As Int) As NUMDOC, LTrim(RTrim(R.CODDOC)), LTrim(RTrim(R.IDunico)), DESTINO = CASE WHEN Localizacao_Fisica='EXTERNA' THEN LTrim(RTrim(C.CodEntidade)) + ' - ' + LTrim(RTrim(C.NomeEntidade)) WHEN LOJADESTINO IS NOT NULL THEN LTrim(RTrim(LojaDestino)) + ' - ' + LTrim(RTrim(NomeLoja)) ELSE '' END, U.NomeUtilizador, R.DataCria " +
                "From AFENTIDADE C With (NoLock), ASDOCS D With (NoLock), ASFICMOV1 F With (NoLock) LEFT OUTER JOIN ASLOJA L With(NoLock)ON LojaDestino = L.CODLOJA, AREGDOC R With (NoLock), UTILIZADORES U With (NoLock) Where R.CodLoja = 'INF' And F.CodLoja=R.CodLoja And R.IDunico = F.IDunico And R.CodEntidade = C.CodEntidade And D.Coddoc = R.Coddoc And R.Coddoc = F.Coddoc and R.CODDOC= 'RI' and F.CodDoc = 'RI' And F.CODUTILIZADOR = U.CODUTILIZADOR And (R.STATUS<>'A' And R.STATUS<>'C') And QTD>=IsNull(QTD_ATENDIDA,0)  ";
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
            {
                sql = sql + "And LEFT(R.DataCria, 10) >=  '" + dataIniacial + "' And LEFT(R.DataCria, 10) <= '" + dataFim + "' ";
            }
            sql = sql + "Union Select DISTINCT Convert(Varchar(10), Cast(R.DataCria As DateTime),105), Cast(R.NumDoc As Int) As NUMDOC, LTrim(RTrim(R.CODDOC)), RTrim(R.IDunico), LTrim(RTrim(R.CodEntidade)) + ' - ' + LTrim(RTrim(C.NomeEntidade)) AS CLIENTE, U.NomeUtilizador, R.DataCria From AFENTIDADE C With (NoLock), ASENTREGA AT With (NoLock), AREGDOC R With (NoLock), ASDOCS D With (NoLock), UTILIZADORES U With (NoLock) " +
                "Where AT.IDunico=R.IDunico And R.CodEntidade = C.CodEntidade And R.CodLoja = 'INF' And R.CodDoc=D.CodDoc And (D.TIPOUTIL = 'S' And D.TIPOPOS = 'S' And D.ALTSTOCK = 'S') And AT.Status='E' And R.Status<>'A' And AT.CODUTILIZADOR = U.CODUTILIZADOR and R.CODDOC= 'RI' ";
            if (Convert.ToInt32(Permissoes.nPrgPos) < 3)
            {
                sql = sql + "And R.CodUtilizador = '" + Variavel.codUtilizador + "' ";
            }
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
            {
                sql = sql + "And LEFT(R.DataCria, 10) >=  '" + dataIniacial + "' And LEFT(R.DataCria, 10) <= '" + dataFim + "' ";
            }
            if(txtNumero.Text != "")
                sql = sql + "AND C.CodEntidade = '"+ txtNumero.Text +"' ";
            if (cmbLoja.Text != "" || cmbLoja.PromptText != "")
                sql = sql + "AND R.codloja = '"+ sCodLoja +"' ";
            if(txtNome.Text !=  "")
                sql = sql + "And C.NomeEntidade = '"+ txtNome.Text +"' ";
            if (txtNumero2.Text != "")
                sql = sql + "And R.NumDoc = '" + txtNumero2.Text + "' ";
            sql = sql + "Order By R.DataCria";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataAdapter.SelectCommand.CommandTimeout = 90000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
                conexao.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void prenxerCmbLoja()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeLoja from ASLOJA with (NoLock) order by NomeLoja");
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbLoja.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void gridDuploClick(DataGridView dataGrid)
        {
            data1 = dataGrid.CurrentRow.Cells[0].Value.ToString();
            codDoc = dataGrid.CurrentRow.Cells[1].Value.ToString();
            numeroDoc = dataGrid.CurrentRow.Cells[2].Value.ToString();
            idunico = dataGrid.CurrentRow.Cells[3].Value.ToString();
            operador = dataGrid.CurrentRow.Cells[5].Value.ToString();

            FormRequisicaoAtend form = new FormRequisicaoAtend();
            form.ShowDialog();
        }

        private void FormRequisicaoAtendimento_Load(object sender, EventArgs e)
        {
            txtDataInicial.Text = txtDataFinal.Text = DateTime.Now.ToShortDateString();
            sCodLoja = FormRequisicaoInterna.codLojaRI;
            cmbLoja.PromptText = Variavel.nomeLoja;
            metroGrid1.AutoGenerateColumns = false;
            prenxerGrid();
        }
        private void btGravar_Click(object sender, EventArgs e)
        {
            data = Convert.ToDateTime(txtDataInicial.Text);
            if (data.Month < 10) dataIniacial = data.Year + "-0" + data.Month;
            else dataIniacial = data.Year + "-" + data.Month;
            if (data.Day < 10) dataIniacial = dataIniacial + "-0" + data.Day;
            else dataIniacial = dataIniacial + "-" + data.Day;
            data = Convert.ToDateTime(txtDataFinal.Text);
            if (data.Month < 10) dataFim = data.Year + "-0" + data.Month;
            else dataFim = data.Year + "-" + data.Month;
            if (data.Day < 10) dataFim = dataFim + "-0" + data.Day;
            else dataFim = dataFim + "-" + data.Day;
            Cursor.Current = Cursors.WaitCursor;
            prenxerGrid();
            /*try
            {
                data = Convert.ToDateTime(txtDataInicial.Text);
                dataIniacial = "" + data.Year + "-" + data.Month + "-" + data.Day;
                try
                {
                    data = Convert.ToDateTime(txtDataFinal.Text);
                    dataFim = "" + data.Year + "-" + data.Month + "-" + data.Day;
                    Cursor.Current = Cursors.WaitCursor;
                    prenxerGrid();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message + " Data Invalida" + " - " + txtDataFinal.Text); }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message + " Data Invalida" + " - " + txtDataInicial.Text); } */
        }
        private void metroGrid1_CellContentDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    try { gridDuploClick(metroGrid1); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void cmbLoja_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            SqlCommand comd = new SqlCommand("select RTRIM(LTRIM(CodLoja)) from ASLOJA WITH (NOLOCK) where NomeLoja = '" + cmbLoja.Text + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { sCodLoja = reader[0].ToString(); } }
            conexao.Close();
        }

        private void cmbLoja_MouseClick_1(object sender, MouseEventArgs e)
        {
            cmbLoja.Items.Clear();
            prenxerCmbLoja();
        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try { gridDuploClick(metroGrid1); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void txtDataInicial_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtDataInicial.Text = ""; }
        }

        private void txtDataFinal_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtDataFinal.Text = ""; }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
