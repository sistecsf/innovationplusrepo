﻿namespace SISTEC_PLUS.POS
{
    partial class FormVendasFormaPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbFormaPagamento = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.txtValorRecebido = new MetroFramework.Controls.MetroTextBox();
            this.lbFormaPagamento = new MetroFramework.Controls.MetroLabel();
            this.lbTroco = new MetroFramework.Controls.MetroLabel();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btO = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbFormaPagamento
            // 
            this.cmbFormaPagamento.FormattingEnabled = true;
            this.cmbFormaPagamento.IntegralHeight = false;
            this.cmbFormaPagamento.ItemHeight = 23;
            this.cmbFormaPagamento.Location = new System.Drawing.Point(191, 81);
            this.cmbFormaPagamento.MaxDropDownItems = 15;
            this.cmbFormaPagamento.Name = "cmbFormaPagamento";
            this.cmbFormaPagamento.Size = new System.Drawing.Size(375, 29);
            this.cmbFormaPagamento.TabIndex = 96;
            this.cmbFormaPagamento.UseSelectable = true;
            this.cmbFormaPagamento.SelectedIndexChanged += new System.EventHandler(this.cmbFormaPagamento_SelectedIndexChanged);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(23, 91);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(139, 19);
            this.metroLabel3.TabIndex = 97;
            this.metroLabel3.Text = "Forma de Pagamento";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 321);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(99, 19);
            this.metroLabel1.TabIndex = 98;
            this.metroLabel1.Text = "Valor Recebido";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(339, 321);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(43, 19);
            this.metroLabel2.TabIndex = 99;
            this.metroLabel2.Text = "Troco";
            // 
            // txtValorRecebido
            // 
            this.txtValorRecebido.Lines = new string[0];
            this.txtValorRecebido.Location = new System.Drawing.Point(191, 317);
            this.txtValorRecebido.MaxLength = 32767;
            this.txtValorRecebido.Name = "txtValorRecebido";
            this.txtValorRecebido.PasswordChar = '\0';
            this.txtValorRecebido.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtValorRecebido.SelectedText = "";
            this.txtValorRecebido.Size = new System.Drawing.Size(127, 23);
            this.txtValorRecebido.TabIndex = 103;
            this.txtValorRecebido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtValorRecebido.UseSelectable = true;
            this.txtValorRecebido.TextChanged += new System.EventHandler(this.txtValorRecebido_TextChanged);
            // 
            // lbFormaPagamento
            // 
            this.lbFormaPagamento.AutoSize = true;
            this.lbFormaPagamento.Location = new System.Drawing.Point(572, 91);
            this.lbFormaPagamento.Name = "lbFormaPagamento";
            this.lbFormaPagamento.Size = new System.Drawing.Size(0, 0);
            this.lbFormaPagamento.TabIndex = 106;
            // 
            // lbTroco
            // 
            this.lbTroco.AutoSize = true;
            this.lbTroco.Location = new System.Drawing.Point(421, 321);
            this.lbTroco.Name = "lbTroco";
            this.lbTroco.Size = new System.Drawing.Size(16, 19);
            this.lbTroco.TabIndex = 107;
            this.lbTroco.Text = "0";
            // 
            // txtValor
            // 
            this.txtValor.BackColor = System.Drawing.SystemColors.Menu;
            this.txtValor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValor.Font = new System.Drawing.Font("Constantia", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValor.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.txtValor.Location = new System.Drawing.Point(191, 157);
            this.txtValor.Name = "txtValor";
            this.txtValor.ReadOnly = true;
            this.txtValor.Size = new System.Drawing.Size(375, 125);
            this.txtValor.TabIndex = 108;
            this.txtValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(23, 214);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(88, 19);
            this.metroLabel4.TabIndex = 109;
            this.metroLabel4.Text = "Total a Pagar";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btO});
            this.toolStrip1.Location = new System.Drawing.Point(495, 315);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(46, 25);
            this.toolStrip1.TabIndex = 110;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btO
            // 
            this.btO.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btO.Image = global::SISTEC_PLUS.Properties.Resources.arrow_11_241;
            this.btO.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btO.Name = "btO";
            this.btO.Size = new System.Drawing.Size(43, 22);
            this.btO.Text = "OK";
            this.btO.Click += new System.EventHandler(this.btO_Click);
            // 
            // FormVendasFormaPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 368);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.txtValor);
            this.Controls.Add(this.lbTroco);
            this.Controls.Add(this.lbFormaPagamento);
            this.Controls.Add(this.txtValorRecebido);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.cmbFormaPagamento);
            this.Name = "FormVendasFormaPagamento";
            this.Text = "Forma de Pagamento";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormVendasFormaPagamento_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox cmbFormaPagamento;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox txtValorRecebido;
        private MetroFramework.Controls.MetroLabel lbFormaPagamento;
        private MetroFramework.Controls.MetroLabel lbTroco;
        private System.Windows.Forms.TextBox txtValor;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btO;
    }
}