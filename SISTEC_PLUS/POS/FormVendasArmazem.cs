﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendasArmazem : MetroFramework.Forms.MetroForm
    {
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao); // conexão com o banco
        public FormVendasArmazem()
        {
            InitializeComponent();
        }

        private void prenxeGrid()
        {
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(@"SELECT CodArmz, Referenc from ASMESTRE WHERE Referenc = '"+ FormVendas.referencia +"'", conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("Loja");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid3.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void FormVendasArmazem_Load(object sender, EventArgs e)
        {
            metroGrid3.AutoGenerateColumns = false;
            prenxeGrid();
        }

        private void metroGrid3_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid3.CurrentRow != null)
                {
                    try
                    {
                        FormVendas.CodArmz = metroGrid3.CurrentRow.Cells[0].Value.ToString();
                        this.DialogResult = DialogResult.OK;
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void metroGrid3_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid3.RowCount - 1)
            {
                try
                {
                    FormVendas.CodArmz = metroGrid3.CurrentRow.Cells[0].Value.ToString();
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }
    }
}
