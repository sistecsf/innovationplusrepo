﻿namespace SISTEC_PLUS.POS
{
    partial class FormRequisicaoRecepcao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRequisicaoRecepcao));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btGravar = new System.Windows.Forms.ToolStripButton();
            this.btSair = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbLoja = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbLoj = new MetroFramework.Controls.MetroLabel();
            this.metroComboBox1 = new MetroFramework.Controls.MetroComboBox();
            this.cmbLoja = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.Mercadoria = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NUMDOC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CODLOJA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NOMEMERC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CODARMZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CODFAM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCR_SUP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATA_FABRICO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DATA_EXPIRA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REGEQUIP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CODMARCA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CODMODELO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CODSUBFAM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GUIA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NomeArz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ArmzDestino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btGravar,
            this.btSair});
            this.toolStrip1.Location = new System.Drawing.Point(376, 60);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(110, 25);
            this.toolStrip1.TabIndex = 90;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btGravar
            // 
            this.btGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btGravar.Image = ((System.Drawing.Image)(resources.GetObject("btGravar.Image")));
            this.btGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(61, 22);
            this.btGravar.Text = "Gravar";
            this.btGravar.Click += new System.EventHandler(this.btGravar_Click);
            // 
            // btSair
            // 
            this.btSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btSair.Image = ((System.Drawing.Image)(resources.GetObject("btSair.Image")));
            this.btSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(46, 22);
            this.btSair.Text = "Sair";
            this.btSair.Click += new System.EventHandler(this.btSair_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(143, 2);
            this.toolStripContainer1.Location = new System.Drawing.Point(376, 60);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(143, 27);
            this.toolStripContainer1.TabIndex = 91;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer1.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer1.TopToolStripPanel.Tag = "XC";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbLoja);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Location = new System.Drawing.Point(23, 91);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(828, 49);
            this.groupBox1.TabIndex = 92;
            this.groupBox1.TabStop = false;
            // 
            // lbLoja
            // 
            this.lbLoja.AutoSize = true;
            this.lbLoja.Location = new System.Drawing.Point(343, 16);
            this.lbLoja.Name = "lbLoja";
            this.lbLoja.Size = new System.Drawing.Size(26, 19);
            this.lbLoja.TabIndex = 94;
            this.lbLoja.Text = "Loj";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(6, 16);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(58, 19);
            this.metroLabel1.TabIndex = 93;
            this.metroLabel1.Text = "Loja -->";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbLoj);
            this.groupBox2.Controls.Add(this.metroComboBox1);
            this.groupBox2.Controls.Add(this.cmbLoja);
            this.groupBox2.Controls.Add(this.metroLabel4);
            this.groupBox2.Controls.Add(this.metroLabel3);
            this.groupBox2.Location = new System.Drawing.Point(23, 156);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(828, 92);
            this.groupBox2.TabIndex = 93;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Emissor";
            // 
            // lbLoj
            // 
            this.lbLoj.AutoSize = true;
            this.lbLoj.Location = new System.Drawing.Point(637, 27);
            this.lbLoj.Name = "lbLoj";
            this.lbLoj.Size = new System.Drawing.Size(0, 0);
            this.lbLoj.TabIndex = 119;
            // 
            // metroComboBox1
            // 
            this.metroComboBox1.FormattingEnabled = true;
            this.metroComboBox1.IntegralHeight = false;
            this.metroComboBox1.ItemHeight = 23;
            this.metroComboBox1.Location = new System.Drawing.Point(175, 53);
            this.metroComboBox1.Name = "metroComboBox1";
            this.metroComboBox1.Size = new System.Drawing.Size(430, 29);
            this.metroComboBox1.TabIndex = 118;
            this.metroComboBox1.UseSelectable = true;
            this.metroComboBox1.SelectedIndexChanged += new System.EventHandler(this.metroComboBox1_SelectedIndexChanged);
            this.metroComboBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.metroComboBox1_MouseClick);
            // 
            // cmbLoja
            // 
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.IntegralHeight = false;
            this.cmbLoja.ItemHeight = 23;
            this.cmbLoja.Location = new System.Drawing.Point(175, 17);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(430, 29);
            this.cmbLoja.TabIndex = 117;
            this.cmbLoja.UseSelectable = true;
            this.cmbLoja.SelectedIndexChanged += new System.EventHandler(this.cmbLoja_SelectedIndexChanged);
            this.cmbLoja.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbLoja_MouseClick);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(6, 63);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(93, 19);
            this.metroLabel4.TabIndex = 95;
            this.metroLabel4.Text = "ID Documento";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(6, 27);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(37, 19);
            this.metroLabel3.TabIndex = 94;
            this.metroLabel3.Text = "Loja ";
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Mercadoria,
            this.Column6,
            this.Column5,
            this.Column9,
            this.Column12,
            this.Column19,
            this.Column20,
            this.Column1,
            this.Column3,
            this.Column2,
            this.Column4,
            this.Column7,
            this.Column8,
            this.Column10,
            this.NUMDOC,
            this.CODLOJA,
            this.NOMEMERC,
            this.CODARMZ,
            this.CODFAM,
            this.DESCR_SUP,
            this.DATA_FABRICO,
            this.DATA_EXPIRA,
            this.REGEQUIP,
            this.CODMARCA,
            this.CODMODELO,
            this.CODSUBFAM,
            this.GUIA,
            this.NomeArz,
            this.ArmzDestino});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(23, 278);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(828, 234);
            this.metroGrid1.TabIndex = 98;
            this.metroGrid1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid1_CellContentClick);
            this.metroGrid1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.metroGrid1_EditingControlShowing);
            // 
            // Mercadoria
            // 
            this.Mercadoria.DataPropertyName = "Mercadoria";
            this.Mercadoria.HeaderText = "Mercadoria";
            this.Mercadoria.Name = "Mercadoria";
            this.Mercadoria.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Mercadoria.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "Localizacao";
            this.Column6.HeaderText = "Localização";
            this.Column6.Name = "Column6";
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Column1";
            this.Column5.HeaderText = "Referência";
            this.Column5.Name = "Column5";
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column9.DataPropertyName = "Column2";
            this.Column9.HeaderText = "Descrição";
            this.Column9.Name = "Column9";
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "QTD";
            this.Column12.HeaderText = "QTD";
            this.Column12.Name = "Column12";
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "Column18";
            this.Column19.HeaderText = "QTD-RI";
            this.Column19.Name = "Column19";
            // 
            // Column20
            // 
            this.Column20.HeaderText = "Contagem";
            this.Column20.Name = "Column20";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "codArmz";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "codAo";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Visible = false;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "FOB";
            this.Column2.HeaderText = "FOF";
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "PCLD";
            this.Column4.HeaderText = "PCLD";
            this.Column4.Name = "Column4";
            this.Column4.Visible = false;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "PVPD";
            this.Column7.HeaderText = "PVPD";
            this.Column7.Name = "Column7";
            this.Column7.Visible = false;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Column7";
            this.Column8.HeaderText = "PCL";
            this.Column8.Name = "Column8";
            this.Column8.Visible = false;
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "Column8";
            this.Column10.HeaderText = "PVP";
            this.Column10.Name = "Column10";
            this.Column10.Visible = false;
            // 
            // NUMDOC
            // 
            this.NUMDOC.DataPropertyName = "NUMDOC";
            this.NUMDOC.HeaderText = "NUMDOC";
            this.NUMDOC.Name = "NUMDOC";
            // 
            // CODLOJA
            // 
            this.CODLOJA.DataPropertyName = "Column4";
            this.CODLOJA.HeaderText = "CODLOJA";
            this.CODLOJA.Name = "CODLOJA";
            this.CODLOJA.Visible = false;
            // 
            // NOMEMERC
            // 
            this.NOMEMERC.DataPropertyName = "Column3";
            this.NOMEMERC.HeaderText = "NOMEMERC";
            this.NOMEMERC.Name = "NOMEMERC";
            this.NOMEMERC.Visible = false;
            // 
            // CODARMZ
            // 
            this.CODARMZ.DataPropertyName = "Column5";
            this.CODARMZ.HeaderText = "CODARMZ";
            this.CODARMZ.Name = "CODARMZ";
            this.CODARMZ.Visible = false;
            // 
            // CODFAM
            // 
            this.CODFAM.DataPropertyName = "Column6";
            this.CODFAM.HeaderText = "CODFAM";
            this.CODFAM.Name = "CODFAM";
            this.CODFAM.Visible = false;
            // 
            // DESCR_SUP
            // 
            this.DESCR_SUP.DataPropertyName = "Column9";
            this.DESCR_SUP.HeaderText = "DESCR_SUP";
            this.DESCR_SUP.Name = "DESCR_SUP";
            this.DESCR_SUP.Visible = false;
            // 
            // DATA_FABRICO
            // 
            this.DATA_FABRICO.DataPropertyName = "Column10";
            this.DATA_FABRICO.HeaderText = "DATA_FABRICO";
            this.DATA_FABRICO.Name = "DATA_FABRICO";
            this.DATA_FABRICO.Visible = false;
            // 
            // DATA_EXPIRA
            // 
            this.DATA_EXPIRA.DataPropertyName = "Column11";
            this.DATA_EXPIRA.HeaderText = "DATA_EXPIRA";
            this.DATA_EXPIRA.Name = "DATA_EXPIRA";
            this.DATA_EXPIRA.Visible = false;
            // 
            // REGEQUIP
            // 
            this.REGEQUIP.DataPropertyName = "Column12";
            this.REGEQUIP.HeaderText = "REGEQUIP";
            this.REGEQUIP.Name = "REGEQUIP";
            this.REGEQUIP.Visible = false;
            // 
            // CODMARCA
            // 
            this.CODMARCA.DataPropertyName = "Column13";
            this.CODMARCA.HeaderText = "CODMARCA";
            this.CODMARCA.Name = "CODMARCA";
            this.CODMARCA.Visible = false;
            // 
            // CODMODELO
            // 
            this.CODMODELO.DataPropertyName = "Column14";
            this.CODMODELO.HeaderText = "CODMODELO";
            this.CODMODELO.Name = "CODMODELO";
            this.CODMODELO.Visible = false;
            // 
            // CODSUBFAM
            // 
            this.CODSUBFAM.DataPropertyName = "Column15";
            this.CODSUBFAM.HeaderText = "CODSUBFAM";
            this.CODSUBFAM.Name = "CODSUBFAM";
            this.CODSUBFAM.Visible = false;
            // 
            // GUIA
            // 
            this.GUIA.DataPropertyName = "Column17";
            this.GUIA.HeaderText = "GUIA";
            this.GUIA.Name = "GUIA";
            this.GUIA.Visible = false;
            // 
            // NomeArz
            // 
            this.NomeArz.DataPropertyName = "Column18";
            this.NomeArz.HeaderText = "NomeArz";
            this.NomeArz.Name = "NomeArz";
            this.NomeArz.Visible = false;
            // 
            // ArmzDestino
            // 
            this.ArmzDestino.DataPropertyName = "Column19";
            this.ArmzDestino.HeaderText = "ArmzDestino";
            this.ArmzDestino.Name = "ArmzDestino";
            this.ArmzDestino.Visible = false;
            // 
            // FormRequisicaoRecepcao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 535);
            this.Controls.Add(this.metroGrid1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "FormRequisicaoRecepcao";
            this.Text = "Requisição Interna - Entrada de Material";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormRequisicaoRecepcao_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btGravar;
        private System.Windows.Forms.ToolStripButton btSair;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroLabel lbLoja;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel lbLoj;
        private MetroFramework.Controls.MetroComboBox metroComboBox1;
        private MetroFramework.Controls.MetroComboBox cmbLoja;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private System.Windows.Forms.DataGridViewComboBoxColumn Mercadoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn NUMDOC;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODLOJA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NOMEMERC;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODARMZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODFAM;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCR_SUP;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_FABRICO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DATA_EXPIRA;
        private System.Windows.Forms.DataGridViewTextBoxColumn REGEQUIP;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODMARCA;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODMODELO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CODSUBFAM;
        private System.Windows.Forms.DataGridViewTextBoxColumn GUIA;
        private System.Windows.Forms.DataGridViewTextBoxColumn NomeArz;
        private System.Windows.Forms.DataGridViewTextBoxColumn ArmzDestino;
    }
}