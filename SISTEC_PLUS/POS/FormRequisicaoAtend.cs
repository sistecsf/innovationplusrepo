﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS.POS
{
    public partial class FormRequisicaoAtend : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;
        SqlCommand comd;
        string sql, loja, codLoja, codLojaDestino, lojaDestino, strNomeEntidade, strLocalizacao_Fisica, strCodigoCliente;
        string sTName, sMovimentoST, strANULADO, sStatusDocAux, nIDUnicoLOJ, sStatusRI, strNrGuia, nIDUnicoRI, strREquipamento, strCodEntidadeX;
        int nFactura, nPreco, nNumdocST, nNumdocRI, tamanho;
        string[] referencia, descricao, dada, qtd, qtrRI, armazem;
        string numero, dat, loj, requisicao, id;
        public FormRequisicaoAtend()
        {
            InitializeComponent();
        }///*
        private void imprimir()
        {
            int i = 0;
            conexao.Open();
            comd = new SqlCommand("select count(F.Referenc) from ASFICMOV1 F WITH (NOLOCK), ASARMAZ A WITH (NOLOCK), ASMESTRE M WITH (NOLOCK) where IDUnico = '" + nIDUnicoLOJ + "' AND F.CodArmz = A.CodArmz AND F.Referenc = M.Referenc", conexao);
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { tamanho = Convert.ToInt32(reader[0].ToString()); } }

            referencia = new string[tamanho]; descricao = new string[tamanho]; dada = new string[tamanho]; qtd = new string[tamanho]; qtrRI = new string[metroGrid1.RowCount - 1]; armazem = new string[tamanho];

            comd = new SqlCommand("select LTrim(RTrim(F.Referenc)), LTrim(RTrim(F.QTD)), A.NomeArz, LTrim(RTrim(M.DescrArtigo)), CONVERT(VARCHAR(10), CAST(F.DataCria AS DATETIME), 105) from ASFICMOV1 F WITH (NOLOCK), ASARMAZ A WITH (NOLOCK), ASMESTRE M WITH (NOLOCK) where IDUnico = '" + nIDUnicoLOJ + "' AND F.CodArmz = A.CodArmz AND F.Referenc = M.Referenc order by A.NomeArz, F.DataCria", conexao);
            using (SqlDataReader reader = comd.ExecuteReader())
            { 
                while (reader.Read()) 
                {
                    referencia[i] = reader[0].ToString();
                    qtd[i] = reader[1].ToString();
                    armazem[i] = reader[2].ToString();
                    descricao[i] = reader[3].ToString();
                    dada[i] = reader[4].ToString();
                    i++;
                } 
            }
            for (int a = 0; a < metroGrid1.RowCount - 1; a ++)
            {
                qtrRI[a] = metroGrid1[3, a].Value.ToString();
            }

            comd = new SqlCommand("select top 1 LTrim(RTrim(R.NumDoc)), LTrim(RTrim(R.IDOrig)), LTrim(RTrim(F.GUIA)), L.NomeLoja, CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105) from ARegDoc R WITH (NOLOCK), ASLOJA L WITH (NOLOCK), ASFICMOV1 F WITH (NOLOCK) where R.IDUnico = '" + nIDUnicoLOJ + "' AND F.LojaDestino = L.CodLoja AND R.IDUnico = F.IDunico", conexao);
            using (SqlDataReader reader = comd.ExecuteReader())
            {
                while (reader.Read())
                {
                    numero = reader[0].ToString();
                    id = reader[1].ToString();
                    requisicao = reader[2].ToString();
                    loj = reader[3].ToString();
                    dat = reader[4].ToString();
                }
            }
            conexao.Close();

            RequisicaoAtend form = new RequisicaoAtend(numero, dat, loj, id + " - " + requisicao, "SH584", referencia, descricao, qtd, qtrRI, dada, armazem, tamanho);
            form.ShowDialog();
        }
        private void gravarIf()
        {
            SqlCommand comand = conexao.CreateCommand();
            SqlCommand[] comand1 = new SqlCommand[metroGrid1.RowCount - 1];

            //================================= INSERT AREGDOC =======================================
            comand.CommandText = "INSERT INTO adm.AADOCUMENTO (IDUNICO, CODUTILIZADOR,                    CodDoc,                   CodEntidade,                  DataCria)" +
                              "values ('"+ nIDUnicoLOJ +"', '"+ Variavel.codUtilizador +"', '"+ Gerais.PARAM_CODDOC_GI +"', '"+ strCodEntidadeX +"', Convert(Varchar(20),GetDate(),120))";

            //================================ INSERT ASFICMOV1 ======================================
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                if (metroGrid1[20, i].Value.ToString() == "S")
                    strREquipamento = "S";
                comand1[i] = conexao.CreateCommand();
                comand1[i].CommandText = "INSERT INTO adm.AAMOVIMENTOS (CodLoja,Guia,             DataCria,                      Data_Lancamento,                                            FOB,                                                             PCLD,                                                           PVPD,                                                       PCL,                                                             PVP,                                         Moeda,                      CodEntidade,               CodUtilizador,                         CodAO,                       IDunico,                NumDoc,                     CodArmz,                            Referenc,              LojaDestino,                        QTD,                              X1,                           CodDoc,                     Anulado,               TIPMOV,               ESTADO)" +
                         "values ('" + Variavel.codLoja + "', '" + strNrGuia + "', Convert(varchar(20),GetDate(),120), Convert(varchar(10),GetDate(),120), '" + metroGrid1[9, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[12, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[13, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[14, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[10, i].Value.ToString().Replace(',', '.') + "','" + Gerais.PARAM_MOEDA_PDR + "', '" + strCodEntidadeX + "', '" + Variavel.codUtilizador + "', '" + metroGrid1[15, i].Value + "', '" + nIDUnicoLOJ + "', '" + nNumdocST + "', '" + metroGrid1[16, i].Value + "', '" + metroGrid1[0, i].Value + "', '" + lbCodigo.Text + "', '" + metroGrid1[6, i].Value + "', '" + metroGrid1[8, i].Value + "', '" + Gerais.PARAM_CODDOC_GI + "', '" + strANULADO + "', '" + sMovimentoST + "', '" + sStatusRI + "')";
            }

            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                comand.Transaction = tran;
                comand.ExecuteNonQuery();

                for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                {
                    comand1[i].Transaction = tran;
                    comand1[i].ExecuteNonQuery();
                }
                /*
                for (int i = 0; i < metroGrid3.RowCount - 1; i++)
                {
                    SqlCommand comando = new SqlCommand("UPDATE adm.AAMOVIMENTOS SET QTD_ATENDIDA = IsNull(QTD_ATENDIDA,0) + '" + metroGrid3[2, i].Value + "', ESTADO=:frmRequisicoes_S.tblXReply.colEstado Where LTRIM(RTRIM(IDUNICO)) = '" + nIDUnicoRI + "' And CODDOC= '" + Gerais.PARAM_CODDOC_RI + "' And LTRIM(RTRIM(CODLOJA)) = '" + Variavel.codLoja + "' And LTRIM(RTRIM(CODARMZ)) = '" + metroGrid3[0, i].Value + "' And LTRIM(RTRIM(REFERENC)) = '" + metroGrid3[1, i].Value + "'");
                    comando.Connection = conexao;
                    comando.Transaction = tran;
                    comando.ExecuteNonQuery();
                } */

                //****************************************** INVOCAR A SP_CAIXA_RECIBO PARA O TRATAMENTO DA TRANSACÇÃO ******************************************
                SqlCommand cmad = new SqlCommand("SP_DOCUMENTO_GI", conexao);
                cmad.CommandType = CommandType.StoredProcedure;

                cmad.Parameters.Add(new SqlParameter("@p_nIDUNICO", nIDUnicoLOJ));
                cmad.Parameters.Add(new SqlParameter("@p_nIDUnicoRI", nIDUnicoRI));
                cmad.Parameters.Add(new SqlParameter("@p_sUserCode", Variavel.codUtilizador));
                cmad.Parameters.Add(new SqlParameter("@p_sCodLoja", Variavel.codLoja));
                cmad.Parameters.Add(new SqlParameter("@p_sCodEntidade", strCodEntidadeX));
                cmad.Parameters.Add(new SqlParameter("@p_sREquipamento", strREquipamento));

                cmad.Transaction = tran;
                cmad.ExecuteNonQuery();

                //****************************************** OPERAÇÃO COM SUCESSO ******************************************
                SqlCommand cmad1 = new SqlCommand("SP_DOCUMENTO_RI", conexao);
                cmad1.CommandType = CommandType.StoredProcedure;

                cmad1.Parameters.Add(new SqlParameter("@IDUnico", nIDUnicoRI));

                cmad1.Transaction = tran;
                cmad1.ExecuteNonQuery();

                tran.Commit();
                MessageBox.Show("Gravado com sucesso!");
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void gravarElse()
        {
            if (lbCodigo.Text == "" && lbLAF.Text == "")
                MessageBox.Show("Por favor especifique a [ Loja Receptora ]");
            else if (lbCodigo.Text == "" && lbLAF.Text != "") { requisicaoLocal(); }
            else
            {
                SqlCommand comand = conexao.CreateCommand();
                SqlCommand[] comand1 = new SqlCommand[metroGrid1.RowCount - 1];

                //================================= INSERT AREGDOC =======================================
                comand.CommandText = "INSERT INTO adm.AADOCUMENTO (IDUNICO, CODUTILIZADOR,                     CodDoc,                   CodEntidade,                         DataCria)" +
                                  "values ('"+ nIDUnicoLOJ +"', '"+ Variavel.codUtilizador +"', '"+ Gerais.PARAM_CODDOC_SR +"', '"+ Gerais.PARAM_CODCLI_GERAL +"', Convert(Varchar(20),GetDate(),120))";

                //================================ INSERT ASFICMOV1 ======================================
                for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                {
                    if (metroGrid1[20, i].Value.ToString() == "S")
                        strREquipamento = "S";

                    if (metroGrid1[3, i].Value.ToString() == metroGrid1[6, i].Value.ToString())
                        sStatusRI = "C";
                    else if (metroGrid1[3, i].Value.ToString() == "0")
                        sStatusRI = "C";
                    else if (Convert.ToInt32(metroGrid1[3, i].Value.ToString()) < Convert.ToInt32(metroGrid1[6, i].Value.ToString()))
                        sStatusRI = "W";
                    comand1[i] = conexao.CreateCommand();
                    comand1[i].CommandText = "INSERT INTO adm.AAMOVIMENTOS (CodLoja,Guia,             DataCria,                     Data_Lancamento,                                     FOB,                                                              PCLD,                                                            PVPD,                                                           PCL,                                               PVP,                                                        CodEntidade,                        CodUtilizador,                      CodAO,                     IDunico,              NumDoc,                        CodArmz,                        Referenc,                 LojaDestino,                     ARMZDESTINO,                         QTD,                               X1,                           CodDoc,                     Anulado,              TIPMOV,                ESTADO)" +
                             "values ('" + Variavel.codLoja + "', '" + strNrGuia + "', Convert(varchar(20),GetDate(),120), Convert(varchar(10),GetDate(),120), '" + metroGrid1[9, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[12, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[13, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[14, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[10, i].Value.ToString().Replace(',', '.') + "','" + Gerais.PARAM_CODCLI_GERAL + "', '" + Variavel.codUtilizador + "', '" + metroGrid1[15, i].Value + "', '" + nIDUnicoLOJ + "', '" + nNumdocST + "', '" + metroGrid1[16, i].Value + "', '" + metroGrid1[0, i].Value + "', '" + lbCodigo.Text + "', '" + metroGrid1[19, i].Value + "', '" + metroGrid1[6, i].Value + "', '" + metroGrid1[8, i].Value + "', '" + Gerais.PARAM_CODDOC_GI + "', '" + strANULADO + "', '" + sMovimentoST + "', '" + sStatusRI + "')";
                }

                conexao.Open();
                SqlTransaction tran = conexao.BeginTransaction();
                try
                {
                    comand.Transaction = tran;
                    comand.ExecuteNonQuery();

                    for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                    {
                        comand1[i].Transaction = tran;
                        comand1[i].ExecuteNonQuery();
                    }

                    //****************************************** INVOCAR A SP_CAIXA_RECIBO PARA O TRATAMENTO DA TRANSACÇÃO ******************************************
                    SqlCommand cmad = new SqlCommand("SP_DOCUMENTO_SR", conexao);
                    cmad.CommandType = CommandType.StoredProcedure;

                    cmad.Parameters.Add(new SqlParameter("@p_nIDUNICO", nIDUnicoLOJ));
                    cmad.Parameters.Add(new SqlParameter("@p_nIDUnicoRI", nIDUnicoRI));
                    cmad.Parameters.Add(new SqlParameter("@p_sUserCode", Variavel.codUtilizador));
                    cmad.Parameters.Add(new SqlParameter("@p_sCodLoja", Variavel.codLoja));
                    cmad.Parameters.Add(new SqlParameter("@p_sCodEntidade", Gerais.PARAM_CODCLI_GERAL));
                    cmad.Parameters.Add(new SqlParameter("@p_sREquipamento", strREquipamento));

                    cmad.Transaction = tran;
                    cmad.ExecuteNonQuery();

                    //****************************************** OPERAÇÃO COM SUCESSO ******************************************
                    SqlCommand cmad1 = new SqlCommand("SP_DOCUMENTO_RI", conexao);
                    cmad1.CommandType = CommandType.StoredProcedure;

                    cmad1.Parameters.Add(new SqlParameter("@IDUnico", nIDUnicoRI));

                    cmad1.Transaction = tran;
                    cmad1.ExecuteNonQuery();

                    tran.Commit();
                    MessageBox.Show("Gravado com sucesso!");
                }
                catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
                finally { conexao.Close(); }
            }
        }
        private void requisicaoLocal()
        {
            SqlCommand comand = conexao.CreateCommand();
            SqlCommand[] comand1 = new SqlCommand[metroGrid1.RowCount - 1];

            //================================= INSERT AREGDOC =======================================
            comand.CommandText = "INSERT INTO adm.AADOCUMENTO (IDUNICO, CODUTILIZADOR, CodDoc, CodEntidade, DataCria)" +
                              "values ('" + nIDUnicoLOJ + "', '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_SR + "', '" + Gerais.PARAM_CODCLI_GERAL + "', Convert(Varchar(20),GetDate(),120))";

            //================================ INSERT ASFICMOV1 ======================================
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                if (metroGrid1[20, i].Value.ToString() == "S")
                    strREquipamento = "S";
                comand1[i] = conexao.CreateCommand();
                comand1[i].CommandText = "INSERT INTO adm.AAMOVIMENTOS (CodLoja,Guia,             DataCria,                     Data_Lancamento,                                     FOB,                                                              PCLD,                                                            PVPD,                                                           PCL,                                               PVP,                                                        CodEntidade,                        CodUtilizador,                      CodAO,                     IDunico,              NumDoc,                        CodArmz,                        Referenc,                        ARMZDESTINO,                         QTD,                               X1,                           CodDoc,                     Anulado,              TIPMOV)" +
                         "values ('" + Variavel.codLoja + "', '" + strNrGuia + "', Convert(varchar(20),GetDate(),120), Convert(varchar(10),GetDate(),120), '" + metroGrid1[9, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[12, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[13, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[14, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[10, i].Value.ToString().Replace(',', '.') + "','" + Gerais.PARAM_CODCLI_GERAL + "', '" + Variavel.codUtilizador + "', '" + metroGrid1[15, i].Value + "', '" + nIDUnicoLOJ + "', '" + nNumdocST + "', '" + metroGrid1[16, i].Value + "', '" + metroGrid1[0, i].Value + "' '" + metroGrid1[19, i].Value + "', '" + metroGrid1[6, i].Value + "', '" + metroGrid1[8, i].Value + "', '" + Gerais.PARAM_CODDOC_GI + "', '" + strANULADO + "', '" + sMovimentoST + "')";
            }

            conexao.Open();
                SqlTransaction tran = conexao.BeginTransaction();
                try
                {
                    comand.Transaction = tran;
                    comand.ExecuteNonQuery();

                    for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                    {
                        comand1[i].Transaction = tran;
                        comand1[i].ExecuteNonQuery();
                    }

                    //****************************************** INVOCAR SP PARA O TRATAMENTO DA TRANSACÇÃO ******************************************
                    SqlCommand cmad = new SqlCommand("SP_DOCUMENTO_SRA", conexao);
                    cmad.CommandType = CommandType.StoredProcedure;

                    cmad.Parameters.Add(new SqlParameter("@p_nIDUNICO", nIDUnicoLOJ));
                    cmad.Parameters.Add(new SqlParameter("@p_nIDUnicoRI", nIDUnicoRI));
                    cmad.Parameters.Add(new SqlParameter("@p_sUserCode", Variavel.codUtilizador));
                    cmad.Parameters.Add(new SqlParameter("@p_sCodLoja", Variavel.codLoja));
                    cmad.Parameters.Add(new SqlParameter("@p_sCodEntidade", Gerais.PARAM_CODCLI_GERAL));
                    cmad.Parameters.Add(new SqlParameter("@p_sREquipamento", strREquipamento));

                    cmad.Transaction = tran;
                    cmad.ExecuteNonQuery();
                    //****************************************** OPERAÇÃO COM SUCESSO ******************************************
                    SqlCommand cmad1 = new SqlCommand("SP_DOCUMENTO_RI", conexao);
                    cmad1.CommandType = CommandType.StoredProcedure;

                    cmad1.Parameters.Add(new SqlParameter("@IDUnico", nIDUnicoRI));

                    cmad1.Transaction = tran;
                    cmad1.ExecuteNonQuery();

                    tran.Commit();
                    MessageBox.Show("Gravado com sucesso!");
                }
                catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
                finally { conexao.Close(); }
        }
        private void gravar()
        {
            strCodEntidadeX = "1212";
            strREquipamento = "XX";
            
            sTName = "T_INS_TRANS_ARMAZ";
            nFactura = 0;
            nPreco = 0;
            sMovimentoST = "-";
            strANULADO = "Y";
            sStatusDocAux = "E";
            strNrGuia = Gerais.PARAM_CODDOC_RI + "#" + nNumdocRI;

            conexao.Open();
            comd = new SqlCommand("SELECT RTRIM(ISNULL(CODLOJA,'" + Variavel.codLoja + "')) + CAST (ISNULL( MAX( CAST( RIGHT (RTRIM(IDUNICO), LEN(RTRIM(IDUNICO)) - (LEN(ISNULL(CODLOJA,'" + Variavel.codLoja + "'))))AS INT )), 1) + 1 AS VARCHAR(23)) AS IDUNICO " +
                                "FROM AREGDOC  WITH (XLOCK) WHERE CODLOJA = '" + Variavel.codLoja + "' AND LEFT(IDUNICO, LEN(CODLOJA)) = '" + Variavel.codLoja + "' GROUP BY CODLOJA", conexao);
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { nIDUnicoLOJ = reader[0].ToString(); } }
            if (nIDUnicoLOJ == "" || nIDUnicoLOJ == null)
                nIDUnicoLOJ = Variavel.codUtilizador + "1";

            comd = new SqlCommand("SELECT ISNULL(MAX(NUMDOC),0) + 1 FROM AregDoc WITH (XLOCK) WHERE CodDoc = 'SR' AND CodLoja= '" + Variavel.codLoja + "'", conexao);
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { try { nNumdocST = Convert.ToInt32(reader[0].ToString()); } catch (Exception) { nNumdocST = 1; } } }
            conexao.Close();

            if (strLocalizacao_Fisica == "EXTERNA")
                gravarIf();
            else
                gravarElse();
        }//*/
        private void apagarLinhasNoGrid()
        {
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                metroGrid1[6, i].ReadOnly =  true;
            }
            metroGrid1.Refresh();
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                if (metroGrid1[6, i].Value == null)
                {
                    metroGrid1.Rows.RemoveAt(i);
                    //metroGrid3.Rows.RemoveAt(i);
                }
            }
            metroGrid1.Refresh();
            //metroGrid3.Refresh();
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                metroGrid1[6, i].ReadOnly = false;
            }
            metroGrid1.Refresh();
        }
        private void prenxerGrid()
        {
            sql = "Select LTrim(RTrim(F.Referenc)), M.DescrArtigo, F.QTD, QTD_ATENDIDA, F.FOB, F.PVPD * '" + Convert.ToDouble(Variavel.cambio) + "', F.PVP * F.QTD, F.PCLD, F.PVPD, F.PCLD * '" + Convert.ToDouble(Variavel.cambio) + "', LTrim(RTrim(F.CodAO)), LTrim(RTrim(F.codarmz)), LTrim(RTrim(M.CodFam)), M.CodSubFam, R.CODLOJA, LOJADESTINO, LTrim(RTrim(ARMZDESTINO)), LTrim(RTrim(J.NOMELOJA)), " +
                "CASE WHEN QTD_ATENDIDA=F.QTD THEN 'ATENDIDA' WHEN QTD_ATENDIDA=0 THEN 'ATENDIDA' WHEN QTD_ATENDIDA<F.QTD THEN 'PENDENTE' ELSE 'EMITIDO' END, LTrim(RTrim(Locali)), LTrim(RTrim(M.QTD)), LTrim(RTrim(RegEquip)), LTrim(RTrim(CodMarca)), LTrim(RTrim(CodModelo)) " +
                "From ASFICMOV1 F WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), AFENTIDADE C WITH (NOLOCK), AREGDOC R WITH (NOLOCK), ASDOCS D WITH (NOLOCK), ASLOJA J WITH (NOLOCK) " +
                "Where F.Referenc=M.Referenc And F.codarmz=M.codarmz And F.CodEntidade=C.CodEntidade And F.IDunico=R.IDunico And LTRIM(RTRIM(F.coddoc))=LTRIM(RTRIM(D.coddoc)) And LTRIM(RTRIM(R.CODLOJA))=LTRIM(RTRIM(J.CODLOJA)) "+
                "And LTRIM(RTRIM(F.CODLOJA))=LTRIM(RTRIM(J.CODLOJA)) And F.CodLoja=M.CodLoja And (ISNULL(QTD_ATENDIDA,0)<F.QTD OR QTD_ATENDIDA<>0) And R.IDunico = '" + FormRequisicaoAtendimento.idunico + "' And F.coddoc= 'RI' And F.numdoc= '" + FormRequisicaoAtendimento.numeroDoc + "' ";

            if (Convert.ToInt32(Permissoes.nPrgPos) < 4)
                sql = sql + "AND F.CodLoja= '"+ Variavel.codLoja +"' ";
            sql = sql + "Order By LTrim(RTrim(Locali)), M.DescrArtigo";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
                conexao.Close();
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }

        private void prenxerGrid2()
        {
            sql = "Select LTrim(RTrim(F.CodArmz)), LTrim(RTrim(F.Referenc)) From ASFICMOV1 F With (NoLock), AREGDOC R With (NoLock), ASLOJA J With (NoLock) " +
                "Where F.IDunico=R.IDunico And LTRIM(RTRIM(R.CODLOJA))=LTRIM(RTRIM(J.CODLOJA)) And (ISNULL(QTD_ATENDIDA,0)<F.QTD OR QTD_ATENDIDA<>0) And R.IDunico = '" + FormRequisicaoAtendimento.idunico + "' And F.coddoc= 'RI' And F.numdoc= '" + FormRequisicaoAtendimento.numeroDoc + "' ";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid3.DataSource = dataView;
                conexao.Close();
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }
        private void FormRequisicaoAtend_Load(object sender, EventArgs e)
        {
            lbData.Text = FormRequisicaoAtendimento.data1;
            lbUtilizador.Text = FormRequisicaoAtendimento.operador;
            lbRIN.Text = FormRequisicaoAtendimento.numeroDoc;
            nNumdocRI = Convert.ToInt32(FormRequisicaoAtendimento.numeroDoc);
            nIDUnicoRI = FormRequisicaoAtendimento.idunico;
            conexao.Open();
            comd = new SqlCommand("Select LTrim(RTrim(R.CODLOJA)), LTrim(RTrim(LOJADESTINO)), LTrim(RTrim(J.NOMELOJA)) "+
                "From ASFICMOV1 F WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), AFENTIDADE C WITH (NOLOCK), AREGDOC R WITH (NOLOCK), ASDOCS D WITH (NOLOCK), ASLOJA J WITH (NOLOCK) "+
                "Where F.Referenc=M.Referenc And F.codarmz=M.codarmz And F.CodEntidade=C.CodEntidade And F.IDunico=R.IDunico And LTRIM(RTRIM(F.coddoc))=LTRIM(RTRIM(D.coddoc)) And LTRIM(RTRIM(R.CODLOJA))=LTRIM(RTRIM(J.CODLOJA)) "+
                "And LTRIM(RTRIM(F.CODLOJA))=LTRIM(RTRIM(J.CODLOJA)) And F.CodLoja=M.CodLoja And (ISNULL(QTD_ATENDIDA,0)<F.QTD OR QTD_ATENDIDA<>0) And R.IDunico = '" + FormRequisicaoAtendimento.idunico + "' And F.coddoc= 'RI' And F.numdoc = '" + FormRequisicaoAtendimento.numeroDoc + "'", conexao);
            using (SqlDataReader reader = comd.ExecuteReader())
            { 
                while (reader.Read()) 
                { 
                    codLoja = reader[0].ToString();
                    codLojaDestino = reader[1].ToString();
                    loja = reader[2].ToString();
                } 
            }

            comd = new SqlCommand("Select CodEntidade From Aregdoc With (NoLock) Where Idunico = '" + FormRequisicaoAtendimento.idunico + "'", conexao);
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { strCodigoCliente = reader[0].ToString(); } }

            comd = new SqlCommand("select NomeLoja from ASLOJA where CodLoja = '" + codLojaDestino + "'", conexao);
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { lojaDestino = reader[0].ToString(); } }

            comd = new SqlCommand("Select RTrim(LTrim(Localizacao_Fisica)), RTrim(LTrim(NomeEntidade)) From AFENTIDADE With (NoLock) Where CodEntidade = '" + strCodigoCliente + "'", conexao);
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { strLocalizacao_Fisica = reader[0].ToString(); strNomeEntidade = reader[1].ToString(); } }
            conexao.Close();
            lbLoja.Text = codLoja + " - " + loja;
            lbLAF.Text = lojaDestino;
            lbCodigo.Text = codLojaDestino;

            metroGrid1.AutoGenerateColumns = false;
            metroGrid3.AutoGenerateColumns = false;
            prenxerGrid();
            prenxerGrid2();
        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void btGravar_Click(object sender, EventArgs e)
        {
            metroGrid1.Refresh();
            
            for (int i = 0; i < metroGrid1.RowCount; i++)
            {
                apagarLinhasNoGrid();
            }
            
            gravar(); 
            imprimir();
        }

        private void metroGrid1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void metroGrid1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {

            }
        }

        private void btConsultar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
