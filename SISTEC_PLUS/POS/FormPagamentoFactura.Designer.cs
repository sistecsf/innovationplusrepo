﻿namespace SISTEC_PLUS.POS
{
    partial class FormPagamentoFactura
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPagamentoFactura));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.btSair = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelNomeDoCliente = new MetroFramework.Controls.MetroLabel();
            this.labelNumeroDoCliente = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelNomeUtilizadorQueEmitiuFactura = new MetroFramework.Controls.MetroLabel();
            this.labelDataDeCriacaoDoDocumento = new MetroFramework.Controls.MetroLabel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.labelTotalValorEmDolares = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.labelTotalValorEmKwanzas = new MetroFramework.Controls.MetroLabel();
            this.labelNumeroDaFactura = new MetroFramework.Controls.MetroLabel();
            this.txtIdFactura = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorPadrao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Moeda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cambio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Operador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbSaldarCompletamenteAFactura = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.comboBoxMoeda = new MetroFramework.Controls.MetroComboBox();
            this.labelValor = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.txtValorReceBido = new MetroFramework.Controls.MetroTextBox();
            this.comboTipoPagamento = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.cbCambioManual = new MetroFramework.Controls.MetroCheckBox();
            this.cbImpostoSelo = new MetroFramework.Controls.MetroCheckBox();
            this.comboBoFormaPagamento = new MetroFramework.Controls.MetroComboBox();
            this.txtCambio = new MetroFramework.Controls.MetroTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.labelNomeContaCaixa = new MetroFramework.Controls.MetroLabel();
            this.labelNomeContaCliente = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.Caixa = new MetroFramework.Controls.MetroLabel();
            this.comboContaCaixa = new MetroFramework.Controls.MetroComboBox();
            this.comboContaCliente = new MetroFramework.Controls.MetroComboBox();
            this.txtObservacao = new System.Windows.Forms.TextBox();
            this.labelObservacao = new MetroFramework.Controls.MetroLabel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.labelValorRestanteEmUSD = new MetroFramework.Controls.MetroLabel();
            this.labelValorRestanteEmKZ = new MetroFramework.Controls.MetroLabel();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btGravar,
            this.toolStripButton2,
            this.btSair});
            this.toolStrip1.Location = new System.Drawing.Point(295, 60);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(219, 25);
            this.toolStrip1.TabIndex = 89;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btGravar
            // 
            this.btGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btGravar.Image = ((System.Drawing.Image)(resources.GetObject("btGravar.Image")));
            this.btGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(61, 22);
            this.btGravar.Text = "Gravar";
            this.btGravar.Click += new System.EventHandler(this.btGravar_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(78, 22);
            this.toolStripButton2.Text = "Consultar";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // btSair
            // 
            this.btSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btSair.Image = ((System.Drawing.Image)(resources.GetObject("btSair.Image")));
            this.btSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(46, 22);
            this.btSair.Text = "Sair";
            this.btSair.Click += new System.EventHandler(this.btSair_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(218, 2);
            this.toolStripContainer1.Location = new System.Drawing.Point(295, 60);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(218, 27);
            this.toolStripContainer1.TabIndex = 90;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer1.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer1.TopToolStripPanel.Tag = "XC";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelNomeDoCliente);
            this.groupBox2.Controls.Add(this.labelNumeroDoCliente);
            this.groupBox2.Controls.Add(this.metroLabel6);
            this.groupBox2.Controls.Add(this.metroLabel14);
            this.groupBox2.Controls.Add(this.metroLabel15);
            this.groupBox2.Controls.Add(this.metroLabel16);
            this.groupBox2.Controls.Add(this.metroLabel17);
            this.groupBox2.Location = new System.Drawing.Point(26, 210);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(605, 69);
            this.groupBox2.TabIndex = 131;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados do Cliente";
            // 
            // labelNomeDoCliente
            // 
            this.labelNomeDoCliente.AutoSize = true;
            this.labelNomeDoCliente.BackColor = System.Drawing.Color.White;
            this.labelNomeDoCliente.ForeColor = System.Drawing.Color.DodgerBlue;
            this.labelNomeDoCliente.Location = new System.Drawing.Point(223, 34);
            this.labelNomeDoCliente.Name = "labelNomeDoCliente";
            this.labelNomeDoCliente.Size = new System.Drawing.Size(0, 0);
            this.labelNomeDoCliente.Style = MetroFramework.MetroColorStyle.Blue;
            this.labelNomeDoCliente.TabIndex = 105;
            // 
            // labelNumeroDoCliente
            // 
            this.labelNumeroDoCliente.AutoSize = true;
            this.labelNumeroDoCliente.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelNumeroDoCliente.Location = new System.Drawing.Point(49, 34);
            this.labelNumeroDoCliente.Name = "labelNumeroDoCliente";
            this.labelNumeroDoCliente.Size = new System.Drawing.Size(0, 0);
            this.labelNumeroDoCliente.TabIndex = 104;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(621, 30);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(0, 0);
            this.metroLabel6.TabIndex = 102;
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(164, 34);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(53, 19);
            this.metroLabel14.TabIndex = 101;
            this.metroLabel14.Text = "Nome: ";
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(423, 30);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(0, 0);
            this.metroLabel15.TabIndex = 100;
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.ForeColor = System.Drawing.Color.DodgerBlue;
            this.metroLabel16.Location = new System.Drawing.Point(114, 34);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(0, 0);
            this.metroLabel16.TabIndex = 99;
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(15, 34);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(28, 19);
            this.metroLabel17.TabIndex = 91;
            this.metroLabel17.Text = "Nº:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelNomeUtilizadorQueEmitiuFactura);
            this.groupBox1.Controls.Add(this.labelDataDeCriacaoDoDocumento);
            this.groupBox1.Controls.Add(this.metroLabel13);
            this.groupBox1.Controls.Add(this.metroLabel12);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.labelTotalValorEmDolares);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.labelTotalValorEmKwanzas);
            this.groupBox1.Controls.Add(this.labelNumeroDaFactura);
            this.groupBox1.Controls.Add(this.txtIdFactura);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Location = new System.Drawing.Point(26, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(759, 86);
            this.groupBox1.TabIndex = 130;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados da factura";
            // 
            // labelNomeUtilizadorQueEmitiuFactura
            // 
            this.labelNomeUtilizadorQueEmitiuFactura.AutoSize = true;
            this.labelNomeUtilizadorQueEmitiuFactura.Location = new System.Drawing.Point(400, 56);
            this.labelNomeUtilizadorQueEmitiuFactura.Name = "labelNomeUtilizadorQueEmitiuFactura";
            this.labelNomeUtilizadorQueEmitiuFactura.Size = new System.Drawing.Size(0, 0);
            this.labelNomeUtilizadorQueEmitiuFactura.TabIndex = 108;
            // 
            // labelDataDeCriacaoDoDocumento
            // 
            this.labelDataDeCriacaoDoDocumento.AutoSize = true;
            this.labelDataDeCriacaoDoDocumento.Location = new System.Drawing.Point(611, 56);
            this.labelDataDeCriacaoDoDocumento.Name = "labelDataDeCriacaoDoDocumento";
            this.labelDataDeCriacaoDoDocumento.Size = new System.Drawing.Size(0, 0);
            this.labelDataDeCriacaoDoDocumento.TabIndex = 107;
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(580, 56);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(38, 19);
            this.metroLabel13.TabIndex = 106;
            this.metroLabel13.Text = "Aos: ";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(318, 56);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(87, 19);
            this.metroLabel12.TabIndex = 105;
            this.metroLabel12.Text = "Emitido por: ";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.ForeColor = System.Drawing.Color.Honeydew;
            this.metroLabel4.Location = new System.Drawing.Point(114, 27);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(85, 19);
            this.metroLabel4.TabIndex = 104;
            this.metroLabel4.Text = "Nº da fatura:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(505, 27);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(113, 19);
            this.metroLabel3.TabIndex = 103;
            this.metroLabel3.Text = "Valor Total (USD): ";
            // 
            // labelTotalValorEmDolares
            // 
            this.labelTotalValorEmDolares.AutoSize = true;
            this.labelTotalValorEmDolares.ForeColor = System.Drawing.Color.Honeydew;
            this.labelTotalValorEmDolares.Location = new System.Drawing.Point(624, 27);
            this.labelTotalValorEmDolares.Name = "labelTotalValorEmDolares";
            this.labelTotalValorEmDolares.Size = new System.Drawing.Size(0, 0);
            this.labelTotalValorEmDolares.TabIndex = 102;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(307, 27);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(101, 19);
            this.metroLabel2.TabIndex = 101;
            this.metroLabel2.Text = "Valor Total (Kz): ";
            // 
            // labelTotalValorEmKwanzas
            // 
            this.labelTotalValorEmKwanzas.AutoSize = true;
            this.labelTotalValorEmKwanzas.ForeColor = System.Drawing.Color.Honeydew;
            this.labelTotalValorEmKwanzas.Location = new System.Drawing.Point(405, 27);
            this.labelTotalValorEmKwanzas.Name = "labelTotalValorEmKwanzas";
            this.labelTotalValorEmKwanzas.Size = new System.Drawing.Size(0, 0);
            this.labelTotalValorEmKwanzas.TabIndex = 100;
            // 
            // labelNumeroDaFactura
            // 
            this.labelNumeroDaFactura.AutoSize = true;
            this.labelNumeroDaFactura.ForeColor = System.Drawing.Color.Honeydew;
            this.labelNumeroDaFactura.Location = new System.Drawing.Point(202, 29);
            this.labelNumeroDaFactura.Name = "labelNumeroDaFactura";
            this.labelNumeroDaFactura.Size = new System.Drawing.Size(0, 0);
            this.labelNumeroDaFactura.TabIndex = 99;
            // 
            // txtIdFactura
            // 
            this.txtIdFactura.Lines = new string[0];
            this.txtIdFactura.Location = new System.Drawing.Point(42, 25);
            this.txtIdFactura.MaxLength = 32767;
            this.txtIdFactura.Name = "txtIdFactura";
            this.txtIdFactura.PasswordChar = '\0';
            this.txtIdFactura.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIdFactura.SelectedText = "";
            this.txtIdFactura.Size = new System.Drawing.Size(64, 23);
            this.txtIdFactura.TabIndex = 98;
            this.txtIdFactura.UseSelectable = true;
            this.txtIdFactura.Leave += new System.EventHandler(this.idFactura_leaveFocus);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(15, 27);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(21, 19);
            this.metroLabel1.TabIndex = 91;
            this.metroLabel1.Text = "ID";
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Data,
            this.ValorPadrao,
            this.Moeda,
            this.Cambio,
            this.Operador});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(23, 328);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(762, 120);
            this.metroGrid1.TabIndex = 129;
            // 
            // Data
            // 
            this.Data.HeaderText = "Data";
            this.Data.Name = "Data";
            // 
            // ValorPadrao
            // 
            this.ValorPadrao.HeaderText = "Valor Padrão";
            this.ValorPadrao.Name = "ValorPadrao";
            // 
            // Moeda
            // 
            this.Moeda.HeaderText = "Moeda";
            this.Moeda.Name = "Moeda";
            // 
            // Cambio
            // 
            this.Cambio.HeaderText = "Câmbio";
            this.Cambio.Name = "Cambio";
            // 
            // Operador
            // 
            this.Operador.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Operador.HeaderText = "Operador";
            this.Operador.Name = "Operador";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(20, 299);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 13);
            this.label2.TabIndex = 128;
            this.label2.Text = "Detalhes da Factura";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbSaldarCompletamenteAFactura);
            this.groupBox3.Controls.Add(this.metroLabel8);
            this.groupBox3.Controls.Add(this.comboBoxMoeda);
            this.groupBox3.Controls.Add(this.labelValor);
            this.groupBox3.Controls.Add(this.metroLabel10);
            this.groupBox3.Controls.Add(this.metroLabel9);
            this.groupBox3.Controls.Add(this.txtValorReceBido);
            this.groupBox3.Controls.Add(this.comboTipoPagamento);
            this.groupBox3.Controls.Add(this.metroLabel11);
            this.groupBox3.Controls.Add(this.cbCambioManual);
            this.groupBox3.Controls.Add(this.cbImpostoSelo);
            this.groupBox3.Controls.Add(this.comboBoFormaPagamento);
            this.groupBox3.Controls.Add(this.txtCambio);
            this.groupBox3.Location = new System.Drawing.Point(23, 454);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(762, 119);
            this.groupBox3.TabIndex = 132;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pagamento";
            // 
            // cbSaldarCompletamenteAFactura
            // 
            this.cbSaldarCompletamenteAFactura.AutoSize = true;
            this.cbSaldarCompletamenteAFactura.Location = new System.Drawing.Point(595, 32);
            this.cbSaldarCompletamenteAFactura.Name = "cbSaldarCompletamenteAFactura";
            this.cbSaldarCompletamenteAFactura.Size = new System.Drawing.Size(142, 15);
            this.cbSaldarCompletamenteAFactura.TabIndex = 121;
            this.cbSaldarCompletamenteAFactura.Text = "Saldar completamente";
            this.cbSaldarCompletamenteAFactura.UseSelectable = true;
            this.cbSaldarCompletamenteAFactura.CheckedChanged += new System.EventHandler(this.cbSaldarCompletamenteAFactura_CheckedChanged);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(26, 28);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(51, 19);
            this.metroLabel8.TabIndex = 109;
            this.metroLabel8.Text = "Moeda";
            // 
            // comboBoxMoeda
            // 
            this.comboBoxMoeda.FormattingEnabled = true;
            this.comboBoxMoeda.ItemHeight = 23;
            this.comboBoxMoeda.Location = new System.Drawing.Point(83, 28);
            this.comboBoxMoeda.Name = "comboBoxMoeda";
            this.comboBoxMoeda.Size = new System.Drawing.Size(121, 29);
            this.comboBoxMoeda.TabIndex = 113;
            this.comboBoxMoeda.UseSelectable = true;
            this.comboBoxMoeda.SelectedIndexChanged += new System.EventHandler(this.comboBoxMoeda_SelectedIndexChanged);
            // 
            // labelValor
            // 
            this.labelValor.AutoSize = true;
            this.labelValor.Location = new System.Drawing.Point(443, 28);
            this.labelValor.Name = "labelValor";
            this.labelValor.Size = new System.Drawing.Size(38, 19);
            this.labelValor.TabIndex = 108;
            this.labelValor.Text = "Valor";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(21, 82);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(56, 19);
            this.metroLabel10.TabIndex = 111;
            this.metroLabel10.Text = "Câmbio";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(241, 28);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(35, 19);
            this.metroLabel9.TabIndex = 110;
            this.metroLabel9.Text = "Tipo";
            // 
            // txtValorReceBido
            // 
            this.txtValorReceBido.Lines = new string[0];
            this.txtValorReceBido.Location = new System.Drawing.Point(487, 28);
            this.txtValorReceBido.MaxLength = 32767;
            this.txtValorReceBido.Name = "txtValorReceBido";
            this.txtValorReceBido.PasswordChar = '\0';
            this.txtValorReceBido.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtValorReceBido.SelectedText = "";
            this.txtValorReceBido.Size = new System.Drawing.Size(102, 23);
            this.txtValorReceBido.TabIndex = 117;
            this.txtValorReceBido.UseSelectable = true;
            this.txtValorReceBido.Leave += new System.EventHandler(this.txtValorReceBido_Leave);
            // 
            // comboTipoPagamento
            // 
            this.comboTipoPagamento.FormattingEnabled = true;
            this.comboTipoPagamento.ItemHeight = 23;
            this.comboTipoPagamento.Location = new System.Drawing.Point(282, 28);
            this.comboTipoPagamento.Name = "comboTipoPagamento";
            this.comboTipoPagamento.Size = new System.Drawing.Size(121, 29);
            this.comboTipoPagamento.TabIndex = 116;
            this.comboTipoPagamento.UseSelectable = true;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(343, 82);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(138, 19);
            this.metroLabel11.TabIndex = 112;
            this.metroLabel11.Text = "Forma de Pagamento";
            // 
            // cbCambioManual
            // 
            this.cbCambioManual.AutoSize = true;
            this.cbCambioManual.Location = new System.Drawing.Point(173, 82);
            this.cbCambioManual.Name = "cbCambioManual";
            this.cbCambioManual.Size = new System.Drawing.Size(108, 15);
            this.cbCambioManual.TabIndex = 114;
            this.cbCambioManual.Text = "Câmbio Manual";
            this.cbCambioManual.UseSelectable = true;
            this.cbCambioManual.CheckedChanged += new System.EventHandler(this.cbCambioManual_CheckedChanged);
            // 
            // cbImpostoSelo
            // 
            this.cbImpostoSelo.AutoSize = true;
            this.cbImpostoSelo.Location = new System.Drawing.Point(614, 82);
            this.cbImpostoSelo.Name = "cbImpostoSelo";
            this.cbImpostoSelo.Size = new System.Drawing.Size(108, 15);
            this.cbImpostoSelo.TabIndex = 115;
            this.cbImpostoSelo.Text = "Imposto de Selo";
            this.cbImpostoSelo.UseSelectable = true;
            // 
            // comboBoFormaPagamento
            // 
            this.comboBoFormaPagamento.FormattingEnabled = true;
            this.comboBoFormaPagamento.ItemHeight = 23;
            this.comboBoFormaPagamento.Location = new System.Drawing.Point(487, 78);
            this.comboBoFormaPagamento.Name = "comboBoFormaPagamento";
            this.comboBoFormaPagamento.Size = new System.Drawing.Size(121, 29);
            this.comboBoFormaPagamento.TabIndex = 119;
            this.comboBoFormaPagamento.UseSelectable = true;
            this.comboBoFormaPagamento.SelectedIndexChanged += new System.EventHandler(this.comboBoFormaPagamento_SelectedIndexChanged);
            // 
            // txtCambio
            // 
            this.txtCambio.Lines = new string[0];
            this.txtCambio.Location = new System.Drawing.Point(83, 78);
            this.txtCambio.MaxLength = 32767;
            this.txtCambio.Name = "txtCambio";
            this.txtCambio.PasswordChar = '\0';
            this.txtCambio.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCambio.SelectedText = "";
            this.txtCambio.Size = new System.Drawing.Size(84, 23);
            this.txtCambio.TabIndex = 118;
            this.txtCambio.UseSelectable = true;
            this.txtCambio.Leave += new System.EventHandler(this.txtCambio_Leave);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.labelNomeContaCaixa);
            this.groupBox4.Controls.Add(this.labelNomeContaCliente);
            this.groupBox4.Controls.Add(this.metroLabel5);
            this.groupBox4.Controls.Add(this.Caixa);
            this.groupBox4.Controls.Add(this.comboContaCaixa);
            this.groupBox4.Controls.Add(this.comboContaCliente);
            this.groupBox4.Location = new System.Drawing.Point(23, 585);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(762, 71);
            this.groupBox4.TabIndex = 133;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Contas Contabilísticas";
            // 
            // labelNomeContaCaixa
            // 
            this.labelNomeContaCaixa.AutoSize = true;
            this.labelNomeContaCaixa.Location = new System.Drawing.Point(614, 27);
            this.labelNomeContaCaixa.Name = "labelNomeContaCaixa";
            this.labelNomeContaCaixa.Size = new System.Drawing.Size(0, 0);
            this.labelNomeContaCaixa.TabIndex = 135;
            // 
            // labelNomeContaCliente
            // 
            this.labelNomeContaCliente.AutoSize = true;
            this.labelNomeContaCliente.Location = new System.Drawing.Point(287, 27);
            this.labelNomeContaCliente.Name = "labelNomeContaCliente";
            this.labelNomeContaCliente.Size = new System.Drawing.Size(0, 0);
            this.labelNomeContaCliente.TabIndex = 117;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(21, 27);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(49, 19);
            this.metroLabel5.TabIndex = 108;
            this.metroLabel5.Text = "Cliente";
            // 
            // Caixa
            // 
            this.Caixa.AutoSize = true;
            this.Caixa.Location = new System.Drawing.Point(430, 27);
            this.Caixa.Name = "Caixa";
            this.Caixa.Size = new System.Drawing.Size(41, 19);
            this.Caixa.TabIndex = 109;
            this.Caixa.Text = "Caixa";
            // 
            // comboContaCaixa
            // 
            this.comboContaCaixa.FormattingEnabled = true;
            this.comboContaCaixa.ItemHeight = 23;
            this.comboContaCaixa.Location = new System.Drawing.Point(487, 22);
            this.comboContaCaixa.Name = "comboContaCaixa";
            this.comboContaCaixa.Size = new System.Drawing.Size(121, 29);
            this.comboContaCaixa.TabIndex = 113;
            this.comboContaCaixa.UseSelectable = true;
            this.comboContaCaixa.SelectedIndexChanged += new System.EventHandler(this.comboContaCaixa_SelectedIndexChanged);
            // 
            // comboContaCliente
            // 
            this.comboContaCliente.FormattingEnabled = true;
            this.comboContaCliente.ItemHeight = 23;
            this.comboContaCliente.Location = new System.Drawing.Point(76, 22);
            this.comboContaCliente.Name = "comboContaCliente";
            this.comboContaCliente.Size = new System.Drawing.Size(205, 29);
            this.comboContaCliente.TabIndex = 116;
            this.comboContaCliente.UseSelectable = true;
            this.comboContaCliente.SelectedIndexChanged += new System.EventHandler(this.comboContaCliente_SelectedIndexChanged);
            // 
            // txtObservacao
            // 
            this.txtObservacao.Location = new System.Drawing.Point(23, 689);
            this.txtObservacao.Multiline = true;
            this.txtObservacao.Name = "txtObservacao";
            this.txtObservacao.Size = new System.Drawing.Size(281, 50);
            this.txtObservacao.TabIndex = 134;
            // 
            // labelObservacao
            // 
            this.labelObservacao.AutoSize = true;
            this.labelObservacao.Location = new System.Drawing.Point(19, 667);
            this.labelObservacao.Name = "labelObservacao";
            this.labelObservacao.Size = new System.Drawing.Size(85, 19);
            this.labelObservacao.TabIndex = 117;
            this.labelObservacao.Text = "Observações";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.labelValorRestanteEmUSD);
            this.groupBox5.Controls.Add(this.labelValorRestanteEmKZ);
            this.groupBox5.Controls.Add(this.metroLabel19);
            this.groupBox5.Controls.Add(this.metroLabel21);
            this.groupBox5.Location = new System.Drawing.Point(637, 210);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(148, 69);
            this.groupBox5.TabIndex = 132;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Valor Remanescente";
            // 
            // labelValorRestanteEmUSD
            // 
            this.labelValorRestanteEmUSD.AutoSize = true;
            this.labelValorRestanteEmUSD.Location = new System.Drawing.Point(89, 34);
            this.labelValorRestanteEmUSD.Name = "labelValorRestanteEmUSD";
            this.labelValorRestanteEmUSD.Size = new System.Drawing.Size(0, 0);
            this.labelValorRestanteEmUSD.TabIndex = 104;
            // 
            // labelValorRestanteEmKZ
            // 
            this.labelValorRestanteEmKZ.AutoSize = true;
            this.labelValorRestanteEmKZ.Location = new System.Drawing.Point(13, 34);
            this.labelValorRestanteEmKZ.Name = "labelValorRestanteEmKZ";
            this.labelValorRestanteEmKZ.Size = new System.Drawing.Size(0, 0);
            this.labelValorRestanteEmKZ.TabIndex = 103;
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(621, 30);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(0, 0);
            this.metroLabel19.TabIndex = 102;
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.Location = new System.Drawing.Point(423, 30);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(0, 0);
            this.metroLabel21.TabIndex = 100;
            // 
            // FormPagamentoFactura
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 762);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.labelObservacao);
            this.Controls.Add(this.txtObservacao);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.metroGrid1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "FormPagamentoFactura";
            this.Text = "Pagamento de Factura";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormPagamentoFactura_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btGravar;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton btSair;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.GroupBox groupBox2;
        private MetroFramework.Controls.MetroLabel labelNomeDoCliente;
        private MetroFramework.Controls.MetroLabel labelNumeroDoCliente;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel labelTotalValorEmDolares;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel labelTotalValorEmKwanzas;
        private MetroFramework.Controls.MetroLabel labelNumeroDaFactura;
        private MetroFramework.Controls.MetroTextBox txtIdFactura;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorPadrao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Moeda;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cambio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Operador;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private MetroFramework.Controls.MetroLabel labelValor;
        private MetroFramework.Controls.MetroTextBox txtValorReceBido;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroComboBox comboBoxMoeda;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroComboBox comboTipoPagamento;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroCheckBox cbCambioManual;
        private MetroFramework.Controls.MetroCheckBox cbImpostoSelo;
        private MetroFramework.Controls.MetroComboBox comboBoFormaPagamento;
        private MetroFramework.Controls.MetroTextBox txtCambio;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.GroupBox groupBox4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel Caixa;
        private MetroFramework.Controls.MetroComboBox comboContaCaixa;
        private MetroFramework.Controls.MetroComboBox comboContaCliente;
        private System.Windows.Forms.TextBox txtObservacao;
        private MetroFramework.Controls.MetroLabel labelObservacao;
        private MetroFramework.Controls.MetroLabel labelNomeContaCaixa;
        private MetroFramework.Controls.MetroLabel labelNomeContaCliente;
        private MetroFramework.Controls.MetroLabel labelNomeUtilizadorQueEmitiuFactura;
        private MetroFramework.Controls.MetroLabel labelDataDeCriacaoDoDocumento;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroCheckBox cbSaldarCompletamenteAFactura;
        private System.Windows.Forms.GroupBox groupBox5;
        private MetroFramework.Controls.MetroLabel labelValorRestanteEmKZ;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private MetroFramework.Controls.MetroLabel labelValorRestanteEmUSD;
    }
}