﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendasAnulaMotivo : MetroFramework.Forms.MetroForm
    {
        public FormVendasAnulaMotivo()
        {
            InitializeComponent();
        }

        private void FormVendasAnulaMotivo_Load(object sender, EventArgs e)
        {
            txtMotivo.Select();
        }
        private void btGravar_Click(object sender, EventArgs e)
        {
            FormVendas.lsAnulaMotivo = txtMotivo.Text;
            this.DialogResult = DialogResult.OK;
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void txtMotivo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                FormVendas.lsAnulaMotivo = txtMotivo.Text;
                this.DialogResult = DialogResult.OK;
            }
        }
    }
}
