﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS;
using SISTEC_PLUS;
using SISTEC_PLUS.OPS;
using System.IO;

namespace SISTEC_PLUS.POS
{
    public partial class FormTabelaFamiliaConsultar : MetroFramework.Forms.MetroForm
    {
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        string sql;
        public FormTabelaFamiliaConsultar()
        {
            InitializeComponent();
        }
        private void prenxeGrid()
        {
            sql = @"select CodFam, NomeMerc, ContVend, ContStok,ContCusto, Desconto from ASFamilia where CodFam like  '%" + txtCodigo.Text + "%' ";
            if (txtCodigo.Text != "")
                sql = sql + "and CodFam =  '" + txtCodigo.Text + "' ";
            if (txtNome.Text != "")
                sql = sql + "and NomeMerc =  '" + txtNome.Text + "'";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("Loja");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void gridDuploClick(DataGridView dataGrid)
        {
            FormTabelas.Fcodigo = dataGrid.CurrentRow.Cells[0].Value.ToString();
            FormTabelas.Fnome = dataGrid.CurrentRow.Cells[1].Value.ToString();
            FormTabelas.FcontabilizacaoVenda = dataGrid.CurrentRow.Cells[2].Value.ToString();
            FormTabelas.FcontabilizacaoStock = dataGrid.CurrentRow.Cells[3].Value.ToString();
            FormTabelas.FcontabilizacaoCusto = dataGrid.CurrentRow.Cells[4].Value.ToString();
            FormTabelas.Fdesconto = dataGrid.CurrentRow.Cells[5].Value.ToString();
            if (FormTabelas.Fcodigo != null)
            {
                SqlCommand cmd2 = new SqlCommand("select Prazo from ASFamilia where CodFam=@loja ", conexao);
                cmd2.Parameters.Add(new SqlParameter("@loja", FormTabelas.Fcodigo));
                conexao.Open();
                try { FormTabelas.Fpraso = (Int32)cmd2.ExecuteScalar(); }
                catch (Exception) { FormTabelas.Fpraso = 0; }

                conexao.Close();
            }
            try
            {
                SqlCommand cmdSelect = new SqlCommand("select IMAGEM from ASFamilia where CodFam =@ID ", conexao);
                cmdSelect.Parameters.Add("@ID", SqlDbType.Char, 4);
                cmdSelect.Parameters["@ID"].Value = FormTabelas.Fcodigo;

                conexao.Open();
                //byte[] barrImg = (byte[])cmdSelect.ExecuteScalar();
                FormTabelas.foto = (byte[])cmdSelect.ExecuteScalar();
                FormTabelas.Fimagem = Convert.ToString(DateTime.Now.ToFileTime());
                FileStream fs = new FileStream(FormTabelas.Fimagem, FileMode.CreateNew, FileAccess.Write);
                fs.Write(FormTabelas.foto, 0, FormTabelas.foto.Length);
                fs.Flush();
                fs.Close();
            }
            catch (Exception) { FormTabelas.Fimagem = null; }
            finally { conexao.Close(); }

            this.DialogResult = DialogResult.OK;
        }
        private void FormTabelaFamiliaConsultar_Load(object sender, EventArgs e)
        {
            metroGrid1.AutoGenerateColumns = false;
            prenxeGrid();
        }

        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    try { gridDuploClick(metroGrid1); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try { gridDuploClick(metroGrid1); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }
    }
}
