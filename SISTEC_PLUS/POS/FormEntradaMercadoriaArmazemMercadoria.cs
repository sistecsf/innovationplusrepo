﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;


namespace SISTEC_PLUS.POS
{
    public partial class FormEntradaMercadoriaArmazemMercadoria : MetroFramework.Forms.MetroForm
    {
        string CodFam, CodArmz, Referenc, QTD;

        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
       // SqlConnection conexao = new SqlConnection(@"Data Source=sisdbs1b;Initial Catalog=INNOVUIGE;User ID=admin;Password=admin"); // conexão com o banco

        SqlConnection conexao = new SqlConnection(Variavel.Conexao);

        public FormEntradaMercadoriaArmazemMercadoria()
        {
            InitializeComponent();

        }

        private void FormEntradaMercadoriaArmazemMercadoria_Load(object sender, EventArgs e)
        {
            metroGrid1.AutoGenerateColumns = false;
            prenxeGrid();
        }


    
        private void EntradaDataGrid()
        {

            //metroGrid1.Rows.Add(cmbReferencia1.Text, UNIDADE, txtQdt.Text, CodArmz, FOB1, PCA1, txtDesc.Text, txtTotal.Text, LOCALI, txtContagem.Text);
            //a++;

           
        }

        // FUNCAO PARA PREENCHER O DATAGRID
        private void prenxeGrid()
        {
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(@"select distinct M.CodFam,  M.CodArmz,Referenc,QTD,NomeMerc,NOMEARZ from ASMESTRE M WITH (NOLOCK), ASARMAZ AR WITH (NOLOCK) ,ASFamilia F WITH (NOLOCK) "+
                                                "WHERE Referenc = '" + FormEntradaMercadoria.REFERENC + "'  AND  M.CodLoja='" + Variavel.codLoja + "' AND F.CodFam=M.CODFAM AND m.CodArmz=AR.CodArmz", conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("Armazem");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
    
        string codigo;
        int count = 0;

        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    FormEntradaMercadoria.CodArmz = metroGrid1.CurrentRow.Cells[1].Value.ToString();
                    this.DialogResult = DialogResult.OK;
                }
            }
        }
     
    }
}