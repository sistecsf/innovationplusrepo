﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormTranferencias : MetroFramework.Forms.MetroForm
    {
        string nomeDaLoja; //valor passado no construtor do form
        string codigoDaLoja;//valor passado no construtor do form
        private string codigoDoArmazem;
        SqlConnection Conexao = new SqlConnection(Variavel.Conexao);
        private string nomeDoArmazem;

        private int numeroDaFactura;
        private double preco;
        private char movimentoST = '-';
        private char movimentoET = '+';
        private string idUnicoST;
        private string idUnicoET;
        private int numeroDoDocumentoST;
        private int numeroDoDocumentoET;

        private Hashtable registosDoDataGridView = new Hashtable();


        private int tabSeleccionado = 0;

        
        public FormTranferencias(string paramNomeDaloja)
        {
            InitializeComponent();
            this.nomeDaLoja = paramNomeDaloja;
        }

        /*
         * 
         * 
         *  Métodos utilitários 
         * 
         * 
         */
        public void getNovaColuna(DataGridView paramMetroGrid, string paramCabecalho , string paramValor)
        {
            int currentCollsNumber = 0;
            int counter = 0;
            if (counter == 0)
            {
                metroGridTransferenciaTab2.Columns.Add(new DataGridViewColumn(new DataGridViewTextBoxCell()));
                metroGridTransferenciaTab2.Columns[metroGridTransferenciaTab2.Columns.Count - 1].Name = paramCabecalho;
                metroGridTransferenciaTab2.Columns[metroGridTransferenciaTab2.CurrentRow.Cells.Count - 1].Visible = true;
                metroGridTransferenciaTab2.CurrentRow.Cells[metroGridTransferenciaTab2.CurrentRow.Cells.Count - 1].Value = paramValor;
                counter++;
                currentCollsNumber = metroGridTransferenciaTab2.CurrentRow.Cells.Count;
            }
            else if(counter > 0)
            {
                int column = 5;
                if (column <= currentCollsNumber)
                {
                    metroGridTransferenciaTab2.Columns[column].Visible = true;
                    metroGridTransferenciaTab2.CurrentRow.Cells[column].Value = paramValor;
                    counter++;
                    column++;
                }
                
            }
        }

        /*
         * 
         * METODOS DO NEGOCIO 
         * 
         * 
         *
         */
        
        //FUNCAO - getNomeDasLojas
        public List<string> getNomeDasLojas()
        {
            List<string> nomes = new List<string>();
            SqlCommand cmd = new SqlCommand("Select Distinct RTRIM(LTRIM(NomeLoja)) as nomeDaLoja From AsLOJA WHERE NomeLoja not in( '"+this.nomeDaLoja+"')", Conexao);
            Conexao.Open();
            SqlDataReader res = cmd.ExecuteReader();
            
            if (res.HasRows)
            {
                while (res.Read())
                {
                    nomes.Add(res["nomeDaLoja"].ToString());
                }
            }
            Conexao.Close();
            return nomes; 
        }

        //FUNCAO - getCodigoDaLoja
        public string getCodigoDaLoja(string nomeDaLoja)
        {
            SqlCommand cmd = new SqlCommand("Select Distinct RTRIM(LTRIM(CodLoja)) as codigoDaLoja From AsLOJA WITH (NOLOCK) where nomeLoja = @nomeDaLoja", Conexao);
            cmd.Parameters.Add("@nomeDaLoja", nomeDaLoja);
            Conexao.Open();
            string codLoja = cmd.ExecuteScalar().ToString();
            Conexao.Close();

            return codLoja;
        }

        //FUNCAO getArmazensDestino
        public List<string> getArmazens(bool E_ArmazemDestino = false, DataGridView arg = null)
        {
            //Lista para albergar o resultado da consulta
            List<string> nomesDosArmazens = new List<string>();
            string sql;
            sql = "SELECT DISTINCT RTRIM(LTRIM(NomeArz)) as nomeDoArmazem From ASARMAZ With (NoLock) Where CodLoja='"+this.codigoDaLoja+"' And Ativo='S' ";
            

            //Verifica se é para retornar so armazens de venda
            if (cbSoArmazensVenda.Checked == true)
            {
                sql += " And ARMA_VENDA='S' ";
            }

            //if it's destination container add more clauses
            if (E_ArmazemDestino)
            {
                DataGridView grid = new DataGridView();
                grid = arg;
                List<string> listaDeArmazens = new List<string>();

                //get the selecteds containers
                if (grid.Rows.Count > 0 && grid.Rows[0].Index != -1)
                {
                    for (int i = 0; i < grid.Rows.Count -1; i++ )
                    {
                        listaDeArmazens.Add(grid[0, i].Value.ToString());
                    }

                    sql += "  And RTRIM(LTRIM(NomeArz)) Not In ( ";
                    for (int i = 0; i < listaDeArmazens.Count; i++)
                    {
                        sql += " '" + listaDeArmazens[i].ToString() + "', ";

                        if (i == listaDeArmazens.Count - 1)
                        {
                            sql += " '" + listaDeArmazens[i].ToString() + "' ";
                        }
                    }
                }

                sql += ")";
            }

            SqlCommand cmd = new SqlCommand(sql, Conexao);
            
            //Abre a conexao e executa a consulta
            Conexao.Open();
            SqlDataReader res = cmd.ExecuteReader();

            if (res.HasRows)
            {
                while (res.Read())
                {
                    nomesDosArmazens.Add(res["nomeDoArmazem"].ToString());
                }
                Conexao.Close();
                return nomesDosArmazens;
            }
            else
            {
                return null;
            }   
        }

        //FUNCAO getReferencias
        public List<string> getReferencias(string codigoDoArmazem)
        {
            //Lista para albergar o resultado da consulta
            List<string> referencias = new List<string>();

            SqlCommand cmd = new SqlCommand("SELECT RTRIM(LTRIM(Referenc)) as referencias FROM ASMESTRE WITH (NOLOCK) WHERE CodLoja='" + this.codigoDaLoja + "' AND  CodArmz = '" + codigoDoArmazem + "' AND QTD > 0 AND CONTASTOCKS='S' AND ACTIVO = 'S' ORDER BY RTRIM(LTRIM(Referenc)) ", Conexao);
          
            //Abre a conexao e executa a consulta
            Conexao.Open();
            SqlDataReader res = cmd.ExecuteReader();

            if (res.HasRows)
            {
                while (res.Read())
                {
                    referencias.Add(res["referencias"].ToString());
                }
                Conexao.Close();
                return referencias;
            }
            else
            {
                return null;
            }
        }

        //FUNCAO getCodigoDoArmazem
        public string getCodigoDoArmazem(string nomeDoArmazem)
        {
            string codigoDoArmazem;
            SqlCommand cmd = new SqlCommand(" SELECT RTRIM(LTRIM(CodArmz)) as codAo FROM ASARMAZ WITH (NOLOCK) WHERE CodLoja=@codigoDaLoja AND NomeArz=@nomeDoArmazem",Conexao);
            cmd.Parameters.Add("@codigoDaLoja", this.codigoDaLoja);
            cmd.Parameters.Add("@nomeDoArmazem", nomeDoArmazem);

            //Open the connection securelly
            if (Conexao.State == ConnectionState.Closed) { Conexao.Open(); }
            
            string res = cmd.ExecuteScalar().ToString();
            codigoDoArmazem = res;
            //Close the connection securelly
            if (Conexao.State == ConnectionState.Open) { Conexao.Close(); }

            return codigoDoArmazem;
        }

        //FUNCAO getCodigoDaAreaOrganica
        public string getCodigoDaAreaOrganica(string codigoDoArmazem, string codigoDaLoja)
        {
            string codigoDaAreaOrganica;
            SqlCommand cmd = new SqlCommand(" SELECT LTRIM(RTRIM(CodAO)) as codAo FROM ASARMAZ WITH (NOLOCK) WHERE CODARMZ =@codigoDoArmazem AND CODLOJA=@codigoDaLoja", Conexao);
            Conexao.Open();
            string res = cmd.ExecuteScalar().ToString();

            codigoDaAreaOrganica = res;
            Conexao.Close();

            return codigoDaAreaOrganica;
        }

        //FUNCAO getDescricaoDoArtigo
        public string getDescricaoDoArtigo(string codigoDoArmazem)
        {
            string descricaoDoArtigo;
            SqlCommand cmd = new SqlCommand(" SELECT RTRIM(LTRIM(DESCRARTIGO)) as descricaoDoArtigo From ASMESTRE WITH (NOLOCK) Where CodLoja=@codigoDaloja And CodArmz=@codigoDoArmazem And QTD > 0 And CONTASTOCKS='S' And ACTIVO = 'S' Order By RTRIM(LTRIM(Referenc))", Conexao);
            cmd.Parameters.Add("@codigoDaLoja", this.codigoDaLoja);
            cmd.Parameters.Add("@codigoDoArmazem", codigoDoArmazem);

            Conexao.Open();
            string res = cmd.ExecuteScalar().ToString();

            descricaoDoArtigo = res;
            Conexao.Close();

            return descricaoDoArtigo;
        }


        //FUNCAO getDadosDoArtigo
        public Hashtable getDadosDoArtigo(string paramReferencia, string paramNomeDoArmazem)
        {
            this.codigoDoArmazem = this.getCodigoDoArmazem(paramNomeDoArmazem);

            Hashtable dadosDoArtigo = new Hashtable();

            SqlCommand cmd = new SqlCommand("SELECT * FROM ASMESTRE WHERE Referenc = @referencia and CodLoja = @codigoDaLoja and CodArmz = @codigoDoArmazem", Conexao);
            cmd.Parameters.Add("@referencia", paramReferencia);
            cmd.Parameters.Add("@codigoDaLoja", this.codigoDaLoja);
            cmd.Parameters.Add("@codigoDoArmazem", this.codigoDoArmazem);

            if (Conexao.State == ConnectionState.Closed) { Conexao.Open(); }

            SqlDataReader dadosVindosDoBanco = cmd.ExecuteReader();

            while (dadosVindosDoBanco.Read())
            {
                dadosDoArtigo.Add("referencia", dadosVindosDoBanco["REFERENC"].ToString());
                dadosDoArtigo.Add("codigoDaLoja", dadosVindosDoBanco["CODLOJA"].ToString());
                dadosDoArtigo.Add("codigoDoArmazem", dadosVindosDoBanco["CODARMZ"].ToString());
                dadosDoArtigo.Add("codigoDaFamilia", dadosVindosDoBanco["CODFAM"].ToString());
                dadosDoArtigo.Add("codigoDaSubfamilia", dadosVindosDoBanco["CODSUBFAM"].ToString());
                dadosDoArtigo.Add("descricaoDoArtigo", dadosVindosDoBanco["DESCRARTIGO"].ToString());
                dadosDoArtigo.Add("descricaoDoArtigo2", dadosVindosDoBanco["DescrArtigo2"].ToString());
                dadosDoArtigo.Add("cobrar", dadosVindosDoBanco["CODBAR"].ToString());
                dadosDoArtigo.Add("quantidade", dadosVindosDoBanco["QTD"].ToString());
                dadosDoArtigo.Add("FOB", dadosVindosDoBanco["FOB"].ToString());
                dadosDoArtigo.Add("PCA", dadosVindosDoBanco["PCA"].ToString());
                dadosDoArtigo.Add("PVP", dadosVindosDoBanco["PVP"].ToString());
                dadosDoArtigo.Add("localizacao", dadosVindosDoBanco["LOCALI"].ToString());
                dadosDoArtigo.Add("dataDeCriacao", dadosVindosDoBanco["DATACRIA"].ToString());
                dadosDoArtigo.Add("tempo", dadosVindosDoBanco["TEMPO"].ToString());
                dadosDoArtigo.Add("MTBF", dadosVindosDoBanco["MTBF"].ToString());
                dadosDoArtigo.Add("OBS2", dadosVindosDoBanco["OBS2"].ToString());
                dadosDoArtigo.Add("cambio", dadosVindosDoBanco["CAMBIO"].ToString());
                dadosDoArtigo.Add("cara", dadosVindosDoBanco["CARA"].ToString());
                dadosDoArtigo.Add("PONENC", dadosVindosDoBanco["PONENC"].ToString());
                dadosDoArtigo.Add("unidade", dadosVindosDoBanco["UNIDADE"].ToString());
                dadosDoArtigo.Add("desembalagem", dadosVindosDoBanco["DESEMBALAGEM"].ToString());
                dadosDoArtigo.Add("QUAEMBALAGEM", dadosVindosDoBanco["QUAEMBALAGEM"].ToString());
                dadosDoArtigo.Add("tara", dadosVindosDoBanco["TARA"].ToString());
                dadosDoArtigo.Add("descontoMaximo", dadosVindosDoBanco["DESCMAX"].ToString());
                dadosDoArtigo.Add("descontoMinimo", dadosVindosDoBanco["DESCMIN"].ToString());
                dadosDoArtigo.Add("percentagemDeQuebra", dadosVindosDoBanco["PERCQUEBRA"].ToString());
                dadosDoArtigo.Add("guardar", dadosVindosDoBanco["GUARDAR"].ToString());
                dadosDoArtigo.Add("podeAlterarPreco", dadosVindosDoBanco["ALTERAPRECO"].ToString());
                dadosDoArtigo.Add("podeContarStock", dadosVindosDoBanco["CONTASTOCKS"].ToString());
                dadosDoArtigo.Add("E_StockNegativo", dadosVindosDoBanco["STOCKNEGATIVO"].ToString());
                dadosDoArtigo.Add("nivelDeDesconto", dadosVindosDoBanco["NIVELDESC"].ToString());
                dadosDoArtigo.Add("STOCKCONSIG", dadosVindosDoBanco["STOCKCONSIG"].ToString());
                dadosDoArtigo.Add("stockMaximo", dadosVindosDoBanco["STOCKMAX"].ToString());
                dadosDoArtigo.Add("stockMinimo", dadosVindosDoBanco["STOCKMIN"].ToString());
                dadosDoArtigo.Add("stockTotal", dadosVindosDoBanco["STOCKTOTAL"].ToString());
                dadosDoArtigo.Add("fornecedor", dadosVindosDoBanco["FORNEC"].ToString());
                dadosDoArtigo.Add("OBS", dadosVindosDoBanco["OBS"].ToString());
                dadosDoArtigo.Add("dataDeFabrico", dadosVindosDoBanco["data_fabrico"].ToString());
                dadosDoArtigo.Add("PROMOCAO_I", dadosVindosDoBanco["PROMOCAO_I"].ToString());
                dadosDoArtigo.Add("PROMOCAO_F", dadosVindosDoBanco["PROMOCAO_F"].ToString());
                dadosDoArtigo.Add("PMC", dadosVindosDoBanco["PMC"].ToString());
                dadosDoArtigo.Add("imagem", dadosVindosDoBanco["IMAGEM"].ToString());
                dadosDoArtigo.Add("E_Anulado", dadosVindosDoBanco["ANULADO"].ToString());
                dadosDoArtigo.Add("norma", dadosVindosDoBanco["NORMA"].ToString());
                dadosDoArtigo.Add("codigoDoGrupo", dadosVindosDoBanco["COD_GRUPO"].ToString());
                dadosDoArtigo.Add("REGEQUIP", dadosVindosDoBanco["REGEQUIP"].ToString());
                dadosDoArtigo.Add("codigoDoUtilizador", dadosVindosDoBanco["CODUTILIZADOR"].ToString());
                dadosDoArtigo.Add("codigoDaSeccao", dadosVindosDoBanco["CODSECCAO"].ToString());
                dadosDoArtigo.Add("dataDeExpiracao", dadosVindosDoBanco["Data_Expira"].ToString());
                dadosDoArtigo.Add("lucro", dadosVindosDoBanco["Lucro"].ToString());
                dadosDoArtigo.Add("permiteOBS", dadosVindosDoBanco["Permitir_Obs"].ToString());
                dadosDoArtigo.Add("Activo", dadosVindosDoBanco["Activo"].ToString());
                dadosDoArtigo.Add("pontoDeEncomenda", dadosVindosDoBanco["PONTOENCOMENDA"].ToString());
                dadosDoArtigo.Add("CODMARCA", dadosVindosDoBanco["CODMARCA"].ToString());
                dadosDoArtigo.Add("CODMODELO", dadosVindosDoBanco["CODMODELO"].ToString());
                dadosDoArtigo.Add("impostoDeConsumo", dadosVindosDoBanco["ImpostoConsumo"].ToString());
            }
          
            if (Conexao.State == ConnectionState.Open) { Conexao.Close(); }
            return dadosDoArtigo;
        }

        //FUNCAO que retorna proximo IDUnico para doc no AREGDOC
        public string getProximoIdUnico()
        {
            var cmd = new SqlCommand("SELECT RTRIM(ISNULL(CODLOJA,'" + Variavel.codLoja + "')) +   CAST (ISNULL( MAX( CAST( RIGHT (RTRIM(IDUNICO), LEN(RTRIM(IDUNICO))-( LEN(ISNULL(CODLOJA,'" + Variavel.codLoja + "')) ) )AS INT ) ) , 1) + 1 AS VARCHAR(23)) AS IDUNICO FROM AREGDOC  WITH (XLOCK) WHERE CODLOJA='" + Variavel.codLoja + "' AND LEFT(IDUNICO, LEN(CODLOJA) ) = '" + Variavel.codLoja + "' GROUP BY CODLOJA", Conexao);
            if (Conexao.State == ConnectionState.Closed) { Conexao.Open(); }
            string res = cmd.ExecuteScalar() as string;
            if (string.IsNullOrEmpty(res)) { res = Variavel.codLoja + "1"; }
            if (Conexao.State == ConnectionState.Open) { Conexao.Close(); }
            return res;
        }

        //FUNCAO getProximoNumeroDeDocumento()
        public string getProximoNumeroDeDocumento(string parametro)
        {
            var cmd = new SqlCommand("Select IsNull(MAX(NUMDOC), 0) + 1 FROM AREGDOC With (NoLock) Where Coddoc In ( '" + parametro + "')  And Usar_MaxNumDoc = 'S'  Group By Coddoc, Usar_MaxNumDoc  Order By Max(NUMDOC) DESC", Conexao);
            Conexao.Open();
            var res = cmd.ExecuteScalar() as string;
            if (string.IsNullOrEmpty(res)) { res ="1"; }
            Conexao.Close();

            return res;
        }

        /*
         * INSERT NO AREGDOC 
         * 
         */
        private void insertNoAregDoc()
        {
            this.idUnicoST = this.getProximoIdUnico();
            this.idUnicoET = this.getProximoIdUnico();

            //verifica se ID's sao iguais para atribuir um id diferente
            if (this.idUnicoET == this.idUnicoST)
            {
                this.idUnicoST = (this.idUnicoST.Length - Variavel.codLoja.Length).ToString() + "1" + Variavel.codLoja;
            }

            this.numeroDoDocumentoST = Convert.ToInt32(getProximoNumeroDeDocumento(Gerais.PARAM_CODDOC_ST));
            this.numeroDoDocumentoET = Convert.ToInt32(getProximoNumeroDeDocumento(Gerais.PARAM_CODDOC_ET));

            //Sql ST
            string sql1 = "INSERT INTO AREGDOC (IDUNICO, NUMDOC, NUMFACT, CODDOC, CodEntidade,CODUTILIZADOR, DATACRIA, PRECO, PRECOD, CODLOJA, IDORIG, DATA_LANCAMENTO, STATUS) VALUES ('" + this.idUnicoST + "', " + this.numeroDoDocumentoST + ", " + this.numeroDoDocumentoST + ", " + Gerais.PARAM_CODDOC_ST + ", " + Gerais.PARAM_CODCLI_GERAL + ", '" + Variavel.codUtilizador + "', Left(Convert(varchar(20),GetDate(),120),20), 0, 0, '" + Variavel.codLoja + "', '" + this.idUnicoST + "', Left(Convert(varchar(10),GetDate(),120),10), 'E')";

            //adiciona a lista de consultas
            Variavel.ListaDeConsultas[0] = sql1;

            //Sql ET
            string sql2 = "INSERT INTO AREGDOC (IDUNICO, NUMDOC, NUMFACT, CODDOC, CodEntidade,CODUTILIZADOR, DATACRIA, PRECO, PRECOD, CODLOJA, IDORIG, DATA_LANCAMENTO, STATUS) VALUES ('" + this.idUnicoET + "', " + this.numeroDoDocumentoET + ", " + this.numeroDoDocumentoET + ", " + Gerais.PARAM_CODDOC_ET + ", " + Gerais.PARAM_CODCLI_GERAL + ", '" + Variavel.codUtilizador + "', Left(Convert(varchar(20),GetDate(),120),20), 0, 0, '" + Variavel.codLoja + "', '" + this.idUnicoET + "', Left(Convert(varchar(10),GetDate(),120),10), 'E')";

            //adiciona a lista de consultas
            Variavel.ListaDeConsultas[1] = sql2;

        }


        /*
         * INSERT NO ASFICMOV1 
         * 
         */
        private void insertNoASFICMOV()
        {
            txtNumeroDaGuiaTab1.Text = txtNumeroDaGuiaTab2.Text;
            txtNumeroDaGuiaTab2.Text = txtNumeroDaGuiaTab1.Text;

            string sql3 = "INSERT INTO ASFICMOV1 (CODLOJA, GUIA, DATACRIA, DATA_LANCAMENTO, FOB, PCLD, PVPD, PCL, PVP, CodEntidade, CODUTILIZADOR, CODAO, IDUNICO, NUMDOC, CODARMZ, REFERENC, ARMZDESTINO, QTD, X1, CODDOC, TIPMOV ) "
                            +" Values ("
                            + "'" + Variavel.codLoja +"',"
                            + "'" + txtNumeroDaGuiaTab1.Text + "'"
                            + "CONVERT(VARCHAR(20), GETDATE(), 120) ,"
                            + "CONVERT(VARCHAR(20), GETDATE(), 120) ,"
                            //FOB,
                            //PCA,
                            //

                            +")";
            
            //Continur depois de traze todos dos dados dos produtos da datagridView

        }

    
        /*
         * 
         * UPDATE no ASMestre
         * 
         * 
         */
        public void updateNoASMestre()
        {
            //sql4
            string sql4 = " UPDATE ASMESTRE SET  'PMC   = :nPMC,   DescrArtigo2 =:dlgTransf.tblArtigos.colDescr2 ,   FOB   = :dlgTransf.tblArtigos.colFOB,   PCA   = :dlgTransf.tblArtigos.colPCA   Where   CODLOJA=:sLoja AND   CODARMZ=:dfCodArmaRecep AND   REFERENC=:dlgTransf.tblArtigos.colRef '";
        }

        //verificarSeReferenciaExisteNoArmazem
        public bool referenciaExiste(string armazemDestino, string argReferencia)
        {
            SqlCommand cmd = new SqlCommand("SELECT CODLOJA, CODARMZ, REFERENC FROM ASMESTRE WITH (NOLOCK) Where CodLoja= '" + Variavel.codLoja + "'and CodArmz= '" + armazemDestino + "' and Referenc='" + argReferencia + "'", Conexao);
            if (ConnectionState.Closed == Conexao.State) { Conexao.Open(); };
            string referencia = (string) cmd.ExecuteScalar();
            if (ConnectionState.Open == Conexao.State) { Conexao.Close() ;}

            //Verifica se retornou alguma referencia
            if (!string.IsNullOrEmpty(referencia)) 
            { 
                return true; 
            }
            else
            {
                return false;
            }
        }

        /*
         * 
         * UPDATE na tabela ASMESTRE
         * Se a referencia ja existe usa a funcao para actualizar o PMC, Quantidade, PCA e FOB
         * 
         */
        public void actualizarASMESTRE(double paramPMC, string paramDescricao, double paramFOB, double paramPCA, string armazemRecepcao, string paramReferencia)
        {
            SqlCommand cmd = new SqlCommand("UPDATE ASMESTRE SET  PMC   = "+paramPMC+", DescrArtigo2 ='"+paramDescricao+"' ,   FOB   = "+paramFOB+",   PCA   = "+paramPCA+"   Where   CODLOJA= '"+Variavel.codLoja+"' AND   CODARMZ='"+armazemRecepcao+"' AND   REFERENC='"+paramReferencia+"' ", Conexao);
            if (ConnectionState.Closed == Conexao.State) { Conexao.Open(); }
            cmd.ExecuteNonQuery();
            if (ConnectionState.Open == Conexao.State) { Conexao.Close(); }
        }


        /*
         * 
         *  REPLICA DADOS da referencia caso ela exista
         * 
         *
         */



        /*
         * 
         * 
         * Calculo do PMC
         * 
         * 
         */
        private double calcularPMP(string paramCodigoDoArmazem, string paramReferencia, double paramPCA, int paramQuantidadeATransferir, string paramCodigoDaLoja)
        {
            int nQTDPMC = 0, nQTDTMP = 0, nQTDKeep = 0;
            double nPMC = 0, nNovo_PMC = 0;
            //======================= SELECT DO QTD E PREÇO MÉDIO =======================
            SqlCommand cmd = new SqlCommand("SELECT M.QTD, M.PMC From ASMESTRE M WITH (NOLOCK)Where M.CODLOJA = '" + paramCodigoDaLoja + "' And M.CODARMZ= '" + paramCodigoDoArmazem + "' And M.REFERENC= '" + paramReferencia + "'", Conexao);
            Conexao.Open();
            try
            {
                using (SqlDataReader reader = cmd.ExecuteReader())
                { 
                    while (reader.Read()) 
                    { 
                        nQTDTMP = Convert.ToInt32(reader[0].ToString()); 
                        nPMC = Convert.ToDouble(reader[0].ToString()); 
                    }
                }
            }
            catch (Exception) { nQTDTMP = 0; nPMC = 0; }
            finally { Conexao.Close(); }
            //======================== CALCULO PARA PREÇO MÉDIO ========================
            if (nQTDPMC < 0)
            {
                nQTDKeep = nQTDTMP;
                nQTDTMP = 0;
            }
            else if (nPMC == 0 && nQTDPMC == 0)
                nNovo_PMC = paramPCA;
            else
            {
                nNovo_PMC = (nPMC * nQTDPMC) + (paramPCA * paramQuantidadeATransferir);
                nQTDTMP = nQTDTMP + paramQuantidadeATransferir;
                nNovo_PMC = nNovo_PMC / nQTDTMP;
            }
            return nNovo_PMC;
        }

        /*
         * 
         * EXECUTAR transacao 
         * 
         * 
         */
        public void executarATransacao()
        {
            using (SqlConnection conexao = new SqlConnection(Variavel.Conexao))
            {
                
                SqlTransaction transacao;
                conexao.Open();
                transacao = conexao.BeginTransaction();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conexao;
                cmd.Transaction = transacao;

                try
                {
                    foreach (string query in Variavel.ListaDeConsultas)
                    {
                        if (query != null)
                        {
                            //Executa todas as queries da lista
                            cmd.CommandText = query;
                            cmd.ExecuteNonQuery();
                        }
                    }

                    //
                    transacao.Commit();
                    
                    //Mensagem de sucesso
                    MessageBox.Show("Operação efectuada com sucesso! ", "Innovation - Sucesso");

                }
                catch (SqlException erro)
                {
                    MessageBox.Show("Ooops! algo correu mal, tente de novo", "Innovation - Erro");
                    try
                    {
                        transacao.Rollback();
                    }
                    catch (Exception erroEmRollBack)
                    {
                        MessageBox.Show("Ooops! algo correu mal, tente de novo", "Innovation - Erro");
                    }
                }
                catch (Exception erroNaTransacao)
                {
                    MessageBox.Show("Ooops! algo correu mal, tente de novo", "Innovation - Erro");
                    try
                    {
                        transacao.Rollback();
                    }
                    catch (Exception erroEmRollBack)
                    {
                        MessageBox.Show("Ooops! algo correu mal, tente de novo", "Innovation - Erro");
                    }
                }
                finally
                {
                    Conexao.Close();
                }
            }
        }


        /*
         * 
         * 
         * Metodos utilitarios
         * 
         */
        //EliminarLinhasIncompletas
        public void eliminarLinhasIncompletas(DataGridView grid)
        {
            if (grid == null) { return; }
            //null cell confirmation
            bool ExisteCelulaNula = false;

            //Caso a grid tiver mais de uma linha, Ler as linhas do datagridView
            if (grid.RowCount > 1)
            {
                for (int i= 0; i <= grid.RowCount - 1; i++)
                {
                    
                    if (grid.Rows[i].Cells.Count> 1)
                    {
                        
                        for (int j = 0; j <= grid.Rows[i].Cells.Count -1; j++)
                        {
                            
                            if (grid.Rows[i].Cells[j].Value == null)
                            {
                                ExisteCelulaNula = true;

                                if (!grid.Rows[i].IsNewRow)
                                {
                                    grid.Rows.RemoveAt(i);
                                }
                            }
                        }

                        if (ExisteCelulaNula)
                        {
                            MessageBox.Show("Preencha as quantidades a transferir", "Innovation - Atenção");
                        }
                    }
                }
            }

            //Ler as celulas do gridView uma a uma

            //caso a ultima nao esteja preenchida elimine a linha
        }





        /*
         * 
         *  ACÇÕES NOS EVENTOS
         * 
         * 
         * 
         */

        private void FormTranferencias_Loaf(object sender, EventArgs e)
        {
            //precisa da data actual na tela das transferencias
            labelDataActualTab1.Text = DateTime.Now.ToShortDateString();
            labelDataActualTab2.Text = labelDataActualTab1.Text;
            labelDataActualTab3.Text = labelDataActualTab1.Text;
            
            //carrega usuario logado para mostrar na tela
            labelOperadorTab1.Text = Variavel.codUtilizador;
            labelOperadorTab2.Text = labelOperadorTab1.Text;
            labelOperadorTab3.Text = labelOperadorTab1.Text;
  
            //carrega o nome da loja nas labels
            labelLojaCorrente.Text = this.nomeDaLoja;
            labelNomeLojaTab1.Text = labelLojaCorrente.Text;
            labelNomeLojaTab2.Text = labelNomeLojaTab1.Text;
            labelNomeLojaTab3.Text = labelNomeLojaTab1.Text;

            //Oculta DatagridView dos registos dos produtos a transferir
            //metroGridRegistosAtransferir.Visible = false;

            //carrega o codigo da loja
            try
            {
                this.codigoDaLoja = getCodigoDaLoja(this.nomeDaLoja);
            }
            catch (SqlException erro)
            {
                MessageBox.Show("Erro no carregamento do codigo da loja: "+erro.Message,"Innovation - Erro");
            }
            finally
            {
                if (Conexao.State == ConnectionState.Open)
                {
                    Conexao.Close();
                }
            }
            

            //carrega os armazens na combo nas combos de origem
            try
            {
                List<string> armazens = new List<string>();
                armazens = getArmazens();
                if (armazens != null)
                {
                    foreach (string armazem in armazens)
                    {
                        dataGridViewComboBoxTab1Mercadoria.Items.Add(armazem);
                        dataGridViewComboBoxTab2Mercadoria.Items.Add(armazem);
                    } 
                }
            }
            catch(SqlException erro)
            {
                MessageBox.Show("Erro de consulta " + erro.Message, "Innovation - Erro");
            }
            catch(Exception erro)
            {
                MessageBox.Show("Erro na aplicação "+erro.Message, "Innovation - Erro");
            }
            finally
            {
                if (Conexao.State == ConnectionState.Open)
                {
                    Conexao.Close();
                }
            }
        }

        private void metroGrid2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            MessageBoxButtons botoes = MessageBoxButtons.YesNo;
            DialogResult mensagem = MessageBox.Show("Tem certeza que deseja eliminar linha?", "Innovation - Atenção!", botoes);

            if(mensagem == DialogResult.Yes)
            {
               
            }
            else
            {
                return;
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

       
        private void bntTransferir_Click(object sender, EventArgs e)
        {
            if (tabSeleccionado == 1)
            {
                eliminarLinhasIncompletas(metroGridTransferenciaTab1);
            }
            else if(tabSeleccionado == 2)
            {
                eliminarLinhasIncompletas(metroGridTransferenciaTab2);
            }
            
        }

        
        private void comboBoxLojasTab2_DropDown(object sender, EventArgs e)
        {
            try
            {
                foreach (string loja in this.getNomeDasLojas())
                {
                    this.comboBoxLojasTab2.Items.Add(loja);
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show("Erro ao carregar as lojas" + erro.Message + erro.StackTrace, "Innovatio - Erro");
            }
            finally
            {
                if (Conexao.State == ConnectionState.Open)
                {
                    Conexao.Close();
                }
            }
        }

        private void comboBoxMercadoria_dropDown(object sender, EventArgs e)
        {
            for (int i = 0; i <= metroGridTransferenciaTab1.Rows.Count - 1; i++ )
            {
                
            }
            
        }

        

        private void metroGridTransferenciaTab2_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            ComboBox comboBox = e.Control as ComboBox;

            if (comboBox != null)
            {
                comboBox.DropDown -= new EventHandler(comboBoxMercadoria_dropDown);
                comboBox.DropDown += new EventHandler(comboBoxMercadoria_dropDown);
            }
        }

        private void metroGridTransferenciaTab1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
                if (metroGridTransferenciaTab1.CurrentRow.Cells[0].Value != null)
                {
                    this.nomeDoArmazem = metroGridTransferenciaTab1.CurrentRow.Cells[0].Value.ToString();
                    
                    //Try to fill the comboBox with the references
                    try
                    {
                        this.codigoDoArmazem = getCodigoDoArmazem(this.nomeDoArmazem);

                        DataGridViewComboBoxCell combo = (DataGridViewComboBoxCell) metroGridTransferenciaTab1[1, metroGridTransferenciaTab1.CurrentRow.Index];

                        //Return all references from the database
                        if (this.getReferencias(this.codigoDoArmazem) != null)
                        {
                            combo.DataSource = this.getReferencias(this.codigoDoArmazem);                          
                        }
                    }
                    catch (Exception erro)
                    {
                        MessageBox.Show("Erro ao carregar referencias do armazem " + this.nomeDoArmazem + " Originou o seguinte erro: " + erro.Message + erro.StackTrace);
                    }
                    finally
                    {
                        if (Conexao.State == ConnectionState.Open)
                        {
                            Conexao.Close();
                        }
                    }


                    //try to fill the text box with the products fields
                    try
                    {
                        if (metroGridTransferenciaTab1.CurrentRow.Cells[1].Value != null)
                        {
                            Hashtable dadosDoArtigo = new Hashtable();
                            dadosDoArtigo = this.getDadosDoArtigo(metroGridTransferenciaTab1.CurrentRow.Cells[1].Value.ToString(), metroGridTransferenciaTab1.CurrentRow.Cells[0].Value.ToString());
                            metroGridTransferenciaTab1.CurrentRow.Cells[2].Value = dadosDoArtigo["stockMinimo"];
                            metroGridTransferenciaTab1.CurrentRow.Cells[3].Value = dadosDoArtigo["quantidade"];

                            
                        }
                    }
                    catch (SqlException erro)
                    {
                        MessageBox.Show("Ups! alguma coisa falhou durante a execução do programa. " + erro.Message, "Innovation - Erro ");
                    }
                    catch (ArgumentOutOfRangeException erro)
                    {
                        MessageBox.Show("Ups! alguma coisa falhou durante a execução do programa. " + erro.Message, "Innovation - Erro ");
                    }
                    catch (Exception erro)
                    {
                        MessageBox.Show("Ups! alguma coisa falhou durante a execução do programa. " + erro.Message, "Innovation - Erro ");
                    }
                    finally
                    {

                    }
            }
        }

        private void metroGridTransferenciaTab1_DataError(object sender, DataGridViewDataErrorEventArgs anError)
        {
            MessageBox.Show("Erro ao carregar dados \n Linha: "+anError.RowIndex + " Coluna: " + anError.ColumnIndex);
        }

        private void metroGridTransferenciaTab2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (metroGridTransferenciaTab2.CurrentRow.Cells[0].Value != null)
            {
                this.nomeDoArmazem = metroGridTransferenciaTab2.CurrentRow.Cells[0].Value.ToString();

                //Try to fill the comboBox with the references
                try
                {
                    this.codigoDoArmazem = getCodigoDoArmazem(this.nomeDoArmazem);

                    DataGridViewComboBoxCell combo = (DataGridViewComboBoxCell)metroGridTransferenciaTab2[1, metroGridTransferenciaTab2.CurrentRow.Index];

                    //Return all products references from the database
                    if (this.getReferencias(this.codigoDoArmazem) != null)
                    {
                        combo.DataSource = this.getReferencias(this.codigoDoArmazem);
                    }
                }
                catch (Exception erro)
                {
                    MessageBox.Show("Erro ao carregar referencias do armazem " + this.nomeDoArmazem + " Originou o seguinte erro: " + erro.Message + erro.StackTrace);
                }
                finally
                {
                    if (Conexao.State == ConnectionState.Open)
                    {
                        Conexao.Close();
                    }
                }

                //try to fill the text box with the products fields
                try
                {
                    if (metroGridTransferenciaTab2.CurrentRow.Cells[1].Value != null)
                    {
                        Hashtable dadosDoArtigo = new Hashtable();
                        dadosDoArtigo = this.getDadosDoArtigo(metroGridTransferenciaTab2.CurrentRow.Cells[1].Value.ToString(), metroGridTransferenciaTab2.CurrentRow.Cells[0].Value.ToString());
                        metroGridTransferenciaTab2.CurrentRow.Cells[2].Value = dadosDoArtigo["stockMinimo"];
                        metroGridTransferenciaTab2.CurrentRow.Cells[3].Value = dadosDoArtigo["quantidade"];
                    }
                }
                catch(SqlException erro)
                {
                    MessageBox.Show("Ups! alguma coisa falhou durante a execução do programa. "+erro.Message, "Innovation - Erro ");
                }
                catch(ArgumentOutOfRangeException erro)
                {
                    MessageBox.Show("Ups! alguma coisa falhou durante a execução do programa. " + erro.Message, "Innovation - Erro ");
                }
                catch (Exception erro)
                {

                    MessageBox.Show("Ups! alguma coisa falhou durante a execução do programa. " + erro.Message +erro.StackTrace, "Innovation - Erro ");

                }
                finally
                {

                }
            }
        }

        private void comboBoxArmazemDestinoTab1_DropDown(object sender, EventArgs e)
        {
            try
            {
                //get all of destinations containers
                comboBoxArmazemDestinoTab1.DataSource = this.getArmazens(true, metroGridTransferenciaTab1);
            }
            catch(SqlException erro)
            {
                MessageBox.Show("Erro ao carregar amazéns destino "+erro.Message,"Innovation - Erro");
            }
            finally
            {
                if (Conexao.State == ConnectionState.Open)
                {
                    Conexao.Close();
                }
            }
        }


        private void metroGridTransferenciaTab1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (metroGridTransferenciaTab1.CurrentRow.Cells[metroGridTransferenciaTab1.CurrentRow.Cells.Count - 2].Value != null)
            {
                int stockMaximo = Convert.ToInt32(metroGridTransferenciaTab1.CurrentRow.Cells[metroGridTransferenciaTab1.CurrentRow.Cells.Count - 2].Value.ToString());

                //Verifica se a celula da quantidade a digitar esta nula
                if (metroGridTransferenciaTab1.CurrentRow.Cells[metroGridTransferenciaTab1.CurrentRow.Cells.Count - 1].Value != null)
                {
                    int quantidadeATransferir = Convert.ToInt32(metroGridTransferenciaTab1.CurrentRow.Cells[metroGridTransferenciaTab1.CurrentRow.Cells.Count - 1].Value.ToString());

                    //Compara o valor digitado
                    if ((quantidadeATransferir > stockMaximo) && (quantidadeATransferir > 0))
                    {
                        MessageBox.Show("A quantidade que você digitou é superior ao stock existente ou igual a zero. Volte a digitar", "Innovation - Atenção");
                        metroGridTransferenciaTab1.CurrentRow.Cells[metroGridTransferenciaTab1.CurrentRow.Cells.Count - 1].Value = null;
                    }
                }

                tabSeleccionado = 1;
            }
        }

       
        private void metroGridTransferenciaTab2_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (metroGridTransferenciaTab2.CurrentRow.Cells[3].Value != null)
            {
                int stockMaximo = Convert.ToInt32(metroGridTransferenciaTab2.CurrentRow.Cells[3].Value.ToString());

                //Verifica se a celula da quantidade a transferir esta nula
                if (metroGridTransferenciaTab2.CurrentRow.Cells[4].Value != null)
                {
                    int quantidadeATransferir = Convert.ToInt32(metroGridTransferenciaTab2.CurrentRow.Cells[4].Value.ToString());

                    //Compara o valor digitado
                    if ((quantidadeATransferir > stockMaximo) && (quantidadeATransferir > 0))
                    {
                        MessageBox.Show("A quantidade que você digitou é superior ao stock existente ou igual a zero. Volte a digitar", "Innovation - Atenção");
                        metroGridTransferenciaTab2.CurrentRow.Cells[metroGridTransferenciaTab2.CurrentRow.Cells.Count - 1].Value = null;
                        
                    }
                    else
                    {
                        Hashtable dadosDoArtigo = new Hashtable();
                        dadosDoArtigo = this.getDadosDoArtigo(metroGridTransferenciaTab2.CurrentRow.Cells[1].Value.ToString(), metroGridTransferenciaTab2.CurrentRow.Cells[0].Value.ToString());

                        getNovaColuna(metroGridTransferenciaTab2, "FOB", dadosDoArtigo["FOB"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "Código de loja", dadosDoArtigo["codigoDaLoja"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "Código do armazém", dadosDoArtigo["codigoDoArmazem"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "Código da familia", dadosDoArtigo["codigoDaFamilia"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "Código da subfamilia", dadosDoArtigo["codigoDaSubfamilia"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "Descrição do artigo", dadosDoArtigo["descricaoDoArtigo"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "Descrição do artigo 2", dadosDoArtigo["descricaoDoArtigo2"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "Cobrar", dadosDoArtigo["cobrar"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "Quantidade", dadosDoArtigo["quantidade"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "FOB", dadosDoArtigo["FOB"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "PCA", dadosDoArtigo["PCA"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "PVP", dadosDoArtigo["PVP"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "Lcalização", dadosDoArtigo["localizacao"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "DataDeCriação", dadosDoArtigo["dataDeCriacao"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "Tempo", dadosDoArtigo["tempo"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "MTBF", dadosDoArtigo["MTBF"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "OBS2", dadosDoArtigo["OBS2"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "cambio", dadosDoArtigo["cambio"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "cara", dadosDoArtigo["cara"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "PONENC", dadosDoArtigo["PONENC"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "unidade", dadosDoArtigo["unidade"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "desembalagem", dadosDoArtigo["desembalagem"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "QUAEMBALAGEM", dadosDoArtigo["QUAEMBALAGEM"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "tara", dadosDoArtigo["tara"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "descontoMaximo", dadosDoArtigo["descontoMaximo"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "descontoMinimo", dadosDoArtigo["descontoMinimo"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "percentagemDeQuebra", dadosDoArtigo["percentagemDeQuebra"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "guardar", dadosDoArtigo["guardar"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "podeAlterarPreco", dadosDoArtigo["podeAlterarPreco"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "podeContarStock", dadosDoArtigo["podeContarStock"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "E_StockNegativo", dadosDoArtigo["E_StockNegativo"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "nivelDeDesconto", dadosDoArtigo["nivelDeDesconto"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "STOCKCONSIG", dadosDoArtigo["STOCKCONSIG"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "stockMaximo", dadosDoArtigo["stockMaximo"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "stockMinimo", dadosDoArtigo["stockMinimo"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "stockTotal", dadosDoArtigo["stockTotal"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "fornecedor", dadosDoArtigo["fornecedor"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "OBS", dadosDoArtigo["OBS"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "dataDeFabrico", dadosDoArtigo["dataDeFabrico"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "PROMOCAO_I", dadosDoArtigo["PROMOCAO_I"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "PROMOCAO_F", dadosDoArtigo["PROMOCAO_F"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "PMC", dadosDoArtigo["PMC"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "imagem", dadosDoArtigo["imagem"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "E_Anulado", dadosDoArtigo["E_Anulado"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "norma", dadosDoArtigo["norma"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "codigoDoGrupo", dadosDoArtigo["codigoDoGrupo"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "REGEQUIP", dadosDoArtigo["REGEQUIP"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "codigoDoUtilizador", dadosDoArtigo["codigoDoUtilizador"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "codigoDaSeccao", dadosDoArtigo["codigoDaSeccao"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "dataDeExpiracao", dadosDoArtigo["dataDeExpiracao"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "lucro", dadosDoArtigo["lucro"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "permiteOBS", dadosDoArtigo["permiteOBS"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "Activo", dadosDoArtigo["Activo"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "pontoDeEncomenda", dadosDoArtigo["pontoDeEncomenda"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "CODMARCA", dadosDoArtigo["CODMARCA"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "CODMODELO", dadosDoArtigo["CODMODELO"].ToString());
                        getNovaColuna(metroGridTransferenciaTab2, "impostoDeConsumo", dadosDoArtigo["impostoDeConsumo"].ToString());
                        
                    
                    }
                }

                tabSeleccionado = 2;

            }
        }
    }
}
