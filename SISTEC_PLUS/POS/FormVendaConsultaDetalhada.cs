﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendaConsultaDetalhada : MetroFramework.Forms.MetroForm
    {
        public static string dataIniacial, dataFim, sql, codVendedro, codLoja;
        DateTime data;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public FormVendaConsultaDetalhada()
        {
            InitializeComponent();
        }
        private void prenxerGrid()
        {
            sql = @"SELECT DISTINCT CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105), R.coddoc, D.Descrdoc AS TIPODOC, CAST(R.numdoc AS INT) AS NUMDOC, LTrim(RTrim(C.CodEntidade)) + ' - ' +" +
                              "LTrim(RTrim(C.NomeEntidade)) AS CLIENTE, STATUS = CASE R.Status WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO' WHEN 'F' THEN 'PAG. PARCIAL' WHEN 'J' THEN 'ADJUDICADO' WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END," +
                                "RTRIM(R.IDunico), U.NOMEUTILIZADOR, PRECOD FROM AFENTIDADE C WITH (NOLOCK) , ASDOCS D WITH (NOLOCK), ASFICMOV1 F WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), AREGDOC R WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK)" +
                                "WHERE R.IDunico = F.IDunico AND R.CodEntidade = C.CodEntidade and D.Coddoc = R.Coddoc AND M.codarmz = F.codarmz AND M.referenc = F.referenc AND M.codloja = F.codloja AND UPPER(D.TipoPos) = 'S' AND F.CODUTILIZADOR = U.CODUTILIZADOR ";
            if (Convert.ToInt32(Permissoes.nPrgPos) < 3)
                sql = sql + "AND R.codutilizador = '" + Variavel.codUtilizador + "'";
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                sql = sql + "AND CONVERT(DATETIME, R.DataCria, 120) > CONVERT(DATETIME, '" + dataIniacial + "', 120) AND CONVERT(DATETIME, R.DataCria, 120) <= CONVERT(DATETIME, '" + dataFim + " 18:59" + "', 120)";
            if (txtNumero.Text != "")
                sql = sql + " AND C.CodEntidade LIKE '" + txtNumero.Text + "%'";
            if (cmbLoja.Text != "" || cmbLoja.Text != "")
                sql = sql + " AND R.codloja = '" + codLoja + "'";
            if (txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '" + txtNome.Text + "%'";
            if (txtDoc.Text != "")
                sql = sql + " AND R.coddoc LIKE '" + txtDoc.Text + "%'";
            if (txtDescDoc.Text != "")
                sql = sql + " AND D.descrdoc LIKE '" + txtDescDoc.Text + "%'";
            if (txtNrDoc.Text != "")
                sql = sql + " AND R.numdoc = '" + txtNrDoc.Text + "'";
            if (txtCodArtigo.Text != "")
                sql = sql + " And F.Referenc Like '%" + txtCodArtigo.Text + "%'";
            if (txtDescArtigo.Text != "")
                sql = sql + " AND M.DescrArtigo LIKE '%" + txtDescArtigo.Text + "%' ";
            sql = sql + " ORDER BY CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME),105), R.coddoc, NUMDOC, CLIENTE, TIPODOC";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataAdapter.SelectCommand.CommandTimeout = 90000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                gridConsulta.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGridNSeries(string idunico)
        {
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(@"select R.DescrMarca, D.DescrModelo, F.REFERENC, F.NSERIE from AEFICMOV F, AEMODELO D, AEMarca R where F.CODMARCA = R.CODMARCA and F.CODMODELO = D.CODMODELO and F.IDUNICO = '" + idunico + "'", conexao);
                dataAdapter.SelectCommand.CommandTimeout = 90000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerCmbLoja()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeLoja from ASLOJA with (NoLock) order by NomeLoja");
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbLoja.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void gridDuploClick(DataGridView dataGrid, DataGridView dataGrid1, DataGridView dataGrid2)
        {
            lbCodigo.Text = dataGrid.CurrentRow.Cells[1].Value.ToString();
            lbDNumero.Text = dataGrid.CurrentRow.Cells[3].Value.ToString();
            lbTipo.Text = dataGrid.CurrentRow.Cells[2].Value.ToString();
            lbUSD.Text = dataGrid.CurrentRow.Cells[6].Value.ToString();
            dataGrid1.AutoGenerateColumns = false;
            prenxerGridNSeries(dataGrid.CurrentRow.Cells[5].Value.ToString());
            dataGrid2.AutoGenerateColumns = false;
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(@"select A.Referenc, M.DescrArtigo, A.PVP, A.QTD, A.PVP*A.QTD, A.Descont from ASFICMOV1 A with (NOLOCK), ASMESTRE M with (NOLOCK) where IDUNICO = '" + dataGrid.CurrentRow.Cells[5].Value.ToString() + "' AND A.Referenc = M.Referenc", conexao);
                dataAdapter.SelectCommand.CommandTimeout = 90000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dataGrid2.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }

            try
            {
                SqlCommand cmd = new SqlCommand("select D.CodEntidade, E.NomeEntidade, E.DESC1, AT.CAMBIO, D.PRECO, D.Moeda, D.CodVend from AREGDOC D WITH (NOLOCK), AFENTIDADE E WITH (NOLOCK), ASDOCS A WITH (NOLOCK), ATRECIBO AT WITH (NOLOCK) where D.IDUNICO = '" + dataGrid.CurrentRow.Cells[5].Value.ToString() + "' AND E.CodEntidade = D.CodEntidade AND A.CodDoc = D.CodDoc", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        lbNumero.Text = reader[0].ToString();
                        lbNome.Text = reader[1].ToString();
                        lbDesconto.Text = reader[2].ToString() + " %";
                        lbCambio.Text = reader[3].ToString();
                        lbKWZ.Text = reader[4].ToString();
                        lbMoeda.Text = reader[5].ToString();
                        codVendedro = reader[6].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void FormVendaConsultaDetalhada_Load(object sender, EventArgs e)
        {
            txtDataInicial.Text = txtDataFinal.Text = DateTime.Now.ToShortDateString();
            prenxerCmbLoja();
            cmbLoja.PromptText = Variavel.nomeLoja;
            codLoja = Variavel.codLoja;
            metroTabControl2.SelectedTab = metroTabPage9;
        }
        private void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                data = Convert.ToDateTime(txtDataInicial.Text);
                dataIniacial = "" + data.Year + "-" + data.Month + "-" + data.Day;
                try
                {
                    data = Convert.ToDateTime(txtDataFinal.Text);
                    dataFim = "" + data.Year + "-" + data.Month + "-" + data.Day;
                    gridConsulta.AutoGenerateColumns = false;
                    Cursor.Current = Cursors.WaitCursor;
                    prenxerGrid();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message + " Data Invalida " + txtDataFinal.Text); }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message + " Data Invalida " + txtDataInicial.Text); }
        }

        private void gridConsulta_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (gridConsulta.CurrentRow != null)
                {
                    try { gridDuploClick(gridConsulta, metroGrid1, gridDetalhe); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("select CodLoja from ASLOJA with (nolock) where NomeLoja = '" + cmbLoja.Text + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codLoja = reader[0].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }

        private void gridConsulta_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try { gridDuploClick(gridConsulta, metroGrid1, gridDetalhe); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void txtDataInicial_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception ex)
            {
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!");
                txtDataInicial.Text = "";
            }
        }

        private void txtDataFinal_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception ex)
            {
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!");
                txtDataFinal.Text = "";
            }
        }
    }
}
