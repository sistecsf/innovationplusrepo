﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS.POS
{
    public partial class FormPagamentoFactura : MetroFramework.Forms.MetroForm
    {
        //String de conexao para o form
        SqlConnection Conexao = new SqlConnection(Variavel.Conexao);

        string idUnicoDaFactura;
        string paramCodDoc;
        double debitoEmKZ = 0.0;
        double debitoEmUSD =0.0;
        string contraValor;
        string statusDocumento;
        string tipoDocumento;
        string numeroDaFactura;
        string numeroDoCliente;
        double valorRecebidoEmKZ; //Valor digitado pelo operador para efectuar pagamento da factura
        double valorRecebidoEmUSD;
        double valorPagoPeloClienteEmKZ = 0; //Valor que o cliente ja pagou parcelado em moeda nacional
        double valorPagoPeloClienteEmUSD = 0; //Valor que o cliente ja pagou parcelado em moeda padrao
        double valorRestanteEmKZ; //Valor remanescente da factura em KZ
        double valorRestanteEmUSD; //Valor remanescente da factura em USD

        double creditoDoCliente; //Credito do cliente baseado na factura
        double debitoDoCliente; //Debito do cliente baseado na factura
        double saldoDoCliente; //Saldo docliente de acordo com o seu debito e credito

        double diferecaDeDebitoEmKZNaFactura; //Diferenca que ocorre devido as alteracoes cambiais

        double cambioDaEmissaoDaFactura;
        string dataLimite;
        string codigoDaLojaOrigem;
        string observacao;
        string codigoDaAreaOrganicaOrigem;
        string descricaoDoDocumento;
        string observacaoDoRecibo;
        string codigoDoCaixa;
        double cambioActual = Convert.ToDouble(Variavel.cambio);
        string codigoDePagamento = "PF";
        int numeroDoDocumento;
        private double cambioManual;

        public static bool areaOrganicaFoiAlterada;
        public double debitoActualEmUSD;
        public double debitoActualEmKZ;
        private double valorEmKZParaNotaDeDebito;


        public FormPagamentoFactura()
        {
            InitializeComponent();
        }

        /*
         * 
         * METODOS UTILITARIOS
         * 
         */
        public static int getAnoCorrente()
        {
            DateTime dataActual = DateTime.Now;
            int anoCorrente = dataActual.Year;
            //define o nome da tabela
            return anoCorrente;
        }

        private DateTime getDataActual()
        {
            DateTime dataActual = DateTime.Now;
            return dataActual.Date;
        }


        //FUNCAO converterKwanzaEmDolar
        public double converterKwanzaEmDolar(double valor)
        {
            valor *= this.cambioActual;
            return valor;
        }


        //fUNCAO para carregar valor default da combobox
        public void carregarItemDefaultDaComboBox(ComboBox combo)
        {
            if (combo.SelectedIndex == -1)
            {
                combo.SelectedIndex = 0;
            }
        }


        //FUNCAO para limpar os campos do formulario
        public void limparCampos()
        {
            txtIdFactura.Clear();
            labelNomeDoCliente.Text = null;
            labelNumeroDaFactura.Text = null;
            labelNumeroDoCliente.Text = null;
            labelTotalValorEmDolares.Text = null;
            labelTotalValorEmKwanzas.Text = null;
            labelDataDeCriacaoDoDocumento.Text = null;
            labelValorRestanteEmKZ.Text = null;
            labelValorRestanteEmUSD.Text = null;
            labelNomeUtilizadorQueEmitiuFactura.Text = null;

            limparLinhasDagrid(metroGrid1);

        }

        //NOTA: Rever esse metodo
        public void limparLinhasDagrid(DataGridView grid)
        {
            //remove todas as linhas do datagrid
            if (grid.Rows.Count >= 1)
            {
                for (int i = 1; i <= grid.Rows.Count - 1; i++)
                {
                    if (!grid.Rows[i].IsNewRow)
                        grid.Rows.RemoveAt(i);
                }
            }
        }


        //FUNCAO para carregar tipos de pagameto de acordo a moeda
        public void retornarTipoDePagamentoDeAcordoAMoeda(int CodigoDaMoeda, ComboBox comboTipoPagamento)
        {
            switch(CodigoDaMoeda)
            {
                //se for Kwanza
                case 0:
                    comboTipoPagamento.Items.Clear();
                    comboTipoPagamento.Items.Add("Numerário");
                    comboTipoPagamento.Items.Add("Cheque");
                    carregarItemDefaultDaComboBox(comboTipoPagamento);
                    break;

                //Se for Dólar    
                case 1:
                    comboTipoPagamento.Items.Clear();
                    comboTipoPagamento.Items.Add("Outros");
                    carregarItemDefaultDaComboBox(comboTipoPagamento);
                    break;
            }
        }

        //FUNCAO que retorna o cambio actual
        public double retornarCambioActual(string moeda = "KZ")
        {
            DateTime data;
            data = DateTime.Now;
            string dataActual = data.Year + "-" + data.Month +"-"+ data.Day;

            SqlConnection Conexao = new SqlConnection(Variavel.Conexao);
            SqlCommand cmd = new SqlCommand("Select distinct A.Cambio     FROM AFCAMBIO A  WITH (NOLOCK)  where A.Datacria=(Select MAX(B.Datacria) from AFCAMBIO B  WITH (NOLOCK)   where B.Sigla=A.Sigla AND  LEFT(B.Datacria,10) <=@dataActual   AND A.Sigla=@moeda)", Conexao);
            cmd.Parameters.Add(new SqlParameter("dataActual", dataActual));
            cmd.Parameters.Add(new SqlParameter("moeda", moeda));
            Conexao.Open();

            var res = Convert.ToDouble(cmd.ExecuteScalar());
            Conexao.Close();
            return res;
        }


        /*
         * 
         * PAGAMENTO DA FACTURA 
         * 
         * 
         */

        //FUNCAO que retorna proximo IDUnico para doc no AREGDOC
        public string getProximoIdUnico()
        {
            var cmd = new SqlCommand("SELECT RTRIM(ISNULL(CODLOJA,'"+this.codigoDaLojaOrigem+"')) +   CAST (ISNULL( MAX( CAST( RIGHT (RTRIM(IDUNICO), LEN(RTRIM(IDUNICO))-( LEN(ISNULL(CODLOJA,'"+this.codigoDaLojaOrigem+"')) ) )AS INT ) ) , 1) + 1 AS VARCHAR(23)) AS IDUNICO FROM AREGDOC  WITH (XLOCK) WHERE CODLOJA='"+this.codigoDaLojaOrigem+"' AND LEFT(IDUNICO, LEN(CODLOJA) ) = '"+this.codigoDaLojaOrigem+"' GROUP BY CODLOJA", Conexao);
            Conexao.Open();
            string  res = cmd.ExecuteScalar() as string;
            Conexao.Close();
            return res;    
        }

        //FUNCAO getProximoNumeroDaFactura()
        public string getProximoNumeroDaFactura()
        {
            var cmd = new SqlCommand("Select IsNull(MAX(NUMDOC), 0) + 1 FROM AREGDOC With (NoLock) Where Coddoc In ( '"+Gerais.PARAM_CODDOC_PP+"', '"+Gerais.PARAM_CODDOC_PN+"')  And Usar_MaxNumDoc = 'S'  Group By Coddoc, Usar_MaxNumDoc  Order By Max(NUMDOC) DESC", Conexao);
            Conexao.Open();
            var res = cmd.ExecuteScalar() as string;
            Conexao.Close();

            return res;
        }

        //FUNCAO getDadosDaFacturaPorPagar()
        public bool getDadosDaFactura(string idUnico)
        {
            SqlCommand cmd = new SqlCommand(" SELECT  D.Coddoc, A.Tipodoc, D.numFact as numeroDaFactura, RTRIM(D.Status) as status, D.idUnico, C.Debito as DebitoEmKZ, C.Debitod as debitoEmUSD, CONVERT (VARCHAR (10), CAST(D.Datacria AS DATETIME), 105) as dataCriacao, Data_Limite as dataLimite, U.login as nomeUtilizador, D.CodAO as codigoDaAreaOrganica, A.DescrDoc as descricaoDoDocumento, C.CAMBIO, RTRIM(D.CODLOJA) as codLoja, D.CodEntidade, (select nomeEntidade from afentidade where codEntidade = D.CodEntidade) as nomeEntidade FROM AREGDOC D  WITH (NOLOCK) , UTILIZADORES U  WITH (NOLOCK) ,  ASDOCS A  WITH (NOLOCK) , AFCONCLI C  WITH (NOLOCK) WHERE D.idUnico = @idUnicoDaFactura AND D.CodEntidade= C.CodEntidade AND D.IDUNICO = C.IDUNICO AND U.Codutilizador=D.Codutilizador AND D.Coddoc = A.Coddoc AND (ISNULL(D.STATUS, 'E')='E' OR D.STATUS='F')", Conexao);
            cmd.Parameters.Add(new SqlParameter("@idUnicoDaFactura", idUnico));

            Conexao.Open();
            SqlDataReader res = cmd.ExecuteReader();

            if (res.HasRows)
            {
                while (res.Read())
                {
                    labelNomeDoCliente.Text = res["nomeEntidade"].ToString();
                    labelNumeroDaFactura.Text = res["numeroDaFactura"].ToString().Trim();
                    labelNumeroDoCliente.Text = res["CodEntidade"].ToString();
                    this.numeroDoCliente = res["CodEntidade"].ToString().Trim();
                    this.paramCodDoc = res["codDoc"].ToString();
                    labelTotalValorEmKwanzas.Text = res["DebitoEmKZ"].ToString();
                    string resDebitoEmKz = ((res["DebitoEmKZ"].ToString() == "") || (res["DebitoEmKZ"].ToString() == null))? "0": (res["DebitoEmKZ"].ToString());
                    this.debitoEmKZ = Convert.ToDouble(resDebitoEmKz);
                    labelTotalValorEmDolares.Text = res["debitoEmUSD"].ToString();
                    string resDebitoUSD = ((res["debitoEmUSD"].ToString() == "") || (res["debitoEmUSD"].ToString() == null)) ? "0" : (res["debitoEmUSD"].ToString());
                    this.debitoEmUSD = Convert.ToDouble(resDebitoUSD);
                    this.cambioDaEmissaoDaFactura = Convert.ToDouble(res["cambio"].ToString());
                    labelNomeUtilizadorQueEmitiuFactura.Text = res["nomeUtilizador"].ToString();

                    this.statusDocumento = res["status"].ToString();
                    this.tipoDocumento = res["tipoDoc"].ToString();
                    this.numeroDaFactura = res["numeroDaFactura"].ToString().Trim();
                    this.idUnicoDaFactura = res["idUnico"].ToString().Trim();

                    labelDataDeCriacaoDoDocumento.Text = res["dataCriacao"].ToString();

                    this.dataLimite = res["dataLimite"].ToString();
                    this.codigoDaLojaOrigem = res["codLoja"].ToString();
                    this.codigoDaAreaOrganicaOrigem = res["codigoDaAreaOrganica"].ToString().Trim();
                    this.descricaoDoDocumento = res["descricaoDoDocumento"].ToString();
                }
                Conexao.Close();
                return true;
            }
            else
            {
                Conexao.Close();
                return false;
            }
            
        }

        //FUNCAO getHistoricoDaFactura() - retorna o historico de pagamentos da factura
        public void getHistoriocDaFactura(string idUnicoDaFactura, string numeroDoCliente, DataGridView grid)
        {
            SqlCommand cmd = new SqlCommand(" SELECT  A.Datacria,  A.Preco, A.Precod,  A.Moeda, ROUND(A.PRECO / A.PRECOD, 0) AS CAMBIO, U.NomeUtilizador From Aregdoc A  WITH (NOLOCK) , Aregdoc P  WITH (NOLOCK) , Utilizadores U WITH (NOLOCK) Where  U.Codutilizador = A.Codutilizador AND P.CodEntidade  = A.CodEntidade AND P.IDUNICO  = A.IDOrig AND A.CodEntidade  = '" + numeroDoCliente + "' AND A.IDOrig  = '" + idUnicoDaFactura + "' AND A.IDUnico  != '" + idUnicoDaFactura + "' ORDER BY A.DATACRIA DESC", Conexao);
            //cmd.Parameters.Add(new SqlParameter("@idUnicoDaFactura", idUnicoDaFactura));
            //cmd.Parameters.Add(new SqlParameter("@numeroDoCliente", numeroDoCliente));

            Conexao.Open();
            var res = cmd.ExecuteReader();
            //verifica se retornou dados
            if(res.HasRows)
            {
                //Retorna o historico da factura
                while (res.Read())
                {
                    grid.Rows.Add(new string[]{res["dataCria"].ToString(),res["PrecoD"].ToString(), res["moeda"].ToString(),res["Cambio"].ToString(), res["NomeUtilizador"].ToString()});
                }
            }
            Conexao.Close();

        }

        //FUNCAO getValorRestante() que retorna o valor remanescente da factura por pagar
        public List<double> getValorPagoPeloCliente(string idUnicoDaFactura, string numeroDaFactura)
        {
            var cmd = new SqlCommand("SELECT PRECO_P - PRECO_S 'valorRestanteEmKZ', PRECOD_P - PRECOD_S  'valorRestanteEmUSD' FROM (SELECT C.DEBITO AS PRECO_P FROM AFCONCLI C  WITH (NOLOCK) WHERE C.IDUNICO= @idUnicoDaFactura)  PRECO_P, (SELECT C.DEBITOD AS PRECOD_P FROM AFCONCLI C  WITH (NOLOCK) WHERE C.IDUNICO= @idUnicoDaFactura) PRECOD_P,(SELECT SUM(PRECO) AS PRECO_S FROM AREGDOC D WITH (NOLOCK), ASDOCS A   WITH (NOLOCK) WHERE NUMFACT = @numeroDaFactura AND D.CODDOC = A.CODDOC AND STATUS<>'A' AND D.IDORIG= @idUnicoDaFactura) PRECO_S,(SELECT SUM(PRECOD) AS PRECOD_S FROM AREGDOC D   WITH (NOLOCK) , ASDOCS A   WITH (NOLOCK) WHERE NUMFACT = @numeroDaFactura AND D.CODDOC = A.CODDOC AND STATUS<>'A' AND D.IDORIG=@idUnicoDaFactura) PRECOD_S", Conexao);
            cmd.Parameters.Add(new SqlParameter("@idUnicoDaFactura", idUnicoDaFactura));
            cmd.Parameters.Add(new SqlParameter("@numeroDaFactura", numeroDaFactura));
            //cmd.Parameters.Add(new SqlParameter("@tipoDoDocumento", tipoDoDocumento));

            Conexao.Open();

            var res = cmd.ExecuteReader();
            List<double> Valores = new List<double>();

            if (res.HasRows)
            {
                while (res.Read())
                {
                    string resRestanteEmKZ = ((res["valorRestanteEmKZ"].ToString() == "") || (res["valorRestanteEmKZ"].ToString() == null)) ? "0" : res["valorRestanteEmKZ"].ToString();
                    
                    string resRestanteEmUSD = ((res["valorRestanteEmUSD"].ToString() == "") || (res["valorRestanteEmUSD"].ToString() == null)) ? "0" : res["valorRestanteEmUSD"].ToString();

                    Valores.Add(Convert.ToDouble(resRestanteEmKZ));
                    Valores.Add(Convert.ToDouble(resRestanteEmUSD));
                }
                Conexao.Close();
                return Valores;
            }
            else
            {
                Valores = null;
                Conexao.Close();
                return Valores;
            }
           
        }

        //FUNCAO getIdUnicoParaNotaDeDebito
        public string getIdUnicoParaNotaDeDebito()
        {
            SqlCommand cmd = new SqlCommand(" EXEC SP_GET_IDUNICO @p_sCodLoja", Conexao);
            cmd.Parameters.Add(new SqlParameter("@p_sCodLoja", Variavel.codLoja));

            Conexao.Open();
            var res = (string)cmd.ExecuteScalar();
            Conexao.Close();

            //verifica se o retorno foi nulo
            if ((res == "") || (res == null))
            {
               return  res = (Variavel.codLoja).Trim() + "1";
            }

            return res;
        }

        //FUNCAO getNumeroDePagamentosEfectuados()
        public int getNumeroDePagamentosEfectuados(string numeroDoCliente, string idUnicoDaFactura)
        {
            SqlCommand cmd = new SqlCommand(" SELECT COUNT(A.DataCria) as numeroDePagamentos From Aregdoc A  WITH (NOLOCK) , Aregdoc P  WITH (NOLOCK) , Utilizadores U  WITH (NOLOCK) Where  U.Codutilizador = A.Codutilizador AND P.CodEntidade  = A.CodEntidade AND P.IDUNICO  = A.IDOrig AND A.CodEntidade  = '" + numeroDoCliente + "' AND A.IDOrig  = '" + idUnicoDaFactura + "' AND A.IDUnico  != '" + idUnicoDaFactura + "'", Conexao);
            //cmd.Parameters.Add(new SqlParameter("@numeroDoCliente", numeroDoCliente));
            //cmd.Parameters.Add(new SqlParameter("@idUnicoDaFactura", idUnicoDaFactura));

            Conexao.Open();
            var res = (int)cmd.ExecuteScalar();
            Conexao.Close();

            return res;
        }

        //FUNCAO getNumeroDaNotaDeDebito()
        public string getNumeroDaNotaDeDebito()
        {
            string numeroDaNotaDeDebito;
            numeroDaNotaDeDebito = getNumeroDePagamentosEfectuados(this.numeroDoCliente, this.idUnicoDaFactura).ToString();

            return numeroDaNotaDeDebito;
        }


        //FUNCAO getDescricao do documento em dolares ou em kwanzas
        //retorna a descricao do documento no ASDOCS
        public string getDescricaoDoDocumento(string param_coddoc_moeda)
        {
            var cmd = new SqlCommand("Select RTRIM(DescrDoc) from ASDOCS  WITH (NOLOCK) Where Coddoc = @para_coddoc_moeda", Conexao);
            cmd.Parameters.Add(new SqlParameter("@para_coddoc_moeda", param_coddoc_moeda));

            Conexao.Open();
            string res = cmd.ExecuteScalar() as string;
            Conexao.Close();

            return res;
        }

        /*
         * 
         * 
         * 
         * ###########################################################
         * 
         *  EXECTRANS
         * 
         * ###########################################################
         * 
         * 
         */

        public void executarATransacao()
        {
            using (SqlConnection conexao = new SqlConnection(Variavel.Conexao))
            {
                //idUnico do recibo
                string idUnicoDoRecibo = this.getProximoIdUnico();

                SqlTransaction transacao;
                conexao.Open();
                transacao = conexao.BeginTransaction();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conexao;
                cmd.Transaction = transacao;

                try
                {
                    foreach(string query in Variavel.ListaDeConsultas)
                    {
                        if (query != null)
                        {
                            //Executa todas as queries da lista
                            cmd.CommandText = query;
                            cmd.ExecuteNonQuery();
                        }
                    }
                    
                    //Funcao verificadora de emissao de defactura de 
                    if (this.deveEmitirFacturaDeCorreccaoCambial())
                    {
                        this.emitirFacturaDeCorrecaoCambial();
                    }
                   
                    transacao.Commit();
                    //Retorna True

                    //Imprime a nota de debito
                    MessageBox.Show("Operação efectuada com sucesso! " + idUnicoDoRecibo, "Innovation - Sucesso");

                    //Emite recibo de pagamento de factura
                    reportViewReciboPagamentoDeFactura recibo = new reportViewReciboPagamentoDeFactura(idUnicoDoRecibo);
                    recibo.ShowDialog();
                    
                    //limpa os campos
                    this.limparCampos();

                }
                catch (SqlException erro)
                {
                    MessageBox.Show("Ooops! algo correu mal, tente de novo", "Innovation - Erro");
                    try
                    {
                        transacao.Rollback();
                    }
                    catch (Exception erroEmRollBack)
                    {
                        MessageBox.Show("Ooops! algo correu mal, tente de novo", "Innovation - Erro");
                    }
                }
                catch(Exception erroNaTransacao)
                {
                    MessageBox.Show("Ooops! algo correu mal, tente de novo", "Innovation - Erro");
                    try
                    {
                        transacao.Rollback();
                    }
                    catch(Exception erroEmRollBack)
                    {
                        MessageBox.Show("Ooops! algo correu mal, tente de novo", "Innovation - Erro");
                    }
                }
                finally
                {
                    Conexao.Close();
                }
                
            }
        }


        public void transacao(ComboBox comboMoeda){
            
            string PARAM_COD_DOC;
            double valorRecebido;

            //verifica a moeda escolhida
            if (comboBoxMoeda.SelectedItem.ToString() == Gerais.PARAM_MOEDA_PDR)
            {
                PARAM_COD_DOC = Gerais.PARAM_CODDOC_PP;

                valorRecebido = this.valorRecebidoEmKZ;
                
                getDescricaoDoDocumento(Gerais.PARAM_CODDOC_PP);
            }
            else
            {
                PARAM_COD_DOC = Gerais.PARAM_CODDOC_PN;

                getDescricaoDoDocumento(Gerais.PARAM_CODDOC_PN);

                valorRecebido = this.valorRecebidoEmUSD;
            }
            //insert no atrecibo
            insertNoAtRecibo();

            //update no afconcli
            updateNoAfConcli();

        }

        

        //INSERT no ATRECIBO
        public void insertNoAtRecibo()
        {
            //sql8
            this.descricaoDoDocumento = "Factura Nº " + this.numeroDaFactura;
            string cmd = "INSERT INTO ATRECIBO  (IDUnico, ValorRec, NumRecibo, DescRec, Coddoc, DataCria,  Data_Lancamento, Cambio, Forma_Pagamento, CodEntidade, Codutilizador)  VALUES ('" + getProximoIdUnico() + "', " + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + ", " + Convert.ToInt32(this.getProximoNumeroDaFactura()) + ", '" + descricaoDoDocumento + "', '" + Gerais.PARAM_CODDOC_PN + "', CONVERT(VARCHAR(20), GETDATE(), 120),  CONVERT (VARCHAR (10) , GETDATE ( ) , 120), " + cambioActual + ", '" + comboBoFormaPagamento.SelectedItem.ToString() + "', '" + this.numeroDoCliente + "', '" + Variavel.codUtilizador + "')";
            //adiciona na lista de consultas
            Variavel.ListaDeConsultas[7] = cmd;
        }

        //UPDATE no AFCONCLI
        public void updateNoAfConcli()
        {
            if (FormPagamentoFactura.areaOrganicaFoiAlterada)
            {
                //sql9
                string cmd = "UPDATE AFCONCLI SET DOC=" + int.Parse(this.numeroDaFactura) + ", CODCAIXA = '" + Variavel.codUtilizador + "' CODAO = '" + Variavel.codArea + "', DATA_PAG= CONVERT(varchar(20), GETDATE(), 120) WHERE IDUNICO='" + this.idUnicoDaFactura + "'";
                //adiciona na lista de consultas
                Variavel.ListaDeConsultas[9] = cmd;
            }
            else
            {
                //sql9
                string cmd = "UPDATE AFCONCLI SET DOC="+int.Parse(this.numeroDaFactura)+", CODCAIXA = '"+Variavel.codUtilizador+"', DATA_PAG= CONVERT(varchar(20), GETDATE(), 120) WHERE IDUNICO='"+this.idUnicoDaFactura+"'";
                //adiciona na lista de consultas
                //Variavel.ListaDeConsultas[9] = cmd;
            }
        }


        /*
        * 
        * 
        * 
        * ###########################################################
        * 
        *  FIM EXECTRAS
        * 
        * ###########################################################
        * 
        * 
        */



        //FUNCAO pagarImpostoDeSelo(string tipoParam, moeda, double valorEmKZ, double ValorEmUSD, string codEntidade, string codDocumento, string formaDePagamento)
        public void pagarImpostoDeSelo(ComboBox moeda)
        {
            string tipoParam;
            //verifica o tipo de moeda passada no parametro
            if (moeda.SelectedItem.ToString() == Gerais.PARAM_MOEDA_NAC)
            {
                tipoParam = Gerais.PARAM_CODDOC_PN;
            }
            else
            {
                tipoParam = Gerais.PARAM_CODDOC_PP;
            }

            //Retorna a forma de pagamento
            string formaDePagamento = comboBoFormaPagamento.SelectedItem.ToString();
            if (cbImpostoSelo.Checked)
            {
                //sql8
                string cmd = "INSERT into  ATSELOS(IDUNICO, NDOC, VALOR, VALORD, DATACRIA, DATA_LANCAMENTO, CodEntidade, CODDOC, FORMA_PAGAMENTO, CODUTILIZADOR, CAMBIO)      values ( '" + getProximoIdUnico() + "', '" + this.numeroDaFactura + "', " + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + "," + this.valorRecebidoEmUSD.ToString().Replace(',', '.') + ", CONVERT (VARCHAR (20) , GETDATE ( ) , 120), CONVERT (VARCHAR (10) , GETDATE ( ) , 120), '" + this.numeroDoCliente + "', '" + tipoParam + "', '" + formaDePagamento + "', '" + Variavel.codUtilizador + "', " + this.cambioActual+")";
                Variavel.ListaDeConsultas[8] = cmd;
            }
        }

        //FUNCAO gerarNotaDeDebito()
        public void gerarNotaDeDebito()
        {
            string obServecao        = "Actualização Cambial da Factura n.º"+this.numeroDaFactura+" - DOC. INTERNO ";
            string idUnicoNovo       = getNumeroDaNotaDeDebito();
            int numeroDoDocumento    = Convert.ToInt32(numeroDaFactura + getNumeroDePagamentosEfectuados(numeroDoCliente, idUnicoDaFactura));
            string descricaoDoDocumento = getDescricaoDoDocumento(Gerais.PARAM_CODDOC_CC);

            //insert no aregDoc
            string cmd = "INSERT INTO AREGDOC (IDUnico, Datacria, Data_Lancamento, Data_Limite, Codutilizador, Coddoc, Descr, CodEntidade, Codloja, CodAO, Numdoc, Numfact, Preco, STATUS, Moeda, OBS, IDOrig) Values ('" + this.getProximoIdUnico() + "', CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), '" + this.dataLimite + "', '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_CC + "', '" + this.descricaoDoDocumento + "', '" + this.numeroDoCliente + "', '" + this.codigoDaLojaOrigem + "', '" + Variavel.codArea + "', " + this.numeroDoDocumento + ", '" + this.numeroDaFactura + "', " + this.valorEmKZParaNotaDeDebito.ToString().Replace(',', '.') + ", 'E', '" + Gerais.PARAM_CODDOC_PN + "', '" + this.observacao + "', RTRIM(LTRIM('" + this.idUnicoDaFactura + "')))";
            //adiciona a lista das queries
            Variavel.ListaDeConsultas[Variavel.ListaDeConsultas.Length +1] = cmd;
            //insert no afconcli
            string cmd2 = "INSERT Into AFCONCLI (IDUnico, Descricao, Valor, Cambio,  Datacria, Data_Lancamento, Debito, CodEntidade, CodUtilizador, CodAO, CodDoc) Values ('" + this.getProximoIdUnico() + "', '" + this.observacao + "', " + this.valorEmKZParaNotaDeDebito.ToString().Replace(',', '.') + ", " + this.cambioActual + ", CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), " + this.valorEmKZParaNotaDeDebito.ToString().Replace(',', '.') + ", '" + this.numeroDoCliente + "', '" + Variavel.codUtilizador + "', '" + Variavel.codArea + "', '" + Gerais.PARAM_CODDOC_CC + "')";
            //adiciona a lista das queries
            Variavel.ListaDeConsultas[Variavel.ListaDeConsultas.Length + 2] = cmd2;
        }

        //FUNCAO diferencaDeValorEmKz(string moeda)
        public double diferencaDeValorEmUSD()
        {
            if (this.cambioActual > this.cambioDaEmissaoDaFactura)
            {
                double valorDaFacturaEmUSD = valorRecebidoEmKZ * this.cambioDaEmissaoDaFactura;
                double diferencaDeValorEmUSD = valorDaFacturaEmUSD - debitoEmUSD;

                return diferencaDeValorEmUSD;
            }
            else
            {
                return 0;
            }
        }



        //FUNCAO saldarTotalmenteAFatura()
        public void saldarTotalmenteAFatura()
        {
            //Observacao do recido
            this.observacaoDoRecibo = " Liquidação Parcial da Fact.nº " + numeroDaFactura;

            //inicializa o codico da conta caixa
            this.codigoDoCaixa = comboContaCaixa.SelectedItem.ToString();

            //numero do documento temporario
            string numeroDodocumentoTemporario = this.numeroDaFactura;

            //verifica se a area organica foi alterada
            if (FormPagamentoFactura.areaOrganicaFoiAlterada)
            {
                //sql3
                string cmd = "UPDATE AREGDOC SET STATUS = 'P', CODCAIXA ='" + Variavel.codUtilizador + "', CODAO ='" + Variavel.codArea + "', DATA_PAG = CONVERT(VARCHAR(20), GETDATE(), 120) where IDUNICO ='" + this.idUnicoDaFactura + "'";
                //atribui na lista de consultas
                Variavel.ListaDeConsultas[3] = cmd;
            }
            else
            {
                //sql3
                string cmd = "UPDATE AREGDOC SET STATUS = 'P', CODCAIXA ='" + Variavel.codUtilizador + "', DATA_PAG = CONVERT(VARCHAR(20), GETDATE(), 120) where IDUNICO ='" + this.idUnicoDaFactura + "'";
                //atribui na lista de consultas
                Variavel.ListaDeConsultas[3] = cmd;
            }

            //Verifica a moeda
            if (comboBoxMoeda.SelectedItem.ToString() == Gerais.PARAM_MOEDA_PDR)
            {
                //efectua pagamento
                efectuarPagamento();
                //paga imposto se selo - a verificacao e interna
                pagarImpostoDeSelo(comboBoxMoeda);
                //prepara a trancacao
                transacao(comboBoxMoeda);
                //executa a transacao
                executarATransacao();
            }
            else//se a moeda for kwanza
            {
                //retorna o CREDITO e DEBITO do cliente
                SqlCommand cmd = new SqlCommand("SELECT DEBITO + ISNULL(DEBITO_X,0) 'DEBITO_XPTO', CREDITO + ISNULL(CREDITO_X,0) 'CREDITO_XPTO'   FROM   (SELECT SUM(DEBITO) AS DEBITO, SUM(CREDITO) AS CREDITO "
                + "FROM AFCONCLI CC WITH (NOLOCK),AREGDOC R WITH (NOLOCK)"
                + "Where CC.CodEntidade='" + this.numeroDoCliente + "' And R.IDUNICO=CC.IDUNICO And R.NUMFACT='" + this.numeroDaFactura + "' And  (Debito Is Not Null Or (CC.CodDoc!='PA' Or CC.CodDoc!='PN' Or CC.CodDoc!='PP' Or CC.CodDoc!='NC'  Or CC.CodDoc='ND' )) And  (LEFT(CC.DATA_LANCAMENTO,10)>='2006-01-01' And LEFT(CC.DATA_LANCAMENTO,10)<=Convert(Varchar(10),GetDate(),120)) )A,"
                + "(SELECT SUM(DEBITO) AS DEBITO_X, SUM(CREDITO) AS CREDITO_X  FROM AFCONCLI CC, AREGDOC R Where CC.CodEntidade=R.CodEntidade And CC.CodDoc=R.CodDoc And CC.IDUnico=R.IDUnico And  CC.CodEntidade='" + this.numeroDoCliente + "' And (CC.CodDoc='ND' And Descricao Like 'Valor Adicional a Factura%')  And RIGHT(RTRIM(Descricao),LEN('" + this.numeroDaFactura + "'))='" + this.numeroDaFactura + "' ) B ", Conexao);

                Conexao.Open();
                SqlDataReader res = cmd.ExecuteReader();
                while(res.Read())
                {
                    this.debitoDoCliente    = (res["DEBITO_XPTO"].ToString() == "") ? 0 : Convert.ToDouble(res["DEBITO_XPTO"].ToString());
                    this.creditoDoCliente     = (res["CREDITO_XPTO"].ToString() == "") ? 0 : Convert.ToDouble(res["DEBITO_XPTO"].ToString()); 
                }
                Conexao.Close();


                //Verifica saldo do cliente e diferenca de valor por
                //causa de alteracao cambial
                this.saldoDoCliente = Math.Abs(this.debitoDoCliente - this.creditoDoCliente);

                valorEmKZParaNotaDeDebito = this.saldoDoCliente - (this.valorRestanteEmUSD * this.cambioActual);

                if (valorEmKZParaNotaDeDebito == 0)
                {
                    //MessageBox.Show("Valor incompleto para saldar totalemente a factura devido alteração cambial. Digite o valor que deseja pagar.", "Innovation - Informação.");
                    //txtValorReceBido.Enabled = true;
                    //cbSaldarCompletamenteAFactura.Enabled = false;
                    //return;
                }

                //efectua pagamento
                efectuarPagamento();
                //paga imposto se selo - a verificacao e interna
                pagarImpostoDeSelo(comboBoxMoeda);
                //prepara a trancacao
                transacao(comboBoxMoeda);
                //executa a transacao
                executarATransacao();
            }


        }
 


        //FUNCAO saldarParcialmenteAFatura()
        public void saldarParcialmenteAFatura()
        {
            //inicializa a moeda
            string moedaEscolhida = comboBoxMoeda.SelectedItem.ToString().ToUpper();

            //Observacao do recido
            this.observacaoDoRecibo = " Liquidação Parcial da Fact.nº " + this.numeroDaFactura;
           
            //inicializa o codico da conta caixa
            this.codigoDoCaixa = comboContaCaixa.SelectedItem.ToString();

            //numero do documento temporario
            string numeroDodocumentoTemporario = this.numeroDaFactura;


            //verifica se a area organica foi alterada
            if (FormPagamentoFactura.areaOrganicaFoiAlterada)
            {

                //sql3
                string cmd = "UPDATE AREGDOC SET STATUS = 'F', CODCAIXA = '"+ Variavel.codUtilizador +"' CODAO = '" + Variavel.codArea + "' DATA_PAG = CONVERT(VARCHAR(20), GETDATE(), 120) WHERE IDUNICO = '"+this.idUnicoDaFactura+"'";
                Variavel.ListaDeConsultas[3] = cmd;
            }
            else
            {

                //sql3
                string cmd = "UPDATE AREGDOC SET STATUS = 'F', CODCAIXA = '" + Variavel.codUtilizador + "', DATA_PAG = CONVERT(VARCHAR(20), GETDATE(), 120) WHERE IDUNICO = '" + this.idUnicoDaFactura + "'";
                
                //adiciona na lista de queries
                Variavel.ListaDeConsultas[3] = cmd;
            }


            if (moedaEscolhida == Gerais.PARAM_MOEDA_PDR)
            {
                //Verifica o status
                if (this.statusDocumento == "F")
                {
                    if (valorRecebidoEmUSD == valorRestanteEmUSD)
                    {
                        //sql3
                        string cmd = "UPDATE AREGDOC SET STATUS = 'P', CODCAIXA = '" + Variavel.codUtilizador + "', DATA_PAG = CONVERT(VARCHAR(20), GETDATE(), 120) WHERE IDUNICO = '" + this.idUnicoDaFactura + "'";

                        this.valorRecebidoEmKZ = this.valorRestanteEmKZ;

                        //adiciona na lista de queries
                        Variavel.ListaDeConsultas[3] = cmd;
                    }
                }

                //chanche status
                this.efectuarPagamento();

                //ins_selos
                this.pagarImpostoDeSelo(comboBoxMoeda);

                //exec_trans
                this.transacao(comboBoxMoeda);

                //executa a transacao
                this.executarATransacao();
            }
            else //Se a moeda for nacional
            {
                //Verifica o status
                if (this.statusDocumento == "F")
                {
                    if (valorRecebidoEmKZ == valorRestanteEmKZ)
                    {
                        //sql3
                        string cmd = "UPDATE AREGDOC SET STATUS = 'P', CODCAIXA = '" + Variavel.codUtilizador + "', DATA_PAG = CONVERT(VARCHAR(20), GETDATE(), 120) WHERE IDUNICO = '" + this.idUnicoDaFactura + "'";

                        valorRecebidoEmUSD = valorRestanteEmUSD;

                        //Adiciona na lista de consultas
                        Variavel.ListaDeConsultas[3] = cmd;
                    }
                }

                //Verifica a diferenca no cambio
                if (this.cambioActual > this.cambioDaEmissaoDaFactura)
                {
                    if (this.valorRestanteEmKZ == this.debitoEmKZ)
                    {
                        this.diferecaDeDebitoEmKZNaFactura = (this.valorRestanteEmUSD * this.cambioActual) - this.debitoEmKZ;
                    }
                    else
                    {
                        //Retorna Saldo 01
                        SqlCommand cmd1 = new SqlCommand("SELECT DISTINCT DEBITO  'DEBITO' FROM    (SELECT RIGHT(RTRIM(Descricao), LEN('" + this.numeroDaFactura + "')) AS FACTURA FROM AFCONCLI CC, AREGDOC R WHERE CC.CodEntidade=R.CodEntidade And CC.CodDoc=R.CodDoc And CC.IDUnico=R.IDUnico And CC.CodEntidade='" + this.labelNumeroDoCliente + "' And (CC.CodDoc='ND' And Descricao Like 'Valor Adicional a Factura%'))  LEN_FACTURA, (SELECT SUM(DEBITO) AS DEBITO FROM AFCONCLI CC, AREGDOC R WHERE CC.CodEntidade=R.CodEntidade And CC.CodDoc=R.CodDoc And CC.IDUnico=R.IDUnico And CC.CodEntidade='" + this.numeroDoCliente + "' And (CC.CodDoc='ND' And Descricao Like 'Valor Adicional a Factura%')) NUMFACT WHERE FACTURA='" + numeroDodocumentoTemporario + "'", Conexao);
                        SqlCommand cmd2 = new SqlCommand("Select IsNull(Sum(DEBITO),0) as Debito From AFCONCLI CC, AREGDOC R Where CC.CodEntidade=R.CodEntidade And CC.CodDoc=R.CodDoc And CC.IDUnico=R.IDUnico And CC.CodEntidade='" + this.numeroDoCliente + "' And CC.CodDoc='CC' AND R.NUMFACT='" + this.numeroDaFactura + "'", Conexao);


                        Conexao.Open();
                        string res1 = ((cmd1.ExecuteScalar().ToString() == null) || (cmd1.ExecuteScalar().ToString() == "")) ? "0" : cmd1.ExecuteScalar().ToString();
                        string res2 = ((cmd2.ExecuteScalar().ToString() == null) || (cmd1.ExecuteScalar().ToString() == "")) ? "0" : cmd1.ExecuteScalar().ToString();
                        Conexao.Close();

                        double saldo1 = Convert.ToDouble(res1);
                        double saldo2 = Convert.ToDouble(res2);

                        //Saldo do Cliente
                        double saldoDoClienteEmKZ = saldo1 + saldo2 + this.valorRestanteEmKZ;
                        this.valorEmKZParaNotaDeDebito = Math.Abs(saldoDoClienteEmKZ - (this.valorRestanteEmUSD * this.cambioActual));

                        //Verifica se o retorno da diferenca foi 0
                        if (this.valorEmKZParaNotaDeDebito == 0)
                        {
                            MessageBoxButtons botoes = MessageBoxButtons.OKCancel;
                            //Pagamento parcial a kwanza retorna false
                            DialogResult mensagem = MessageBox.Show("O valor que digitou é sufuciente para saldar a factura completamente. Saldar completamente?", "Innovation - Atenção!", botoes);
                            
                            if (mensagem == DialogResult.OK)
                            {
                                //salda completamente a factura
                                this.saldarTotalmenteAFatura();
                            }
                            else
                            {
                                txtValorReceBido.Focus();
                                return;
                            }
                        }
                    }
                }

                //chanche status
                this.efectuarPagamento();

                //ins_selos
                this.pagarImpostoDeSelo(comboBoxMoeda);

                //exec_trans
                this.transacao(comboBoxMoeda);

                //Executa a transacao
                this.executarATransacao();
            }
        }


        //FUNCAO efectuarPagamento() - Equivalente a funcao chanceStatus do cintura
        public void efectuarPagamento() //Ex chanceStatus
        {
            //moeda seleccionada na comboBox
            string moedaEscolhida = comboBoxMoeda.SelectedItem.ToString();

            //verifica se a factura tem diferenca de credito
            bool temdiferencaDeCredito = getDiferencaDeCreditoEmUSD() >= Convert.ToDouble(Gerais.PARAM_VALMIN_FC);

            this.numeroDoDocumento = Convert.ToInt32(this.numeroDaFactura);
            //descricao no afConcli
            string descricaoNoAfConcli;
            descricaoNoAfConcli = "Pagamento de factura Nº " + getNumeroDePagamentosEfectuados(this.numeroDoCliente, this.idUnicoDaFactura) + this.numeroDaFactura;

            if (moedaEscolhida == Gerais.PARAM_MOEDA_PDR)
            {
                //Retorna a descricao de documentos em pagamento padrao
                this.descricaoDoDocumento =  getDescricaoDoDocumento(Gerais.PARAM_CODDOC_PP);

                //sql2
                //isert no AregDoc
                string cmd = "INSERT INTO AREGDOC (IDUnico, Datacria, Data_Lancamento, Codutilizador, Coddoc, Descr, CodEntidade, Codloja, CodAO, Numdoc, Numfact, Preco, Precod, STATUS, OBS, Moeda, IDOrig, CODCAIXA, DATA_PAG, CODPAGAMENTO, USAR_MAXNUMDOC)  VALUES ('" + this.getProximoIdUnico() + "', CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_PP + "', '" + this.descricaoDoDocumento + "', '" + this.numeroDoCliente + "', '" + this.codigoDaLojaOrigem + "', '" + this.codigoDaAreaOrganicaOrigem + "', '" + this.numeroDoDocumento + "','" + this.numeroDaFactura + "', " + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + ", " + this.valorRecebidoEmUSD.ToString().Replace(',', '.') + ", 'P', '" + this.observacao + "', '" + moedaEscolhida + "', '" + this.idUnicoDaFactura + "', '" + Variavel.codUtilizador + "', CONVERT(VARCHAR(20), GETDATE(), 120), '" + this.codigoDePagamento + "', 'S')";

                Variavel.ListaDeConsultas[2] = cmd;

                //sql4
                //insert no AfConcli
                string cmd2 = "INSERT INTO AFCONCLI (IDUnico, Descricao, Valor, ValorD, Cambio, Doc, Credito, CreditoD, Datacria, Data_Lancamento, CodEntidade, Codutilizador, CodAO, Coddoc)  VALUES ('" + this.getProximoIdUnico() + "', '" + descricaoNoAfConcli + "', " + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + ", " + this.valorRecebidoEmUSD.ToString().Replace(',', '.') + " , " + this.cambioActual + ", " + this.numeroDoDocumento + ", " + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + ", " + this.valorRecebidoEmUSD.ToString().Replace(',', '.') + ", CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), '" + this.numeroDoCliente + "', '" + Variavel.codUtilizador + "', '" + Variavel.codArea + "', '" + Gerais.PARAM_CODDOC_PP + "')";
                Variavel.ListaDeConsultas[4] = cmd2;
            }
            else
            {
                //Retorna a descricao de documentos em pagamento padrao
                this.descricaoDoDocumento = getDescricaoDoDocumento(Gerais.PARAM_CODDOC_PN);

                //verifica se existe diferenca de cambio entre a emissao da factura a e o pagamento actual
                //Se a diferenca de cambio for menos de 50usd para com o cambio da emissao da factura
                if ((diferencaDeValorEmUSD() < Convert.ToDouble(Gerais.PARAM_VALMIN_FC)))
                {
                    this.valorRecebidoEmKZ = this.valorRecebidoEmUSD * this.cambioDaEmissaoDaFactura;
                }

                //sql2
                //insert no AregDoc
                string cmd = "INSERT INTO AREGDOC (IDUnico, Datacria, Data_Lancamento, Codutilizador, Coddoc, Descr, CodEntidade, Codloja, CodAO, Numdoc, Numfact, Preco, Precod, STATUS, OBS, Moeda, IDOrig, CODCAIXA, DATA_PAG, CODPAGAMENTO, USAR_MAXNUMDOC)  VALUES ('" + this.getProximoIdUnico() + "', CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_PN + "', '" + this.descricaoDoDocumento + "', '" + this.numeroDoCliente + "', '" + this.codigoDaLojaOrigem + "', '" + this.codigoDaAreaOrganicaOrigem + "', '" + this.numeroDoDocumento + "', '" + this.numeroDaFactura + "'," + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + "," + this.valorRecebidoEmUSD.ToString().Replace(',', '.') + ", 'P', '" + this.observacao + "',  '" + moedaEscolhida + "',  '" + this.idUnicoDaFactura + "', '" + Variavel.codUtilizador + "', CONVERT(VARCHAR(20), GETDATE(), 120), '" + this.codigoDePagamento + "', 'S')";
                Variavel.ListaDeConsultas[2] = cmd;

                if (temdiferencaDeCredito)
                {
                    //sql4
                    //insert no AfConcli com diferenca de credito
                    string cmd2 = "INSERT INTO AFCONCLI (IDUnico, Descricao, Valor, ValorD, Cambio, Doc, Credito, CreditoD, Datacria, Data_Lancamento, CodEntidade, Codutilizador, CodAO, Coddoc,  CAMBIO_A_DATA, DIF_CREDITOD)  VALUES ('" + this.getProximoIdUnico() + "', '" + descricaoNoAfConcli + "', " + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + ", " + this.valorRecebidoEmUSD.ToString().Replace(',', '.') + " , " + this.cambioActual + ", " + this.numeroDoDocumento + ", " + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + ", " + this.valorRecebidoEmUSD.ToString().Replace(',', '.') + ", CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), '" + this.numeroDoCliente + "', '" + Variavel.codUtilizador + "', '" + Variavel.codArea + "', '" + Gerais.PARAM_CODDOC_PN + "', " + this.cambioDaEmissaoDaFactura + ", " + getDiferencaDeCreditoEmUSD().ToString().Replace(',','.') + ")";
                    Variavel.ListaDeConsultas[4] = cmd2;
                }
                else
                {
                    //sql4
                    //insert no AfConcli
                    string cmd3 = "INSERT INTO AFCONCLI (IDUnico, Descricao, Valor, ValorD, Cambio, Doc, Credito, CreditoD, Datacria, Data_Lancamento, CodEntidade, Codutilizador, CodAO, Coddoc)  VALUES ('" + this.getProximoIdUnico() + "', '" + descricaoNoAfConcli + "', " + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + ", " + this.valorRecebidoEmUSD.ToString().Replace(',', '.') + " , " + this.cambioActual + ", " + this.numeroDoDocumento + ", " + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + ", " + this.valorRecebidoEmUSD.ToString().Replace(',', '.') + ", CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), '" + this.numeroDoCliente + "', '" + Variavel.codUtilizador + "', '" + Variavel.codArea + "', '" + Gerais.PARAM_CODDOC_PN + "')";
                    Variavel.ListaDeConsultas[4] = cmd3;
                }
            }
        }

        //FUNCAO - getDiferenca de credito em dolar
        public double getDiferencaDeCreditoEmUSD()
        {
            debitoActualEmUSD = this.valorRestanteEmKZ / this.cambioActual;
            return Math.Abs(this.debitoEmUSD - debitoActualEmUSD);
        }

        //FUNCAO - getDigerencaDeCreditoEmKZ()
        public double getDiferencaDeCreditoEmKZ()
        {
            return getDiferencaDeCreditoEmUSD() * this.cambioActual;
        }

        //FUNCAO deveEmitirFacturaDeCorreccaoCambial()
        public bool deveEmitirFacturaDeCorreccaoCambial()
        {
            if (this.cambioActual > this.cambioDaEmissaoDaFactura)
            {
                
                /***
                 * 
                 * 
                 * 
                 * *
                 
                 debitoEmDolarActual = debitoEmKZ / cambioActual
                 * diferencaDeCreditoEmUSD = debitoEmUSD - valorQueFalta
                 * diferecaDeCreditoEmKZ = diferencaDeCreditoEmUSD * cambioActual
                 * 
                 * debitoActual = valorFalta
                 * 
                 */
                int numeroDeDias;

                this.debitoActualEmUSD = this.debitoEmKZ / this.cambioActual; //valorFalta
                this.debitoActualEmKZ = this.debitoActualEmUSD * this.cambioActual; //valorFalta

                //Atribui o numero de dias de acordo com o numero da factura
                if (this.tipoDocumento == Gerais.PARAM_CODDOC_FA)
                {
                    numeroDeDias = int.Parse(Gerais.PARAM_NRDIAS_FA);
                }
                else if (this.tipoDocumento == Gerais.PARAM_CODDOC_FC)
                {
                    numeroDeDias = int.Parse(Gerais.PARAM_NRDIAS_FC);
                }
                else if (this.tipoDocumento == Gerais.PARAM_CODDOC_FD)
                {
                    numeroDeDias = int.Parse(Gerais.PARAM_NRDIAS_FD);
                }
                else if (this.tipoDocumento == Gerais.PARAM_CODDOC_FM)
                {
                    numeroDeDias = int.Parse(Gerais.PARAM_NRDIAS_FM);
                }

                //verifica a data de pagamento limite
                if ((getDataActual() > Convert.ToDateTime(this.dataLimite).Date) && (diferencaDeValorEmUSD() >= Convert.ToDouble(Gerais.PARAM_VALMIN_FC)) && (comboBoFormaPagamento.SelectedItem.ToString() != "TRANSF USD"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //gerarFactura de correccao cambial
        public void emitirFacturaDeCorrecaoCambial()
        {
            if (this.deveEmitirFacturaDeCorreccaoCambial())
            {
                DateTime prazo = proximoDiaUtil(this.dataLimite, int.Parse(Gerais.PARAM_NRDIAS_FC));
                this.descricaoDoDocumento = this.getDescricaoDoDocumento(Gerais.PARAM_CODDOC_FC);
                this.numeroDoDocumento = int.Parse(this.numeroDaFactura);
                this.observacao = "Correcção Cambial Fact. nº " + this.numeroDoDocumento;

                //sql1
                string cmd = "UPDATE AFCONCLI SET DOC = '" + this.numeroDaFactura + "', ValorD =" + this.debitoActualEmUSD.ToString().Replace(',', '.') + ", DebitoD=" + this.debitoActualEmUSD.ToString().Replace(',', '.') + ", CODCAIXA = '" + Variavel.codUtilizador + "', DATA_PAG = CONVERT(VARCHAR(20), GETDATE(), 120) WHERE IDUnico = '" + this.idUnicoDaFactura + "'";
                //adiciona a lista de consultas
                Variavel.ListaDeConsultas[1] = cmd;

                //sql2
                string cmd2 = "INSERT INTO AREGDOC (IDUnico, Datacria, Data_Lancamento, Codutilizador, Coddoc, Descr, CodEntidade, Codloja, CodAO, Numdoc, Numfact, Preco, Precod, STATUS, Moeda, IDOrig, CODCAIXA, DATA_PAG, CODPAGAMENTO, USAR_MAXNUMDOC)  VALUES ('" + this.getProximoIdUnico() + "', CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_PN + "', '" + this.descricaoDoDocumento + "', '" + this.numeroDoCliente + "', '" + this.codigoDaLojaOrigem + "', '" + Variavel.codArea + "', '" + this.numeroDoDocumento + "', '" + this.numeroDaFactura + "', " + this.debitoActualEmKZ.ToString().Replace(',', '.') + ", " + this.debitoActualEmUSD.ToString().Replace(',', '.') + ", 'P', '" + comboBoxMoeda.SelectedItem.ToString() + "', '" + this.idUnicoDaFactura + "', '" + Variavel.codUtilizador + "', CONVERT(VARCHAR(20), GETDATE(), 120), '" + this.codigoDePagamento + "', 'S')";
                //adiciona a lista de consultas
                Variavel.ListaDeConsultas[2] = cmd2;

                //sql4
                string cmd3 = "'INSERT INTO AFCONCLI (IDUnico, Descricao, Valor, ValorD, Cambio, Doc, Credito, CreditoD, Datacria, Data_Lancamento, CodEntidade, Codutilizador, CodAO, Coddoc)  VALUES ('" + this.getProximoIdUnico() + "', '" + this.descricaoDoDocumento + "', " + this.debitoActualEmKZ.ToString().Replace(',','.') + ", " + this.debitoActualEmUSD.ToString().Replace(',','.') + ", " + Variavel.cambio.Replace(',','.') + ", " + this.numeroDoDocumento + ", " + debitoActualEmKZ.ToString().Replace(',','.') + ", " + this.debitoActualEmUSD.ToString().Replace(',','.') + ", CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), '" + this.numeroDoCliente + "', '" + Variavel.codUtilizador + "', '" + Variavel.codArea + "', '" + Gerais.PARAM_CODDOC_PN + "')";
                //adiciona a lista de consultas
                Variavel.ListaDeConsultas[4] = cmd3;

                //sql11 - criacao da factura de correcao
                string cmd4 = "INSERT INTO AREGDOC (IDUnico, Datacria, Data_Lancamento, Data_Limite, Codutilizador, Coddoc, Descr, CodEntidade, Codloja, CodAO, Numdoc, Numfact, Preco, Precod, STATUS, Moeda, OBS, IDOrig)  VALUES ('" + this.getProximoIdUnico() + "', CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), '" + dataLimite + "', '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_FC + "', '" + this.descricaoDoDocumento + "', '" + this.numeroDoCliente + "', '" + this.codigoDaLojaOrigem + "', '" + Variavel.codArea + "', " + getPrimoNumeroDeDocumentoParaFacturaDeCorrecaoCambial() + ", '" + getPrimoNumeroDeDocumentoParaFacturaDeCorrecaoCambial() + "', "+this.getDiferencaDeCreditoEmKZ().ToString().Replace(',', '.')+", "+this.getDiferencaDeCreditoEmUSD().ToString().Replace(',','.')+", 'E', '" + comboBoxMoeda.SelectedItem.ToString() + "', '" + this.observacao + "', '" + this.getProximoIdUnico() + "')";
                //adiciona a lista de consultas
                Variavel.ListaDeConsultas[11] = cmd4;

                //sql2
                string cmd5 = "'INSERT INTO AFCONCLI (IDUnico, Descricao, Valor, ValorD, Cambio, Datacria, Data_Lancamento, Debito, DebitoD, CodEntidade, Codutilizador, CodAO, Coddoc) VALUES ('" + this.getProximoIdUnico() + "', '" + this.descricaoDoDocumento + "', "+this.getDiferencaDeCreditoEmKZ().ToString().Replace(',','.')+", "+this.getDiferencaDeCreditoEmUSD().ToString().Replace(',', '.')+", " + Variavel.cambio + ", CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), "+this.getDiferencaDeCreditoEmKZ().ToString().Replace(',','.')+", "+getDiferencaDeCreditoEmUSD().ToString().Replace(',','.')+"'" + this.numeroDoCliente + "', '" + Variavel.codUtilizador + "', '" + Variavel.codArea + "', '" + Gerais.PARAM_CODDOC_FC + "')";
                //adiciona a lista de consultas
                Variavel.ListaDeConsultas[12] = cmd5;
                //Sql8
                this.pagarImpostoDeSelo(comboBoxMoeda);
            }
            else
            {
                this.descricaoDoDocumento = getDescricaoDoDocumento(Gerais.PARAM_CODDOC_PA);
                //sql2
                string cmd2 = "INSERT INTO AREGDOC (IDUnico, Datacria, Data_Lancamento, Codutilizador, Coddoc, Descr, CodEntidade, Codloja, CodAO, Numdoc, Numfact, Preco, Precod, STATUS, Moeda, IDOrig, CODCAIXA, DATA_PAG, CODPAGAMENTO, USAR_MAXNUMDOC)  VALUES ('"+getProximoIdUnico()+"', CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), '"+Variavel.codUtilizador+"', '"+Gerais.PARAM_CODDOC_PN+"', '"+this.descricaoDoDocumento+"', '"+this.numeroDoCliente+"', '"+this.codigoDaLojaOrigem+"', '"+Variavel.codArea+"', "+this.numeroDoDocumento+", '"+this.numeroDaFactura+"', "+this.valorRecebidoEmKZ.ToString().Replace(',', '.')+", "+this.valorRecebidoEmUSD.ToString().Replace(',','.')+", 'P', '"+comboBoxMoeda.SelectedItem.ToString()+"', '"+this.idUnicoDaFactura+"', '"+Variavel.codUtilizador+"', CONVERT(VARCHAR(20), GETDATE(), 120), '"+this.codigoDePagamento+"', 'S')";
                //Adiciona a lista de consultas
                Variavel.ListaDeConsultas[2] = cmd2;
                //sql4
                string cmd4 = "INSERT INTO AFCONCLI (IDUnico, Descricao, Valor, ValorD, Cambio, Doc, Credito, CreditoD, Datacria, Data_Lancamento, CAMBIO_A_DATA, DIF_CREDITOD, CodEntidade,  Codutilizador, CodAO, Coddoc) VALUES ('" + this.getProximoIdUnico() + "', '" + this.descricaoDoDocumento + "', " + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + ", " + this.valorRecebidoEmUSD.ToString().Replace(',', '.') + ", " + this.cambioDaEmissaoDaFactura.ToString().Replace(',', '.') + ", " + this.numeroDoDocumento + ", " + this.valorRecebidoEmKZ.ToString().Replace(',', '.') + ", " + this.valorRecebidoEmUSD.ToString().Replace(',', '.') + ", CONVERT(VARCHAR(20), GETDATE(), 120), CONVERT(VARCHAR(10), GETDATE(), 120), " + this.cambioActual.ToString().Replace(',', '.') + ", " + this.getDiferencaDeCreditoEmUSD().ToString().Replace(',', '.') + ", '" + this.numeroDoCliente + "', '" + Variavel.codUtilizador + "', '" + Variavel.codArea + "', '" + Gerais.PARAM_CODDOC_PA + "')";
                //Adiciona a lista de consultas
                Variavel.ListaDeConsultas[4] = cmd4;

                //pagar imposto de selo
                this.pagarImpostoDeSelo(comboBoxMoeda);
            }
        }

        //getPrimoIdParaFacturaDeCorrecaoCambial
        public string getPrimoNumeroDeDocumentoParaFacturaDeCorrecaoCambial()
        {
            SqlCommand cmd = new SqlCommand("SELECT ISNULL(MAX(NUMDOC), 0) + 1 FROM AREGDOC  WITH (NOLOCK) WHERE CODDOC = '" + Gerais.PARAM_CODDOC_FC + "'  AND CodLoja = '" + this.codigoDaLojaOrigem + "'  GROUP BY CODLOJA, CODDOC  ORDER BY MAX(NUMDOC) DESC", Conexao);

            Conexao.Open();
            string res = cmd.ExecuteScalar() as string;
            Conexao.Close();

            return res;
        }



        //FUNCAO retornarTiposDePagamento
        public List<Object> retornarTiposDePagamento()
        {
            List<Object> tiposDePagamento = new List<Object>();
            var cmd = new SqlCommand("SELECT ABREVIATURA  FROM ATTIPOPAGAMENTO WITH (NOLOCK)", Conexao);

                Conexao.Open();
                var res = cmd.ExecuteReader();
                
                while (res.Read())
                {
                    tiposDePagamento.Add(res["ABREVIATURA"].ToString());
                }

                Conexao.Close();
                return tiposDePagamento;

        }

        //Funcao permitirObservacao
        public bool permitirObs(string codDoc)
        {
            var cmd = new SqlCommand("SELECT Permitir_Obs FROM ASDOCS  WITH (NOLOCK) WHERE Coddoc in (@codDoc) ", Conexao);
            cmd.Parameters.Add(new SqlParameter("@codDoc", codDoc));

            Conexao.Open();
            var res = cmd.ExecuteScalar().ToString();
            Conexao.Close();

            if (res == "S")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //FUNCAO para mostrar/ocultar textBox e Label no form
        public void mostrarControlesObservacao(bool mostrar)
        {
            if (mostrar)
            {
                labelObservacao.Show();
                txtObservacao.Show();
            }
            else
            {
                labelObservacao.Hide();
                txtObservacao.Hide();
            }
        }

        //FUNCAO para carregar contas contabilisticas cliente
        public List<Object> getCodigosDasContasCliente()
        {

            var cmd = new SqlCommand("SELECT  CodConta from ACPLANO_" + getAnoCorrente() + " WITH (NOLOCK) Where LEFT(CodConta, 2)=@PARAM_CONTA_CLI and Controlo='S'", Conexao);

            //cmd.Parameters.Add(new SqlParameter("@tabelaAC_PLANO_ANO", tabelaAC_PLANO_ANO));
            cmd.Parameters.Add(new SqlParameter("@PARAM_CONTA_CLI", Gerais.PARAM_CONTA_CLI));

            Conexao.Open();
            var res = cmd.ExecuteReader();
            List<Object> lista = new List<object>();

            while (res.Read())
            {
                lista.Add(res["CodConta"].ToString());
            }
           
            Conexao.Close();
        
            return lista;
        }

        //retorna o numero da conta caixa com o codigo passado no parametro
        public string getNomeDaContaCaixa(string codConta)
        {
            var cmd = new SqlCommand("Select NomeConta From ACPLANO_"+getAnoCorrente()+"   WITH (NOLOCK) Where codConta =@codContaCaixa and  Controlo='S'", Conexao);
            cmd.Parameters.Add(new SqlParameter("@codContaCaixa", codConta));

            Conexao.Open();
            var res = cmd.ExecuteScalar().ToString();
            Conexao.Close();

            return res;
        }


        //retorna o numero da conta Cliente com o codigo passado no parametro
        public string getNomeDaConta(string codConta)
        {
            var cmd = new SqlCommand("Select NomeConta From ACPLANO_"+getAnoCorrente()+" WITH (NOLOCK) Where codConta =@codContaCaixa", Conexao);
            cmd.Parameters.Add(new SqlParameter("@codContaCaixa", codConta));

            Conexao.Open();
            var res = cmd.ExecuteScalar().ToString();

            Conexao.Close();

            return res;
        }

        //fUNCAO que lista os codigos das contas caixa
        public List<Object> getCodigosDasContasCaixa()
        {
            var cmd = new SqlCommand("Select CodConta From ACPLANO_"+getAnoCorrente() +"   WITH (NOLOCK) Where  Controlo='S'", Conexao);

            Conexao.Open();
            var res = cmd.ExecuteReader();
            List<Object> lista = new List<object>();

            while (res.Read())
            {
                lista.Add(res["CodConta"].ToString());
            }

            Conexao.Close();

            return lista;
        }

        /*
         * 
         * 
         *   FUNCAO verifica se a factura ja foi paga
         * 
         * 
         */
        public bool faturaEstaPaga()
        {
                SqlCommand cmd = new SqlCommand("SELECT TOP 1  IDUNICO From AREGDOC D WITH (NOLOCK)  WHERE D.IDUnico = '" + this.idUnicoDaFactura + "'  AND D.STATUS = 'P'  ", Conexao);
                Conexao.Open();

                int res = (int)cmd.ExecuteNonQuery();
                Conexao.Close();

                if (res >= 1)
                {
                    MessageBox.Show("Esta Factura já foi paga.", "Innovation - Atenção");
                    return true;
                }
                else
                {
                    return false;
                } 
        }

        /*
        * 
        * 
        *   FUNCAO verifica se a areaOrganica da factura ainda existe
        * 
        * 
        */
        public bool areaOrganicaAindaExiste()
        {
            SqlCommand cmd = new SqlCommand("SELECT  TOP 1 CODAO FROM AIAREAORGANICA  WITH (NOLOCK) WHERE CODAO='" + Variavel.codArea + "' and TO_DISPLAY='N'",Conexao);
            Conexao.Open();
            int res = (int) cmd.ExecuteNonQuery();
            Conexao.Close();

            if (res > 0)
            {
                return false;
            }
            else
            {
                return true;
            } 
        }


       /*
       * 
       * 
       *   FUNCAO validarPagamento
       * 
       * 
       */
        public void validarPagamento()
        {
            if ((int.Parse(Permissoes.nPrgPos) >= 4) && (int.Parse(Permissoes.nPrgCx) >=4))
            {
                //verifica se foi passado um idUnicoDaFactura
                if (string.IsNullOrEmpty(this.idUnicoDaFactura))
                {
                    MessageBox.Show("Digite um numero de factura para fazer pagamento. ", "Innovation - Atenção");
                    txtIdFactura.Focus();
                    return;
                }

                //verifica se a factura ja foi paga
                if (this.statusDocumento == "P")
                {
                    MessageBox.Show("A factura com o identificador "+ this.idUnicoDaFactura+"já foi paga.", "Innovation - Informação.");
                    txtIdFactura.Focus();
                    return;
                }
                
                //Verifica se a forma de pagamento esta seleccionada
                if (comboBoFormaPagamento.SelectedIndex == -1)
                {
                    MessageBox.Show("Escolha a forma de pagamento. ","Innovation - Atenção");
                    comboBoFormaPagamento.Focus();
                    return;
                    
                }

                //Verifica se existe conta de cliente seleccionada
                if (comboContaCliente.SelectedIndex == -1)
                {
                    MessageBox.Show("Escolha a Conta do Cliente. ", "Innovation - Atenção");
                    comboContaCliente.Focus();
                    return;
                }

                //Verifica se existe conta do caixa seleccionada
                if (comboContaCaixa.SelectedIndex == -1)
                {
                    MessageBox.Show("Escolha a Conta do caixa. ", "Innovation - Atenção");
                    comboContaCaixa.Focus();
                    return;
                }

                //Verifica se a AreaOrganica foi alterada
                if (!areaOrganicaAindaExiste())
                {
                    //Exibe lista das areas organicas
                    FormSelectAreaOrganica ListaDeAreasOrganicas = new FormSelectAreaOrganica();
                    ListaDeAreasOrganicas.ShowDialog();

                }
                
                //Verifica se a txtField esta a receber um valor para saldar a factura
                if (string.IsNullOrEmpty(txtValorReceBido.Text) && (txtValorReceBido.Enabled == true))
                {
                    MessageBox.Show("Digite uma parcela do valor remanescente da factura.", "Innovation - Atenção");
                    txtValorReceBido.Focus();
                    return;
                }
            }
        }


         


        
        /*
         *
         * EVENTOS DOS FORMS E CONTROLES
         * 
         */
        private void FormPagamentoFactura_Load(object sender, EventArgs e)
        {
            //esconde a checkbox
            cbImpostoSelo.Hide();

            //inicializa a combobox Moeda
            comboBoxMoeda.Items.Add(Gerais.PARAM_MOEDA_NAC);
            comboBoxMoeda.Items.Add(Gerais.PARAM_MOEDA_PDR);

            //inicializa o valor default da comboBoxMoeda
            carregarItemDefaultDaComboBox(comboBoxMoeda);

            //incializa textBoxCambio com o cambio
            txtCambio.Text = retornarCambioActual().ToString()+".00";

            //actualiza a comboTipo de acodo a moeda seleccionada
            if (comboBoxMoeda.SelectedIndex == 0)
            {
                comboTipoPagamento.Items.Add("Cheque");
                comboTipoPagamento.Items.Add("Numerario");
            }

            //inicializa o valor default da comboBoxTipoPagamento
            carregarItemDefaultDaComboBox(comboTipoPagamento);

            //oculta controles para observacao
            mostrarControlesObservacao(false);

            //Retorna os tipos de pagamento
            try 
            {
                var tiposDePagamento = retornarTiposDePagamento();
                foreach (var tipos in tiposDePagamento)
                {
                    comboBoFormaPagamento.Items.Add(tipos);
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Ups! Erro no banco de dados.");
            }


            //carrega as contas contabilisticas do cliente
            try
            {
                foreach (var conta in getCodigosDasContasCliente())
                {
                    comboContaCliente.Items.Add(conta);
                }
                //inicializa o valor default da comboContaCliente
                carregarItemDefaultDaComboBox(comboContaCliente);
            }
            catch(Exception erro)
            {
                MessageBox.Show("Erro ao carregar as contas Cliente "+erro.Message);
            }

            //carrega as contas contabilisticas do cliente
            try
            {
                foreach (var conta in getCodigosDasContasCaixa())
                {
                    comboContaCaixa.Items.Add(conta);
                }
                //inicializa o valor default da comboContaCliente
                carregarItemDefaultDaComboBox(comboContaCliente);
            }
            catch (Exception erro)
            {
                MessageBox.Show("Erro ao carregar as contas Caixa " + erro.Message);
            }
        }



        private void idFactura_leaveFocus(object sender, EventArgs e)
        {
            string idUnico = txtIdFactura.Text;
            if (!string.IsNullOrEmpty(idUnico))
            {
                try
                {
                    //Retorna os dados da factura
                    if (getDadosDaFactura(idUnico) == true)
                    {
                        //retorna os valores pago pelo cliente
                        var valores = getValorPagoPeloCliente(idUnicoDaFactura, numeroDaFactura);
                    
                        if ((valores != null) || (valores.Count > 0))
                        {
                            valorPagoPeloClienteEmKZ = Math.Abs(Convert.ToDouble(valores[0]));
                            valorPagoPeloClienteEmUSD = Math.Abs(Convert.ToDouble(valores[1]));

                            valorRestanteEmKZ = debitoEmKZ - valorPagoPeloClienteEmKZ;
                            valorRestanteEmUSD = debitoEmUSD - valorPagoPeloClienteEmUSD;

                            if (valorPagoPeloClienteEmKZ == 0)
                            {
                                valorRestanteEmKZ = debitoEmKZ;
                                valorRestanteEmUSD = debitoEmUSD;

                                labelValorRestanteEmKZ.Text = "AKZ " + valorRestanteEmKZ.ToString();
                                labelValorRestanteEmUSD.Text = "$ " + valorRestanteEmUSD.ToString();
                            }
                            else
                            {
                                valorRestanteEmKZ = debitoEmKZ - valorPagoPeloClienteEmKZ;
                                valorRestanteEmUSD = debitoEmUSD - valorPagoPeloClienteEmUSD;

                                //Recebe tambem nas labels
                                labelValorRestanteEmKZ.Text = "AKZ " + valorRestanteEmKZ.ToString();
                                labelValorRestanteEmUSD.Text = "$ " + valorRestanteEmUSD.ToString();
                            }
                        }
                        else
                        {
                            valorRestanteEmKZ = debitoEmKZ;
                            valorRestanteEmUSD = debitoEmUSD;

                            //Recebe tambem nas labels
                            labelValorRestanteEmKZ.Text = "AKZ " + valorRestanteEmKZ.ToString();
                            labelValorRestanteEmUSD.Text = "$ " + valorRestanteEmUSD.ToString();
                        }

                        //limpa os campos do datagridView se existem
                        limparLinhasDagrid(metroGrid1);

                        //Retorna o historico da factura no dataGridView
                        getHistoriocDaFactura(idUnicoDaFactura, numeroDoCliente, metroGrid1);
                    
                    }//getDadosDaFactura()
                    else
                    {
                        MessageBox.Show("Não existe factura por pagar com o numero " + idUnico);
                        limparCampos();
                    }
                }
                catch(TimeoutException)
                {
                    MessageBox.Show("Ups! parece que a rede está lenta. Repita novamente a operação.","Innovatio - Informação");
                }
                catch(SqlException)
                {
                    MessageBox.Show("Alguma coisa falhou! insira correctamente os dados.", "Innovation - Erro");
                }
                catch (Exception)
                {
                    MessageBox.Show("Ooops! parece que alguma coisa está mal. Contante um helpDesk.", "Innovation - Erro");
                }
                finally
                {
                    Conexao.Close();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(labelNumeroDaFactura.Text))
                {
                    limparCampos();
                }
            }
        }

        private void comboBoFormaPagamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoFormaPagamento.SelectedItem.ToString())
            {
                case "TRANSF USD": cbImpostoSelo.Show();
                    break;
                case "SAQUE": cbImpostoSelo.Show();
                    break;

                default:    cbImpostoSelo.Checked = false;
                    //desabilita o checkbox e esconde-a
                    cbImpostoSelo.Hide();
                    break;
            }
        }

        private void comboBoxMoeda_SelectedIndexChanged(object sender, EventArgs e)
        {
            retornarTipoDePagamentoDeAcordoAMoeda(comboBoxMoeda.SelectedIndex, comboTipoPagamento);

            //limpar combo e label conta caixa
            comboContaCaixa.SelectedIndex = -1;
            //limpar txtValorRecebido se tivel valor
            if(!(string.IsNullOrEmpty(txtValorReceBido.Text)))
            {
                txtValorReceBido.Clear();
            }

            labelNomeContaCaixa.Text = null;

            if (comboBoxMoeda.SelectedIndex == 0)
            {
                try
                {
                    //limpa os componentes da observacao se estiver visivel
                    mostrarControlesObservacao(false);

                    if (permitirObs(Gerais.PARAM_CODDOC_PN))
                    {
                        mostrarControlesObservacao(true);
                    }

                }
                catch (Exception erro)
                {
                    MessageBox.Show("Houve algum erro ao carregar a caixa de Observação."+erro.Message);
                }
             }
            else if (comboBoxMoeda.SelectedIndex == 1)
            {
                try
                {
                    //limpa os componentes da observacao se estiver visivel
                    mostrarControlesObservacao(false);

                    if (permitirObs(Gerais.PARAM_CODDOC_PP))
                    {
                        mostrarControlesObservacao(true);
                    }
                }
                catch (Exception erro)
                {
                    MessageBox.Show("Houve algum erro ao carregar a caixa de Observação." + erro.Message);
                }
            }

        }

        private void comboContaCaixa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboContaCaixa.SelectedItem != null)
            {
                string codigoDaConta = comboContaCaixa.SelectedItem.ToString();

                try
                {
                    labelNomeContaCaixa.Text = getNomeDaConta(codigoDaConta);
                }
                catch(Exception erro)
                {
                    MessageBox.Show("Erro ao retornar o nome da conta caixa "+erro.Message);
                }
                

                if (comboContaCaixa.SelectedIndex == -1)
                {
                    txtCambio.Text = null;
                }
            }
        }

        private void comboContaCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboContaCliente.SelectedItem != null)
            {
                string codigoDaConta = comboContaCliente.SelectedItem.ToString();

                try
                {
                    labelNomeContaCliente.Text = getNomeDaConta(codigoDaConta);
                }
                catch (Exception erro)
                {
                    MessageBox.Show("Erro ao retornar o nome da conta caixa " + erro.Message);
                    
                }
            }
        }

        private void cbCambioManual_CheckedChanged(object sender, EventArgs e)
        {
            if (cbCambioManual.Checked == true)
            {
                txtCambio.Enabled = true;
                txtCambio.Text = this.cambioActual.ToString();
            }
            else
            {
                //incializa textBoxCambio com o cambio
                txtCambio.Text = retornarCambioActual().ToString();
            }
        }

        private void cbSaldarCompletamenteAFactura_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSaldarCompletamenteAFactura.Checked)
            {
                txtValorReceBido.Enabled = false;
                txtValorReceBido.Clear();
                labelValor.Enabled = false;
                labelValor.ForeColor = Color.LightSalmon;
                
                //Valor restante 
                valorRecebidoEmKZ = valorRestanteEmKZ;
                valorRecebidoEmUSD = valorRestanteEmUSD;

            }
            else
            {
                txtValorReceBido.Enabled = true;
                labelValor.Enabled = true;
            } 
        }

        private void txtValorReceBido_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtValorReceBido.Text))
            {
                //Verifica se foi pasado um numero de factura
                if (string.IsNullOrEmpty(txtIdFactura.Text))
                {
                    MessageBox.Show("Digite o ID da factura por pagar", "Innovation - Alerta");
                    txtValorReceBido.Clear();
                    txtIdFactura.Focus();
                    return;
                }

                //verifica o tipo de moeda escolhida
                if (comboBoxMoeda.SelectedItem.ToString() == Gerais.PARAM_MOEDA_NAC)
                {
                 
                    this.valorRecebidoEmKZ = Convert.ToDouble(txtValorReceBido.Text);
                    this.valorRecebidoEmUSD = this.valorRecebidoEmKZ / this.cambioActual;

                    //se o cambio for manual
                    if (cbCambioManual.Checked == true)
                    {
                        this.valorRecebidoEmUSD = this.valorRecebidoEmKZ * this.cambioManual;
                    }

                    if (valorRecebidoEmKZ > valorRestanteEmKZ)
                    {
                        MessageBox.Show("O valor " + valorRecebidoEmKZ + comboBoxMoeda.SelectedItem.ToString() + " é superior ao valor da factura. So faltam " + valorRestanteEmKZ + " " + comboBoxMoeda.SelectedItem.ToString(), "Innovation - Atenção!");
                        txtValorReceBido.Clear();
                        txtValorReceBido.Focus();
                        return;
                    }
                }
                else if(comboBoxMoeda.SelectedItem.ToString() == Gerais.PARAM_MOEDA_PDR)
                {
                    this.valorRecebidoEmUSD = Convert.ToDouble(txtValorReceBido.Text);
                    this.valorRecebidoEmKZ = this.valorRecebidoEmUSD * this.cambioActual;

                    if (valorRecebidoEmUSD > valorRestanteEmUSD)
                    {
                        MessageBox.Show("O valor " + valorRecebidoEmUSD + comboBoxMoeda.SelectedItem.ToString() + " é superior ao valor da factura. So faltam " + valorRestanteEmKZ + " " + comboBoxMoeda.SelectedItem.ToString(), "Innovation - Atenção!");
                        txtValorReceBido.Clear();
                        txtValorReceBido.Focus();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Escolha o tipo de moeda para efectuar o pagamento.","Innovation - Atenção!");
                }

                //DEBUG
                MessageBox.Show(this.valorRecebidoEmKZ + " " + this.valorRecebidoEmUSD);
            }           
        }

        private void txtCambio_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCambio.Text))
            {
                try
                {
                    if (this.cambioActual.ToString() == txtCambio.Text)
                    {
                        MessageBox.Show("Você digitou o cambio actual.");
                        cbCambioManual.Checked = false;
                        txtCambio.Enabled = false;
                    }

                    this.cambioManual = double.Parse(txtCambio.Text);
                }
                catch (FormatException)
                {
                    MessageBox.Show("Cambio no formato incorrecto.");
                }
            }
            
        }

        private void btGravar_Click(object sender, EventArgs e)
        {
            //Realiza as validacoes
            this.validarPagamento();

            //verifica se o usuario está a saldar a factura por completo
            if (cbSaldarCompletamenteAFactura.Checked)
            {
                MessageBoxButtons botoes = MessageBoxButtons.YesNoCancel;
                DialogResult res;

                res = MessageBox.Show("Tem certeza que pretende saldar totalmente a factura?", "Innovation - Atenção", botoes);

                if (res == DialogResult.Yes)
                {
                    try
                    {
                        this.saldarTotalmenteAFatura();
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("Verifique se inseriu os dados correctamente, a operação falhou", "Innovation - Erro");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Não foi possivel saldar a factura na totalidade, a operação falhou.", "Innovatio - Erro");
                    }   
                    
                }
                else if(res == DialogResult.No)
                {
                    MessageBox.Show("Digite uma parcela do valor remanescente da factura.", "Innovation - Atenção");
                    cbSaldarCompletamenteAFactura.Checked = false;
                    txtValorReceBido.Focus();
                    return;  
                }
                else
                {
                    return;
                }
            }
            else if((this.valorRecebidoEmUSD == this.debitoEmUSD) && (comboBoxMoeda.SelectedItem.ToString() == Gerais.PARAM_MOEDA_PDR))
            {
                MessageBoxButtons botoes = MessageBoxButtons.YesNoCancel;
                DialogResult res;

                res = MessageBox.Show("Tem certeza que pretende saldar totalmente a factura?", "Innovation - Atenção", botoes);

                if (res == DialogResult.Yes)
                {
                    try
                    {
                        this.saldarTotalmenteAFatura();
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("Verifique se inseriu os dados correctamente, a operação falho", "Innovation - Erro");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Não foi possivel saldar a factura na totalidade, a operação falhou.", "Innovatio - Erro");
                    }   
                }
                else if (res == DialogResult.No)
                {
                    MessageBox.Show("Digite uma parcela do valor remanescente da factura.", "Innovation - Atenção");
                    cbSaldarCompletamenteAFactura.Checked = false;
                    txtValorReceBido.Focus();
                    return;
                }
                else
                {
                    return;
                }
            }
            else
            {
                MessageBoxButtons botoes = MessageBoxButtons.YesNo;

                DialogResult resultadoDaMensagem = MessageBox.Show("Pretende efectuar pagamento da factura parceladamente?", "Innovation - Atenção", botoes);

                if (resultadoDaMensagem == DialogResult.Yes)
                {
                    try
                    {
                        //Confirma pagamento do valor parcial da factura
                        this.saldarParcialmenteAFatura();
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("Verifique se inseriu os dados correctamente, a operação falho", "Innovation - Erro");
                    }
                    catch(Exception)
                    {
                        MessageBox.Show("Não foi possivel saldar parcialmente a factura, a operação falhou.", "Innovatio - Erro");
                    }
                    
                }
                else
                {
                    MessageBox.Show("Pagamento cancelado.", "Innovation - Informação");
                    txtValorReceBido.Focus();
                }
                
            } 
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            MessageBoxButtons botoes = MessageBoxButtons.YesNo;

            DialogResult janelaDeConfirmacao = MessageBox.Show("Deseja sair de 'Pagamento de factura'?", "Innovation - Aviso", botoes);

            if (janelaDeConfirmacao == DialogResult.Yes)
            {
                this.Hide();
                GC.Collect();
            }
            else
            {
                return;
            }
     
        }

        private DateTime proximoDiaUtil(string data, int numeroDeDias)
        {
            DateTime dData = Convert.ToDateTime(data);
            int nDias = 0, nYear, nMonth, nNewDay, nWeekDay;
            if (data == null || numeroDeDias == null)
                return dData;
            else
            {
                while (nDias < numeroDeDias)
                {
                    nYear = dData.Year;
                    nMonth = dData.Month;
                    nNewDay = dData.Day;
                    nNewDay = nNewDay + 1;
                    //dData = SalDateConstruct(nYear, nMonth, nNewDay, 0, 0, 0);
                    nWeekDay = Convert.ToInt32(dData.DayOfWeek);
                    // ============= Verificar se o dia resultante do incremento calha num Sábado/Domingo ========================
                    if (Gerais.PARAM_SAB_DIA_UTIL != "1")
                    {
                        if (nWeekDay != 0 && nWeekDay != 1)
                            nDias = nDias + 1;
                        // ============= Se não for Sábado/Domingo verificar se o dia em questão é um Feriado =======================
                        else
                        {
                            nDias = nDias + 1;
                        }
                    }
                    else if (Gerais.PARAM_DOM_DIA_UTIL != "1")
                    {
                        if (nWeekDay != 1)
                            nDias = nDias + 1;
                        else
                            nDias = nDias + 1;
                    }
                }
                return dData;
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            FormPesquisarNotasDeDebito pesquisarNotaDeDebito = new FormPesquisarNotasDeDebito();
            pesquisarNotaDeDebito.Show();
        }
    }
}
