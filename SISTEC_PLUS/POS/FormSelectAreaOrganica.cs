﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;


namespace SISTEC_PLUS.POS
{
    public partial class FormSelectAreaOrganica : MetroFramework.Forms.MetroForm
    {
        SqlConnection Conexao = new SqlConnection(Variavel.Conexao);
        public FormSelectAreaOrganica()
        {
            InitializeComponent();
        }

        //Funcao retorna a lista das areas organicas validas
        public List<Object> getNomesDasAreasOrganicas()
        {
            
            var cmd = new SqlCommand("Select NomeAO from AIAREAORGANICA WITH (NOLOCK) WHERE ISNULL(TO_DISPLAY,'S') ='S'", Conexao);
            List<Object> lista = new List<object>();

            Conexao.Open();
            var res = cmd.ExecuteReader();
            while (res.Read())
            {
                lista.Add(res["NomeAO"].ToString());
            }

            Conexao.Close();
            return lista;
        }

        //FUNCAO getCodAreaOrganica(string nomeDaAreaOrganica)
        public string getCodAreaOrganica(string nomeDaAreaOrganica)
        {
            SqlCommand cmd = new SqlCommand("Select CodAO from AIAREAORGANICA WITH (NOLOCK) where NomeAO = '"+ nomeDaAreaOrganica +"'", Conexao);
            Conexao.Open();
            string res = cmd.ExecuteScalar() as string;
            Conexao.Close();
            return res;
        }


        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            if (getNomesDasAreasOrganicas().Count > 0)
            {
                foreach (string area in getNomesDasAreasOrganicas())
                {
                    comboBoxAreasOrganicas.Items.Add(area);
                }
            }
        }

        private void FormSelectAreaOrganica_Load(object sender, EventArgs e)
        {
            MessageBox.Show("A Área Orgânica desta Factura deixou de ser válida, terá que seleccionar outra na lista a seguir. Obrigado.", "Innovation - Atenção");
            return;
        }

        private void buttonConfirmar_Click(object sender, EventArgs e)
        {
            //Verifica se a comboBoxAreasOrganicas esta vazia
            if(comboBoxAreasOrganicas.SelectedIndex == -1)
            {
                MessageBox.Show("Seleccione uma area organica.", "Innovation - Alerta!");
                return;
            }

            Variavel.codArea = getCodAreaOrganica(comboBoxAreasOrganicas.SelectedItem as string);

            //debug
            MessageBox.Show("Você escolheu a area: " + comboBoxAreasOrganicas.SelectedItem.ToString()+"- Cod:"+ Variavel.codArea, "Innovation - Confirmação");

            FormPagamentoFactura.areaOrganicaFoiAlterada = true;
            
            //fecha o FormSelectAreaOrganica
            this.Close();
        }

        
    }
}
