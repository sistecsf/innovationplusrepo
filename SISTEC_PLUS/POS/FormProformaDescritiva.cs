﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS.POS
{
    public partial class FormProformaDescritiva : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        string codCliente, nomeCliente, telefoneCliente, codArea, codVendedor = "", imposto;
        double desconto, subTota, total, descontoTotal, impost, imposto1, quantidade, valor, sub;
        string sEstado, sTName, sDescricaoDoc, nIdUnicoFD, nNumDoc, sDescricaoFD, nTotal_NAC, nTotal_PDR, GARANTIA, INSTALACAO, PEM;
        bool bImprimir;
        string factura, moeda, data, codEntidade, nomeEntidade, telefoneL, postal, morada, Morada, DESCT, ValorImpostoConsumo, ValorImpostoConsumoD, refe, refe2, PRECO, PRECOD, codLoja;
        DateTime Data;
        ProformaDescritiva form;
        int linha;
        public FormProformaDescritiva()
        {
            InitializeComponent();
        }
        private void imprimir()
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 GARANTIA, INSTALACAO, PEM FROM ASDETALHE where IDUnico = '" + nIdUnicoFD + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        GARANTIA = reader[0].ToString();
                        INSTALACAO = reader[1].ToString();
                        PEM = reader[2].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            if (GARANTIA == null || GARANTIA == "")
                GARANTIA = ".";
            if (INSTALACAO == null || INSTALACAO == "")
                INSTALACAO = ".";
            if (PEM == null || PEM == "")
                PEM = ".";
            try
            {
                SqlCommand cmd3 = new SqlCommand("select R.NumFact, R.Moeda, R.DataCria, R.CodEntidade, F.NomeEntidade, F.Morada, R.DESCT, R.ValorImpostoConsumo, R.ValorImpostoConsumoD, R.PRECO, R.PRECOD, R.CodLoja, F.FONE from ARegDoc R with (NOLOCK), AFENTIDADE F with (NOLOCK) where IDUnico = '" + nIdUnicoFD + "' AND R.CodEntidade = F.CodEntidade", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd3.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        factura = reader[0].ToString();
                        moeda = reader[1].ToString();
                        data = reader[2].ToString();
                        codEntidade = reader[3].ToString();
                        nomeEntidade = reader[4].ToString();
                        Morada = reader[5].ToString();
                        DESCT = reader[6].ToString();
                        ValorImpostoConsumo = reader[7].ToString();
                        ValorImpostoConsumoD = reader[8].ToString();
                        PRECO = reader[9].ToString();
                        PRECOD = reader[10].ToString();
                        codLoja = reader[11].ToString();
                        telefoneL = reader[12].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            MessageBox.Show(nIdUnicoFD);
            Data = Convert.ToDateTime(data);
            data = "" + Data.Day + "-" + Data.Month + "-" + Data.Year;
            if (moeda == "USD")
            {
                PRECO = PRECOD;
                ValorImpostoConsumo = ValorImpostoConsumoD;
            }
            if (DESCT == null)
                DESCT = "0";
            if (ValorImpostoConsumo == null)
                ValorImpostoConsumo = "0";
            if (morada == null || morada == "") morada = ".";
            if (telefoneL == null || telefoneL == "") telefoneL = ".";
            if (postal == null || postal == "") postal = ".";
            if (Morada == null || Morada == "") Morada = ".";

            form = new ProformaDescritiva(PEM, INSTALACAO, GARANTIA, DESCT, ValorImpostoConsumo, PRECO, nNumDoc + " / " + codLoja, nIdUnicoFD, moeda, data, Empreza.sEmpresaContaKZ, Morada, telefoneL, nomeEntidade);
            form.ShowDialog();
        }
        private void cmbNomeCliente()
        {
            int tipo = 0;
            SqlCommand cmd = new SqlCommand("SP_ComboEntidade", conexao);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@p_iTipoSeleccao", tipo));
            cmd.Parameters.Add(new SqlParameter("@p_sNivelUSER", Permissoes.nPrgClie));
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbNome.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void preemxeCombo(string sql, ComboBox combo)
        {
            SqlCommand cmd = new SqlCommand(sql);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    combo.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void gravar()
        {
            sEstado = "E";
            bImprimir = false;
            sTName = "T_PDESCRITIVA";
            if (cmbMoeda.Text != null && cmbMoeda.Text != "")
                moeda = cmbMoeda.Text;
            else moeda = cmbMoeda.PromptText;
            SqlCommand cmd = new SqlCommand("Select DescrDoc From asdocs where coddoc = '" + Gerais.PARAM_CODDOC_PD + "'", conexao);
            conexao.Open();
            sDescricaoDoc = "" + cmd.ExecuteScalar();
            conexao.Close();
            SqlCommand cmd1 = new SqlCommand("SELECT RTRIM(ISNULL(CODLOJA, '" + Variavel.codLoja + "')) + CAST (ISNULL( MAX( CAST(RIGHT (RTRIM(IDUNICO), LEN(RTRIM(IDUNICO)) - ( LEN(ISNULL(CODLOJA, '" + Variavel.codLoja + "')) ) )AS INT ) ) , 1) + 1 AS VARCHAR(23)) AS IDUNICO FROM AREGDOC WITH (XLOCK) WHERE CODLOJA= '" + Variavel.codLoja + "' AND LEFT(IDUNICO, LEN(CODLOJA) ) = '" + Variavel.codLoja + "' GROUP BY CODLOJA", conexao);
            conexao.Open();
            nIdUnicoFD = "" + cmd1.ExecuteScalar();
            conexao.Close();
            SqlCommand cmd2 = new SqlCommand("Select ISNULL(MAX(NumDoc),0) + 1 from ARegDoc WITH (XLOCK) where CodDoc = '" + Gerais.PARAM_CODDOC_PD + "' and CodLoja= '" + Variavel.codLoja + "'", conexao);
            conexao.Open();
            nNumDoc = "" + cmd2.ExecuteScalar();
            conexao.Close();
            sDescricaoFD = sDescricaoDoc + " II " + " - " + codArea;
            if (txtDesconto.Text == "")
                desconto = 0;
            else desconto = Convert.ToDouble(txtDesconto.Text);

            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                if (cmbMoeda.Text == Gerais.PARAM_MOEDA_NAC || cmbMoeda.PromptText == Gerais.PARAM_MOEDA_NAC)
                {
                    ValorImpostoConsumo = lbImposto.Text;
                    ValorImpostoConsumoD = "" + Convert.ToDouble(ValorImpostoConsumo) / Convert.ToDouble(lbCambio.Text);
                    nTotal_NAC = lbTotal.Text;
                    nTotal_PDR = "" + Convert.ToDouble(nTotal_NAC) / Convert.ToDouble(lbCambio.Text);
                    SqlCommand comand = new SqlCommand("INSERT INTO ARegDoc ( IDUnico, DataCria,                          CodUtilizador,                        CodDoc,                      CodEntidade,              NumDoc,                CodLoja,                 CodAO,                           PRECO,                           PRECOD,                         NumFact,             Data_Lancamento,                     Moeda,                  CodVend,                 IDOrig,            STATUS,            DESCT,                        ValorImpostoConsumo,                           ValorImpostoConsumoD,                      DESCR )" +
                                                "VALUES ('" + nIdUnicoFD + "', Convert(Varchar(20),GetDate(),120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_PD + "', '" + txtCliente.Text + "', '" + nNumDoc + "', '" + Variavel.codLoja + "', '" + codArea + "', '" + nTotal_NAC.Replace(',', '.') + "', '" + nTotal_PDR.Replace(',', '.') + "', '" + nNumDoc + "', Convert(Varchar(10),GetDate(),120), '" + moeda + "', '" + codVendedor + "', '" + nIdUnicoFD + "', '" + sEstado + "', '" + desconto + "', '" + ValorImpostoConsumo.Replace(',', '.') + "', '" + ValorImpostoConsumoD.Replace(',', '.') + "', '" + txtAssunto.Text + "')");
                    
                    comand.Connection = conexao;
                    comand.Transaction = tran;
                    comand.ExecuteNonQuery();
                }
                else if (cmbMoeda.Text == Gerais.PARAM_MOEDA_PDR || cmbMoeda.PromptText == Gerais.PARAM_MOEDA_PDR)
                {
                    ValorImpostoConsumoD = lbImposto.Text;
                    ValorImpostoConsumo = "" + Convert.ToDouble(ValorImpostoConsumoD) * Convert.ToDouble(lbCambio.Text);
                    nTotal_PDR = lbTotal.Text;
                    nTotal_NAC = "" + Convert.ToDouble(nTotal_PDR) * Convert.ToDouble(lbCambio.Text);
                    SqlCommand comand = new SqlCommand("INSERT INTO ARegDoc (IDUnico,         DataCria,                      CodUtilizador,                       CodDoc,                     CodEntidade,             NumDoc,               CodLoja,                  CodAO,                        PRECO,                              PRECOD,                         NumFact,                     Data_Lancamento,              Moeda,                 CodVend,               IDOrig,               STATUS,            DESCT,                   ValorImpostoConsumo,                             ValorImpostoConsumoD,                       DESCR )" +
                                                "VALUES ('" + nIdUnicoFD + "', Convert(Varchar(20),GetDate(),120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_PD + "', '" + txtCliente.Text + "', '" + nNumDoc + "', '" + Variavel.codLoja + "', '" + codArea + "', '" + nTotal_NAC.Replace(',', '.') + "', '" + nTotal_PDR.Replace(',', '.') + "', '" + nNumDoc + "', Convert(Varchar(10),GetDate(),120), '" + cmbMoeda.Text + "', '" + codVendedor + "', '" + nIdUnicoFD + "', '" + sEstado + "', '" + desconto + "', '" + ValorImpostoConsumo.Replace(',', '.') + "', '" + ValorImpostoConsumoD.Replace(',', '.') + "', '" + txtAssunto.Text + "')");
                    
                    comand.Connection = conexao;
                    comand.Transaction = tran;
                    comand.ExecuteNonQuery();
                }
                //************************************************** INSERT ASDETALHE **************************************************
                for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                {
                    if (metroGrid1[3, i].Value == null)
                        imposto = "0";
                    else
                        imposto = metroGrid1[3, i].Value.ToString();
                    SqlCommand comd = new SqlCommand("INSERT INTO ASDETALHE (IDUNICO,           CodDoc,                           DataCria,                        CodUtilizador,                             Item,                                         Descricao,                                            QTD,              ImpostoConsumo,                                    PrecoUnit,                                                           PrecoTotal,                        PAGA_Nac,                          PAGA_Pdr,                       ASSUNTO,                    GARANTIA,                 INSTALACAO,                    PEM)" +
                                                    "VALUES ('" + nIdUnicoFD + "', '" + Gerais.PARAM_CODDOC_PD + "', CONVERT(VARCHAR(20),GETDATE(),120), '" + Variavel.codUtilizador + "', '" + metroGrid1[0, i].Value.ToString() + "', '" + metroGrid1[1, i].Value.ToString() + "', '" + metroGrid1[2, i].Value.ToString() + "', '" + imposto + "', '" + metroGrid1[4, i].Value.ToString().Replace(',', '.') + "', '" + metroGrid1[5, i].Value.ToString().Replace(',', '.') + "', '" + Empreza.sEmpresaContaKZ + "', '" + Empreza.sEmpresaContaUSD + "', '" + txtAssunto.Text + "', '" + txtGarantia.Text + "', '" + txtInstalacao.Text + "', '" + txtPrazoEntrega.Text + "')");
                    
                    comd.Connection = conexao;
                    comd.Transaction = tran;
                    comd.ExecuteNonQuery();
                }
                tran.Commit();
                MessageBox.Show("Guardado Com Sucesso!");
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void limparCampos()
        {
            btGravar.Enabled = false;
            lbCambio.Text = Variavel.cambio;
            txtPagamentoNacional.Text = Empreza.sEmpresaContaKZ;
            txtPagamentoPadrao.Text = Empreza.sEmpresaContaUSD;
            txtCliente.Text = "";
            txtAssunto.Text = "";
            txtDesconto.Text = "";
            txtPrazoEntrega.Text = "";
            txtGarantia.Text = "";
            txtInstalacao.Text = "";
            cmbNome.Text = "";
            cmbArea.Text = "";
            cmbVenderor.Text = "";
            cmbNome.PromptText = "";
            cmbArea.PromptText = "";
            cmbVenderor.PromptText = "";
            lbNFactura.Text = "0";
            lbSubTotal.Text = "0";
            lbDesconto.Text = "0";
            lbImposto.Text = "0";
            lbTotal.Text = "0";
            metroGrid1.Rows.Clear();
        }
        private void FormProformaDescritiva_Load(object sender, EventArgs e)
        {
            btGravar.Enabled = false;
            lbCambio.Text = Variavel.cambio;
            txtPagamentoNacional.Text = Empreza.sEmpresaContaKZ;
             txtPagamentoPadrao.Text = Empreza.sEmpresaContaUSD;
        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            limparCampos();
        }

        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                try
                {
                    SqlCommand cmd2 = new SqlCommand("SELECT RTRIM(NomeEntidade), FONE FROM AFENTIDADE WITH (NOLOCK) WHERE RTRIM(CODENTIDADE) = '" + txtCliente.Text + "' ", conexao);
                    conexao.Open();
                    using (SqlDataReader reader = cmd2.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            nomeCliente = reader[0].ToString();
                            telefoneCliente = reader[1].ToString();
                        }
                    }
                    cmbNome.Text = nomeEntidade = nomeCliente;
                    cmbNome.PromptText = nomeCliente;
                }
                catch (Exception) { }
                finally { conexao.Close(); }
            }
        }

        private void cmbArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("Select CodAO from AIAREAORGANICA WITH (NOLOCK) where NomeAO = '" + cmbArea.Text + "'", conexao);
            conexao.Open();
            codArea = (string)cmd.ExecuteScalar();
            conexao.Close();
        }

        private void metroGrid1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            impost = 0;
            linha = e.RowIndex;
            this.metroGrid1[0, e.RowIndex].Value = (e.RowIndex + 1);
            if (e.ColumnIndex == 2)
            {
                if (this.metroGrid1[4, e.RowIndex].Value != null)
                {
                    try { valor = Convert.ToDouble(this.metroGrid1[4, e.RowIndex].Value.ToString()); }
                    catch { 
                        MessageBox.Show("Valor INvalido " + this.metroGrid1[4, e.RowIndex].Value.ToString() + " na coluna PVP");
                        this.metroGrid1[4, e.RowIndex].Value = "0";
                    }
                }
            }
            else if (e.ColumnIndex == 4)
            {
                if (this.metroGrid1[2, e.RowIndex].Value != null)
                {
                    try { quantidade = Convert.ToDouble(this.metroGrid1[2, e.RowIndex].Value.ToString()); }
                    catch
                    {
                        MessageBox.Show("Valor INvalido " + this.metroGrid1[2, e.RowIndex].Value.ToString() + " na coluna Quantidade");
                        this.metroGrid1[2, e.RowIndex].Value = "0";
                    }
                }
            }
            else if (e.ColumnIndex == 5)
            {
                subTota = 0;
                try { desconto = Convert.ToDouble(txtDesconto.Text); }
                catch { desconto = 0; }
                try { quantidade = Convert.ToDouble(this.metroGrid1[2, e.RowIndex].Value.ToString()); }
                catch { quantidade = 0; }
                try { valor = Convert.ToDouble(this.metroGrid1[4, e.RowIndex].Value.ToString()); }
                catch { valor = 0; }

                this.metroGrid1[5, e.RowIndex].Value = quantidade * valor;
                for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                {
                    try { sub = Convert.ToDouble(this.metroGrid1[5, i].Value.ToString()); }
                    catch { sub = 0; }
                    if (this.metroGrid1[3, i].Value != null)
                    {
                        try { imposto1 = Convert.ToDouble(this.metroGrid1[3, i].Value.ToString()); }
                        catch { imposto1 = 0; }
                        impost = impost + (sub * imposto1 / 100);
                    }
                    subTota = subTota + sub;
                }
                lbSubTotal.Text = "" + subTota;
                lbDesconto.Text = "" + subTota * desconto / 100;
                lbImposto.Text = "" + impost;
                descontoTotal = subTota * desconto / 100;
                total = subTota - descontoTotal + impost;
                lbTotal.Text = "" + total;
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            FormFacturaDescritivaApagar form = new FormFacturaDescritivaApagar();
            FormFacturaDescritivaApagar.tipo = 3;
            form.ShowDialog();
        }

        private void btGravar_Click(object sender, EventArgs e)
        {
            if (txtCliente.Text != "")
            {
                if (cmbNome.Text != "" || cmbNome.PromptText != "")
                {
                    if (cmbArea.Text != "" || cmbArea.PromptText != "")
                    {
                        if (cmbMoeda.Text != "" || cmbMoeda.PromptText != "")
                        {
                            if (txtAssunto.Text != "")
                            {
                                if (metroGrid1.RowCount > 1)
                                {
                                    gravar();
                                    imprimir();
                                }
                                else
                                    MessageBox.Show("A tabela tem de ser prenxido!");
                            }
                            else
                                MessageBox.Show("O campo [ Assunto ] tem de ser preenchido. Por favor reveja o lançamento!");
                        }
                        else
                            MessageBox.Show("O campo [ Moeda ] tem de ser preenchido. Por favor reveja o lançamento!");
                    }
                    else
                        MessageBox.Show("O campo [ Área ] tem de ser preenchido. Por favor reveja o lançamento!");
                }
                else
                    MessageBox.Show("O campo [ Cliente ] tem de ser preenchido. Por favor reveja o lançamento!");
            }
            else
                MessageBox.Show("O campo [ Cliente ] tem de ser preenchido. Por favor reveja o lançamento!");
        }

        private void cmbVenderor_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("SELECT CodEntidade FROM AFENTIDADE with (NoLock) WHERE NomeEntidade = '" + cmbVenderor.Text + "'", conexao);
            conexao.Open();
            codVendedor = (string)cmd.ExecuteScalar();
            conexao.Close();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            FormFacturaDescritivaConsulta form = new FormFacturaDescritivaConsulta();
            FormFacturaDescritivaConsulta.tipo = 3;
            form.ShowDialog();
        }

        private void cmbContacto_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void cmbVenderor_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private void cmbNome_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void cmbArea_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void cmbNome_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("SELECT RTRIM(CODENTIDADE), FONE FROM AFENTIDADE WITH (NOLOCK) WHERE RTRIM(NomeEntidade) = '" + cmbNome.Text + "' ", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codCliente = reader[0].ToString();
                        telefoneCliente = reader[1].ToString();
                    }
                }
                txtCliente.Text = codCliente;
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }

        private void cmbNome_MouseClick_1(object sender, MouseEventArgs e)
        {
            if (cmbNome.Items.Count <= 0)
            {
                cmbNome.Items.Clear();
                Cursor.Current = Cursors.WaitCursor;
                cmbNomeCliente();
            }
        }

        private void lbTotal_Click(object sender, EventArgs e)
        {

        }

        private void FormProformaDescritiva_Leave(object sender, EventArgs e)
        {
            limparCampos();
        }

        private void cmbVenderor_DropDown(object sender, EventArgs e)
        {
            cmbVenderor.Items.Clear();
            string sql = @"SELECT NomeEntidade FROM AFENTIDADE with (NoLock) WHERE CodTipoCli = 'VEN' ORDER BY NomeEntidade";
            preemxeCombo(sql, cmbVenderor);
        }

        private void cmbArea_DropDown(object sender, EventArgs e)
        {
            string sql = @"Select NomeAO from AIAREAORGANICA WITH (NOLOCK) WHERE ISNULL(TO_DISPLAY,'S') ='S'";
            cmbArea.Items.Clear();
            preemxeCombo(sql, cmbArea);
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void metroGrid1_Leave(object sender, EventArgs e)
        {
            metroGrid1.ReadOnly = true;
            subTota = 0;
            try { desconto = Convert.ToDouble(txtDesconto.Text); }
            catch { desconto = 0; }
            try { quantidade = Convert.ToDouble(this.metroGrid1[2, linha].Value.ToString()); }
            catch { quantidade = 0; }
            try { valor = Convert.ToDouble(this.metroGrid1[4, linha].Value.ToString()); }
            catch { valor = 0; }

            this.metroGrid1[5, linha].Value = quantidade * valor;
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                try { sub = Convert.ToDouble(this.metroGrid1[5, i].Value.ToString()); }
                catch { sub = 0; }
                if (this.metroGrid1[3, i].Value != null)
                {
                    try { imposto1 = Convert.ToDouble(this.metroGrid1[3, i].Value.ToString()); }
                    catch { imposto1 = 0; }
                    impost = impost + (sub * imposto1 / 100);
                }
                subTota = subTota + sub;
            }
            lbSubTotal.Text = "" + subTota;
            lbDesconto.Text = "" + subTota * desconto / 100;
            lbImposto.Text = "" + impost;
            descontoTotal = subTota * desconto / 100;
            total = subTota - descontoTotal + impost;
            lbTotal.Text = "" + total;
            metroGrid1.ReadOnly = false;
        }
    }
}
