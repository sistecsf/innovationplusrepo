﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS.POS
{
    public partial class FormFacturaDescritivaConsulta : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        string DESCT, ValorImpostoConsumo, ValorImpostoConsumoD, PRECO, codLoja, refe, refe2;
        public static string dataIniacial, dataFim, sql;
        public static int tipo;
        string moeda, preco, precoD, area, nome, codigo, morada, GARANTIA, INSTALACAO, PEM;
        string factura, data, codEntidade, nomeEntidade, Morada, PRECOD, DESCR, NomeAO, telefoneL, postal;
        FacturaDescritiva form;
        DateTime Data;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        public FormFacturaDescritivaConsulta()
        {
            InitializeComponent();
        }
        private void recibo(string idUnico)
        {
            try
            {
                SqlCommand cmd3 = new SqlCommand("select R.NumFact, R.DataCria, R.CodEntidade, F.NomeEntidade, F.Morada, R.PRECOD, R.DESCR, A.NomeAO, RTRIM(R.CodLoja) from ARegDoc R with (NOLOCK), AFENTIDADE F with (NOLOCK), AIAREAORGANICA A WITH (NOLOCK), AFCONCLI C WITH (NOLOCK) where R.IDUnico = '" + idUnico + "' AND R.CodEntidade = F.CodEntidade AND R.CodAO = A.CodAO AND R.IDUnico = C.IDUnico", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd3.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        factura = reader[0].ToString();
                        data = reader[1].ToString();
                        codEntidade = reader[2].ToString();
                        nomeEntidade = reader[3].ToString();
                        Morada = reader[4].ToString();
                        PRECOD = reader[5].ToString();
                        DESCR = reader[6].ToString();
                        NomeAO = reader[7].ToString();
                        codigo = reader[8].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }

            try
            {
                SqlCommand cmd4 = new SqlCommand("Select Fone, Caixa_Post, Morada from ASLOJA WITH (NOLOCK) WHERE CodLoja = '" + codigo + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd4.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        telefoneL = reader[0].ToString();
                        postal = reader[1].ToString();
                        morada = reader[2].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
            Data = Convert.ToDateTime(data);
            data = "" + Data.Day + "-" + Data.Month + "-" + Data.Year;

            form = new FacturaDescritiva(nomeEntidade, codEntidade, Morada, morada, telefoneL, postal, idUnico, factura + " / " + codigo, NomeAO, DESCR, PRECOD, data);
            form.ShowDialog();
        }
        private void imprimir(string idUnico)
        {
            try
            {
                SqlCommand cmd3 = new SqlCommand("select R.NumFact, R.Moeda, R.DataCria, R.CodEntidade, F.NomeEntidade, F.Morada, R.DESCT, R.ValorImpostoConsumo, R.ValorImpostoConsumoD, R.PRECO, R.PRECOD, R.CodLoja from ARegDoc R with (NOLOCK), AFENTIDADE F with (NOLOCK) where IDUnico = '" + idUnico + "' AND R.CodEntidade = F.CodEntidade", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd3.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        factura = reader[0].ToString();
                        moeda = reader[1].ToString();
                        data = reader[2].ToString();
                        codEntidade = reader[3].ToString();
                        nomeEntidade = reader[4].ToString();
                        Morada = reader[5].ToString();
                        DESCT = reader[6].ToString();
                        ValorImpostoConsumo = reader[7].ToString();
                        ValorImpostoConsumoD = reader[8].ToString();
                        PRECO = reader[9].ToString();
                        PRECOD = reader[10].ToString();
                        codLoja = reader[11].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }

            try
            {
                SqlCommand cmd4 = new SqlCommand("Select Fone, Caixa_Post, Morada from ASLOJA WITH (NOLOCK) WHERE CodLoja = '" + codLoja + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd4.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        telefoneL = reader[0].ToString();
                        postal = reader[1].ToString();
                        morada = reader[2].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }

            Data = Convert.ToDateTime(data);
            data = "" + Data.Day + "-" + Data.Month + "-" + Data.Year;
            if (moeda == "USD")
            {
                refe = "Referencia:";
                refe2 = ValorImpostoConsumo;
                PRECO = PRECOD;
                ValorImpostoConsumo = ValorImpostoConsumoD;
            }
            else
            {
                refe = ".";
                refe2 = ".";
            }
            if (DESCT == null)
                DESCT = "0";
            if (ValorImpostoConsumo == null)
                ValorImpostoConsumo = "0";
            if (morada == null || morada == "") morada = ".";
            if (telefoneL == null || telefoneL == "") telefoneL = ".";
            if (postal == null || postal == "") postal = ".";
            if (Morada == null || Morada == "") Morada = ".";

            FacturaD2 form1 = new FacturaD2(telefoneL, postal, morada, PRECO, refe, refe2, DESCT, ValorImpostoConsumo, codEntidade, nomeEntidade, Morada, idUnico, factura + " / " + codLoja, moeda, data);
            form1.ShowDialog();
        }
        private void imprimir2(string idUnico)
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("SELECT TOP 1 GARANTIA, INSTALACAO, PEM FROM ASDETALHE where IDUnico = '" + idUnico + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        GARANTIA = reader[0].ToString();
                        INSTALACAO = reader[1].ToString();
                        PEM = reader[2].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
            if (GARANTIA == null || GARANTIA == "")
                GARANTIA = ".";
            if (INSTALACAO == null || INSTALACAO == "")
                INSTALACAO = ".";
            if (PEM == null || PEM == "")
                PEM = ".";
            try
            {
                SqlCommand cmd3 = new SqlCommand("select R.NumFact, R.Moeda, R.DataCria, R.CodEntidade, F.NomeEntidade, F.Morada, R.DESCT, R.ValorImpostoConsumo, R.ValorImpostoConsumoD, R.PRECO, R.PRECOD, R.CodLoja, F.FONE from ARegDoc R with (NOLOCK), AFENTIDADE F with (NOLOCK) where IDUnico = '" + idUnico + "' AND R.CodEntidade = F.CodEntidade", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd3.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        factura = reader[0].ToString();
                        moeda = reader[1].ToString();
                        data = reader[2].ToString();
                        codEntidade = reader[3].ToString();
                        nomeEntidade = reader[4].ToString();
                        Morada = reader[5].ToString();
                        DESCT = reader[6].ToString();
                        ValorImpostoConsumo = reader[7].ToString();
                        ValorImpostoConsumoD = reader[8].ToString();
                        PRECO = reader[9].ToString();
                        PRECOD = reader[10].ToString();
                        codLoja = reader[11].ToString();
                        telefoneL = reader[12].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
            Data = Convert.ToDateTime(data);
            data = "" + Data.Day + "-" + Data.Month + "-" + Data.Year;
            if (moeda == "USD")
            {
                PRECO = PRECOD;
                ValorImpostoConsumo = ValorImpostoConsumoD;
            }
            if (DESCT == null)
                DESCT = "0";
            if (ValorImpostoConsumo == null)
                ValorImpostoConsumo = "0";
            if (morada == null || morada == "") morada = ".";
            if (telefoneL == null || telefoneL == "") telefoneL = ".";
            if (postal == null || postal == "") postal = ".";
            if (Morada == null || Morada == "") Morada = ".";

            ProformaDescritiva form = new ProformaDescritiva(PEM, INSTALACAO, GARANTIA, DESCT, ValorImpostoConsumo, PRECO, factura + " / " + codLoja, idUnico, moeda, data, Empreza.sEmpresaContaKZ, Morada, telefoneL, nomeEntidade);
            form.ShowDialog();
        }
        private void prenxerCmbLoja()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeLoja from ASLOJA with (NoLock) order by NomeLoja");
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbLoja.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void decritiva1()
        {
            sql = @"select DISTINCT CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105), R.CodDoc, R.DESCR, R.NumDoc, LTrim(RTrim(C.CodEntidade)) + ' - ' +" +
                              "LTrim(RTrim(C.NomeEntidade)) AS CLIENTE, STATUS = CASE R.Status WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO' WHEN 'F' THEN 'PAG. PARCIAL' WHEN 'J' THEN 'ADJUDICADO' WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END," +
                                "RTRIM(R.IDunico), U.NOMEUTILIZADOR, R.CodLoja from AFCONCLI F WITH (NOLOCK), ARegDoc R WITH (NOLOCK), AFENTIDADE C WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK), ASDETALHE H with (NOLOCK) " +
                                "where R.CodDoc = '" + Gerais.PARAM_CODDOC_FD + "' AND R.CodEntidade = C.CodEntidade AND R.CodUtilizador = U.CodUtilizador AND R.IDUnico = F.IDUnico and R.IDUnico Not IN (Select IDunico From ASDETALHE) ";
            if (Convert.ToInt32(Permissoes.nPrgPos) < 3)
                sql = sql + "AND R.codutilizador = '" + Variavel.codUtilizador + "' ";
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                sql = sql + "AND CONVERT(DATETIME, R.DataCria, 120) > CONVERT(DATETIME, '" + dataIniacial + "', 120) AND CONVERT(DATETIME, R.DataCria, 120) <= CONVERT(DATETIME, '" + dataFim + " 18:59" + "', 120)";
            if (txtNumero.Text != "")
                sql = sql + " AND C.CodEntidade LIKE '" + txtNumero.Text + "%'";
            if (cmbLoja.Text != "")
                sql = sql + " AND R.codloja = '" + codLoja + "'";
            if (txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '" + txtNome.Text + "%'";
            if (txtCodArtigo.Text != "")
                sql = sql + " And F.Referenc Like '%" + txtCodArtigo.Text + "%'";
            if (txtDescArtigo.Text != "")
                sql = sql + " AND M.DescrArtigo LIKE '%" + txtDescArtigo.Text + "%' ";
            sql = sql + " ORDER BY CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME),105), R.coddoc, NUMDOC, CLIENTE";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void descritiva2()
        {
            sql = @"select DISTINCT CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105), R.CodDoc, R.DESCR, R.NumDoc, LTrim(RTrim(C.CodEntidade)) + ' - ' +" +
                              "LTrim(RTrim(C.NomeEntidade)) AS CLIENTE, STATUS = CASE R.Status WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO' WHEN 'F' THEN 'PAG. PARCIAL' WHEN 'J' THEN 'ADJUDICADO' WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END," +
                                "RTRIM(R.IDunico), U.NOMEUTILIZADOR, R.CodLoja from AFCONCLI F WITH (NOLOCK), ARegDoc R WITH (NOLOCK), AFENTIDADE C WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK), ASDETALHE H with (NOLOCK) " +
                                "where R.CodDoc = '" + Gerais.PARAM_CODDOC_FD + "' AND R.CodEntidade = C.CodEntidade AND R.CodUtilizador = U.CodUtilizador AND R.IDUnico = F.IDUnico and R.IDUnico IN (Select IDunico From ASDETALHE) ";
            if (Convert.ToInt32(Permissoes.nPrgPos) < 3)
                sql = sql + "AND R.codutilizador = '" + Variavel.codUtilizador + "' ";
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                sql = sql + "AND CONVERT(DATETIME, R.DataCria, 120) > CONVERT(DATETIME, '" + dataIniacial + "', 120) AND CONVERT(DATETIME, R.DataCria, 120) <= CONVERT(DATETIME, '" + dataFim + " 18:59" + "', 120)";
            if (txtNumero.Text != "")
                sql = sql + " AND C.CodEntidade LIKE '" + txtNumero.Text + "%'";
            if (cmbLoja.Text != "")
                sql = sql + " AND R.codloja = '" + codLoja + "'";
            if (txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '" + txtNome.Text + "%'";
            if (txtCodArtigo.Text != "")
                sql = sql + " And F.Referenc Like '%" + txtCodArtigo.Text + "%'";
            if (txtDescArtigo.Text != "")
                sql = sql + " AND M.DescrArtigo LIKE '%" + txtDescArtigo.Text + "%' ";
            sql = sql + " ORDER BY CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME),105), R.coddoc, NUMDOC, CLIENTE";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void proforma()
        {
            sql = @"select DISTINCT CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105), R.CodDoc, R.DESCR, R.NumDoc, LTrim(RTrim(C.CodEntidade)) + ' - ' +" +
                              "LTrim(RTrim(C.NomeEntidade)) AS CLIENTE, STATUS = CASE R.Status WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO' WHEN 'F' THEN 'PAG. PARCIAL' WHEN 'J' THEN 'ADJUDICADO' WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END," +
                                "RTRIM(R.IDunico), U.NOMEUTILIZADOR, R.CodLoja from ARegDoc R WITH (NOLOCK), AFENTIDADE C WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK), ASDETALHE H with (NOLOCK) " +
                                "where R.CodDoc = '" + Gerais.PARAM_CODDOC_PD + "' AND R.CodEntidade = C.CodEntidade AND R.CodUtilizador = U.CodUtilizador ";
            if (Convert.ToInt32(Permissoes.nPrgPos) < 3)
                sql = sql + "AND R.codutilizador = '" + Variavel.codUtilizador + "' ";
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                sql = sql + "AND CONVERT(DATETIME, R.DataCria, 120) > CONVERT(DATETIME, '" + dataIniacial + "', 120) AND CONVERT(DATETIME, R.DataCria, 120) <= CONVERT(DATETIME, '" + dataFim + " 18:59" + "', 120)";
            if (txtNumero.Text != "")
                sql = sql + " AND C.CodEntidade LIKE '" + txtNumero.Text + "%'";
            if (cmbLoja.Text != "" || cmbLoja.PromptText != "")
                sql = sql + " AND R.codloja = '" + codLoja + "'";
            if (txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '" + txtNome.Text + "%'";
            if (txtCodArtigo.Text != "")
                sql = sql + " And F.Referenc Like '%" + txtCodArtigo.Text + "%'";
            if (txtDescArtigo.Text != "")
                sql = sql + " AND M.DescrArtigo LIKE '%" + txtDescArtigo.Text + "%' ";
            sql = sql + " ORDER BY CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME),105), R.coddoc, NUMDOC, CLIENTE";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void prenxerGrid()
        {
            if (tipo == 1)
                decritiva1();
            else if (tipo == 2)
                descritiva2();
            else if (tipo == 3)
                proforma();
        }
        private void GridDuploClick(int tip, DataGridView dataGrid)
        {
            if (tip == 1)
                recibo(dataGrid.CurrentRow.Cells[6].Value.ToString());
            else if (tip == 2)
                imprimir(dataGrid.CurrentRow.Cells[6].Value.ToString());
            else if (tip == 3)
                imprimir2(dataGrid.CurrentRow.Cells[6].Value.ToString());
        }

        private void FormFacturaDescritivaConsulta_Load(object sender, EventArgs e)
        {
            codLoja = Variavel.codLoja;
            txtDataInicial.Text = txtDataFinal.Text = DateTime.Now.ToShortDateString();
            prenxerCmbLoja();
            cmbLoja.PromptText = Variavel.nomeLoja;
        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                Data = Convert.ToDateTime(txtDataInicial.Text);
                dataIniacial = "" + Data.Year + "-" + Data.Month + "-" + Data.Day;
                try
                {
                    Data = Convert.ToDateTime(txtDataFinal.Text);
                    dataFim = "" + Data.Year + "-" + Data.Month + "-" + Data.Day;

                    metroGrid1.AutoGenerateColumns = false;
                    Cursor.Current = Cursors.WaitCursor;
                    prenxerGrid();
                }
                catch (Exception ex) { MessageBox.Show(ex.Message + " Data Invalida " + txtDataFinal.Text); }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message + " Data Invalida " + txtDataInicial.Text); }
        }

        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    try { GridDuploClick(tipo, metroGrid1); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void cmbLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("select CodLoja from ASLOJA with (nolock) where NomeLoja = '"+ cmbLoja.Text +"'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codLoja = reader[0].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try { GridDuploClick(tipo, metroGrid1); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void txtDataInicial_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtDataInicial.Text = ""; }
        }

        private void txtDataFinal_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtDataFinal.Text = ""; }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
