﻿namespace SISTEC_PLUS.OPS
{
    partial class FormTabelas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTabelas));
            this.TabTabelas = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btAtualizar = new System.Windows.Forms.ToolStripButton();
            this.txtLContaNacional = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.txtLContaPa = new MetroFramework.Controls.MetroTextBox();
            this.txtLContaPadrao = new MetroFramework.Controls.MetroLabel();
            this.txtLContaOutros = new MetroFramework.Controls.MetroTextBox();
            this.cbCodEmpresa = new MetroFramework.Controls.MetroLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pbLImagem = new System.Windows.Forms.PictureBox();
            this.metroCheckBox5 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox4 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox3 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox2 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox1 = new MetroFramework.Controls.MetroCheckBox();
            this.txtLPraso2 = new MetroFramework.Controls.MetroTextBox();
            this.txtLPraso = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.txtLFax = new MetroFramework.Controls.MetroTextBox();
            this.txtLTelefone = new MetroFramework.Controls.MetroTextBox();
            this.txtLCodigoPostal = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.txtLMarada = new MetroFramework.Controls.MetroTextBox();
            this.cmbLEmpresa = new MetroFramework.Controls.MetroComboBox();
            this.txtLDesignacao = new MetroFramework.Controls.MetroTextBox();
            this.txtLCodigo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.lbLoja = new MetroFramework.Controls.MetroLabel();
            this.lbArea = new MetroFramework.Controls.MetroLabel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbMEmitirRI = new MetroFramework.Controls.MetroCheckBox();
            this.cbMActivo = new MetroFramework.Controls.MetroCheckBox();
            this.cbMMercadoriaRemoto = new MetroFramework.Controls.MetroCheckBox();
            this.cbMMercadoriaVenda = new MetroFramework.Controls.MetroCheckBox();
            this.cmbMAreaOrganica = new MetroFramework.Controls.MetroComboBox();
            this.cmbMDesignacaoLoja = new MetroFramework.Controls.MetroComboBox();
            this.txtMContaCustos = new MetroFramework.Controls.MetroTextBox();
            this.txtMContaVendas = new MetroFramework.Controls.MetroTextBox();
            this.txtMDesignacao = new MetroFramework.Controls.MetroTextBox();
            this.txtMCodigo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.cbFContabilizacaoCusto = new MetroFramework.Controls.MetroCheckBox();
            this.cbFContabilizacaoStock = new MetroFramework.Controls.MetroCheckBox();
            this.cbFContabilizacaoVenda = new MetroFramework.Controls.MetroCheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtFPrazo = new MetroFramework.Controls.MetroTextBox();
            this.txtFDesconto = new MetroFramework.Controls.MetroTextBox();
            this.txtFDesignacao = new MetroFramework.Controls.MetroTextBox();
            this.txtFCodigo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.pbFImagem = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.metroTabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.cmbSFamilia2 = new MetroFramework.Controls.MetroComboBox();
            this.lbSFamilia = new MetroFramework.Controls.MetroLabel();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSDesignacao = new MetroFramework.Controls.MetroTextBox();
            this.txtSCodigo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel24 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel23 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage5 = new MetroFramework.Controls.MetroTabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroLabel72 = new MetroFramework.Controls.MetroLabel();
            this.txtAPercetagemQeuebra = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel71 = new MetroFramework.Controls.MetroLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbAImposto10 = new MetroFramework.Controls.MetroRadioButton();
            this.rbAInsento = new MetroFramework.Controls.MetroRadioButton();
            this.rbAImposto5 = new MetroFramework.Controls.MetroRadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtADataExpiracao = new System.Windows.Forms.MaskedTextBox();
            this.txtADataFabrico = new System.Windows.Forms.MaskedTextBox();
            this.metroLabel70 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel69 = new MetroFramework.Controls.MetroLabel();
            this.lbASubFam = new MetroFramework.Controls.MetroLabel();
            this.lbAFam = new MetroFramework.Controls.MetroLabel();
            this.lbAmerc = new MetroFramework.Controls.MetroLabel();
            this.lbALoj = new MetroFramework.Controls.MetroLabel();
            this.lbASfamilia = new MetroFramework.Controls.MetroLabel();
            this.lbAFamilia = new MetroFramework.Controls.MetroLabel();
            this.lbAMercadoria = new MetroFramework.Controls.MetroLabel();
            this.lbALoja = new MetroFramework.Controls.MetroLabel();
            this.cbABookingHotel = new MetroFramework.Controls.MetroCheckBox();
            this.cbATermoGarantia = new MetroFramework.Controls.MetroCheckBox();
            this.cbAMostraImagem = new MetroFramework.Controls.MetroCheckBox();
            this.cbAPromocao = new MetroFramework.Controls.MetroCheckBox();
            this.cbAPermiteOBS = new MetroFramework.Controls.MetroCheckBox();
            this.cbAActivo = new MetroFramework.Controls.MetroCheckBox();
            this.cbARegistoEquipamento = new MetroFramework.Controls.MetroCheckBox();
            this.cbAContabilizarStock = new MetroFramework.Controls.MetroCheckBox();
            this.cbAPermiteAlterar = new MetroFramework.Controls.MetroCheckBox();
            this.metroTabControl2 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage9 = new MetroFramework.Controls.MetroTabPage();
            this.label25 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.metroTextBox15 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox14 = new MetroFramework.Controls.MetroTextBox();
            this.metroCheckBox15 = new MetroFramework.Controls.MetroCheckBox();
            this.metroTextBox13 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox12 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox10 = new MetroFramework.Controls.MetroTextBox();
            this.txtAPCM = new MetroFramework.Controls.MetroTextBox();
            this.txtAPCP = new MetroFramework.Controls.MetroTextBox();
            this.txtAFOB = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel43 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel44 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel45 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel42 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel41 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel40 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage10 = new MetroFramework.Controls.MetroTabPage();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.Marca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modelo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumeroSerie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroTabPage11 = new MetroFramework.Controls.MetroTabPage();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.metroTextBox18 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox19 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel48 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel49 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox17 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox16 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel47 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel46 = new MetroFramework.Controls.MetroLabel();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.metroTabPage12 = new MetroFramework.Controls.MetroTabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.metroCheckBox17 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox16 = new MetroFramework.Controls.MetroCheckBox();
            this.txtADesconto = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel54 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox23 = new MetroFramework.Controls.MetroTextBox();
            this.txtALucro = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox21 = new MetroFramework.Controls.MetroTextBox();
            this.txtAPVP = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel53 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel52 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel51 = new MetroFramework.Controls.MetroLabel();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.metroLabel50 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage13 = new MetroFramework.Controls.MetroTabPage();
            this.txtAStockMimimo = new MetroFramework.Controls.MetroTextBox();
            this.txtAStockMaximo = new MetroFramework.Controls.MetroTextBox();
            this.txtAPontoEncomenda = new MetroFramework.Controls.MetroTextBox();
            this.txtAStockReal = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel58 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel57 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel56 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel55 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage14 = new MetroFramework.Controls.MetroTabPage();
            this.metroGrid2 = new MetroFramework.Controls.MetroGrid();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtANorme = new MetroFramework.Controls.MetroTextBox();
            this.txtALocal = new MetroFramework.Controls.MetroTextBox();
            this.txtQdtEmbalagem = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel38 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel37 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel36 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel35 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel34 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel33 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel32 = new MetroFramework.Controls.MetroLabel();
            this.txtADesignacapSuplementar = new System.Windows.Forms.RichTextBox();
            this.txtADesignacaoII = new MetroFramework.Controls.MetroTextBox();
            this.txtADesignacao = new MetroFramework.Controls.MetroTextBox();
            this.txtAReferencia = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel31 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel30 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel29 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel28 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel27 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel26 = new MetroFramework.Controls.MetroLabel();
            this.Loja = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage6 = new MetroFramework.Controls.MetroTabPage();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtODesignacao = new MetroFramework.Controls.MetroTextBox();
            this.txtOCodigo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel60 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel59 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage8 = new MetroFramework.Controls.MetroTabPage();
            this.lbALAreaOrganica = new MetroFramework.Controls.MetroLabel();
            this.lbALLoja = new MetroFramework.Controls.MetroLabel();
            this.cmbALAreaOrganica = new System.Windows.Forms.ComboBox();
            this.cmbALDesignacaoLoja = new System.Windows.Forms.ComboBox();
            this.metroCheckBox13 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox12 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox11 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox10 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox9 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox8 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox7 = new MetroFramework.Controls.MetroCheckBox();
            this.metroCheckBox6 = new MetroFramework.Controls.MetroCheckBox();
            this.metroGrid3 = new MetroFramework.Controls.MetroGrid();
            this.A = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.E = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.G = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.H = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label24 = new System.Windows.Forms.Label();
            this.txtALDesignacaoArmazem = new MetroFramework.Controls.MetroTextBox();
            this.txtALCodigo = new MetroFramework.Controls.MetroTextBox();
            this.cbALProcessadorMatriz = new MetroFramework.Controls.MetroCheckBox();
            this.cbALActivo = new MetroFramework.Controls.MetroCheckBox();
            this.cbALArmazemVenda = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel68 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel67 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel66 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel65 = new MetroFramework.Controls.MetroLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btNovo = new System.Windows.Forms.ToolStripButton();
            this.btGravar = new System.Windows.Forms.ToolStripButton();
            this.btConsultar = new System.Windows.Forms.ToolStripButton();
            this.btSair = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.cmbALoja1 = new MetroFramework.Controls.MetroComboBox();
            this.cmbAMercadoria1 = new MetroFramework.Controls.MetroComboBox();
            this.cmbAFamilia1 = new MetroFramework.Controls.MetroComboBox();
            this.cmbASubFamilia1 = new MetroFramework.Controls.MetroComboBox();
            this.cmbAGrupoWeb1 = new MetroFramework.Controls.MetroComboBox();
            this.cmbAUnidade = new MetroFramework.Controls.MetroComboBox();
            this.cmbADesigEmbalagem = new MetroFramework.Controls.MetroComboBox();
            this.lbAGrupo = new MetroFramework.Controls.MetroLabel();
            this.TabTabelas.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLImagem)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            this.toolStrip4.SuspendLayout();
            this.metroTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFImagem)).BeginInit();
            this.metroTabPage4.SuspendLayout();
            this.metroTabPage5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.metroTabControl2.SuspendLayout();
            this.metroTabPage9.SuspendLayout();
            this.metroTabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.metroTabPage11.SuspendLayout();
            this.metroTabPage12.SuspendLayout();
            this.metroTabPage13.SuspendLayout();
            this.metroTabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid2)).BeginInit();
            this.metroTabPage6.SuspendLayout();
            this.metroTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid3)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabTabelas
            // 
            this.TabTabelas.Controls.Add(this.metroTabPage1);
            this.TabTabelas.Controls.Add(this.metroTabPage2);
            this.TabTabelas.Controls.Add(this.metroTabPage3);
            this.TabTabelas.Controls.Add(this.metroTabPage4);
            this.TabTabelas.Controls.Add(this.metroTabPage5);
            this.TabTabelas.Controls.Add(this.metroTabPage6);
            this.TabTabelas.Controls.Add(this.metroTabPage8);
            this.TabTabelas.Location = new System.Drawing.Point(23, 91);
            this.TabTabelas.Name = "TabTabelas";
            this.TabTabelas.SelectedIndex = 4;
            this.TabTabelas.Size = new System.Drawing.Size(967, 550);
            this.TabTabelas.TabIndex = 13;
            this.TabTabelas.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.groupBox3);
            this.metroTabPage1.Controls.Add(this.cbCodEmpresa);
            this.metroTabPage1.Controls.Add(this.label4);
            this.metroTabPage1.Controls.Add(this.label3);
            this.metroTabPage1.Controls.Add(this.label2);
            this.metroTabPage1.Controls.Add(this.pbLImagem);
            this.metroTabPage1.Controls.Add(this.metroCheckBox5);
            this.metroTabPage1.Controls.Add(this.metroCheckBox4);
            this.metroTabPage1.Controls.Add(this.metroCheckBox3);
            this.metroTabPage1.Controls.Add(this.metroCheckBox2);
            this.metroTabPage1.Controls.Add(this.metroCheckBox1);
            this.metroTabPage1.Controls.Add(this.txtLPraso2);
            this.metroTabPage1.Controls.Add(this.txtLPraso);
            this.metroTabPage1.Controls.Add(this.metroLabel8);
            this.metroTabPage1.Controls.Add(this.txtLFax);
            this.metroTabPage1.Controls.Add(this.txtLTelefone);
            this.metroTabPage1.Controls.Add(this.txtLCodigoPostal);
            this.metroTabPage1.Controls.Add(this.metroLabel7);
            this.metroTabPage1.Controls.Add(this.metroLabel6);
            this.metroTabPage1.Controls.Add(this.metroLabel5);
            this.metroTabPage1.Controls.Add(this.txtLMarada);
            this.metroTabPage1.Controls.Add(this.cmbLEmpresa);
            this.metroTabPage1.Controls.Add(this.txtLDesignacao);
            this.metroTabPage1.Controls.Add(this.txtLCodigo);
            this.metroTabPage1.Controls.Add(this.metroLabel4);
            this.metroTabPage1.Controls.Add(this.metroLabel3);
            this.metroTabPage1.Controls.Add(this.metroLabel2);
            this.metroTabPage1.Controls.Add(this.metroLabel1);
            this.metroTabPage1.Controls.Add(this.groupBox4);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(959, 508);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Loja";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            this.metroTabPage1.Leave += new System.EventHandler(this.metroTabPage1_Leave);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox3.Controls.Add(this.toolStrip2);
            this.groupBox3.Controls.Add(this.txtLContaNacional);
            this.groupBox3.Controls.Add(this.metroLabel10);
            this.groupBox3.Controls.Add(this.metroLabel11);
            this.groupBox3.Controls.Add(this.txtLContaPa);
            this.groupBox3.Controls.Add(this.txtLContaPadrao);
            this.groupBox3.Controls.Add(this.txtLContaOutros);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(0, 323);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(499, 123);
            this.groupBox3.TabIndex = 33;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "ContasndenVendas";
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btAtualizar});
            this.toolStrip2.Location = new System.Drawing.Point(322, 90);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(66, 25);
            this.toolStrip2.TabIndex = 94;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btAtualizar
            // 
            this.btAtualizar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btAtualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btAtualizar.Name = "btAtualizar";
            this.btAtualizar.Size = new System.Drawing.Size(63, 22);
            this.btAtualizar.Text = "Ver Conta";
            this.btAtualizar.Click += new System.EventHandler(this.btAtualizar_Click);
            // 
            // txtLContaNacional
            // 
            this.txtLContaNacional.Lines = new string[0];
            this.txtLContaNacional.Location = new System.Drawing.Point(122, 32);
            this.txtLContaNacional.MaxLength = 32767;
            this.txtLContaNacional.Name = "txtLContaNacional";
            this.txtLContaNacional.PasswordChar = '\0';
            this.txtLContaNacional.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLContaNacional.SelectedText = "";
            this.txtLContaNacional.Size = new System.Drawing.Size(191, 23);
            this.txtLContaNacional.TabIndex = 29;
            this.txtLContaNacional.UseSelectable = true;
            this.txtLContaNacional.Click += new System.EventHandler(this.txtLContaNacional_Click);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(9, 36);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(109, 19);
            this.metroLabel10.TabIndex = 3;
            this.metroLabel10.Text = "Conta - Nacional";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(9, 65);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(98, 19);
            this.metroLabel11.TabIndex = 4;
            this.metroLabel11.Text = "Conta - Outros";
            // 
            // txtLContaPa
            // 
            this.txtLContaPa.Lines = new string[0];
            this.txtLContaPa.Location = new System.Drawing.Point(122, 90);
            this.txtLContaPa.MaxLength = 32767;
            this.txtLContaPa.Name = "txtLContaPa";
            this.txtLContaPa.PasswordChar = '\0';
            this.txtLContaPa.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLContaPa.SelectedText = "";
            this.txtLContaPa.Size = new System.Drawing.Size(191, 23);
            this.txtLContaPa.TabIndex = 31;
            this.txtLContaPa.UseSelectable = true;
            this.txtLContaPa.Click += new System.EventHandler(this.txtLContaPa_Click);
            // 
            // txtLContaPadrao
            // 
            this.txtLContaPadrao.AutoSize = true;
            this.txtLContaPadrao.Location = new System.Drawing.Point(9, 94);
            this.txtLContaPadrao.Name = "txtLContaPadrao";
            this.txtLContaPadrao.Size = new System.Drawing.Size(101, 19);
            this.txtLContaPadrao.TabIndex = 5;
            this.txtLContaPadrao.Text = "Conta - Padrão";
            // 
            // txtLContaOutros
            // 
            this.txtLContaOutros.Lines = new string[0];
            this.txtLContaOutros.Location = new System.Drawing.Point(122, 61);
            this.txtLContaOutros.MaxLength = 32767;
            this.txtLContaOutros.Name = "txtLContaOutros";
            this.txtLContaOutros.PasswordChar = '\0';
            this.txtLContaOutros.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLContaOutros.SelectedText = "";
            this.txtLContaOutros.Size = new System.Drawing.Size(191, 23);
            this.txtLContaOutros.TabIndex = 30;
            this.txtLContaOutros.UseSelectable = true;
            this.txtLContaOutros.Click += new System.EventHandler(this.txtLContaOutros_Click);
            // 
            // cbCodEmpresa
            // 
            this.cbCodEmpresa.AutoSize = true;
            this.cbCodEmpresa.Location = new System.Drawing.Point(435, 94);
            this.cbCodEmpresa.Name = "cbCodEmpresa";
            this.cbCodEmpresa.Size = new System.Drawing.Size(0, 0);
            this.cbCodEmpresa.TabIndex = 32;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(105, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(105, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(105, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "*";
            // 
            // pbLImagem
            // 
            this.pbLImagem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbLImagem.Location = new System.Drawing.Point(747, 177);
            this.pbLImagem.Name = "pbLImagem";
            this.pbLImagem.Size = new System.Drawing.Size(116, 103);
            this.pbLImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLImagem.TabIndex = 27;
            this.pbLImagem.TabStop = false;
            this.pbLImagem.Click += new System.EventHandler(this.pbLImagem_Click);
            // 
            // metroCheckBox5
            // 
            this.metroCheckBox5.AutoSize = true;
            this.metroCheckBox5.Location = new System.Drawing.Point(444, 256);
            this.metroCheckBox5.Name = "metroCheckBox5";
            this.metroCheckBox5.Size = new System.Drawing.Size(120, 15);
            this.metroCheckBox5.TabIndex = 23;
            this.metroCheckBox5.Text = "Visita Pre-Definida";
            this.metroCheckBox5.UseSelectable = true;
            // 
            // metroCheckBox4
            // 
            this.metroCheckBox4.AutoSize = true;
            this.metroCheckBox4.Location = new System.Drawing.Point(220, 256);
            this.metroCheckBox4.Name = "metroCheckBox4";
            this.metroCheckBox4.Size = new System.Drawing.Size(179, 15);
            this.metroCheckBox4.TabIndex = 22;
            this.metroCheckBox4.Text = "Abertura / Feicho Obrigatorio";
            this.metroCheckBox4.UseSelectable = true;
            // 
            // metroCheckBox3
            // 
            this.metroCheckBox3.AutoSize = true;
            this.metroCheckBox3.Location = new System.Drawing.Point(220, 235);
            this.metroCheckBox3.Name = "metroCheckBox3";
            this.metroCheckBox3.Size = new System.Drawing.Size(93, 15);
            this.metroCheckBox3.TabIndex = 21;
            this.metroCheckBox3.Text = "Emitir Recibo";
            this.metroCheckBox3.UseSelectable = true;
            // 
            // metroCheckBox2
            // 
            this.metroCheckBox2.AutoSize = true;
            this.metroCheckBox2.Location = new System.Drawing.Point(3, 256);
            this.metroCheckBox2.Name = "metroCheckBox2";
            this.metroCheckBox2.Size = new System.Drawing.Size(163, 15);
            this.metroCheckBox2.TabIndex = 20;
            this.metroCheckBox2.Text = "Agente Telecomunicações";
            this.metroCheckBox2.UseSelectable = true;
            // 
            // metroCheckBox1
            // 
            this.metroCheckBox1.AutoSize = true;
            this.metroCheckBox1.Location = new System.Drawing.Point(3, 235);
            this.metroCheckBox1.Name = "metroCheckBox1";
            this.metroCheckBox1.Size = new System.Drawing.Size(113, 15);
            this.metroCheckBox1.TabIndex = 19;
            this.metroCheckBox1.Text = "Visualizar OnLine";
            this.metroCheckBox1.UseSelectable = true;
            // 
            // txtLPraso2
            // 
            this.txtLPraso2.Lines = new string[0];
            this.txtLPraso2.Location = new System.Drawing.Point(206, 154);
            this.txtLPraso2.MaxLength = 32767;
            this.txtLPraso2.Name = "txtLPraso2";
            this.txtLPraso2.PasswordChar = '\0';
            this.txtLPraso2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLPraso2.SelectedText = "";
            this.txtLPraso2.Size = new System.Drawing.Size(223, 23);
            this.txtLPraso2.TabIndex = 18;
            this.txtLPraso2.UseSelectable = true;
            // 
            // txtLPraso
            // 
            this.txtLPraso.Lines = new string[0];
            this.txtLPraso.Location = new System.Drawing.Point(141, 154);
            this.txtLPraso.MaxLength = 32767;
            this.txtLPraso.Name = "txtLPraso";
            this.txtLPraso.PasswordChar = '\0';
            this.txtLPraso.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLPraso.SelectedText = "";
            this.txtLPraso.Size = new System.Drawing.Size(59, 23);
            this.txtLPraso.TabIndex = 17;
            this.txtLPraso.UseSelectable = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(0, 158);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(135, 19);
            this.metroLabel8.TabIndex = 16;
            this.metroLabel8.Text = "Prazo-Reservas (Dias)";
            // 
            // txtLFax
            // 
            this.txtLFax.Lines = new string[0];
            this.txtLFax.Location = new System.Drawing.Point(658, 80);
            this.txtLFax.MaxLength = 32767;
            this.txtLFax.Name = "txtLFax";
            this.txtLFax.PasswordChar = '\0';
            this.txtLFax.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLFax.SelectedText = "";
            this.txtLFax.Size = new System.Drawing.Size(263, 23);
            this.txtLFax.TabIndex = 15;
            this.txtLFax.UseSelectable = true;
            // 
            // txtLTelefone
            // 
            this.txtLTelefone.Lines = new string[0];
            this.txtLTelefone.Location = new System.Drawing.Point(658, 52);
            this.txtLTelefone.MaxLength = 32767;
            this.txtLTelefone.Name = "txtLTelefone";
            this.txtLTelefone.PasswordChar = '\0';
            this.txtLTelefone.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLTelefone.SelectedText = "";
            this.txtLTelefone.Size = new System.Drawing.Size(263, 23);
            this.txtLTelefone.TabIndex = 14;
            this.txtLTelefone.UseSelectable = true;
            // 
            // txtLCodigoPostal
            // 
            this.txtLCodigoPostal.Lines = new string[0];
            this.txtLCodigoPostal.Location = new System.Drawing.Point(658, 22);
            this.txtLCodigoPostal.MaxLength = 32767;
            this.txtLCodigoPostal.Name = "txtLCodigoPostal";
            this.txtLCodigoPostal.PasswordChar = '\0';
            this.txtLCodigoPostal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLCodigoPostal.SelectedText = "";
            this.txtLCodigoPostal.Size = new System.Drawing.Size(263, 23);
            this.txtLCodigoPostal.TabIndex = 13;
            this.txtLCodigoPostal.UseSelectable = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(535, 84);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(29, 19);
            this.metroLabel7.TabIndex = 12;
            this.metroLabel7.Text = "Fax";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(535, 56);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(59, 19);
            this.metroLabel6.TabIndex = 11;
            this.metroLabel6.Text = "Telefone";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(535, 26);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(92, 19);
            this.metroLabel5.TabIndex = 10;
            this.metroLabel5.Text = "Código Postal";
            // 
            // txtLMarada
            // 
            this.txtLMarada.Lines = new string[0];
            this.txtLMarada.Location = new System.Drawing.Point(116, 121);
            this.txtLMarada.MaxLength = 32767;
            this.txtLMarada.Name = "txtLMarada";
            this.txtLMarada.PasswordChar = '\0';
            this.txtLMarada.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLMarada.SelectedText = "";
            this.txtLMarada.Size = new System.Drawing.Size(805, 23);
            this.txtLMarada.TabIndex = 9;
            this.txtLMarada.UseSelectable = true;
            // 
            // cmbLEmpresa
            // 
            this.cmbLEmpresa.FormattingEnabled = true;
            this.cmbLEmpresa.IntegralHeight = false;
            this.cmbLEmpresa.ItemHeight = 23;
            this.cmbLEmpresa.Location = new System.Drawing.Point(116, 84);
            this.cmbLEmpresa.Name = "cmbLEmpresa";
            this.cmbLEmpresa.Size = new System.Drawing.Size(313, 29);
            this.cmbLEmpresa.TabIndex = 8;
            this.cmbLEmpresa.UseSelectable = true;
            this.cmbLEmpresa.SelectedIndexChanged += new System.EventHandler(this.cmbLEmpresa_SelectedIndexChanged);
            this.cmbLEmpresa.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbLEmpresa_MouseClick);
            // 
            // txtLDesignacao
            // 
            this.txtLDesignacao.Lines = new string[0];
            this.txtLDesignacao.Location = new System.Drawing.Point(116, 52);
            this.txtLDesignacao.MaxLength = 32767;
            this.txtLDesignacao.Name = "txtLDesignacao";
            this.txtLDesignacao.PasswordChar = '\0';
            this.txtLDesignacao.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLDesignacao.SelectedText = "";
            this.txtLDesignacao.Size = new System.Drawing.Size(313, 23);
            this.txtLDesignacao.TabIndex = 7;
            this.txtLDesignacao.UseSelectable = true;
            // 
            // txtLCodigo
            // 
            this.txtLCodigo.Lines = new string[0];
            this.txtLCodigo.Location = new System.Drawing.Point(116, 22);
            this.txtLCodigo.MaxLength = 32767;
            this.txtLCodigo.Name = "txtLCodigo";
            this.txtLCodigo.PasswordChar = '\0';
            this.txtLCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLCodigo.SelectedText = "";
            this.txtLCodigo.Size = new System.Drawing.Size(75, 23);
            this.txtLCodigo.TabIndex = 6;
            this.txtLCodigo.UseSelectable = true;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(0, 125);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(56, 19);
            this.metroLabel4.TabIndex = 5;
            this.metroLabel4.Text = "Morada";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(0, 94);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(60, 19);
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Empresa";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(0, 56);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(76, 19);
            this.metroLabel2.TabIndex = 3;
            this.metroLabel2.Text = "Designação";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(3, 26);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(53, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Código";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox4.Controls.Add(this.toolStrip3);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(736, 159);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(135, 160);
            this.groupBox4.TabIndex = 34;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Imagem";
            // 
            // toolStrip3
            // 
            this.toolStrip3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip3.Location = new System.Drawing.Point(29, 124);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(51, 25);
            this.toolStrip3.TabIndex = 95;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(48, 22);
            this.toolStripButton1.Text = "Limpar";
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.toolStrip4);
            this.metroTabPage2.Controls.Add(this.lbLoja);
            this.metroTabPage2.Controls.Add(this.lbArea);
            this.metroTabPage2.Controls.Add(this.label8);
            this.metroTabPage2.Controls.Add(this.label7);
            this.metroTabPage2.Controls.Add(this.label6);
            this.metroTabPage2.Controls.Add(this.label5);
            this.metroTabPage2.Controls.Add(this.cbMEmitirRI);
            this.metroTabPage2.Controls.Add(this.cbMActivo);
            this.metroTabPage2.Controls.Add(this.cbMMercadoriaRemoto);
            this.metroTabPage2.Controls.Add(this.cbMMercadoriaVenda);
            this.metroTabPage2.Controls.Add(this.cmbMAreaOrganica);
            this.metroTabPage2.Controls.Add(this.cmbMDesignacaoLoja);
            this.metroTabPage2.Controls.Add(this.txtMContaCustos);
            this.metroTabPage2.Controls.Add(this.txtMContaVendas);
            this.metroTabPage2.Controls.Add(this.txtMDesignacao);
            this.metroTabPage2.Controls.Add(this.txtMCodigo);
            this.metroTabPage2.Controls.Add(this.metroLabel18);
            this.metroTabPage2.Controls.Add(this.metroLabel17);
            this.metroTabPage2.Controls.Add(this.metroLabel16);
            this.metroTabPage2.Controls.Add(this.metroLabel15);
            this.metroTabPage2.Controls.Add(this.metroLabel14);
            this.metroTabPage2.Controls.Add(this.metroLabel13);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(959, 508);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Mercadoria";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            this.metroTabPage2.Leave += new System.EventHandler(this.metroTabPage2_Leave);
            // 
            // toolStrip4
            // 
            this.toolStrip4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip4.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2});
            this.toolStrip4.Location = new System.Drawing.Point(543, 221);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.Size = new System.Drawing.Size(66, 25);
            this.toolStrip4.TabIndex = 94;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(63, 22);
            this.toolStripButton2.Text = "Ver Conta";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // lbLoja
            // 
            this.lbLoja.AutoSize = true;
            this.lbLoja.Location = new System.Drawing.Point(675, 127);
            this.lbLoja.Name = "lbLoja";
            this.lbLoja.Size = new System.Drawing.Size(0, 0);
            this.lbLoja.TabIndex = 24;
            // 
            // lbArea
            // 
            this.lbArea.AutoSize = true;
            this.lbArea.Location = new System.Drawing.Point(675, 160);
            this.lbArea.Name = "lbArea";
            this.lbArea.Size = new System.Drawing.Size(0, 0);
            this.lbArea.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(316, 150);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(316, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(316, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(316, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "*";
            // 
            // cbMEmitirRI
            // 
            this.cbMEmitirRI.AutoSize = true;
            this.cbMEmitirRI.Location = new System.Drawing.Point(156, 376);
            this.cbMEmitirRI.Name = "cbMEmitirRI";
            this.cbMEmitirRI.Size = new System.Drawing.Size(67, 15);
            this.cbMEmitirRI.TabIndex = 18;
            this.cbMEmitirRI.Text = "Emitir RI";
            this.cbMEmitirRI.UseSelectable = true;
            // 
            // cbMActivo
            // 
            this.cbMActivo.AutoSize = true;
            this.cbMActivo.Location = new System.Drawing.Point(156, 345);
            this.cbMActivo.Name = "cbMActivo";
            this.cbMActivo.Size = new System.Drawing.Size(57, 15);
            this.cbMActivo.TabIndex = 17;
            this.cbMActivo.Text = "Activo";
            this.cbMActivo.UseSelectable = true;
            // 
            // cbMMercadoriaRemoto
            // 
            this.cbMMercadoriaRemoto.AutoSize = true;
            this.cbMMercadoriaRemoto.Location = new System.Drawing.Point(156, 315);
            this.cbMMercadoriaRemoto.Name = "cbMMercadoriaRemoto";
            this.cbMMercadoriaRemoto.Size = new System.Drawing.Size(133, 15);
            this.cbMMercadoriaRemoto.TabIndex = 16;
            this.cbMMercadoriaRemoto.Text = "Mercadoria Remoto?";
            this.cbMMercadoriaRemoto.UseSelectable = true;
            // 
            // cbMMercadoriaVenda
            // 
            this.cbMMercadoriaVenda.AutoSize = true;
            this.cbMMercadoriaVenda.Location = new System.Drawing.Point(156, 285);
            this.cbMMercadoriaVenda.Name = "cbMMercadoriaVenda";
            this.cbMMercadoriaVenda.Size = new System.Drawing.Size(140, 15);
            this.cbMMercadoriaVenda.TabIndex = 15;
            this.cbMMercadoriaVenda.Text = "Mercadoria de Venda?";
            this.cbMMercadoriaVenda.UseSelectable = true;
            // 
            // cmbMAreaOrganica
            // 
            this.cmbMAreaOrganica.FormattingEnabled = true;
            this.cmbMAreaOrganica.IntegralHeight = false;
            this.cmbMAreaOrganica.ItemHeight = 23;
            this.cmbMAreaOrganica.Location = new System.Drawing.Point(333, 150);
            this.cmbMAreaOrganica.Name = "cmbMAreaOrganica";
            this.cmbMAreaOrganica.Size = new System.Drawing.Size(336, 29);
            this.cmbMAreaOrganica.TabIndex = 13;
            this.cmbMAreaOrganica.UseSelectable = true;
            this.cmbMAreaOrganica.SelectedIndexChanged += new System.EventHandler(this.cmbMAreaOrganica_SelectedIndexChanged);
            this.cmbMAreaOrganica.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbMAreaOrganica_MouseClick);
            // 
            // cmbMDesignacaoLoja
            // 
            this.cmbMDesignacaoLoja.FormattingEnabled = true;
            this.cmbMDesignacaoLoja.IntegralHeight = false;
            this.cmbMDesignacaoLoja.ItemHeight = 23;
            this.cmbMDesignacaoLoja.Location = new System.Drawing.Point(333, 117);
            this.cmbMDesignacaoLoja.Name = "cmbMDesignacaoLoja";
            this.cmbMDesignacaoLoja.Size = new System.Drawing.Size(336, 29);
            this.cmbMDesignacaoLoja.TabIndex = 12;
            this.cmbMDesignacaoLoja.UseSelectable = true;
            this.cmbMDesignacaoLoja.SelectedIndexChanged += new System.EventHandler(this.cmbMDesignacaoLoja_SelectedIndexChanged);
            this.cmbMDesignacaoLoja.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbMDesignacaoLoja_MouseClick);
            // 
            // txtMContaCustos
            // 
            this.txtMContaCustos.Lines = new string[0];
            this.txtMContaCustos.Location = new System.Drawing.Point(333, 223);
            this.txtMContaCustos.MaxLength = 32767;
            this.txtMContaCustos.Name = "txtMContaCustos";
            this.txtMContaCustos.PasswordChar = '\0';
            this.txtMContaCustos.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtMContaCustos.SelectedText = "";
            this.txtMContaCustos.Size = new System.Drawing.Size(207, 23);
            this.txtMContaCustos.TabIndex = 11;
            this.txtMContaCustos.UseSelectable = true;
            this.txtMContaCustos.Click += new System.EventHandler(this.txtMContaCustos_Click);
            // 
            // txtMContaVendas
            // 
            this.txtMContaVendas.Lines = new string[0];
            this.txtMContaVendas.Location = new System.Drawing.Point(333, 190);
            this.txtMContaVendas.MaxLength = 32767;
            this.txtMContaVendas.Name = "txtMContaVendas";
            this.txtMContaVendas.PasswordChar = '\0';
            this.txtMContaVendas.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtMContaVendas.SelectedText = "";
            this.txtMContaVendas.Size = new System.Drawing.Size(207, 23);
            this.txtMContaVendas.TabIndex = 10;
            this.txtMContaVendas.UseSelectable = true;
            this.txtMContaVendas.Click += new System.EventHandler(this.txtMContaVendas_Click);
            // 
            // txtMDesignacao
            // 
            this.txtMDesignacao.Lines = new string[0];
            this.txtMDesignacao.Location = new System.Drawing.Point(333, 88);
            this.txtMDesignacao.MaxLength = 32767;
            this.txtMDesignacao.Name = "txtMDesignacao";
            this.txtMDesignacao.PasswordChar = '\0';
            this.txtMDesignacao.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtMDesignacao.SelectedText = "";
            this.txtMDesignacao.Size = new System.Drawing.Size(336, 23);
            this.txtMDesignacao.TabIndex = 9;
            this.txtMDesignacao.UseSelectable = true;
            // 
            // txtMCodigo
            // 
            this.txtMCodigo.Lines = new string[0];
            this.txtMCodigo.Location = new System.Drawing.Point(333, 52);
            this.txtMCodigo.MaxLength = 32767;
            this.txtMCodigo.Name = "txtMCodigo";
            this.txtMCodigo.PasswordChar = '\0';
            this.txtMCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtMCodigo.SelectedText = "";
            this.txtMCodigo.Size = new System.Drawing.Size(97, 23);
            this.txtMCodigo.TabIndex = 8;
            this.txtMCodigo.UseSelectable = true;
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(156, 227);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(96, 19);
            this.metroLabel18.TabIndex = 7;
            this.metroLabel18.Text = "Conta - Custos";
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(156, 194);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(100, 19);
            this.metroLabel17.TabIndex = 6;
            this.metroLabel17.Text = "Conta - Vendas";
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(156, 160);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(95, 19);
            this.metroLabel16.TabIndex = 5;
            this.metroLabel16.Text = "Área Orgânica";
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(156, 127);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(123, 19);
            this.metroLabel15.TabIndex = 4;
            this.metroLabel15.Text = "Designação da Loja";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(156, 92);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(76, 19);
            this.metroLabel14.TabIndex = 3;
            this.metroLabel14.Text = "Designação";
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(156, 56);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(53, 19);
            this.metroLabel13.TabIndex = 2;
            this.metroLabel13.Text = "Código";
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.cbFContabilizacaoCusto);
            this.metroTabPage3.Controls.Add(this.cbFContabilizacaoStock);
            this.metroTabPage3.Controls.Add(this.cbFContabilizacaoVenda);
            this.metroTabPage3.Controls.Add(this.label11);
            this.metroTabPage3.Controls.Add(this.label10);
            this.metroTabPage3.Controls.Add(this.label9);
            this.metroTabPage3.Controls.Add(this.txtFPrazo);
            this.metroTabPage3.Controls.Add(this.txtFDesconto);
            this.metroTabPage3.Controls.Add(this.txtFDesignacao);
            this.metroTabPage3.Controls.Add(this.txtFCodigo);
            this.metroTabPage3.Controls.Add(this.metroLabel21);
            this.metroTabPage3.Controls.Add(this.metroLabel20);
            this.metroTabPage3.Controls.Add(this.metroLabel19);
            this.metroTabPage3.Controls.Add(this.metroLabel12);
            this.metroTabPage3.Controls.Add(this.pbFImagem);
            this.metroTabPage3.Controls.Add(this.groupBox5);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(959, 508);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Família";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            this.metroTabPage3.Leave += new System.EventHandler(this.metroTabPage3_Leave);
            // 
            // cbFContabilizacaoCusto
            // 
            this.cbFContabilizacaoCusto.AutoSize = true;
            this.cbFContabilizacaoCusto.Location = new System.Drawing.Point(206, 241);
            this.cbFContabilizacaoCusto.Name = "cbFContabilizacaoCusto";
            this.cbFContabilizacaoCusto.Size = new System.Drawing.Size(151, 15);
            this.cbFContabilizacaoCusto.TabIndex = 20;
            this.cbFContabilizacaoCusto.Text = "Contabilização de Custo";
            this.cbFContabilizacaoCusto.UseSelectable = true;
            // 
            // cbFContabilizacaoStock
            // 
            this.cbFContabilizacaoStock.AutoSize = true;
            this.cbFContabilizacaoStock.Location = new System.Drawing.Point(206, 210);
            this.cbFContabilizacaoStock.Name = "cbFContabilizacaoStock";
            this.cbFContabilizacaoStock.Size = new System.Drawing.Size(149, 15);
            this.cbFContabilizacaoStock.TabIndex = 19;
            this.cbFContabilizacaoStock.Text = "Contabilização de Stock";
            this.cbFContabilizacaoStock.UseSelectable = true;
            // 
            // cbFContabilizacaoVenda
            // 
            this.cbFContabilizacaoVenda.AutoSize = true;
            this.cbFContabilizacaoVenda.Location = new System.Drawing.Point(208, 174);
            this.cbFContabilizacaoVenda.Name = "cbFContabilizacaoVenda";
            this.cbFContabilizacaoVenda.Size = new System.Drawing.Size(153, 15);
            this.cbFContabilizacaoVenda.TabIndex = 18;
            this.cbFContabilizacaoVenda.Text = "Contabilização de Venda";
            this.cbFContabilizacaoVenda.UseSelectable = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(314, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(314, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(11, 13);
            this.label10.TabIndex = 12;
            this.label10.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(412, 125);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "%";
            // 
            // txtFPrazo
            // 
            this.txtFPrazo.Lines = new string[0];
            this.txtFPrazo.Location = new System.Drawing.Point(532, 115);
            this.txtFPrazo.MaxLength = 32767;
            this.txtFPrazo.Name = "txtFPrazo";
            this.txtFPrazo.PasswordChar = '\0';
            this.txtFPrazo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFPrazo.SelectedText = "";
            this.txtFPrazo.Size = new System.Drawing.Size(113, 23);
            this.txtFPrazo.TabIndex = 10;
            this.txtFPrazo.UseSelectable = true;
            // 
            // txtFDesconto
            // 
            this.txtFDesconto.Lines = new string[0];
            this.txtFDesconto.Location = new System.Drawing.Point(331, 115);
            this.txtFDesconto.MaxLength = 32767;
            this.txtFDesconto.Name = "txtFDesconto";
            this.txtFDesconto.PasswordChar = '\0';
            this.txtFDesconto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFDesconto.SelectedText = "";
            this.txtFDesconto.Size = new System.Drawing.Size(75, 23);
            this.txtFDesconto.TabIndex = 9;
            this.txtFDesconto.UseSelectable = true;
            // 
            // txtFDesignacao
            // 
            this.txtFDesignacao.Lines = new string[0];
            this.txtFDesignacao.Location = new System.Drawing.Point(331, 78);
            this.txtFDesignacao.MaxLength = 32767;
            this.txtFDesignacao.Name = "txtFDesignacao";
            this.txtFDesignacao.PasswordChar = '\0';
            this.txtFDesignacao.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFDesignacao.SelectedText = "";
            this.txtFDesignacao.Size = new System.Drawing.Size(313, 23);
            this.txtFDesignacao.TabIndex = 8;
            this.txtFDesignacao.UseSelectable = true;
            // 
            // txtFCodigo
            // 
            this.txtFCodigo.Lines = new string[0];
            this.txtFCodigo.Location = new System.Drawing.Point(331, 38);
            this.txtFCodigo.MaxLength = 32767;
            this.txtFCodigo.Name = "txtFCodigo";
            this.txtFCodigo.PasswordChar = '\0';
            this.txtFCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFCodigo.SelectedText = "";
            this.txtFCodigo.Size = new System.Drawing.Size(75, 23);
            this.txtFCodigo.TabIndex = 7;
            this.txtFCodigo.UseSelectable = true;
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.Location = new System.Drawing.Point(483, 119);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(43, 19);
            this.metroLabel21.TabIndex = 5;
            this.metroLabel21.Text = "Prazo";
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.Location = new System.Drawing.Point(208, 119);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(63, 19);
            this.metroLabel20.TabIndex = 4;
            this.metroLabel20.Text = "Desconto";
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(208, 82);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(76, 19);
            this.metroLabel19.TabIndex = 3;
            this.metroLabel19.Text = "Designação";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(208, 42);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(53, 19);
            this.metroLabel12.TabIndex = 2;
            this.metroLabel12.Text = "Código";
            // 
            // pbFImagem
            // 
            this.pbFImagem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbFImagem.Location = new System.Drawing.Point(514, 170);
            this.pbFImagem.Name = "pbFImagem";
            this.pbFImagem.Size = new System.Drawing.Size(124, 118);
            this.pbFImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbFImagem.TabIndex = 14;
            this.pbFImagem.TabStop = false;
            this.pbFImagem.Click += new System.EventHandler(this.pbFImagem_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(507, 154);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(136, 143);
            this.groupBox5.TabIndex = 21;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Imagem";
            // 
            // metroTabPage4
            // 
            this.metroTabPage4.Controls.Add(this.cmbSFamilia2);
            this.metroTabPage4.Controls.Add(this.lbSFamilia);
            this.metroTabPage4.Controls.Add(this.label14);
            this.metroTabPage4.Controls.Add(this.label13);
            this.metroTabPage4.Controls.Add(this.label12);
            this.metroTabPage4.Controls.Add(this.txtSDesignacao);
            this.metroTabPage4.Controls.Add(this.txtSCodigo);
            this.metroTabPage4.Controls.Add(this.metroLabel25);
            this.metroTabPage4.Controls.Add(this.metroLabel24);
            this.metroTabPage4.Controls.Add(this.metroLabel23);
            this.metroTabPage4.HorizontalScrollbarBarColor = true;
            this.metroTabPage4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.HorizontalScrollbarSize = 10;
            this.metroTabPage4.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage4.Name = "metroTabPage4";
            this.metroTabPage4.Size = new System.Drawing.Size(959, 508);
            this.metroTabPage4.TabIndex = 3;
            this.metroTabPage4.Text = "SubFamília";
            this.metroTabPage4.VerticalScrollbarBarColor = true;
            this.metroTabPage4.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.VerticalScrollbarSize = 10;
            this.metroTabPage4.Leave += new System.EventHandler(this.metroTabPage4_Leave);
            // 
            // cmbSFamilia2
            // 
            this.cmbSFamilia2.FormattingEnabled = true;
            this.cmbSFamilia2.IntegralHeight = false;
            this.cmbSFamilia2.ItemHeight = 23;
            this.cmbSFamilia2.Location = new System.Drawing.Point(302, 71);
            this.cmbSFamilia2.Name = "cmbSFamilia2";
            this.cmbSFamilia2.Size = new System.Drawing.Size(316, 29);
            this.cmbSFamilia2.TabIndex = 117;
            this.cmbSFamilia2.Tag = "";
            this.cmbSFamilia2.UseSelectable = true;
            this.cmbSFamilia2.DropDown += new System.EventHandler(this.cmbSFamilia2_DropDown);
            this.cmbSFamilia2.SelectedIndexChanged += new System.EventHandler(this.cmbSFamilia2_SelectedIndexChanged);
            // 
            // lbSFamilia
            // 
            this.lbSFamilia.AutoSize = true;
            this.lbSFamilia.Location = new System.Drawing.Point(628, 81);
            this.lbSFamilia.Name = "lbSFamilia";
            this.lbSFamilia.Size = new System.Drawing.Size(0, 0);
            this.lbSFamilia.TabIndex = 44;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(285, 170);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(11, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(285, 124);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(11, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "*";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(285, 71);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(11, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "*";
            // 
            // txtSDesignacao
            // 
            this.txtSDesignacao.Lines = new string[0];
            this.txtSDesignacao.Location = new System.Drawing.Point(302, 170);
            this.txtSDesignacao.MaxLength = 32767;
            this.txtSDesignacao.Name = "txtSDesignacao";
            this.txtSDesignacao.PasswordChar = '\0';
            this.txtSDesignacao.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSDesignacao.SelectedText = "";
            this.txtSDesignacao.Size = new System.Drawing.Size(320, 23);
            this.txtSDesignacao.TabIndex = 7;
            this.txtSDesignacao.UseSelectable = true;
            // 
            // txtSCodigo
            // 
            this.txtSCodigo.Lines = new string[0];
            this.txtSCodigo.Location = new System.Drawing.Point(302, 124);
            this.txtSCodigo.MaxLength = 32767;
            this.txtSCodigo.Name = "txtSCodigo";
            this.txtSCodigo.PasswordChar = '\0';
            this.txtSCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSCodigo.SelectedText = "";
            this.txtSCodigo.Size = new System.Drawing.Size(75, 23);
            this.txtSCodigo.TabIndex = 6;
            this.txtSCodigo.UseSelectable = true;
            // 
            // metroLabel25
            // 
            this.metroLabel25.AutoSize = true;
            this.metroLabel25.Location = new System.Drawing.Point(190, 174);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(76, 19);
            this.metroLabel25.TabIndex = 4;
            this.metroLabel25.Text = "Designação";
            // 
            // metroLabel24
            // 
            this.metroLabel24.AutoSize = true;
            this.metroLabel24.Location = new System.Drawing.Point(190, 128);
            this.metroLabel24.Name = "metroLabel24";
            this.metroLabel24.Size = new System.Drawing.Size(53, 19);
            this.metroLabel24.TabIndex = 3;
            this.metroLabel24.Text = "Código";
            // 
            // metroLabel23
            // 
            this.metroLabel23.AutoSize = true;
            this.metroLabel23.Location = new System.Drawing.Point(190, 81);
            this.metroLabel23.Name = "metroLabel23";
            this.metroLabel23.Size = new System.Drawing.Size(51, 19);
            this.metroLabel23.TabIndex = 2;
            this.metroLabel23.Text = "Família";
            // 
            // metroTabPage5
            // 
            this.metroTabPage5.Controls.Add(this.lbAGrupo);
            this.metroTabPage5.Controls.Add(this.cmbADesigEmbalagem);
            this.metroTabPage5.Controls.Add(this.cmbAUnidade);
            this.metroTabPage5.Controls.Add(this.cmbAGrupoWeb1);
            this.metroTabPage5.Controls.Add(this.cmbASubFamilia1);
            this.metroTabPage5.Controls.Add(this.cmbAFamilia1);
            this.metroTabPage5.Controls.Add(this.cmbAMercadoria1);
            this.metroTabPage5.Controls.Add(this.cmbALoja1);
            this.metroTabPage5.Controls.Add(this.groupBox6);
            this.metroTabPage5.Controls.Add(this.metroLabel72);
            this.metroTabPage5.Controls.Add(this.txtAPercetagemQeuebra);
            this.metroTabPage5.Controls.Add(this.metroLabel71);
            this.metroTabPage5.Controls.Add(this.groupBox2);
            this.metroTabPage5.Controls.Add(this.groupBox1);
            this.metroTabPage5.Controls.Add(this.lbASubFam);
            this.metroTabPage5.Controls.Add(this.lbAFam);
            this.metroTabPage5.Controls.Add(this.lbAmerc);
            this.metroTabPage5.Controls.Add(this.lbALoj);
            this.metroTabPage5.Controls.Add(this.lbASfamilia);
            this.metroTabPage5.Controls.Add(this.lbAFamilia);
            this.metroTabPage5.Controls.Add(this.lbAMercadoria);
            this.metroTabPage5.Controls.Add(this.lbALoja);
            this.metroTabPage5.Controls.Add(this.cbABookingHotel);
            this.metroTabPage5.Controls.Add(this.cbATermoGarantia);
            this.metroTabPage5.Controls.Add(this.cbAMostraImagem);
            this.metroTabPage5.Controls.Add(this.cbAPromocao);
            this.metroTabPage5.Controls.Add(this.cbAPermiteOBS);
            this.metroTabPage5.Controls.Add(this.cbAActivo);
            this.metroTabPage5.Controls.Add(this.cbARegistoEquipamento);
            this.metroTabPage5.Controls.Add(this.cbAContabilizarStock);
            this.metroTabPage5.Controls.Add(this.cbAPermiteAlterar);
            this.metroTabPage5.Controls.Add(this.metroTabControl2);
            this.metroTabPage5.Controls.Add(this.txtANorme);
            this.metroTabPage5.Controls.Add(this.txtALocal);
            this.metroTabPage5.Controls.Add(this.txtQdtEmbalagem);
            this.metroTabPage5.Controls.Add(this.metroLabel38);
            this.metroTabPage5.Controls.Add(this.metroLabel37);
            this.metroTabPage5.Controls.Add(this.metroLabel36);
            this.metroTabPage5.Controls.Add(this.metroLabel35);
            this.metroTabPage5.Controls.Add(this.metroLabel34);
            this.metroTabPage5.Controls.Add(this.metroLabel33);
            this.metroTabPage5.Controls.Add(this.metroLabel32);
            this.metroTabPage5.Controls.Add(this.txtADesignacapSuplementar);
            this.metroTabPage5.Controls.Add(this.txtADesignacaoII);
            this.metroTabPage5.Controls.Add(this.txtADesignacao);
            this.metroTabPage5.Controls.Add(this.txtAReferencia);
            this.metroTabPage5.Controls.Add(this.metroLabel31);
            this.metroTabPage5.Controls.Add(this.metroLabel30);
            this.metroTabPage5.Controls.Add(this.metroLabel29);
            this.metroTabPage5.Controls.Add(this.metroLabel28);
            this.metroTabPage5.Controls.Add(this.metroLabel27);
            this.metroTabPage5.Controls.Add(this.metroLabel26);
            this.metroTabPage5.Controls.Add(this.Loja);
            this.metroTabPage5.HorizontalScrollbarBarColor = true;
            this.metroTabPage5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.HorizontalScrollbarSize = 10;
            this.metroTabPage5.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage5.Name = "metroTabPage5";
            this.metroTabPage5.Size = new System.Drawing.Size(959, 508);
            this.metroTabPage5.TabIndex = 4;
            this.metroTabPage5.Text = "Artigos";
            this.metroTabPage5.VerticalScrollbarBarColor = true;
            this.metroTabPage5.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.VerticalScrollbarSize = 10;
            this.metroTabPage5.Leave += new System.EventHandler(this.metroTabPage5_Leave);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox6.Controls.Add(this.pictureBox1);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(255, 268);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(139, 134);
            this.groupBox6.TabIndex = 62;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Imagem";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(9, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(122, 106);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // metroLabel72
            // 
            this.metroLabel72.AutoSize = true;
            this.metroLabel72.Location = new System.Drawing.Point(243, 429);
            this.metroLabel72.Name = "metroLabel72";
            this.metroLabel72.Size = new System.Drawing.Size(20, 19);
            this.metroLabel72.TabIndex = 61;
            this.metroLabel72.Text = "%";
            // 
            // txtAPercetagemQeuebra
            // 
            this.txtAPercetagemQeuebra.Lines = new string[] {
        "0"};
            this.txtAPercetagemQeuebra.Location = new System.Drawing.Point(162, 426);
            this.txtAPercetagemQeuebra.MaxLength = 32767;
            this.txtAPercetagemQeuebra.Name = "txtAPercetagemQeuebra";
            this.txtAPercetagemQeuebra.PasswordChar = '\0';
            this.txtAPercetagemQeuebra.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAPercetagemQeuebra.SelectedText = "";
            this.txtAPercetagemQeuebra.Size = new System.Drawing.Size(75, 23);
            this.txtAPercetagemQeuebra.TabIndex = 60;
            this.txtAPercetagemQeuebra.Text = "0";
            this.txtAPercetagemQeuebra.UseSelectable = true;
            // 
            // metroLabel71
            // 
            this.metroLabel71.AutoSize = true;
            this.metroLabel71.Location = new System.Drawing.Point(-4, 429);
            this.metroLabel71.Name = "metroLabel71";
            this.metroLabel71.Size = new System.Drawing.Size(148, 19);
            this.metroLabel71.TabIndex = 59;
            this.metroLabel71.Text = "Percetagem de Quebra";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Controls.Add(this.rbAImposto10);
            this.groupBox2.Controls.Add(this.rbAInsento);
            this.groupBox2.Controls.Add(this.rbAImposto5);
            this.groupBox2.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.groupBox2.Location = new System.Drawing.Point(510, 357);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(188, 95);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Imposto de Consumo";
            // 
            // rbAImposto10
            // 
            this.rbAImposto10.AutoSize = true;
            this.rbAImposto10.Location = new System.Drawing.Point(45, 64);
            this.rbAImposto10.Name = "rbAImposto10";
            this.rbAImposto10.Size = new System.Drawing.Size(108, 15);
            this.rbAImposto10.TabIndex = 3;
            this.rbAImposto10.Text = "Imposto de 10%";
            this.rbAImposto10.UseSelectable = true;
            // 
            // rbAInsento
            // 
            this.rbAInsento.AutoSize = true;
            this.rbAInsento.Location = new System.Drawing.Point(6, 22);
            this.rbAInsento.Name = "rbAInsento";
            this.rbAInsento.Size = new System.Drawing.Size(62, 15);
            this.rbAInsento.TabIndex = 2;
            this.rbAInsento.Text = "Insento";
            this.rbAInsento.UseSelectable = true;
            // 
            // rbAImposto5
            // 
            this.rbAImposto5.AutoSize = true;
            this.rbAImposto5.Location = new System.Drawing.Point(23, 43);
            this.rbAImposto5.Name = "rbAImposto5";
            this.rbAImposto5.Size = new System.Drawing.Size(102, 15);
            this.rbAImposto5.TabIndex = 1;
            this.rbAImposto5.Text = "Imposto de 5%";
            this.rbAImposto5.UseSelectable = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.txtADataExpiracao);
            this.groupBox1.Controls.Add(this.txtADataFabrico);
            this.groupBox1.Controls.Add(this.metroLabel70);
            this.groupBox1.Controls.Add(this.metroLabel69);
            this.groupBox1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.groupBox1.Location = new System.Drawing.Point(752, 400);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(187, 91);
            this.groupBox1.TabIndex = 57;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data (Opcional)";
            // 
            // txtADataExpiracao
            // 
            this.txtADataExpiracao.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtADataExpiracao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtADataExpiracao.Location = new System.Drawing.Point(85, 59);
            this.txtADataExpiracao.Mask = "00/00/0000";
            this.txtADataExpiracao.Name = "txtADataExpiracao";
            this.txtADataExpiracao.Size = new System.Drawing.Size(84, 20);
            this.txtADataExpiracao.TabIndex = 64;
            this.txtADataExpiracao.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtADataExpiracao.ValidatingType = typeof(System.DateTime);
            this.txtADataExpiracao.Leave += new System.EventHandler(this.txtADataExpiracao_Leave);
            // 
            // txtADataFabrico
            // 
            this.txtADataFabrico.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtADataFabrico.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtADataFabrico.Location = new System.Drawing.Point(85, 34);
            this.txtADataFabrico.Mask = "00/00/0000";
            this.txtADataFabrico.Name = "txtADataFabrico";
            this.txtADataFabrico.Size = new System.Drawing.Size(84, 20);
            this.txtADataFabrico.TabIndex = 63;
            this.txtADataFabrico.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtADataFabrico.ValidatingType = typeof(System.DateTime);
            this.txtADataFabrico.Leave += new System.EventHandler(this.txtADataFabrico_Leave);
            // 
            // metroLabel70
            // 
            this.metroLabel70.AutoSize = true;
            this.metroLabel70.Location = new System.Drawing.Point(6, 60);
            this.metroLabel70.Name = "metroLabel70";
            this.metroLabel70.Size = new System.Drawing.Size(66, 19);
            this.metroLabel70.TabIndex = 1;
            this.metroLabel70.Text = "Expiração";
            // 
            // metroLabel69
            // 
            this.metroLabel69.AutoSize = true;
            this.metroLabel69.Location = new System.Drawing.Point(6, 35);
            this.metroLabel69.Name = "metroLabel69";
            this.metroLabel69.Size = new System.Drawing.Size(53, 19);
            this.metroLabel69.TabIndex = 0;
            this.metroLabel69.Text = "Fabrico";
            // 
            // lbASubFam
            // 
            this.lbASubFam.AutoSize = true;
            this.lbASubFam.Location = new System.Drawing.Point(400, 147);
            this.lbASubFam.Name = "lbASubFam";
            this.lbASubFam.Size = new System.Drawing.Size(0, 0);
            this.lbASubFam.TabIndex = 56;
            // 
            // lbAFam
            // 
            this.lbAFam.AutoSize = true;
            this.lbAFam.Location = new System.Drawing.Point(400, 112);
            this.lbAFam.Name = "lbAFam";
            this.lbAFam.Size = new System.Drawing.Size(0, 0);
            this.lbAFam.TabIndex = 55;
            // 
            // lbAmerc
            // 
            this.lbAmerc.AutoSize = true;
            this.lbAmerc.Location = new System.Drawing.Point(400, 47);
            this.lbAmerc.Name = "lbAmerc";
            this.lbAmerc.Size = new System.Drawing.Size(0, 0);
            this.lbAmerc.TabIndex = 54;
            // 
            // lbALoj
            // 
            this.lbALoj.AutoSize = true;
            this.lbALoj.Location = new System.Drawing.Point(400, 15);
            this.lbALoj.Name = "lbALoj";
            this.lbALoj.Size = new System.Drawing.Size(0, 0);
            this.lbALoj.TabIndex = 53;
            // 
            // lbASfamilia
            // 
            this.lbASfamilia.AutoSize = true;
            this.lbASfamilia.Location = new System.Drawing.Point(407, 147);
            this.lbASfamilia.Name = "lbASfamilia";
            this.lbASfamilia.Size = new System.Drawing.Size(0, 0);
            this.lbASfamilia.TabIndex = 52;
            // 
            // lbAFamilia
            // 
            this.lbAFamilia.AutoSize = true;
            this.lbAFamilia.Location = new System.Drawing.Point(407, 112);
            this.lbAFamilia.Name = "lbAFamilia";
            this.lbAFamilia.Size = new System.Drawing.Size(0, 0);
            this.lbAFamilia.TabIndex = 51;
            // 
            // lbAMercadoria
            // 
            this.lbAMercadoria.AutoSize = true;
            this.lbAMercadoria.Location = new System.Drawing.Point(407, 50);
            this.lbAMercadoria.Name = "lbAMercadoria";
            this.lbAMercadoria.Size = new System.Drawing.Size(0, 0);
            this.lbAMercadoria.TabIndex = 50;
            // 
            // lbALoja
            // 
            this.lbALoja.AutoSize = true;
            this.lbALoja.Location = new System.Drawing.Point(400, 16);
            this.lbALoja.Name = "lbALoja";
            this.lbALoja.Size = new System.Drawing.Size(0, 0);
            this.lbALoja.TabIndex = 49;
            // 
            // cbABookingHotel
            // 
            this.cbABookingHotel.AutoSize = true;
            this.cbABookingHotel.Location = new System.Drawing.Point(510, 475);
            this.cbABookingHotel.Name = "cbABookingHotel";
            this.cbABookingHotel.Size = new System.Drawing.Size(166, 15);
            this.cbABookingHotel.TabIndex = 41;
            this.cbABookingHotel.Text = "Booking no Hotel (Registo)";
            this.cbABookingHotel.UseSelectable = true;
            // 
            // cbATermoGarantia
            // 
            this.cbATermoGarantia.AutoSize = true;
            this.cbATermoGarantia.Location = new System.Drawing.Point(286, 434);
            this.cbATermoGarantia.Name = "cbATermoGarantia";
            this.cbATermoGarantia.Size = new System.Drawing.Size(121, 15);
            this.cbATermoGarantia.TabIndex = 40;
            this.cbATermoGarantia.Text = "Termo de Garantia";
            this.cbATermoGarantia.UseSelectable = true;
            // 
            // cbAMostraImagem
            // 
            this.cbAMostraImagem.AutoSize = true;
            this.cbAMostraImagem.Location = new System.Drawing.Point(0, 454);
            this.cbAMostraImagem.Name = "cbAMostraImagem";
            this.cbAMostraImagem.Size = new System.Drawing.Size(154, 15);
            this.cbAMostraImagem.TabIndex = 39;
            this.cbAMostraImagem.Text = "Mostrar Imagem na Web";
            this.cbAMostraImagem.UseSelectable = true;
            // 
            // cbAPromocao
            // 
            this.cbAPromocao.AutoSize = true;
            this.cbAPromocao.Location = new System.Drawing.Point(388, 475);
            this.cbAPromocao.Name = "cbAPromocao";
            this.cbAPromocao.Size = new System.Drawing.Size(78, 15);
            this.cbAPromocao.TabIndex = 38;
            this.cbAPromocao.Text = "Promoção";
            this.cbAPromocao.UseSelectable = true;
            // 
            // cbAPermiteOBS
            // 
            this.cbAPermiteOBS.AutoSize = true;
            this.cbAPermiteOBS.Location = new System.Drawing.Point(388, 454);
            this.cbAPermiteOBS.Name = "cbAPermiteOBS";
            this.cbAPermiteOBS.Size = new System.Drawing.Size(90, 15);
            this.cbAPermiteOBS.TabIndex = 37;
            this.cbAPermiteOBS.Text = "Permitir OBS";
            this.cbAPermiteOBS.UseSelectable = true;
            // 
            // cbAActivo
            // 
            this.cbAActivo.AutoSize = true;
            this.cbAActivo.Location = new System.Drawing.Point(432, 433);
            this.cbAActivo.Name = "cbAActivo";
            this.cbAActivo.Size = new System.Drawing.Size(57, 15);
            this.cbAActivo.TabIndex = 36;
            this.cbAActivo.Text = "Activo";
            this.cbAActivo.UseSelectable = true;
            // 
            // cbARegistoEquipamento
            // 
            this.cbARegistoEquipamento.AutoSize = true;
            this.cbARegistoEquipamento.Location = new System.Drawing.Point(191, 475);
            this.cbARegistoEquipamento.Name = "cbARegistoEquipamento";
            this.cbARegistoEquipamento.Size = new System.Drawing.Size(157, 15);
            this.cbARegistoEquipamento.TabIndex = 35;
            this.cbARegistoEquipamento.Text = "Registo de Equipamentos";
            this.cbARegistoEquipamento.UseSelectable = true;
            this.cbARegistoEquipamento.CheckedChanged += new System.EventHandler(this.cbARegistoEquipamento_CheckedChanged);
            // 
            // cbAContabilizarStock
            // 
            this.cbAContabilizarStock.AutoSize = true;
            this.cbAContabilizarStock.Location = new System.Drawing.Point(191, 455);
            this.cbAContabilizarStock.Name = "cbAContabilizarStock";
            this.cbAContabilizarStock.Size = new System.Drawing.Size(123, 15);
            this.cbAContabilizarStock.TabIndex = 34;
            this.cbAContabilizarStock.Text = "Contabilizar Stocks";
            this.cbAContabilizarStock.UseSelectable = true;
            // 
            // cbAPermiteAlterar
            // 
            this.cbAPermiteAlterar.AutoSize = true;
            this.cbAPermiteAlterar.Location = new System.Drawing.Point(0, 475);
            this.cbAPermiteAlterar.Name = "cbAPermiteAlterar";
            this.cbAPermiteAlterar.Size = new System.Drawing.Size(170, 15);
            this.cbAPermiteAlterar.TabIndex = 33;
            this.cbAPermiteAlterar.Text = "Permite alt. Preço da Tabela";
            this.cbAPermiteAlterar.UseSelectable = true;
            // 
            // metroTabControl2
            // 
            this.metroTabControl2.Controls.Add(this.metroTabPage9);
            this.metroTabControl2.Controls.Add(this.metroTabPage10);
            this.metroTabControl2.Controls.Add(this.metroTabPage11);
            this.metroTabControl2.Controls.Add(this.metroTabPage12);
            this.metroTabControl2.Controls.Add(this.metroTabPage13);
            this.metroTabControl2.Controls.Add(this.metroTabPage14);
            this.metroTabControl2.Location = new System.Drawing.Point(454, 103);
            this.metroTabControl2.Name = "metroTabControl2";
            this.metroTabControl2.SelectedIndex = 0;
            this.metroTabControl2.Size = new System.Drawing.Size(496, 248);
            this.metroTabControl2.TabIndex = 32;
            this.metroTabControl2.UseSelectable = true;
            // 
            // metroTabPage9
            // 
            this.metroTabPage9.Controls.Add(this.label25);
            this.metroTabPage9.Controls.Add(this.label23);
            this.metroTabPage9.Controls.Add(this.label22);
            this.metroTabPage9.Controls.Add(this.label21);
            this.metroTabPage9.Controls.Add(this.metroTextBox15);
            this.metroTabPage9.Controls.Add(this.metroTextBox14);
            this.metroTabPage9.Controls.Add(this.metroCheckBox15);
            this.metroTabPage9.Controls.Add(this.metroTextBox13);
            this.metroTabPage9.Controls.Add(this.metroTextBox12);
            this.metroTabPage9.Controls.Add(this.metroTextBox10);
            this.metroTabPage9.Controls.Add(this.txtAPCM);
            this.metroTabPage9.Controls.Add(this.txtAPCP);
            this.metroTabPage9.Controls.Add(this.txtAFOB);
            this.metroTabPage9.Controls.Add(this.metroLabel43);
            this.metroTabPage9.Controls.Add(this.metroLabel44);
            this.metroTabPage9.Controls.Add(this.metroLabel45);
            this.metroTabPage9.Controls.Add(this.metroLabel42);
            this.metroTabPage9.Controls.Add(this.metroLabel41);
            this.metroTabPage9.Controls.Add(this.metroLabel40);
            this.metroTabPage9.HorizontalScrollbarBarColor = true;
            this.metroTabPage9.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage9.HorizontalScrollbarSize = 10;
            this.metroTabPage9.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage9.Name = "metroTabPage9";
            this.metroTabPage9.Size = new System.Drawing.Size(488, 206);
            this.metroTabPage9.TabIndex = 0;
            this.metroTabPage9.Text = "Compra";
            this.metroTabPage9.VerticalScrollbarBarColor = true;
            this.metroTabPage9.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage9.VerticalScrollbarSize = 10;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(178, 101);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(11, 13);
            this.label25.TabIndex = 44;
            this.label25.Text = "*";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(27, 104);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 13);
            this.label23.TabIndex = 43;
            this.label23.Text = "*";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(178, 40);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(11, 13);
            this.label22.TabIndex = 42;
            this.label22.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(27, 40);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 13);
            this.label21.TabIndex = 41;
            this.label21.Text = "*";
            // 
            // metroTextBox15
            // 
            this.metroTextBox15.Lines = new string[] {
        "0"};
            this.metroTextBox15.Location = new System.Drawing.Point(346, 152);
            this.metroTextBox15.MaxLength = 32767;
            this.metroTextBox15.Name = "metroTextBox15";
            this.metroTextBox15.PasswordChar = '\0';
            this.metroTextBox15.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox15.SelectedText = "";
            this.metroTextBox15.Size = new System.Drawing.Size(112, 23);
            this.metroTextBox15.TabIndex = 40;
            this.metroTextBox15.Text = "0";
            this.metroTextBox15.UseSelectable = true;
            // 
            // metroTextBox14
            // 
            this.metroTextBox14.Lines = new string[] {
        "0"};
            this.metroTextBox14.Location = new System.Drawing.Point(195, 152);
            this.metroTextBox14.MaxLength = 32767;
            this.metroTextBox14.Name = "metroTextBox14";
            this.metroTextBox14.PasswordChar = '\0';
            this.metroTextBox14.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox14.SelectedText = "";
            this.metroTextBox14.Size = new System.Drawing.Size(114, 23);
            this.metroTextBox14.TabIndex = 39;
            this.metroTextBox14.Text = "0";
            this.metroTextBox14.UseSelectable = true;
            // 
            // metroCheckBox15
            // 
            this.metroCheckBox15.AutoSize = true;
            this.metroCheckBox15.Location = new System.Drawing.Point(44, 160);
            this.metroCheckBox15.Name = "metroCheckBox15";
            this.metroCheckBox15.Size = new System.Drawing.Size(90, 15);
            this.metroCheckBox15.TabIndex = 38;
            this.metroCheckBox15.Text = "Calcular PCL";
            this.metroCheckBox15.UseSelectable = true;
            // 
            // metroTextBox13
            // 
            this.metroTextBox13.Lines = new string[] {
        "0"};
            this.metroTextBox13.Location = new System.Drawing.Point(346, 104);
            this.metroTextBox13.MaxLength = 32767;
            this.metroTextBox13.Name = "metroTextBox13";
            this.metroTextBox13.PasswordChar = '\0';
            this.metroTextBox13.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox13.SelectedText = "";
            this.metroTextBox13.Size = new System.Drawing.Size(112, 23);
            this.metroTextBox13.TabIndex = 37;
            this.metroTextBox13.Text = "0";
            this.metroTextBox13.UseSelectable = true;
            // 
            // metroTextBox12
            // 
            this.metroTextBox12.Lines = new string[] {
        "0"};
            this.metroTextBox12.Location = new System.Drawing.Point(195, 104);
            this.metroTextBox12.MaxLength = 32767;
            this.metroTextBox12.Name = "metroTextBox12";
            this.metroTextBox12.PasswordChar = '\0';
            this.metroTextBox12.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox12.SelectedText = "";
            this.metroTextBox12.Size = new System.Drawing.Size(114, 23);
            this.metroTextBox12.TabIndex = 36;
            this.metroTextBox12.Text = "0";
            this.metroTextBox12.UseSelectable = true;
            // 
            // metroTextBox10
            // 
            this.metroTextBox10.Lines = new string[] {
        "0"};
            this.metroTextBox10.Location = new System.Drawing.Point(44, 104);
            this.metroTextBox10.MaxLength = 32767;
            this.metroTextBox10.Name = "metroTextBox10";
            this.metroTextBox10.PasswordChar = '\0';
            this.metroTextBox10.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox10.SelectedText = "";
            this.metroTextBox10.Size = new System.Drawing.Size(116, 23);
            this.metroTextBox10.TabIndex = 35;
            this.metroTextBox10.Text = "0";
            this.metroTextBox10.UseSelectable = true;
            // 
            // txtAPCM
            // 
            this.txtAPCM.Lines = new string[] {
        "0"};
            this.txtAPCM.Location = new System.Drawing.Point(346, 40);
            this.txtAPCM.MaxLength = 32767;
            this.txtAPCM.Name = "txtAPCM";
            this.txtAPCM.PasswordChar = '\0';
            this.txtAPCM.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAPCM.SelectedText = "";
            this.txtAPCM.Size = new System.Drawing.Size(112, 23);
            this.txtAPCM.TabIndex = 34;
            this.txtAPCM.Text = "0";
            this.txtAPCM.UseSelectable = true;
            // 
            // txtAPCP
            // 
            this.txtAPCP.Lines = new string[] {
        "0"};
            this.txtAPCP.Location = new System.Drawing.Point(195, 40);
            this.txtAPCP.MaxLength = 32767;
            this.txtAPCP.Name = "txtAPCP";
            this.txtAPCP.PasswordChar = '\0';
            this.txtAPCP.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAPCP.SelectedText = "";
            this.txtAPCP.Size = new System.Drawing.Size(114, 23);
            this.txtAPCP.TabIndex = 33;
            this.txtAPCP.Text = "0";
            this.txtAPCP.UseSelectable = true;
            // 
            // txtAFOB
            // 
            this.txtAFOB.Lines = new string[] {
        "0"};
            this.txtAFOB.Location = new System.Drawing.Point(44, 40);
            this.txtAFOB.MaxLength = 32767;
            this.txtAFOB.Name = "txtAFOB";
            this.txtAFOB.PasswordChar = '\0';
            this.txtAFOB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAFOB.SelectedText = "";
            this.txtAFOB.Size = new System.Drawing.Size(116, 23);
            this.txtAFOB.TabIndex = 32;
            this.txtAFOB.Text = "0";
            this.txtAFOB.UseSelectable = true;
            // 
            // metroLabel43
            // 
            this.metroLabel43.AutoSize = true;
            this.metroLabel43.Location = new System.Drawing.Point(356, 82);
            this.metroLabel43.Name = "metroLabel43";
            this.metroLabel43.Size = new System.Drawing.Size(93, 19);
            this.metroLabel43.TabIndex = 7;
            this.metroLabel43.Text = "PMC Nacional";
            // 
            // metroLabel44
            // 
            this.metroLabel44.AutoSize = true;
            this.metroLabel44.Location = new System.Drawing.Point(208, 82);
            this.metroLabel44.Name = "metroLabel44";
            this.metroLabel44.Size = new System.Drawing.Size(90, 19);
            this.metroLabel44.TabIndex = 6;
            this.metroLabel44.Text = "PCA Nacional";
            // 
            // metroLabel45
            // 
            this.metroLabel45.AutoSize = true;
            this.metroLabel45.Location = new System.Drawing.Point(57, 82);
            this.metroLabel45.Name = "metroLabel45";
            this.metroLabel45.Size = new System.Drawing.Size(90, 19);
            this.metroLabel45.TabIndex = 5;
            this.metroLabel45.Text = "FOB Nacional";
            // 
            // metroLabel42
            // 
            this.metroLabel42.AutoSize = true;
            this.metroLabel42.Location = new System.Drawing.Point(387, 18);
            this.metroLabel42.Name = "metroLabel42";
            this.metroLabel42.Size = new System.Drawing.Size(38, 19);
            this.metroLabel42.TabIndex = 4;
            this.metroLabel42.Text = "PMC";
            // 
            // metroLabel41
            // 
            this.metroLabel41.AutoSize = true;
            this.metroLabel41.Location = new System.Drawing.Point(231, 18);
            this.metroLabel41.Name = "metroLabel41";
            this.metroLabel41.Size = new System.Drawing.Size(35, 19);
            this.metroLabel41.TabIndex = 3;
            this.metroLabel41.Text = "PCA";
            // 
            // metroLabel40
            // 
            this.metroLabel40.AutoSize = true;
            this.metroLabel40.Location = new System.Drawing.Point(81, 18);
            this.metroLabel40.Name = "metroLabel40";
            this.metroLabel40.Size = new System.Drawing.Size(35, 19);
            this.metroLabel40.TabIndex = 2;
            this.metroLabel40.Text = "FOB";
            // 
            // metroTabPage10
            // 
            this.metroTabPage10.Controls.Add(this.metroGrid1);
            this.metroTabPage10.HorizontalScrollbarBarColor = true;
            this.metroTabPage10.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage10.HorizontalScrollbarSize = 10;
            this.metroTabPage10.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage10.Name = "metroTabPage10";
            this.metroTabPage10.Size = new System.Drawing.Size(488, 206);
            this.metroTabPage10.TabIndex = 1;
            this.metroTabPage10.Text = "Equipamento";
            this.metroTabPage10.VerticalScrollbarBarColor = true;
            this.metroTabPage10.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage10.VerticalScrollbarSize = 10;
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Marca,
            this.Modelo,
            this.NumeroSerie});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle11;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(0, 3);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(485, 200);
            this.metroGrid1.TabIndex = 2;
            // 
            // Marca
            // 
            this.Marca.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Marca.HeaderText = "Marca";
            this.Marca.Name = "Marca";
            // 
            // Modelo
            // 
            this.Modelo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Modelo.HeaderText = "Modelo";
            this.Modelo.Name = "Modelo";
            // 
            // NumeroSerie
            // 
            this.NumeroSerie.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NumeroSerie.HeaderText = "Nº de Série";
            this.NumeroSerie.Name = "NumeroSerie";
            // 
            // metroTabPage11
            // 
            this.metroTabPage11.Controls.Add(this.metroPanel2);
            this.metroTabPage11.Controls.Add(this.metroTextBox18);
            this.metroTabPage11.Controls.Add(this.metroTextBox19);
            this.metroTabPage11.Controls.Add(this.metroLabel48);
            this.metroTabPage11.Controls.Add(this.metroLabel49);
            this.metroTabPage11.Controls.Add(this.metroTextBox17);
            this.metroTabPage11.Controls.Add(this.metroTextBox16);
            this.metroTabPage11.Controls.Add(this.metroLabel47);
            this.metroTabPage11.Controls.Add(this.metroLabel46);
            this.metroTabPage11.Controls.Add(this.label16);
            this.metroTabPage11.Controls.Add(this.label15);
            this.metroTabPage11.HorizontalScrollbarBarColor = true;
            this.metroTabPage11.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage11.HorizontalScrollbarSize = 10;
            this.metroTabPage11.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage11.Name = "metroTabPage11";
            this.metroTabPage11.Size = new System.Drawing.Size(488, 206);
            this.metroTabPage11.TabIndex = 2;
            this.metroTabPage11.Text = "Criação/Alteração";
            this.metroTabPage11.VerticalScrollbarBarColor = true;
            this.metroTabPage11.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage11.VerticalScrollbarSize = 10;
            // 
            // metroPanel2
            // 
            this.metroPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(240, 62);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(10, 84);
            this.metroPanel2.TabIndex = 12;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // metroTextBox18
            // 
            this.metroTextBox18.Lines = new string[0];
            this.metroTextBox18.Location = new System.Drawing.Point(361, 113);
            this.metroTextBox18.MaxLength = 32767;
            this.metroTextBox18.Name = "metroTextBox18";
            this.metroTextBox18.PasswordChar = '\0';
            this.metroTextBox18.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox18.SelectedText = "";
            this.metroTextBox18.Size = new System.Drawing.Size(108, 23);
            this.metroTextBox18.TabIndex = 11;
            this.metroTextBox18.UseSelectable = true;
            // 
            // metroTextBox19
            // 
            this.metroTextBox19.Lines = new string[0];
            this.metroTextBox19.Location = new System.Drawing.Point(361, 62);
            this.metroTextBox19.MaxLength = 32767;
            this.metroTextBox19.Name = "metroTextBox19";
            this.metroTextBox19.PasswordChar = '\0';
            this.metroTextBox19.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox19.SelectedText = "";
            this.metroTextBox19.Size = new System.Drawing.Size(108, 23);
            this.metroTextBox19.TabIndex = 10;
            this.metroTextBox19.UseSelectable = true;
            // 
            // metroLabel48
            // 
            this.metroLabel48.AutoSize = true;
            this.metroLabel48.Location = new System.Drawing.Point(274, 117);
            this.metroLabel48.Name = "metroLabel48";
            this.metroLabel48.Size = new System.Drawing.Size(36, 19);
            this.metroLabel48.TabIndex = 9;
            this.metroLabel48.Text = "Data";
            // 
            // metroLabel49
            // 
            this.metroLabel49.AutoSize = true;
            this.metroLabel49.Location = new System.Drawing.Point(274, 66);
            this.metroLabel49.Name = "metroLabel49";
            this.metroLabel49.Size = new System.Drawing.Size(68, 19);
            this.metroLabel49.TabIndex = 8;
            this.metroLabel49.Text = "Operador";
            // 
            // metroTextBox17
            // 
            this.metroTextBox17.Lines = new string[0];
            this.metroTextBox17.Location = new System.Drawing.Point(101, 113);
            this.metroTextBox17.MaxLength = 32767;
            this.metroTextBox17.Name = "metroTextBox17";
            this.metroTextBox17.PasswordChar = '\0';
            this.metroTextBox17.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox17.SelectedText = "";
            this.metroTextBox17.Size = new System.Drawing.Size(108, 23);
            this.metroTextBox17.TabIndex = 7;
            this.metroTextBox17.UseSelectable = true;
            // 
            // metroTextBox16
            // 
            this.metroTextBox16.Lines = new string[0];
            this.metroTextBox16.Location = new System.Drawing.Point(101, 62);
            this.metroTextBox16.MaxLength = 32767;
            this.metroTextBox16.Name = "metroTextBox16";
            this.metroTextBox16.PasswordChar = '\0';
            this.metroTextBox16.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox16.SelectedText = "";
            this.metroTextBox16.Size = new System.Drawing.Size(108, 23);
            this.metroTextBox16.TabIndex = 6;
            this.metroTextBox16.UseSelectable = true;
            // 
            // metroLabel47
            // 
            this.metroLabel47.AutoSize = true;
            this.metroLabel47.Location = new System.Drawing.Point(14, 117);
            this.metroLabel47.Name = "metroLabel47";
            this.metroLabel47.Size = new System.Drawing.Size(36, 19);
            this.metroLabel47.TabIndex = 5;
            this.metroLabel47.Text = "Data";
            // 
            // metroLabel46
            // 
            this.metroLabel46.AutoSize = true;
            this.metroLabel46.Location = new System.Drawing.Point(14, 66);
            this.metroLabel46.Name = "metroLabel46";
            this.metroLabel46.Size = new System.Drawing.Size(68, 19);
            this.metroLabel46.TabIndex = 4;
            this.metroLabel46.Text = "Operador";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(271, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "Alteração";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(11, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Creiação";
            // 
            // metroTabPage12
            // 
            this.metroTabPage12.Controls.Add(this.label26);
            this.metroTabPage12.Controls.Add(this.metroCheckBox17);
            this.metroTabPage12.Controls.Add(this.metroCheckBox16);
            this.metroTabPage12.Controls.Add(this.txtADesconto);
            this.metroTabPage12.Controls.Add(this.metroLabel54);
            this.metroTabPage12.Controls.Add(this.metroTextBox23);
            this.metroTabPage12.Controls.Add(this.txtALucro);
            this.metroTabPage12.Controls.Add(this.metroTextBox21);
            this.metroTabPage12.Controls.Add(this.txtAPVP);
            this.metroTabPage12.Controls.Add(this.metroLabel53);
            this.metroTabPage12.Controls.Add(this.metroLabel52);
            this.metroTabPage12.Controls.Add(this.metroLabel51);
            this.metroTabPage12.Controls.Add(this.label18);
            this.metroTabPage12.Controls.Add(this.label17);
            this.metroTabPage12.Controls.Add(this.metroLabel50);
            this.metroTabPage12.HorizontalScrollbarBarColor = true;
            this.metroTabPage12.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage12.HorizontalScrollbarSize = 10;
            this.metroTabPage12.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage12.Name = "metroTabPage12";
            this.metroTabPage12.Size = new System.Drawing.Size(488, 206);
            this.metroTabPage12.TabIndex = 3;
            this.metroTabPage12.Text = "Venda";
            this.metroTabPage12.VerticalScrollbarBarColor = true;
            this.metroTabPage12.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage12.VerticalScrollbarSize = 10;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(3, 57);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(11, 13);
            this.label26.TabIndex = 42;
            this.label26.Text = "*";
            // 
            // metroCheckBox17
            // 
            this.metroCheckBox17.AutoSize = true;
            this.metroCheckBox17.Location = new System.Drawing.Point(321, 144);
            this.metroCheckBox17.Name = "metroCheckBox17";
            this.metroCheckBox17.Size = new System.Drawing.Size(103, 15);
            this.metroCheckBox17.TabIndex = 38;
            this.metroCheckBox17.Text = "Stock Negativo";
            this.metroCheckBox17.UseSelectable = true;
            // 
            // metroCheckBox16
            // 
            this.metroCheckBox16.AutoSize = true;
            this.metroCheckBox16.Location = new System.Drawing.Point(320, 123);
            this.metroCheckBox16.Name = "metroCheckBox16";
            this.metroCheckBox16.Size = new System.Drawing.Size(82, 15);
            this.metroCheckBox16.TabIndex = 37;
            this.metroCheckBox16.Text = "Obsolecto?";
            this.metroCheckBox16.UseSelectable = true;
            // 
            // txtADesconto
            // 
            this.txtADesconto.Lines = new string[] {
        "0"};
            this.txtADesconto.Location = new System.Drawing.Point(321, 57);
            this.txtADesconto.MaxLength = 32767;
            this.txtADesconto.Name = "txtADesconto";
            this.txtADesconto.PasswordChar = '\0';
            this.txtADesconto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtADesconto.SelectedText = "";
            this.txtADesconto.Size = new System.Drawing.Size(112, 23);
            this.txtADesconto.TabIndex = 36;
            this.txtADesconto.Text = "0";
            this.txtADesconto.UseSelectable = true;
            // 
            // metroLabel54
            // 
            this.metroLabel54.AutoSize = true;
            this.metroLabel54.Location = new System.Drawing.Point(321, 31);
            this.metroLabel54.Name = "metroLabel54";
            this.metroLabel54.Size = new System.Drawing.Size(112, 19);
            this.metroLabel54.TabIndex = 35;
            this.metroLabel54.Text = "Percentagem - %";
            // 
            // metroTextBox23
            // 
            this.metroTextBox23.Lines = new string[] {
        "0"};
            this.metroTextBox23.Location = new System.Drawing.Point(144, 117);
            this.metroTextBox23.MaxLength = 32767;
            this.metroTextBox23.Name = "metroTextBox23";
            this.metroTextBox23.PasswordChar = '\0';
            this.metroTextBox23.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox23.SelectedText = "";
            this.metroTextBox23.Size = new System.Drawing.Size(103, 23);
            this.metroTextBox23.TabIndex = 34;
            this.metroTextBox23.Text = "0";
            this.metroTextBox23.UseSelectable = true;
            // 
            // txtALucro
            // 
            this.txtALucro.Lines = new string[] {
        "0"};
            this.txtALucro.Location = new System.Drawing.Point(17, 117);
            this.txtALucro.MaxLength = 32767;
            this.txtALucro.Name = "txtALucro";
            this.txtALucro.PasswordChar = '\0';
            this.txtALucro.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtALucro.SelectedText = "";
            this.txtALucro.Size = new System.Drawing.Size(103, 23);
            this.txtALucro.TabIndex = 33;
            this.txtALucro.Text = "0";
            this.txtALucro.UseSelectable = true;
            // 
            // metroTextBox21
            // 
            this.metroTextBox21.Lines = new string[] {
        "0"};
            this.metroTextBox21.Location = new System.Drawing.Point(144, 57);
            this.metroTextBox21.MaxLength = 32767;
            this.metroTextBox21.Name = "metroTextBox21";
            this.metroTextBox21.PasswordChar = '\0';
            this.metroTextBox21.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox21.SelectedText = "";
            this.metroTextBox21.Size = new System.Drawing.Size(103, 23);
            this.metroTextBox21.TabIndex = 32;
            this.metroTextBox21.Text = "0";
            this.metroTextBox21.UseSelectable = true;
            // 
            // txtAPVP
            // 
            this.txtAPVP.Lines = new string[] {
        "0"};
            this.txtAPVP.Location = new System.Drawing.Point(17, 57);
            this.txtAPVP.MaxLength = 32767;
            this.txtAPVP.Name = "txtAPVP";
            this.txtAPVP.PasswordChar = '\0';
            this.txtAPVP.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAPVP.SelectedText = "";
            this.txtAPVP.Size = new System.Drawing.Size(103, 23);
            this.txtAPVP.TabIndex = 31;
            this.txtAPVP.Text = "0";
            this.txtAPVP.UseSelectable = true;
            // 
            // metroLabel53
            // 
            this.metroLabel53.AutoSize = true;
            this.metroLabel53.Location = new System.Drawing.Point(144, 92);
            this.metroLabel53.Name = "metroLabel53";
            this.metroLabel53.Size = new System.Drawing.Size(56, 19);
            this.metroLabel53.TabIndex = 7;
            this.metroLabel53.Text = "Cambio";
            // 
            // metroLabel52
            // 
            this.metroLabel52.AutoSize = true;
            this.metroLabel52.Location = new System.Drawing.Point(-4, 92);
            this.metroLabel52.Name = "metroLabel52";
            this.metroLabel52.Size = new System.Drawing.Size(78, 19);
            this.metroLabel52.TabIndex = 6;
            this.metroLabel52.Text = "Lucro PV-%";
            // 
            // metroLabel51
            // 
            this.metroLabel51.AutoSize = true;
            this.metroLabel51.Location = new System.Drawing.Point(144, 31);
            this.metroLabel51.Name = "metroLabel51";
            this.metroLabel51.Size = new System.Drawing.Size(80, 19);
            this.metroLabel51.TabIndex = 5;
            this.metroLabel51.Text = "PV Nacional";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(318, 9);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(61, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "Desconto";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(-3, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "Preço";
            // 
            // metroLabel50
            // 
            this.metroLabel50.AutoSize = true;
            this.metroLabel50.Location = new System.Drawing.Point(0, 31);
            this.metroLabel50.Name = "metroLabel50";
            this.metroLabel50.Size = new System.Drawing.Size(72, 19);
            this.metroLabel50.TabIndex = 2;
            this.metroLabel50.Text = "PV Padrão";
            // 
            // metroTabPage13
            // 
            this.metroTabPage13.Controls.Add(this.txtAStockMimimo);
            this.metroTabPage13.Controls.Add(this.txtAStockMaximo);
            this.metroTabPage13.Controls.Add(this.txtAPontoEncomenda);
            this.metroTabPage13.Controls.Add(this.txtAStockReal);
            this.metroTabPage13.Controls.Add(this.metroLabel58);
            this.metroTabPage13.Controls.Add(this.metroLabel57);
            this.metroTabPage13.Controls.Add(this.metroLabel56);
            this.metroTabPage13.Controls.Add(this.metroLabel55);
            this.metroTabPage13.HorizontalScrollbarBarColor = true;
            this.metroTabPage13.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage13.HorizontalScrollbarSize = 10;
            this.metroTabPage13.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage13.Name = "metroTabPage13";
            this.metroTabPage13.Size = new System.Drawing.Size(488, 206);
            this.metroTabPage13.TabIndex = 4;
            this.metroTabPage13.Text = "Stock";
            this.metroTabPage13.VerticalScrollbarBarColor = true;
            this.metroTabPage13.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage13.VerticalScrollbarSize = 10;
            // 
            // txtAStockMimimo
            // 
            this.txtAStockMimimo.Lines = new string[] {
        "0"};
            this.txtAStockMimimo.Location = new System.Drawing.Point(214, 134);
            this.txtAStockMimimo.MaxLength = 32767;
            this.txtAStockMimimo.Name = "txtAStockMimimo";
            this.txtAStockMimimo.PasswordChar = '\0';
            this.txtAStockMimimo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAStockMimimo.SelectedText = "";
            this.txtAStockMimimo.Size = new System.Drawing.Size(180, 23);
            this.txtAStockMimimo.TabIndex = 34;
            this.txtAStockMimimo.Text = "0";
            this.txtAStockMimimo.UseSelectable = true;
            // 
            // txtAStockMaximo
            // 
            this.txtAStockMaximo.Lines = new string[] {
        "0"};
            this.txtAStockMaximo.Location = new System.Drawing.Point(214, 100);
            this.txtAStockMaximo.MaxLength = 32767;
            this.txtAStockMaximo.Name = "txtAStockMaximo";
            this.txtAStockMaximo.PasswordChar = '\0';
            this.txtAStockMaximo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAStockMaximo.SelectedText = "";
            this.txtAStockMaximo.Size = new System.Drawing.Size(180, 23);
            this.txtAStockMaximo.TabIndex = 33;
            this.txtAStockMaximo.Text = "0";
            this.txtAStockMaximo.UseSelectable = true;
            // 
            // txtAPontoEncomenda
            // 
            this.txtAPontoEncomenda.Lines = new string[] {
        "0"};
            this.txtAPontoEncomenda.Location = new System.Drawing.Point(214, 64);
            this.txtAPontoEncomenda.MaxLength = 32767;
            this.txtAPontoEncomenda.Name = "txtAPontoEncomenda";
            this.txtAPontoEncomenda.PasswordChar = '\0';
            this.txtAPontoEncomenda.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAPontoEncomenda.SelectedText = "";
            this.txtAPontoEncomenda.Size = new System.Drawing.Size(180, 23);
            this.txtAPontoEncomenda.TabIndex = 32;
            this.txtAPontoEncomenda.Text = "0";
            this.txtAPontoEncomenda.UseSelectable = true;
            // 
            // txtAStockReal
            // 
            this.txtAStockReal.Lines = new string[] {
        "0"};
            this.txtAStockReal.Location = new System.Drawing.Point(214, 31);
            this.txtAStockReal.MaxLength = 32767;
            this.txtAStockReal.Name = "txtAStockReal";
            this.txtAStockReal.PasswordChar = '\0';
            this.txtAStockReal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAStockReal.SelectedText = "";
            this.txtAStockReal.Size = new System.Drawing.Size(180, 23);
            this.txtAStockReal.TabIndex = 31;
            this.txtAStockReal.Text = "0";
            this.txtAStockReal.UseSelectable = true;
            // 
            // metroLabel58
            // 
            this.metroLabel58.AutoSize = true;
            this.metroLabel58.Location = new System.Drawing.Point(67, 68);
            this.metroLabel58.Name = "metroLabel58";
            this.metroLabel58.Size = new System.Drawing.Size(136, 19);
            this.metroLabel58.TabIndex = 5;
            this.metroLabel58.Text = "Ponto de Encomenda";
            // 
            // metroLabel57
            // 
            this.metroLabel57.AutoSize = true;
            this.metroLabel57.Location = new System.Drawing.Point(67, 138);
            this.metroLabel57.Name = "metroLabel57";
            this.metroLabel57.Size = new System.Drawing.Size(89, 19);
            this.metroLabel57.TabIndex = 4;
            this.metroLabel57.Text = "Stock Mínimo";
            // 
            // metroLabel56
            // 
            this.metroLabel56.AutoSize = true;
            this.metroLabel56.Location = new System.Drawing.Point(67, 104);
            this.metroLabel56.Name = "metroLabel56";
            this.metroLabel56.Size = new System.Drawing.Size(92, 19);
            this.metroLabel56.TabIndex = 3;
            this.metroLabel56.Text = "Stock Máximo";
            // 
            // metroLabel55
            // 
            this.metroLabel55.AutoSize = true;
            this.metroLabel55.Location = new System.Drawing.Point(67, 35);
            this.metroLabel55.Name = "metroLabel55";
            this.metroLabel55.Size = new System.Drawing.Size(69, 19);
            this.metroLabel55.TabIndex = 2;
            this.metroLabel55.Text = "Stock Real";
            // 
            // metroTabPage14
            // 
            this.metroTabPage14.Controls.Add(this.metroGrid2);
            this.metroTabPage14.HorizontalScrollbarBarColor = true;
            this.metroTabPage14.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage14.HorizontalScrollbarSize = 10;
            this.metroTabPage14.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage14.Name = "metroTabPage14";
            this.metroTabPage14.Size = new System.Drawing.Size(488, 206);
            this.metroTabPage14.TabIndex = 5;
            this.metroTabPage14.Text = "Fornecedor";
            this.metroTabPage14.VerticalScrollbarBarColor = true;
            this.metroTabPage14.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage14.VerticalScrollbarSize = 10;
            // 
            // metroGrid2
            // 
            this.metroGrid2.AllowUserToResizeRows = false;
            this.metroGrid2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.metroGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo,
            this.Nome});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid2.DefaultCellStyle = dataGridViewCellStyle14;
            this.metroGrid2.EnableHeadersVisualStyles = false;
            this.metroGrid2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid2.Location = new System.Drawing.Point(-4, 3);
            this.metroGrid2.Name = "metroGrid2";
            this.metroGrid2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.metroGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid2.Size = new System.Drawing.Size(489, 200);
            this.metroGrid2.TabIndex = 2;
            // 
            // Codigo
            // 
            this.Codigo.HeaderText = "Código";
            this.Codigo.Name = "Codigo";
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.HeaderText = "Nome";
            this.Nome.Name = "Nome";
            // 
            // txtANorme
            // 
            this.txtANorme.Lines = new string[] {
        "0"};
            this.txtANorme.Location = new System.Drawing.Point(134, 395);
            this.txtANorme.MaxLength = 32767;
            this.txtANorme.Name = "txtANorme";
            this.txtANorme.PasswordChar = '\0';
            this.txtANorme.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtANorme.SelectedText = "";
            this.txtANorme.Size = new System.Drawing.Size(103, 23);
            this.txtANorme.TabIndex = 31;
            this.txtANorme.Text = "0";
            this.txtANorme.UseSelectable = true;
            // 
            // txtALocal
            // 
            this.txtALocal.Lines = new string[] {
        "0"};
            this.txtALocal.Location = new System.Drawing.Point(134, 366);
            this.txtALocal.MaxLength = 32767;
            this.txtALocal.Name = "txtALocal";
            this.txtALocal.PasswordChar = '\0';
            this.txtALocal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtALocal.SelectedText = "";
            this.txtALocal.Size = new System.Drawing.Size(103, 23);
            this.txtALocal.TabIndex = 30;
            this.txtALocal.Text = "0";
            this.txtALocal.UseSelectable = true;
            // 
            // txtQdtEmbalagem
            // 
            this.txtQdtEmbalagem.Lines = new string[] {
        "0"};
            this.txtQdtEmbalagem.Location = new System.Drawing.Point(134, 337);
            this.txtQdtEmbalagem.MaxLength = 32767;
            this.txtQdtEmbalagem.Name = "txtQdtEmbalagem";
            this.txtQdtEmbalagem.PasswordChar = '\0';
            this.txtQdtEmbalagem.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtQdtEmbalagem.SelectedText = "";
            this.txtQdtEmbalagem.Size = new System.Drawing.Size(103, 23);
            this.txtQdtEmbalagem.TabIndex = 29;
            this.txtQdtEmbalagem.Text = "0";
            this.txtQdtEmbalagem.UseSelectable = true;
            // 
            // metroLabel38
            // 
            this.metroLabel38.AutoSize = true;
            this.metroLabel38.Location = new System.Drawing.Point(-4, 399);
            this.metroLabel38.Name = "metroLabel38";
            this.metroLabel38.Size = new System.Drawing.Size(51, 19);
            this.metroLabel38.TabIndex = 23;
            this.metroLabel38.Text = "Norma";
            // 
            // metroLabel37
            // 
            this.metroLabel37.AutoSize = true;
            this.metroLabel37.Location = new System.Drawing.Point(-4, 370);
            this.metroLabel37.Name = "metroLabel37";
            this.metroLabel37.Size = new System.Drawing.Size(39, 19);
            this.metroLabel37.TabIndex = 22;
            this.metroLabel37.Text = "Local";
            // 
            // metroLabel36
            // 
            this.metroLabel36.AutoSize = true;
            this.metroLabel36.Location = new System.Drawing.Point(-4, 341);
            this.metroLabel36.Name = "metroLabel36";
            this.metroLabel36.Size = new System.Drawing.Size(110, 19);
            this.metroLabel36.TabIndex = 21;
            this.metroLabel36.Text = "Qtd. Embalagem";
            // 
            // metroLabel35
            // 
            this.metroLabel35.AutoSize = true;
            this.metroLabel35.Location = new System.Drawing.Point(-4, 312);
            this.metroLabel35.Name = "metroLabel35";
            this.metroLabel35.Size = new System.Drawing.Size(119, 19);
            this.metroLabel35.TabIndex = 20;
            this.metroLabel35.Text = "Desig. Embalagem";
            // 
            // metroLabel34
            // 
            this.metroLabel34.AutoSize = true;
            this.metroLabel34.Location = new System.Drawing.Point(-4, 277);
            this.metroLabel34.Name = "metroLabel34";
            this.metroLabel34.Size = new System.Drawing.Size(58, 19);
            this.metroLabel34.TabIndex = 19;
            this.metroLabel34.Text = "Unidade";
            // 
            // metroLabel33
            // 
            this.metroLabel33.AutoSize = true;
            this.metroLabel33.Location = new System.Drawing.Point(-4, 242);
            this.metroLabel33.Name = "metroLabel33";
            this.metroLabel33.Size = new System.Drawing.Size(96, 19);
            this.metroLabel33.TabIndex = 18;
            this.metroLabel33.Text = "Grupo na Web";
            // 
            // metroLabel32
            // 
            this.metroLabel32.AutoSize = true;
            this.metroLabel32.Location = new System.Drawing.Point(611, 7);
            this.metroLabel32.Name = "metroLabel32";
            this.metroLabel32.Size = new System.Drawing.Size(143, 19);
            this.metroLabel32.TabIndex = 17;
            this.metroLabel32.Text = "Descrição Suplementar";
            // 
            // txtADesignacapSuplementar
            // 
            this.txtADesignacapSuplementar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.txtADesignacapSuplementar.Location = new System.Drawing.Point(510, 29);
            this.txtADesignacapSuplementar.Name = "txtADesignacapSuplementar";
            this.txtADesignacapSuplementar.Size = new System.Drawing.Size(429, 67);
            this.txtADesignacapSuplementar.TabIndex = 16;
            this.txtADesignacapSuplementar.Text = "";
            // 
            // txtADesignacaoII
            // 
            this.txtADesignacaoII.Lines = new string[0];
            this.txtADesignacaoII.Location = new System.Drawing.Point(121, 203);
            this.txtADesignacaoII.MaxLength = 32767;
            this.txtADesignacaoII.Name = "txtADesignacaoII";
            this.txtADesignacaoII.PasswordChar = '\0';
            this.txtADesignacaoII.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtADesignacaoII.SelectedText = "";
            this.txtADesignacaoII.Size = new System.Drawing.Size(273, 23);
            this.txtADesignacaoII.TabIndex = 13;
            this.txtADesignacaoII.UseSelectable = true;
            // 
            // txtADesignacao
            // 
            this.txtADesignacao.Lines = new string[0];
            this.txtADesignacao.Location = new System.Drawing.Point(121, 174);
            this.txtADesignacao.MaxLength = 32767;
            this.txtADesignacao.Name = "txtADesignacao";
            this.txtADesignacao.PasswordChar = '\0';
            this.txtADesignacao.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtADesignacao.SelectedText = "";
            this.txtADesignacao.Size = new System.Drawing.Size(273, 23);
            this.txtADesignacao.TabIndex = 12;
            this.txtADesignacao.UseSelectable = true;
            // 
            // txtAReferencia
            // 
            this.txtAReferencia.Lines = new string[0];
            this.txtAReferencia.Location = new System.Drawing.Point(121, 73);
            this.txtAReferencia.MaxLength = 32767;
            this.txtAReferencia.Name = "txtAReferencia";
            this.txtAReferencia.PasswordChar = '\0';
            this.txtAReferencia.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAReferencia.SelectedText = "";
            this.txtAReferencia.Size = new System.Drawing.Size(273, 23);
            this.txtAReferencia.TabIndex = 11;
            this.txtAReferencia.UseSelectable = true;
            // 
            // metroLabel31
            // 
            this.metroLabel31.AutoSize = true;
            this.metroLabel31.Location = new System.Drawing.Point(-4, 207);
            this.metroLabel31.Name = "metroLabel31";
            this.metroLabel31.Size = new System.Drawing.Size(86, 19);
            this.metroLabel31.TabIndex = 8;
            this.metroLabel31.Text = "Designação II";
            // 
            // metroLabel30
            // 
            this.metroLabel30.AutoSize = true;
            this.metroLabel30.Location = new System.Drawing.Point(0, 178);
            this.metroLabel30.Name = "metroLabel30";
            this.metroLabel30.Size = new System.Drawing.Size(76, 19);
            this.metroLabel30.TabIndex = 7;
            this.metroLabel30.Text = "Designação";
            // 
            // metroLabel29
            // 
            this.metroLabel29.AutoSize = true;
            this.metroLabel29.Location = new System.Drawing.Point(0, 147);
            this.metroLabel29.Name = "metroLabel29";
            this.metroLabel29.Size = new System.Drawing.Size(79, 19);
            this.metroLabel29.TabIndex = 6;
            this.metroLabel29.Text = "Sub-Família";
            // 
            // metroLabel28
            // 
            this.metroLabel28.AutoSize = true;
            this.metroLabel28.Location = new System.Drawing.Point(0, 112);
            this.metroLabel28.Name = "metroLabel28";
            this.metroLabel28.Size = new System.Drawing.Size(51, 19);
            this.metroLabel28.TabIndex = 5;
            this.metroLabel28.Text = "Família";
            // 
            // metroLabel27
            // 
            this.metroLabel27.AutoSize = true;
            this.metroLabel27.Location = new System.Drawing.Point(0, 77);
            this.metroLabel27.Name = "metroLabel27";
            this.metroLabel27.Size = new System.Drawing.Size(70, 19);
            this.metroLabel27.TabIndex = 4;
            this.metroLabel27.Text = "Referência";
            // 
            // metroLabel26
            // 
            this.metroLabel26.AutoSize = true;
            this.metroLabel26.Location = new System.Drawing.Point(0, 47);
            this.metroLabel26.Name = "metroLabel26";
            this.metroLabel26.Size = new System.Drawing.Size(77, 19);
            this.metroLabel26.TabIndex = 3;
            this.metroLabel26.Text = "Mercadoria";
            // 
            // Loja
            // 
            this.Loja.AutoSize = true;
            this.Loja.Location = new System.Drawing.Point(0, 16);
            this.Loja.Name = "Loja";
            this.Loja.Size = new System.Drawing.Size(33, 19);
            this.Loja.TabIndex = 2;
            this.Loja.Text = "Loja";
            // 
            // metroTabPage6
            // 
            this.metroTabPage6.Controls.Add(this.label20);
            this.metroTabPage6.Controls.Add(this.label19);
            this.metroTabPage6.Controls.Add(this.txtODesignacao);
            this.metroTabPage6.Controls.Add(this.txtOCodigo);
            this.metroTabPage6.Controls.Add(this.metroLabel60);
            this.metroTabPage6.Controls.Add(this.metroLabel59);
            this.metroTabPage6.HorizontalScrollbarBarColor = true;
            this.metroTabPage6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.HorizontalScrollbarSize = 10;
            this.metroTabPage6.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage6.Name = "metroTabPage6";
            this.metroTabPage6.Size = new System.Drawing.Size(959, 508);
            this.metroTabPage6.TabIndex = 5;
            this.metroTabPage6.Text = "Origem Artigos";
            this.metroTabPage6.VerticalScrollbarBarColor = true;
            this.metroTabPage6.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.VerticalScrollbarSize = 10;
            this.metroTabPage6.Leave += new System.EventHandler(this.metroTabPage6_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(323, 106);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 13);
            this.label20.TabIndex = 7;
            this.label20.Text = "*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(323, 65);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "*";
            // 
            // txtODesignacao
            // 
            this.txtODesignacao.Lines = new string[0];
            this.txtODesignacao.Location = new System.Drawing.Point(340, 106);
            this.txtODesignacao.MaxLength = 32767;
            this.txtODesignacao.Name = "txtODesignacao";
            this.txtODesignacao.PasswordChar = '\0';
            this.txtODesignacao.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtODesignacao.SelectedText = "";
            this.txtODesignacao.Size = new System.Drawing.Size(336, 23);
            this.txtODesignacao.TabIndex = 5;
            this.txtODesignacao.UseSelectable = true;
            // 
            // txtOCodigo
            // 
            this.txtOCodigo.Lines = new string[0];
            this.txtOCodigo.Location = new System.Drawing.Point(340, 65);
            this.txtOCodigo.MaxLength = 32767;
            this.txtOCodigo.Name = "txtOCodigo";
            this.txtOCodigo.PasswordChar = '\0';
            this.txtOCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtOCodigo.SelectedText = "";
            this.txtOCodigo.Size = new System.Drawing.Size(75, 23);
            this.txtOCodigo.TabIndex = 4;
            this.txtOCodigo.UseSelectable = true;
            // 
            // metroLabel60
            // 
            this.metroLabel60.AutoSize = true;
            this.metroLabel60.Location = new System.Drawing.Point(194, 110);
            this.metroLabel60.Name = "metroLabel60";
            this.metroLabel60.Size = new System.Drawing.Size(76, 19);
            this.metroLabel60.TabIndex = 3;
            this.metroLabel60.Text = "Designação";
            // 
            // metroLabel59
            // 
            this.metroLabel59.AutoSize = true;
            this.metroLabel59.Location = new System.Drawing.Point(194, 69);
            this.metroLabel59.Name = "metroLabel59";
            this.metroLabel59.Size = new System.Drawing.Size(53, 19);
            this.metroLabel59.TabIndex = 2;
            this.metroLabel59.Text = "Código";
            // 
            // metroTabPage8
            // 
            this.metroTabPage8.Controls.Add(this.lbALAreaOrganica);
            this.metroTabPage8.Controls.Add(this.lbALLoja);
            this.metroTabPage8.Controls.Add(this.cmbALAreaOrganica);
            this.metroTabPage8.Controls.Add(this.cmbALDesignacaoLoja);
            this.metroTabPage8.Controls.Add(this.metroCheckBox13);
            this.metroTabPage8.Controls.Add(this.metroCheckBox12);
            this.metroTabPage8.Controls.Add(this.metroCheckBox11);
            this.metroTabPage8.Controls.Add(this.metroCheckBox10);
            this.metroTabPage8.Controls.Add(this.metroCheckBox9);
            this.metroTabPage8.Controls.Add(this.metroCheckBox8);
            this.metroTabPage8.Controls.Add(this.metroCheckBox7);
            this.metroTabPage8.Controls.Add(this.metroCheckBox6);
            this.metroTabPage8.Controls.Add(this.metroGrid3);
            this.metroTabPage8.Controls.Add(this.label24);
            this.metroTabPage8.Controls.Add(this.txtALDesignacaoArmazem);
            this.metroTabPage8.Controls.Add(this.txtALCodigo);
            this.metroTabPage8.Controls.Add(this.cbALProcessadorMatriz);
            this.metroTabPage8.Controls.Add(this.cbALActivo);
            this.metroTabPage8.Controls.Add(this.cbALArmazemVenda);
            this.metroTabPage8.Controls.Add(this.metroLabel68);
            this.metroTabPage8.Controls.Add(this.metroLabel67);
            this.metroTabPage8.Controls.Add(this.metroLabel66);
            this.metroTabPage8.Controls.Add(this.metroLabel65);
            this.metroTabPage8.HorizontalScrollbarBarColor = true;
            this.metroTabPage8.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage8.HorizontalScrollbarSize = 10;
            this.metroTabPage8.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage8.Name = "metroTabPage8";
            this.metroTabPage8.Size = new System.Drawing.Size(959, 508);
            this.metroTabPage8.TabIndex = 7;
            this.metroTabPage8.Text = "Armazém/Localização";
            this.metroTabPage8.VerticalScrollbarBarColor = true;
            this.metroTabPage8.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage8.VerticalScrollbarSize = 10;
            // 
            // lbALAreaOrganica
            // 
            this.lbALAreaOrganica.AutoSize = true;
            this.lbALAreaOrganica.Location = new System.Drawing.Point(644, 118);
            this.lbALAreaOrganica.Name = "lbALAreaOrganica";
            this.lbALAreaOrganica.Size = new System.Drawing.Size(21, 19);
            this.lbALAreaOrganica.TabIndex = 48;
            this.lbALAreaOrganica.Text = "m";
            // 
            // lbALLoja
            // 
            this.lbALLoja.AutoSize = true;
            this.lbALLoja.Location = new System.Drawing.Point(644, 83);
            this.lbALLoja.Name = "lbALLoja";
            this.lbALLoja.Size = new System.Drawing.Size(21, 19);
            this.lbALLoja.TabIndex = 47;
            this.lbALLoja.Text = "m";
            // 
            // cmbALAreaOrganica
            // 
            this.cmbALAreaOrganica.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbALAreaOrganica.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbALAreaOrganica.FormattingEnabled = true;
            this.cmbALAreaOrganica.IntegralHeight = false;
            this.cmbALAreaOrganica.Location = new System.Drawing.Point(337, 110);
            this.cmbALAreaOrganica.Name = "cmbALAreaOrganica";
            this.cmbALAreaOrganica.Size = new System.Drawing.Size(301, 27);
            this.cmbALAreaOrganica.TabIndex = 46;
            // 
            // cmbALDesignacaoLoja
            // 
            this.cmbALDesignacaoLoja.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbALDesignacaoLoja.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbALDesignacaoLoja.FormattingEnabled = true;
            this.cmbALDesignacaoLoja.IntegralHeight = false;
            this.cmbALDesignacaoLoja.Location = new System.Drawing.Point(337, 75);
            this.cmbALDesignacaoLoja.Name = "cmbALDesignacaoLoja";
            this.cmbALDesignacaoLoja.Size = new System.Drawing.Size(301, 27);
            this.cmbALDesignacaoLoja.TabIndex = 45;
            // 
            // metroCheckBox13
            // 
            this.metroCheckBox13.AutoSize = true;
            this.metroCheckBox13.Location = new System.Drawing.Point(693, 254);
            this.metroCheckBox13.Name = "metroCheckBox13";
            this.metroCheckBox13.Size = new System.Drawing.Size(32, 15);
            this.metroCheckBox13.TabIndex = 22;
            this.metroCheckBox13.Text = "H";
            this.metroCheckBox13.UseSelectable = true;
            // 
            // metroCheckBox12
            // 
            this.metroCheckBox12.AutoSize = true;
            this.metroCheckBox12.Location = new System.Drawing.Point(619, 252);
            this.metroCheckBox12.Name = "metroCheckBox12";
            this.metroCheckBox12.Size = new System.Drawing.Size(31, 15);
            this.metroCheckBox12.TabIndex = 21;
            this.metroCheckBox12.Text = "G";
            this.metroCheckBox12.UseSelectable = true;
            // 
            // metroCheckBox11
            // 
            this.metroCheckBox11.AutoSize = true;
            this.metroCheckBox11.Location = new System.Drawing.Point(548, 252);
            this.metroCheckBox11.Name = "metroCheckBox11";
            this.metroCheckBox11.Size = new System.Drawing.Size(29, 15);
            this.metroCheckBox11.TabIndex = 20;
            this.metroCheckBox11.Text = "F";
            this.metroCheckBox11.UseSelectable = true;
            // 
            // metroCheckBox10
            // 
            this.metroCheckBox10.AutoSize = true;
            this.metroCheckBox10.Location = new System.Drawing.Point(475, 250);
            this.metroCheckBox10.Name = "metroCheckBox10";
            this.metroCheckBox10.Size = new System.Drawing.Size(29, 15);
            this.metroCheckBox10.TabIndex = 19;
            this.metroCheckBox10.Text = "E";
            this.metroCheckBox10.UseSelectable = true;
            // 
            // metroCheckBox9
            // 
            this.metroCheckBox9.AutoSize = true;
            this.metroCheckBox9.Location = new System.Drawing.Point(409, 250);
            this.metroCheckBox9.Name = "metroCheckBox9";
            this.metroCheckBox9.Size = new System.Drawing.Size(31, 15);
            this.metroCheckBox9.TabIndex = 18;
            this.metroCheckBox9.Text = "D";
            this.metroCheckBox9.UseSelectable = true;
            // 
            // metroCheckBox8
            // 
            this.metroCheckBox8.AutoSize = true;
            this.metroCheckBox8.Location = new System.Drawing.Point(331, 252);
            this.metroCheckBox8.Name = "metroCheckBox8";
            this.metroCheckBox8.Size = new System.Drawing.Size(31, 15);
            this.metroCheckBox8.TabIndex = 17;
            this.metroCheckBox8.Text = "C";
            this.metroCheckBox8.UseSelectable = true;
            // 
            // metroCheckBox7
            // 
            this.metroCheckBox7.AutoSize = true;
            this.metroCheckBox7.Location = new System.Drawing.Point(263, 252);
            this.metroCheckBox7.Name = "metroCheckBox7";
            this.metroCheckBox7.Size = new System.Drawing.Size(30, 15);
            this.metroCheckBox7.TabIndex = 16;
            this.metroCheckBox7.Text = "B";
            this.metroCheckBox7.UseSelectable = true;
            // 
            // metroCheckBox6
            // 
            this.metroCheckBox6.AutoSize = true;
            this.metroCheckBox6.Location = new System.Drawing.Point(190, 252);
            this.metroCheckBox6.Name = "metroCheckBox6";
            this.metroCheckBox6.Size = new System.Drawing.Size(31, 15);
            this.metroCheckBox6.TabIndex = 15;
            this.metroCheckBox6.Text = "A";
            this.metroCheckBox6.UseSelectable = true;
            // 
            // metroGrid3
            // 
            this.metroGrid3.AllowUserToResizeRows = false;
            this.metroGrid3.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.metroGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.A,
            this.B,
            this.C,
            this.D,
            this.E,
            this.F,
            this.G,
            this.H});
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid3.DefaultCellStyle = dataGridViewCellStyle17;
            this.metroGrid3.EnableHeadersVisualStyles = false;
            this.metroGrid3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid3.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid3.Location = new System.Drawing.Point(147, 279);
            this.metroGrid3.Name = "metroGrid3";
            this.metroGrid3.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid3.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.metroGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid3.Size = new System.Drawing.Size(618, 213);
            this.metroGrid3.TabIndex = 14;
            // 
            // A
            // 
            this.A.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.A.HeaderText = "A";
            this.A.Name = "A";
            // 
            // B
            // 
            this.B.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.B.HeaderText = "B";
            this.B.Name = "B";
            // 
            // C
            // 
            this.C.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.C.HeaderText = "C";
            this.C.Name = "C";
            // 
            // D
            // 
            this.D.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.D.HeaderText = "D";
            this.D.Name = "D";
            // 
            // E
            // 
            this.E.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.E.HeaderText = "E";
            this.E.Name = "E";
            // 
            // F
            // 
            this.F.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.F.HeaderText = "F";
            this.F.Name = "F";
            // 
            // G
            // 
            this.G.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.G.HeaderText = "G";
            this.G.Name = "G";
            // 
            // H
            // 
            this.H.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.H.HeaderText = "H";
            this.H.Name = "H";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(144, 219);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(131, 13);
            this.label24.TabIndex = 13;
            this.label24.Text = "Matriz de Localização";
            // 
            // txtALDesignacaoArmazem
            // 
            this.txtALDesignacaoArmazem.Lines = new string[0];
            this.txtALDesignacaoArmazem.Location = new System.Drawing.Point(337, 44);
            this.txtALDesignacaoArmazem.MaxLength = 32767;
            this.txtALDesignacaoArmazem.Name = "txtALDesignacaoArmazem";
            this.txtALDesignacaoArmazem.PasswordChar = '\0';
            this.txtALDesignacaoArmazem.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtALDesignacaoArmazem.SelectedText = "";
            this.txtALDesignacaoArmazem.Size = new System.Drawing.Size(301, 23);
            this.txtALDesignacaoArmazem.TabIndex = 10;
            this.txtALDesignacaoArmazem.UseSelectable = true;
            // 
            // txtALCodigo
            // 
            this.txtALCodigo.Lines = new string[0];
            this.txtALCodigo.Location = new System.Drawing.Point(337, 15);
            this.txtALCodigo.MaxLength = 32767;
            this.txtALCodigo.Name = "txtALCodigo";
            this.txtALCodigo.PasswordChar = '\0';
            this.txtALCodigo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtALCodigo.SelectedText = "";
            this.txtALCodigo.Size = new System.Drawing.Size(75, 23);
            this.txtALCodigo.TabIndex = 9;
            this.txtALCodigo.UseSelectable = true;
            // 
            // cbALProcessadorMatriz
            // 
            this.cbALProcessadorMatriz.AutoSize = true;
            this.cbALProcessadorMatriz.Location = new System.Drawing.Point(147, 192);
            this.cbALProcessadorMatriz.Name = "cbALProcessadorMatriz";
            this.cbALProcessadorMatriz.Size = new System.Drawing.Size(173, 15);
            this.cbALProcessadorMatriz.TabIndex = 8;
            this.cbALProcessadorMatriz.Text = "Processar Matriz Localização";
            this.cbALProcessadorMatriz.UseSelectable = true;
            // 
            // cbALActivo
            // 
            this.cbALActivo.AutoSize = true;
            this.cbALActivo.Location = new System.Drawing.Point(147, 171);
            this.cbALActivo.Name = "cbALActivo";
            this.cbALActivo.Size = new System.Drawing.Size(57, 15);
            this.cbALActivo.TabIndex = 7;
            this.cbALActivo.Text = "Activo";
            this.cbALActivo.UseSelectable = true;
            // 
            // cbALArmazemVenda
            // 
            this.cbALArmazemVenda.AutoSize = true;
            this.cbALArmazemVenda.Location = new System.Drawing.Point(147, 150);
            this.cbALArmazemVenda.Name = "cbALArmazemVenda";
            this.cbALArmazemVenda.Size = new System.Drawing.Size(131, 15);
            this.cbALArmazemVenda.TabIndex = 6;
            this.cbALArmazemVenda.Text = "Armazém de Venda?";
            this.cbALArmazemVenda.UseSelectable = true;
            // 
            // metroLabel68
            // 
            this.metroLabel68.AutoSize = true;
            this.metroLabel68.Location = new System.Drawing.Point(147, 118);
            this.metroLabel68.Name = "metroLabel68";
            this.metroLabel68.Size = new System.Drawing.Size(95, 19);
            this.metroLabel68.TabIndex = 5;
            this.metroLabel68.Text = "Área Orgânica";
            // 
            // metroLabel67
            // 
            this.metroLabel67.AutoSize = true;
            this.metroLabel67.Location = new System.Drawing.Point(147, 83);
            this.metroLabel67.Name = "metroLabel67";
            this.metroLabel67.Size = new System.Drawing.Size(123, 19);
            this.metroLabel67.TabIndex = 4;
            this.metroLabel67.Text = "Designação da Loja";
            // 
            // metroLabel66
            // 
            this.metroLabel66.AutoSize = true;
            this.metroLabel66.Location = new System.Drawing.Point(147, 48);
            this.metroLabel66.Name = "metroLabel66";
            this.metroLabel66.Size = new System.Drawing.Size(158, 19);
            this.metroLabel66.TabIndex = 3;
            this.metroLabel66.Text = "Designação do Armazém";
            // 
            // metroLabel65
            // 
            this.metroLabel65.AutoSize = true;
            this.metroLabel65.Location = new System.Drawing.Point(147, 19);
            this.metroLabel65.Name = "metroLabel65";
            this.metroLabel65.Size = new System.Drawing.Size(53, 19);
            this.metroLabel65.TabIndex = 2;
            this.metroLabel65.Text = "Código";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btNovo,
            this.btGravar,
            this.btConsultar,
            this.btSair});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(244, 25);
            this.toolStrip1.TabIndex = 14;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btNovo
            // 
            this.btNovo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btNovo.Image = ((System.Drawing.Image)(resources.GetObject("btNovo.Image")));
            this.btNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(56, 22);
            this.btNovo.Text = "Novo";
            this.btNovo.Click += new System.EventHandler(this.btNovo_Click);
            // 
            // btGravar
            // 
            this.btGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btGravar.Image = ((System.Drawing.Image)(resources.GetObject("btGravar.Image")));
            this.btGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(61, 22);
            this.btGravar.Text = "Gravar";
            this.btGravar.Click += new System.EventHandler(this.btGravar_Click);
            // 
            // btConsultar
            // 
            this.btConsultar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btConsultar.Image = ((System.Drawing.Image)(resources.GetObject("btConsultar.Image")));
            this.btConsultar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btConsultar.Name = "btConsultar";
            this.btConsultar.Size = new System.Drawing.Size(78, 22);
            this.btConsultar.Text = "Consultar";
            this.btConsultar.Click += new System.EventHandler(this.btConsultar_Click);
            // 
            // btSair
            // 
            this.btSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btSair.Image = ((System.Drawing.Image)(resources.GetObject("btSair.Image")));
            this.btSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(46, 22);
            this.btSair.Text = "Sair";
            this.btSair.Click += new System.EventHandler(this.btSair_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(276, 2);
            this.toolStripContainer1.Location = new System.Drawing.Point(372, 63);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(276, 27);
            this.toolStripContainer1.TabIndex = 15;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            this.toolStripContainer1.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer1.TopToolStripPanel.Tag = "XC";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // cmbALoja1
            // 
            this.cmbALoja1.FormattingEnabled = true;
            this.cmbALoja1.IntegralHeight = false;
            this.cmbALoja1.ItemHeight = 23;
            this.cmbALoja1.Location = new System.Drawing.Point(121, 7);
            this.cmbALoja1.Name = "cmbALoja1";
            this.cmbALoja1.Size = new System.Drawing.Size(273, 29);
            this.cmbALoja1.TabIndex = 115;
            this.cmbALoja1.UseSelectable = true;
            this.cmbALoja1.DropDown += new System.EventHandler(this.cmbALoja1_DropDown);
            this.cmbALoja1.SelectedIndexChanged += new System.EventHandler(this.cmbALoja1_SelectedIndexChanged);
            // 
            // cmbAMercadoria1
            // 
            this.cmbAMercadoria1.FormattingEnabled = true;
            this.cmbAMercadoria1.IntegralHeight = false;
            this.cmbAMercadoria1.ItemHeight = 23;
            this.cmbAMercadoria1.Location = new System.Drawing.Point(121, 40);
            this.cmbAMercadoria1.Name = "cmbAMercadoria1";
            this.cmbAMercadoria1.Size = new System.Drawing.Size(273, 29);
            this.cmbAMercadoria1.TabIndex = 116;
            this.cmbAMercadoria1.UseSelectable = true;
            this.cmbAMercadoria1.DropDown += new System.EventHandler(this.cmbAMercadoria1_DropDown);
            this.cmbAMercadoria1.SelectedIndexChanged += new System.EventHandler(this.cmbAMercadoria1_SelectedIndexChanged);
            // 
            // cmbAFamilia1
            // 
            this.cmbAFamilia1.FormattingEnabled = true;
            this.cmbAFamilia1.IntegralHeight = false;
            this.cmbAFamilia1.ItemHeight = 23;
            this.cmbAFamilia1.Location = new System.Drawing.Point(121, 102);
            this.cmbAFamilia1.Name = "cmbAFamilia1";
            this.cmbAFamilia1.Size = new System.Drawing.Size(273, 29);
            this.cmbAFamilia1.TabIndex = 117;
            this.cmbAFamilia1.UseSelectable = true;
            this.cmbAFamilia1.DropDown += new System.EventHandler(this.cmbAFamilia1_DropDown);
            this.cmbAFamilia1.SelectedIndexChanged += new System.EventHandler(this.cmbAFamilia1_SelectedIndexChanged);
            // 
            // cmbASubFamilia1
            // 
            this.cmbASubFamilia1.FormattingEnabled = true;
            this.cmbASubFamilia1.IntegralHeight = false;
            this.cmbASubFamilia1.ItemHeight = 23;
            this.cmbASubFamilia1.Location = new System.Drawing.Point(121, 137);
            this.cmbASubFamilia1.Name = "cmbASubFamilia1";
            this.cmbASubFamilia1.Size = new System.Drawing.Size(273, 29);
            this.cmbASubFamilia1.TabIndex = 118;
            this.cmbASubFamilia1.UseSelectable = true;
            this.cmbASubFamilia1.DropDown += new System.EventHandler(this.cmbASubFamilia1_DropDown);
            this.cmbASubFamilia1.SelectedIndexChanged += new System.EventHandler(this.cmbASubFamilia1_SelectedIndexChanged);
            // 
            // cmbAGrupoWeb1
            // 
            this.cmbAGrupoWeb1.FormattingEnabled = true;
            this.cmbAGrupoWeb1.IntegralHeight = false;
            this.cmbAGrupoWeb1.ItemHeight = 23;
            this.cmbAGrupoWeb1.Location = new System.Drawing.Point(121, 232);
            this.cmbAGrupoWeb1.Name = "cmbAGrupoWeb1";
            this.cmbAGrupoWeb1.Size = new System.Drawing.Size(273, 29);
            this.cmbAGrupoWeb1.TabIndex = 119;
            this.cmbAGrupoWeb1.UseSelectable = true;
            this.cmbAGrupoWeb1.DropDown += new System.EventHandler(this.cmbAGrupoWeb1_DropDown);
            this.cmbAGrupoWeb1.SelectedIndexChanged += new System.EventHandler(this.cmbAGrupoWeb1_SelectedIndexChanged);
            // 
            // cmbAUnidade
            // 
            this.cmbAUnidade.FormattingEnabled = true;
            this.cmbAUnidade.IntegralHeight = false;
            this.cmbAUnidade.ItemHeight = 23;
            this.cmbAUnidade.Items.AddRange(new object[] {
            "CM",
            "CM²",
            "CM³",
            "KG",
            "LT",
            "M",
            "M²",
            "M³",
            "UN"});
            this.cmbAUnidade.Location = new System.Drawing.Point(134, 267);
            this.cmbAUnidade.Name = "cmbAUnidade";
            this.cmbAUnidade.Size = new System.Drawing.Size(103, 29);
            this.cmbAUnidade.TabIndex = 120;
            this.cmbAUnidade.UseSelectable = true;
            // 
            // cmbADesigEmbalagem
            // 
            this.cmbADesigEmbalagem.FormattingEnabled = true;
            this.cmbADesigEmbalagem.IntegralHeight = false;
            this.cmbADesigEmbalagem.ItemHeight = 23;
            this.cmbADesigEmbalagem.Items.AddRange(new object[] {
            "CAIXA",
            "SACO"});
            this.cmbADesigEmbalagem.Location = new System.Drawing.Point(134, 302);
            this.cmbADesigEmbalagem.Name = "cmbADesigEmbalagem";
            this.cmbADesigEmbalagem.Size = new System.Drawing.Size(103, 29);
            this.cmbADesigEmbalagem.TabIndex = 121;
            this.cmbADesigEmbalagem.UseSelectable = true;
            // 
            // lbAGrupo
            // 
            this.lbAGrupo.AutoSize = true;
            this.lbAGrupo.Location = new System.Drawing.Point(400, 239);
            this.lbAGrupo.Name = "lbAGrupo";
            this.lbAGrupo.Size = new System.Drawing.Size(0, 0);
            this.lbAGrupo.TabIndex = 122;
            // 
            // FormTabelas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 652);
            this.ControlBox = false;
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.TabTabelas);
            this.Name = "FormTabelas";
            this.Text = "Tabelas";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormTabelas_Load);
            this.TabTabelas.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLImagem)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            this.metroTabPage3.ResumeLayout(false);
            this.metroTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFImagem)).EndInit();
            this.metroTabPage4.ResumeLayout(false);
            this.metroTabPage4.PerformLayout();
            this.metroTabPage5.ResumeLayout(false);
            this.metroTabPage5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.metroTabControl2.ResumeLayout(false);
            this.metroTabPage9.ResumeLayout(false);
            this.metroTabPage9.PerformLayout();
            this.metroTabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.metroTabPage11.ResumeLayout(false);
            this.metroTabPage11.PerformLayout();
            this.metroTabPage12.ResumeLayout(false);
            this.metroTabPage12.PerformLayout();
            this.metroTabPage13.ResumeLayout(false);
            this.metroTabPage13.PerformLayout();
            this.metroTabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid2)).EndInit();
            this.metroTabPage6.ResumeLayout(false);
            this.metroTabPage6.PerformLayout();
            this.metroTabPage8.ResumeLayout(false);
            this.metroTabPage8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid3)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl TabTabelas;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroTabPage metroTabPage4;
        private MetroFramework.Controls.MetroTabPage metroTabPage5;
        private MetroFramework.Controls.MetroTabPage metroTabPage6;
        private MetroFramework.Controls.MetroTabPage metroTabPage8;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox5;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox4;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox3;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox2;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox1;
        private MetroFramework.Controls.MetroTextBox txtLPraso2;
        private MetroFramework.Controls.MetroTextBox txtLPraso;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox txtLFax;
        private MetroFramework.Controls.MetroTextBox txtLTelefone;
        private MetroFramework.Controls.MetroTextBox txtLCodigoPostal;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox txtLMarada;
        private MetroFramework.Controls.MetroComboBox cmbLEmpresa;
        private MetroFramework.Controls.MetroTextBox txtLDesignacao;
        private MetroFramework.Controls.MetroTextBox txtLCodigo;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btNovo;
        private System.Windows.Forms.ToolStripButton btGravar;
        private System.Windows.Forms.ToolStripButton btConsultar;
        private System.Windows.Forms.ToolStripButton btSair;
        private System.Windows.Forms.PictureBox pbLImagem;
        private MetroFramework.Controls.MetroTextBox txtLContaPa;
        private MetroFramework.Controls.MetroTextBox txtLContaOutros;
        private MetroFramework.Controls.MetroTextBox txtLContaNacional;
        private MetroFramework.Controls.MetroLabel txtLContaPadrao;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private MetroFramework.Controls.MetroCheckBox cbMEmitirRI;
        private MetroFramework.Controls.MetroCheckBox cbMActivo;
        private MetroFramework.Controls.MetroCheckBox cbMMercadoriaRemoto;
        private MetroFramework.Controls.MetroCheckBox cbMMercadoriaVenda;
        private MetroFramework.Controls.MetroComboBox cmbMAreaOrganica;
        private MetroFramework.Controls.MetroComboBox cmbMDesignacaoLoja;
        private MetroFramework.Controls.MetroTextBox txtMContaCustos;
        private MetroFramework.Controls.MetroTextBox txtMContaVendas;
        private MetroFramework.Controls.MetroTextBox txtMDesignacao;
        private MetroFramework.Controls.MetroTextBox txtMCodigo;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pbFImagem;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private MetroFramework.Controls.MetroTextBox txtFPrazo;
        private MetroFramework.Controls.MetroTextBox txtFDesconto;
        private MetroFramework.Controls.MetroTextBox txtFDesignacao;
        private MetroFramework.Controls.MetroTextBox txtFCodigo;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private MetroFramework.Controls.MetroTextBox txtSDesignacao;
        private MetroFramework.Controls.MetroTextBox txtSCodigo;
        private MetroFramework.Controls.MetroLabel metroLabel25;
        private MetroFramework.Controls.MetroLabel metroLabel24;
        private MetroFramework.Controls.MetroLabel metroLabel23;
        private MetroFramework.Controls.MetroCheckBox cbABookingHotel;
        private MetroFramework.Controls.MetroCheckBox cbATermoGarantia;
        private MetroFramework.Controls.MetroCheckBox cbAMostraImagem;
        private MetroFramework.Controls.MetroCheckBox cbAPromocao;
        private MetroFramework.Controls.MetroCheckBox cbAPermiteOBS;
        private MetroFramework.Controls.MetroCheckBox cbAActivo;
        private MetroFramework.Controls.MetroCheckBox cbARegistoEquipamento;
        private MetroFramework.Controls.MetroCheckBox cbAContabilizarStock;
        private MetroFramework.Controls.MetroCheckBox cbAPermiteAlterar;
        private MetroFramework.Controls.MetroTabControl metroTabControl2;
        private MetroFramework.Controls.MetroTabPage metroTabPage9;
        private MetroFramework.Controls.MetroTextBox metroTextBox15;
        private MetroFramework.Controls.MetroTextBox metroTextBox14;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox15;
        private MetroFramework.Controls.MetroTextBox metroTextBox13;
        private MetroFramework.Controls.MetroTextBox metroTextBox12;
        private MetroFramework.Controls.MetroTextBox metroTextBox10;
        private MetroFramework.Controls.MetroTextBox txtAPCM;
        private MetroFramework.Controls.MetroTextBox txtAPCP;
        private MetroFramework.Controls.MetroTextBox txtAFOB;
        private MetroFramework.Controls.MetroLabel metroLabel43;
        private MetroFramework.Controls.MetroLabel metroLabel44;
        private MetroFramework.Controls.MetroLabel metroLabel45;
        private MetroFramework.Controls.MetroLabel metroLabel42;
        private MetroFramework.Controls.MetroLabel metroLabel41;
        private MetroFramework.Controls.MetroLabel metroLabel40;
        private MetroFramework.Controls.MetroTabPage metroTabPage10;
        private MetroFramework.Controls.MetroTabPage metroTabPage11;
        private MetroFramework.Controls.MetroTabPage metroTabPage12;
        private MetroFramework.Controls.MetroTabPage metroTabPage13;
        private MetroFramework.Controls.MetroTabPage metroTabPage14;
        private MetroFramework.Controls.MetroTextBox txtANorme;
        private MetroFramework.Controls.MetroTextBox txtALocal;
        private MetroFramework.Controls.MetroTextBox txtQdtEmbalagem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroLabel metroLabel38;
        private MetroFramework.Controls.MetroLabel metroLabel37;
        private MetroFramework.Controls.MetroLabel metroLabel36;
        private MetroFramework.Controls.MetroLabel metroLabel35;
        private MetroFramework.Controls.MetroLabel metroLabel34;
        private MetroFramework.Controls.MetroLabel metroLabel33;
        private MetroFramework.Controls.MetroLabel metroLabel32;
        private System.Windows.Forms.RichTextBox txtADesignacapSuplementar;
        private MetroFramework.Controls.MetroTextBox txtADesignacaoII;
        private MetroFramework.Controls.MetroTextBox txtADesignacao;
        private MetroFramework.Controls.MetroTextBox txtAReferencia;
        private MetroFramework.Controls.MetroLabel metroLabel31;
        private MetroFramework.Controls.MetroLabel metroLabel30;
        private MetroFramework.Controls.MetroLabel metroLabel29;
        private MetroFramework.Controls.MetroLabel metroLabel28;
        private MetroFramework.Controls.MetroLabel metroLabel27;
        private MetroFramework.Controls.MetroLabel metroLabel26;
        private MetroFramework.Controls.MetroLabel Loja;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Marca;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumeroSerie;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroTextBox metroTextBox18;
        private MetroFramework.Controls.MetroTextBox metroTextBox19;
        private MetroFramework.Controls.MetroLabel metroLabel48;
        private MetroFramework.Controls.MetroLabel metroLabel49;
        private MetroFramework.Controls.MetroTextBox metroTextBox17;
        private MetroFramework.Controls.MetroTextBox metroTextBox16;
        private MetroFramework.Controls.MetroLabel metroLabel47;
        private MetroFramework.Controls.MetroLabel metroLabel46;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox17;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox16;
        private MetroFramework.Controls.MetroTextBox txtADesconto;
        private MetroFramework.Controls.MetroLabel metroLabel54;
        private MetroFramework.Controls.MetroTextBox metroTextBox23;
        private MetroFramework.Controls.MetroTextBox txtALucro;
        private MetroFramework.Controls.MetroTextBox metroTextBox21;
        private MetroFramework.Controls.MetroTextBox txtAPVP;
        private MetroFramework.Controls.MetroLabel metroLabel53;
        private MetroFramework.Controls.MetroLabel metroLabel52;
        private MetroFramework.Controls.MetroLabel metroLabel51;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private MetroFramework.Controls.MetroLabel metroLabel50;
        private MetroFramework.Controls.MetroTextBox txtAStockMimimo;
        private MetroFramework.Controls.MetroTextBox txtAStockMaximo;
        private MetroFramework.Controls.MetroTextBox txtAPontoEncomenda;
        private MetroFramework.Controls.MetroTextBox txtAStockReal;
        private MetroFramework.Controls.MetroLabel metroLabel58;
        private MetroFramework.Controls.MetroLabel metroLabel57;
        private MetroFramework.Controls.MetroLabel metroLabel56;
        private MetroFramework.Controls.MetroLabel metroLabel55;
        private MetroFramework.Controls.MetroGrid metroGrid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private MetroFramework.Controls.MetroTextBox txtODesignacao;
        private MetroFramework.Controls.MetroTextBox txtOCodigo;
        private MetroFramework.Controls.MetroLabel metroLabel60;
        private MetroFramework.Controls.MetroLabel metroLabel59;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox13;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox12;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox11;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox10;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox9;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox8;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox7;
        private MetroFramework.Controls.MetroCheckBox metroCheckBox6;
        private MetroFramework.Controls.MetroGrid metroGrid3;
        private System.Windows.Forms.DataGridViewTextBoxColumn A;
        private System.Windows.Forms.DataGridViewTextBoxColumn B;
        private System.Windows.Forms.DataGridViewTextBoxColumn C;
        private System.Windows.Forms.DataGridViewTextBoxColumn D;
        private System.Windows.Forms.DataGridViewTextBoxColumn E;
        private System.Windows.Forms.DataGridViewTextBoxColumn F;
        private System.Windows.Forms.DataGridViewTextBoxColumn G;
        private System.Windows.Forms.DataGridViewTextBoxColumn H;
        private System.Windows.Forms.Label label24;
        private MetroFramework.Controls.MetroTextBox txtALDesignacaoArmazem;
        private MetroFramework.Controls.MetroTextBox txtALCodigo;
        private MetroFramework.Controls.MetroCheckBox cbALProcessadorMatriz;
        private MetroFramework.Controls.MetroCheckBox cbALActivo;
        private MetroFramework.Controls.MetroCheckBox cbALArmazemVenda;
        private MetroFramework.Controls.MetroLabel metroLabel68;
        private MetroFramework.Controls.MetroLabel metroLabel67;
        private MetroFramework.Controls.MetroLabel metroLabel66;
        private MetroFramework.Controls.MetroLabel metroLabel65;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private MetroFramework.Controls.MetroLabel cbCodEmpresa;
        private MetroFramework.Controls.MetroLabel lbLoja;
        private MetroFramework.Controls.MetroLabel lbArea;
        private MetroFramework.Controls.MetroCheckBox cbFContabilizacaoCusto;
        private MetroFramework.Controls.MetroCheckBox cbFContabilizacaoStock;
        private MetroFramework.Controls.MetroCheckBox cbFContabilizacaoVenda;
        private MetroFramework.Controls.MetroLabel lbASfamilia;
        private MetroFramework.Controls.MetroLabel lbAFamilia;
        private MetroFramework.Controls.MetroLabel lbAMercadoria;
        private MetroFramework.Controls.MetroLabel lbALoja;
        private System.Windows.Forms.ComboBox cmbALAreaOrganica;
        private System.Windows.Forms.ComboBox cmbALDesignacaoLoja;
        private MetroFramework.Controls.MetroLabel lbSFamilia;
        private MetroFramework.Controls.MetroLabel lbALAreaOrganica;
        private MetroFramework.Controls.MetroLabel lbALLoja;
        private MetroFramework.Controls.MetroLabel lbASubFam;
        private MetroFramework.Controls.MetroLabel lbAFam;
        private MetroFramework.Controls.MetroLabel lbAmerc;
        private MetroFramework.Controls.MetroLabel lbALoj;
        private System.Windows.Forms.GroupBox groupBox2;
        private MetroFramework.Controls.MetroRadioButton rbAImposto10;
        private MetroFramework.Controls.MetroRadioButton rbAInsento;
        private MetroFramework.Controls.MetroRadioButton rbAImposto5;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroLabel metroLabel70;
        private MetroFramework.Controls.MetroLabel metroLabel69;
        private MetroFramework.Controls.MetroLabel metroLabel72;
        private MetroFramework.Controls.MetroTextBox txtAPercetagemQeuebra;
        private MetroFramework.Controls.MetroLabel metroLabel71;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btAtualizar;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private MetroFramework.Controls.MetroComboBox cmbSFamilia2;
        private System.Windows.Forms.MaskedTextBox txtADataFabrico;
        private System.Windows.Forms.MaskedTextBox txtADataExpiracao;
        private MetroFramework.Controls.MetroComboBox cmbALoja1;
        private MetroFramework.Controls.MetroComboBox cmbAMercadoria1;
        private MetroFramework.Controls.MetroComboBox cmbAFamilia1;
        private MetroFramework.Controls.MetroComboBox cmbASubFamilia1;
        private MetroFramework.Controls.MetroComboBox cmbAGrupoWeb1;
        private MetroFramework.Controls.MetroComboBox cmbAUnidade;
        private MetroFramework.Controls.MetroComboBox cmbADesigEmbalagem;
        private MetroFramework.Controls.MetroLabel lbAGrupo;
    }
}