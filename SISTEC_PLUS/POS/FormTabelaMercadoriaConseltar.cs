﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS;
using SISTEC_PLUS;
using SISTEC_PLUS.OPS;

namespace SISTEC_PLUS.POS
{
    public partial class FormTabelaMercadoriaConseltar : MetroFramework.Forms.MetroForm
    {
        string codLoja = "%", mercVenda;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao); // conexão com o banco
        public FormTabelaMercadoriaConseltar()
        {
            InitializeComponent();
        }
        private void prenseCombo()
        {
            SqlCommand cmd = new SqlCommand(@"SELECT RTRIM(LTRIM(NomeLoja)) FROM ASLOJA WITH (NOLOCK)", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbLoja.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex){MessageBox.Show(ex.Message);}
            finally{conexao.Close();}
        }
        private void prenxeGrid()
        {
            if (rbSim.Checked) mercVenda = "S";
            else if (rbNao.Checked) mercVenda = "N";
            else mercVenda = "%";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(@"select CodArmz, NomeArz, CodLoja, Morada, ContVend, ContCusto from ASARMAZ where CodArmz like  '%" + txtCodigo.Text + "%' AND NomeArz like '%" + txtNome.Text + "%' AND ARMA_VENDA like '" + mercVenda + "' AND CodLoja like '"+codLoja+"'", conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("Armazem");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void gridDuploClick(DataGridView dataGrid)
        {
            FormTabelas.Mcodigo = dataGrid.CurrentRow.Cells[0].Value.ToString();
            FormTabelas.Mdesignacao = dataGrid.CurrentRow.Cells[1].Value.ToString();
            FormTabelas.Mloja = dataGrid.CurrentRow.Cells[2].Value.ToString();
            FormTabelas.MareaO = dataGrid.CurrentRow.Cells[3].Value.ToString();
            FormTabelas.McontaV = dataGrid.CurrentRow.Cells[4].Value.ToString();
            FormTabelas.McontaC = dataGrid.CurrentRow.Cells[5].Value.ToString();

            SqlCommand cmd2 = new SqlCommand("select ARMA_VENDA, ARMA_REMOTO, Ativo, EMITIR_RI from ASARMAZ where CodArmz = '" + FormTabelas.Mcodigo + "' ", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd2.ExecuteReader())
            {
                while (reader.Read())
                {
                    FormTabelas.MmercadoriaV = reader["ARMA_VENDA"].ToString();
                    FormTabelas.MmercadoriaR = reader["ARMA_REMOTO"].ToString();
                    FormTabelas.Mativo = reader["Ativo"].ToString();
                    FormTabelas.Memitir = reader["EMITIR_RI"].ToString();

                }
            }
            conexao.Close();
            this.DialogResult = DialogResult.OK;
        }
        private void FormTabelaMercadoriaConseltar_Load(object sender, EventArgs e)
        {
            metroGrid1.AutoGenerateColumns = false;
            prenxeGrid();
            prenseCombo();
        }

        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    try { gridDuploClick(metroGrid1); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void cmbLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("select CodLoja from ASLOJA WHERE NomeLoja = '"+cmbLoja.Text+"'", conexao);
            try
            {
                cmd.Connection = conexao;
                conexao.Open();
                codLoja = (string)cmd.ExecuteScalar();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }

        private void cmbLoja_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void rbSim_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void rbNao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try { gridDuploClick(metroGrid1); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }
    }
}
