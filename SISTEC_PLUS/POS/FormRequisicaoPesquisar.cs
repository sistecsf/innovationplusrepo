﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormRequisicaoPesquisar : MetroFramework.Forms.MetroForm
    {
        public static string dataIniacial, dataFim, sql, tipo, status, codLoja;
        DateTime data;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public FormRequisicaoPesquisar()
        {
            InitializeComponent();
        }
        private void prenxerGrid()
        {
            sql = @"select DISTINCT CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105), R.CodDoc, R.DESCR, R.NumDoc, LTrim(RTrim(C.CodEntidade)) + ' - ' +" +
                              "LTrim(RTrim(C.NomeEntidade)) AS CLIENTE, STATUS = CASE R.Status WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO' WHEN 'F' THEN 'PAG. PARCIAL' WHEN 'J' THEN 'ADJUDICADO' WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END," +
                                "RTRIM(R.IDunico), U.NOMEUTILIZADOR, R.CodLoja from ARegDoc R WITH (NOLOCK), AFENTIDADE C WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK), ASDETALHE H with (NOLOCK) " +
                                "where R.CodDoc = '" + Gerais.PARAM_CODDOC_RI + "' AND R.CodEntidade = C.CodEntidade AND R.CodUtilizador = U.CodUtilizador ";
            if (Convert.ToInt32(Permissoes.nPrgPos) < 3)
                sql = sql + "AND R.codutilizador = '" + Variavel.codUtilizador + "'";
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                sql = sql + "AND CONVERT(DATETIME, R.DataCria, 120) > CONVERT(DATETIME, '" + dataIniacial + "', 120) AND CONVERT(DATETIME, R.DataCria, 120) <= CONVERT(DATETIME, '" + dataFim + " 18:59" + "', 120)";
            if (txtNumero.Text != "")
                sql = sql + " AND C.CodEntidade LIKE '" + txtNumero.Text + "%'";
            if (cmbLoja.Text != "" || cmbLoja.PromptText != "")
                sql = sql + " AND R.codloja = '" + codLoja + "'";
            if (txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '" + txtNome.Text + "%'";
            if (txtDoc.Text != "")
                sql = sql + " AND R.coddoc LIKE '" + txtDoc.Text + "%'";
            if (txtCodArtigo.Text != "")
                sql = sql + " And F.Referenc Like '%" + txtCodArtigo.Text + "%'";
            if (txtDescArtigo.Text != "")
                sql = sql + " AND M.DescrArtigo LIKE '%" + txtDescArtigo.Text + "%' ";
            sql = sql + " ORDER BY CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME),105), R.coddoc, NUMDOC, CLIENTE";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataAdapter.SelectCommand.CommandTimeout = 90000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void prenxerCmbLoja()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeLoja from ASLOJA with (NoLock) order by NomeLoja");
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbLoja.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void gridDuploClick(DataGridView dataGrid)
        {
            FormRequisicao.data = dataGrid.CurrentRow.Cells[0].Value.ToString();
            FormRequisicao.idunico = dataGrid.CurrentRow.Cells[6].Value.ToString();
            FormRequisicao.nome = dataGrid.CurrentRow.Cells[7].Value.ToString();
            try
            {
                SqlCommand cmd2 = new SqlCommand("select NumDoc, CodLoja from ARegDoc WITH (NOLOCK) where IDUnico = '" + FormRequisicao.idunico + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        FormRequisicao.num = "" + reader[0].ToString();
                        FormRequisicao.codLoja = reader[1].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
            SqlCommand cmd = new SqlCommand("SELECT RTRIM(LTRIM(STATUS)) From AREGDOC R where R.CODLOJA = '" + FormRequisicao.codLoja + "' And R.CODDOC = 'RI' And R.NUMDOC = '" + FormRequisicao.num + "' And LTRIM(RTRIM(R.IDunico)) = '" + FormRequisicao.idunico + "'", conexao);
            cmd.Connection = conexao;
            conexao.Open();
            try { status = (string)cmd.ExecuteScalar(); }
            catch (Exception) { }
            finally { conexao.Close(); }
            if (status == "A")
            {
                MessageBox.Show("Este Documento já esta Anulado!");
            }
            else
                this.DialogResult = DialogResult.OK;
        }
        private void FormRequisicaoPesquisar_Load(object sender, EventArgs e)
        {
            txtDataInicial.Text = txtDataFinal.Text = DateTime.Now.ToShortDateString();
            cmbLoja.PromptText = Variavel.nomeLoja;
            codLoja = Variavel.codLoja;
            prenxerCmbLoja();
        }
        private void btGravar_Click(object sender, EventArgs e)
        {
            bool a = false, b = false;
            try { data = Convert.ToDateTime(txtDataInicial.Text); a = true; }
            catch (Exception ex) { MessageBox.Show(ex.Message + " Data Invalida" +" - " + txtDataInicial.Text); }
            if (a)
            {
                dataIniacial = "" + data.Year + "-" + data.Month + "-" + data.Day;
                try { data = Convert.ToDateTime(txtDataFinal.Text); b = true; }
                catch (Exception ex) { MessageBox.Show(ex.Message + " Data Invalida" + " - " + txtDataFinal.Text); }
                if (b)
                {
                    dataFim = "" + data.Year + "-" + data.Month + "-" + data.Day;

                    metroGrid1.AutoGenerateColumns = false;
                    Cursor.Current = Cursors.WaitCursor;
                    prenxerGrid();
                }
            }
        }

        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    try { gridDuploClick(metroGrid1); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void cmbLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("select CodLoja from ASLOJA with (nolock) where NomeLoja = '"+ cmbLoja.Text +"'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codLoja = reader[0].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try { gridDuploClick(metroGrid1); }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void txtDataInicial_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtDataInicial.Text = ""; }
        }

        private void txtDataFinal_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtDataFinal.Text = ""; }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
