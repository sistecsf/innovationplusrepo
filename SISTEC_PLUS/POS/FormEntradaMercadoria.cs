﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS.Recibos;


namespace SISTEC_PLUS.POS
{
    public partial class FormEntradaMercadoria : MetroFramework.Forms.MetroForm
    {
        FormEntrad regisEq = new FormEntrad();

        public static string REFERENC, CodArmz, Qtd, fob, FOB1, MARCA, MODELO, data;
        public static string[] NSERIE, IMEI, TEL_SIM ;
        string PCA1, LOCALI, PVP, PontoEncomenda, RegEquip, Desconto, DESCRARTIGO, UNIDADE, total;
        string NUMDOC, GUIA, DescrDoc, DataCria, FOBrec, PCL, PVPrecibo, QTD, NomeCli, CodUtilizador, CodFam, CodArmzRecibo, CodLoja, Referen, LocalizaStock, IDunico, DescrArt, NLoja, InvoiceNumb;

        public static int countNS;
        int  a = 0, count, c;
        double totalFob, totalDesc, totalTotal, totalPca;


        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        //SqlConnection conexao = new SqlConnection(Variavel.Conn);
        //SqlConnection conexao = new SqlConnection(@"Data Source=sisdbs1b;Initial Catalog=INNOVUIGE;User ID=admin;Password=admin"); // conexão com o banco 
        public FormEntradaMercadoria()
        {
            InitializeComponent();
        }

        // METODO PARA CHAMAR A E PRENCHER O GRID INVISIVEL QUE FARA INSERT

        private void prenxeGridInvisivel()
        {
           // metroGrid2.Rows.Clear();
            for (int i = 0; i < countNS; i++ )
                metroGrid2.Rows.Add(NSERIE[i], IMEI[i], MARCA, MODELO, REFERENC, TEL_SIM[i], CodArmz, FOB1);
            
        }



        // CHAMANDO A PROCEDURE DO ID UNICO
        private string idUnico()
        {
            string unico;
            SqlCommand cmd = new SqlCommand("sp_GET_IDUNICO", conexao);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@p_sCodLoja", Variavel.codLoja));
            try
            {
                conexao.Open();
                unico = (string)cmd.ExecuteScalar();
                return unico;
            }
            catch (Exception ex) {
                unico = "0";
                return unico;
            }
            finally
            {
                conexao.Close();
            }
            MessageBox.Show(unico);
        }


        // FUNCAO PARA PREENCHER A TABELA  DATAGRID COM OS DADOS QUE ESTAO NA TEXT  BOX E NA COMBO BOX
        private void EntradaDataGrid()
        {
            bool b = false;
            
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                if (cmbReferencia1.Text == metroGrid1[0, i].Value.ToString())
                {
                    b = true;
                    c = i;
                }
            }

            if (b == true)
            {
                    Qtd = "" + (Convert.ToInt32(txtQdt.Text) + Convert.ToInt32(metroGrid1[2, c].Value));
                    total = "" + (Convert.ToDouble(txtTotal.Text) + Convert.ToDouble(metroGrid1[7, c].Value));
                    this.metroGrid1[2, c].Value = Qtd;
                    this.metroGrid1[7, c].Value = total;
                
            }
            else
            {

                metroGrid1.Rows.Add(cmbReferencia1.Text, UNIDADE, txtQdt.Text, CodArmz, FOB1, PCA1, txtDesc.Text, txtTotal.Text, LOCALI, txtContagem.Text);
            }
            

            totalTotal = 0;
            totalDesc = 0;
            totalFob = 0;
            totalPca = 0;

            // for para somar  as linhas dentro do grid e posteriormente mostrar nos text box em baixo
            for (int i = 0; i < metroGrid1.RowCount -1 ; i++)
            {
                totalTotal = totalTotal + Convert.ToDouble(metroGrid1[7, i].Value);
                totalDesc = totalDesc + Convert.ToDouble(metroGrid1[6, i].Value);
                totalFob = totalFob + Convert.ToDouble(metroGrid1[4, i].Value);
                totalPca = totalPca + Convert.ToDouble(metroGrid1[5, i].Value);
            }

            txtTotalValor.Text = ""+totalTotal;
            txtTotalDesconto.Text = "" + totalDesc;
            txtTotalFob.Text = "" + totalFob;
            txtTotalPca.Text = "" + totalPca;
           
        }

        // CHAMANDO A PROCEDURE DO NUMERO DO DOCUMENTO
        private string numeroDoc()
        {
            string numero;
            SqlCommand cmd = new SqlCommand("SP_GET_NUMDOC", conexao);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@p_sCodLoja", Variavel.codLoja));
             cmd.Parameters.Add(new SqlParameter("@p_sCodDoc", Gerais.PARAM_CODDOC_EM));

            try
            {
                conexao.Open();
                numero = ""+cmd.ExecuteScalar();
                return numero;
            }
            catch (Exception ex) { return "0"; }
            finally
            {
                conexao.Close();

            }
        }
      
        
        
        // CHAMANDO A PROCEDURE DO NUMERO DA GUIA DO DOCUMENTO
       
        private string guiaDoc()
        {
            string guia;
            SqlCommand cmd = new SqlCommand("sp_GET_NUMGUIAS", conexao);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@p_sCodLoja", Variavel.codLoja));
            cmd.Parameters.Add(new SqlParameter("@p_sCodDoc", Gerais.PARAM_CODDOC_EM));
            try
            {
                conexao.Open();
                guia = ""+cmd.ExecuteScalar();
                return guia;
            }
            catch (Exception ex) { return "0"; }
            finally
            {
                conexao.Close();

            }
        }



        // funcao para receber o codigo da descriçao do artigo da referencia
        private void CmbDescricaoArtigo()
        {
            SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT RTRIM(LTRIM(DESCRARTIGO)) FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA=@CodLoja AND CONTASTOCKS=@S AND ACTIVO=@S AND CONVERT(VARCHAR(10), ISNULL(DATA_EXPIRA,GETDATE()), 120) >= CONVERT(VARCHAR(10), GETDATE(), 120)", conexao);
            cmd.Parameters.Add(new SqlParameter("@CodLoja", Variavel.codLoja));
            cmd.Parameters.Add(new SqlParameter("@S", "S"));
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbDescricaoArtigo1.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }  
        }

        // funcao para ver o codigo e o nome do fonecedor 
        private void CmbFornecedor()
        {
            SqlCommand cmd = new SqlCommand(@" select NomeEntidade from AFENTIDADE where CodTipoCli = 'frn'", conexao);
            
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbFornecedor.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }

        // FUNCAO PARA PEGA A ORIGEM 
        private void CmbOrigemEntrada()
        {
            SqlCommand cmd = new SqlCommand(@"select RTRIM(LTRIM(Descricao)) from ASORIGEM WITH (NOLOCK) order by RTRIM(LTRIM(Descricao))", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbOrigemEntrada.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }


        // FUNCAO PARA PEGAR A REFERENCIA 
        private void CmbReferencia()
        {
            SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT RTRIM(LTRIM(REFERENC)) FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA=@CodLoja AND CONTASTOCKS=@S AND ACTIVO=@S AND CONVERT(VARCHAR(10), ISNULL(DATA_EXPIRA,GETDATE()), 120) >= CONVERT(VARCHAR(10), GETDATE(), 120)", conexao);
            cmd.Parameters.Add(new SqlParameter("@CodLoja", Variavel.codLoja));
            cmd.Parameters.Add(new SqlParameter("@S", "S"));
            cmd.Connection = conexao;
            //try
            //{
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbReferencia1.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            //}
            //catch (SqlException ex)
            //{
              //  MessageBox.Show(ex.Message);
            //}
            //finally
            //{
                conexao.Close();
            //}
        }

        private DateTime DataLimiteDoc()
        {
            DateTime data;
            conexao.Close();
            SqlCommand cmd = new SqlCommand(@"Select CONVERT(VARCHAR(20),GETDATE(),120)", conexao);
            cmd.Connection = conexao;
            conexao.Open();
            data = Convert.ToDateTime(cmd.ExecuteScalar());
            conexao.Close();
            return data;
        }




        // fromulario principal 
        private void FormEntradaMercadoria_Load(object sender, EventArgs e)
        {
            txtFornecedor.ReadOnly = true;
            metroGrid1.AutoGenerateColumns = false; // chamar o datagrid com autogenerate para poder editar as tabelas e as colunas
            data = ""+ DataLimiteDoc();
           

          
        }

      

     

        private void cmbFornecedor_MouseClick(object sender, MouseEventArgs e)
        {
            cmbFornecedor.Items.Clear(); // LIMPA OS DADOS DA COMBO SEMPRE QUE FAZ NOVA CONSULTA
            CmbFornecedor();             // CHAMA A FUNCAO DA COMBO 
        }


        // EVENTO QUE MANDA O VALOR DA COMBO BOX PARA O TEXT BOX
        private void cmbFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            metroGrid1.Rows.Clear();
            SqlCommand cmd1 = new SqlCommand(@"select CodEntidade from AFENTIDADE WHERE NomeEntidade=  '" + cmbFornecedor.Text + "' ");
            cmd1.Connection = conexao;
            try
             {
                conexao.Open();
                txtFornecedor.Text = (string)cmd1.ExecuteScalar();
              
             }
              catch (Exception) {  }
             finally { conexao.Close(); }
        }

        // EVENTO QUE MANDA  O CODIGO DA ORIGEM PARA O TEXTBOX
        private void cmbOrigemEntrada_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand(@"select RTRIM(LTRIM(CodOrigem)) from ASORIGEM WITH (NOLOCK) where Descricao= '" + cmbOrigemEntrada.Text + "' ");
            cmd1.Connection = conexao;
            try
            {
                conexao.Open();
                txtOrigemEntrada.Text = (string)cmd1.ExecuteScalar();
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }

        // EVENTO CLIQUE ORIGEM DE ENTRADA
        private void cmbOrigemEntrada_MouseClick(object sender, MouseEventArgs e)
        {
            cmbOrigemEntrada.Items.Clear();
            CmbOrigemEntrada();
        }

        // EVENTO CLIQUE PARA A REFERENCIA 
        private void cmbReferencia1_MouseClick_1(object sender, MouseEventArgs e)
        {
            cmbReferencia1.Items.Clear();
            CmbReferencia();
        }

        //  EVENTO CLIQUE PARA A DESCRICAO DO ARTIGO
        private void cmbDescricaoArtigo1_MouseClick_1(object sender, MouseEventArgs e)
        {
            cmbDescricaoArtigo1.Items.Clear();
            CmbDescricaoArtigo();
        }


        // EVENTO QUE MUDA A DESCRICAO E TAMBEM MUDA A REFERENCIA CONSOANTE A DESCRICAO SELECIONADA
        private void cmbDescricaoArtigo1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("SELECT FOB, PCA, LOCALI, Desconto, REFERENC, UNIDADE, CodArmz ,PVP ,PontoEncomenda,RegEquip  FROM ASMESTRE  WITH (NOLOCK) where  DESCRARTIGO = '" + cmbDescricaoArtigo1.Text + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    // COLOCANDO OS DADOS DENTRO DAS VARIAVEIS, ORGANIZADOS POR SELECT
                    FOB1 = reader[0].ToString();
                    PCA1 = reader[1].ToString();
                    LOCALI = reader[2].ToString();
                    Desconto = reader[3].ToString();
                    REFERENC = reader[4].ToString();
                    UNIDADE = reader[5].ToString();
                    CodArmz = reader[6].ToString();
                    PVP = reader[7].ToString();
                    PontoEncomenda = reader[8].ToString();
                    RegEquip = reader[9].ToString();
                }
            }

            // SELECT PARA PEGAR O CODIGO ARMAZEM
            SqlCommand comd = new SqlCommand("select CodArmz from ASMESTRE  WHERE Referenc = '" + REFERENC + "'", conexao);
            using (SqlDataReader reader = comd.ExecuteReader())
            {
                while (reader.Read())
                {
                    count = count + 1;
                }
            }
            conexao.Close();

            if (count > 1)
            {
                FormEntradaMercadoriaArmazemMercadoria form = new FormEntradaMercadoriaArmazemMercadoria();
                if (form.ShowDialog() == DialogResult.OK)
                {

                }
            }
            else
            {
                //MANDANDO OS DADOS PARA A COMBOX E PARA A TEXTBOX
                cmbReferencia1.Text = REFERENC;
                txtFOB.Text = FOB1;
                PCAP.Text = PCA1;
                cmbLocalizacao.Text = LOCALI;
                txtDesc.Text = "0";
                txtQdt.Text = "1";
                txtContagem.Text = "0";
                totalTotal = Convert.ToInt32(txtQdt.Text) * Convert.ToDouble(PCA1);
                txtTotal.Text = "" + totalTotal;
                txtMovStock.Text = "+";
              //  txtGuia.Text = (guiaDoc());
               // txtNumero.Text = (numeroDoc());
                //txtIdUnico.Text = (idUnico());
                //txtLocali.Text = LOCALI;
                Qtd = txtQdt.Text;
                txtInvoiceN.Text = "";
            }
        }


        // evento para mandar a referencia consoante a a descricao
        private void cmbReferencia1_SelectedIndexChanged(object sender, EventArgs e)
        {
            count = 0;
            SqlCommand cmd = new SqlCommand("SELECT FOB, PCA, LOCALI, Desconto, DESCRARTIGO, UNIDADE, CodArmz ,PVP ,PontoEncomenda,RegEquip FROM ASMESTRE  WITH (NOLOCK) where REFERENC = '" + cmbReferencia1.Text + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    // COLOCANDO OS DADOS DENTRO DAS VARIAVEIS , ORGANIZADOS POR SELECT
                   FOB1 = reader[0].ToString();
                    PCA1 = reader[1].ToString();
                    LOCALI = reader[2].ToString();
                    Desconto = reader[3].ToString(); 
                    DESCRARTIGO = reader[4].ToString();
                    UNIDADE = reader[5].ToString();
                    CodArmz = reader[6].ToString();
                    PVP = reader[7].ToString();
                    PontoEncomenda = reader[8].ToString();
                    RegEquip = reader[9].ToString();
                }
            }
            conexao.Close();

            // mandando os dados para as variaveis publicas
            REFERENC = cmbReferencia1.Text;
            Qtd = txtQdt.Text;
            //fob = txtFOB.Text;



            



            // SELECT PARA PEGAR O CODIGO ARMAZEM

            SqlCommand comd = new SqlCommand("select CodArmz from ASMESTRE  WHERE Referenc = '" + REFERENC + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = comd.ExecuteReader())
            {
                while (reader.Read())
                {
                    string codigo = reader[0].ToString();
                    count = count + 1;
                }
            }
            conexao.Close();

            if (count > 1)
            {
                FormEntradaMercadoriaArmazemMercadoria form = new FormEntradaMercadoriaArmazemMercadoria();
                if (form.ShowDialog() == DialogResult.OK)
                {
                    cmbDescricaoArtigo1.Text = DESCRARTIGO;
                    txtFOB.Text = FOB1;
                    PCAP.Text = PCA1;
                    cmbLocalizacao.Text = LOCALI;
                    txtDesc.Text = "0";
                    txtQdt.Text = "1";
                    txtContagem.Text = "0";
                    totalTotal = Convert.ToInt32(txtQdt.Text) * Convert.ToDouble(PCA1);
                    txtTotal.Text = "" + totalTotal;
                    txtMovStock.Text = "+";
                  //  txtGuia.Text = (guiaDoc());
                   // txtNumero.Text = (numeroDoc());
                   // txtIdUnico.Text = (idUnico());
                    txtLocali.Text = "A"+LOCALI;
                    txtInvoiceN.Text = "0";
             
                
                }
            }
            else
            {
                //MANDANDO OS DADOS PARA A COMBOX E PARA A TEXTBOX
                cmbDescricaoArtigo1.Text = DESCRARTIGO;
                txtFOB.Text = FOB1;
                PCAP.Text = PCA1;
                cmbLocalizacao.Text = LOCALI;
                txtDesc.Text = "0";
                txtQdt.Text = "1";
                txtContagem.Text = "0";
                totalTotal = Convert.ToInt32(txtQdt.Text) * Convert.ToDouble(PCA1);
                txtTotal.Text = "" + totalTotal;
                txtMovStock.Text = "+";
               // txtGuia.Text = (guiaDoc());
                //txtNumero.Text = (numeroDoc());
                //txtIdUnico.Text = (idUnico());
                txtLocali.Text = "A" + LOCALI;
                txtInvoiceN.Text = "0";
               
            }
        }



        // FUNCAO DATA DE LANCAMENTO FOMATADA COM APENAS AS DATAS
        private string dataLancamente()
        {
            SqlCommand cmd2 = new SqlCommand("select MAX(data_lancamento) from AFLANCAMENTO where CodLoja=@loja ", conexao);
            cmd2.Parameters.Add(new SqlParameter("@loja", Variavel.codLoja));
            string data = "";
            try
            {
                conexao.Open();
                data = (string)cmd2.ExecuteScalar();
            }
            catch (Exception ex) { data = "0"; }
            finally { conexao.Close(); }
            return data;
        }





        // CHAMAR A FUNCAO PARA PREENCHER O GRID QUANDO APERTA O VALOR DA QUANTIDADE 
        private void txtQdt_KeyPress(object sender, KeyPressEventArgs e)
        {


            if (e.KeyChar == (char)Keys.Enter)
                if (Convert.ToDouble(PCAP.Text) < Convert.ToDouble(txtFOB.Text) ) // comparacao entre pca e fob
                {


                    if (RegEquip == "S") // abre uma tela se o campo regequip for igaula s
                    {

                        FormEntrad form = new FormEntrad();

                        if (form.ShowDialog() == DialogResult.OK) // mostra a tema
                        {
                            EntradaDataGrid();
                            prenxeGridInvisivel();
                        }
                    }

                    else

                        EntradaDataGrid();
                }
                else MessageBox.Show("O preço [ PCA] não pode ser menor que  [ FOB]!");

        }









        // FUNCAO QUE MULTIPLICA A QUANTIDADE PELO VAOR TOTAL E CALCULA O DESCONTO
        private void txtQdt_TextChanged(object sender, EventArgs e)
        {
            Qtd = txtQdt.Text;
            try
            {
                txtTotal.Text = "" + (( Convert.ToInt32(txtQdt.Text) * Convert.ToDouble(PCA1)) - ( Convert.ToInt32(txtQdt.Text) * Convert.ToDouble(PCA1)) * Convert.ToDouble(txtDesc.Text)/100);
                totalTotal = Convert.ToDouble(txtTotal.Text);
        

            }
            catch
            {
                txtTotal.Text = "" + 0 * Convert.ToDouble(PCA1);
                totalTotal = Convert.ToDouble(txtTotal.Text);
            }
             

        }


        // FUNCAO QUE MULTIPLICA A QUANTIDADE PELO VAOR TOTAL E CALCULA O DESCONTO ( NO TEXTBOX DESCONTO)
        private void txtDesc_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTotal.Text = "" + ((Convert.ToInt32(txtQdt.Text) * Convert.ToDouble(PCA1)) - (Convert.ToInt32(txtQdt.Text) * Convert.ToDouble(PCA1)) * Convert.ToDouble(txtDesc.Text) / 100);
                totalTotal = Convert.ToDouble(txtTotal.Text);


            }
            catch
            {
                txtTotal.Text = "" + 0 * Convert.ToDouble(PCA1);
                totalTotal = Convert.ToDouble(txtTotal.Text);
            }
             
        }



        // FUNCAO PARA PEGAR O CODIGO AO
        public string codigoAo()
        {
            string codAo = "";
            SqlCommand cmd = new SqlCommand(@"SELECT CODAO FROM ASARMAZ WHERE CodArmz='" + CodArmz + "' AND CodLoja='" + Variavel.codLoja + "'", conexao);

            cmd.Connection = conexao;
            try
            {
                conexao.Open();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codAo = reader[0].ToString(); ; //Buscando dados do campo NOME


                    }

                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
            return codAo;

        }



        private void gravar()
        {

            txtCodAo.Text = (codigoAo());
            txtGuia.Text = (guiaDoc());
            txtNumero.Text = (numeroDoc());
            txtIdUnico.Text = (idUnico());
            txtLocali.Text = LOCALI;



            String precoKz = ""+Convert.ToDouble(Variavel.cambio) * Convert.ToDouble(txtTotalValor.Text);

              SqlCommand comand = new SqlCommand("INSERT INTO AregDoc(         IDUnico,                      DataCria,                                  CodUtilizador,                               CodDoc,                                CodEntidade,                            CodLoja,                               NumDoc,                                   NumFact,                                            PrecoD,                                                                                                     Preco,                                             CodOrigem,                          CodFornec,                              DATA_LANCAMENTO,                   STATUS)" +
             "values (                                                    '" + txtIdUnico.Text + "',     CONVERT(VARCHAR(20),GETDATE(),120),          '" + Variavel.codUtilizador + "',           '" + Gerais.PARAM_CODDOC_EM + "',    '" + Gerais.PARAM_CODCLI_GERAL + "',    '" + Variavel.codLoja + "',           '" + txtNumero.Text.Replace(',', '.') + "',       '" + txtNumero.Text.Replace(',', '.') + "',     '" + txtTotalValor.Text.Replace(',', '.') + "',                                                        '" + precoKz.Replace(',', '.') + "',                          '" + txtOrigemEntrada.Text + "',      '" + txtFornecedor.Text + "',          '" + dataLancamente() + "',                'E'                 )");

            conexao.Open();
            comand.Connection = conexao;
            comand.ExecuteNonQuery();
            conexao.Close();
            MessageBox.Show("Gravado com sucesso AREG");
            


                for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {


                String pvpKz, pclKz;
                pvpKz = "" + Convert.ToDouble(Variavel.cambio) * Convert.ToDouble(metroGrid1[5, i].Value.ToString()) ;
                pclKz = "" + Convert.ToDouble(Variavel.cambio) * Convert.ToDouble(metroGrid1[5, i].Value.ToString());

             SqlCommand coman = new SqlCommand("INSERT INTO ASFICMOV1 (  CODLOJA,            GUIA,                       DATACRIA,                           DATA_LANCAMENTO,                    IDUNICO,                  NUMDOC,                                                CODDOC                                    ,CODENTIDADE,                          CODUTILIZADOR,                                 CODAO,                         NUMDOCFRN,                                                CODFORNEC,                         TIPMOV,                          FOB,                                                                      PCLD,                                                                    PVP,                                                                     QTD,                                      CODARMZ,                            REFERENC,                               PCL,                                                             PVPD,                                               X1                             )" +
              "values (                                '" + Variavel.codLoja + "',    '" + txtGuia.Text + "' , CONVERT(VARCHAR(20),GETDATE(),120),     '" + dataLancamente() + "',        '" + txtIdUnico.Text + "' ,   '" + txtNumero.Text.Replace(',', '.') + "' ,                 '" + Gerais.PARAM_CODDOC_EM + "',       '" + Gerais.PARAM_CODCLI_GERAL + "',      '" + Variavel.codUtilizador + "',       '" + txtCodAo.Text + "',        '" + txtInvoiceN.Text.Replace(',', '.') + "'  ,         '" + txtFornecedor.Text + "',          '+',                    '" + metroGrid1[4, i].Value.ToString().Replace(',', '.') + "',            '" + metroGrid1[5, i].Value.ToString().Replace(',', '.') + "',           '" + pvpKz.Replace(',', '.') + "',                                  '" + metroGrid1[2, i].Value + "',          '" + metroGrid1[3, i].Value + "',                  '" + metroGrid1[0, i].Value  +"',        '" + pclKz.Replace(',', '.') + "'       ,                       '" + PVP.Replace(',', '.') + "'     ,             '" + metroGrid1[9, i].Value + "'             )");

            conexao.Open();
            coman.Connection = conexao;
            coman.ExecuteNonQuery();
            
            conexao.Close();


            }

                MessageBox.Show("Gravado com sucesso ASFICMOV");






            SqlCommand comandd = new SqlCommand(" UPDATE ASMESTRE SET LOCALI ='" + txtLocali.Text + "'  , Data_Alteracao = CONVERT(VARCHAR(20),GETDATE(),120)  Where CODLOJA = '" + Variavel.codLoja + "' And CODARMZ = '" + CodArmz + "'   And REFERENC = '" + REFERENC + " '");
            conexao.Open();
            comandd.Connection = conexao;
            comandd.ExecuteNonQuery();
            MessageBox.Show("Update feito com sucesso");
            conexao.Close();



               for (int i = 0; i < metroGrid2.RowCount - 1; i++)
            {
           
                       SqlCommand comanddd = new SqlCommand("  INSERT INTO AEFICHAS(   NSERIE,                                 FOB,                                                                                            DATACRIA,                                                    CodUtilizador,                              CodEntidade,                          CodModelo,                                            CodMarca,                                           Referenc,                                 IDUnico,                                        CODLOJA,                                 CODARMZ,                                                 IMEI_ICCID,                                             TEL_SIM)"+
                         "values (                                               '" + metroGrid2[0, i].Value + "',      '" + metroGrid2[7, i].Value.ToString().Replace(',', '.') + "',                        CONVERT(VARCHAR(20),GETDATE(),120),                       '" + Variavel.codUtilizador + "',          '" + Gerais.PARAM_CODCLI_GERAL + "',      '" + metroGrid2[3, i].Value + "',           '" + metroGrid2[2, i].Value + "',                  '" + metroGrid2[4, i].Value + "',                '" + idUnico() + "',          '" + Variavel.codLoja + "',                           '" + metroGrid2[6, i].Value + "' ,                   '" + metroGrid2[1, i].Value.ToString().Replace(',', '.') + "' ,                       '" + metroGrid2[5, i].Value + "'                  )");
                       conexao.Open();
                       comanddd.Connection = conexao;
                       comanddd.ExecuteNonQuery();
                    
                       conexao.Close();
            }
               MessageBox.Show("Gravado com sucesso AEFICHAS");





               SqlCommand coma = new SqlCommand("INSERT Into AEFICMOV (IDUNICO, CODDOC, CODMARCA, CODMODELO, NSERIE,CODLOJA, REFERENC,CODENTIDADE, DataCria, CodUtilizador) Select IDUNICO, 'EM', CODMARCA, CODMODELO, NSERIE,CODLOJA, REFERENC,CODENTIDADE, DataCria, CodUtilizador From Aefichas Where IDunico= '" + txtIdUnico.Text + "'    ");
            

               conexao.Open();
               coma.Connection = conexao;
               coma.ExecuteNonQuery();
               conexao.Close();
               MessageBox.Show("Gravado com total sucesso AEFICMOV");


                  
        }


    
        
        private void btConsultar_Click(object sender, EventArgs e)
        {
            imprimirFatura();

           // txtContagem.Text = guiaDoc



        }


      //  /*
        private void imprimirFatura()
        {

            int i = 0;
            string[] codigo = new string[metroGrid1.RowCount - 1], referencia = new string[metroGrid1.RowCount - 1], descricao = new string[metroGrid1.RowCount - 1], localizacao = new string[metroGrid1.RowCount - 1], fob = new string[metroGrid1.RowCount - 1], pca = new string[metroGrid1.RowCount - 1], pv = new string[metroGrid1.RowCount - 1], totalFob = new string[metroGrid1.RowCount - 1], nserie = new string[metroGrid1.RowCount - 1], mercadoria = new string[metroGrid1.RowCount - 1], qtd = new string[metroGrid1.RowCount - 1];

            
            SqlCommand cmd = new SqlCommand("SELECT DISTINCT LTRIM(RTRIM(F.NUMDOC)), LTRIM(RTRIM(GUIA)), D.DESCRDOC, Cast(F.DataCria As DateTime), F.FOB, F.PCLD, F.PVPD, F.QTD, E.NomeEntidade, U.NOMEUTILIZADOR, M.CodFam, AR.NomeArz, F.CodLoja, RTRIM(LTRIM(F.REFERENC)), LOCALI, F.IDunico, M.DescrArtigo, L.NomeLoja, NumDocFRN FROM ASFICMOV1 F WITH (NOLOCK), AFENTIDADE E WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), ASDOCS D WITH (NOLOCK), ASLOJA L WITH (NOLOCK), ASARMAZ AR WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK), AREGDOC R WITH (NOLOCK) " +
               " WHERE E.CODENTIDADE  =R.CODENTIDADE AND R.CODENTIDADE =F.CODENTIDADE AND D.CODDOC =R.CODDOC AND R.IDUNICO  =F.IDUNICO AND R.CODDOC  =F.CODDOC AND R.NUMDOC  =F.NUMDOC  AND L.CODLOJA =M.CODLOJA AND M.CODLOJA  =F.CODLOJA AND L.CODLOJA   =AR.CODLOJA AND AR.CODARMZ  =M.CODARMZ AND M.CODARMZ =F.CODARMZ AND M.REFERENC =F.REFERENC AND F.CODUTILIZADOR =U.CODUTILIZADOR AND F.CODDOC  = '" + Gerais.PARAM_CODDOC_EM.Replace(',', '.') + "' AND F.CODLOJA ='" + Variavel.codLoja + "' AND F.NUMDOC  ='" + txtNumero.Text.Replace(',', '.') + "' ORDER BY AR.NOMEARZ, RTRIM(LTRIM(F.REFERENC)) ");
            cmd.Connection = conexao;
            conexao.Open();   
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                
                while (reader.Read())
                {

             NUMDOC = reader[0].ToString();
            GUIA= reader[1].ToString();
            DescrDoc= reader[2].ToString(); 
            DataCria= reader[3].ToString();
            fob[i] = reader[4].ToString();
            pca[i] = reader[5].ToString();
            pv[i] = reader[6].ToString();
            qtd[i] = reader[7].ToString();
            NomeCli= reader[8].ToString();
            CodUtilizador= reader[9].ToString();
            CodFam= reader[10].ToString();
            mercadoria[i] = reader[11].ToString(); 
            CodLoja= reader[12].ToString();
            referencia[i] = reader[13].ToString();
            localizacao[i] = reader[14].ToString();
            IDunico= reader[15].ToString();
            descricao[i] = reader[16].ToString();
            NLoja = reader[17].ToString();
            InvoiceNumb = reader[18].ToString();

            totalFob[i] = reader[4].ToString();
            nserie[i] = reader[17].ToString();
            i++;
            
                }
            }

            
            cmd.ExecuteNonQuery();
            conexao.Close();
            MessageBox.Show(NUMDOC);

            /*
            MessageBox.Show(LocalizaStock);
            MessageBox.Show(IDunico);
            MessageBox.Show(NUMDOC);
            MessageBox.Show(GUIA);
            MessageBox.Show(DescrDoc);
            MessageBox.Show( DataCria);
            MessageBox.Show(FOBrec);
            MessageBox.Show(PCL);
            MessageBox.Show(PVPrecibo);
            MessageBox.Show(QTD);
            MessageBox.Show(CodArmzRecibo);

         

            

            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
          
           
          
                referencia[i] =  Referen;
                descricao[i] = DescrArt;
                localizacao[i] = LocalizaStock;
                fob[i] = FOBrec;
                pca[i] =  PCL;
                pv[i] = PVPrecibo;
               totalFob[i] = FOBrec;
                nserie[i] = NLoja;
                mercadoria[i] = CodArmzRecibo;
                qtd[i] =  QTD;
          
         
            }
               */


            DateTime Data = Convert.ToDateTime(data);
            string data1 = "" + Data.Day + "-" + Data.Month + "-" + Data.Year;
            string hora = "" + Data.Hour + ":" + Data.Minute + ":" + Data.Second;





            EntradaMercadoriaRecibo formRecibo = new EntradaMercadoriaRecibo(IDunico, GUIA, InvoiceNumb, data1, Variavel.codLoja, cmbFornecedor.Text, NUMDOC, Variavel.codUtilizador, NLoja,
              referencia, descricao, localizacao, fob, pca, pv, totalFob, nserie, mercadoria, qtd, metroGrid1.RowCount - 1);



            formRecibo.Show();
             
             


            


        }

       








        // FUNCAO PARA INSERIR OS DADOS 
        private void btGravar_Click(object sender, EventArgs e)
        {
            if (cmbFornecedor.Text == "") MessageBox.Show("O campo [ Fornecedor] tem que ser preenchido!");
            else if (cmbOrigemEntrada.Text == "") MessageBox.Show("O campo [ Origem da entrada ] tem que ser preenchido!");
            else if (txtInvoiceN.Text == "") MessageBox.Show("O campo [ Invoice ] tem que ser preenchido!");
            else if (cmbLocalizacao.Text == "") MessageBox.Show("O campo [ Localização] não pode ficar em branco!");
            

            else
            {

            gravar();
            imprimirFatura();
        


         

      // "INSERT Into AEFICMOV (IDUNICO, CODDOC, CODMARCA, CODMODELO, NSERIE,CODLOJA, REFERENC,CODENTIDADE, DataCria, CodUtilizador)

            // Select IDUNICO, :PARAM_CODDOC_EM, CODMARCA, CODMODELO, NSERIE,CODLOJA, REFERENC,CODENTIDADE, DataCria, CodUtilizador From Aefichas Where IDunico=idUnico()" )



          



        /*}
                 

               */

        }
        }
   

       

    }
}
