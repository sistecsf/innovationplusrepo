﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS;
using SISTEC_PLUS;
using SISTEC_PLUS.OPS;
using System.Drawing.Imaging;
using System.IO;

namespace SISTEC_PLUS.POS
{
    public partial class FormTabelaArtigoConsulta : MetroFramework.Forms.MetroForm
    {
        string codLoja, Codmercadoria, registaEquip, contaStock, sql;
        DateTime data;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public FormTabelaArtigoConsulta()
        {
            InitializeComponent();
        }
        private void prenxeCombo()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeLoja from ASLOJA with (NOLOCK) ORDER BY NomeLoja");
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbLoja.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void prenxeCombo1()
        {
            sql = @"select NomeArz from ASARMAZ with (NOLOCK) WHERE CodLoja like '%' ";
            if (cmbLoja.Text != "")
                sql = sql + "and CodLoja = '"+ codLoja +"' ";
            sql = sql + "ORDER BY NomeArz";
            SqlCommand cmd = new SqlCommand(sql);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbMercadoria.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void prenxeGrid()
        {
            sql = @"select Referenc, Activo, DescrArtigo,CodFam, CodArmz, QTD, FOB, PCA, PVP, PMC, LOCALI, STOCKMIN, RegEquip, CodLoja, Data_Expira, CONTASTOCKS from ASMESTRE where Referenc is not null ";
            if (rbRSim.Checked)
                sql = sql + "AND ARMA_VENDA = 'S' ";
            if (rbRNao.Checked)
                sql = sql + "AND ARMA_VENDA = 'N' ";
            if (rbCSim.Checked)
                sql = sql + "AND ARMA_REMOTO = 'S' ";
            if (rbCNao.Checked)
                sql = sql + "AND ARMA_REMOTO = 'N' ";
            if (cmbLoja.Text != "" || cmbLoja.PromptText != "")
                sql = sql + "AND CodLoja = '" + codLoja + "' ";
            if (cmbMercadoria.Text != "" || cmbMercadoria.PromptText != "")
                sql = sql + "AND CodArmz = '" + Codmercadoria + "' ";
            if(txtReferencia.Text != "")
                sql = sql + "AND Referenc =  '" + txtReferencia.Text + "' ";
            if(txtDesignacao.Text != "")
                sql = sql + "AND DescrArtigo = '" + txtDesignacao.Text + "' ";
            if(txtFamilia.Text != "")
                sql = sql + "AND CodFam = '" + txtFamilia.Text + "' ";
            sql = sql + "ORDER BY CONVERT(VARCHAR(10), CAST(DataCria AS DATETIME),105)";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("Loja");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
                conexao.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void gridDuploClick(DataGridView dataGrid)
        {
            FormTabelas.Areferc = dataGrid.CurrentRow.Cells[0].Value.ToString();
            FormTabelas.Aativo = dataGrid.CurrentRow.Cells[1].Value.ToString();
            FormTabelas.Adesig = dataGrid.CurrentRow.Cells[2].Value.ToString();
            FormTabelas.AcodFam = dataGrid.CurrentRow.Cells[3].Value.ToString();
            FormTabelas.AcodMerc = dataGrid.CurrentRow.Cells[4].Value.ToString();
            FormTabelas.Aqtd = dataGrid.CurrentRow.Cells[5].Value.ToString();
            FormTabelas.Afob = dataGrid.CurrentRow.Cells[6].Value.ToString();
            FormTabelas.Apca = dataGrid.CurrentRow.Cells[7].Value.ToString();
            FormTabelas.Apvp = dataGrid.CurrentRow.Cells[8].Value.ToString();
            FormTabelas.Apmc = dataGrid.CurrentRow.Cells[9].Value.ToString();
            FormTabelas.Alocal = dataGrid.CurrentRow.Cells[10].Value.ToString();
            FormTabelas.AstockMin = dataGrid.CurrentRow.Cells[11].Value.ToString();
            FormTabelas.AregE = dataGrid.CurrentRow.Cells[12].Value.ToString();
            FormTabelas.AcodLoja = dataGrid.CurrentRow.Cells[13].Value.ToString();
            FormTabelas.AdataEx = dataGrid.CurrentRow.Cells[14].Value.ToString();
            FormTabelas.AcontStock = dataGrid.CurrentRow.Cells[15].Value.ToString();

            data = Convert.ToDateTime(FormTabelas.AdataEx);
            if (data.Day < 10)
                FormTabelas.AdataEx = "0" + data.Day;
            else
                FormTabelas.AdataEx = "" + data.Day;
            if (data.Month < 10)
                FormTabelas.AdataEx = FormTabelas.AdataEx + "-0" + data.Month + "-" + data.Year;
            else
                FormTabelas.AdataEx = FormTabelas.AdataEx + "-" + data.Month + "-" + data.Year;
            
            SqlCommand cmd2 = new SqlCommand("select PROMOCAO, RegEquip, ALTERAPRECO, IMAGEM_WEB, CodSubFam, DESCR_SUP, DESEMBALAGEM, QUAEMBALAGEM, NORMA, COD_GRUPO, Permitir_Obs, HotelBooKing, Alterado_Por, CodUtilizador, DataCria, Data_Alteracao, Lucro, CAMBIO, ImpostoConsumo, DATA_FABRICO, STOCKNEGATIVO, STOCKMAX, PontoEncomenda, percquebra, Desconto from ASMESTRE where Referenc = '" + FormTabelas.Areferc + "' ", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd2.ExecuteReader())
            {
                while (reader.Read())
                {
                    FormTabelas.AcodSubFam = reader["CodSubFam"].ToString();
                    FormTabelas.AdesiSupl = reader["DESCR_SUP"].ToString();
                    FormTabelas.AdesgE = reader["DESEMBALAGEM"].ToString();
                    FormTabelas.Anorma = reader["NORMA"].ToString();
                    FormTabelas.Agrupo = reader["COD_GRUPO"].ToString();
                    FormTabelas.AregE = reader["RegEquip"].ToString();
                    FormTabelas.Apermite = reader["Permitir_Obs"].ToString();
                    FormTabelas.Abooking = reader["HotelBooKing"].ToString();
                    FormTabelas.Amostrar = reader["IMAGEM_WEB"].ToString();
                    FormTabelas.Apermitir = reader["ALTERAPRECO"].ToString();
                    FormTabelas.Aregisto = reader["RegEquip"].ToString();
                    FormTabelas.Apromocao = reader["PROMOCAO"].ToString();
                    FormTabelas.AstockMax = reader["STOCKMAX"].ToString();

                    FormTabelas.Alucro = reader["Lucro"].ToString();
                    FormTabelas.AalteradoP = reader["Alterado_Por"].ToString();
                    FormTabelas.Autilizador = reader["CodUtilizador"].ToString();
                    FormTabelas.AdataC = reader["DataCria"].ToString();
                    FormTabelas.AdataA = reader["Data_Alteracao"].ToString();
                    FormTabelas.Acambio = reader["CAMBIO"].ToString();
                    FormTabelas.Aimposto = reader["ImpostoConsumo"].ToString();
                    FormTabelas.AdataF = reader["DATA_FABRICO"].ToString();
                    FormTabelas.Anegativo = reader["STOCKNEGATIVO"].ToString();
                    FormTabelas.ApontoEncomenda = reader["PontoEncomenda"].ToString();
                    FormTabelas.Apercquebra = reader["percquebra"].ToString();
                    FormTabelas.Adesconto = reader["Desconto"].ToString();
                }
            }
            data = Convert.ToDateTime(FormTabelas.AdataF);
            if (data.Day < 10)
                FormTabelas.AdataF = "0" + data.Day;
            else
                FormTabelas.AdataF = "" + data.Day;
            if (data.Month < 10)
                FormTabelas.AdataF = FormTabelas.AdataF + "-0" + data.Month + "-" + data.Year;
            else
                FormTabelas.AdataF = FormTabelas.AdataF + "-" + data.Month + "-" + data.Year;
            
            SqlCommand cmdfamilia = new SqlCommand("select NomeMerc from ASFamilia where CodFam=@loja ", conexao);
            cmdfamilia.Parameters.Add(new SqlParameter("@loja", FormTabelas.AcodFam));
            FormTabelas.Afamilia = (string)cmdfamilia.ExecuteScalar();

            SqlCommand cmdsubfamilia = new SqlCommand("seleCt Descricao from ASSUBFAMILIA where CodSubFam=@loja ", conexao);
            cmdsubfamilia.Parameters.Add(new SqlParameter("@loja", FormTabelas.AcodSubFam));
            FormTabelas.AsubFamilia = (string)cmdsubfamilia.ExecuteScalar();

            SqlCommand cmdloja = new SqlCommand("seleCt NomeLoja from ASLOJA WHERE CodLoja=@loja ", conexao);
            cmdloja.Parameters.Add(new SqlParameter("@loja", FormTabelas.AcodLoja));
            FormTabelas.ALoja = (string)cmdloja.ExecuteScalar();

            SqlCommand cmdmercadoria = new SqlCommand("seleCt NomeArz from ASARMAZ where CodArmz=@loja ", conexao);
            cmdmercadoria.Parameters.Add(new SqlParameter("@loja", FormTabelas.AcodMerc));
            FormTabelas.Amercadoria = (string)cmdmercadoria.ExecuteScalar();

            conexao.Close();

            this.DialogResult = DialogResult.OK;
        }
        private void FormTabelaArtigoConsulta_Load(object sender, EventArgs e)
        {
            metroGrid1.AutoGenerateColumns = false;
            prenxeCombo();
        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    try { gridDuploClick(metroGrid1); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void cmbLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand(@"select CodLoja from ASLOJA with (NOLOCK) where NomeLoja = '" + cmbLoja.Text + "' ");
            try
            {
                cmd1.Connection = conexao;
                conexao.Open();
                codLoja = (string)cmd1.ExecuteScalar();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            cmbMercadoria.Items.Clear();
            prenxeCombo1();
        }

        private void cmbMercadoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand(@"select CodArmz from ASARMAZ with (NOLOCK) where NomeArz = '" + cmbMercadoria.Text + "' ");
            try
            {
                cmd1.Connection = conexao;
                conexao.Open();
                Codmercadoria = (string)cmd1.ExecuteScalar();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }

        private void txtReferencia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void txtDesignacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void txtFamilia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void rbRSim_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void rbRNao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void rbCSim_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void rbCNao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void btConsultar_Click(object sender, EventArgs e)
        {
            metroGrid1.DataSource = null;
            metroGrid1.Rows.Clear();
            Cursor.Current = Cursors.WaitCursor;
            prenxeGrid();
        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try { gridDuploClick(metroGrid1); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
