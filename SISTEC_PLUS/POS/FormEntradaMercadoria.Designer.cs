﻿using SISTEC_PLUS.Properties;
namespace SISTEC_PLUS.POS
{
    partial class FormEntradaMercadoria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btConsultar = new System.Windows.Forms.ToolStripButton();
            this.btNovo = new System.Windows.Forms.ToolStripButton();
            this.btGravar = new System.Windows.Forms.ToolStripButton();
            this.btSair = new System.Windows.Forms.ToolStripButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.txtFornecedor = new MetroFramework.Controls.MetroTextBox();
            this.txtOrigemEntrada = new MetroFramework.Controls.MetroTextBox();
            this.cmbFornecedor = new MetroFramework.Controls.MetroComboBox();
            this.cmbOrigemEntrada = new MetroFramework.Controls.MetroComboBox();
            this.txtNumero = new MetroFramework.Controls.MetroTextBox();
            this.txtInvoiceN = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.txtLocali = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.txtMovStock = new MetroFramework.Controls.MetroTextBox();
            this.txtGuia = new MetroFramework.Controls.MetroTextBox();
            this.txtQdt = new MetroFramework.Controls.MetroTextBox();
            this.txtContagem = new MetroFramework.Controls.MetroTextBox();
            this.txtFOB = new MetroFramework.Controls.MetroTextBox();
            this.txtDesc = new MetroFramework.Controls.MetroTextBox();
            this.txtTotal = new MetroFramework.Controls.MetroTextBox();
            this.PCAP = new MetroFramework.Controls.MetroTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.Referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Un = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mercadoria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PCA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Localizacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTotalFob = new MetroFramework.Controls.MetroTextBox();
            this.txtTotalPca = new MetroFramework.Controls.MetroTextBox();
            this.txtTotalValor = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.cmbReferencia1 = new System.Windows.Forms.ComboBox();
            this.cmbDescricaoArtigo1 = new System.Windows.Forms.ComboBox();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.txtIdUnico = new MetroFramework.Controls.MetroTextBox();
            this.txtTotalDesconto = new MetroFramework.Controls.MetroTextBox();
            this.metroGrid2 = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCodAo = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.cmbLocalizacao = new System.Windows.Forms.ComboBox();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid2)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(276, 2);
            this.toolStripContainer1.Location = new System.Drawing.Point(432, 72);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(276, 27);
            this.toolStripContainer1.TabIndex = 16;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            this.toolStripContainer1.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer1.TopToolStripPanel.Tag = "XC";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btConsultar,
            this.btNovo,
            this.btGravar,
            this.btSair});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(175, 25);
            this.toolStrip1.TabIndex = 14;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btConsultar
            // 
            this.btConsultar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btConsultar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btConsultar.Name = "btConsultar";
            this.btConsultar.Size = new System.Drawing.Size(57, 22);
            this.btConsultar.Text = "Importar";
            this.btConsultar.Click += new System.EventHandler(this.btConsultar_Click);
            // 
            // btNovo
            // 
            this.btNovo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(40, 22);
            this.btNovo.Text = "Novo";
            // 
            // btGravar
            // 
            this.btGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(45, 22);
            this.btGravar.Text = "Gravar";
            this.btGravar.Click += new System.EventHandler(this.btGravar_Click);
            // 
            // btSair
            // 
            this.btSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(30, 22);
            this.btSair.Text = "Sair";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 124);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(77, 19);
            this.metroLabel1.TabIndex = 17;
            this.metroLabel1.Text = "Fornecedor";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(23, 159);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(123, 19);
            this.metroLabel2.TabIndex = 18;
            this.metroLabel2.Text = "Origem da Entrada";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(23, 191);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(69, 19);
            this.metroLabel3.TabIndex = 19;
            this.metroLabel3.Text = "Invoice Nº";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(23, 226);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(70, 19);
            this.metroLabel4.TabIndex = 20;
            this.metroLabel4.Text = "Referência";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(624, 163);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(58, 19);
            this.metroLabel6.TabIndex = 22;
            this.metroLabel6.Text = "Número";
            // 
            // txtFornecedor
            // 
            this.txtFornecedor.Lines = new string[0];
            this.txtFornecedor.Location = new System.Drawing.Point(161, 120);
            this.txtFornecedor.MaxLength = 32767;
            this.txtFornecedor.Name = "txtFornecedor";
            this.txtFornecedor.PasswordChar = '\0';
            this.txtFornecedor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFornecedor.SelectedText = "";
            this.txtFornecedor.Size = new System.Drawing.Size(75, 23);
            this.txtFornecedor.TabIndex = 23;
            this.txtFornecedor.UseSelectable = true;
            // 
            // txtOrigemEntrada
            // 
            this.txtOrigemEntrada.ForeColor = System.Drawing.Color.Maroon;
            this.txtOrigemEntrada.Lines = new string[0];
            this.txtOrigemEntrada.Location = new System.Drawing.Point(161, 155);
            this.txtOrigemEntrada.MaxLength = 32767;
            this.txtOrigemEntrada.Name = "txtOrigemEntrada";
            this.txtOrigemEntrada.PasswordChar = '\0';
            this.txtOrigemEntrada.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtOrigemEntrada.SelectedText = "";
            this.txtOrigemEntrada.Size = new System.Drawing.Size(75, 23);
            this.txtOrigemEntrada.TabIndex = 24;
            this.txtOrigemEntrada.UseSelectable = true;
            // 
            // cmbFornecedor
            // 
            this.cmbFornecedor.FormattingEnabled = true;
            this.cmbFornecedor.IntegralHeight = false;
            this.cmbFornecedor.ItemHeight = 23;
            this.cmbFornecedor.Location = new System.Drawing.Point(263, 114);
            this.cmbFornecedor.MaxDropDownItems = 10;
            this.cmbFornecedor.Name = "cmbFornecedor";
            this.cmbFornecedor.Size = new System.Drawing.Size(464, 29);
            this.cmbFornecedor.TabIndex = 25;
            this.cmbFornecedor.UseSelectable = true;
            this.cmbFornecedor.SelectedIndexChanged += new System.EventHandler(this.cmbFornecedor_SelectedIndexChanged);
            this.cmbFornecedor.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbFornecedor_MouseClick);
            // 
            // cmbOrigemEntrada
            // 
            this.cmbOrigemEntrada.FormattingEnabled = true;
            this.cmbOrigemEntrada.IntegralHeight = false;
            this.cmbOrigemEntrada.ItemHeight = 23;
            this.cmbOrigemEntrada.Location = new System.Drawing.Point(263, 149);
            this.cmbOrigemEntrada.MaxDropDownItems = 10;
            this.cmbOrigemEntrada.Name = "cmbOrigemEntrada";
            this.cmbOrigemEntrada.Size = new System.Drawing.Size(282, 29);
            this.cmbOrigemEntrada.TabIndex = 26;
            this.cmbOrigemEntrada.UseSelectable = true;
            this.cmbOrigemEntrada.SelectedIndexChanged += new System.EventHandler(this.cmbOrigemEntrada_SelectedIndexChanged);
            this.cmbOrigemEntrada.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbOrigemEntrada_MouseClick);
            // 
            // txtNumero
            // 
            this.txtNumero.Lines = new string[0];
            this.txtNumero.Location = new System.Drawing.Point(688, 159);
            this.txtNumero.MaxLength = 32767;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.PasswordChar = '\0';
            this.txtNumero.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtNumero.SelectedText = "";
            this.txtNumero.Size = new System.Drawing.Size(75, 23);
            this.txtNumero.TabIndex = 28;
            this.txtNumero.UseSelectable = true;
            // 
            // txtInvoiceN
            // 
            this.txtInvoiceN.Lines = new string[0];
            this.txtInvoiceN.Location = new System.Drawing.Point(263, 187);
            this.txtInvoiceN.MaxLength = 32767;
            this.txtInvoiceN.Name = "txtInvoiceN";
            this.txtInvoiceN.PasswordChar = '\0';
            this.txtInvoiceN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtInvoiceN.SelectedText = "";
            this.txtInvoiceN.Size = new System.Drawing.Size(282, 23);
            this.txtInvoiceN.TabIndex = 29;
            this.txtInvoiceN.UseSelectable = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(326, 225);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(76, 19);
            this.metroLabel7.TabIndex = 31;
            this.metroLabel7.Text = "Localização";
            // 
            // txtLocali
            // 
            this.txtLocali.Lines = new string[0];
            this.txtLocali.Location = new System.Drawing.Point(460, 257);
            this.txtLocali.MaxLength = 32767;
            this.txtLocali.Name = "txtLocali";
            this.txtLocali.PasswordChar = '\0';
            this.txtLocali.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLocali.SelectedText = "";
            this.txtLocali.Size = new System.Drawing.Size(85, 23);
            this.txtLocali.TabIndex = 34;
            this.txtLocali.UseSelectable = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(650, 192);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(32, 19);
            this.metroLabel8.TabIndex = 35;
            this.metroLabel8.Text = "Qtd";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(611, 252);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(71, 19);
            this.metroLabel9.TabIndex = 36;
            this.metroLabel9.Text = "Contagem";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(826, 163);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(72, 19);
            this.metroLabel10.TabIndex = 37;
            this.metroLabel10.Text = "Mov. Stock";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(863, 194);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(35, 19);
            this.metroLabel11.TabIndex = 38;
            this.metroLabel11.Text = "Guia";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(863, 227);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(35, 19);
            this.metroLabel12.TabIndex = 39;
            this.metroLabel12.Text = "FOB";
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(646, 221);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(36, 19);
            this.metroLabel13.TabIndex = 40;
            this.metroLabel13.Text = "Desc";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(644, 286);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(36, 19);
            this.metroLabel14.TabIndex = 41;
            this.metroLabel14.Text = "Total";
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(851, 253);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(47, 19);
            this.metroLabel15.TabIndex = 42;
            this.metroLabel15.Text = "PCA P.";
            // 
            // txtMovStock
            // 
            this.txtMovStock.Lines = new string[0];
            this.txtMovStock.Location = new System.Drawing.Point(904, 159);
            this.txtMovStock.MaxLength = 32767;
            this.txtMovStock.Name = "txtMovStock";
            this.txtMovStock.PasswordChar = '\0';
            this.txtMovStock.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtMovStock.SelectedText = "";
            this.txtMovStock.Size = new System.Drawing.Size(125, 23);
            this.txtMovStock.TabIndex = 43;
            this.txtMovStock.UseSelectable = true;
            // 
            // txtGuia
            // 
            this.txtGuia.Lines = new string[0];
            this.txtGuia.Location = new System.Drawing.Point(904, 192);
            this.txtGuia.MaxLength = 32767;
            this.txtGuia.Name = "txtGuia";
            this.txtGuia.PasswordChar = '\0';
            this.txtGuia.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtGuia.SelectedText = "";
            this.txtGuia.Size = new System.Drawing.Size(125, 23);
            this.txtGuia.TabIndex = 44;
            this.txtGuia.UseSelectable = true;
            // 
            // txtQdt
            // 
            this.txtQdt.Lines = new string[0];
            this.txtQdt.Location = new System.Drawing.Point(688, 192);
            this.txtQdt.MaxLength = 32767;
            this.txtQdt.Name = "txtQdt";
            this.txtQdt.PasswordChar = '\0';
            this.txtQdt.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtQdt.SelectedText = "";
            this.txtQdt.Size = new System.Drawing.Size(75, 23);
            this.txtQdt.TabIndex = 45;
            this.txtQdt.UseSelectable = true;
            this.txtQdt.TextChanged += new System.EventHandler(this.txtQdt_TextChanged);
            this.txtQdt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQdt_KeyPress);
            // 
            // txtContagem
            // 
            this.txtContagem.Lines = new string[0];
            this.txtContagem.Location = new System.Drawing.Point(688, 253);
            this.txtContagem.MaxLength = 32767;
            this.txtContagem.Name = "txtContagem";
            this.txtContagem.PasswordChar = '\0';
            this.txtContagem.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtContagem.SelectedText = "";
            this.txtContagem.Size = new System.Drawing.Size(75, 23);
            this.txtContagem.TabIndex = 46;
            this.txtContagem.UseSelectable = true;
            // 
            // txtFOB
            // 
            this.txtFOB.Lines = new string[0];
            this.txtFOB.Location = new System.Drawing.Point(904, 220);
            this.txtFOB.MaxLength = 32767;
            this.txtFOB.Name = "txtFOB";
            this.txtFOB.PasswordChar = '\0';
            this.txtFOB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFOB.SelectedText = "";
            this.txtFOB.Size = new System.Drawing.Size(125, 23);
            this.txtFOB.TabIndex = 47;
            this.txtFOB.UseSelectable = true;
            // 
            // txtDesc
            // 
            this.txtDesc.Lines = new string[0];
            this.txtDesc.Location = new System.Drawing.Point(688, 221);
            this.txtDesc.MaxLength = 32767;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.PasswordChar = '\0';
            this.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDesc.SelectedText = "";
            this.txtDesc.Size = new System.Drawing.Size(75, 23);
            this.txtDesc.TabIndex = 48;
            this.txtDesc.UseSelectable = true;
            this.txtDesc.TextChanged += new System.EventHandler(this.txtDesc_TextChanged);
            // 
            // txtTotal
            // 
            this.txtTotal.Lines = new string[0];
            this.txtTotal.Location = new System.Drawing.Point(688, 282);
            this.txtTotal.MaxLength = 32767;
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.PasswordChar = '\0';
            this.txtTotal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTotal.SelectedText = "";
            this.txtTotal.Size = new System.Drawing.Size(75, 23);
            this.txtTotal.TabIndex = 49;
            this.txtTotal.UseSelectable = true;
            // 
            // PCAP
            // 
            this.PCAP.Lines = new string[0];
            this.PCAP.Location = new System.Drawing.Point(904, 249);
            this.PCAP.MaxLength = 32767;
            this.PCAP.Name = "PCAP";
            this.PCAP.PasswordChar = '\0';
            this.PCAP.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.PCAP.SelectedText = "";
            this.PCAP.Size = new System.Drawing.Size(125, 23);
            this.PCAP.TabIndex = 50;
            this.PCAP.UseSelectable = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(246, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(246, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 13);
            this.label2.TabIndex = 52;
            this.label2.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(246, 194);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 13);
            this.label3.TabIndex = 53;
            this.label3.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(408, 232);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 13);
            this.label4.TabIndex = 54;
            this.label4.Text = "*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(769, 227);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(15, 13);
            this.label5.TabIndex = 55;
            this.label5.Text = "%";
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Referencia,
            this.Un,
            this.Quantidade,
            this.Mercadoria,
            this.FOB,
            this.PCA,
            this.Desc,
            this.Total,
            this.Localizacao,
            this.Contagem});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(27, 339);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(1045, 150);
            this.metroGrid1.TabIndex = 56;
            // 
            // Referencia
            // 
            this.Referencia.DataPropertyName = "Referenc";
            this.Referencia.HeaderText = "Referência";
            this.Referencia.Name = "Referencia";
            // 
            // Un
            // 
            this.Un.HeaderText = "Un";
            this.Un.Name = "Un";
            // 
            // Quantidade
            // 
            this.Quantidade.DataPropertyName = "QTD";
            this.Quantidade.HeaderText = "Quantidade";
            this.Quantidade.Name = "Quantidade";
            // 
            // Mercadoria
            // 
            this.Mercadoria.HeaderText = "Mercadoria";
            this.Mercadoria.Name = "Mercadoria";
            // 
            // FOB
            // 
            this.FOB.DataPropertyName = "FOB";
            this.FOB.HeaderText = "FOB";
            this.FOB.Name = "FOB";
            // 
            // PCA
            // 
            this.PCA.HeaderText = "PCA";
            this.PCA.Name = "PCA";
            // 
            // Desc
            // 
            this.Desc.HeaderText = "% Desc";
            this.Desc.Name = "Desc";
            // 
            // Total
            // 
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            // 
            // Localizacao
            // 
            this.Localizacao.HeaderText = "Localização";
            this.Localizacao.Name = "Localizacao";
            // 
            // Contagem
            // 
            this.Contagem.HeaderText = "Contagem";
            this.Contagem.Name = "Contagem";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(408, 524);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 83;
            this.label6.Text = "Total";
            // 
            // txtTotalFob
            // 
            this.txtTotalFob.Lines = new string[0];
            this.txtTotalFob.Location = new System.Drawing.Point(460, 514);
            this.txtTotalFob.MaxLength = 32767;
            this.txtTotalFob.Name = "txtTotalFob";
            this.txtTotalFob.PasswordChar = '\0';
            this.txtTotalFob.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTotalFob.SelectedText = "";
            this.txtTotalFob.Size = new System.Drawing.Size(75, 23);
            this.txtTotalFob.TabIndex = 84;
            this.txtTotalFob.UseSelectable = true;
            // 
            // txtTotalPca
            // 
            this.txtTotalPca.Lines = new string[0];
            this.txtTotalPca.Location = new System.Drawing.Point(551, 514);
            this.txtTotalPca.MaxLength = 32767;
            this.txtTotalPca.Name = "txtTotalPca";
            this.txtTotalPca.PasswordChar = '\0';
            this.txtTotalPca.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTotalPca.SelectedText = "";
            this.txtTotalPca.Size = new System.Drawing.Size(75, 23);
            this.txtTotalPca.TabIndex = 85;
            this.txtTotalPca.UseSelectable = true;
            // 
            // txtTotalValor
            // 
            this.txtTotalValor.Lines = new string[0];
            this.txtTotalValor.Location = new System.Drawing.Point(744, 514);
            this.txtTotalValor.MaxLength = 32767;
            this.txtTotalValor.Name = "txtTotalValor";
            this.txtTotalValor.PasswordChar = '\0';
            this.txtTotalValor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTotalValor.SelectedText = "";
            this.txtTotalValor.Size = new System.Drawing.Size(75, 23);
            this.txtTotalValor.TabIndex = 86;
            this.txtTotalValor.UseSelectable = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(23, 261);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(65, 19);
            this.metroLabel5.TabIndex = 87;
            this.metroLabel5.Text = "Descrição";
            // 
            // cmbReferencia1
            // 
            this.cmbReferencia1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbReferencia1.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbReferencia1.FormattingEnabled = true;
            this.cmbReferencia1.IntegralHeight = false;
            this.cmbReferencia1.Location = new System.Drawing.Point(99, 220);
            this.cmbReferencia1.Name = "cmbReferencia1";
            this.cmbReferencia1.Size = new System.Drawing.Size(183, 28);
            this.cmbReferencia1.TabIndex = 114;
            this.cmbReferencia1.SelectedIndexChanged += new System.EventHandler(this.cmbReferencia1_SelectedIndexChanged);
            this.cmbReferencia1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbReferencia1_MouseClick_1);
            // 
            // cmbDescricaoArtigo1
            // 
            this.cmbDescricaoArtigo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDescricaoArtigo1.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbDescricaoArtigo1.FormattingEnabled = true;
            this.cmbDescricaoArtigo1.IntegralHeight = false;
            this.cmbDescricaoArtigo1.Location = new System.Drawing.Point(99, 256);
            this.cmbDescricaoArtigo1.Name = "cmbDescricaoArtigo1";
            this.cmbDescricaoArtigo1.Size = new System.Drawing.Size(316, 28);
            this.cmbDescricaoArtigo1.TabIndex = 113;
            this.cmbDescricaoArtigo1.SelectedIndexChanged += new System.EventHandler(this.cmbDescricaoArtigo1_SelectedIndexChanged);
            this.cmbDescricaoArtigo1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbDescricaoArtigo1_MouseClick_1);
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(1182, 368);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(56, 19);
            this.metroLabel16.TabIndex = 115;
            this.metroLabel16.Text = "ID unico";
            // 
            // txtIdUnico
            // 
            this.txtIdUnico.Lines = new string[0];
            this.txtIdUnico.Location = new System.Drawing.Point(1244, 364);
            this.txtIdUnico.MaxLength = 32767;
            this.txtIdUnico.Name = "txtIdUnico";
            this.txtIdUnico.PasswordChar = '\0';
            this.txtIdUnico.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtIdUnico.SelectedText = "";
            this.txtIdUnico.Size = new System.Drawing.Size(125, 23);
            this.txtIdUnico.TabIndex = 116;
            this.txtIdUnico.UseSelectable = true;
            // 
            // txtTotalDesconto
            // 
            this.txtTotalDesconto.Lines = new string[0];
            this.txtTotalDesconto.Location = new System.Drawing.Point(652, 514);
            this.txtTotalDesconto.MaxLength = 32767;
            this.txtTotalDesconto.Name = "txtTotalDesconto";
            this.txtTotalDesconto.PasswordChar = '\0';
            this.txtTotalDesconto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTotalDesconto.SelectedText = "";
            this.txtTotalDesconto.Size = new System.Drawing.Size(75, 23);
            this.txtTotalDesconto.TabIndex = 117;
            this.txtTotalDesconto.UseSelectable = true;
            // 
            // metroGrid2
            // 
            this.metroGrid2.AllowUserToResizeRows = false;
            this.metroGrid2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.metroGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.Column7,
            this.Column8});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid2.DefaultCellStyle = dataGridViewCellStyle5;
            this.metroGrid2.EnableHeadersVisualStyles = false;
            this.metroGrid2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid2.Location = new System.Drawing.Point(1169, 124);
            this.metroGrid2.Name = "metroGrid2";
            this.metroGrid2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.metroGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid2.Size = new System.Drawing.Size(345, 161);
            this.metroGrid2.TabIndex = 144;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Nº Serie";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "IMEI / ICCID";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "MARCA";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "MODELO";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "REFERÊNCIA";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "TEL. OU SIM";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "ARMAZEM";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "FOB";
            this.Column8.Name = "Column8";
            // 
            // txtCodAo
            // 
            this.txtCodAo.Lines = new string[0];
            this.txtCodAo.Location = new System.Drawing.Point(1239, 330);
            this.txtCodAo.MaxLength = 32767;
            this.txtCodAo.Name = "txtCodAo";
            this.txtCodAo.PasswordChar = '\0';
            this.txtCodAo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCodAo.SelectedText = "";
            this.txtCodAo.Size = new System.Drawing.Size(125, 23);
            this.txtCodAo.TabIndex = 146;
            this.txtCodAo.UseSelectable = true;
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(1182, 330);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(51, 19);
            this.metroLabel17.TabIndex = 145;
            this.metroLabel17.Text = "CodAo";
            // 
            // cmbLocalizacao
            // 
            this.cmbLocalizacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLocalizacao.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.cmbLocalizacao.FormattingEnabled = true;
            this.cmbLocalizacao.IntegralHeight = false;
            this.cmbLocalizacao.Location = new System.Drawing.Point(425, 225);
            this.cmbLocalizacao.Name = "cmbLocalizacao";
            this.cmbLocalizacao.Size = new System.Drawing.Size(120, 28);
            this.cmbLocalizacao.TabIndex = 147;
            // 
            // FormEntradaMercadoria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1106, 558);
            this.ControlBox = false;
            this.Controls.Add(this.cmbLocalizacao);
            this.Controls.Add(this.txtCodAo);
            this.Controls.Add(this.metroLabel17);
            this.Controls.Add(this.metroGrid2);
            this.Controls.Add(this.txtTotalDesconto);
            this.Controls.Add(this.txtIdUnico);
            this.Controls.Add(this.metroLabel16);
            this.Controls.Add(this.cmbReferencia1);
            this.Controls.Add(this.cmbDescricaoArtigo1);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.txtTotalValor);
            this.Controls.Add(this.txtTotalPca);
            this.Controls.Add(this.txtTotalFob);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.metroGrid1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PCAP);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.txtDesc);
            this.Controls.Add(this.txtFOB);
            this.Controls.Add(this.txtContagem);
            this.Controls.Add(this.txtQdt);
            this.Controls.Add(this.txtGuia);
            this.Controls.Add(this.txtMovStock);
            this.Controls.Add(this.metroLabel15);
            this.Controls.Add(this.metroLabel14);
            this.Controls.Add(this.metroLabel13);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.txtLocali);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.txtInvoiceN);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.cmbOrigemEntrada);
            this.Controls.Add(this.cmbFornecedor);
            this.Controls.Add(this.txtOrigemEntrada);
            this.Controls.Add(this.txtFornecedor);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "FormEntradaMercadoria";
            this.Text = "Entrada de Mercadoria";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormEntradaMercadoria_Load);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btConsultar;
        private System.Windows.Forms.ToolStripButton btNovo;
        private System.Windows.Forms.ToolStripButton btGravar;
        private System.Windows.Forms.ToolStripButton btSair;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox txtFornecedor;
        private MetroFramework.Controls.MetroTextBox txtOrigemEntrada;
        private MetroFramework.Controls.MetroComboBox cmbFornecedor;
        private MetroFramework.Controls.MetroComboBox cmbOrigemEntrada;
        private MetroFramework.Controls.MetroTextBox txtNumero;
        private MetroFramework.Controls.MetroTextBox txtInvoiceN;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox txtLocali;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroTextBox txtMovStock;
        private MetroFramework.Controls.MetroTextBox txtGuia;
        private MetroFramework.Controls.MetroTextBox txtContagem;
        private MetroFramework.Controls.MetroTextBox txtFOB;
        private MetroFramework.Controls.MetroTextBox txtDesc;
        private MetroFramework.Controls.MetroTextBox txtTotal;
        private MetroFramework.Controls.MetroTextBox PCAP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private System.Windows.Forms.Label label6;
        private MetroFramework.Controls.MetroTextBox txtTotalFob;
        private MetroFramework.Controls.MetroTextBox txtTotalPca;
        private MetroFramework.Controls.MetroTextBox txtTotalValor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Un;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mercadoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn FOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn PCA;
        private System.Windows.Forms.DataGridViewTextBoxColumn Desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn Localizacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contagem;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private System.Windows.Forms.ComboBox cmbReferencia1;
        private System.Windows.Forms.ComboBox cmbDescricaoArtigo1;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroTextBox txtIdUnico;
        private MetroFramework.Controls.MetroTextBox txtTotalDesconto;
        public MetroFramework.Controls.MetroTextBox txtQdt;
        private MetroFramework.Controls.MetroGrid metroGrid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private MetroFramework.Controls.MetroTextBox txtCodAo;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private System.Windows.Forms.ComboBox cmbLocalizacao;
    }
}