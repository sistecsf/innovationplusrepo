﻿namespace SISTEC_PLUS.POS
{
    partial class FormRelatorioPOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.Vendas = new MetroFramework.Controls.MetroTabPage();
            this.cbVListagemMov = new MetroFramework.Controls.MetroCheckBox();
            this.cbVfaturacaoSemanaDia = new MetroFramework.Controls.MetroCheckBox();
            this.cbVfaturaDiaHora = new MetroFramework.Controls.MetroCheckBox();
            this.cbVresumoPerVendF = new MetroFramework.Controls.MetroCheckBox();
            this.cbVresumoPerVend = new MetroFramework.Controls.MetroCheckBox();
            this.cbVresumoPerOP = new MetroFramework.Controls.MetroCheckBox();
            this.cbVdocDesc = new MetroFramework.Controls.MetroCheckBox();
            this.cbVdocOperador = new MetroFramework.Controls.MetroCheckBox();
            this.cbVresumoDiario = new MetroFramework.Controls.MetroCheckBox();
            this.cbVresumoDiarioDoc = new MetroFramework.Controls.MetroCheckBox();
            this.cbVlistagem = new MetroFramework.Controls.MetroCheckBox();
            this.cbVvendasDinhVend = new MetroFramework.Controls.MetroCheckBox();
            this.cbVvendasDinhOp = new MetroFramework.Controls.MetroCheckBox();
            this.cbVmovimentoArtigo = new MetroFramework.Controls.MetroCheckBox();
            this.cbVacumuladosFamilia = new MetroFramework.Controls.MetroCheckBox();
            this.cbVacumuladosArmazem = new MetroFramework.Controls.MetroCheckBox();
            this.cbVacumulados = new MetroFramework.Controls.MetroCheckBox();
            this.Inventario = new MetroFramework.Controls.MetroTabPage();
            this.Artigo = new MetroFramework.Controls.MetroTabPage();
            this.Movimentos = new MetroFramework.Controls.MetroTabPage();
            this.MapaEstatico = new MetroFramework.Controls.MetroTabPage();
            this.Tabelas = new MetroFramework.Controls.MetroTabPage();
            this.ReemissãoGuias = new MetroFramework.Controls.MetroTabPage();
            this.ValoresStock = new MetroFramework.Controls.MetroTabPage();
            this.EntArtigos = new MetroFramework.Controls.MetroTabPage();
            this.FolhaEspelho = new MetroFramework.Controls.MetroTabPage();
            this.TransfPendentes = new MetroFramework.Controls.MetroTabPage();
            this.CurvaAbc = new MetroFramework.Controls.MetroTabPage();
            this.ApoioGestão = new MetroFramework.Controls.MetroTabPage();
            this.MapaComparativo = new MetroFramework.Controls.MetroTabPage();
            this.Graficos = new MetroFramework.Controls.MetroTabPage();
            this.metroTabControl1.SuspendLayout();
            this.Vendas.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.Vendas);
            this.metroTabControl1.Controls.Add(this.Inventario);
            this.metroTabControl1.Controls.Add(this.Artigo);
            this.metroTabControl1.Controls.Add(this.Movimentos);
            this.metroTabControl1.Controls.Add(this.MapaEstatico);
            this.metroTabControl1.Controls.Add(this.Tabelas);
            this.metroTabControl1.Controls.Add(this.ReemissãoGuias);
            this.metroTabControl1.Controls.Add(this.ValoresStock);
            this.metroTabControl1.Controls.Add(this.EntArtigos);
            this.metroTabControl1.Controls.Add(this.FolhaEspelho);
            this.metroTabControl1.Controls.Add(this.TransfPendentes);
            this.metroTabControl1.Controls.Add(this.CurvaAbc);
            this.metroTabControl1.Controls.Add(this.ApoioGestão);
            this.metroTabControl1.Controls.Add(this.MapaComparativo);
            this.metroTabControl1.Controls.Add(this.Graficos);
            this.metroTabControl1.Location = new System.Drawing.Point(12, 118);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(1213, 380);
            this.metroTabControl1.TabIndex = 1;
            this.metroTabControl1.UseSelectable = true;
            // 
            // Vendas
            // 
            this.Vendas.Controls.Add(this.cbVListagemMov);
            this.Vendas.Controls.Add(this.cbVfaturacaoSemanaDia);
            this.Vendas.Controls.Add(this.cbVfaturaDiaHora);
            this.Vendas.Controls.Add(this.cbVresumoPerVendF);
            this.Vendas.Controls.Add(this.cbVresumoPerVend);
            this.Vendas.Controls.Add(this.cbVresumoPerOP);
            this.Vendas.Controls.Add(this.cbVdocDesc);
            this.Vendas.Controls.Add(this.cbVdocOperador);
            this.Vendas.Controls.Add(this.cbVresumoDiario);
            this.Vendas.Controls.Add(this.cbVresumoDiarioDoc);
            this.Vendas.Controls.Add(this.cbVlistagem);
            this.Vendas.Controls.Add(this.cbVvendasDinhVend);
            this.Vendas.Controls.Add(this.cbVvendasDinhOp);
            this.Vendas.Controls.Add(this.cbVmovimentoArtigo);
            this.Vendas.Controls.Add(this.cbVacumuladosFamilia);
            this.Vendas.Controls.Add(this.cbVacumuladosArmazem);
            this.Vendas.Controls.Add(this.cbVacumulados);
            this.Vendas.HorizontalScrollbarBarColor = true;
            this.Vendas.HorizontalScrollbarHighlightOnWheel = false;
            this.Vendas.HorizontalScrollbarSize = 10;
            this.Vendas.Location = new System.Drawing.Point(4, 38);
            this.Vendas.Name = "Vendas";
            this.Vendas.Size = new System.Drawing.Size(1205, 338);
            this.Vendas.TabIndex = 0;
            this.Vendas.Text = "Vendas";
            this.Vendas.VerticalScrollbarBarColor = true;
            this.Vendas.VerticalScrollbarHighlightOnWheel = false;
            this.Vendas.VerticalScrollbarSize = 10;
            // 
            // cbVListagemMov
            // 
            this.cbVListagemMov.AutoSize = true;
            this.cbVListagemMov.Location = new System.Drawing.Point(698, 101);
            this.cbVListagemMov.Name = "cbVListagemMov";
            this.cbVListagemMov.Size = new System.Drawing.Size(208, 15);
            this.cbVListagemMov.TabIndex = 23;
            this.cbVListagemMov.Text = "Listagem Detalhada de Movimento";
            this.cbVListagemMov.UseSelectable = true;
            // 
            // cbVfaturacaoSemanaDia
            // 
            this.cbVfaturacaoSemanaDia.AutoSize = true;
            this.cbVfaturacaoSemanaDia.Location = new System.Drawing.Point(698, 80);
            this.cbVfaturacaoSemanaDia.Name = "cbVfaturacaoSemanaDia";
            this.cbVfaturacaoSemanaDia.Size = new System.Drawing.Size(148, 15);
            this.cbVfaturacaoSemanaDia.TabIndex = 22;
            this.cbVfaturacaoSemanaDia.Text = "Faturaçao Semana / Dia";
            this.cbVfaturacaoSemanaDia.UseSelectable = true;
            // 
            // cbVfaturaDiaHora
            // 
            this.cbVfaturaDiaHora.AutoSize = true;
            this.cbVfaturaDiaHora.Location = new System.Drawing.Point(483, 164);
            this.cbVfaturaDiaHora.Name = "cbVfaturaDiaHora";
            this.cbVfaturaDiaHora.Size = new System.Drawing.Size(132, 15);
            this.cbVfaturaDiaHora.TabIndex = 21;
            this.cbVfaturaDiaHora.Text = "Faturação Dia / Hora";
            this.cbVfaturaDiaHora.UseSelectable = true;
            // 
            // cbVresumoPerVendF
            // 
            this.cbVresumoPerVendF.AutoSize = true;
            this.cbVresumoPerVendF.Location = new System.Drawing.Point(483, 143);
            this.cbVresumoPerVendF.Name = "cbVresumoPerVendF";
            this.cbVresumoPerVendF.Size = new System.Drawing.Size(244, 15);
            this.cbVresumoPerVendF.TabIndex = 20;
            this.cbVresumoPerVendF.Text = "Resumo Períodico por Vendedor (Factura)";
            this.cbVresumoPerVendF.UseSelectable = true;
            // 
            // cbVresumoPerVend
            // 
            this.cbVresumoPerVend.AutoSize = true;
            this.cbVresumoPerVend.Location = new System.Drawing.Point(483, 122);
            this.cbVresumoPerVend.Name = "cbVresumoPerVend";
            this.cbVresumoPerVend.Size = new System.Drawing.Size(194, 15);
            this.cbVresumoPerVend.TabIndex = 19;
            this.cbVresumoPerVend.Text = "Resumo Períodico por Vendedor";
            this.cbVresumoPerVend.UseSelectable = true;
            // 
            // cbVresumoPerOP
            // 
            this.cbVresumoPerOP.AutoSize = true;
            this.cbVresumoPerOP.Location = new System.Drawing.Point(483, 101);
            this.cbVresumoPerOP.Name = "cbVresumoPerOP";
            this.cbVresumoPerOP.Size = new System.Drawing.Size(193, 15);
            this.cbVresumoPerOP.TabIndex = 18;
            this.cbVresumoPerOP.Text = "Resumo Periódico por Operador";
            this.cbVresumoPerOP.UseSelectable = true;
            // 
            // cbVdocDesc
            // 
            this.cbVdocDesc.AutoSize = true;
            this.cbVdocDesc.Location = new System.Drawing.Point(483, 80);
            this.cbVdocDesc.Name = "cbVdocDesc";
            this.cbVdocDesc.Size = new System.Drawing.Size(171, 15);
            this.cbVdocDesc.TabIndex = 17;
            this.cbVdocDesc.Text = "Documentos com Desconto";
            this.cbVdocDesc.UseSelectable = true;
            // 
            // cbVdocOperador
            // 
            this.cbVdocOperador.AutoSize = true;
            this.cbVdocOperador.Location = new System.Drawing.Point(243, 164);
            this.cbVdocOperador.Name = "cbVdocOperador";
            this.cbVdocOperador.Size = new System.Drawing.Size(161, 15);
            this.cbVdocOperador.TabIndex = 16;
            this.cbVdocOperador.Text = "Documentos do Operador";
            this.cbVdocOperador.UseSelectable = true;
            // 
            // cbVresumoDiario
            // 
            this.cbVresumoDiario.AutoSize = true;
            this.cbVresumoDiario.Location = new System.Drawing.Point(243, 143);
            this.cbVresumoDiario.Name = "cbVresumoDiario";
            this.cbVresumoDiario.Size = new System.Drawing.Size(100, 15);
            this.cbVresumoDiario.TabIndex = 15;
            this.cbVresumoDiario.Text = "Resumo Diário";
            this.cbVresumoDiario.UseSelectable = true;
            // 
            // cbVresumoDiarioDoc
            // 
            this.cbVresumoDiarioDoc.AutoSize = true;
            this.cbVresumoDiarioDoc.Location = new System.Drawing.Point(243, 122);
            this.cbVresumoDiarioDoc.Name = "cbVresumoDiarioDoc";
            this.cbVresumoDiarioDoc.Size = new System.Drawing.Size(187, 15);
            this.cbVresumoDiarioDoc.TabIndex = 14;
            this.cbVresumoDiarioDoc.Text = "Resumo Diário por Documento";
            this.cbVresumoDiarioDoc.UseSelectable = true;
            // 
            // cbVlistagem
            // 
            this.cbVlistagem.AutoSize = true;
            this.cbVlistagem.Location = new System.Drawing.Point(243, 101);
            this.cbVlistagem.Name = "cbVlistagem";
            this.cbVlistagem.Size = new System.Drawing.Size(183, 15);
            this.cbVlistagem.TabIndex = 13;
            this.cbVlistagem.Text = "Listagem detalhada de Vendas";
            this.cbVlistagem.UseSelectable = true;
            // 
            // cbVvendasDinhVend
            // 
            this.cbVvendasDinhVend.AutoSize = true;
            this.cbVvendasDinhVend.Location = new System.Drawing.Point(243, 80);
            this.cbVvendasDinhVend.Name = "cbVvendasDinhVend";
            this.cbVvendasDinhVend.Size = new System.Drawing.Size(191, 15);
            this.cbVvendasDinhVend.TabIndex = 12;
            this.cbVvendasDinhVend.Text = "Vendas a dinheiro por vendedor";
            this.cbVvendasDinhVend.UseSelectable = true;
            // 
            // cbVvendasDinhOp
            // 
            this.cbVvendasDinhOp.AutoSize = true;
            this.cbVvendasDinhOp.Location = new System.Drawing.Point(33, 164);
            this.cbVvendasDinhOp.Name = "cbVvendasDinhOp";
            this.cbVvendasDinhOp.Size = new System.Drawing.Size(192, 15);
            this.cbVvendasDinhOp.TabIndex = 11;
            this.cbVvendasDinhOp.Text = "Vendas a Dinheiro por Operador";
            this.cbVvendasDinhOp.UseSelectable = true;
            // 
            // cbVmovimentoArtigo
            // 
            this.cbVmovimentoArtigo.AutoSize = true;
            this.cbVmovimentoArtigo.Location = new System.Drawing.Point(33, 143);
            this.cbVmovimentoArtigo.Name = "cbVmovimentoArtigo";
            this.cbVmovimentoArtigo.Size = new System.Drawing.Size(147, 15);
            this.cbVmovimentoArtigo.TabIndex = 10;
            this.cbVmovimentoArtigo.Text = "Movimentos por Artigo";
            this.cbVmovimentoArtigo.UseSelectable = true;
            // 
            // cbVacumuladosFamilia
            // 
            this.cbVacumuladosFamilia.AutoSize = true;
            this.cbVacumuladosFamilia.Location = new System.Drawing.Point(33, 122);
            this.cbVacumuladosFamilia.Name = "cbVacumuladosFamilia";
            this.cbVacumuladosFamilia.Size = new System.Drawing.Size(152, 15);
            this.cbVacumuladosFamilia.TabIndex = 9;
            this.cbVacumuladosFamilia.Text = "Acumulados por Familia";
            this.cbVacumuladosFamilia.UseSelectable = true;
            // 
            // cbVacumuladosArmazem
            // 
            this.cbVacumuladosArmazem.AutoSize = true;
            this.cbVacumuladosArmazem.Location = new System.Drawing.Point(33, 101);
            this.cbVacumuladosArmazem.Name = "cbVacumuladosArmazem";
            this.cbVacumuladosArmazem.Size = new System.Drawing.Size(165, 15);
            this.cbVacumuladosArmazem.TabIndex = 8;
            this.cbVacumuladosArmazem.Text = "Acumulados por Armazém";
            this.cbVacumuladosArmazem.UseSelectable = true;
            // 
            // cbVacumulados
            // 
            this.cbVacumulados.AutoSize = true;
            this.cbVacumulados.Location = new System.Drawing.Point(33, 80);
            this.cbVacumulados.Name = "cbVacumulados";
            this.cbVacumulados.Size = new System.Drawing.Size(198, 15);
            this.cbVacumulados.TabIndex = 7;
            this.cbVacumulados.Text = "Acumulados por Doc e Armazém";
            this.cbVacumulados.UseSelectable = true;
            this.cbVacumulados.CheckedChanged += new System.EventHandler(this.cbALArmazemVenda_CheckedChanged);
            // 
            // Inventario
            // 
            this.Inventario.HorizontalScrollbarBarColor = true;
            this.Inventario.HorizontalScrollbarHighlightOnWheel = false;
            this.Inventario.HorizontalScrollbarSize = 10;
            this.Inventario.Location = new System.Drawing.Point(4, 38);
            this.Inventario.Name = "Inventario";
            this.Inventario.Size = new System.Drawing.Size(1205, 338);
            this.Inventario.TabIndex = 1;
            this.Inventario.Text = "Inventário";
            this.Inventario.VerticalScrollbarBarColor = true;
            this.Inventario.VerticalScrollbarHighlightOnWheel = false;
            this.Inventario.VerticalScrollbarSize = 10;
            // 
            // Artigo
            // 
            this.Artigo.HorizontalScrollbarBarColor = true;
            this.Artigo.HorizontalScrollbarHighlightOnWheel = false;
            this.Artigo.HorizontalScrollbarSize = 10;
            this.Artigo.Location = new System.Drawing.Point(4, 38);
            this.Artigo.Name = "Artigo";
            this.Artigo.Size = new System.Drawing.Size(1205, 338);
            this.Artigo.TabIndex = 2;
            this.Artigo.Text = "Artigo";
            this.Artigo.VerticalScrollbarBarColor = true;
            this.Artigo.VerticalScrollbarHighlightOnWheel = false;
            this.Artigo.VerticalScrollbarSize = 10;
            // 
            // Movimentos
            // 
            this.Movimentos.HorizontalScrollbarBarColor = true;
            this.Movimentos.HorizontalScrollbarHighlightOnWheel = false;
            this.Movimentos.HorizontalScrollbarSize = 10;
            this.Movimentos.Location = new System.Drawing.Point(4, 38);
            this.Movimentos.Name = "Movimentos";
            this.Movimentos.Size = new System.Drawing.Size(1205, 338);
            this.Movimentos.TabIndex = 3;
            this.Movimentos.Text = "Movimentos";
            this.Movimentos.VerticalScrollbarBarColor = true;
            this.Movimentos.VerticalScrollbarHighlightOnWheel = false;
            this.Movimentos.VerticalScrollbarSize = 10;
            // 
            // MapaEstatico
            // 
            this.MapaEstatico.HorizontalScrollbarBarColor = true;
            this.MapaEstatico.HorizontalScrollbarHighlightOnWheel = false;
            this.MapaEstatico.HorizontalScrollbarSize = 10;
            this.MapaEstatico.Location = new System.Drawing.Point(4, 38);
            this.MapaEstatico.Name = "MapaEstatico";
            this.MapaEstatico.Size = new System.Drawing.Size(1205, 338);
            this.MapaEstatico.TabIndex = 4;
            this.MapaEstatico.Text = "Mapa Estático";
            this.MapaEstatico.VerticalScrollbarBarColor = true;
            this.MapaEstatico.VerticalScrollbarHighlightOnWheel = false;
            this.MapaEstatico.VerticalScrollbarSize = 10;
            // 
            // Tabelas
            // 
            this.Tabelas.HorizontalScrollbarBarColor = true;
            this.Tabelas.HorizontalScrollbarHighlightOnWheel = false;
            this.Tabelas.HorizontalScrollbarSize = 10;
            this.Tabelas.Location = new System.Drawing.Point(4, 38);
            this.Tabelas.Name = "Tabelas";
            this.Tabelas.Size = new System.Drawing.Size(1205, 338);
            this.Tabelas.TabIndex = 5;
            this.Tabelas.Text = "Tabelas";
            this.Tabelas.VerticalScrollbarBarColor = true;
            this.Tabelas.VerticalScrollbarHighlightOnWheel = false;
            this.Tabelas.VerticalScrollbarSize = 10;
            // 
            // ReemissãoGuias
            // 
            this.ReemissãoGuias.HorizontalScrollbarBarColor = true;
            this.ReemissãoGuias.HorizontalScrollbarHighlightOnWheel = false;
            this.ReemissãoGuias.HorizontalScrollbarSize = 10;
            this.ReemissãoGuias.Location = new System.Drawing.Point(4, 38);
            this.ReemissãoGuias.Name = "ReemissãoGuias";
            this.ReemissãoGuias.Size = new System.Drawing.Size(1205, 338);
            this.ReemissãoGuias.TabIndex = 6;
            this.ReemissãoGuias.Text = "Reemissão de guias";
            this.ReemissãoGuias.VerticalScrollbarBarColor = true;
            this.ReemissãoGuias.VerticalScrollbarHighlightOnWheel = false;
            this.ReemissãoGuias.VerticalScrollbarSize = 10;
            // 
            // ValoresStock
            // 
            this.ValoresStock.HorizontalScrollbarBarColor = true;
            this.ValoresStock.HorizontalScrollbarHighlightOnWheel = false;
            this.ValoresStock.HorizontalScrollbarSize = 10;
            this.ValoresStock.Location = new System.Drawing.Point(4, 38);
            this.ValoresStock.Name = "ValoresStock";
            this.ValoresStock.Size = new System.Drawing.Size(1205, 338);
            this.ValoresStock.TabIndex = 7;
            this.ValoresStock.Text = "Valores em Stock";
            this.ValoresStock.VerticalScrollbarBarColor = true;
            this.ValoresStock.VerticalScrollbarHighlightOnWheel = false;
            this.ValoresStock.VerticalScrollbarSize = 10;
            // 
            // EntArtigos
            // 
            this.EntArtigos.HorizontalScrollbarBarColor = true;
            this.EntArtigos.HorizontalScrollbarHighlightOnWheel = false;
            this.EntArtigos.HorizontalScrollbarSize = 10;
            this.EntArtigos.Location = new System.Drawing.Point(4, 38);
            this.EntArtigos.Name = "EntArtigos";
            this.EntArtigos.Size = new System.Drawing.Size(1205, 338);
            this.EntArtigos.TabIndex = 8;
            this.EntArtigos.Text = "Ent/Transf. Artigos";
            this.EntArtigos.VerticalScrollbarBarColor = true;
            this.EntArtigos.VerticalScrollbarHighlightOnWheel = false;
            this.EntArtigos.VerticalScrollbarSize = 10;
            // 
            // FolhaEspelho
            // 
            this.FolhaEspelho.HorizontalScrollbarBarColor = true;
            this.FolhaEspelho.HorizontalScrollbarHighlightOnWheel = false;
            this.FolhaEspelho.HorizontalScrollbarSize = 10;
            this.FolhaEspelho.Location = new System.Drawing.Point(4, 38);
            this.FolhaEspelho.Name = "FolhaEspelho";
            this.FolhaEspelho.Size = new System.Drawing.Size(1205, 338);
            this.FolhaEspelho.TabIndex = 9;
            this.FolhaEspelho.Text = "Folha espelho";
            this.FolhaEspelho.VerticalScrollbarBarColor = true;
            this.FolhaEspelho.VerticalScrollbarHighlightOnWheel = false;
            this.FolhaEspelho.VerticalScrollbarSize = 10;
            // 
            // TransfPendentes
            // 
            this.TransfPendentes.HorizontalScrollbarBarColor = true;
            this.TransfPendentes.HorizontalScrollbarHighlightOnWheel = false;
            this.TransfPendentes.HorizontalScrollbarSize = 10;
            this.TransfPendentes.Location = new System.Drawing.Point(4, 38);
            this.TransfPendentes.Name = "TransfPendentes";
            this.TransfPendentes.Size = new System.Drawing.Size(1205, 338);
            this.TransfPendentes.TabIndex = 10;
            this.TransfPendentes.Text = "Transf. Pendentes";
            this.TransfPendentes.VerticalScrollbarBarColor = true;
            this.TransfPendentes.VerticalScrollbarHighlightOnWheel = false;
            this.TransfPendentes.VerticalScrollbarSize = 10;
            // 
            // CurvaAbc
            // 
            this.CurvaAbc.HorizontalScrollbarBarColor = true;
            this.CurvaAbc.HorizontalScrollbarHighlightOnWheel = false;
            this.CurvaAbc.HorizontalScrollbarSize = 10;
            this.CurvaAbc.Location = new System.Drawing.Point(4, 38);
            this.CurvaAbc.Name = "CurvaAbc";
            this.CurvaAbc.Size = new System.Drawing.Size(1205, 338);
            this.CurvaAbc.TabIndex = 11;
            this.CurvaAbc.Text = "Curva ABC";
            this.CurvaAbc.VerticalScrollbarBarColor = true;
            this.CurvaAbc.VerticalScrollbarHighlightOnWheel = false;
            this.CurvaAbc.VerticalScrollbarSize = 10;
            // 
            // ApoioGestão
            // 
            this.ApoioGestão.HorizontalScrollbarBarColor = true;
            this.ApoioGestão.HorizontalScrollbarHighlightOnWheel = false;
            this.ApoioGestão.HorizontalScrollbarSize = 10;
            this.ApoioGestão.Location = new System.Drawing.Point(4, 38);
            this.ApoioGestão.Name = "ApoioGestão";
            this.ApoioGestão.Size = new System.Drawing.Size(1205, 338);
            this.ApoioGestão.TabIndex = 12;
            this.ApoioGestão.Text = "Apoio a Gestão";
            this.ApoioGestão.VerticalScrollbarBarColor = true;
            this.ApoioGestão.VerticalScrollbarHighlightOnWheel = false;
            this.ApoioGestão.VerticalScrollbarSize = 10;
            // 
            // MapaComparativo
            // 
            this.MapaComparativo.HorizontalScrollbarBarColor = true;
            this.MapaComparativo.HorizontalScrollbarHighlightOnWheel = false;
            this.MapaComparativo.HorizontalScrollbarSize = 10;
            this.MapaComparativo.Location = new System.Drawing.Point(4, 38);
            this.MapaComparativo.Name = "MapaComparativo";
            this.MapaComparativo.Size = new System.Drawing.Size(1205, 338);
            this.MapaComparativo.TabIndex = 13;
            this.MapaComparativo.Text = "Mapa comparativo";
            this.MapaComparativo.VerticalScrollbarBarColor = true;
            this.MapaComparativo.VerticalScrollbarHighlightOnWheel = false;
            this.MapaComparativo.VerticalScrollbarSize = 10;
            // 
            // Graficos
            // 
            this.Graficos.HorizontalScrollbarBarColor = true;
            this.Graficos.HorizontalScrollbarHighlightOnWheel = false;
            this.Graficos.HorizontalScrollbarSize = 10;
            this.Graficos.Location = new System.Drawing.Point(4, 38);
            this.Graficos.Name = "Graficos";
            this.Graficos.Size = new System.Drawing.Size(1205, 338);
            this.Graficos.TabIndex = 14;
            this.Graficos.Text = "Graficos";
            this.Graficos.VerticalScrollbarBarColor = true;
            this.Graficos.VerticalScrollbarHighlightOnWheel = false;
            this.Graficos.VerticalScrollbarSize = 10;
            // 
            // FormRelatorioPOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 629);
            this.ControlBox = false;
            this.Controls.Add(this.metroTabControl1);
            this.Name = "FormRelatorioPOS";
            this.Text = "Relatórios P.O.S";
            this.TransparencyKey = System.Drawing.Color.Empty;
            this.Load += new System.EventHandler(this.FormRelatorioPOS_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.Vendas.ResumeLayout(false);
            this.Vendas.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        //private Telerik.WinControls.Themes.TelerikMetroTheme telerikMetroTheme1;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage Vendas;
        private MetroFramework.Controls.MetroTabPage Inventario;
        private MetroFramework.Controls.MetroTabPage Artigo;
        private MetroFramework.Controls.MetroTabPage Movimentos;
        private MetroFramework.Controls.MetroTabPage MapaEstatico;
        private MetroFramework.Controls.MetroTabPage Tabelas;
        private MetroFramework.Controls.MetroTabPage ReemissãoGuias;
        private MetroFramework.Controls.MetroTabPage ValoresStock;
        private MetroFramework.Controls.MetroTabPage EntArtigos;
        private MetroFramework.Controls.MetroTabPage FolhaEspelho;
        private MetroFramework.Controls.MetroTabPage TransfPendentes;
        private MetroFramework.Controls.MetroTabPage CurvaAbc;
        private MetroFramework.Controls.MetroTabPage ApoioGestão;
        private MetroFramework.Controls.MetroTabPage MapaComparativo;
        private MetroFramework.Controls.MetroTabPage Graficos;
        private MetroFramework.Controls.MetroCheckBox cbVacumulados;
        private MetroFramework.Controls.MetroCheckBox cbVListagemMov;
        private MetroFramework.Controls.MetroCheckBox cbVfaturacaoSemanaDia;
        private MetroFramework.Controls.MetroCheckBox cbVfaturaDiaHora;
        private MetroFramework.Controls.MetroCheckBox cbVresumoPerVendF;
        private MetroFramework.Controls.MetroCheckBox cbVresumoPerVend;
        private MetroFramework.Controls.MetroCheckBox cbVresumoPerOP;
        private MetroFramework.Controls.MetroCheckBox cbVdocDesc;
        private MetroFramework.Controls.MetroCheckBox cbVdocOperador;
        private MetroFramework.Controls.MetroCheckBox cbVresumoDiario;
        private MetroFramework.Controls.MetroCheckBox cbVresumoDiarioDoc;
        private MetroFramework.Controls.MetroCheckBox cbVlistagem;
        private MetroFramework.Controls.MetroCheckBox cbVvendasDinhVend;
        private MetroFramework.Controls.MetroCheckBox cbVvendasDinhOp;
        private MetroFramework.Controls.MetroCheckBox cbVmovimentoArtigo;
        private MetroFramework.Controls.MetroCheckBox cbVacumuladosFamilia;
        private MetroFramework.Controls.MetroCheckBox cbVacumuladosArmazem;
    }
}