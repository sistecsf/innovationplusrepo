﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using Excel = Microsoft.Office.Interop.Excel;
using System.Threading;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS.POS
{
    public partial class FormRequisicaoInterna : MetroFramework.Forms.MetroForm
    {
        Microsoft.Office.Interop.Excel.Application XcelApp = new Microsoft.Office.Interop.Excel.Application();
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand comd;
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public static string codLojaRI;
        string sql, codLoja, dataInicio, dataFinal, nIDUnicoGI, strCdLoja, codCliente;
        string CODIGOLJ;
        int nTamanho, nRowId;
        DateTime data;
        int tipo;
        string pstrCodigoX, strIDunico, nNumdoc, CODLOJAx;
        bool externa;
        string codLojaConsulta = Variavel.codLoja;
        public FormRequisicaoInterna()
        {
            InitializeComponent();
        }
        private void calcular()
        {
            fEncomendaX(dgEncomendas);
            fGetStockOrigem(dgEncomendas);
            fPopulateY(dgEncomendas, dgEncomendas1, dgEncomendas2);
        }
        private void prenxerGridEntradaGuia()
        {
            sql = @"Select LTrim(RTrim(NomeArz)), Cast(A.Referenc As varchar(50)), LTrim(RTrim(A.DescrArtigo)), M.QTD, A.FOB, M.PCLD, M.QTD*M.PCLD From ASMESTRE A With(NoLock), ASFICMOV1 M With(NoLock), ASARMAZ Z With(NoLock)" +
                              "Where IDunico='" + nIDUnicoGI + "' And A.CODLOJA=M.CODLOJA AND A.CODARMZ=M.CODARMZ AND A.REFERENC=M.REFERENC And Z.CodLoja=A.CodLoja And Z.CODARMZ=A.CODARMZ " +
                                "And CODDOC='GI' AND LEFT(M.DATACRIA,10) >= '" + dataInicio + "' AND LEFT(M.DATACRIA,10) <= '" + dataFinal + "' ";

            if (cmbExEntidadeFilial.Text != "" && cmbExEntidadeFilial.Text != null)
                sql = sql + "And M.CODENTIDADE = '"+ codCliente +"' ";
            sql = sql + " ORDER BY LTrim(RTrim(NomeArz)), A.Referenc";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dgEntradaGuia.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGridL1()
        {
            sql = @"SELECT R.NUMDOC, E.NOMEENTIDADE, LTrim(RTrim(L.NOMELOJA)), R.IDUNICO From AREGDOC R WITH (NOLOCK), AFENTIDADE E WITH (NOLOCK), ASLOJA L WITH (NOLOCK) " +
                              "Where R.IDUNICO='" + nIDUnicoGI + "' And RTRIM(R.CODENTIDADE)=RTRIM(E.CODENTIDADE) And RTRIM(R.CODLOJA)=RTRIM(L.CODLOJA) And RTRIM(R.CODDOC)='GI' " +
                                "And LEFT(R.DATACRIA,10) >= '"+ dataInicio +"' And LEFT(R.DATACRIA,10) <= '"+ dataFinal +"' AND R.CODENTIDADE = '"+ codCliente +"' ORDER BY L.NOMELOJA, R.DATACRIA";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dgL1.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGriddgDetail()
        {
            sql = @"SELECT IDunico, NUMDOC,CodLoja, CodArmz, CAST (Referenc AS VARCHAR (50)), GUIA,AltStok, Cast(DataCria As DateTime), FOB, PCL, PVP, " +
                              "Descont, QTD, CodEntidade, TIPMOV, POSTO, Moeda, ANULADOR, CodDoc, Anulado, CodUtilizador, CodVend, LojaDestino, PCLD, NumDocFRN, CodFornec, PVPD, ARMZDESTINO, Obs, CodAO, Data_Lancamento, DATA_INV, QTD_INV FROM ASFICMOV1 M WITH (NOLOCK) " +
                                "Where IDunico = '" + nIDUnicoGI + "' And CodDoc = 'GI' AND LEFT(DATACRIA,10) >= '" + dataInicio + "' AND LEFT(DATACRIA,10) <= '" + dataFinal + "' AND M.CODLOJA = '" + strCdLoja + "' AND M.CODENTIDADE = '" + codCliente + "' ORDER BY M.DATACRIA";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dgDetail.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGriddgArt()
        {
            sql = @"SELECT M.IDUNICO, CAST (A.Referenc AS VARCHAR (50)), A.CodLoja, A.CodArmz, A.Codfam, A.Codsubfam, A.Descrartigo, A.Descr_sup, A.Codbar, A.Qtd, A.FOB, A.PCA, A.PVP, Cast(A.Datacria As DateTime), A.Unidade, A.Desembalagem, A.Alterapreco, A.Contastocks, A.Stocknegativo, A.Devolver, A.Stockmax, A.Stockmin, A.Stocktotal, A.Obs, A.Data_fabrico, A.Pmc, A.Regequip, A.Codutilizador, " +
                              "IsNull(A.Data_Expira,Convert(Varchar(10),DateAdd(year, 1, GetDate()), 120)), A.Desconto, A.Lucro, A.Codmarca, A.Codmodelo, A.Tiposervico, A.Permitir_obs, A.Activo, A.Pontoencomenda FROM ASMESTRE A WITH(NOLOCK), ASFICMOV1 M WITH (NOLOCK) Where IDunico = '" + nIDUnicoGI + "' And A.CODLOJA=M.CODLOJA " +
                                "AND A.CODARMZ=M.CODARMZ AND A.REFERENC=M.REFERENC AND CODDOC='GI' AND LEFT(M.DATACRIA,10) >= '"+ dataInicio +"' AND LEFT(M.DATACRIA,10) <= '"+ dataFinal +"' AND M.CODLOJA = '"+ strCdLoja +"' AND M.CODENTIDADE = '"+ codCliente +"' ORDER BY M.DATACRIA";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dgArt.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGriddgLoja()
        {
            sql = @"Select DISTINCT L.Codloja, L.Datacria, L.Nomeloja, L.Datamov, L.Flag, L.Codutilizador, L.Morada, L.Caixa_Post, L.Fone, L.Fax, L.Email, L.Lojaonline, L.Codconta, L.CodcontaPdr, L.CodcontaNac, L.CodcontaOutros, " +
                              "L.Data_alteracao, L.Alterado_por, L.Display_conta, L.Agtelec from ASLOJA L WITH (NOLOCK), Aregdoc R WITH (NOLOCK) " +
                                "Where R.IDunico= '"+ nIDUnicoGI +"' And R.CODLOJA=L.CODLOJA AND R.CODDOC = 'GI' AND LEFT(R.DATACRIA,10) >= '"+ dataInicio +"' AND LEFT(R.DATACRIA,10) <= '"+ dataFinal +"' AND R.CODLOJA = '"+ strCdLoja +"' AND R.CODENTIDADE = '"+ codCliente +"' ORDER BY L.Codloja, L.Nomeloja";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dgLoja.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGriddgArma()
        {
            sql = @"Select DISTINCT A.Codloja, A.Codarmz, A.Nomearz, A.Cabecalho, A.Rodape, A.Morada, A.Codutilizador, A.ContVend, A.ContStock, A.ContCusto, A.Datacria, A.Data_Alteracao, A.Alterado_por, A.Codao, A.Arma_venda, A.Arma_remoto " +
                              "from ASARMAZ A WITH (NOLOCK), ASLOJA L WITH (NOLOCK), Aregdoc R WITH (NOLOCK), ASficmov1 M WITH (NOLOCK) Where R.IDunico= '"+ nIDUnicoGI +"' And M.IDUNICO=R.IDUNICO AND M.CODLOJA=L.CODLOJA " +
                                "AND M.CODARMZ=A.CODARMZ AND R.CODLOJA=L.CODLOJA AND A.CODLOJA=L.CODLOJA AND R.CODDOC = 'GI' AND LEFT(R.DATACRIA,10) >= '"+ dataInicio +"' AND LEFT(R.DATACRIA,10) <= '"+ dataFinal +"' AND R.CODLOJA = '"+ strCdLoja +"' AND R.CODENTIDADE = '"+ codCliente +"' ORDER BY A.Codloja, A.Nomearz";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dgArma.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGriddgFam()
        {
            sql = @"Select DISTINCT F.Codfam, F.ContVend, F.ContStok, F.ContCusto, F.Nomemerc, F.Datacria, F.Codutilizador, F.Desconto, F.Data_alteracao, F.Alterado_por " +
                              "FROM ASFAMILIA F WITH(NOLOCK), ASFICMOV1 M WITH(NOLOCK), ASMESTRE A WITH(NOLOCK) WHERE IDunico= '"+ nIDUnicoGI +"' And A.CODFAM=F.CODFAM AND A.CODLOJA=M.CODLOJA AND A.CODARMZ=M.CODARMZ AND A.REFERENC=M.REFERENC " +
                                "AND M.CODDOC='GI' AND LEFT(M.DATACRIA,10) >= '"+ dataInicio +"' AND LEFT(M.DATACRIA,10) <= '"+ dataFinal +"' AND M.CODLOJA = '"+ strCdLoja +"' AND M.CODENTIDADE = '"+ codCliente +"' ORDER BY F.Codfam, F.Nomemerc";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dgFam.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGriddgSubFam()
        {
            sql = @"Select DISTINCT S.CodSubFam, S.CodFam, S.Descricao, M.DataCria, S.CodUtilizador From ASSUBFAMILIA S WITH(NOLOCK), ASFAMILIA F WITH(NOLOCK), ASFICMOV1 M WITH(NOLOCK), ASMESTRE A WITH(NOLOCK) " +
                              "WHERE IDunico= '"+ nIDUnicoGI +"' And S.CODFAM=F.CODFAM AND S.CODFAM=A.CODFAM AND S.CODSUBFAM=A.CODSUBFAM AND M.CODLOJA=A.CODLOJA AND M.CODARMZ=A.CODARMZ " +
                                "AND M.REFERENC=A.REFERENC AND M.CODDOC='GI' AND LEFT(M.DATACRIA,10) >= '"+ dataInicio +"' AND LEFT(M.DATACRIA,10) <= '"+ dataFinal +"' AND M.CODLOJA = '"+ strCdLoja +"' AND M.CODENTIDADE = '"+ codCliente +"' ORDER BY S.CodSubFam , S.Descricao";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dgSubFam.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGriddgMarca()
        {
            sql = @"Select DISTINCT C.CODMARCA, C.DESCRMARCA FROM AEMARCA C WITH(NOLOCK), ASFICMOV1 M WITH(NOLOCK), AEFICHAS F WITH(NOLOCK) " +
                              "WHERE M.IDunico= '"+nIDUnicoGI +"' And M.CODLOJA=F.CODLOJA AND M.REFERENC=F.REFERENC AND M.IDUNICO=F.IDUNICO_SAIDA AND F.CODMARCA=C.CODMARCA AND M.CODDOC='GI' " +
                                "AND LEFT(M.DATACRIA,10) >= '"+ dataInicio +"' AND LEFT(M.DATACRIA,10) <= '"+ dataFinal +"' AND M.CODLOJA = '"+ strCdLoja +"' AND M.CODENTIDADE = '"+ codCliente +"' ORDER BY C.CODMARCA, C.DESCRMARCA";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dgMarca.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGriddgModelo()
        {
            sql = @"Select D.CODMARCA, D.CODMODELO, D.DESCRMODELO FROM AEMARCA C WITH(NOLOCK), ASFICMOV1 M WITH(NOLOCK), AEFICHAS F WITH(NOLOCK), AEMODELO D WITH(NOLOCK) " +
                              "WHERE M.IDunico= '"+nIDUnicoGI +"' And M.CODLOJA=F.CODLOJA AND M.REFERENC=F.REFERENC AND M.IDUNICO=F.IDUNICO_SAIDA AND F.CODMARCA=C.CODMARCA AND D.CODMARCA=C.CODMARCA AND F.CODMODELO=D.CODMODELO AND M.CODDOC='GI' " +
                                "AND LEFT(M.DATACRIA,10) >= '"+ dataInicio +"' AND LEFT(M.DATACRIA,10) <= '"+ dataFinal +"' AND M.CODLOJA = '"+ strCdLoja +"' AND M.CODENTIDADE = '"+ codCliente +"' ORDER BY D.CODMARCA, D.CODMODELO, D.DESCRMODELO";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dgModelo.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGriddgFicha()
        {
            sql = @"Select DISTINCT F.CODMARCA, F.CODMODELO, F.NSERIE, F.REFERENC, F.CODUTILIZADOR, Cast(F.DataCria As DateTime), F.FOB, F.CODENTIDADE,F.IDUNICO_SAIDA, F.DATAGARANTIA,F.CODLOJA,F.CODARMZ, F.TEL_SIM FROM ASFICMOV1 M WITH(NOLOCK), AEFICHAS F WITH(NOLOCK) " +
                              "Where M.IDunico= '"+ nIDUnicoGI +"' And M.CODLOJA=F.CODLOJA AND M.REFERENC=F.REFERENC AND M.IDUNICO=F.IDUNICO_SAIDA AND M.CODDOC='GI' " +
                                "AND LEFT(M.DATACRIA,10) >= '"+ dataInicio +"' AND LEFT(M.DATACRIA,10) <= '"+ dataFinal +"' AND M.CODLOJA = '"+ strCdLoja +"' AND M.CODENTIDADE = '"+ codCliente +"' ORDER BY F.CODMARCA, F.CODMODELO, F.NSERIE, F.REFERENC";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dgFicha.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGrid()
        {
            prenxerGridEntradaGuia();
            prenxerGridL1();
            prenxerGriddgDetail();
            prenxerGriddgArt();
            prenxerGriddgLoja();
            prenxerGriddgArma();
            prenxerGriddgFam();
            prenxerGriddgSubFam();
            prenxerGriddgMarca();
            prenxerGriddgModelo();
            prenxerGriddgFicha();
        }
        private void preenxeGring1(DataGridView dataGrid)
        {
            int nDias, nPercentagem;
            try { nDias = Convert.ToInt32(txtEnNDias.Text); }
            catch (Exception) { nDias = 30; }
            try { nPercentagem = Convert.ToInt32(txtEnPercentualSaida.Text); }
            catch (Exception) { nPercentagem = 40; }
            //nDias = 20; nPercentagem = 20;
            sql = @"Select Distinct LTrim(RTrim(CodLoja)), LTrim(RTrim(xCODIGOLOJA)), CodArmz, B.REFERENC, DESCRARTIGO, STOCK, QTDSAIDA, PERCENTAGEM, PCA, FOB, PVP, B.LOCALI, IsNull(QTD_ENCOMENDA,0) " +
                              "From (SeLect Referenc, Min(CodArmz) As CodigoArmz From fn_POS_ENCOMENDAS ('" + lbELoja.Text + "', '" + nDias + "') Group By Referenc) As A Inner Join fn_POS_ENCOMENDAS ('" + lbELoja.Text + "', '" + nDias + "') As B On B.Referenc = A.Referenc " +
                                "And CodArmz = CodigoArmz And QTDSAIDA > 0 And PERCENTAGEM > '" + nPercentagem + "' And CODLOJA='" + lbELoja.Text + "' Where QTDSAIDA>0 And PERCENTAGEM > '" + nPercentagem + "' And CODLOJA = '" + lbELoja.Text + "' Order By LTrim(RTrim(xCODIGOLOJA)), B.LOCALI, B.REFERENC";

            //sql = @"Select Distinct TOP 10 LTrim(RTrim(CodLoja)), LTrim(RTrim(xCODIGOLOJA)), CodArmz, B.REFERENC, DESCRARTIGO, STOCK, QTDSAIDA, PERCENTAGEM, PCA, FOB, PVP, B.LOCALI, IsNull(QTD_ENCOMENDA,0) " +
              //"From (SeLect Referenc, Min(CodArmz) As CodigoArmz From fn_POS_ENCOMENDAS ('INF', '10') Group By Referenc) As A Inner Join fn_POS_ENCOMENDAS ('INF', '10') As B On B.Referenc = A.Referenc " +
                //"And CodArmz = CodigoArmz And QTDSAIDA > 0 And PERCENTAGEM > '20' And CODLOJA = 'INF' Where QTDSAIDA>0 And PERCENTAGEM > '20' And CODLOJA = 'INF' Order By LTrim(RTrim(xCODIGOLOJA)), B.LOCALI, B.REFERENC";
            //MessageBox.Show("1");
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataAdapter.SelectCommand.CommandTimeout = 9000;
                //MessageBox.Show("2");
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dataGrid.DataSource = dataView;
                //MessageBox.Show("3");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            for (int i = 0; i < dataGrid.RowCount - 1; i++)
            {
                dataGrid[16, i].Value = "S";
                dataGrid[8, i].Value = Convert.ToDouble(dataGrid[8, i].Value.ToString()) - ((Convert.ToDouble(dataGrid[8, i].Value.ToString()) / 100) * Convert.ToDouble(lbECusto.Text));
                dataGrid[13, i].Value = dataGrid[8, i].Value.ToString();

                if (txtEnDiasRepor.Text == "")
                {
                    if (Convert.ToInt32(dataGrid[7, i].Value.ToString()) > Convert.ToInt32(dataGrid[5, i].Value.ToString()))
                        dataGrid[6, i].Value = Convert.ToInt32(dataGrid[7, i].Value.ToString()) - Convert.ToInt32(dataGrid[5, i].Value.ToString());
                    else if (Convert.ToInt32(dataGrid[7, i].Value.ToString()) < Convert.ToInt32(dataGrid[5, i].Value.ToString()))
                        dataGrid[6, i].Value = Convert.ToInt32(dataGrid[5, i].Value.ToString()) - Convert.ToInt32(dataGrid[7, i].Value.ToString());
                    else if (Convert.ToInt32(dataGrid[7, i].Value.ToString()) == Convert.ToInt32(dataGrid[5, i].Value.ToString()))
                        dataGrid[6, i].Value = dataGrid[5, i].Value.ToString();
                }
                else
                {
                    dataGrid[6, i].Value = Convert.ToInt32(txtEnDiasRepor.Text) * Convert.ToDouble(dataGrid[7, i].Value.ToString()) / nDias;
                    dataGrid[6, i].Value = dataGrid[6, i].Value.ToString().Trim();
                    if (dataGrid[6, i].Value.ToString() == "0")
                        dataGrid[6, i].Value = 1 * 2;
                }
                dataGrid[8, i].Value = Convert.ToDouble(dataGrid[8, i].Value.ToString()) * Convert.ToDouble(dataGrid[6, i].Value.ToString());
                dataGrid[18, i].Value = "" + nDias + " DIAS" + " -- " + nPercentagem + " %";
            }
        }
        private void DesativarAutoGenerateColumnsGrid(DataGridView dataGrid, DataGridView dataGrid1, DataGridView dataGrid2, DataGridView dataGrid3, DataGridView dataGrid4, DataGridView dataGrid5,
            DataGridView dataGrid6, DataGridView dataGrid7, DataGridView dataGrid8, DataGridView dataGrid9, DataGridView dataGrid10, DataGridView dataGrid11, DataGridView dataGrid12)
        {
            dataGrid.AutoGenerateColumns = false;
            dataGrid1.AutoGenerateColumns = false;
            dataGrid2.AutoGenerateColumns = false;
            dataGrid3.AutoGenerateColumns = false;
            dataGrid4.AutoGenerateColumns = false;
            dataGrid5.AutoGenerateColumns = false;
            dataGrid6.AutoGenerateColumns = false;
            dataGrid7.AutoGenerateColumns = false;
            dataGrid8.AutoGenerateColumns = false;
            dataGrid9.AutoGenerateColumns = false;
            dataGrid10.AutoGenerateColumns = false;
            dataGrid11.AutoGenerateColumns = false;
            dataGrid12.AutoGenerateColumns = false;
        }
        private void CmbALoja(ComboBox combo)
        {
            SqlCommand cmd = new SqlCommand(@"select RTRIM(LTRIM(NomeLoja)) from ASLOJA WITH (NOLOCK)", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read()) { combo.Items.Add(leitor.GetValue(0)); }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }
        private void prenxeComboEntidadeFilial(ComboBox combo)
        {
            SqlCommand cmd = new SqlCommand(@"Select Distinct LTRIM(RTRIM(C.NOMEENTIDADE)) From AFENTIDADE C With (NoLock) Where (CodTipoCli='INT' Or CodTipoCli='FIL') And Localizacao_Fisica='EXTERNA' Order By LTRIM(RTRIM(C.NOMEENTIDADE))", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read()) { combo.Items.Add(leitor.GetValue(0)); }
            }
            catch (SqlException ex) { MessageBox.Show("Erro ao carregar a Combo Box" + ex.Message); }
            finally{conexao.Close();}
        }
        private void prenxeComboDIGI(ComboBox combo)
        {
            try { data = Convert.ToDateTime(txtExDataInicial.Text); }
            catch (Exception) { data = Convert.ToDateTime("1-1-1900"); }
            dataInicio = "" + data.Year + "-" + data.Month + "-" + data.Day;
            try { data = Convert.ToDateTime(txtExDataFinal.Text); }
            catch (Exception) { data = Convert.ToDateTime("1-1-1900"); }
            dataFinal = "" + data.Year + "-" + data.Month + "-" + data.Day + " " + "18:00";
            SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT RTRIM(LTRIM(IDUNICO)) From AREGDOC R With (NoLock) Where CODDOC = '" + Gerais.PARAM_CODDOC_GI + "' And CodEntidade = '" + codCliente + "' And IsNull(R.Actualizacao,'N') = 'N' And R.STATUS<>'A' And LEFT(R.DataCria,10) >= '" + dataInicio + "' And LEFT(R.DataCria,10) <= '" + dataFinal + "' ORDER BY RTRIM(LTRIM(R.IDUNICO))", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read()) { combo.Items.Add(leitor.GetValue(0)); }
            }
            catch (SqlException ex) { MessageBox.Show("Erro ao carregar a Combo Box" + ex.Message); }
            finally { conexao.Close(); }
        }
        private void exportarParaExcel1(DataGridView dataGrid, DataGridView dataGrid1)
        {
            
            SaveFileDialog salvar = new SaveFileDialog(); // novo SaveFileDialog;
 
            Excel.Application App; // Aplicação Excel
            Excel.Workbook WorkBook; // Pasta
            Excel.Worksheet WorkSheet; // Planilha
            object misValue = System.Reflection.Missing.Value;
 
            App = new Excel.Application();
            WorkBook = App.Workbooks.Add(misValue);
            WorkSheet = (Excel.Worksheet)WorkBook.Worksheets.get_Item(1);
            int i = 0;
            int j = 0;
 
           // passa as celulas do DataGridView para a Pasta do Excel
            for (i = 1; i < dataGrid.Columns.Count + 1; i++)
            {
                //DataGridViewCell cell = dataGrid.;
                WorkSheet.Cells[1, i] = dataGrid.Columns[i - 1].HeaderText;
                //XcelApp.Cells[1, i] = dataGrid.Columns[i - 1].HeaderText;
            }
            for (i = 0; i <= dataGrid.RowCount - 1; i++)
            {
                for (j = 0; j <= dataGrid.ColumnCount - 1; j++)
                {
                    
                    DataGridViewCell cell = dataGrid[j, i];
                    WorkSheet.Cells[i + 1, j + 1] = cell.Value;
                }
            }


            WorkSheet = (Excel.Worksheet)WorkBook.Worksheets.get_Item(2);

            // passa as celulas do DataGridView para a Pasta do Excel
            for (i = 0; i <= dataGrid1.RowCount - 1; i++)
            {
                for (j = 0; j <= dataGrid1.ColumnCount - 1; j++)
                {
                    DataGridViewCell cell = dataGrid1[j, i];
                    WorkSheet.Cells[i + 1, j + 1] = cell.Value;
                }
            }
 
            // define algumas propriedades da caixa salvar
            salvar.Title = "Exportar para Excel";
            salvar.Filter = "Arquivo do Excel *.xls | *.xls";
            salvar.ShowDialog(); // mostra
 
            // salva o arquivo
            WorkBook.SaveAs(salvar.FileName, Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue,
 
            Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            WorkBook.Close(true, misValue, misValue);
            App.Quit(); // encerra o excel
 
            MessageBox.Show("Exportado com sucesso!");
        }
        // =========== Função que cria uma planilha no Excel ==================================
        private void planilha(DataGridView dataGrid, Excel.Workbook WorkBook, Excel.Worksheet WorkSheet, int tam)
        {
            if(tam == 0)
                WorkSheet = (Excel.Worksheet)WorkBook.Worksheets.Add(); // adiciona mais uma página na planilha do excel
            else
                WorkSheet = (Excel.Worksheet)WorkBook.Worksheets.get_Item(tam); // usa uma página já existente na planilha do excel

            for (int i = 1; i < dataGrid.Columns.Count + 1; i++)
            {
                WorkSheet.Cells[1, i] = dataGrid.Columns[i - 1].HeaderText;
            }

            for (int i = 0; i < dataGrid.Rows.Count - 1; i++)
            {
                for (int j = 0; j < dataGrid.Columns.Count; j++)
                {
                    WorkSheet.Cells[i + 2, j + 1] = dataGrid.Rows[i].Cells[j].Value.ToString();
                }
            }
        }
        // ============= Função que tira os dados do DataGrid para o Excel ===========================================================
        private void exportarParaExcel(DataGridView dataGrid, DataGridView dataGrid1, DataGridView dataGrid2, DataGridView dataGrid3,
            DataGridView dataGrid4, DataGridView dataGrid5, DataGridView dataGrid6, DataGridView dataGrid7, DataGridView dataGrid8,
            DataGridView dataGrid9, DataGridView dataGrid10)
        {
            if (dataGrid.Rows.Count > 0)
            {
                try
                {
                    Excel.Workbook WorkBook;
                    Excel.Worksheet WorkSheet; // Planilha
                    object misValue = System.Reflection.Missing.Value;
                    WorkBook = XcelApp.Workbooks.Add(misValue);
                    WorkSheet = (Excel.Worksheet)WorkBook.Worksheets.get_Item(1);

                    planilha(dataGrid, WorkBook, WorkSheet, 1);
                    //=============================================================================//
                    planilha(dataGrid1, WorkBook, WorkSheet, 2);
                    //=============================================================================//
                    planilha(dataGrid2, WorkBook, WorkSheet, 3);
                    //=============================================================================//
                    planilha(dataGrid3, WorkBook, WorkSheet, 0);
                    //=============================================================================//
                    planilha(dataGrid4, WorkBook, WorkSheet, 0);
                    //=============================================================================//
                    planilha(dataGrid5, WorkBook, WorkSheet, 0);
                    //=============================================================================//
                    planilha(dataGrid6, WorkBook, WorkSheet, 0);
                    //=============================================================================//
                    planilha(dataGrid7, WorkBook, WorkSheet, 0);
                    //=============================================================================//
                    planilha(dataGrid8, WorkBook, WorkSheet, 0);
                    //=============================================================================//
                    planilha(dataGrid9, WorkBook, WorkSheet, 0);
                    //=============================================================================//
                    planilha(dataGrid10, WorkBook, WorkSheet, 0);
                    //=============================================================================//

                    XcelApp.Columns.AutoFit();
                    //
                    XcelApp.Visible = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro ao exportar os dados para o Excel : " + ex.Message);
                    XcelApp.Quit();
                }
            }
        }
        private void fGetStockOrigem(DataGridView dataGrid)
        {
            string strCodigoArm = "", QTD = "", LOCALI = "";
            try
            {
                conexao.Open();
                for (int i = 0; i < dataGrid.RowCount - 1; i++)
                {
                    strCodigoArm = "";
                    if (dataGrid[1, i].Value != null && dataGrid[1, i].Value.ToString() != "" && dataGrid[1, i].Value.ToString() != null)
                    {
                        //comd = new SqlCommand("Select Top 1 CodArmz, QTD, LTrim(RTrim(UPPER(LOCALI))), RTrim(CodLoja) From ASMESTRE With (NoLock) " +
                          //  "Where CODLOJA<>'" + dataGrid[1, i].Value.ToString().Trim() + "' And REFERENC= '" + dataGrid[3, i].Value.ToString().Trim() + "' And QTD>= '" + dataGrid[5, i].Value.ToString().Trim() + "' And CodArmz In (Select CodArmz From ASARMAZ Where CODLOJA <> '" + dataGrid[1, i].Value.ToString().Trim() + "' " +
                            //"And UPPER(IsNull(EMITIR_RI,'N'))='S') And CodLoja In (Select CodLoja From ASLOJA Where CODLOJA <> '" + dataGrid[1, i].Value.ToString().Trim() + "' And (IsNull(Flag,'S')='S'))", conexao);
                        comd = new SqlCommand("Select Top 1 CodArmz, QTD, LTrim(RTrim(UPPER(LOCALI))), RTrim(CodLoja) From ASMESTRE With (NoLock) " +
                            "Where CODLOJA<>'" + dataGrid[1, i].Value.ToString().Trim() + "' And REFERENC= '" + dataGrid[3, i].Value.ToString().Trim() + "' And QTD>= '" + dataGrid[5, i].Value.ToString().Trim() + "' And CodLoja In (Select CodLoja From ASLOJA Where CODLOJA <> '" + dataGrid[1, i].Value.ToString().Trim() + "' And (IsNull(Flag,'S')='S'))", conexao);

                        using (SqlDataReader reader = comd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                strCodigoArm = reader[0].ToString();
                                QTD = reader[1].ToString();
                                LOCALI = reader[2].ToString();
                                CODLOJAx = reader[3].ToString();
                            }
                        }
                    }

                    if (strCodigoArm != "" && strCodigoArm != null)
                    {
                        //MessageBox.Show(strCodigoArm);
                        dataGrid[15, i].Value = "RI";
                        dataGrid[14, i].Value = QTD;
                        dataGrid[11, i].Value = LOCALI;
                        dataGrid[23, i].Value = strCodigoArm;
                        
                    }
                    else { dataGrid[15, i].Value = "SE"; }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private string insertDouble(string valor)
        {
            double num = 0;
            if (valor != null && valor != "")
            {
                try { num = Convert.ToDouble(valor); }
                catch (Exception) { num = 0; }
            }
            return "" + num;
        }
        private int insertInt(string valor)
        {
            int num = 0;
            if (valor != null && valor != "")
            {
                try { num = Convert.ToInt32(valor); }
                catch (Exception) { num = 0; }
            }
            return num;
        }
        private string insertString(string valor)
        {
            if (valor != "" || valor != null)
                return valor;
            else
                return "";
        }
        private void fEncomendaX(DataGridView dataGrid)
        {
            int nDias, nPercentagem;
            Double PERCENTAGEM_CUSTOS;
            try {nDias = Convert.ToInt32(txtEnNDias.Text); }
            catch(Exception){nDias = 30;}
            try { nPercentagem = Convert.ToInt32(txtEnPercentualSaida.Text); }
            catch(Exception){nPercentagem = 40;}
            lbECusto.Text = Gerais.PARAM_COD_PERCENT_CUSTOS;
            try { PERCENTAGEM_CUSTOS = Convert.ToDouble(Gerais.PARAM_COD_PERCENT_CUSTOS); }
            catch (Exception ex) { PERCENTAGEM_CUSTOS = 0; }

            preenxeGring1(dataGrid);
        }
        private void insert1(DataGridView dataGrid)
        {
            SqlCommand[] comd = new SqlCommand[dataGrid.RowCount - 1];
            for (int i = 0; i < dataGrid.RowCount - 1; i++)
            {
                comd[i] = conexao.CreateCommand();
                comd[i].CommandText = "Insert Into xTemp.ASENCOMENDAS01 (CodLoja,CodArmz,                              Referenc,                                                                    DescrArtigo,                                               LOCALI,                    DataCria,                        CodUtilizador,                                         FOB,                                                                        PCLD,                                                                        PVPD,                                                                  QTD_STK,                                             QTD_SAIDA,                                              QTD_SUGESTAO,                               CODIGOLOJA_GE,                         CodigoArmzGE,                                    QTDGE,                                              QTD_ENCOMENDA,                   CodEntidade,              Indice,                              INDICEX) " +
                       "values ('" + lbELoja.Text + "', '" + insertString(dataGrid[2, i].Value.ToString()) + "', '" + insertString(dataGrid[3, i].Value.ToString()).Trim() + "', '" + insertString(dataGrid[4, i].Value.ToString()).Trim() + "', '" + dataGrid[11, i].Value + "',Convert(Varchar(20),GetDate(),120), '" + Variavel.codUtilizador + "', '" + insertDouble(dataGrid[9, i].Value.ToString()).Replace(',', '.') + "', '" + insertDouble(dataGrid[13, i].Value.ToString()).Replace(',', '.') + "', '" + insertDouble(dataGrid[10, i].Value.ToString()).Replace(',', '.') + "',  '" + insertInt(dataGrid[5, i].Value.ToString()) + "', '" + insertInt(dataGrid[7, i].Value.ToString()) + "', '" + insertInt(dataGrid[6, i].Value.ToString()) + "', '" + dataGrid[1, i].Value.ToString() + "', '" + dataGrid[23, i].Value + "', '" + insertInt(dataGrid[14, i].Value.ToString()) + "', '" + insertInt(dataGrid[12, i].Value.ToString()) + "',   'G1',       '" + dataGrid[19, i].Value + "',     '" + dataGrid[20, i].Value + "')";

            }

            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                for (int i = 0; i < dataGrid.RowCount - 1; i++)
                {
                    try
                    {
                        comd[i].Transaction = tran;
                        comd[i].ExecuteNonQuery();
                    }
                    catch (Exception ex) { /*MessageBox.Show(ex.Message);*/ }
                }

                tran.Commit();
                MessageBox.Show("Gravado com sucesso!");
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void insert2(string loja, DataGridView dataGrid, string nNumdocGE, string nIDunicoGE)
        {
            string locali = "";
            int indice = 0, indiceX = 0;
            SqlCommand[] comd = new SqlCommand[dataGrid.RowCount - 1];

            for (int i = 0; i < dataGrid.RowCount - 1; i++)
            {
                //MessageBox.Show(dataGrid[20, i].Value.ToString());
                comd[i] = conexao.CreateCommand();
                comd[i].CommandText = "Insert Into xTemp.ASENCOMENDAS02 (IDunico,NUMDOC,CodLoja,              CodArmz,                                                        Referenc,                                                        DescrArtigo,                                DataCria,                                                FOB,                                                                       PCLD,                                                                        PVPD,                                                    CODIGOLOJA_GE,                             CodigoArmzGE,                     CodEntidade,                                       Indice,                                              INDICEX,                                              QTD_STK,                                              QTD_SAIDA,                                          QTD_SUGESTAO,                        Doc) " +
                       "values ('" + nIDunicoGE + "', '" + nNumdocGE + "', '" + loja + "', '" + insertString(dataGrid[8, i].Value.ToString()) + "', '" + insertString(dataGrid[9, i].Value.ToString()).Trim() + "', '" + insertString(dataGrid[10, i].Value.ToString()).Trim() + "', Convert(Varchar(20),GetDate(),120), '" + insertDouble(dataGrid[12, i].Value.ToString()).Replace(',', '.') + "', '" + insertDouble(dataGrid[13, i].Value.ToString()).Replace(',', '.') + "', '" + insertDouble(dataGrid[14, i].Value.ToString()).Replace(',', '.') + "', '" + dataGrid[19, i].Value + "', '" + dataGrid[20, i].Value.ToString() + "', '" + Gerais.PARAM_CODCLI_GERAL + "', '" + insertInt(dataGrid[4, i].Value.ToString()) + "', '" + insertInt(dataGrid[5, i].Value.ToString()) + "', '" + insertInt(dataGrid[16, i].Value.ToString()) + "', '" + insertInt(dataGrid[17, i].Value.ToString()) + "', '" + insertInt(dataGrid[18, i].Value.ToString()) + "', 'RI')";

            }

            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                for (int i = 0; i < dataGrid.RowCount - 1; i++)
                {
                    try
                    {
                        comd[i].Transaction = tran;
                        comd[i].ExecuteNonQuery();
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }

                tran.Commit();
                MessageBox.Show("Gravado com sucesso!");
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void prenxerGrid2(DataGridView dataGrid)
        {
            dataGrid.DataSource = null;
            dataGrid.Rows.Clear();

            sql = @"Select CodLoja,CodArmz,Referenc,DescrArtigo,LOCALI,FOB,PCLD,PVPD,IsNull(QTD_ENCOMENDA,0), QTD_STK,QTD_SAIDA,QTD_SUGESTAO,CODIGOLOJA_GE,CodigoArmzGE, QTDGE, PCLD*QTD_SUGESTAO, 1 " +
                "From xTemp.ASENCOMENDAS01 Where CodLoja = '" + lbELoja.Text + "' Order By CODIGOLOJA_GE, LOCALI, REFERENC";
            
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataAdapter.SelectCommand.CommandTimeout = 9000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dataGrid.DataSource = dataView;
                conexao.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void prenxerGridConsulta(DataGridView dataGrid, string data1, string data2)
        {
            dataGrid.DataSource = null;
            dataGrid.Rows.Clear();

            sql = @"Select Distinct R.IDUnico, R.NumDoc, Cast(R.DataCria As DateTime), U.NOMEUTILIZADOR, STATUS = CASE R.Status When 'A' THEN 'ANULADO' When 'W' THEN 'PENDENTE' When 'C' THEN 'ATENDIDA' ELSE 'EMITIDO' END, LTrim(RTrim(SR.IDUnico)), LTrim(RTrim(SR.NumDoc)), SR.CODDOC, Cast(SR.DataCria As DateTime), LTrim(RTrim(US.NOMEUTILIZADOR)), ESTADO = CASE SR.STATUS When 'A' THEN 'ANULADO' ELSE 'EMITIDO' END " +
                "From AREGDOC R  With (NoLock), ASFICMOV1 F With (NoLock), AREGDOC SR  With (NoLock), ASFICMOV1 FM With (NoLock),ASMESTRE M With (NoLock), ASDOCS D With (NoLock), ASLOJA A With (NoLock), AFENTIDADE C With (NoLock), UTILIZADORES U WITH (NoLock), UTILIZADORES US WITH (NoLock)  " +
                "Where F.REFERENC = M.REFERENC And F.CODARMZ = M.CODARMZ And F.CODLOJA = M.CODLOJA And R.CODDOC=D.CODDOC And R.CODENTIDADE = C.CODENTIDADE And A.CODLOJA=F.CODLOJA And R.CODLOJA=F.CODLOJA  And R.NUMDOC=F.NUMDOC And F.CODDOC=R.CODDOC  And R.CODUTILIZADOR =U.CODUTILIZADOR And R.IDUNICO=F.IDUNICO And SR.CODUTILIZADOR =US.CODUTILIZADOR  " +
                "And R.CODDOC<>'GI' And SR.IDUNICO=FM.IDUNICO And SR.CODDOC In ('SR', 'GI') And SR.IDOrig=R.IDUnico And F.Referenc=FM.Referenc And R.IDunico = F.IDunico AND R.CodEntidade = C.CodEntidade and D.Coddoc = R.Coddoc And M.codarmz = F.codarmz AND M.referenc = F.referenc AND M.codloja = F.codloja And R.CODDOC='RI' AND F.CODUTILIZADOR = U.CODUTILIZADOR And R.Status<> 'A'";
            if (Convert.ToInt32(Permissoes.nPrgPos) < 3)
                sql = sql + "AND R.CodUtilizador = '" + Variavel.codUtilizador + "' ";
            if (txtCDataInicial.Text != "" && txtCDataFinal.Text != "")
                sql = sql + "And LEFT(SR.DataCria, 10) >=  '" + data1 + "' And LEFT(SR.DataCria, 10) <= '" + data2 + "' ";
            if (cmbCLoja.Text != "" || cmbCLoja.PromptText != "")
                sql = sql + " AND R.codloja  = '" + codLojaConsulta + "' ";
            if (txtCRIN.Text != "")
                sql = sql + " AND R.numdoc = '" + txtCRIN.Text + "' ";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataAdapter.SelectCommand.CommandTimeout = 9000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                dataGrid.DataSource = dataView;
                conexao.Close();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void Calculo1(DataGridView dataGrid, int i, int tamanho)
        {
            //MessageBox.Show(dataGrid[4, i].Value.ToString() + " - " + tamanho + "0020");
            dataGrid[5, i].Value = tamanho + "0001";
            if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0280"))
                dataGrid[5, i].Value = tamanho + "0281";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0260"))
                dataGrid[5, i].Value = tamanho + "0261";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0240"))
                dataGrid[5, i].Value = tamanho + "0241";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0220"))
                dataGrid[5, i].Value = tamanho + "0221";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0200"))
                dataGrid[5, i].Value = tamanho + "0201";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0180"))
                dataGrid[5, i].Value = tamanho + "0181";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0160"))
                dataGrid[5, i].Value = tamanho + "0161";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0140"))
                dataGrid[5, i].Value = tamanho + "0141";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0120"))
                dataGrid[5, i].Value = tamanho + "0121";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0100"))
                dataGrid[5, i].Value = tamanho + "0101";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0080"))
                dataGrid[5, i].Value = tamanho + "0081";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0060"))
                dataGrid[5, i].Value = tamanho + "0061";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0040"))
                dataGrid[5, i].Value = tamanho + "0041";
            else if (Convert.ToInt32(dataGrid[4, i].Value.ToString()) > Convert.ToInt32(tamanho + "0020"))
                dataGrid[5, i].Value = tamanho + "0021";
        }
        private void preenxeIndice(DataGridView dataGrid, int i, int tamanho)
        {
            string lg = "";
            if (dataGrid[1, i].Value != null)
                lg = dataGrid[1, i].Value.ToString();
            else
                lg = null;
            if (CODIGOLJ != lg)
            {
                CODIGOLJ = lg;
                tamanho = tamanho + 1;

                if (tamanho == 1)
                    nRowId = 10000;
                else if (tamanho == 2)
                    nRowId = 20000;
                else if (tamanho == 3)
                    nRowId = 30000;
                else if (tamanho == 4)
                    nRowId = 40000;
                else if (tamanho == 5)
                    nRowId = 50000;
                else if (tamanho == 6)
                    nRowId = 60000;
                else if (tamanho == 7)
                    nRowId = 70000;
                else if (tamanho == 8)
                    nRowId = 80000;
                else if (tamanho == 9)
                    nRowId = 90000;
                else if (tamanho == 10)
                    nRowId = 100000;
            }
            if (CODIGOLJ == lg)
            {
                nRowId = nRowId + 1;
                dataGrid[4, i].Value = nRowId;
                dataGrid[5, i].Value = nRowId;
                Calculo1(dataGrid, i, tamanho);
            }
        }
        private void fPopulateY(DataGridView dataGrid, DataGridView dataGrid1, DataGridView dataGrid2)
        {
            double total = 0;
            SqlCommand comand;
            SqlTransaction tran;
            for (int i = 0; i < dataGrid.RowCount - 1; i++)
            {
                if (dataGrid[15, i].Value.ToString() == "RI")
                    dataGrid2.Rows.Add(dataGrid[0, i].Value, dataGrid[1, i].Value, dataGrid[2, i].Value, dataGrid[3, i].Value, dataGrid[4, i].Value, dataGrid[5, i].Value, dataGrid[6, i].Value, dataGrid[7, i].Value, dataGrid[8, i].Value, dataGrid[9, i].Value, dataGrid[10, i].Value, dataGrid[11, i].Value, dataGrid[12, i].Value, dataGrid[13, i].Value, dataGrid[14, i].Value, dataGrid[15, i].Value, dataGrid[16, i].Value, dataGrid[17, i].Value, dataGrid[18, i].Value, dataGrid[19, i].Value, dataGrid[20, i].Value, dataGrid[21, i].Value, dataGrid[22, i].Value, dataGrid[23, i].Value);
                else if (dataGrid[15, i].Value.ToString() == "SE")
                    dataGrid1.Rows.Add(dataGrid[0, i].Value, dataGrid[1, i].Value, dataGrid[2, i].Value, dataGrid[3, i].Value, dataGrid[4, i].Value, dataGrid[5, i].Value, dataGrid[6, i].Value, dataGrid[7, i].Value, dataGrid[8, i].Value, dataGrid[9, i].Value, dataGrid[10, i].Value, dataGrid[12, i].Value, dataGrid[13, i].Value, dataGrid[15, i].Value, dataGrid[16, i].Value, dataGrid[17, i].Value, dataGrid[18, i].Value, dataGrid[19, i].Value, dataGrid[20, i].Value, dataGrid[21, i].Value, dataGrid[23, i].Value);
            }
            //*
            conexao.Open();
            tran = conexao.BeginTransaction();
            try
            {
                comand = new SqlCommand("Delete From xTemp.ASENCOMENDAS01 Where CodLoja = '" + lbELoja.Text + "'");
                comand.Connection = conexao;
                comand.Transaction = tran;
                comand.ExecuteNonQuery();

                MessageBox.Show("Apagado com Sucesso");
                tran.Commit();
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            
            insert1(dataGrid2);
            
            prenxerGrid2(dataGrid2);
            lbItens.Text = "" + (dataGrid2.RowCount - 1);
            
            CODIGOLJ = "";
            nTamanho = 0;

            for (int i = 0; i < dataGrid2.RowCount - 1; i++)
            {
                preenxeIndice(dataGrid2, i, nTamanho);
                total = total + (Convert.ToInt32(dataGrid2[18, i].Value.ToString()) * Convert.ToDouble(dataGrid2[13, i].Value.ToString()));
            }
            
            if (total < 1000)
                lbTotalSelecionado.Text = lbTotalEncomendar.Text = String.Format("{0:0.00}", total);
            else if(total < 1000000)
                lbTotalSelecionado.Text = lbTotalEncomendar.Text = String.Format("{0:0,000.00}", total);
            else
                lbTotalSelecionado.Text = lbTotalEncomendar.Text = String.Format("{0:0,000,000.00}", total);
            conexao.Open();
            tran = conexao.BeginTransaction();
            try{
                comand = new SqlCommand("Delete From xTemp.ASENCOMENDAS03 Where CodLoja = '" + lbELoja.Text + "'");
                comand.Connection = conexao;
                comand.Transaction = tran;
                comand.ExecuteNonQuery();
                
                for (int i = 0; i < dataGrid1.RowCount - 1; i++)
                {
                    SqlCommand cmd = new SqlCommand("Insert Into xTemp.ASENCOMENDAS03 (CodLoja,CodArmz,      Referenc,                     DescrArtigo,                                DataCria,                                      CodUtilizador,           Doc,                      QTD_ENCOMENDA,                                        QTD_STK,                                                QTD_SAIDA,                                            QTD_SUGESTAO,                                CODIGOLOJA_GE,                        CodEntidade,                                        FOB,                                                                                          PCLD,                                                           PVPD )" +
                             "Values ('" + lbELoja.Text + "', '" + dataGrid1[2, i].Value + "', '" + dataGrid1[3, i].Value + "', '" + dataGrid1[4, i].Value + "', Left(Convert(varchar(20),GetDate(),120),20), LTrim(RTrim('" + Variavel.codUtilizador + "')),  'SE', '" + insertInt(dataGrid1[11, i].Value.ToString()) + "', '" + insertInt(dataGrid1[5, i].Value.ToString()) + "', '" + insertInt(dataGrid1[7, i].Value.ToString()) + "','" + insertInt(dataGrid1[6, i].Value.ToString()) + "', '" + dataGrid1[1, i].Value + "', '" + Gerais.PARAM_CODCLI_GERAL + "', '" + insertDouble(dataGrid1[9, i].Value.ToString()).Replace(',', '.') + "', '" + insertDouble(dataGrid1[12, i].Value.ToString()).Replace(',', '.') + "', '" + insertDouble(dataGrid1[10, i].Value.ToString()).Replace(',', '.') + "')");

                    try
                    {
                        cmd.Connection = conexao;
                        cmd.Transaction = tran;
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex) {}
                }
                tran.Commit();
                MessageBox.Show("Gravado com sucesso!");
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); } //*/
        }
        private string selecionarCodLoja()
        {
            string codigo = "";
            try
            {
                SqlCommand cmd2 = new SqlCommand("SELECT CodLoja FROM ASLOJA WITH (NOLOCK) WHERE RTRIM(NomeLoja) = '" + cmbELoja.Text + "' ", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codigo = reader[0].ToString();
                    }
                }
            }
            catch (Exception) { codigo = ""; }
            finally { conexao.Close(); }
            return codigo;
        }
        private string idUnico(string loja)
        {
            string id = "";
            SqlCommand cmd1 = new SqlCommand("SELECT RTRIM(ISNULL(CODLOJA,'" + loja + "')) +   CAST (ISNULL( MAX( CAST( RIGHT (RTRIM(IDUNICO), LEN(RTRIM(IDUNICO)) - " +
                "( LEN(ISNULL(CODLOJA,'" + loja + "')) ) )AS INT ) ) , 1) + 1 AS VARCHAR(23)) AS IDUNICO FROM AREGDOC  WITH (XLOCK)  " +
                "WHERE CODLOJA='" + loja + "' AND LEFT(IDUNICO, LEN(CODLOJA) ) = '" + loja + "' GROUP BY CODLOJA", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        id = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            if (id == "" || id == null)
                id = loja + "1";

            return id;
        }
        private int numeroDocumento(string loja, string documento)
        {
            int numero = 0;
            SqlCommand cmd1 = new SqlCommand("SELECT ISNULL(MAX(NUMDOC),0) + 1 FROM AregDoc  WITH (XLOCK) WHERE  CodLoja='" + loja + "' AND CodDoc='" + documento + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        numero = Convert.ToInt32(reader[0].ToString());
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            if (numero == 0 || numero == null)
                numero = 1;

            return numero;
        }
        private void fDoXport(string loja, DataGridView dataGrid)
        {
            string nIDunicoGE = idUnico(loja);
            string nNumdocGE = "" + numeroDocumento(loja, "GE");

            insert2(loja, dataGrid, nNumdocGE, nIDunicoGE);
        }
        private void fTransacaoRI(string loja, bool externa, string codCliente)
        {
            string sTName, nIndicadorX = "";
		    string zTMP02 = "";
		    string strIDunico;
            string nNumDoc_GE = "0";
            SqlCommand cmd, comd;
            
            if (externa)
            {
                pstrCodigoX = codCliente;
                SqlCommand cmd1 = new SqlCommand("Select IsNull(Max(IdRequisicao),0) + 1 From ASFICMOV1 With(XLock) Where CodEntidade='" + pstrCodigoX + "' And CodDoc='RI'", conexao);
                try
                {
                    conexao.Open();
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        while (reader.Read()) { nNumDoc_GE = reader[0].ToString(); }
                    }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
                finally { conexao.Close(); }
            }
            else
            {
                pstrCodigoX = Gerais.PARAM_CODCLI_GERAL;
                SqlCommand cmd1 = new SqlCommand("Select IsNull(Max(IdRequisicao),0) + 1 From ASFICMOV1 With(XLock) Where LOJADESTINO='" + loja + "' And CodDoc='RI'", conexao);
                try
                {
                    conexao.Open();
                    using (SqlDataReader reader = cmd1.ExecuteReader())
                    {
                        while (reader.Read()) { nNumDoc_GE = reader[0].ToString(); }
                    }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
                finally { conexao.Close(); }
            }

            if (nNumDoc_GE == "0" || nNumDoc_GE == null || nNumDoc_GE == "")
                nNumDoc_GE = "1";

            cmd = new SqlCommand("Select Distinct LTrim(RTrim(CODIGOLOJA_GE)), IndiceX From xTemp.ASENCOMENDAS02 Where CodLoja='" + loja + "' Order By LTrim(RTrim(CODIGOLOJA_GE)), IndiceX ", conexao);
            //cmd = new SqlCommand("Select Distinct LTrim(RTrim(CODIGOLOJA_GE)), IndiceX From xTemp.ASENCOMENDAS02 Where CodLoja='" + loja + "' Order By IndiceX ", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read()) { zTMP02 = reader[0].ToString(); nIndicadorX = reader[1].ToString(); }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            
            sTName = "T_INS_ENTRYGR_LOJA";
            strIDunico = idUnico(zTMP02);

            nNumdoc = "" + numeroDocumento(zTMP02, "RI");

            cmd = new SqlCommand("Insert Into AREGDOC ( IDUNICO,                 NUMDOC,           NUMFACT,     CODDOC,     CodEntidade,               CODUTILIZADOR,                            DATACRIA,         PRECO, PRECOD,      CODLOJA,           IDORIG,                    DATA_LANCAMENTO,        STATUS,    IdRequisicao)" +
                             "Values ((LTrim(RTrim('" + strIDunico + "'))), '" + nNumdoc + "', '" + nNumdoc + "', 'RI', '" + pstrCodigoX + "', '" + Variavel.codUtilizador + "', Convert(varchar(20),GetDate(), 120), 0,      0,   '" + zTMP02 + "', '" + strIDunico + "', Convert(varchar(10),GetDate(),120), 'E', '" + nNumDoc_GE + "')");
            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                cmd.Connection = conexao;
                cmd.Transaction = tran;
                cmd.ExecuteNonQuery();
                //*
                if (externa)
                {
                    comd = new SqlCommand("Insert Into ASFICMOV1 (IDUNICO, NUMDOC, CODLOJA, CODARMZ, REFERENC,            PVP,             PVPD,            PCL,                  PCLD, FOB,                DATACRIA,               QTD,            CODENTIDADE,                     CODDOC,            TIPMOV,        CODUTILIZADOR,                     MOEDA,                       IdRequisicao,                    DATA_LANCAMENTO ) " +
                             "Select '" + strIDunico + "', '" + nNumdoc + "', CODLOJA, CodArmz, Referenc, PVPD* '" + Variavel.cambio + "', PVPD, PCLD* '" + Variavel.cambio + "', PCLD, FOB, Convert(varchar(20),GetDate(), 120), QTD_SUGESTAO, '" + pstrCodigoX + "', '" + Gerais.PARAM_CODDOC_RI + "', '=', '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_MOEDA_PDR + "', '" + nNumDoc_GE + "', Convert(varchar(10),GetDate(), 120) From xTemp.ASENCOMENDAS02 Where LTrim(RTrim(CodLoja))='" + loja + "' And LTrim(RTrim(CODIGOLOJA_GE))='" + zTMP02 + "' And IndiceX= '" + nIndicadorX + "' And QTD_SUGESTAO > 0 Order By Indice ");
                    
                        comd.Connection = conexao;
                        comd.Transaction = tran;
                        comd.ExecuteNonQuery();
                }
                else
                {
                    comd = new SqlCommand("Insert Into ASFICMOV1 (IDUNICO, NUMDOC,     CODLOJA, CODARMZ, REFERENC, PVP,                             PVPD, PCL,                             PCLD, FOB,                  DATACRIA,                QTD,                 CODENTIDADE,                             CODDOC,          TIPMOV,           CODUTILIZADOR,         LOJADESTINO, ARMZDESTINO,                MOEDA,                   IdRequisicao,         DATA_LANCAMENTO) " +
                             "Select distinct '" + strIDunico + "', '" + nNumdoc + "', CODLOJA, CodArmz, Referenc, PVPD* '" + Variavel.cambio + "', PVPD, PCLD* '" + Variavel.cambio + "', PCLD, FOB, Convert(varchar(20),GetDate(), 120), QTD_SUGESTAO, '" + Gerais.PARAM_CODCLI_GERAL + "', '" + Gerais.PARAM_CODDOC_RI + "', '=', '" + Variavel.codUtilizador + "', CODIGOLOJA_GE, CODIGOArmzGE, '" + Gerais.PARAM_MOEDA_PDR + "', '" + nNumDoc_GE + "', Convert(varchar(10),GetDate(), 120) From xTemp.ASENCOMENDAS02 Where LTrim(RTrim(CodLoja))='" + loja + "' And LTrim(RTrim(CODIGOLOJA_GE))='" + zTMP02 + "' And IndiceX='" + nIndicadorX + "' And QTD_SUGESTAO>0");
                    
                        comd.Connection = conexao;
                        comd.Transaction = tran;
                        comd.ExecuteNonQuery();
                }
                SqlCommand comando = new SqlCommand("UPDATE ASMESTRE SET ENCOMENDA = 'S', OBS2 = Convert(varchar(20),GetDate(),120), QTD_Encomenda = IsNull(M.QTD_Encomenda,0) + QTD_Sugestao From ASMESTRE M, xTemp.ASENCOMENDAS02 GE Where M.CodLoja = '" + loja + "' And M.CODLOJA = GE.CODLOJA And M.CODARMZ = GE.CODARMZ And M.REFERENC = GE.REFERENC");
                comando.Connection = conexao;
                comando.Transaction = tran;
                comando.ExecuteNonQuery(); //*/

                tran.Commit();
                MessageBox.Show("Gravado com sucesso!");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void fTransacaoSE(string loja, bool externa, string codCliente)
        {
		    string sIdUnico;
		    int nDocumento;

            if (externa)
                pstrCodigoX = codCliente;
            else
                pstrCodigoX = Gerais.PARAM_CODCLI_GERAL;
            sIdUnico = idUnico(loja);
            nDocumento = numeroDocumento(loja, "SE");

            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                SqlCommand cmd = new SqlCommand("Insert Into xTeste.XXDOCUMENTO (IDUNICO, NUMDOC,   NUMFACT,                       CODDOC,                 CodEntidade,               CODUTILIZADOR,                           DATACRIA,                  PRECO, PRECOD,              CODLOJA,                       IDORIG,                 DATA_LANCAMENTO,          STATUS )" +
                             "Values (LTrim(RTrim('" + sIdUnico + "')), '" + nDocumento + "', '" + nDocumento + "', '" + Gerais.PARAM_CODDOC_SE + "', '" + pstrCodigoX + "', '" + Variavel.codUtilizador + "', Left(Convert(varchar(20),GetDate(),120),20), 0,     0, LTrim(RTrim('" + loja + "')), LTrim(RTrim('" + sIdUnico + "')),Convert(varchar(10),GetDate(),120), 'E' )");

                cmd.Connection = conexao;
                cmd.Transaction = tran;
                cmd.ExecuteNonQuery();

                SqlCommand cmd1 = new SqlCommand("Insert Into ASENCOMENDAS(IDUNICO, NUMDOC, CODLOJA, CODARMZ, REFERENC, PVP, PVPD, PCL, PCLD, FOB, DATACRIA, QTD, CODENTIDADE, CODDOC, TIPMOV, CODUTILIZADOR, LOJADESTINO, ARMZDESTINO, MOEDA, ESTADO, DATA_LANCAMENTO) " +
                             "Select '" + sIdUnico + "', '" + nDocumento + "', CODIGOLOJA_GE, CODARMZ, Referenc, PVPD*:nCambio, PVPD, PCLD* '" + Variavel.cambio + "', PCLD, FOB, Convert(varchar(20),GetDate(), 120), QTD_SUGESTAO, '" + pstrCodigoX + "', '" + Gerais.PARAM_CODDOC_SE + "', '=', '" + Variavel.codUtilizador + "', CODLOJA, CodArmz, '" + Gerais.PARAM_MOEDA_PDR + "', 'E', Convert(varchar(10),GetDate(), 120) From xTemp.ASENCOMENDAS03 Where Doc= '" + Gerais.PARAM_CODDOC_SE + "')");

                cmd1.Connection = conexao;
                cmd1.Transaction = tran;
                cmd1.ExecuteNonQuery();

                tran.Commit();
                MessageBox.Show("Gravado com sucesso!");
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            RequisicaoEncom form = new RequisicaoEncom(nDocumento, externa, codCliente, sIdUnico, txtEnNDias.Text, txtEnPercentualSaida.Text);
            form.ShowDialog();
        }
        private void FormRequisicaoInterna_Load(object sender, EventArgs e)
        {
            btC.SelectedTab = metroTabPage1;
            btEnCriar.Enabled = false;
            txtExDataInicial.Text = txtExDataFinal.Text = txtCDataInicial.Text = txtCDataFinal.Text = DateTime.Now.ToShortDateString();
            cmbLoja.PromptText = cmbCLoja.PromptText = Variavel.nomeLoja;
            cmbLoja2.PromptText = cmbLoja3.PromptText = Variavel.nomeLoja;
            codLoja = Variavel.codLoja;
            DesativarAutoGenerateColumnsGrid(dgEntradaGuia, dgL1, dgDetail, dgArt, dgLoja, dgArma, dgFam, dgSubFam, dgMarca, dgModelo, dgFicha, dgEncomendas, metroGrid3);
        }
        private void cmbLoja_MouseClick(object sender, MouseEventArgs e)
        {
            cmbLoja.Items.Clear();
            CmbALoja(cmbLoja);
        }

        private void btROk_Click(object sender, EventArgs e)
        {
            if (cmbLoja.Text == "")
                codLojaRI = Variavel.codLoja;
            FormRequisicao form = new FormRequisicao();
            form.ShowDialog();
        }

        private void cmbLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            codLojaRI = selecionarCodLoja();
        }

        private void cmbLoja2_MouseClick(object sender, MouseEventArgs e)
        {
            cmbLoja2.Items.Clear();
            CmbALoja(cmbLoja2);
        }

        private void cmbLoja3_MouseClick(object sender, MouseEventArgs e)
        {
            cmbLoja3.Items.Clear();
            CmbALoja(cmbLoja3);
        }

        private void cmbCLoja_MouseClick(object sender, MouseEventArgs e)
        {
            cmbCLoja.Items.Clear();
            CmbALoja(cmbCLoja);
        }

        private void cmbLoja2_SelectedIndexChanged(object sender, EventArgs e)
        {
            codLojaRI = codLojaRI = selecionarCodLoja();
        }

        private void btAOk_Click(object sender, EventArgs e)
        {
            if (cmbLoja2.Text == "")
                codLojaRI = Variavel.codLoja;
            FormRequisicaoAlerta form = new FormRequisicaoAlerta();
            if (form.ShowDialog() == DialogResult.OK)
            {
                FormRequisicaoAtendimento form1 = new FormRequisicaoAtendimento();
                form1.ShowDialog();
            }
        }

        private void cmbLoja3_SelectedIndexChanged(object sender, EventArgs e)
        {
            codLojaRI = selecionarCodLoja();
        }

        private void btReGravar_Click(object sender, EventArgs e)
        {
            if (cmbLoja3.Text == "")
                codLojaRI = Variavel.codLoja;
            FormRequisicaoRecepcao form = new FormRequisicaoRecepcao();
            form.ShowDialog();
        }

        private void cmbExEntidadeFilial_MouseClick(object sender, MouseEventArgs e)
        {
            cmbExEntidadeFilial.Items.Clear();
            prenxeComboEntidadeFilial(cmbExEntidadeFilial);
        }

        private void cmbExIDGI_MouseClick(object sender, MouseEventArgs e)
        {
            cmbExIDGI.Items.Clear();
            prenxeComboDIGI(cmbExIDGI);
        }

        private void cmbExEntidadeFilial_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand comd = new SqlCommand("Select RTRIM(CODENTIDADE) From AFENTIDADE With (NoLock) Where RTrim(NomeEntidade) = '" + cmbExEntidadeFilial.Text + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { codCliente = reader[0].ToString(); } }
            conexao.Close();
        }

        private void cmbExIDGI_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand comd;
            comd = new SqlCommand("Select RTRIM(IdUnico), RTRIM(CodLoja) From AregDoc Where IdUnico = '" + cmbExIDGI.Text + "' And CodDoc = '" + Gerais.PARAM_CODDOC_GI + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { nIDUnicoGI = reader[0].ToString(); strCdLoja = reader[1].ToString(); } }
            
            comd = new SqlCommand("Select R.NUMDOC, LTrim(RTrim(L.NOMELOJA)) From AREGDOC R WITH (NOLOCK), AFENTIDADE E WITH (NOLOCK), ASLOJA L WITH (NOLOCK) " +
                    "Where R.IDUNICO = '" + nIDUnicoGI + "' And R.STATUS<>'A' And RTRIM(R.CODENTIDADE) = RTRIM(E.CODENTIDADE) And RTRIM(R.CODLOJA) = RTRIM(L.CODLOJA) And RTRIM(R.CODDOC) = '" + Gerais.PARAM_CODDOC_GI + "' And LEFT(R.DATACRIA,10) >= '" + dataInicio + "' And LEFT(R.DATACRIA,10) <= '" + dataFinal + "'", conexao);
            using (SqlDataReader reader = comd.ExecuteReader())
            { while (reader.Read()) { lbNumero.Text = reader[0].ToString(); lbfuncao.Text = reader[1].ToString(); } }
            conexao.Close();

            Cursor.Current = Cursors.WaitCursor;
            prenxerGrid();
        }
        private void btEExportacao_Click(object sender, EventArgs e)
        {
            exportarParaExcel(dgEntradaGuia, dgL1, dgDetail, dgArt, dgLoja, 
                dgArma, dgFam, dgSubFam, dgMarca, dgModelo, dgFicha);
        }

        private void btEnCalcular_Click(object sender, EventArgs e)
        {
            string lg = "";
            if (lbELoja.Text == "" || lbELoja.Text == null)
                MessageBox.Show("O campo [ Loja ]  deve ser preenxido");
            else
            {
                //desabilita os botões enquanto a tarefa é executada. 
                //progressBar1.Visible = true;
                btEnCalcular.Enabled = false;
                btEnCriar.Enabled = false;
                Cursor.Current = Cursors.WaitCursor;
                calcular();
                btEnCalcular.Enabled = true;
                btEnCriar.Enabled = true;

                //backgroundWorker1.RunWorkerAsync();
                //define a progressBar para Marquee 
                //progressBar1.Style = ProgressBarStyle.Marquee;
                //progressBar1.MarqueeAnimationSpeed = 5;
                //informa que a tarefa esta sendo executada. 
                //label1.Text = "Processando...";
            }
        }

        private void btEnCriar_Click(object sender, EventArgs e)
        {
            string codloja = "";
            comd = new SqlCommand("select CodigoEntidade from ASLOJA WHERE CodLoja = '" + lbELoja.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codloja = reader[0].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            if (codloja != null && codloja != "")
                externa = true;
            else
                externa = false;

            Cursor.Current = Cursors.WaitCursor;
            fDoXport(lbELoja.Text, dgEncomendas2);
            Cursor.Current = Cursors.WaitCursor;
            fTransacaoRI(lbELoja.Text, externa, codloja);
            Cursor.Current = Cursors.WaitCursor;
            fTransacaoSE(lbELoja.Text, externa, codloja);
            btEnCriar.Enabled = false;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //executa a tarefa a primeira vez 
            calcular();
            //Verifica se houve uma requisição para cancelar a operação. 
            if (backgroundWorker1.CancellationPending)
            {
                //se sim, define a propriedade Cancel para true 
                //para que o evento WorkerCompleted saiba que a tarefa foi cancelada. 
                e.Cancel = true;
                return;
            } //executa a tarefa pela segunda vez 
            calcular();
            if (backgroundWorker1.CancellationPending)
            {
                //se sim, define a propriedade Cancel para true 
                //para que o evento WorkerCompleted saiba que a tarefa foi cancelada. 
                e.Cancel = true;
                return;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Caso cancelado... 
            if (e.Cancelled)
            {
                // reconfigura a progressbar para o padrao. 
                progressBar1.MarqueeAnimationSpeed = 0;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 0;
                //caso a operação seja cancelada, informa ao usuario. 
                label2.Text = "Operação Cancelada pelo Usuário!";
                //habilita o botao cancelar 
                //btnCancelar.Enabled = true;
                //limpa a label 
                //label1.Text = string.Empty;
            }
            else if (e.Error != null)
            {
                //informa ao usuario do acontecimento de algum erro. 
                label2.Text = "Aconteceu um erro durante a execução do processo!";
                // reconfigura a progressbar para o padrao. 
                progressBar1.MarqueeAnimationSpeed = 0;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 0;
            }
            else
            {
                //informa que a tarefa foi concluida com sucesso. 
                label2.Text = "Tarefa Concluida com sucesso!";
                //Carrega todo progressbar. 
                progressBar1.MarqueeAnimationSpeed = 0;
                progressBar1.Style = ProgressBarStyle.Blocks;
                progressBar1.Value = 100;
                //label1.Text = progressBar1.Value.ToString() + "%";
            }
            //habilita os botões. 
            btEnCalcular.Enabled = true;
            btEnCriar.Enabled = true;
        }

        private void cmbELoja_DropDown(object sender, EventArgs e)
        {
            cmbELoja.Items.Clear();
            //Cursor.Current = Cursors.WaitCursor;
            CmbALoja(cmbELoja);
        }

        private void cmbELoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbELoja.Text = selecionarCodLoja();
        }

        private void txtExDataInicial_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtExDataInicial.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtExDataInicial.Text = ""; }
        }

        private void txtExDataFinal_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtExDataFinal.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtExDataFinal.Text = ""; }
        }

        private void txtCDataInicial_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtCDataInicial.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtCDataInicial.Text = ""; }
        }

        private void txtCDataFinal_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtCDataFinal.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtCDataFinal.Text = ""; }
        }

        private void dgEncomendas2_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            /*if (e.RowIndex >= 0)
            {
                MessageBox.Show(dgEncomendas2.CurrentCell.ColumnIndex.ToString());
            }*/
        }

        private void btAtualizar_Click(object sender, EventArgs e)
        {
            double total = 0;
            
            for (int i = 0; i < dgEncomendas2.RowCount - 1; i++)
            {
                preenxeIndice(dgEncomendas2, i, nTamanho);
                total = total + (Convert.ToInt32(dgEncomendas2[18, i].Value.ToString()) * Convert.ToDouble(dgEncomendas2[13, i].Value.ToString()));
            }

            if (total < 1000)
                lbTotalSelecionado.Text = String.Format("{0:0.00}", total);
            else if (total < 1000000)
                lbTotalSelecionado.Text = String.Format("{0:0,000.00}", total);
            else
                lbTotalSelecionado.Text = String.Format("{0:0,000,000.00}", total); //*/
        }

        private void dgEncomendas_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            /*if (e.RowIndex >= 0)
            {
                MessageBox.Show(dgEncomendas.CurrentCell.ColumnIndex.ToString());
            }*/
        }

        private void dgEncomendas1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            /*if (e.RowIndex >= 0)
            {
                MessageBox.Show(dgEncomendas1.CurrentCell.ColumnIndex.ToString());
            }*/
        }

        private void btCOk_Click(object sender, EventArgs e)
        {
            string dataInicial = "", dataFinal = "";
            data = Convert.ToDateTime(txtCDataInicial.Text);
            if (data.Month < 10) dataInicial = data.Year + "-0" + data.Month;
            else dataInicial = data.Year + "-" + data.Month;
            if (data.Day < 10) dataInicial = dataInicial + "-0" + data.Day;
            else dataInicial = dataInicial+ "-" + data.Day;
            data = Convert.ToDateTime(txtCDataFinal.Text);
            if (data.Month < 10) dataFinal = data.Year + "-0" + data.Month;
            else dataFinal = data.Year + "-" + data.Month;
            if (data.Day < 10) dataFinal = dataFinal + "-0" + data.Day;
            else dataFinal = dataFinal + "-" + data.Day;
            prenxerGridConsulta(metroGrid3, dataInicial, dataFinal);
        }

        private void cmbCLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            comd = new SqlCommand("select CodLoja from ASLOJA WHERE NomeLoja = '" + cmbCLoja.Text + "'", conexao);
            try
            {
                conexao.Open();
                using (SqlDataReader reader = comd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codLojaConsulta = reader["CodLoja"].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
    }
}
