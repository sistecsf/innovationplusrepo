﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASENTREGA
    {
        public string IDUnico { get; set; }
        public string CodUtilizador { get; set; }
        public string DataCria { get; set; }
        public string STATUS { get; set; }
        public string Data_Atendimento { get; set; }
        public string ATENDIDO_POR { get; set; }
        public string Entrega_Referencia { get; set; }
        public string Entrega_Empresa { get; set; }
        public string Entrega_Morada { get; set; }
        public string Entrega_Contacto { get; set; }
        public string Entrega_Telefone { get; set; }
        public Nullable<System.DateTime> Entrega_Data { get; set; }
        public Nullable<System.TimeSpan> Entrega_Hora { get; set; }
        public string Matricula { get; set; }

    }
}
