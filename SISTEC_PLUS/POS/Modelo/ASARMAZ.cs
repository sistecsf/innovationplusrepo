﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASARMAZ
    {
        public string CodLoja { get; set; }
        public string CodArmz { get; set; }
        public string NomeArz { get; set; }
        public string Cabecalho { get; set; }
        public string RODAPE { get; set; }
        public string Morada { get; set; }
        public string CodUtilizador { get; set; }
        public string ContVend { get; set; }
        public string ContStock { get; set; }
        public string ContCusto { get; set; }
        public string DATACRIA { get; set; }
        public string Data_Alteracao { get; set; }
        public string Alterado_Por { get; set; }
        public string CodAO { get; set; }
        public string ARMA_VENDA { get; set; }
        public string ARMA_REMOTO { get; set; }
        public string Ativo { get; set; }
        public string EMITIR_RI { get; set; }
        


    }
}
