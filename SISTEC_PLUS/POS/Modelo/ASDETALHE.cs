﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASDETALHE
    {
        public string Item { get; set; }
        public string Descricao { get; set; }
        public Nullable<decimal> PrecoUnit { get; set; }
        public Nullable<int> Qtd { get; set; }
        public Nullable<decimal> PrecoTotal { get; set; }
        public string DataCria { get; set; }
        public string CodUtilizador { get; set; }
        public string CodDoc { get; set; }
        public string IDUnico { get; set; }
        public string PEM { get; set; }
        public string GARANTIA { get; set; }
        public string ASSUNTO { get; set; }
        public string INSTALACAO { get; set; }
        public string PAGA_Nac { get; set; }
        public string PAGA_Pdr { get; set; }

    }
}
