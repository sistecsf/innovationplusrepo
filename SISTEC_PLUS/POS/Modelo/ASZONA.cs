﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASZONA
    {
        public ASZONA()
        {
            this.ASVENDEDORs = new List<ASVENDEDOR>();
        }

        public string CodZona { get; set; }
        public string DataCria { get; set; }
        public string DescrZona { get; set; }
        public string CodUtilizador { get; set; }
        public string CodPais { get; set; }
       // public virtual AFPais AFPais { get; set; }
        public virtual ICollection<ASVENDEDOR> ASVENDEDORs { get; set; }

    }
}
