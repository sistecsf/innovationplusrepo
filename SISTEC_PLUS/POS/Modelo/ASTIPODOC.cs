﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASTIPODOC
    {
        public ASTIPODOC()
        {
            this.ASDOCS = new List<ASDOC>();
        }

        public string Codigo { get; set; }
        public string Descr { get; set; }
        public string CodUtilizador { get; set; }
        public string DataCria { get; set; }
        public virtual ICollection<ASDOC> ASDOCS { get; set; }
      //  public virtual UTILIZADORE UTILIZADORE { get; set; }


    }
}
