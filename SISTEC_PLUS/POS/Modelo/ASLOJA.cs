﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASLOJA
    {
        public string CodLoja { get; set; }
        public string DataCria { get; set; }
        public string NomeLoja { get; set; }
        public string DataMov { get; set; }
        public string Flag { get; set; }
        public string CodUtilizador { get; set; }
        public string Morada { get; set; }
        public string Caixa_Post { get; set; }
        public string Fone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string LOGO { get; set; }
        public string LojaOnLine { get; set; }
        public string CodConta { get; set; }
        public string CodContaPDR { get; set; }
        public string CodContaNAC { get; set; }
        public string CodContaOUTROS { get; set; }
        public string Data_Alteracao { get; set; }
        public string Alterado_Por { get; set; }
        public string Display_Conta { get; set; }
        public string AGTELEC { get; set; }
        public string EMITIR_RC { get; set; }
        public string Obriga_FechoDia { get; set; }
        public string CodEmpresa { get; set; }
        public string Visita_Definida { get; set; }
        public string VENDAREMOTA { get; set; }
        public string PAGTO_REMOTO { get; set; }
        public string CodigoEntidade { get; set; }
        public Nullable<int> NRCOPIAS_LOJA { get; set; }
        public Nullable<decimal> ValorVendas { get; set; }
        public string xxCodigoLJ { get; set; }
     
        public virtual ICollection<ASARMAZ> ASARMAZs { get; set; }
        public virtual ICollection<ASARMAZF> ASARMAZFs { get; set; }

    }
}
