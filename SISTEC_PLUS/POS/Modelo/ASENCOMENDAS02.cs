﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASENCOMENDAS02
    {
        public string IDunico { get; set; }
        public decimal NUMDOC { get; set; }
        public string CodLoja { get; set; }
        public string CodArmz { get; set; }
        public string Referenc { get; set; }
        public string DescrArtigo { get; set; }
        public Nullable<int> QTD_STK { get; set; }
        public Nullable<int> QTD_SAIDA { get; set; }
        public Nullable<int> QTD_SUGESTAO { get; set; }
        public Nullable<int> QTD_ENCOMENDA { get; set; }
        public Nullable<decimal> FOB { get; set; }
        public Nullable<decimal> PCLD { get; set; }
        public Nullable<decimal> PVPD { get; set; }
        public string CodAO { get; set; }
        public string CodEntidade { get; set; }
        public string CodUtilizador { get; set; }
        public string DataCria { get; set; }
        public string CODIGOLOJA_GE { get; set; }
        public Nullable<int> QTD_ATENDIDA { get; set; }
        public Nullable<int> Indice { get; set; }
        public Nullable<int> IndiceX { get; set; }
        public Nullable<decimal> NUMDOC_GE01 { get; set; }
        public Nullable<decimal> NUMDOC_GE02 { get; set; }
        public string CODIGOArmzGE { get; set; }
        public string DOC { get; set; }


    }
}
