﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASIMAGEM
    {
        public string Referenc { get; set; }
        public string CodLoja { get; set; }
        public string CodArmz { get; set; }
        public string DescrArtigo { get; set; }
        public string IMAGEM_WEB { get; set; }
        public byte[] IMAGEM { get; set; }
        public string CodUtilizador { get; set; }
        public string DataCria { get; set; }

    }
}
