﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASFICMOV
    {
        public string IDunico { get; set; }
        public decimal NUMDOC { get; set; }
        public string CodLoja { get; set; }
        public string CodArmz { get; set; }
        public string Referenc { get; set; }
        public string GUIA { get; set; }
        public string AltStok { get; set; }
        public string DataCria { get; set; }
        public Nullable<decimal> FOB { get; set; }
        public decimal PCL { get; set; }
        public decimal PVP { get; set; }
        public Nullable<decimal> Descont { get; set; }
        public Nullable<int> QTD { get; set; }
        public string CodEntidade { get; set; }
        public string TIPMOV { get; set; }
        public Nullable<decimal> POSTO { get; set; }
        public string Moeda { get; set; }
        public string ANULADOR { get; set; }
        public string CodDoc { get; set; }
        public string Anulado { get; set; }
        public string CodUtilizador { get; set; }
        public string CodVend { get; set; }
        public string LojaDestino { get; set; }
        public Nullable<decimal> PCLD { get; set; }
        public Nullable<decimal> NumDocFRN { get; set; }
        public string CodFornec { get; set; }
        public Nullable<decimal> PVPD { get; set; }
        public string ARMZDESTINO { get; set; }
        public string Obs { get; set; }
        public string CodAO { get; set; }
        public string Data_Lancamento { get; set; }
        public string DATA_INV { get; set; }
        public Nullable<int> QTD_INV { get; set; }
        public Nullable<int> QTD_SISTEMA { get; set; }
        public Nullable<int> X1 { get; set; }
        public Nullable<int> QTD_ATENDIDA { get; set; }
        public string ESTADO { get; set; }
        public Nullable<decimal> ImpostoConsumo { get; set; }
        public Nullable<int> X2 { get; set; }
    }
}
