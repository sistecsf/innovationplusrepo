﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class AABANCO
    {
        public int Codigo { get; set; }
        public string SiglaBanco { get; set; }
        public string NumeroConta { get; set; }
        public string NomeBanco { get; set; }
        public string NIB { get; set; }
        public string IBAN { get; set; }
        public string Branch { get; set; }
        public string SWIFT { get; set; }
        public string CodUtilizador { get; set; }
        public string DataCria { get; set; }

    }
}
