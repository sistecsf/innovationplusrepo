﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASANEXO
    {
        public string IDUnico { get; set; }
        public byte[] Anexo { get; set; }
        public decimal ROWID { get; set; }
        public string CodUtilizador { get; set; }
        public string DataCria { get; set; }
        public string Alterado_Por { get; set; }
        public string Data_Alteracao { get; set; }


    }
}
