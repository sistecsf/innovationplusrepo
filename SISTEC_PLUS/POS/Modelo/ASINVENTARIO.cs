﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASINVENTARIO
    {

        public string CodLoja { get; set; }
        public string CodArmz { get; set; }
        public string Referenc { get; set; }
        public string LOCALI { get; set; }
        public Nullable<decimal> FOB { get; set; }
        public Nullable<decimal> PCA { get; set; }
        public Nullable<decimal> PVP { get; set; }
        public Nullable<decimal> PMC { get; set; }
        public string CONTASTOCKS { get; set; }
        public string DataCria { get; set; }
        public Nullable<int> QTD_SIST { get; set; }
        public Nullable<int> QTD_FIS { get; set; }
        public string CodUtilizador { get; set; }
        public string Data_AbreInv { get; set; }
        public string Data_FechaInv { get; set; }
        public Nullable<int> QuantidadeSistemaX { get; set; }
        public string Contado_Por { get; set; }
        public Nullable<int> QuantidadeFisicaX { get; set; }
        public Nullable<int> QuantidadeEM { get; set; }
        public string Zerado { get; set; }
        public string Acerto { get; set; }
        public Nullable<int> QuantidadeEntrada { get; set; }
        public Nullable<int> QuantidadeSaida { get; set; }
        
    }
}
