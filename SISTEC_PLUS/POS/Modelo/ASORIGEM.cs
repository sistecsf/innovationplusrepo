﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASORIGEM
    {
        public string CodOrigem { get; set; }
        public string Descricao { get; set; }
        public string DataCria { get; set; }
        public string CodUtilizador { get; set; }


    }
}
