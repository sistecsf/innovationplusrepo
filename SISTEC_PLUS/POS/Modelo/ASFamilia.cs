﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASFamilia
    {
        public string CodFam { get; set; }
        public string ContVend { get; set; }
        public string ContStok { get; set; }
        public string ContCusto { get; set; }
        public string NomeMerc { get; set; }
        public string DataCria { get; set; }
        public string CodUtilizador { get; set; }
        public Nullable<decimal> Desconto { get; set; }
        public string Data_Alteracao { get; set; }
        public string Alterado_Por { get; set; }
        public Nullable<int> Prazo { get; set; }
        public byte[] IMAGEM { get; set; }

    }
}
