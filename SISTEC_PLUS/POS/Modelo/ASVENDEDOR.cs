﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASVENDEDOR
    {
        public string CodVend { get; set; }
        public string EMail { get; set; }
        public string NomeVend { get; set; }
        public string MORADA { get; set; }
        public string CODPOSTAL { get; set; }
        public string CONTRIB { get; set; }
        public string TELEFONE { get; set; }
        public Nullable<decimal> DESCMAX { get; set; }
        public string DataCria { get; set; }
        public string CodUtilizador { get; set; }
        public string CodZona { get; set; }
        public virtual ASZONA ASZONA { get; set; }


    }
}
