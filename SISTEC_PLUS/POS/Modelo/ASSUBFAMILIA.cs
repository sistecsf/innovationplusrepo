﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASSUBFAMILIA
    {

        public string CodSubFam { get; set; }
        public string CodFam { get; set; }
        public string Descricao { get; set; }
        public string DataCria { get; set; }
        public string CodUtilizador { get; set; }
        public virtual ASFamilia ASFamilia { get; set; }
   
    }
}
