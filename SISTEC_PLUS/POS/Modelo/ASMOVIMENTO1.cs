﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASMOVIMENTO1
    {
        public string Data { get; set; }
        public string DataCria { get; set; }
        public decimal NUMDOC { get; set; }
        public Nullable<int> QTD_ENTRADA { get; set; }
        public Nullable<int> QTD_SAIDA { get; set; }
        public Nullable<int> QTD_IGUAL { get; set; }
        public Nullable<int> SaldoEntradas { get; set; }
        public Nullable<int> SaldoSaidas { get; set; }
        public string CodDoc { get; set; }
        public string LojaDestino { get; set; }
        public string TIPMOV { get; set; }
        public string NomeUtilizador { get; set; }
        public string GUIA { get; set; }
        public string DataAnulacao { get; set; }
        public string NomeAnulacao { get; set; }
        public string NOMEENTIDADE { get; set; }
        public string STATUS { get; set; }
        public string CodLoja { get; set; }
        public string NomeLoja { get; set; }
        public string NomeArz { get; set; }
        public string CodArmz { get; set; }
        public string Referenc { get; set; }
        public string DescrArtigo { get; set; }
        public string DescrFamilia { get; set; }
        public string DescrSubFamilia { get; set; }
        public string LOCALI { get; set; }
    }
}
