﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASSeccao
    {

        public string CodSeccao { get; set; }
        public string DescrSeccao { get; set; }
        public string DataCria { get; set; }
        public string CodUtilizador { get; set; }
    }
}
