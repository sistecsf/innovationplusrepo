﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASLOJA_TMP
    {
         public string CodLoja { get; set; }
        public string DataCria { get; set; }
        public string NomeLoja { get; set; }
        public Nullable<decimal> ValorVendas { get; set; }
        public Nullable<decimal> ValorVendasD { get; set; }

    }
}
