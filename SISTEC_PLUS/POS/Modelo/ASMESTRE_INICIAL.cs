﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASMESTRE_INICIAL
    {
        public string Referenc { get; set; }
        public string CodLoja { get; set; }
        public string CodArmz { get; set; }
        public string CodFam { get; set; }
        public string CodSubFam { get; set; }
        public string DescrArtigo { get; set; }
        public string DESCR_SUP { get; set; }
        public string CODBAR { get; set; }
        public int QTD { get; set; }
        public decimal FOB { get; set; }
        public decimal PCA { get; set; }
        public decimal PVP { get; set; }
        public string LOCALI { get; set; }
        public string DataCria { get; set; }
        public Nullable<int> TEMPO { get; set; }
        public Nullable<int> MTBF { get; set; }
        public string OBS2 { get; set; }
        public string CAMBIO { get; set; }
        public string FORNEC2 { get; set; }
        public string CARA { get; set; }
        public Nullable<decimal> PONENC { get; set; }
        public string UNIDADE { get; set; }
        public string DESEMBALAGEM { get; set; }
        public string QUAEMBALAGEM { get; set; }
        public string TARA { get; set; }
        public Nullable<decimal> DESCMAX { get; set; }
        public Nullable<decimal> DESCMIN { get; set; }
        public Nullable<decimal> PERCQUEBRA { get; set; }
        public Nullable<int> GUARDAR { get; set; }
        public string ALTERAPRECO { get; set; }
        public string CONTASTOCKS { get; set; }
        public string STOCKNEGATIVO { get; set; }
        public Nullable<int> DEVOLVER { get; set; }
        public string NIVELDESC { get; set; }
        public Nullable<int> STOCKCONSIG { get; set; }
        public Nullable<int> STOCKMAX { get; set; }
        public Nullable<int> STOCKMIN { get; set; }
        public Nullable<int> STOCKTOTAL { get; set; }
        public Nullable<int> VAL_ENT { get; set; }
        public Nullable<int> VAL_SAIDO { get; set; }
        public Nullable<int> FORNEC { get; set; }
        public string OBS { get; set; }
        public string DATA_FABRICO { get; set; }
        public string PROMOCAO_I { get; set; }
        public string PROMOCAO_F { get; set; }
        public Nullable<decimal> PMC { get; set; }
        public string IMAGEM { get; set; }
        public string ANULADO { get; set; }
        public Nullable<decimal> NORMA { get; set; }
        public string COD_GRUPO { get; set; }
        public string RegEquip { get; set; }
        public string CodUtilizador { get; set; }
        public string CodSeccao { get; set; }
        public string Data_Expira { get; set; }
        public Nullable<decimal> Desconto { get; set; }
        public Nullable<decimal> Lucro { get; set; }
        public string CodMarca { get; set; }
        public string CodModelo { get; set; }
        public string TipoServico { get; set; }
        public string Permitir_Obs { get; set; }
        public string Activo { get; set; }
        public string Data_Alteracao { get; set; }
        public string Alterado_Por { get; set; }
        public Nullable<int> PontoEncomenda { get; set; }
        public string DataEncomenda { get; set; }
        public string Data_Lancamento { get; set; }



    }
}
