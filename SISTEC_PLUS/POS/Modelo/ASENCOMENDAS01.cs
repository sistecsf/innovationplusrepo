﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASENCOMENDAS01
    {
        public string CodLoja { get; set; }
        public string CodArmz { get; set; }
        public string Referenc { get; set; }
        public string DescrArtigo { get; set; }
        public string LOCALI { get; set; }
        public Nullable<int> QTD_STK { get; set; }
        public Nullable<int> QTD_SAIDA { get; set; }
        public Nullable<int> QTD_SUGESTAO { get; set; }
        public Nullable<int> QTD_ENCOMENDA { get; set; }
        public Nullable<decimal> FOB { get; set; }
        public Nullable<decimal> PCLD { get; set; }
        public Nullable<decimal> PVPD { get; set; }
        public string CodAO { get; set; }
        public string CodEntidade { get; set; }
        public string CodUtilizador { get; set; }
        public string DataCria { get; set; }
        public string CODIGOLOJA_GE { get; set; }
        public string CodigoArmzGE { get; set; }
        public Nullable<int> Indice { get; set; }
        public Nullable<int> IndiceX { get; set; }
        public Nullable<int> QTDGE { get; set; }

    }
}
