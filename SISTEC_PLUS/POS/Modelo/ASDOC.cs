﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.Modelo
{
    class ASDOC
    {
        public string CodDoc { get; set; }
        public string DataCria { get; set; }
        public string DescrDoc { get; set; }
        public string TIPOUTIL { get; set; }
        public string CCORRENTE { get; set; }
        public string CUSTO_PCA { get; set; }
        public string ALTPR { get; set; }
        public string ALTDATA { get; set; }
        public string ALTSTOCK { get; set; }
        public string AFETAVEND { get; set; }
        public string CodUtilizador { get; set; }
        public string TipoCCorrente { get; set; }
        public string TipoPOS { get; set; }
        public Nullable<int> Permissao { get; set; }
        public string UniMonet { get; set; }
        public string TipoDoc { get; set; }
        public string AfetaVolumeVenda { get; set; }
        public string Emitir_NL { get; set; }
        public string Emitir_CT { get; set; }
        public Nullable<int> NRCOPIAS { get; set; }
        public string Alterar_AO { get; set; }
        public string Permitir_Obs { get; set; }
        public string Display_Preco { get; set; }
        public string Display_Conta { get; set; }
        public string TIPOFILE { get; set; }
        public string ARMA_VENDA { get; set; }




    }
}
