﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS.POS
{
    public partial class FormFacturaDescritiva : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        int imposto;
        string codCliente, telefoneCliente, nomeCliente, codArea;
        string sFacDescriti, nUnicFD, sql, sql2;
        decimal nDosFD;
        string valorD, valorDesconto, statos, data, codigo, morada, factura, codEntidade, nomeEntidade, telefoneL, postal, Morada, PRECOD, DESCR, NomeAO;
        double desconto;
        DateTime Data;
        FacturaDescritiva form;
        public FormFacturaDescritiva()
        {
            InitializeComponent();
        }
        private void gravar()
        {
            if (rbInsentoImposto.Checked)
                imposto = 0;
            else if (cbImpostoConsumo.Checked)
                imposto = 5;
            else if (rbImpostoConsumo2.Checked)
                imposto = 10;
            SqlCommand cmd = new SqlCommand("Select DescrDoc From asdocs where coddoc= '" + Gerais.PARAM_CODDOC_FD + "'", conexao);
            conexao.Open();
            sFacDescriti = (string)cmd.ExecuteScalar();
            conexao.Close();
            SqlCommand comd = new SqlCommand("SELECT RTRIM(ISNULL(CODLOJA, '" + Variavel.codLoja + "')) + CAST (ISNULL( MAX( CAST(RIGHT (RTRIM(IDUNICO), LEN(RTRIM(IDUNICO)) -( LEN(ISNULL(CODLOJA,'" + Variavel.codLoja + "')) ) )AS INT ) ) , 1) + 1 AS VARCHAR(23)) AS IDUNICO FROM AREGDOC WITH (XLOCK) where CODLOJA= '" + Variavel.codLoja + "' AND LEFT(IDUNICO, LEN(CODLOJA) ) = '" + Variavel.codLoja + "' GROUP BY CODLOJA", conexao);
            conexao.Open();
            nUnicFD = (string)comd.ExecuteScalar();
            conexao.Close();
            SqlCommand comad = new SqlCommand("Select ISNULL(MAX(NumDoc),0) + 1 from ARegDoc WITH (XLOCK) where CodDoc= '" + Gerais.PARAM_CODDOC_FD + "' and CodLoja= '" + Variavel.codLoja + "'", conexao);
            conexao.Open();
            nDosFD = (decimal)comad.ExecuteScalar();
            conexao.Close();
            try { desconto = Convert.ToDouble(txtDesconto.Text); }
            catch { desconto = 0; }
            lbDesconto.Text = "" + (Convert.ToDouble(txtValor.Text) + (Convert.ToDouble(txtValor.Text) * imposto / 100) - (Convert.ToDouble(txtValor.Text) * desconto / 100));
            
            //*
            if (cmbMoeda.Text == Gerais.PARAM_MOEDA_NAC)
            {
                valorD = "" + Convert.ToDouble(lbDesconto.Text) / Convert.ToDouble(lbCambio.Text);
                valorDesconto = "" + Convert.ToDouble(lbDesconto.Text) / Convert.ToDouble(lbCambio.Text);
                sql = "INSERT INTO ARegDoc (IDUnico, DataCria,                               CodUtilizador,                       CodDoc,                      CodEntidade,              NumDoc,            CodLoja,                   CodAO,                PRECO,                       PRECOD,                    NumFact,                      Descr,                   Data_Lancamento,                         Moeda,                Desct,                   CodVend,             IDOrig,     STATUS, TaxaImpostoConsumo)" +
                    "VALUES ('" + nUnicFD + "', CONVERT(VARCHAR(20),GETDATE(),120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_FD + "', '" + txtCliente.Text + "', '" + nDosFD + "', '" + Variavel.codLoja + "', '" + codArea + "', '" + txtValor.Text + "', '" + valorD.Replace(',', '.') + "', '" + nDosFD + "', '" + txtDescreveDescricao.Text + "', CONVERT(VARCHAR(10),GETDATE(),120), '" + cmbMoeda.Text + "', '" + desconto + "', '" + lbVendedor.Text + "', '" + nUnicFD + "', 'E', '" + imposto + "' )";

                sql2 = "INSERT INTO AFCONCLI (IDUnico, Valor,                          ValorD,                          DataCria,                         CodEntidade,                CodUtilizador,                CodAO,                CodDoc,                                       Debito,                                DebitoD,                            CAMBIO,                                      Descricao,                 Data_Lancamento )" +
                    "VALUES ('" + nUnicFD + "', '" + txtValor.Text + "', '" + valorD.Replace(',', '.') + "', Convert(varchar(20),GetDate(),120), '" + txtCliente.Text + "', '" + Variavel.codUtilizador + "', '" + codArea + "', '" + Gerais.PARAM_CODDOC_FD + "', '" + lbDesconto.Text.Replace(',', '.') + "', '" + valorDesconto.Replace(',', '.') + "', '" + lbCambio.Text + "', '" + "FACTURA DESCRITIVA - " + codArea + "', Convert(varchar(10),GetDate(),120))";
                
            }
            if (cmbMoeda.Text == Gerais.PARAM_MOEDA_PDR)
            {
                valorD = "" + Convert.ToDouble(lbDesconto.Text) * Convert.ToDouble(lbCambio.Text);
                valorDesconto = "" + Convert.ToDouble(lbDesconto.Text) * Convert.ToDouble(lbCambio.Text);
                sql = "INSERT INTO ARegDoc (IDUnico,            DataCria,                   CodUtilizador,                        CodDoc,                       CodEntidade,             NumDoc,              CodLoja,                  CodAO,                     PRECO,                        PRECOD,                NumFact,                     Descr,                     Data_Lancamento,                      Moeda,               Desct,                   CodVend,                 IDOrig,     STATUS,   TaxaImpostoConsumo)" +
                    "VALUES ('" + nUnicFD + "', Convert(varchar(20),GetDate(),120), '" + Variavel.codUtilizador + "', '" + Gerais.PARAM_CODDOC_FD + "', '" + txtCliente.Text + "', '" + nDosFD + "', '" + Variavel.codLoja + "', '" + codArea + "', '" + valorD.Replace(',', '.') + "', '" + txtValor.Text + "', '" + nDosFD + "', '" + txtDescreveDescricao.Text + "', Convert(varchar(10),GetDate(),120), '" + cmbMoeda.Text + "', '" + desconto + "', '" + lbVendedor.Text + "', '" + nUnicFD + "', 'F', '" + imposto + "')";

                sql2 = "INSERT INTO AFCONCLI (IDUnico,        Valor,                       ValorD,                 DataCria,                       CodEntidade,                CodUtilizador,                 CodAO,                 CodDoc,                                           Debito,                           DebitoD,                                 CAMBIO,                            Descricao,                   Data_Lancamento)" +
                    "VALUES ('" + nUnicFD + "', '" + valorD.Replace(',', '.') + "', '" + txtValor.Text + "', Convert(varchar(20),GetDate(),120), '" + txtCliente.Text + "', '" + Variavel.codUtilizador + "', '" + codArea + "', '" + Gerais.PARAM_CODDOC_FD + "', '" + valorDesconto.Replace(',', '.') + "', '" + lbDesconto.Text.Replace(',', '.') + "', '" + lbCambio.Text + "', '" + "FACTURA DESCRITIVA - " + codArea + "', Convert(varchar(10),GetDate(),120))";
                
            }  //*/
            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                SqlCommand comand = new SqlCommand(sql);

                comand.Connection = conexao;
                comand.Transaction = tran;
                comand.ExecuteNonQuery();

                SqlCommand cõmad = new SqlCommand(sql2);
                cõmad.Connection = conexao;
                cõmad.Transaction = tran;
                cõmad.ExecuteNonQuery();

                tran.Commit();
                MessageBox.Show("OPERAÇÃO REALIZADA COM SUCESSO!");
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void recibo()
        {
            try
            {
                SqlCommand cmd3 = new SqlCommand("select R.NumFact, R.DataCria, R.CodEntidade, F.NomeEntidade, F.Morada, R.PRECOD, R.DESCR, A.NomeAO, RTRIM(R.CodLoja) from ARegDoc R with (NOLOCK), AFENTIDADE F with (NOLOCK), AIAREAORGANICA A WITH (NOLOCK), AFCONCLI C WITH (NOLOCK) where R.IDUnico = '" + nUnicFD + "' AND R.CodEntidade = F.CodEntidade AND R.CodAO = A.CodAO AND R.IDUnico = C.IDUnico", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd3.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        factura = reader[0].ToString();
                        data = reader[1].ToString();
                        codEntidade = reader[2].ToString();
                        nomeEntidade = reader[3].ToString();
                        Morada = reader[4].ToString();
                        PRECOD = reader[5].ToString();
                        DESCR = reader[6].ToString();
                        NomeAO = reader[7].ToString();
                        codigo = reader[8].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }

            try
            {
                SqlCommand cmd4 = new SqlCommand("Select Fone, Caixa_Post, Morada from ASLOJA WITH (NOLOCK) WHERE CodLoja = '" + codigo + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd4.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        telefoneL = reader[0].ToString();
                        postal = reader[1].ToString();
                        morada = reader[2].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
            Data = Convert.ToDateTime(data);
            data = "" + Data.Day + "-" + Data.Month + "-" + Data.Year;
            
            form = new FacturaDescritiva(nomeEntidade, codEntidade, Morada, morada, telefoneL, postal, nUnicFD, factura + " / " + codigo, NomeAO, DESCR, PRECOD, data);
            form.ShowDialog();
        }
        private void cmbNomeCliente()
        {
            int tipo = 0;
            SqlCommand cmd = new SqlCommand("SP_ComboEntidade", conexao);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@p_iTipoSeleccao", tipo));
            cmd.Parameters.Add(new SqlParameter("@p_sNivelUSER", Permissoes.nPrgClie));
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbNome.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void preemxeCombo(string sql, ComboBox combo)
        {
            SqlCommand cmd = new SqlCommand(sql);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    combo.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void limparCampos(Control.ControlCollection controles)
        {
            foreach (Control ctrl in controles)
            {
                //Se o contorle for um TextBox...
                if (ctrl is MetroFramework.Controls.MetroTextBox)
                {
                    ((MetroFramework.Controls.MetroTextBox)(ctrl)).Text = String.Empty;
                }
                //Se o contorle for um ComboBox...
                if (ctrl is MetroFramework.Controls.MetroComboBox)
                {
                    ((MetroFramework.Controls.MetroComboBox)(ctrl)).Text = null;
                    ((MetroFramework.Controls.MetroComboBox)(ctrl)).Text = "";
                    ((MetroFramework.Controls.MetroComboBox)(ctrl)).PromptText = "";
                }
                //Se o contorle for um RadioButton...
                if (ctrl is MetroFramework.Controls.MetroRadioButton)
                {
                    ((MetroFramework.Controls.MetroRadioButton)(ctrl)).Checked = false;
                }
            }
        }
        private void FormFacturaDescritiva_Load(object sender, EventArgs e)
        {
            btGravar.Enabled = false;
            rbInsentoImposto.Checked = true;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            limparCampos(this.Controls);
            limparCampos(groupBox2.Controls);
            limparCampos(cmbVendador.Controls);
            limparCampos(groupBox1.Controls);
            
            btGravar.Enabled = true;
            txtDescreveDescricao.Text = "";
            rbInsentoImposto.Checked = true;
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
        private void txtCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                try
                {
                    SqlCommand cmd2 = new SqlCommand("SELECT RTRIM(NomeEntidade), FONE FROM AFENTIDADE WITH (NOLOCK) WHERE RTRIM(CODENTIDADE) = '" + txtCliente.Text + "' ", conexao);
                    conexao.Open();
                    using (SqlDataReader reader = cmd2.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            nomeCliente = reader[0].ToString();
                            telefoneCliente = reader[1].ToString();
                        }
                    }
                    cmbNome.Text = nomeCliente;
                    cmbNome.PromptText = nomeCliente;
                }
                catch (Exception) { }
                finally 
                { 
                    conexao.Close();
                }
            }
        }
        private void btGravar_Click_1(object sender, EventArgs e)
        {
            if (txtCliente.Text != "")
            {
                if (cmbNome.Text != "" || cmbNome.PromptText != "")
                {
                    if (cmbArea1.Text != "" || cmbArea1.PromptText != "")
                    {
                        if (cmbMoeda.Text != "" || cmbMoeda.PromptText != "")
                        {
                            if (txtValor.Text != "")
                            {
                                if (txtDescreveDescricao.Text != "")
                                {
                                    gravar();
                                    recibo();
                                }
                                else
                                    MessageBox.Show("O campo [Descrição sobre o objectivo da factura] tem de ser prenxido!");
                            }
                            else
                                MessageBox.Show("O campo [Valor] tem de ser prenxido!");
                        }
                        else
                            MessageBox.Show("O campo [Moeda] tem de ser prenxido!");
                    }
                    else
                        MessageBox.Show("O campo [Área] tem de ser prenxido!");
                }
                else
                    MessageBox.Show("O campo [nome do cliente] tem de ser prenxido!");
            }
            else
                MessageBox.Show("O campo [código do cliente] tem de ser prenxido!");
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            FormFacturaDescritivaApagar form = new FormFacturaDescritivaApagar();
            FormFacturaDescritivaApagar.tipo = 1;
            form.ShowDialog();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            FormFacturaDescritivaConsulta form = new FormFacturaDescritivaConsulta();
            FormFacturaDescritivaConsulta.tipo = 1;
            form.ShowDialog();
        }

        private void cmbMoeda_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            lbCambio.Text = Variavel.cambio;
        }
        private void cmbNome_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("SELECT RTRIM(CODENTIDADE), FONE FROM AFENTIDADE WITH (NOLOCK) WHERE RTRIM(NomeEntidade) = '" + cmbNome.Text + "' ", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codCliente = reader[0].ToString();
                        telefoneCliente = reader[1].ToString();
                    }
                }
                txtCliente.Text = codCliente;
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }

        private void cmbNome_MouseClick(object sender, MouseEventArgs e)
        {
            if (cmbNome.Items.Count <= 0)
            {
                cmbNome.Items.Clear();
                //Cursor.Current = Cursors.WaitCursor;
                cmbNomeCliente();
            }
        }
        private void metroComboBox2_MouseClick(object sender, MouseEventArgs e)
        {
            string sql = @"SELECT NomeEntidade FROM AFENTIDADE with (NoLock) WHERE CodTipoCli = 'VEN' ORDER BY NomeEntidade";
            cmbVendedor1.Items.Clear();
            preemxeCombo(sql, cmbVendedor1);
        }

        private void cmbArea1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("Select CodAO from AIAREAORGANICA WITH (NOLOCK) where NomeAO = '" + cmbArea1.Text + "'", conexao);
            conexao.Open();
            codArea = (string)cmd.ExecuteScalar();
            conexao.Close();
        }

        private void cmbArea1_MouseClick_1(object sender, MouseEventArgs e)
        {
            string sql = @"Select NomeAO from AIAREAORGANICA WITH (NOLOCK) WHERE ISNULL(TO_DISPLAY,'S') ='S'";
            cmbArea1.Items.Clear();
            preemxeCombo(sql, cmbArea1);
        }

        private void FormFacturaDescritiva_Leave(object sender, EventArgs e)
        {
            limparCampos(this.Controls);
            limparCampos(groupBox2.Controls);
            limparCampos(cmbVendador.Controls);
            limparCampos(groupBox1.Controls);

            btGravar.Enabled = true;
            txtDescreveDescricao.Text = "";
            rbInsentoImposto.Checked = true;
        }
    }
}
