﻿namespace SISTEC_PLUS.OPS
{
    partial class FormAberturaFechodoDia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAberturaFechodoDia));
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.cbFecharDia = new MetroFramework.Controls.MetroCheckBox();
            this.txtLancamentoDia = new MetroFramework.Controls.MetroTextBox();
            this.txtAberturoPor = new MetroFramework.Controls.MetroTextBox();
            this.txtFechadoPor = new MetroFramework.Controls.MetroTextBox();
            this.lbAbertura = new MetroFramework.Controls.MetroLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 85);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(123, 19);
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Lançamento do Dia";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(23, 124);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(75, 19);
            this.metroLabel2.TabIndex = 1;
            this.metroLabel2.Text = "Aberto Por";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(349, 124);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(84, 19);
            this.metroLabel3.TabIndex = 2;
            this.metroLabel3.Text = "Fechado Por";
            // 
            // cbFecharDia
            // 
            this.cbFecharDia.AutoSize = true;
            this.cbFecharDia.Location = new System.Drawing.Point(349, 85);
            this.cbFecharDia.Name = "cbFecharDia";
            this.cbFecharDia.Size = new System.Drawing.Size(88, 15);
            this.cbFecharDia.TabIndex = 3;
            this.cbFecharDia.Text = "Fechar o Dia";
            this.cbFecharDia.UseSelectable = true;
            // 
            // txtLancamentoDia
            // 
            this.txtLancamentoDia.Lines = new string[0];
            this.txtLancamentoDia.Location = new System.Drawing.Point(152, 81);
            this.txtLancamentoDia.MaxLength = 32767;
            this.txtLancamentoDia.Name = "txtLancamentoDia";
            this.txtLancamentoDia.PasswordChar = '\0';
            this.txtLancamentoDia.ReadOnly = true;
            this.txtLancamentoDia.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtLancamentoDia.SelectedText = "";
            this.txtLancamentoDia.Size = new System.Drawing.Size(138, 23);
            this.txtLancamentoDia.TabIndex = 4;
            this.txtLancamentoDia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtLancamentoDia.UseSelectable = true;
            // 
            // txtAberturoPor
            // 
            this.txtAberturoPor.Lines = new string[0];
            this.txtAberturoPor.Location = new System.Drawing.Point(152, 120);
            this.txtAberturoPor.MaxLength = 32767;
            this.txtAberturoPor.Name = "txtAberturoPor";
            this.txtAberturoPor.PasswordChar = '\0';
            this.txtAberturoPor.ReadOnly = true;
            this.txtAberturoPor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAberturoPor.SelectedText = "";
            this.txtAberturoPor.Size = new System.Drawing.Size(138, 23);
            this.txtAberturoPor.TabIndex = 5;
            this.txtAberturoPor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtAberturoPor.UseSelectable = true;
            // 
            // txtFechadoPor
            // 
            this.txtFechadoPor.Lines = new string[0];
            this.txtFechadoPor.Location = new System.Drawing.Point(476, 120);
            this.txtFechadoPor.MaxLength = 32767;
            this.txtFechadoPor.Name = "txtFechadoPor";
            this.txtFechadoPor.PasswordChar = '\0';
            this.txtFechadoPor.ReadOnly = true;
            this.txtFechadoPor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFechadoPor.SelectedText = "";
            this.txtFechadoPor.Size = new System.Drawing.Size(138, 23);
            this.txtFechadoPor.TabIndex = 6;
            this.txtFechadoPor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFechadoPor.UseSelectable = true;
            // 
            // lbAbertura
            // 
            this.lbAbertura.AutoSize = true;
            this.lbAbertura.Location = new System.Drawing.Point(257, 54);
            this.lbAbertura.Name = "lbAbertura";
            this.lbAbertura.Size = new System.Drawing.Size(0, 0);
            this.lbAbertura.TabIndex = 9;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(241, 191);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(141, 25);
            this.toolStrip1.TabIndex = 109;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(61, 22);
            this.toolStripButton1.Text = "Gravar";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(46, 22);
            this.toolStripButton2.Text = "Sair";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // FormAberturaFechodoDia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 249);
            this.ControlBox = false;
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.lbAbertura);
            this.Controls.Add(this.txtFechadoPor);
            this.Controls.Add(this.txtAberturoPor);
            this.Controls.Add(this.txtLancamentoDia);
            this.Controls.Add(this.cbFecharDia);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Name = "FormAberturaFechodoDia";
            this.Text = "Abertura ou Fecho do Dia";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormAberturaFechodoDia_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroCheckBox cbFecharDia;
        private MetroFramework.Controls.MetroTextBox txtLancamentoDia;
        private MetroFramework.Controls.MetroTextBox txtAberturoPor;
        private MetroFramework.Controls.MetroTextBox txtFechadoPor;
        private MetroFramework.Controls.MetroLabel lbAbertura;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
    }
}