﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS.Recibos
{
    public partial class FormFacturaDescritivaApagar : MetroFramework.Forms.MetroForm
    {
        public static string dataIniacial, dataFim, sql, statos, statosB;
        public static int tipo;
        DateTime Data, dataFatura, dataAtual;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public FormFacturaDescritivaApagar()
        {
            InitializeComponent();
        }
        
        private void prenxerGrid()
        {
            sql = @"select DISTINCT CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105), R.CodDoc, R.DESCR, R.NumDoc, LTrim(RTrim(C.CodEntidade)) + ' - ' +" +
                              "LTrim(RTrim(C.NomeEntidade)) AS CLIENTE, R.CodUtil_Anulacao, R.Data_Anulacao, U.NomeUtilizador," +
                                "RTRIM(R.IDunico), U.NOMEUTILIZADOR, R.CodLoja from AFCONCLI F WITH (NOLOCK), ARegDoc R WITH (NOLOCK), AFENTIDADE C WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK) " +
                                "where R.CodDoc = 'FD' AND R.CodEntidade = C.CodEntidade AND R.CodUtilizador = U.CodUtilizador ";
            if (Convert.ToInt32(Permissoes.nPrgPos) < 3)
                sql = sql + "AND R.codutilizador = '" + Variavel.codUtilizador + "' ";
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                sql = sql + "AND CONVERT(DATETIME, R.DataCria, 120) > CONVERT(DATETIME, '" + dataIniacial + "', 120) AND CONVERT(DATETIME, R.DataCria, 120) <= CONVERT(DATETIME, '" + dataFim + " 18:59" + "', 120)";
            if (txtNumero.Text != "")
                sql = sql + " AND C.CodEntidade LIKE '" + txtNumero.Text + "%'";
            if (txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '" + txtNome.Text + "%'";
            if (txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '" + txtNome.Text + "%'";
            if (txtID.Text != "")
                sql = sql + " And R.IDunico Like '%" + txtID.Text + "%'";
            if (txtNrDoc.Text != "")
                sql = sql + " AND R.NumDoc LIKE '%" + txtNrDoc.Text + "%' ";
            sql = sql + " ORDER BY CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME),105), R.coddoc, NUMDOC, CLIENTE";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
                conexao.Close();
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGrid2()
        {
            sql = @"select DISTINCT CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105), R.CodDoc, R.DESCR, R.NumDoc, LTrim(RTrim(C.CodEntidade)) + ' - ' +" +
                              "LTrim(RTrim(C.NomeEntidade)) AS CLIENTE, R.CodUtil_Anulacao, R.Data_Anulacao, U.NomeUtilizador," +
                                "RTRIM(R.IDunico), U.NOMEUTILIZADOR, R.CodLoja from AFCONCLI F WITH (NOLOCK), ARegDoc R WITH (NOLOCK), ASDETALHE H with (NOLOCK), AFENTIDADE C WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK) " +
                                "where R.CodDoc = 'FD' AND R.CodEntidade = C.CodEntidade AND R.CodUtilizador = U.CodUtilizador AND R.IDunico = H.IDunico ";
            if (Convert.ToInt32(Permissoes.nPrgPos) < 3)
                sql = sql + "AND R.codutilizador = '" + Variavel.codUtilizador + "' ";
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                sql = sql + "AND CONVERT(DATETIME, R.DataCria, 120) > CONVERT(DATETIME, '" + dataIniacial + "', 120) AND CONVERT(DATETIME, R.DataCria, 120) <= CONVERT(DATETIME, '" + dataFim + " 18:59" + "', 120)";
            if (txtNumero.Text != "")
                sql = sql + " AND C.CodEntidade LIKE '" + txtNumero.Text + "%'";
            if (txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '" + txtNome.Text + "%'";
            if (txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '" + txtNome.Text + "%'";
            if (txtID.Text != "")
                sql = sql + " And R.IDunico Like '%" + txtID.Text + "%'";
            if (txtNrDoc.Text != "")
                sql = sql + " AND R.NumDoc LIKE '%" + txtNrDoc.Text + "%' ";
            sql = sql + " ORDER BY CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME),105), R.coddoc, NUMDOC, CLIENTE";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
                conexao.Close();
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGrid3()
        {
            sql = @"select DISTINCT CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105), R.CodDoc, R.DESCR, R.NumDoc, LTrim(RTrim(C.CodEntidade)) + ' - ' +" +
                              "LTrim(RTrim(C.NomeEntidade)) AS CLIENTE, R.CodUtil_Anulacao, R.Data_Anulacao, U.NomeUtilizador," +
                                "RTRIM(R.IDunico), U.NOMEUTILIZADOR, R.CodLoja from ARegDoc R WITH (NOLOCK), AFENTIDADE C WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK) " +
                                "where R.CodDoc = 'PD' AND R.CodEntidade = C.CodEntidade AND R.CodUtilizador = U.CodUtilizador ";
            if (Convert.ToInt32(Permissoes.nPrgPos) < 3)
                sql = sql + "AND R.codutilizador = '" + Variavel.codUtilizador + "' ";
            if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
                sql = sql + "AND CONVERT(DATETIME, R.DataCria, 120) > CONVERT(DATETIME, '" + dataIniacial + "', 120) AND CONVERT(DATETIME, R.DataCria, 120) <= CONVERT(DATETIME, '" + dataFim + " 18:59" + "', 120)";
            if (txtNumero.Text != "")
                sql = sql + " AND C.CodEntidade LIKE '" + txtNumero.Text + "%'";
            if (txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '" + txtNome.Text + "%'";
            if (txtNome.Text != "")
                sql = sql + " AND C.NomeEntidade LIKE '" + txtNome.Text + "%'";
            if (txtID.Text != "")
                sql = sql + " And R.IDunico Like '%" + txtID.Text + "%'";
            if (txtNrDoc.Text != "")
                sql = sql + " AND R.NumDoc LIKE '%" + txtNrDoc.Text + "%' ";
            sql = sql + " ORDER BY CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME),105), R.coddoc, NUMDOC, CLIENTE";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
                conexao.Close();
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void apagar(DataGridView dataGrid)
        {
            dataFatura = Convert.ToDateTime(dataGrid.CurrentRow.Cells[0].Value.ToString());
                    dataAtual = Convert.ToDateTime(Variavel.dataAtual);
                    try
                    {
                        SqlCommand cmd4 = new SqlCommand("SELECT A.STATUS, B.STATUS FROM AREGDOC A WITH (NOLOCK), AREGDOC B WITH (NOLOCK) WHERE A.IDUNICO= '" + dataGrid.CurrentRow.Cells[5].Value.ToString() + "' AND B.IDUNICO= '" + dataGrid.CurrentRow.Cells[5].Value.ToString() + "'", conexao);
                        conexao.Open();
                        using (SqlDataReader reader = cmd4.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                statos = reader[0].ToString();
                                statosB = reader[1].ToString();
                            }
                        }
                    }
                    catch (Exception) { }
                    finally { conexao.Close(); }
                    const string message = "Pretende anular esse documento?";
                    const string caption = "CONFIRMAÇÃO";
                    var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (Convert.ToInt32(Permissoes.nPrgPos) < 5)
                    {
                        if (dataFatura < dataAtual)
                            MessageBox.Show("Não tens permição para anular esse documento!");
                        else
                        {
                            if (statos == "A")
                                MessageBox.Show("Esse documento já foi anulado!");
                            else if (statosB == "P")
                                MessageBox.Show("Não pode anular este documento porque já foi pago!");
                            else{
                                if (result == DialogResult.Yes)
                                {
                                    conexao.Open();
                                    SqlTransaction tran = conexao.BeginTransaction();
                                    try
                                    {
                                        SqlCommand comand = new SqlCommand("delete AFCONCLI where IDUnico = '" + dataGrid.CurrentRow.Cells[5].Value.ToString() + "'");

                                        comand.Connection = conexao;
                                        comand.Transaction = tran;
                                        comand.ExecuteNonQuery();

                                        SqlCommand comnd = new SqlCommand("update ARegDoc set STATUS = 'A', Data_Anulacao = CONVERT(VARCHAR(20),GETDATE(),120), CodUtil_Anulacao = '" + Variavel.codUtilizador + "' where IDUnico = '" + dataGrid.CurrentRow.Cells[5].Value.ToString() + "'");
                                        comnd.Connection = conexao;
                                        comnd.Transaction = tran;
                                        comnd.ExecuteNonQuery();

                                        tran.Commit();
                                        MessageBox.Show("Documento Anulado com Sucesso!");
                                    }
                                    catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
                                    finally { conexao.Close(); }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (statos == "A")
                            MessageBox.Show("Esse documento já foi anulado!");
                        else if (statosB == "P")
                            MessageBox.Show("Não pode anular este documento porque já foi pago!");
                        else
                        {
                            if (result == DialogResult.Yes)
                            {
                                conexao.Open();
                                SqlTransaction tran = conexao.BeginTransaction();
                                try
                                {
                                    SqlCommand comand = new SqlCommand("delete AFCONCLI where IDUnico = '" + dataGrid.CurrentRow.Cells[5].Value.ToString() + "'");

                                    comand.Connection = conexao;
                                    comand.Transaction = tran;
                                    comand.ExecuteNonQuery();

                                    SqlCommand comnd = new SqlCommand("update ARegDoc set STATUS = 'A', Data_Anulacao = CONVERT(VARCHAR(20),GETDATE(),120), CodUtil_Anulacao = '" + Variavel.codUtilizador + "' where IDUnico = '" + dataGrid.CurrentRow.Cells[5].Value.ToString() + "'");
                                    comnd.Connection = conexao;
                                    comnd.Transaction = tran;
                                    comnd.ExecuteNonQuery();
                                    tran.Commit();
                                    MessageBox.Show("Documento Anulado com Sucesso!");
                                }
                                catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
                                finally { conexao.Close(); }
                            }
                        }
                    }
                
        }
        private void FormFacturaDescritivaApagar_Load(object sender, EventArgs e)
        {
            txtDataInicial.Text = "01-01-" + Variavel.dataAtual.Year;
            txtDataFinal.Text = "" + Variavel.dataAtual.Day + "-" +Variavel.dataAtual.Month + "-" +Variavel.dataAtual.Year;
        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Data = Convert.ToDateTime(txtDataInicial.Text);
            dataIniacial = "" + Data.Year + "-" + Data.Month + "-" + Data.Day;
            Data = Convert.ToDateTime(txtDataFinal.Text);
            dataFim = "" + Data.Year + "-" + Data.Month + "-" + Data.Day;

            metroGrid1.AutoGenerateColumns = false;
            Cursor.Current = Cursors.WaitCursor;
            if (tipo == 1)
                prenxerGrid();
            else if (tipo == 2)
                prenxerGrid2();
            else if (tipo == 3)
                prenxerGrid3();
        }

        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    try { apagar(metroGrid1); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try { apagar(metroGrid1); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void txtDataInicial_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtDataInicial.Text = ""; }
        }

        private void txtDataFinal_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtDataFinal.Text = ""; }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
