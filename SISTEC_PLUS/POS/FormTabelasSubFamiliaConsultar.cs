﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using SISTEC_PLUS.POS;
using SISTEC_PLUS;
using SISTEC_PLUS.OPS;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormTabelasSubFamiliaConsultar : MetroFramework.Forms.MetroForm
    {
        string codFamilia, sql;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public FormTabelasSubFamiliaConsultar()
        {
            InitializeComponent();
        }
        private void prenxeCombo()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeMerc from ASFamilia ORDER BY NomeMerc");
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbFamilia.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void prenxeGrid()
        {
            sql = @"select CodSubFam, Descricao,CodFam from ASSUBFAMILIA where CodSubFam like  '%" + txtCodigo.Text + "%' ";
            if (txtCodigo.Text != "")
                sql = sql + "and CodSubFam =  '" + txtCodigo.Text + "' ";
            if (txtDesignacao.Text != "")
                sql = sql + "and Descricao =  '" + txtDesignacao.Text + "' ";
            if (cmbFamilia.Text != "")
                sql = sql + "and CodFam =  '" + codFamilia + "'";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("Loja");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void gridDuploClick(DataGridView dataGrid)
        {
            FormTabelas.Scodigo = dataGrid.CurrentRow.Cells[0].Value.ToString();
            FormTabelas.designacao = dataGrid.CurrentRow.Cells[1].Value.ToString();
            FormTabelas.Sfamilia = dataGrid.CurrentRow.Cells[2].Value.ToString();
            //*
            if (FormTabelas.Sfamilia != null)
            {
                SqlCommand cmd2 = new SqlCommand("SELECT NomeMerc FROM ASFamilia where CodFam=@loja ", conexao);
                cmd2.Parameters.Add(new SqlParameter("@loja", FormTabelas.Sfamilia));
                conexao.Open();
                try { FormTabelas.SnomeFamilia = (string)cmd2.ExecuteScalar(); }
                catch (Exception) { FormTabelas.Fpraso = 0; }

                conexao.Close();
            } //*/
            this.DialogResult = DialogResult.OK;
        }
        private void FormTabelasSubFamiliaConsultar_Load(object sender, EventArgs e)
        {
            metroGrid1.AutoGenerateColumns = false;
            prenxeGrid();
            cmbFamilia.Items.Clear();
            prenxeCombo();
        }

        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    try { gridDuploClick(metroGrid1); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void cmbFamilia_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("select CodFam from ASFamilia where NomeMerc = '"+cmbFamilia.Text+"'", conexao);
            try
            {
                cmd.Connection = conexao;
                conexao.Open();
                codFamilia = (string)cmd.ExecuteScalar();
                prenxeGrid();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }

        private void cmbFamilia_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void txtDesignacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try { gridDuploClick(metroGrid1); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }
    }
}
