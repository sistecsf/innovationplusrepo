﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.OPS;

namespace SISTEC_PLUS.POS
{
    public partial class FormTabelasLojaConta : MetroFramework.Forms.MetroForm
    {
        public static string codigo = "";
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public FormTabelasLojaConta()
        {
            InitializeComponent();
        }
        private void prenxeGrid()
        {
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(@"select CodConta, NomeConta from ACPLANO_2015 WHERE LEFT(CODCONTA,1) IN ('6')", conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void FormTabelasLojaConta_Load(object sender, EventArgs e)
        {
            metroGrid1.AutoGenerateColumns = false;
            prenxeGrid();
            DateTime agora = DateTime.Now;
            
            int ano = agora.Year;
            txtAno.Text = ""+ano;
            txtAno.Enabled = false;
        }
        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
            
        }

        private void metroGrid1_CellContentDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    try
                    {
                        FormTabelas.Lconta1 = metroGrid1.CurrentRow.Cells[0].Value.ToString();
                        this.DialogResult = DialogResult.OK;
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void metroGrid1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try
                {
                    FormTabelas.Lconta1 = metroGrid1.CurrentRow.Cells[0].Value.ToString();
                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }
    }
}
