﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendasFormaPagamento : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        public FormVendasFormaPagamento()
        {
            InitializeComponent();
        }
        private void combFormaPagamente()
        {
            SqlCommand cmd = new SqlCommand(@"select Descricao from ATTIPOPAGAMENTO where Cash = 'S' ORDER BY Descricao");
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbFormaPagamento.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void FormVendasFormaPagamento_Load(object sender, EventArgs e)
        {
            combFormaPagamente();
            txtValor.ReadOnly = true;
            txtValor.Text = FormVendas.totalkz;
        }

        private void cmbFormaPagamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("select CodPagamento from ATTIPOPAGAMENTO where Descricao ='" + cmbFormaPagamento.Text + "'", conexao);
            conexao.Open();
            try { lbFormaPagamento.Text = (string)cmd2.ExecuteScalar(); }
            catch (Exception) { FormVendas.sForma_Pagamento = ""; }
            finally{ conexao.Close();}
        }

        private void txtValorRecebido_TextChanged(object sender, EventArgs e)
        {
            try { lbTroco.Text = "" + (Convert.ToDouble(txtValorRecebido.Text) - Convert.ToDouble(txtValor.Text)); }
            catch { lbTroco.Text = "" + (0 - Convert.ToDouble(txtValor.Text)); }
        }
        private void btO_Click(object sender, EventArgs e)
        {
            if (Convert.ToDouble(txtValorRecebido.Text) > Convert.ToDouble(txtValor.Text))
            {
                FormVendas.sForma_Pagamento = lbFormaPagamento.Text;
                this.DialogResult = DialogResult.OK;
            }
            else
                MessageBox.Show("O valor recebido tem de ser maior ou igual ao valor a pagar");
        }
    }
}
