﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISTEC_PLUS.POS.FormsRelatoriosPOS
{
    class AcumPorDocAz
    {

        public string padrao, tipoRelatorio, loja, strATCAIXA;

        public string Armazem { get; set; }
        public string NomeLoja { get; set; }
        public string Valor { get; set; }
        public string Contravalor { get; set; }
        public string Padrao { get; set; }

        public string CodArmazem { get; set; }

        public string Doc { get; set; }

        public string qtd { get; set; }

      

        public static List<AcumPorDocAz> Recibo(string[] Armazem, string[] NomeLoja, string[] Valor, string[] Contravalor, string[] Padrao, string[] CodArmazem, string[] Doc, string[] qtd, int tamanho)
        {
            List<AcumPorDocAz> cont = new List<AcumPorDocAz>();
            for (int i = 0; i < tamanho; i++)
            {
                AcumPorDocAz c = new AcumPorDocAz()
                {
                    Armazem = Armazem[i],
                    NomeLoja = NomeLoja[i],

                    Valor = Valor[i],
                    Contravalor = Contravalor[i],
                    Padrao = Padrao[i],
                    CodArmazem = CodArmazem[i],
                    Doc = Doc[i],

                    qtd = qtd[i]

                };
                cont.Add(c);
            }

            return cont;
        }



    }
}