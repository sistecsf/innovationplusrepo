﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;



namespace SISTEC_PLUS.POS.FormsRelatoriosPOS
{
    public partial class FormAcumuladosArmazem : MetroFramework.Forms.MetroForm
    {

        string[] Armazem, NomeLoja, Valor, Contravalor, Padrao, CodArmazem, Doc, qtd;
        string operador, sql1, sql2, sql3;


        int tamanho, i;
        string codLoja;
        string CODLOJA, codArmz;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        //SqlConnection conexao = new SqlConnection(Variavel.Conn);




        public FormAcumuladosArmazem()
        {
            InitializeComponent();
        }
        private void FormAcumuladosArmazem_Load(object sender, EventArgs e)
        {

            radioButton1.Checked = true;

            dtpDataInicial.Format = DateTimePickerFormat.Custom;

            dtpDataInicial.CustomFormat = "yyyy-MM-dd";

            dtpDataFinal.Format = DateTimePickerFormat.Custom;

            dtpDataFinal.CustomFormat = "yyyy-MM-dd";



            // PESQUISA  A LOJA NO BANCO DE  DADOS
            SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT RTRIM (LTRIM (NOMELOJA)) FROM ASLOJA WITH (NOLOCK)");
            cmd.Connection = conexao;



            try
            {

                conexao.Open();
                cmd.Connection = conexao;
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {

                    cmbLoja.Items.Add(leitor.GetValue(0));

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }








        }

        // /*  FUNCAO  PARA CHAMAR A FUNÇAO IMPRIMIR
        private void imprimirRelatorio()
        {


            tamanho = i = 0;


            try
            {
                // SqlCommand cmd2 = new SqlCommand("SELECT NOMELOJA, CODARMZ, NOMEARZ, DOC,SUM(QTD), SUM(NAC), SUM(CONTRAV), SUM(PAD)From FN_POSVENDAS_ResumoVendaArmz ('"+  dtpDataInicial.Text +"',  '"+ dtpDataFinal.Text+"'), ASLOJA With (NoLock) Where CODLOJA=  '" + codLoja + "'   Group By NOMELOJA, CODARMZ, NOMEARZ, DOC Order By NOMELOJA, DOC, NOMEARZ ", conexao);

                SqlCommand cmd2 = new SqlCommand(@"SELECT NOMELOJA, CODARMZ, NOMEARZ, DOC,SUM(QTD), SUM(NAC), SUM(CONTRAV), SUM(PAD)From FN_POSVENDAS_ResumoVendaArmz ('2014-09-01',  '2015-01-15'), ASLOJA With (NoLock) Where CODLOJA= '" + codLoja + "'    Group By NOMELOJA, CODARMZ, NOMEARZ, DOC Order By NOMELOJA, DOC, NOMEARZ ", conexao);


                conexao.Open();

                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tamanho = tamanho + 1;
                    }
                }
            }


            catch (Exception) { }

            finally { conexao.Close(); }
            //  int i = 0;
            Armazem = new string[tamanho]; NomeLoja = new string[tamanho]; Valor = new string[tamanho]; Contravalor = new string[tamanho]; Padrao = new string[tamanho]; CodArmazem = new string[tamanho]; Doc = new string[tamanho]; qtd = new string[tamanho];


            try
            {
                //SqlCommand cmd = new SqlCommand("SELECT NOMELOJA, CODARMZ, NOMEARZ, DOC,SUM(QTD), SUM(NAC), SUM(CONTRAV), SUM(PAD)From FN_POSVENDAS_ResumoVendaArmz ('"+  dtpDataInicial.Text +"', '"+ dtpDataFinal.Text+"'),ASLOJA With (NoLock) Where CODLOJA='"+ cmbLoja.Text +"' Group By NOMELOJA, CODARMZ, NOMEARZ, DOC Order By NOMELOJA, DOC, NOMEARZ ", conexao);

                SqlCommand cmd = new SqlCommand(@"SELECT NOMELOJA, CODARMZ, NOMEARZ, DOC,SUM(QTD), SUM(NAC), SUM(CONTRAV), SUM(PAD)From FN_POSVENDAS_ResumoVendaArmz ('2014-09-01',  '2015-01-15'), ASLOJA With (NoLock) Where CODLOJA='" + codLoja + "'     Group By NOMELOJA, CODARMZ, NOMEARZ, DOC Order By NOMELOJA, DOC, NOMEARZ ", conexao);

                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Armazem[i] = reader[2].ToString();
                        NomeLoja[i] = reader[0].ToString();
                        Valor[i] = reader[5].ToString();
                        Contravalor[i] = reader[6].ToString();
                        Padrao[i] = reader[7].ToString();
                        CodArmazem[i] = reader[1].ToString();

                        Doc[i] = reader[3].ToString();
                        qtd[i] = reader[4].ToString();
                        i++;
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }







        }

        // */


        private void ImprimirRelatorio2()
        {


            if (radioButton1.Checked == true)
            {
                MessageBox.Show("geral");

                sql1 = @"SELECT  CODARMZ, NOMEARZ, DOC,SUM(QTD), SUM(NAC), SUM(CONTRAV), SUM(PAD) , NOMELOJA  From FN_POSVENDAS_ResumoVendaArmz ('" + dtpDataInicial.Text + "', '" + dtpDataFinal.Text + "'), ASLOJA With (NoLock) Where  CODLOJA=CODLOJA Group By NOMELOJA, CODARMZ, NOMEARZ, DOC Order By NOMELOJA, DOC, NOMEARZ ";

            }

            else
            {




                // -----PARCIAL LOJA E ARMAZEM
                if (radioButton2.Checked == true && cmbLoja.Text != "" && cmbMercadoria.Text != "")
                {
                    MessageBox.Show("parcial 1");
                    //sql1 = @"SELECT CODARMZ, NOMEARZ, DOC, SUM(QTD), SUM(NAC),SUM(CONTRAV), SUM(PAD)From FN_POSVENDAS_ResumoVendaArmz  ('" + dtpDataInicial.Text + "', '" + dtpDataFinal.Text + "') WHERE LOJA = '" + codLoja + "' and CODARMZ = '" + codArmz + "' Group By NOMEARZ, CODARMZ, DOC Order By DOC ";


                    sql1 = @" SELECT  CODARMZ, NOMEARZ, DOC,SUM(QTD), SUM(NAC), SUM(CONTRAV), SUM(PAD), NOMELOJA From FN_POSVENDAS_ResumoVendaArmz ('" + dtpDataInicial.Text + "', '" + dtpDataFinal.Text + "'), ASLOJA With (NoLock) Where  CODLOJA= '" + codLoja + "' AND CODARMZ =  '" + codArmz + "' Group By NOMELOJA, CODARMZ, NOMEARZ, DOC Order By NOMELOJA, DOC, NOMEARZ";

                }






                 // -----PARCIAL LOJA
                else if (radioButton2.Checked == true && cmbLoja.Text != "")
                {
                    MessageBox.Show("parcial 2");
                    //sql1 = @"SELECT CODARMZ, NOMEARZ, DOC, SUM(QTD), SUM(NAC),SUM(CONTRAV), SUM(PAD) From FN_POSVENDAS_ResumoVendaArmz ('" + dtpDataInicial.Text + "', '" + dtpDataFinal.Text + "') WHERE LOJA = '" + codLoja + "' Group By NOMEARZ, CODARMZ, DOC Order By DOC , NOMEARZ ";

                    sql1 = @"  SELECT  CODARMZ, NOMEARZ, DOC,SUM(QTD), SUM(NAC), SUM(CONTRAV), SUM(PAD), NOMELOJA From FN_POSVENDAS_ResumoVendaArmz ('" + dtpDataInicial.Text + "', '" + dtpDataFinal.Text + "') Where  CODLOJA='" + codLoja + "' Group By NOMELOJA, CODARMZ, NOMEARZ, DOC Order By NOMELOJA, DOC, NOMEARZ";



                }







            }

            tamanho = i = 0;

            try
            {
                SqlCommand cmd2 = new SqlCommand(sql1, conexao);

                conexao.Open();

                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tamanho = tamanho + 1;
                    }
                }
            }


            catch (Exception) { }

            finally { conexao.Close(); }
            //  int i = 0;
            Armazem = new string[tamanho]; NomeLoja = new string[tamanho]; Valor = new string[tamanho]; Contravalor = new string[tamanho]; Padrao = new string[tamanho]; CodArmazem = new string[tamanho]; Doc = new string[tamanho]; qtd = new string[tamanho];

            try
            {

                SqlCommand cmd = new SqlCommand(sql1, conexao);

                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Armazem[i] = reader[1].ToString();
                        NomeLoja[i] = reader[7].ToString();
                        Valor[i] = reader[4].ToString();
                        Contravalor[i] = reader[5].ToString();
                        Padrao[i] = reader[6].ToString();
                        CodArmazem[i] = reader[0].ToString();

                        Doc[i] = reader[2].ToString();
                        qtd[i] = reader[3].ToString();
                        i++;
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }



        }


        private void ImprimirRelatorio3()
        {
            sql1 = @"SELECT NOMELOJA, CODARMZ, NOMEARZ, DOC,SUM(QTD), SUM(NAC), SUM(CONTRAV), SUM(PAD)From FN_POSVENDAS_ResumoVendaArmz ('" + dtpDataInicial.Text + "', '" + dtpDataFinal.Text + "'), ASLOJA With (NoLock) Where CODLOJA is not null  ";



            if (radioButton1.Checked)
                sql1 = sql1 + " AND CODLOJA=CODLOJA  ";
            else
            {


                if (radioButton2.Checked == true && cmbLoja.Text != "" && cmbMercadoria.Text != "")
                    sql1 = sql1 + " AND CODLOJA=  '" + codLoja + "'  AND CODARMZ =  '" + codArmz + "'  ";


                else if (radioButton2.Checked == true && cmbLoja.Text != "")
                {

                    sql1 = sql1 + " AND CODLOJA=  '" + codLoja + "'    ";
                }

            }


            sql1 = sql1 + " Group By NOMELOJA, CODARMZ, NOMEARZ, DOC Order By NOMELOJA, DOC, NOMEARZ"; // geral 

            tamanho = i = 0;

            try
            {
                SqlCommand cmd2 = new SqlCommand(sql1, conexao);

                conexao.Open();

                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tamanho = tamanho + 1;
                    }
                }
            }


            catch (Exception) { }

            finally { conexao.Close(); }
            //  int i = 0;
            Armazem = new string[tamanho]; NomeLoja = new string[tamanho]; Valor = new string[tamanho]; Contravalor = new string[tamanho]; Padrao = new string[tamanho]; CodArmazem = new string[tamanho]; Doc = new string[tamanho]; qtd = new string[tamanho];

            try
            {

                SqlCommand cmd = new SqlCommand(sql1, conexao);

                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Armazem[i] = reader[2].ToString();
                        NomeLoja[i] = reader[0].ToString();
                        Valor[i] = reader[5].ToString();
                        Contravalor[i] = reader[6].ToString();
                        Padrao[i] = reader[7].ToString();
                        CodArmazem[i] = reader[1].ToString();

                        Doc[i] = reader[3].ToString();
                        qtd[i] = reader[4].ToString();
                        i++;
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }



        }



        

        // BOTAO PARA APLICAR OS FILTROS E OS PARAMENTROS DO RELATORIO


       

       


        

       

      

       
        private void btPaarametro_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
        }

        private void btMinimisar_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
        }

        private void btAplicar_Click(object sender, EventArgs e)
        {
            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[3];


            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);


            reportViewer1.LocalReport.SetParameters(p);

            var contatos = AcumPorDocAz.Recibo(Armazem, NomeLoja, Valor, Contravalor, Padrao, CodArmazem, Doc, qtd, tamanho);




            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", contatos));


            // imprimirRelatorio(); 
            //ImprimirRelatorio2();
            // ImprimirRelatorio3();

            // ------------------- FUNCAO IMPRIMIR
           

            sql1 = @"SELECT NOMELOJA, CODARMZ, NOMEARZ,SUM(QTD), SUM(NAC), SUM(CONTRAV), SUM(PAD)From FN_POSVENDAS_ResumoVendaArmz ('" + dtpDataInicial.Text + "', '" + dtpDataFinal.Text + "'), ASLOJA With (NoLock) Where CODLOJA is not null  ";



            if (radioButton1.Checked)
                sql1 = sql1 + " AND CODLOJA=CODLOJA  ";
            else
            {


                if (radioButton2.Checked == true && cmbLoja.Text != "" && cmbMercadoria.Text != "")
                    sql1 = sql1 + " AND CODLOJA=  '" + codLoja + "'  AND CODARMZ =  '" + codArmz + "'  ";


                else if (radioButton2.Checked == true && cmbLoja.Text != "")
                {

                    sql1 = sql1 + " AND CODLOJA=  '" + codLoja + "'    ";
                }

            }


            sql1 = sql1 + " Group By NOMELOJA, CODARMZ, NOMEARZ, DOC Order By  NOMEARZ"; // geral 

            tamanho = i = 0;

            try
            {
                SqlCommand cmd2 = new SqlCommand(sql1, conexao);

                conexao.Open();

                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tamanho = tamanho + 1;
                    }
                }
            }


            catch (Exception) { }

            finally { conexao.Close(); }
            //  int i = 0;
            Armazem = new string[tamanho]; NomeLoja = new string[tamanho]; Valor = new string[tamanho]; Contravalor = new string[tamanho]; Padrao = new string[tamanho]; CodArmazem = new string[tamanho]; Doc = new string[tamanho]; qtd = new string[tamanho];

            try
            {

                SqlCommand cmd = new SqlCommand(sql1, conexao);

                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Armazem[i] = reader[2].ToString();
                        NomeLoja[i] = reader[0].ToString();
                        Valor[i] = reader[4].ToString();
                        Contravalor[i] = reader[5].ToString();
                        Padrao[i] = reader[6].ToString();
                        CodArmazem[i] = reader[1].ToString();
                        qtd[i] = reader[3].ToString();
                        i++;
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }

            this.reportViewer1.RefreshReport();
        }

        private void cmbLoja_TextChanged(object sender, EventArgs e)
        {
            // PESQUISA  O CODLOJA 
            SqlCommand cmmd = new SqlCommand(@"SELECT CodLoja from ASLOJA WITH (NOLOCK) where NomeLoja='" + cmbLoja.Text + "'");
            // SqlConnection Conn = new SqlConnection(Variavel.Conn);
            cmmd.Connection = conexao;

            try
            {

                conexao.Open();
                cmmd.Connection = conexao;
                SqlDataReader leitor = cmmd.ExecuteReader();
                while (leitor.Read())
                {

                    codLoja = leitor[0].ToString();

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

            MessageBox.Show(codLoja);
            cmbMercadoria.Text = "";
            cmbMercadoria.Items.Clear(); // LIMPAR A COMBO ANTES DE MOSTRAR OS RESULTADOS 



            SqlCommand cmdd = new SqlCommand(@" Select Distinct RTRIM(LTRIM(NomeArz))from ASARMAZ WITH (NOLOCK) where CodLoja='" + codLoja + "'");

            cmdd.Connection = conexao;


            try
            {
                cmdd.Connection = conexao;
                //cmdd .ExecuteNonQuery();
                conexao.Open();
                SqlDataReader leitor = cmdd.ExecuteReader();
                while (leitor.Read())
                {

                    cmbMercadoria.Items.Add(leitor.GetValue(0));


                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

        }

        private void cmbMercadoria_TextChanged(object sender, EventArgs e)
        {
            SqlCommand cmmd = new SqlCommand(@"select CodArmz from ASARMAZ WITH (NOLOCK) where NomeArz='" + cmbMercadoria.Text + "' AND CODLOJA='" + codLoja + "'");
            // SqlConnection Conn = new SqlConnection(Variavel.Conn);
            cmmd.Connection = conexao;

            try
            {

                conexao.Open();
                cmmd.Connection = conexao;
                SqlDataReader leitor = cmmd.ExecuteReader();
                while (leitor.Read())
                {

                    codArmz = leitor[0].ToString();

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

            MessageBox.Show(codArmz);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            cmbMercadoria.Enabled = false;
            cmbLoja.Enabled = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            cmbMercadoria.Enabled = true;
            cmbLoja.Enabled = true;
        }

        





    }
}
