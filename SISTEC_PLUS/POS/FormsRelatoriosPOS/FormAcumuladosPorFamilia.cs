﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using Microsoft.Reporting.WinForms;


namespace SISTEC_PLUS.POS.FormsRelatoriosPOS
{
    public partial class FormAcumuladosPorFamilia : MetroFramework.Forms.MetroForm
    {

        public string padrao;
        string[] Valor, Contravalor, Padrao, Descricao, familia, qtd, PrecoUni, unidade;
        string operador, sql1, sql2, sql3;


        int tamanho, i;
        string codLoja;
        string CODLOJA, codArmz, codArmz2;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        //SqlConnection conexao = new SqlConnection(Variavel.Conn);







        public FormAcumuladosPorFamilia()
        {
            InitializeComponent();
        }

        private void FormAcumuladosPorFamilia_Load(object sender, EventArgs e)
        {

           rdSintetico.Checked = true;
            btFamSintetico.Visible = false;
            btFamAnalitico.Visible = false;
          


        
         
            


            dtpDataInicial.Format = DateTimePickerFormat.Custom;

            dtpDataInicial.CustomFormat = "yyyy-MM-dd";

            dtpDataFinal.Format = DateTimePickerFormat.Custom;

            dtpDataFinal.CustomFormat = "yyyy-MM-dd";



            // PESQUISA  A LOJA NO BANCO DE  DADOS
            SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT RTRIM (LTRIM (NOMELOJA)) FROM ASLOJA WITH (NOLOCK)");
            cmd.Connection = conexao;



            try
            {

                conexao.Open();
                cmd.Connection = conexao;
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {

                    cmbLoja.Items.Add(leitor.GetValue(0));

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

           
        }

      

  

        private void btPaarametro_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
        }

        private void btMinimisar_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
        }

        private void cmbLoja_TextChanged(object sender, EventArgs e)
        {
            // PESQUISA  O CODLOJA 
            SqlCommand cmmd = new SqlCommand(@"SELECT CodLoja from ASLOJA WITH (NOLOCK) where NomeLoja='" + cmbLoja.Text + "'");
            
            cmmd.Connection = conexao;

            try
            {

                conexao.Open();
                cmmd.Connection = conexao;
                SqlDataReader leitor = cmmd.ExecuteReader();
                while (leitor.Read())
                {

                    codLoja = leitor[0].ToString();

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

            MessageBox.Show(codLoja);

          // LIMPAR A COMBO ANTES DE MOSTRAR OS RESULTADOS 
            cbFamilia.Text = "";
            cbFamilia.Items.Clear();
            cbFamilia2.Text = "";
            cbFamilia2.Items.Clear();

            SqlCommand cmdd = new SqlCommand(@" SELECT DISTINCT RTRIM( FA.NOMEMERC )FROM ASFAMILIA FA WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), ASFICMOV1 F WITH (NOLOCK) WHERE M.CODLOJA = F.CODLOJA AND M.CODARMZ = F.CODARMZ AND M.REFERENC = F.REFERENC AND M.CODFAM = FA.CODFAM AND F.Codloja ='" + codLoja + "'");

            cmdd.Connection = conexao;


            try
            {
                cmdd.Connection = conexao;
                //cmdd .ExecuteNonQuery();
                conexao.Open();
                SqlDataReader leitor = cmdd.ExecuteReader();
                while (leitor.Read())
                {

                    cbFamilia.Items.Add(leitor.GetValue(0));
                    cbFamilia2.Items.Add(leitor.GetValue(0));

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }



        }


        // CODIGO ARMAZEM 1
        private void cbFamilia_TextChanged(object sender, EventArgs e)
        {
             SqlCommand cmmd = new SqlCommand(@"SELECT RTRIM( CODFAM ) FROM ASFAMILIA WITH (NOLOCK)  WHERE NOMEMERC = '"+ cbFamilia.Text +"'   ");
            
            cmmd.Connection = conexao;

            try
            {
                
                conexao.Open();
                cmmd.Connection = conexao;
                SqlDataReader leitor = cmmd.ExecuteReader();
                while (leitor.Read())
                {

                    codArmz = leitor[0].ToString();

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

            MessageBox.Show(codArmz);
        }


        // CODIGO ARMAZEM 2
        private void cbFamilia2_TextChanged(object sender, EventArgs e)
        {

            SqlCommand cmmd = new SqlCommand(@"SELECT RTRIM( CODFAM ) FROM ASFAMILIA WITH (NOLOCK)  WHERE NOMEMERC = '" + cbFamilia2.Text + "'   ");

            cmmd.Connection = conexao;

            try
            {

                conexao.Open();
                cmmd.Connection = conexao;
                SqlDataReader leitor = cmmd.ExecuteReader();
                while (leitor.Read())
                {

                    codArmz2 = leitor[0].ToString();

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

            MessageBox.Show(codArmz2);



        }

        private void btAplicar_Click(object sender, EventArgs e)
        {


            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[4];
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);

            reportViewer1.LocalReport.SetParameters(p);
            //   reportViewer2.LocalReport.SetParameters(p);
         
            
            if (rdSintetico.Checked)
            {
                btFamSintetico_Click(sender, e);
            }

            if (rdAnalitico.Checked)
            {
                btFamAnalitico_Click(sender, e);
            }



     



       
      
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        // FUNCAO QUE FAZ A CONSULTA SE O REPORT FOR ANALITICO
        private void btFamAnalitico_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (cbPadrao.Checked)
            { padrao = "USD"; }
            else padrao = "KZ";


            // TODO: This line of code loads data into the 'FamiliaAnalitico.VW_POSVENDAS_ResumoVendaArt' table. You can move, or remove it, as needed.
            this.VW_POSVENDAS_ResumoVendaArtTableAdapter.Fill(this.FamiliaAnalitico.VW_POSVENDAS_ResumoVendaArt, codLoja, codArmz,padrao, codArmz2, dtpDataInicial.Text, dtpDataFinal.Text);
            this.reportViewer1.RefreshReport();

            reportViewer1.Visible = true;

            reportViewer2.Visible = false;
        }

        // FUNCAO QUE FAZ A CONSULTA SE O REPORT FOR SINTETICO
        private void btFamSintetico_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[4];
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);

          //  reportViewer1.LocalReport.SetParameters(p);
            reportViewer2.LocalReport.SetParameters(p);
            
            
            
            this.VW_POSVENDAS_ResumoVendaFamTableAdapter.Fill(this.FamiliaSintetico.VW_POSVENDAS_ResumoVendaFam, codLoja, codArmz, codArmz2, dtpDataInicial.Text, dtpDataFinal.Text);
            this.reportViewer2.RefreshReport();

            reportViewer1.Visible = false;

            reportViewer2.Visible = true;
        }

        private void dtpDataInicial_ValueChanged(object sender, EventArgs e)
        {
            dtpDataInicial.Format = DateTimePickerFormat.Custom;
            dtpDataInicial.CustomFormat = ("yyyy-MM-dd");
            dtpDataFinal.Format = DateTimePickerFormat.Custom;
            dtpDataFinal.CustomFormat = ("yyyy-MM-dd");

            DateTime de = dtpDataInicial.Value.Date;
            DateTime hoje = DateTime.Now.Date;

            int result = DateTime.Compare(hoje, de);


            if (result < 0)
            {
                MessageBox.Show("A data não pode ser menor que hoje.", "Data");
                dtpDataInicial.Text = hoje.ToShortDateString();
                de = dtpDataInicial.Value.Date;

            }

            DateTime a = dtpDataFinal.Value.Date;

            result = DateTime.Compare(a, de);


            if (result < 0)
            {
                MessageBox.Show("Alterou a data de A.", "Data");
                dtpDataFinal.Text = dtpDataInicial.Value.ToShortDateString();

            }



        }

        private void dtpDataFinal_ValueChanged(object sender, EventArgs e)
        {
            dtpDataInicial.Format = DateTimePickerFormat.Custom;
            dtpDataInicial.CustomFormat = ("yyyy-MM-dd");
            dtpDataFinal.Format = DateTimePickerFormat.Custom;
            dtpDataFinal.CustomFormat = ("yyyy-MM-dd");

            DateTime de = dtpDataInicial.Value.Date;
            DateTime a = dtpDataFinal.Value.Date;

            DateTime hoje = DateTime.Now.Date;

            int result = DateTime.Compare(hoje, a);


            if (result < 0)
            {
                MessageBox.Show("A data não pode ser maior que Hoje.", "Data");
                dtpDataFinal.Text = hoje.ToShortDateString();

            }
            else
            {

                result = DateTime.Compare(a, de);


                if (result < 0)
                {
                    MessageBox.Show("A data não pode ser menor.", "Data");
                    dtpDataFinal.Text = dtpDataInicial.Value.ToShortDateString();

                }
            }
        }




        }
   
    
    
    
    
    }

