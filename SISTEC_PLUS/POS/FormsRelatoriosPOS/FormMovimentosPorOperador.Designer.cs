﻿namespace SISTEC_PLUS.POS.FormsRelatoriosPOS
{
    partial class FormMovimentosPorOperador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdQuantidade = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbOperador = new System.Windows.Forms.ComboBox();
            this.rdParcial = new System.Windows.Forms.RadioButton();
            this.rdGeral = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.grpdata = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpDataInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpDataFinal = new System.Windows.Forms.DateTimePicker();
            this.btMinimisar = new System.Windows.Forms.Button();
            this.btMinimizar = new System.Windows.Forms.Button();
            this.btAplicar = new System.Windows.Forms.Button();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.btPaarametro = new System.Windows.Forms.Button();
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.DataTable2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MovimentosPorOperador = new SISTEC_PLUS.POS.FormsRelatoriosPOS.MovimentosPorOperador();
            this.DataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataTable1TableAdapter = new SISTEC_PLUS.POS.FormsRelatoriosPOS.MovimentosPorOperadorTableAdapters.DataTable1TableAdapter();
            this.DataTable2TableAdapter = new SISTEC_PLUS.POS.FormsRelatoriosPOS.MovimentosPorOperadorTableAdapters.DataTable2TableAdapter();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpdata.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable2BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MovimentosPorOperador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.grpdata);
            this.panel1.Controls.Add(this.btMinimisar);
            this.panel1.Controls.Add(this.btMinimizar);
            this.panel1.Controls.Add(this.btAplicar);
            this.panel1.Location = new System.Drawing.Point(-192, 689);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1853, 243);
            this.panel1.TabIndex = 20;
            this.panel1.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1570, 72);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 147;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1570, 44);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 146;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.groupBox2.Controls.Add(this.rdQuantidade);
            this.groupBox2.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox2.Location = new System.Drawing.Point(873, 39);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(338, 115);
            this.groupBox2.TabIndex = 41;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opções de impressão e ordenação";
            // 
            // rdQuantidade
            // 
            this.rdQuantidade.AutoSize = true;
            this.rdQuantidade.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdQuantidade.ForeColor = System.Drawing.Color.White;
            this.rdQuantidade.Location = new System.Drawing.Point(92, 33);
            this.rdQuantidade.Name = "rdQuantidade";
            this.rdQuantidade.Size = new System.Drawing.Size(155, 22);
            this.rdQuantidade.TabIndex = 0;
            this.rdQuantidade.Text = "Contas Contablísticas";
            this.rdQuantidade.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cmbOperador);
            this.groupBox1.Controls.Add(this.rdParcial);
            this.groupBox1.Controls.Add(this.rdGeral);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cmbLoja);
            this.groupBox1.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox1.Location = new System.Drawing.Point(296, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(571, 115);
            this.groupBox1.TabIndex = 145;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo de Listagem";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(115, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 145;
            this.label1.Text = "OPERADOR";
            // 
            // cmbOperador
            // 
            this.cmbOperador.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbOperador.FormattingEnabled = true;
            this.cmbOperador.IntegralHeight = false;
            this.cmbOperador.Location = new System.Drawing.Point(195, 60);
            this.cmbOperador.Name = "cmbOperador";
            this.cmbOperador.Size = new System.Drawing.Size(354, 24);
            this.cmbOperador.TabIndex = 144;
            this.cmbOperador.TextChanged += new System.EventHandler(this.cmbOperador_TextChanged);
            // 
            // rdParcial
            // 
            this.rdParcial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdParcial.AutoSize = true;
            this.rdParcial.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdParcial.Location = new System.Drawing.Point(31, 61);
            this.rdParcial.Name = "rdParcial";
            this.rdParcial.Size = new System.Drawing.Size(63, 20);
            this.rdParcial.TabIndex = 143;
            this.rdParcial.TabStop = true;
            this.rdParcial.Text = "Parcial";
            this.rdParcial.UseVisualStyleBackColor = true;
            this.rdParcial.CheckedChanged += new System.EventHandler(this.rdParcial_CheckedChanged);
            // 
            // rdGeral
            // 
            this.rdGeral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdGeral.AutoSize = true;
            this.rdGeral.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdGeral.Location = new System.Drawing.Point(31, 31);
            this.rdGeral.Name = "rdGeral";
            this.rdGeral.Size = new System.Drawing.Size(56, 20);
            this.rdGeral.TabIndex = 142;
            this.rdGeral.TabStop = true;
            this.rdGeral.Text = "Geral";
            this.rdGeral.UseVisualStyleBackColor = true;
            this.rdGeral.CheckedChanged += new System.EventHandler(this.rdGeral_CheckedChanged);
            
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(152, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "LOJA";
            // 
            // cmbLoja
            // 
            this.cmbLoja.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.IntegralHeight = false;
            this.cmbLoja.Location = new System.Drawing.Point(195, 27);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(354, 24);
            this.cmbLoja.TabIndex = 13;
            this.cmbLoja.TextChanged += new System.EventHandler(this.cmbLoja_TextChanged);
            // 
            // grpdata
            // 
            this.grpdata.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpdata.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.grpdata.Controls.Add(this.label8);
            this.grpdata.Controls.Add(this.label9);
            this.grpdata.Controls.Add(this.dtpDataInicial);
            this.grpdata.Controls.Add(this.dtpDataFinal);
            this.grpdata.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpdata.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpdata.Location = new System.Drawing.Point(1217, 39);
            this.grpdata.Name = "grpdata";
            this.grpdata.Size = new System.Drawing.Size(347, 115);
            this.grpdata.TabIndex = 40;
            this.grpdata.TabStop = false;
            this.grpdata.Text = "Data";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(50, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "A";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(50, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "De";
            // 
            // dtpDataInicial
            // 
            this.dtpDataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataInicial.Location = new System.Drawing.Point(79, 29);
            this.dtpDataInicial.Name = "dtpDataInicial";
            this.dtpDataInicial.Size = new System.Drawing.Size(167, 21);
            this.dtpDataInicial.TabIndex = 16;
            // 
            // dtpDataFinal
            // 
            this.dtpDataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataFinal.Location = new System.Drawing.Point(79, 63);
            this.dtpDataFinal.Name = "dtpDataFinal";
            this.dtpDataFinal.Size = new System.Drawing.Size(167, 21);
            this.dtpDataFinal.TabIndex = 17;
            // 
            // btMinimisar
            // 
            this.btMinimisar.Location = new System.Drawing.Point(215, 39);
            this.btMinimisar.Name = "btMinimisar";
            this.btMinimisar.Size = new System.Drawing.Size(75, 28);
            this.btMinimisar.TabIndex = 13;
            this.btMinimisar.Text = "Minimizar";
            this.btMinimisar.UseVisualStyleBackColor = true;
            this.btMinimisar.Click += new System.EventHandler(this.btMinimisar_Click);
            // 
            // btMinimizar
            // 
            this.btMinimizar.Location = new System.Drawing.Point(35, 39);
            this.btMinimizar.Name = "btMinimizar";
            this.btMinimizar.Size = new System.Drawing.Size(75, 28);
            this.btMinimizar.TabIndex = 6;
            this.btMinimizar.Text = "Minimizar";
            this.btMinimizar.UseVisualStyleBackColor = true;
            // 
            // btAplicar
            // 
            this.btAplicar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btAplicar.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btAplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAplicar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAplicar.ForeColor = System.Drawing.Color.White;
            this.btAplicar.Location = new System.Drawing.Point(296, 165);
            this.btAplicar.Name = "btAplicar";
            this.btAplicar.Size = new System.Drawing.Size(77, 35);
            this.btAplicar.TabIndex = 1;
            this.btAplicar.Text = "Aplicar";
            this.btAplicar.UseVisualStyleBackColor = false;
            this.btAplicar.Click += new System.EventHandler(this.btAplicar_Click);
            // 
            // reportViewer1
            // 
            this.reportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "movimentoOperador";
            reportDataSource1.Value = this.DataTable1BindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "SISTEC_PLUS.POS.FormsRelatoriosPOS.MovimentosPorOperador.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(104, 75);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(1268, 608);
            this.reportViewer1.TabIndex = 25;
            // 
            // btPaarametro
            // 
            this.btPaarametro.Location = new System.Drawing.Point(23, 75);
            this.btPaarametro.Name = "btPaarametro";
            this.btPaarametro.Size = new System.Drawing.Size(75, 27);
            this.btPaarametro.TabIndex = 24;
            this.btPaarametro.Text = "Parametros";
            this.btPaarametro.UseVisualStyleBackColor = true;
            this.btPaarametro.Click += new System.EventHandler(this.btPaarametro_Click);
            // 
            // reportViewer2
            // 
            this.reportViewer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource2.Name = "DataSet1";
            reportDataSource2.Value = this.DataTable2BindingSource;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "SISTEC_PLUS.POS.FormsRelatoriosPOS.MovimentosPorOperadorAnulados.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(104, 99);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.Size = new System.Drawing.Size(1268, 608);
            this.reportViewer2.TabIndex = 26;
            // 
            // DataTable2BindingSource
            // 
            this.DataTable2BindingSource.DataMember = "DataTable2";
            this.DataTable2BindingSource.DataSource = this.MovimentosPorOperador;
            // 
            // MovimentosPorOperador
            // 
            this.MovimentosPorOperador.DataSetName = "MovimentosPorOperador";
            this.MovimentosPorOperador.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DataTable1BindingSource
            // 
            this.DataTable1BindingSource.DataMember = "DataTable1";
            this.DataTable1BindingSource.DataSource = this.MovimentosPorOperador;
            // 
            // DataTable1TableAdapter
            // 
            this.DataTable1TableAdapter.ClearBeforeFill = true;
            // 
            // DataTable2TableAdapter
            // 
            this.DataTable2TableAdapter.ClearBeforeFill = true;
            // 
            // FormMovimentosPorOperador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1468, 912);
            this.Controls.Add(this.reportViewer2);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.btPaarametro);
            this.Controls.Add(this.panel1);
            this.Name = "FormMovimentosPorOperador";
            this.Text = "Relátorio de Movimentos por Operador";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormMovimentosPorOperador_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpdata.ResumeLayout(false);
            this.grpdata.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable2BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MovimentosPorOperador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdParcial;
        private System.Windows.Forms.RadioButton rdGeral;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.GroupBox grpdata;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpDataInicial;
        private System.Windows.Forms.DateTimePicker dtpDataFinal;
        private System.Windows.Forms.Button btMinimisar;
        private System.Windows.Forms.Button btMinimizar;
        private System.Windows.Forms.Button btAplicar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbOperador;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.Button btPaarametro;
        private System.Windows.Forms.BindingSource DataTable1BindingSource;
        private MovimentosPorOperador MovimentosPorOperador;
        private MovimentosPorOperadorTableAdapters.DataTable1TableAdapter DataTable1TableAdapter;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
        private System.Windows.Forms.BindingSource DataTable2BindingSource;
        private MovimentosPorOperadorTableAdapters.DataTable2TableAdapter DataTable2TableAdapter;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox rdQuantidade;
    }
}