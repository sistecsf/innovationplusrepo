﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS.FormsRelatoriosPOS
{
    public partial class FormMovimentoPorArtigo : MetroFramework.Forms.MetroForm
    {

        public string padrao,tipoRelatorio, loja;
        string[] Valor, Contravalor, Padrao, Descricao, familia, qtd, PrecoUni, unidade;
        string operador, sql1, sql2, sql3;


        int tamanho, i;
        string codLoja, parametroLoja;
        string CODLOJA, codArmz, codArmz2;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        //SqlConnection conexao = new SqlConnection(Variavel.Conn);

        public FormMovimentoPorArtigo()
        {
            InitializeComponent();
        }

        private void FormMovimentoPorArtigo_Load(object sender, EventArgs e)
        {
                btRelatGeral.Visible = false ;
                btRelatVendas.Visible = false ;
                btRelatGlFami.Visible = false;
                btRelatVendFam.Visible = false;

            codLoja = Variavel.codLoja;
            MessageBox.Show(codLoja);
            
            rdGeral.Checked = true;
            rdRelatorioGeralArtigo.Checked = true;

            dtpDataInicial.Format = DateTimePickerFormat.Custom;

            dtpDataInicial.CustomFormat = "yyyy-MM-dd";

            dtpDataFinal.Format = DateTimePickerFormat.Custom;

            dtpDataFinal.CustomFormat = "yyyy-MM-dd";

            panel1.Visible = true;

            // PESQUISA  A LOJA NO BANCO DE  DADOS
            SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT RTRIM (LTRIM (NOMELOJA)) FROM ASLOJA WITH (NOLOCK)");
            cmd.Connection = conexao;



            try
            {

                conexao.Open();
                cmd.Connection = conexao;
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {

                    cmbLoja.Items.Add(leitor.GetValue(0));

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }




            
        
        }

    
        private void btPaarametro_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
        }

        private void btMinimisar_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
        }

        //  PRIMEIRA OPCAO DE RELATORIO
        private void btRelatGeral_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            // -----------------CONDIÇÕES PARA OS RELATORIOS GERAIS---------------------------------


            //------------GERAL POR QUANTIDADE
            if (rdGeral.Checked && rdRelatorioGeralArtigo.Checked   && rdQuantidade.Checked )
                {
                    tipoRelatorio = "RESUMO GERAL SAÍDAS POR ARTIGO ORDENADO POR QUANTIDADE ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                    reportViewer1.LocalReport.SetParameters(p);

                    this.RelatorioGeralArtigosDataTable1TableAdapter.FillQuantidade(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, dtpDataInicial.Text, dtpDataFinal.Text);

                this.reportViewer1.RefreshReport();
                MessageBox.Show("DOIS GERAL");
                rdQuantidade.Checked = false;
                 }

             //------------GERAL POR VALOR
            else if (rdGeral.Checked && rdRelatorioGeralArtigo.Checked && rdPorValor.Checked)
                 {
                     tipoRelatorio = "RESUMO GERAL DE SAÍDAS POR ARTIGO ORDENADO POR VALOR";
                     loja = ".";
                     Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                     p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                     p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                     p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                     p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                     p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                     reportViewer1.LocalReport.SetParameters(p);

                     this.RelatorioGeralArtigosDataTable1TableAdapter.FillPorValor(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, dtpDataInicial.Text, dtpDataFinal.Text);

            this.reportViewer1.RefreshReport();
            MessageBox.Show("TRES GERAL");
            rdPorValor.Checked = false;
                 }



             //------------GERAL 
            else if (rdGeral.Checked && rdRelatorioGeralArtigo.Checked)
            {
                tipoRelatorio = "RESUMO GERAL DE SAÍDAS POR ARTIGO";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer1.LocalReport.SetParameters(p);

                this.RelatorioGeralArtigosDataTable1TableAdapter.FillGeral(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, dtpDataInicial.Text, dtpDataFinal.Text);

                this.reportViewer1.RefreshReport();
                MessageBox.Show("UM GERAL");
            }




            // -----------------CONDIÇÕES PARA OS RELATORIOS PARCIAIS--------------------------------


                 //- ---PARCIAL POR QUANTIDADE
            else if (rdParcial.Checked && rdRelatorioGeralArtigo.Checked && rdQuantidade.Checked)

                 {
                     if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {
                    tipoRelatorio = "RESUMO PARCIAL SAÍDAS POR ARTIGO ORDENADO POR QUANTIDADE ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                    reportViewer1.LocalReport.SetParameters(p);

                    this.RelatorioGeralArtigosDataTable1TableAdapter.FillByparcialQuantidade(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
                   MessageBox.Show("DOIS");
                   this.reportViewer1.RefreshReport();
                   rdQuantidade.Checked = false;
                
                 }
                 }

                //-------------PARCIAL POR VALOR
            else if (rdParcial.Checked && rdRelatorioGeralArtigo.Checked && rdPorValor.Checked)
               {
                if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {

                    tipoRelatorio = "RESUMO PARCIAL SAÍDAS POR ARTIGO ORDENADO POR VALOR ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);
                    reportViewer1.LocalReport.SetParameters(p);
                    this.RelatorioGeralArtigosDataTable1TableAdapter.FillByparcialPorValor(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
                MessageBox.Show("TRES");
                this.reportViewer1.RefreshReport();
                rdPorValor.Checked = false;
                 }
               }

             //-----PARCIAL POR VALOR GERAL 
            else if (rdParcial.Checked && rdRelatorioGeralArtigo.Checked)
            {
                if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {

                    tipoRelatorio = "RESUMO PARCIAL GERAL SAÍDAS POR ARTIGO  ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);
                    reportViewer1.LocalReport.SetParameters(p);
                    this.RelatorioGeralArtigosDataTable1TableAdapter.FillparcialGeral(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
                MessageBox.Show("UM");
                this.reportViewer1.RefreshReport();

                }
            }

        //    else MessageBox.Show("POR FAVOR, SELECIONE UM TIPO DE RELATIRO");

            reportViewer1.Visible = true;

            reportViewer2.Visible = false;
            reportViewer3.Visible = false;
            reportViewer4.Visible = false;
        }


        // SEGUNDA OPCAO DE RELATORIO
        private void btRelatVendas_Click(object sender, EventArgs e)
        {
            // -----------------CONDIÇÕES PARA OS RELATORIOS GERAIS---------------------------------

            Cursor.Current = Cursors.WaitCursor;
            //------------GERAL POR QUANTIDADE
            if (rdGeral.Checked && rdRelatVendasArtigo.Checked && rdQuantidade.Checked)
            {
                
                tipoRelatorio = "RESUMO DE VENDAS POR ARTIGO ORDENADO POR QUANTIDADE ";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer2.LocalReport.SetParameters(p);
             
                this.RelatorioGeralArtigosDataTable1TableAdapter.FillQuantidade(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, dtpDataInicial.Text, dtpDataFinal.Text);

                this.reportViewer2.RefreshReport();
                MessageBox.Show("DOIS GERAL");
                rdQuantidade.Checked = false;
            }

             //------------GERAL POR VALOR
            else if (rdGeral.Checked && rdRelatVendasArtigo.Checked && rdPorValor.Checked)
            {
                tipoRelatorio = "RESUMO DE VENDAS  POR ARTIGO ORDENADO POR VALOR";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer2.LocalReport.SetParameters(p);

                this.RelatorioGeralArtigosDataTable1TableAdapter.FillPorValor(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, dtpDataInicial.Text, dtpDataFinal.Text);

                this.reportViewer2.RefreshReport();
                MessageBox.Show("TRES GERAL");
                rdPorValor.Checked = false;
            }



             //------------GERAL 
            else if (rdGeral.Checked && rdRelatVendasArtigo.Checked)
            {
                tipoRelatorio = "RESUMO DE VENDAS POR ARTIGO";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer2.LocalReport.SetParameters(p);

                this.RelatorioGeralArtigosDataTable1TableAdapter.FillGeral(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, dtpDataInicial.Text, dtpDataFinal.Text);

                this.reportViewer2.RefreshReport();
                MessageBox.Show("UM GERAL");
            }




            // -----------------CONDIÇÕES PARA OS RELATORIOS PARCIAIS--------------------------------


                 //- ---PARCIAL POR QUANTIDADE
            else if (rdParcial.Checked && rdRelatVendasArtigo.Checked && rdQuantidade.Checked)
            {
                if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {
                    tipoRelatorio = "RESUMO PARCIAL DE VENDAS POR ARTIGO ORDENADO POR QUANTIDADE ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                    reportViewer2.LocalReport.SetParameters(p);

                    this.RelatorioGeralArtigosDataTable1TableAdapter.FillByparcialQuantidade(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
                    MessageBox.Show("DOIS");
                    this.reportViewer2.RefreshReport();
                    rdQuantidade.Checked = false;

                }
            }

                //-------------PARCIAL POR VALOR
            else if (rdParcial.Checked && rdRelatVendasArtigo.Checked && rdPorValor.Checked)
            {
                if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {

                    tipoRelatorio = "RESUMO PARCIAL DE VENDAS POR ARTIGO ORDENADO POR VALOR ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);
                    reportViewer2.LocalReport.SetParameters(p);
                    this.RelatorioGeralArtigosDataTable1TableAdapter.FillByparcialPorValor(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
                    MessageBox.Show("TRES");
                    this.reportViewer2.RefreshReport();
                    rdPorValor.Checked = false;
                }
            }

             //-----PARCIAL POR VALOR GERAL 
            else if (rdParcial.Checked && rdRelatVendasArtigo.Checked)
            {
                if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {

                    tipoRelatorio = "RESUMO PARCIAL DE VENDAS  POR ARTIGO  ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);
                    reportViewer2.LocalReport.SetParameters(p);
                    this.RelatorioGeralArtigosDataTable1TableAdapter.FillparcialGeral(this.RelatorioGeralArtigos.RelatorioGeralArtigosDataTable1, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
                    MessageBox.Show("UM");
                    this.reportViewer2.RefreshReport();

                }
            }

            //    else MessageBox.Show("POR FAVOR, SELECIONE UM TIPO DE RELATIRO");


            reportViewer1.Visible = false;

            reportViewer2.Visible = true;

            reportViewer3.Visible = false;
            reportViewer4.Visible = false;
        }

        //  TERCEIRA OPCAO DE RELATORIO
        private void btRelatGlFami_Click(object sender, EventArgs e)
        {
         

            // -----------------CONDIÇÕES PARA OS RELATORIOS GERAIS---------------------------------

            Cursor.Current = Cursors.WaitCursor;
            //------------GERAL POR QUANTIDADE
            if (rdGeral.Checked && rdRelatGeralFamilia.Checked && rdQuantidade.Checked)
            {

                tipoRelatorio = "RESUMO GERAL SAIDAS ARTIGO POR FAMILIA ORDENADO POR QUANTIDADE ";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer3.LocalReport.SetParameters(p);

                this.GeraldeSaídasPorFamíliaTableAdapter.FillByGeralPorFamiliaQuantidade(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
               

                this.reportViewer3.RefreshReport();
                MessageBox.Show("DOIS GERAL");
                rdQuantidade.Checked = false;
            }

             //------------GERAL POR VALOR
            else if (rdGeral.Checked && rdRelatGeralFamilia.Checked && rdPorValor.Checked)
            {
                tipoRelatorio = "RESUMO GERAL SAIDAS ARTIGO POR FAMILIA ORDENADO  POR VALOR";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer3.LocalReport.SetParameters(p);

                this.GeraldeSaídasPorFamíliaTableAdapter.FillByGeralPorFamiliaPorValor(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
                this.reportViewer3.RefreshReport();
                MessageBox.Show("TRES GERAL");
                rdPorValor.Checked = false;
            }



             //------------GERAL 
            else if (rdGeral.Checked && rdRelatGeralFamilia.Checked)
            {
                tipoRelatorio = "RESUMO GERAL DE SAIDAS ARTIGO POR FAMILIA ";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer3.LocalReport.SetParameters(p);
                this.GeraldeSaídasPorFamíliaTableAdapter.FillGeralSaidasPorFamilia(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
               

                this.reportViewer3.RefreshReport();
                MessageBox.Show("UM GERAL");
            }




            // -----------------CONDIÇÕES PARA OS RELATORIOS PARCIAIS--------------------------------


                 //- ---PARCIAL POR QUANTIDADE
            else if (rdParcial.Checked && rdRelatGeralFamilia.Checked && rdQuantidade.Checked)
            {
                if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {
                    tipoRelatorio = "RESUMO PARCIAL DE SAIDAS DE ARTIGO POR FAMILIA ORDENADO POR QUANTIDADE ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                    reportViewer3.LocalReport.SetParameters(p);

                    this.GeraldeSaídasPorFamíliaTableAdapter.FillByGeralPorFamiliaQuantidade(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
                    
                    MessageBox.Show("DOIS");
                    this.reportViewer3.RefreshReport();
                    rdQuantidade.Checked = false;

                }
            }

                //-------------PARCIAL POR VALOR
            else if (rdParcial.Checked && rdRelatGeralFamilia.Checked && rdPorValor.Checked)
            {
                if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {

                    tipoRelatorio = "RESUMO PARCIAL DE SAIDAS  ARTIGO POR FAMILIA ORDENADO  POR VALOR ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);
                    reportViewer3.LocalReport.SetParameters(p);

                    this.GeraldeSaídasPorFamíliaTableAdapter.FillByGeralPorFamiliaPorValor(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
                   
                    MessageBox.Show("TRES");
                    this.reportViewer3.RefreshReport();
                    rdPorValor.Checked = false;
                }
            }

             //-----PARCIAL POR VALOR GERAL 
            else if (rdParcial.Checked && rdRelatGeralFamilia.Checked)
            {
                if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {

                    tipoRelatorio = "RESUMO PARCIAL DE SAIDAS DE ARTIGO POR FAMILIA ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);
                    reportViewer3.LocalReport.SetParameters(p);

                    this.GeraldeSaídasPorFamíliaTableAdapter.FillGeralSaidasPorFamilia(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
    
                    
                    MessageBox.Show("UM");
                    this.reportViewer3.RefreshReport();

                }
            }

            //    else MessageBox.Show("POR FAVOR, SELECIONE UM TIPO DE RELATIRO");


            reportViewer1.Visible = false;

            reportViewer2.Visible = false;

            reportViewer3.Visible = true;
            reportViewer4.Visible = false;








        }


        //  QUARTA OPCAO DE RELATORIO
        private void btRelatVendFam_Click(object sender, EventArgs e)
        {

            // -----------------CONDIÇÕES PARA OS RELATORIOS GERAIS---------------------------------

            Cursor.Current = Cursors.WaitCursor;
            //------------GERAL POR QUANTIDADE
            if (rdGeral.Checked && rdRelatVendArtFamilia.Checked && rdQuantidade.Checked)
            {

                tipoRelatorio = "RESUMO GERAL VENDAS ARTIGO POR FAMILIA ORDENADO POR QUANTIDADE ";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer4.LocalReport.SetParameters(p);

                this.GeraldeSaídasPorFamíliaTableAdapter.FillByVendaPorFamiliaQuantidade(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);


                this.reportViewer4.RefreshReport();
                MessageBox.Show("DOIS GERAL");
                rdQuantidade.Checked = false;
            }

             //------------GERAL POR VALOR
            else if (rdGeral.Checked && rdRelatVendArtFamilia.Checked && rdPorValor.Checked)
            {
                tipoRelatorio = "RESUMO GERAL DE VENDAS  ARTIGO POR FAMILIA ORDENADO  POR VALOR";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer4.LocalReport.SetParameters(p);

                this.GeraldeSaídasPorFamíliaTableAdapter.FillByVendasPorFamiliaPorValor(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);
                this.reportViewer4.RefreshReport();
                MessageBox.Show("TRES GERAL");
                rdPorValor.Checked = false;
            }



             //------------GERAL 
            else if (rdGeral.Checked && rdRelatVendArtFamilia.Checked)
            {
                tipoRelatorio = "RESUMO GERAL DE VENDAS DE ARTIGO POR FAMILIA ";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer4.LocalReport.SetParameters(p);
                this.GeraldeSaídasPorFamíliaTableAdapter.FillByVendaPorFamiliaGeral(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);


                this.reportViewer4.RefreshReport();
                MessageBox.Show("UM GERAL");
            }




            // -----------------CONDIÇÕES PARA OS RELATORIOS PARCIAIS--------------------------------


                 //- ---PARCIAL POR QUANTIDADE
            else if (rdParcial.Checked && rdRelatVendArtFamilia.Checked && rdQuantidade.Checked)
            {
                if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {
                    tipoRelatorio = "RESUMO PARCIAL DE VENDAS DE ARTIGO POR FAMILIA ORDENADO POR QUANTIDADE ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                    reportViewer4.LocalReport.SetParameters(p);

                    this.GeraldeSaídasPorFamíliaTableAdapter.FillByVendaPorFamiliaQuantidade(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);

                    MessageBox.Show("DOIS");
                    this.reportViewer4.RefreshReport();
                    rdQuantidade.Checked = false;

                }
            }

                //-------------PARCIAL POR VALOR
            else if (rdParcial.Checked && rdRelatVendArtFamilia.Checked && rdPorValor.Checked)
            {
                if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {

                    tipoRelatorio = "RESUMO PARCIAL DE SAIDAS DE ARTIGO POR FAMILIA ORDENADO  POR VALOR ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);
                    reportViewer4.LocalReport.SetParameters(p);

                    this.GeraldeSaídasPorFamíliaTableAdapter.FillByVendasPorFamiliaPorValor(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);

                    MessageBox.Show("TRES");
                    this.reportViewer4.RefreshReport();
                    rdPorValor.Checked = false;
                }
            }

             //-----PARCIAL POR VALOR GERAL 
            else if (rdParcial.Checked && rdRelatVendArtFamilia.Checked)
            {
                if (cmbLoja.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER A LOJA");
                else
                {

                    tipoRelatorio = "RESUMO PARCIAL DE VENDAS  DE ARTIGO POR FAMILIA ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);
                    reportViewer4.LocalReport.SetParameters(p);

                    this.GeraldeSaídasPorFamíliaTableAdapter.FillByVendaPorFamiliaGeral(this.RelatorioGeralArtigosFamilia.GeraldeSaídasPorFamília, codLoja, dtpDataInicial.Text, dtpDataFinal.Text);


                    MessageBox.Show("UM");
                    this.reportViewer4.RefreshReport();

                }
            }

            //    else MessageBox.Show("POR FAVOR, SELECIONE UM TIPO DE RELATIRO");


            reportViewer1.Visible = false;

            reportViewer2.Visible = false;

            reportViewer3.Visible = true;
            reportViewer4.Visible = true;



        }


                private void rdGeral_CheckedChanged(object sender, EventArgs e)
        {
            cmbLoja.Enabled = false;
        }

        private void rdParcial_CheckedChanged(object sender, EventArgs e)
        {
            cmbLoja.Enabled = true;
            
        }

        private void cmbLoja_TextChanged(object sender, EventArgs e)
        {
            SqlCommand cmmd = new SqlCommand(@"SELECT CodLoja from ASLOJA WITH (NOLOCK) where NomeLoja='" + cmbLoja.Text + "'");

            cmmd.Connection = conexao;

            try
            {

                conexao.Open();
                cmmd.Connection = conexao;
                SqlDataReader leitor = cmmd.ExecuteReader();
                while (leitor.Read())
                {

                    codLoja = leitor[0].ToString();

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

            MessageBox.Show(codLoja);
        }

        private void btAplicar_Click(object sender, EventArgs e)
        {


            // CONDICAO PARA CHAMAR O PRIMEIRO RELATORIO
            if (rdGeral.Checked && rdRelatorioGeralArtigo.Checked && rdQuantidade.Checked || rdGeral.Checked && rdRelatorioGeralArtigo.Checked && rdPorValor.Checked || rdGeral.Checked && rdRelatorioGeralArtigo.Checked || rdParcial.Checked && rdRelatorioGeralArtigo.Checked && rdQuantidade.Checked || rdParcial.Checked && rdRelatorioGeralArtigo.Checked && rdPorValor.Checked || rdParcial.Checked && rdRelatorioGeralArtigo.Checked)
            {
               btRelatGeral_Click( sender,  e);
            }

            // CONDICAO PARA CHAMAR O SEGUNDO RELATORIO
            if (rdGeral.Checked && rdRelatVendasArtigo.Checked && rdQuantidade.Checked || rdGeral.Checked && rdRelatVendasArtigo.Checked && rdPorValor.Checked || rdGeral.Checked && rdRelatVendasArtigo.Checked || rdParcial.Checked && rdRelatVendasArtigo.Checked && rdQuantidade.Checked || rdParcial.Checked && rdRelatVendasArtigo.Checked && rdPorValor.Checked || rdParcial.Checked && rdRelatVendasArtigo.Checked)
            {
                btRelatVendas_Click(sender, e);
            }


             // CONDICAO PARA CHAMAR O TERCEIRO RELATORIO
            if (rdGeral.Checked && rdRelatGeralFamilia.Checked && rdQuantidade.Checked || rdGeral.Checked && rdRelatGeralFamilia.Checked && rdPorValor.Checked || rdGeral.Checked && rdRelatGeralFamilia.Checked || rdParcial.Checked && rdRelatGeralFamilia.Checked && rdQuantidade.Checked || rdParcial.Checked && rdRelatGeralFamilia.Checked && rdPorValor.Checked || rdParcial.Checked && rdRelatGeralFamilia.Checked)
            {
                btRelatGlFami_Click(sender, e);
            }

            // CONDICAO PARA CHAMAR O QUARTO RELATORIO
            if (rdGeral.Checked && rdRelatVendArtFamilia.Checked && rdQuantidade.Checked || rdGeral.Checked && rdRelatVendArtFamilia.Checked && rdPorValor.Checked || rdGeral.Checked && rdRelatVendArtFamilia.Checked || rdParcial.Checked && rdRelatVendArtFamilia.Checked && rdQuantidade.Checked || rdParcial.Checked && rdRelatVendArtFamilia.Checked && rdPorValor.Checked || rdParcial.Checked && rdRelatVendArtFamilia.Checked)
            {
                btRelatVendFam_Click(sender, e);
            }



        }

        private void rdGeral_VisibleChanged(object sender, EventArgs e)
        {
           
        }
    }
}
