﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS.FormsRelatoriosPOS
{
    public partial class FormMovimentosPorOperador : MetroFramework.Forms.MetroForm
    {

        public string padrao, tipoRelatorio, loja, strATCAIXA;
        string[] Valor, Contravalor, Padrao, Descricao, familia, qtd, PrecoUni, unidade;
        string operador, sql1, sql2, sql3;


        int tamanho, i;
        string codLoja, parametroLoja, codUtilizador;
        string CODLOJA, codArmz, codArmz2;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        //SqlConnection conexao = new SqlConnection(Variavel.Conn);
          //dataAdapter.SelectCommand.CommandTimeout = 90000;
        public FormMovimentosPorOperador()
        {
            InitializeComponent();
        }

        private void FormMovimentosPorOperador_Load(object sender, EventArgs e)
        {
            rdGeral.Checked = true;
         

        
            
            
            DateTime Data = DateTime.Now;
            strATCAIXA = "ATCAIXA_" + Data.Year;
            MessageBox.Show (strATCAIXA);
            // PESQUISA  A LOJA NO BANCO DE  DADOS
            SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT RTRIM (LTRIM (NOMELOJA)) FROM ASLOJA WITH (NOLOCK)");
            cmd.Connection = conexao;



            try
            {

                conexao.Open();
                cmd.Connection = conexao;
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {

                    cmbLoja.Items.Add(leitor.GetValue(0));

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }


            codLoja = Variavel.codLoja;
            MessageBox.Show(codLoja);

            rdGeral.Checked = true;
     

            dtpDataInicial.Format = DateTimePickerFormat.Custom;

            dtpDataInicial.CustomFormat = "yyyy-MM-dd";

            dtpDataFinal.Format = DateTimePickerFormat.Custom;

            dtpDataFinal.CustomFormat = "yyyy-MM-dd";
            
            
            
            panel1.Visible = true;
        }




        private void btMinimisar_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
        }

        private void btPaarametro_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
        }

        private void cmbLoja_TextChanged(object sender, EventArgs e)
        {
         
            SqlCommand cmmd = new SqlCommand(@"SELECT CodLoja from ASLOJA WITH (NOLOCK) where NomeLoja='" + cmbLoja.Text + "'");

            cmmd.Connection = conexao;

            try
            {

                conexao.Open();
                cmmd.Connection = conexao;
                SqlDataReader leitor = cmmd.ExecuteReader();
                while (leitor.Read())
                {

                    codLoja = leitor[0].ToString();

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

            MessageBox.Show(codLoja);



            // LIMPAR A COMBO ANTES DE MOSTRAR OS RESULTADOS 
            cmbOperador.Text = "";
            cmbOperador.Items.Clear();



            SqlCommand cmdd = new SqlCommand(@"SELECT DISTINCT RTRIM(U.NOMEUTILIZADOR) FROM UTILIZADORES U WITH (NOLOCK) , ASFICMOV1 F WITH (NOLOCK) WHERE U.CODUTILIZADOR = F.CODUTILIZADOR AND F.CODLOJA = '" + codLoja + "'");

            cmdd.Connection = conexao;


            try
            {
                cmdd.Connection = conexao;
                //cmdd .ExecuteNonQuery();
                conexao.Open();
                SqlDataReader leitor = cmdd.ExecuteReader();
                while (leitor.Read())
                {

                    cmbOperador.Items.Add(leitor.GetValue(0));
                    

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }






        }

        private void cmbOperador_TextChanged(object sender, EventArgs e)
        {
            
          SqlCommand cmm2d = new SqlCommand(@"   SELECT        D.IDUnico, CASE STATUS WHEN 'A' THEN 'ANULADO' END AS STATUS, CAST(D.DataCria AS DateTime) AS Expr1, D.CodDoc, D.PRECO, D.PRECOD, D.NumDoc, U.NomeUtilizador, U.Login, A.DescrDoc, 
                         C.CodConta
FROM            AREGDOC AS D WITH (NOLOCK) INNER JOIN
                         UTILIZADORES AS U WITH (NOLOCK) ON D.CodUtilizador = U.CodUtilizador INNER JOIN
                         ASDOCS AS A WITH (NOLOCK) ON D.CodDoc = A.CodDoc INNER JOIN"+
                         "'" + strATCAIXA + "' " +
"AS C WITH (NOLOCK) ON D.NumDoc = C.NUMDOC AND D.DESCR = C.Descricao AND D.IDUnico = C.IDUNICO AND U.CodUtilizador = C.CodUtilizador"+
"WHERE        (C.DEBITO_D IS NOT NULL) AND (A.TipoPOS = 'S') AND (A.TIPOUTIL = 'S') AND (A.ALTSTOCK = 'S') AND (A.TipoDoc = 'R') AND (LEFT(D.DataCria, 10) >= @DataInicial) AND (LEFT(D.DataCria, 10) <= @DataFinal) AND"+
 "                         (D.CodLoja = @loja) AND (C.CodUtilizador = @utilisador) OR"+
  "                       (A.TipoPOS = 'S') AND (A.TIPOUTIL = 'S') AND (A.ALTSTOCK = 'S') AND (A.TipoDoc = 'R') AND (LEFT(D.DataCria, 10) >= @DataInicial) AND (LEFT(D.DataCria, 10) <= @DataFinal) AND (D.CodLoja = @loja) AND "+
 "                        (C.CodUtilizador = @utilisador) AND (C.DEBITO IS NOT NULL)"+
"ORDER BY U.Login, D.CodDoc, D.NumDoc");



            SqlCommand cmmd = new SqlCommand(@" SELECT DISTINCT RTRIM(U.CODUTILIZADOR)FROM UTILIZADORES U WITH (NOLOCK) WHERE U.NOMEUTILIZADOR ='" + cmbOperador.Text + "'");

            cmmd.Connection = conexao;

            try
            {

                conexao.Open();
                cmmd.Connection = conexao;
                SqlDataReader leitor = cmmd.ExecuteReader();
                while (leitor.Read())
                {

                    codUtilizador= leitor[0].ToString();

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

            MessageBox.Show(codUtilizador);
        }



       
        private void btAplicar_Click(object sender, EventArgs e)
        {
            /*
            Cursor.Current = Cursors.WaitCursor;

            if (rdGeral.Checked && rdRelatorioGeralArtigo.Checked && rdQuantidade.Checked || rdGeral.Checked && rdRelatorioGeralArtigo.Checked && rdPorValor.Checked || rdGeral.Checked && rdRelatorioGeralArtigo.Checked || rdParcial.Checked && rdRelatorioGeralArtigo.Checked && rdQuantidade.Checked || rdParcial.Checked && rdRelatorioGeralArtigo.Checked && rdPorValor.Checked || rdParcial.Checked && rdRelatorioGeralArtigo.Checked)
            {
                btRelatGeral_Click(sender, e);
            }cmbLoja.Text != "" && cmbOperador.Text == "" &&
       
            */




        }

        private void button1_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
       
            if (rdGeral.Checked && rdQuantidade.Checked )
           
               {
                tipoRelatorio = "RESUMO DÍARIO POR OPERADOR DE CONTA CONTABLÍSTICA DE  ";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer1.LocalReport.SetParameters(p);

                this.DataTable1TableAdapter.FillmovimentoOperadorLoja(this.MovimentosPorOperador.DataTable1, dtpDataInicial.Text, dtpDataFinal.Text, Variavel.codLoja);
                MessageBox.Show("1");
                this.reportViewer1.RefreshReport();
                
                
                
                  }



            else if (rdParcial.Checked &&  rdQuantidade.Checked )
            {
                if (cmbLoja.Text=="" || cmbOperador.Text=="")
                      MessageBox.Show("DEVE PRIMEIRO PREENCHER OS CAMPOS EM BRANCO ");
                else { 
                      tipoRelatorio = "RESUMO DÍARIO POR OPERADOR DE CONTA CONTABLISTICA DE ";
                      loja = ".";
                      Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                      p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                      p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                      p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                      p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                      p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                      reportViewer1.LocalReport.SetParameters(p);
                      this.DataTable1TableAdapter.FillmovimentoOperadorLojaEoperador(this.MovimentosPorOperador.DataTable1, dtpDataInicial.Text, dtpDataFinal.Text, codLoja, codUtilizador);
            
                      MessageBox.Show("2");
                      this.reportViewer1.RefreshReport();
                

                   }


            }

            reportViewer1.Visible = true;
            reportViewer2.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            if (rdGeral.Checked )
            {
                tipoRelatorio = "RESUMO DÍARIO POR OPERADOR DE CONTA CONTABLÍSTICA DE  ";
                loja = ".";
                Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                reportViewer2.LocalReport.SetParameters(p);
                MessageBox.Show("1 SEMC");
                this.DataTable2TableAdapter.FillmovimentoOperadorIDloja(this.MovimentosPorOperador.DataTable2, dtpDataInicial.Text, dtpDataFinal.Text, codLoja);
                this.reportViewer2.RefreshReport();



            }



            else if (rdParcial.Checked )
            {
                if (cmbLoja.Text == "" || cmbOperador.Text == "")
                    MessageBox.Show("DEVE PRIMEIRO PREENCHER OS CAMPOS EM BRANCO ");
                else
                {
                    tipoRelatorio = "RESUMO DÍARIO POR OPERADOR DE CONTA CONTABLISTICA DE ";
                    loja = ".";
                    Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[5];
                    p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
                    p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
                    p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
                    p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", loja);
                    p[4] = new Microsoft.Reporting.WinForms.ReportParameter("tipoRelatorio", tipoRelatorio);

                    reportViewer1.LocalReport.SetParameters(p);
                    this.DataTable2TableAdapter.FillBymovimentoOperadorIDlojaUtilizador(this.MovimentosPorOperador.DataTable2, dtpDataInicial.Text, dtpDataFinal.Text, codLoja, codUtilizador);

                    MessageBox.Show("2 SEMC");
                    this.reportViewer2.RefreshReport();


                }


            }

            reportViewer1.Visible =false ;
            reportViewer2.Visible = true;


                                                                                                                                             
           
            
        }

        private void rdGeral_CheckedChanged(object sender, EventArgs e)
        {
            cmbLoja.Enabled = false;
            cmbOperador.Enabled = false;
           
            
            cmbOperador.Text = "";
            cmbLoja.Text = "";
            cmbOperador.Items.Clear();
         

        }

        private void rdParcial_CheckedChanged(object sender, EventArgs e)
        {
            cmbLoja.Enabled = true;
            cmbOperador.Enabled = true;
           
            

        }

      
    }
}
