﻿namespace SISTEC_PLUS.POS.FormsRelatoriosPOS
{
    partial class FormAcumuladosPorFamilia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.btPaarametro = new System.Windows.Forms.Button();
            this.btMinimizar = new System.Windows.Forms.Button();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.dtpDataInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpDataFinal = new System.Windows.Forms.DateTimePicker();
            this.btMinimisar = new System.Windows.Forms.Button();
            this.cbPadrao = new System.Windows.Forms.CheckBox();
            this.rdAnalitico = new System.Windows.Forms.RadioButton();
            this.rdSintetico = new System.Windows.Forms.RadioButton();
            this.cbFamilia2 = new System.Windows.Forms.ComboBox();
            this.cbFamilia = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btFamSintetico = new System.Windows.Forms.Button();
            this.btFamAnalitico = new System.Windows.Forms.Button();
            this.btAplicar = new System.Windows.Forms.Button();
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.grpdata = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.grpcliente = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.VW_POSVENDAS_ResumoVendaFamBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.FamiliaSintetico = new SISTEC_PLUS.POS.FormsRelatoriosPOS.FamiliaSintetico();
            this.VW_POSVENDAS_ResumoVendaArtBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.FamiliaAnalitico = new SISTEC_PLUS.POS.FormsRelatoriosPOS.FamiliaAnalitico();
            this.AcumuladosPorFamiliaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.VW_POSVENDAS_ResumoVendaArtTableAdapter = new SISTEC_PLUS.POS.FormsRelatoriosPOS.FamiliaAnaliticoTableAdapters.VW_POSVENDAS_ResumoVendaArtTableAdapter();
            this.VW_POSVENDAS_ResumoVendaFamTableAdapter = new SISTEC_PLUS.POS.FormsRelatoriosPOS.FamiliaSinteticoTableAdapters.VW_POSVENDAS_ResumoVendaFamTableAdapter();
            this.panel1.SuspendLayout();
            this.grpdata.SuspendLayout();
            this.grpcliente.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VW_POSVENDAS_ResumoVendaFamBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FamiliaSintetico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VW_POSVENDAS_ResumoVendaArtBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FamiliaAnalitico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcumuladosPorFamiliaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btPaarametro
            // 
            this.btPaarametro.Location = new System.Drawing.Point(23, 74);
            this.btPaarametro.Name = "btPaarametro";
            this.btPaarametro.Size = new System.Drawing.Size(75, 27);
            this.btPaarametro.TabIndex = 14;
            this.btPaarametro.Text = "Parametros";
            this.btPaarametro.UseVisualStyleBackColor = true;
            this.btPaarametro.Click += new System.EventHandler(this.btPaarametro_Click);
            // 
            // btMinimizar
            // 
            this.btMinimizar.Location = new System.Drawing.Point(35, 39);
            this.btMinimizar.Name = "btMinimizar";
            this.btMinimizar.Size = new System.Drawing.Size(75, 28);
            this.btMinimizar.TabIndex = 6;
            this.btMinimizar.Text = "Minimizar";
            this.btMinimizar.UseVisualStyleBackColor = true;
            // 
            // cmbLoja
            // 
            this.cmbLoja.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.IntegralHeight = false;
            this.cmbLoja.Location = new System.Drawing.Point(98, 26);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(280, 24);
            this.cmbLoja.TabIndex = 13;
            this.cmbLoja.TextChanged += new System.EventHandler(this.cmbLoja_TextChanged);
            // 
            // dtpDataInicial
            // 
            this.dtpDataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataInicial.Location = new System.Drawing.Point(105, 34);
            this.dtpDataInicial.Name = "dtpDataInicial";
            this.dtpDataInicial.Size = new System.Drawing.Size(192, 21);
            this.dtpDataInicial.TabIndex = 16;
            this.dtpDataInicial.ValueChanged += new System.EventHandler(this.dtpDataInicial_ValueChanged);
            // 
            // dtpDataFinal
            // 
            this.dtpDataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataFinal.Location = new System.Drawing.Point(105, 64);
            this.dtpDataFinal.Name = "dtpDataFinal";
            this.dtpDataFinal.Size = new System.Drawing.Size(192, 21);
            this.dtpDataFinal.TabIndex = 17;
            this.dtpDataFinal.ValueChanged += new System.EventHandler(this.dtpDataFinal_ValueChanged);
            // 
            // btMinimisar
            // 
            this.btMinimisar.Location = new System.Drawing.Point(215, 39);
            this.btMinimisar.Name = "btMinimisar";
            this.btMinimisar.Size = new System.Drawing.Size(75, 28);
            this.btMinimisar.TabIndex = 13;
            this.btMinimisar.Text = "Minimizar";
            this.btMinimisar.UseVisualStyleBackColor = true;
            this.btMinimisar.Click += new System.EventHandler(this.btMinimisar_Click);
            // 
            // cbPadrao
            // 
            this.cbPadrao.AutoSize = true;
            this.cbPadrao.ForeColor = System.Drawing.Color.White;
            this.cbPadrao.Location = new System.Drawing.Point(314, 68);
            this.cbPadrao.Name = "cbPadrao";
            this.cbPadrao.Size = new System.Drawing.Size(64, 20);
            this.cbPadrao.TabIndex = 137;
            this.cbPadrao.Text = "Padrão";
            this.cbPadrao.UseVisualStyleBackColor = true;
            // 
            // rdAnalitico
            // 
            this.rdAnalitico.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdAnalitico.AutoSize = true;
            this.rdAnalitico.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdAnalitico.Location = new System.Drawing.Point(199, 68);
            this.rdAnalitico.Name = "rdAnalitico";
            this.rdAnalitico.Size = new System.Drawing.Size(75, 20);
            this.rdAnalitico.TabIndex = 136;
            this.rdAnalitico.TabStop = true;
            this.rdAnalitico.Text = "Analítico";
            this.rdAnalitico.UseVisualStyleBackColor = true;
            // 
            // rdSintetico
            // 
            this.rdSintetico.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdSintetico.AutoSize = true;
            this.rdSintetico.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdSintetico.Location = new System.Drawing.Point(98, 68);
            this.rdSintetico.Name = "rdSintetico";
            this.rdSintetico.Size = new System.Drawing.Size(73, 20);
            this.rdSintetico.TabIndex = 132;
            this.rdSintetico.TabStop = true;
            this.rdSintetico.Text = "Sintético";
            this.rdSintetico.UseVisualStyleBackColor = true;
            // 
            // cbFamilia2
            // 
            this.cbFamilia2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbFamilia2.FormattingEnabled = true;
            this.cbFamilia2.IntegralHeight = false;
            this.cbFamilia2.Location = new System.Drawing.Point(91, 65);
            this.cbFamilia2.Name = "cbFamilia2";
            this.cbFamilia2.Size = new System.Drawing.Size(282, 24);
            this.cbFamilia2.TabIndex = 135;
            this.cbFamilia2.TextChanged += new System.EventHandler(this.cbFamilia2_TextChanged);
            // 
            // cbFamilia
            // 
            this.cbFamilia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbFamilia.FormattingEnabled = true;
            this.cbFamilia.IntegralHeight = false;
            this.cbFamilia.Location = new System.Drawing.Point(91, 34);
            this.cbFamilia.Name = "cbFamilia";
            this.cbFamilia.Size = new System.Drawing.Size(282, 24);
            this.cbFamilia.TabIndex = 134;
            this.cbFamilia.TextChanged += new System.EventHandler(this.cbFamilia_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.grpcliente);
            this.panel1.Controls.Add(this.grpdata);
            this.panel1.Controls.Add(this.btFamSintetico);
            this.panel1.Controls.Add(this.btFamAnalitico);
            this.panel1.Controls.Add(this.btMinimisar);
            this.panel1.Controls.Add(this.btMinimizar);
            this.panel1.Controls.Add(this.btAplicar);
            this.panel1.Location = new System.Drawing.Point(-192, 689);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1853, 243);
            this.panel1.TabIndex = 13;
            this.panel1.Visible = false;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btFamSintetico
            // 
            this.btFamSintetico.Location = new System.Drawing.Point(1578, 105);
            this.btFamSintetico.Name = "btFamSintetico";
            this.btFamSintetico.Size = new System.Drawing.Size(75, 28);
            this.btFamSintetico.TabIndex = 143;
            this.btFamSintetico.Text = "Sintetico";
            this.btFamSintetico.UseVisualStyleBackColor = true;
            this.btFamSintetico.Click += new System.EventHandler(this.btFamSintetico_Click);
            // 
            // btFamAnalitico
            // 
            this.btFamAnalitico.Location = new System.Drawing.Point(1578, 74);
            this.btFamAnalitico.Name = "btFamAnalitico";
            this.btFamAnalitico.Size = new System.Drawing.Size(75, 28);
            this.btFamAnalitico.TabIndex = 142;
            this.btFamAnalitico.Text = "Analitico";
            this.btFamAnalitico.UseVisualStyleBackColor = true;
            this.btFamAnalitico.Click += new System.EventHandler(this.btFamAnalitico_Click);
            // 
            // btAplicar
            // 
            this.btAplicar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btAplicar.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btAplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAplicar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAplicar.ForeColor = System.Drawing.Color.White;
            this.btAplicar.Location = new System.Drawing.Point(296, 165);
            this.btAplicar.Name = "btAplicar";
            this.btAplicar.Size = new System.Drawing.Size(77, 35);
            this.btAplicar.TabIndex = 1;
            this.btAplicar.Text = "Aplicar";
            this.btAplicar.UseVisualStyleBackColor = false;
            this.btAplicar.Click += new System.EventHandler(this.btAplicar_Click);
            // 
            // reportViewer2
            // 
            this.reportViewer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "FamiliaSintetico";
            reportDataSource1.Value = this.VW_POSVENDAS_ResumoVendaFamBindingSource;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "SISTEC_PLUS.POS.FormsRelatoriosPOS.FamiliaSintetico.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(104, 74);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.Size = new System.Drawing.Size(1268, 609);
            this.reportViewer2.TabIndex = 16;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource2.Name = "FamiliaAnalitico";
            reportDataSource2.Value = this.VW_POSVENDAS_ResumoVendaArtBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "SISTEC_PLUS.POS.FormsRelatoriosPOS.FamiliaAnalitico.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(104, 74);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(1268, 609);
            this.reportViewer1.TabIndex = 15;
            // 
            // grpdata
            // 
            this.grpdata.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpdata.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.grpdata.Controls.Add(this.label8);
            this.grpdata.Controls.Add(this.label9);
            this.grpdata.Controls.Add(this.dtpDataInicial);
            this.grpdata.Controls.Add(this.dtpDataFinal);
            this.grpdata.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpdata.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpdata.Location = new System.Drawing.Point(1213, 35);
            this.grpdata.Name = "grpdata";
            this.grpdata.Size = new System.Drawing.Size(351, 115);
            this.grpdata.TabIndex = 40;
            this.grpdata.TabStop = false;
            this.grpdata.Text = "Data";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(76, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "A";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(76, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "De";
            // 
            // grpcliente
            // 
            this.grpcliente.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.grpcliente.Controls.Add(this.label5);
            this.grpcliente.Controls.Add(this.label1);
            this.grpcliente.Controls.Add(this.cbFamilia);
            this.grpcliente.Controls.Add(this.cbFamilia2);
            this.grpcliente.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpcliente.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpcliente.Location = new System.Drawing.Point(748, 35);
            this.grpcliente.Name = "grpcliente";
            this.grpcliente.Size = new System.Drawing.Size(459, 115);
            this.grpcliente.TabIndex = 144;
            this.grpcliente.TabStop = false;
            this.grpcliente.Text = "Por Família";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(71, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 16);
            this.label5.TabIndex = 3;
            this.label5.Text = "a";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(19, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Da Familia";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cmbLoja);
            this.groupBox1.Controls.Add(this.rdSintetico);
            this.groupBox1.Controls.Add(this.rdAnalitico);
            this.groupBox1.Controls.Add(this.cbPadrao);
            this.groupBox1.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox1.Location = new System.Drawing.Point(296, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(446, 115);
            this.groupBox1.TabIndex = 145;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Por Loja";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(55, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "LOJA";
            // 
            // VW_POSVENDAS_ResumoVendaFamBindingSource
            // 
            this.VW_POSVENDAS_ResumoVendaFamBindingSource.DataMember = "VW_POSVENDAS_ResumoVendaFam";
            this.VW_POSVENDAS_ResumoVendaFamBindingSource.DataSource = this.FamiliaSintetico;
            // 
            // FamiliaSintetico
            // 
            this.FamiliaSintetico.DataSetName = "FamiliaSintetico";
            this.FamiliaSintetico.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // VW_POSVENDAS_ResumoVendaArtBindingSource
            // 
            this.VW_POSVENDAS_ResumoVendaArtBindingSource.DataMember = "VW_POSVENDAS_ResumoVendaArt";
            this.VW_POSVENDAS_ResumoVendaArtBindingSource.DataSource = this.FamiliaAnalitico;
            // 
            // FamiliaAnalitico
            // 
            this.FamiliaAnalitico.DataSetName = "FamiliaAnalitico";
            this.FamiliaAnalitico.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // AcumuladosPorFamiliaBindingSource
            // 
            this.AcumuladosPorFamiliaBindingSource.DataSource = typeof(SISTEC_PLUS.POS.FormsRelatoriosPOS.AcumuladosPorFamilia);
            // 
            // VW_POSVENDAS_ResumoVendaArtTableAdapter
            // 
            this.VW_POSVENDAS_ResumoVendaArtTableAdapter.ClearBeforeFill = true;
            // 
            // VW_POSVENDAS_ResumoVendaFamTableAdapter
            // 
            this.VW_POSVENDAS_ResumoVendaFamTableAdapter.ClearBeforeFill = true;
            // 
            // FormAcumuladosPorFamilia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1468, 912);
            this.Controls.Add(this.reportViewer2);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.btPaarametro);
            this.Controls.Add(this.panel1);
            this.Name = "FormAcumuladosPorFamilia";
            this.Text = "Relátorios de Movimento por familia";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormAcumuladosPorFamilia_Load);
            this.panel1.ResumeLayout(false);
            this.grpdata.ResumeLayout(false);
            this.grpdata.PerformLayout();
            this.grpcliente.ResumeLayout(false);
            this.grpcliente.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VW_POSVENDAS_ResumoVendaFamBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FamiliaSintetico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VW_POSVENDAS_ResumoVendaArtBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FamiliaAnalitico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcumuladosPorFamiliaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource AcumuladosPorFamiliaBindingSource;
        private System.Windows.Forms.Button btPaarametro;
        private System.Windows.Forms.Button btMinimizar;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.DateTimePicker dtpDataInicial;
        private System.Windows.Forms.DateTimePicker dtpDataFinal;
        private System.Windows.Forms.Button btMinimisar;
        private System.Windows.Forms.CheckBox cbPadrao;
        private System.Windows.Forms.RadioButton rdAnalitico;
        private System.Windows.Forms.RadioButton rdSintetico;
        private System.Windows.Forms.ComboBox cbFamilia2;
        private System.Windows.Forms.ComboBox cbFamilia;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btAplicar;
        private System.Windows.Forms.Button btFamSintetico;
        private System.Windows.Forms.Button btFamAnalitico;
        private System.Windows.Forms.BindingSource VW_POSVENDAS_ResumoVendaArtBindingSource;
        private FamiliaAnalitico FamiliaAnalitico;
        private FamiliaAnaliticoTableAdapters.VW_POSVENDAS_ResumoVendaArtTableAdapter VW_POSVENDAS_ResumoVendaArtTableAdapter;
        private System.Windows.Forms.BindingSource VW_POSVENDAS_ResumoVendaFamBindingSource;
        private FamiliaSintetico FamiliaSintetico;
        private FamiliaSinteticoTableAdapters.VW_POSVENDAS_ResumoVendaFamTableAdapter VW_POSVENDAS_ResumoVendaFamTableAdapter;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox grpcliente;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpdata;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;

    }
}