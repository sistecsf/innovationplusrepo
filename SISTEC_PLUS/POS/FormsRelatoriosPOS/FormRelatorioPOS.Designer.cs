﻿namespace SISTEC_PLUS.POS
{
    partial class FormRelatorioPOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.Vendas = new MetroFramework.Controls.MetroTabPage();
            this.cbAcumuladosSubfamilia = new MetroFramework.Controls.MetroCheckBox();
            this.cbVacumulados = new MetroFramework.Controls.MetroCheckBox();
            this.cbVListagemMov = new MetroFramework.Controls.MetroCheckBox();
            this.cbVfaturacaoSemanaDia = new MetroFramework.Controls.MetroCheckBox();
            this.cbVfaturaDiaHora = new MetroFramework.Controls.MetroCheckBox();
            this.cbVresumoPerVendF = new MetroFramework.Controls.MetroCheckBox();
            this.cbVresumoPerVend = new MetroFramework.Controls.MetroCheckBox();
            this.cbVresumoPerOP = new MetroFramework.Controls.MetroCheckBox();
            this.cbVdocDesc = new MetroFramework.Controls.MetroCheckBox();
            this.cbVdocOperador = new MetroFramework.Controls.MetroCheckBox();
            this.cbVresumoDiario = new MetroFramework.Controls.MetroCheckBox();
            this.cbVresumoDiarioDoc = new MetroFramework.Controls.MetroCheckBox();
            this.cbVlistagem = new MetroFramework.Controls.MetroCheckBox();
            this.cbVvendasDinhVend = new MetroFramework.Controls.MetroCheckBox();
            this.cbVvendasDinhOp = new MetroFramework.Controls.MetroCheckBox();
            this.cbVmovimentoArtigo = new MetroFramework.Controls.MetroCheckBox();
            this.cbVacumuladosFamilia = new MetroFramework.Controls.MetroCheckBox();
            this.cbVacumuladosArmazem = new MetroFramework.Controls.MetroCheckBox();
            this.cbIantesFecho = new MetroFramework.Controls.MetroTabPage();
            this.cbContagemAd = new MetroFramework.Controls.MetroCheckBox();
            this.cbIdepoisFecho = new MetroFramework.Controls.MetroCheckBox();
            this.cbIantesFe = new MetroFramework.Controls.MetroCheckBox();
            this.Artigo = new MetroFramework.Controls.MetroTabPage();
            this.cbAitensAtivos = new MetroFramework.Controls.MetroCheckBox();
            this.cbAlocali = new MetroFramework.Controls.MetroCheckBox();
            this.cbAlistagemArt = new MetroFramework.Controls.MetroCheckBox();
            this.cbApontoEncom = new MetroFramework.Controls.MetroCheckBox();
            this.cbArotacaoStock = new MetroFramework.Controls.MetroCheckBox();
            this.cbAsemQuant = new MetroFramework.Controls.MetroCheckBox();
            this.cbAhistorMov = new MetroFramework.Controls.MetroCheckBox();
            this.cbAexistencias = new MetroFramework.Controls.MetroCheckBox();
            this.cbAetiqueta = new MetroFramework.Controls.MetroCheckBox();
            this.cbAprecario = new MetroFramework.Controls.MetroCheckBox();
            this.Movimentos = new MetroFramework.Controls.MetroTabPage();
            this.cbMsaidas = new MetroFramework.Controls.MetroCheckBox();
            this.cbMentradas = new MetroFramework.Controls.MetroCheckBox();
            this.cbMentradaFornec = new MetroFramework.Controls.MetroCheckBox();
            this.cbMgeralArt = new MetroFramework.Controls.MetroCheckBox();
            this.cbMdetalheMov = new MetroFramework.Controls.MetroCheckBox();
            this.MapaEstatico = new MetroFramework.Controls.MetroTabPage();
            this.cbMAartigoAno = new MetroFramework.Controls.MetroCheckBox();
            this.cbMAsaidaPano = new MetroFramework.Controls.MetroCheckBox();
            this.cbMAsaidasAno = new MetroFramework.Controls.MetroCheckBox();
            this.cbMmovimPorAno = new MetroFramework.Controls.MetroCheckBox();
            this.cbMmovimento = new MetroFramework.Controls.MetroCheckBox();
            this.Tabelas = new MetroFramework.Controls.MetroTabPage();
            this.cbTAfamilia = new MetroFramework.Controls.MetroCheckBox();
            this.cbMAsubFam = new MetroFramework.Controls.MetroCheckBox();
            this.cbTAmerc = new MetroFramework.Controls.MetroCheckBox();
            this.cbTAloja = new MetroFramework.Controls.MetroCheckBox();
            this.ReemissãoGuias = new MetroFramework.Controls.MetroTabPage();
            this.cbTAentrMerc = new MetroFramework.Controls.MetroCheckBox();
            this.cbTAguiasTransp = new MetroFramework.Controls.MetroCheckBox();
            this.cbTAentrSaidAjus = new MetroFramework.Controls.MetroCheckBox();
            this.cbTAsaidaReq = new MetroFramework.Controls.MetroCheckBox();
            this.cbTAsaidaTrans = new MetroFramework.Controls.MetroCheckBox();
            this.cbTAentradaMerc = new MetroFramework.Controls.MetroCheckBox();
            this.ValoresStock = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.Loja = new MetroFramework.Controls.MetroLabel();
            this.metroButton6 = new MetroFramework.Controls.MetroButton();
            this.cmbVmercador = new System.Windows.Forms.ComboBox();
            this.cmbVloja = new System.Windows.Forms.ComboBox();
            this.txtRVDataExist = new System.Windows.Forms.TextBox();
            this.metroRadioButton1 = new MetroFramework.Controls.MetroRadioButton();
            this.Geral = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.EntArtigos = new MetroFramework.Controls.MetroTabPage();
            this.btEimprimir = new MetroFramework.Controls.MetroButton();
            this.txtEdataFinal = new System.Windows.Forms.TextBox();
            this.txtEdata = new System.Windows.Forms.TextBox();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.cmbEdescricao = new System.Windows.Forms.ComboBox();
            this.cmbEreferencia = new System.Windows.Forms.ComboBox();
            this.cmbEmercadoria = new System.Windows.Forms.ComboBox();
            this.cmbEloja = new System.Windows.Forms.ComboBox();
            this.FolhaEspelho = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.txtEnumProf = new MetroFramework.Controls.MetroTextBox();
            this.btFimprimir = new MetroFramework.Controls.MetroButton();
            this.TransfPendentes = new MetroFramework.Controls.MetroTabPage();
            this.btTimprimir = new MetroFramework.Controls.MetroButton();
            this.cmbFlojaDest = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.CurvaAbc = new MetroFramework.Controls.MetroTabPage();
            this.cbCexpExSaid = new MetroFramework.Controls.MetroCheckBox();
            this.cbCexportEXvend = new MetroFramework.Controls.MetroCheckBox();
            this.cbCvendasArtSmov = new MetroFramework.Controls.MetroCheckBox();
            this.cbCvendas = new MetroFramework.Controls.MetroCheckBox();
            this.cbCsaidaArt = new MetroFramework.Controls.MetroCheckBox();
            this.ApoioGestão = new MetroFramework.Controls.MetroTabPage();
            this.cbAGsituEnc = new MetroFramework.Controls.MetroCheckBox();
            this.cbAGriAteZero = new MetroFramework.Controls.MetroCheckBox();
            this.cbAGriPend = new MetroFramework.Controls.MetroCheckBox();
            this.cbAGriAtend = new MetroFramework.Controls.MetroCheckBox();
            this.cbAGriEstad = new MetroFramework.Controls.MetroCheckBox();
            this.cbAGsugestEnc = new MetroFramework.Controls.MetroCheckBox();
            this.cbAGstockTem = new MetroFramework.Controls.MetroCheckBox();
            this.cbAGstockGeral = new MetroFramework.Controls.MetroCheckBox();
            this.cbAGstockZero = new MetroFramework.Controls.MetroCheckBox();
            this.MapaComparativo = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.cmbMloja = new MetroFramework.Controls.MetroComboBox();
            this.cmbMlojaNexis = new MetroFramework.Controls.MetroComboBox();
            this.btMimprimir = new MetroFramework.Controls.MetroButton();
            this.Graficos = new MetroFramework.Controls.MetroTabPage();
            this.cmbGgraficosArm = new MetroFramework.Controls.MetroCheckBox();
            this.cmbGgraficosLj = new MetroFramework.Controls.MetroCheckBox();
            this.cmbGgraficosArt = new MetroFramework.Controls.MetroCheckBox();
            this.cmbGgraficosOp = new MetroFramework.Controls.MetroCheckBox();
            this.metroTabControl1.SuspendLayout();
            this.Vendas.SuspendLayout();
            this.cbIantesFecho.SuspendLayout();
            this.Artigo.SuspendLayout();
            this.Movimentos.SuspendLayout();
            this.MapaEstatico.SuspendLayout();
            this.Tabelas.SuspendLayout();
            this.ReemissãoGuias.SuspendLayout();
            this.ValoresStock.SuspendLayout();
            this.EntArtigos.SuspendLayout();
            this.FolhaEspelho.SuspendLayout();
            this.TransfPendentes.SuspendLayout();
            this.CurvaAbc.SuspendLayout();
            this.ApoioGestão.SuspendLayout();
            this.MapaComparativo.SuspendLayout();
            this.Graficos.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.Vendas);
            this.metroTabControl1.Controls.Add(this.cbIantesFecho);
            this.metroTabControl1.Controls.Add(this.Artigo);
            this.metroTabControl1.Controls.Add(this.Movimentos);
            this.metroTabControl1.Controls.Add(this.MapaEstatico);
            this.metroTabControl1.Controls.Add(this.Tabelas);
            this.metroTabControl1.Controls.Add(this.ReemissãoGuias);
            this.metroTabControl1.Controls.Add(this.ValoresStock);
            this.metroTabControl1.Controls.Add(this.EntArtigos);
            this.metroTabControl1.Controls.Add(this.FolhaEspelho);
            this.metroTabControl1.Controls.Add(this.TransfPendentes);
            this.metroTabControl1.Controls.Add(this.CurvaAbc);
            this.metroTabControl1.Controls.Add(this.ApoioGestão);
            this.metroTabControl1.Controls.Add(this.MapaComparativo);
            this.metroTabControl1.Controls.Add(this.Graficos);
            this.metroTabControl1.Location = new System.Drawing.Point(12, 118);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(1213, 419);
            this.metroTabControl1.TabIndex = 1;
            this.metroTabControl1.UseSelectable = true;
            // 
            // Vendas
            // 
            this.Vendas.Controls.Add(this.cbAcumuladosSubfamilia);
            this.Vendas.Controls.Add(this.cbVacumulados);
            this.Vendas.Controls.Add(this.cbVListagemMov);
            this.Vendas.Controls.Add(this.cbVfaturacaoSemanaDia);
            this.Vendas.Controls.Add(this.cbVfaturaDiaHora);
            this.Vendas.Controls.Add(this.cbVresumoPerVendF);
            this.Vendas.Controls.Add(this.cbVresumoPerVend);
            this.Vendas.Controls.Add(this.cbVresumoPerOP);
            this.Vendas.Controls.Add(this.cbVdocDesc);
            this.Vendas.Controls.Add(this.cbVdocOperador);
            this.Vendas.Controls.Add(this.cbVresumoDiario);
            this.Vendas.Controls.Add(this.cbVresumoDiarioDoc);
            this.Vendas.Controls.Add(this.cbVlistagem);
            this.Vendas.Controls.Add(this.cbVvendasDinhVend);
            this.Vendas.Controls.Add(this.cbVvendasDinhOp);
            this.Vendas.Controls.Add(this.cbVmovimentoArtigo);
            this.Vendas.Controls.Add(this.cbVacumuladosFamilia);
            this.Vendas.Controls.Add(this.cbVacumuladosArmazem);
            this.Vendas.HorizontalScrollbarBarColor = true;
            this.Vendas.HorizontalScrollbarHighlightOnWheel = false;
            this.Vendas.HorizontalScrollbarSize = 10;
            this.Vendas.Location = new System.Drawing.Point(4, 38);
            this.Vendas.Name = "Vendas";
            this.Vendas.Size = new System.Drawing.Size(1205, 377);
            this.Vendas.TabIndex = 0;
            this.Vendas.Text = "Vendas";
            this.Vendas.VerticalScrollbarBarColor = true;
            this.Vendas.VerticalScrollbarHighlightOnWheel = false;
            this.Vendas.VerticalScrollbarSize = 10;
            this.Vendas.Click += new System.EventHandler(this.Vendas_Click);
            // 
            // cbAcumuladosSubfamilia
            // 
            this.cbAcumuladosSubfamilia.AutoSize = true;
            this.cbAcumuladosSubfamilia.Location = new System.Drawing.Point(23, 143);
            this.cbAcumuladosSubfamilia.Name = "cbAcumuladosSubfamilia";
            this.cbAcumuladosSubfamilia.Size = new System.Drawing.Size(177, 15);
            this.cbAcumuladosSubfamilia.TabIndex = 25;
            this.cbAcumuladosSubfamilia.Text = "Acumulados por Sub-Familia";
            this.cbAcumuladosSubfamilia.UseSelectable = true;
            this.cbAcumuladosSubfamilia.Click += new System.EventHandler(this.cbAcumuladosSubfamilia_Click);
            // 
            // cbVacumulados
            // 
            this.cbVacumulados.AutoSize = true;
            this.cbVacumulados.BackColor = System.Drawing.Color.White;
            this.cbVacumulados.Location = new System.Drawing.Point(23, 80);
            this.cbVacumulados.Name = "cbVacumulados";
            this.cbVacumulados.Size = new System.Drawing.Size(198, 15);
            this.cbVacumulados.TabIndex = 24;
            this.cbVacumulados.Text = "Acumulados por Doc e Armazém";
            this.cbVacumulados.UseSelectable = true;
            this.cbVacumulados.Click += new System.EventHandler(this.cbVacumulados_Click_1);
            // 
            // cbVListagemMov
            // 
            this.cbVListagemMov.AutoSize = true;
            this.cbVListagemMov.Location = new System.Drawing.Point(743, 101);
            this.cbVListagemMov.Name = "cbVListagemMov";
            this.cbVListagemMov.Size = new System.Drawing.Size(208, 15);
            this.cbVListagemMov.TabIndex = 23;
            this.cbVListagemMov.Text = "Listagem Detalhada de Movimento";
            this.cbVListagemMov.UseSelectable = true;
            // 
            // cbVfaturacaoSemanaDia
            // 
            this.cbVfaturacaoSemanaDia.AutoSize = true;
            this.cbVfaturacaoSemanaDia.Location = new System.Drawing.Point(743, 80);
            this.cbVfaturacaoSemanaDia.Name = "cbVfaturacaoSemanaDia";
            this.cbVfaturacaoSemanaDia.Size = new System.Drawing.Size(148, 15);
            this.cbVfaturacaoSemanaDia.TabIndex = 22;
            this.cbVfaturacaoSemanaDia.Text = "Faturaçao Semana / Dia";
            this.cbVfaturacaoSemanaDia.UseSelectable = true;
            // 
            // cbVfaturaDiaHora
            // 
            this.cbVfaturaDiaHora.AutoSize = true;
            this.cbVfaturaDiaHora.Location = new System.Drawing.Point(743, 122);
            this.cbVfaturaDiaHora.Name = "cbVfaturaDiaHora";
            this.cbVfaturaDiaHora.Size = new System.Drawing.Size(132, 15);
            this.cbVfaturaDiaHora.TabIndex = 21;
            this.cbVfaturaDiaHora.Text = "Faturação Dia / Hora";
            this.cbVfaturaDiaHora.UseSelectable = true;
            // 
            // cbVresumoPerVendF
            // 
            this.cbVresumoPerVendF.AutoSize = true;
            this.cbVresumoPerVendF.Location = new System.Drawing.Point(483, 143);
            this.cbVresumoPerVendF.Name = "cbVresumoPerVendF";
            this.cbVresumoPerVendF.Size = new System.Drawing.Size(244, 15);
            this.cbVresumoPerVendF.TabIndex = 20;
            this.cbVresumoPerVendF.Text = "Resumo Períodico por Vendedor (Factura)";
            this.cbVresumoPerVendF.UseSelectable = true;
            // 
            // cbVresumoPerVend
            // 
            this.cbVresumoPerVend.AutoSize = true;
            this.cbVresumoPerVend.Location = new System.Drawing.Point(483, 122);
            this.cbVresumoPerVend.Name = "cbVresumoPerVend";
            this.cbVresumoPerVend.Size = new System.Drawing.Size(194, 15);
            this.cbVresumoPerVend.TabIndex = 19;
            this.cbVresumoPerVend.Text = "Resumo Períodico por Vendedor";
            this.cbVresumoPerVend.UseSelectable = true;
            // 
            // cbVresumoPerOP
            // 
            this.cbVresumoPerOP.AutoSize = true;
            this.cbVresumoPerOP.Location = new System.Drawing.Point(483, 101);
            this.cbVresumoPerOP.Name = "cbVresumoPerOP";
            this.cbVresumoPerOP.Size = new System.Drawing.Size(193, 15);
            this.cbVresumoPerOP.TabIndex = 18;
            this.cbVresumoPerOP.Text = "Resumo Periódico por Operador";
            this.cbVresumoPerOP.UseSelectable = true;
            // 
            // cbVdocDesc
            // 
            this.cbVdocDesc.AutoSize = true;
            this.cbVdocDesc.Location = new System.Drawing.Point(483, 80);
            this.cbVdocDesc.Name = "cbVdocDesc";
            this.cbVdocDesc.Size = new System.Drawing.Size(171, 15);
            this.cbVdocDesc.TabIndex = 17;
            this.cbVdocDesc.Text = "Documentos com Desconto";
            this.cbVdocDesc.UseSelectable = true;
            // 
            // cbVdocOperador
            // 
            this.cbVdocOperador.AutoSize = true;
            this.cbVdocOperador.Location = new System.Drawing.Point(483, 164);
            this.cbVdocOperador.Name = "cbVdocOperador";
            this.cbVdocOperador.Size = new System.Drawing.Size(161, 15);
            this.cbVdocOperador.TabIndex = 16;
            this.cbVdocOperador.Text = "Documentos do Operador";
            this.cbVdocOperador.UseSelectable = true;
            // 
            // cbVresumoDiario
            // 
            this.cbVresumoDiario.AutoSize = true;
            this.cbVresumoDiario.Location = new System.Drawing.Point(243, 143);
            this.cbVresumoDiario.Name = "cbVresumoDiario";
            this.cbVresumoDiario.Size = new System.Drawing.Size(100, 15);
            this.cbVresumoDiario.TabIndex = 15;
            this.cbVresumoDiario.Text = "Resumo Diário";
            this.cbVresumoDiario.UseSelectable = true;
            // 
            // cbVresumoDiarioDoc
            // 
            this.cbVresumoDiarioDoc.AutoSize = true;
            this.cbVresumoDiarioDoc.Location = new System.Drawing.Point(243, 122);
            this.cbVresumoDiarioDoc.Name = "cbVresumoDiarioDoc";
            this.cbVresumoDiarioDoc.Size = new System.Drawing.Size(187, 15);
            this.cbVresumoDiarioDoc.TabIndex = 14;
            this.cbVresumoDiarioDoc.Text = "Resumo Diário por Documento";
            this.cbVresumoDiarioDoc.UseSelectable = true;
            // 
            // cbVlistagem
            // 
            this.cbVlistagem.AutoSize = true;
            this.cbVlistagem.Location = new System.Drawing.Point(243, 101);
            this.cbVlistagem.Name = "cbVlistagem";
            this.cbVlistagem.Size = new System.Drawing.Size(183, 15);
            this.cbVlistagem.TabIndex = 13;
            this.cbVlistagem.Text = "Listagem detalhada de Vendas";
            this.cbVlistagem.UseSelectable = true;
            // 
            // cbVvendasDinhVend
            // 
            this.cbVvendasDinhVend.AutoSize = true;
            this.cbVvendasDinhVend.Location = new System.Drawing.Point(243, 164);
            this.cbVvendasDinhVend.Name = "cbVvendasDinhVend";
            this.cbVvendasDinhVend.Size = new System.Drawing.Size(191, 15);
            this.cbVvendasDinhVend.TabIndex = 12;
            this.cbVvendasDinhVend.Text = "Vendas a dinheiro por vendedor";
            this.cbVvendasDinhVend.UseSelectable = true;
            // 
            // cbVvendasDinhOp
            // 
            this.cbVvendasDinhOp.AutoSize = true;
            this.cbVvendasDinhOp.Location = new System.Drawing.Point(243, 80);
            this.cbVvendasDinhOp.Name = "cbVvendasDinhOp";
            this.cbVvendasDinhOp.Size = new System.Drawing.Size(192, 15);
            this.cbVvendasDinhOp.TabIndex = 11;
            this.cbVvendasDinhOp.Text = "Vendas a Dinheiro por Operador";
            this.cbVvendasDinhOp.UseSelectable = true;
            this.cbVvendasDinhOp.Click += new System.EventHandler(this.cbVvendasDinhOp_Click);
            // 
            // cbVmovimentoArtigo
            // 
            this.cbVmovimentoArtigo.AutoSize = true;
            this.cbVmovimentoArtigo.Location = new System.Drawing.Point(23, 164);
            this.cbVmovimentoArtigo.Name = "cbVmovimentoArtigo";
            this.cbVmovimentoArtigo.Size = new System.Drawing.Size(147, 15);
            this.cbVmovimentoArtigo.TabIndex = 10;
            this.cbVmovimentoArtigo.Text = "Movimentos por Artigo";
            this.cbVmovimentoArtigo.UseSelectable = true;

            // 
            // cbVacumuladosFamilia
            // 
            this.cbVacumuladosFamilia.AutoSize = true;
            this.cbVacumuladosFamilia.Location = new System.Drawing.Point(23, 122);
            this.cbVacumuladosFamilia.Name = "cbVacumuladosFamilia";
            this.cbVacumuladosFamilia.Size = new System.Drawing.Size(152, 15);
            this.cbVacumuladosFamilia.TabIndex = 9;
            this.cbVacumuladosFamilia.Text = "Acumulados por Familia";
            this.cbVacumuladosFamilia.UseSelectable = true;
            this.cbVacumuladosFamilia.Click += new System.EventHandler(this.cbVacumuladosFamilia_Click);
            // 
            // cbVacumuladosArmazem
            // 
            this.cbVacumuladosArmazem.AutoSize = true;
            this.cbVacumuladosArmazem.Location = new System.Drawing.Point(23, 101);
            this.cbVacumuladosArmazem.Name = "cbVacumuladosArmazem";
            this.cbVacumuladosArmazem.Size = new System.Drawing.Size(165, 15);
            this.cbVacumuladosArmazem.TabIndex = 8;
            this.cbVacumuladosArmazem.Text = "Acumulados por Armazém";
            this.cbVacumuladosArmazem.UseSelectable = true;
            this.cbVacumuladosArmazem.Click += new System.EventHandler(this.cbVacumuladosArmazem_Click);
            // 
            // cbIantesFecho
            // 
            this.cbIantesFecho.Controls.Add(this.cbContagemAd);
            this.cbIantesFecho.Controls.Add(this.cbIdepoisFecho);
            this.cbIantesFecho.Controls.Add(this.cbIantesFe);
            this.cbIantesFecho.HorizontalScrollbarBarColor = true;
            this.cbIantesFecho.HorizontalScrollbarHighlightOnWheel = false;
            this.cbIantesFecho.HorizontalScrollbarSize = 10;
            this.cbIantesFecho.Location = new System.Drawing.Point(4, 38);
            this.cbIantesFecho.Name = "cbIantesFecho";
            this.cbIantesFecho.Size = new System.Drawing.Size(1205, 377);
            this.cbIantesFecho.TabIndex = 1;
            this.cbIantesFecho.Text = "Inventário";
            this.cbIantesFecho.VerticalScrollbarBarColor = true;
            this.cbIantesFecho.VerticalScrollbarHighlightOnWheel = false;
            this.cbIantesFecho.VerticalScrollbarSize = 10;
            // 
            // cbContagemAd
            // 
            this.cbContagemAd.AutoSize = true;
            this.cbContagemAd.Location = new System.Drawing.Point(56, 131);
            this.cbContagemAd.Name = "cbContagemAd";
            this.cbContagemAd.Size = new System.Drawing.Size(122, 15);
            this.cbContagemAd.TabIndex = 4;
            this.cbContagemAd.Text = "Contagem Ad-hoc";
            this.cbContagemAd.UseSelectable = true;
            // 
            // cbIdepoisFecho
            // 
            this.cbIdepoisFecho.AutoSize = true;
            this.cbIdepoisFecho.Location = new System.Drawing.Point(56, 110);
            this.cbIdepoisFecho.Name = "cbIdepoisFecho";
            this.cbIdepoisFecho.Size = new System.Drawing.Size(109, 15);
            this.cbIdepoisFecho.TabIndex = 3;
            this.cbIdepoisFecho.Text = "Depois do fecho";
            this.cbIdepoisFecho.UseSelectable = true;
            // 
            // cbIantesFe
            // 
            this.cbIantesFe.AutoSize = true;
            this.cbIantesFe.Location = new System.Drawing.Point(56, 89);
            this.cbIantesFe.Name = "cbIantesFe";
            this.cbIantesFe.Size = new System.Drawing.Size(105, 15);
            this.cbIantesFe.TabIndex = 2;
            this.cbIantesFe.Text = "Antes do Fecho";
            this.cbIantesFe.UseSelectable = true;
            // 
            // Artigo
            // 
            this.Artigo.Controls.Add(this.cbAitensAtivos);
            this.Artigo.Controls.Add(this.cbAlocali);
            this.Artigo.Controls.Add(this.cbAlistagemArt);
            this.Artigo.Controls.Add(this.cbApontoEncom);
            this.Artigo.Controls.Add(this.cbArotacaoStock);
            this.Artigo.Controls.Add(this.cbAsemQuant);
            this.Artigo.Controls.Add(this.cbAhistorMov);
            this.Artigo.Controls.Add(this.cbAexistencias);
            this.Artigo.Controls.Add(this.cbAetiqueta);
            this.Artigo.Controls.Add(this.cbAprecario);
            this.Artigo.HorizontalScrollbarBarColor = true;
            this.Artigo.HorizontalScrollbarHighlightOnWheel = false;
            this.Artigo.HorizontalScrollbarSize = 10;
            this.Artigo.Location = new System.Drawing.Point(4, 38);
            this.Artigo.Name = "Artigo";
            this.Artigo.Size = new System.Drawing.Size(1205, 377);
            this.Artigo.TabIndex = 2;
            this.Artigo.Text = "Artigo";
            this.Artigo.VerticalScrollbarBarColor = true;
            this.Artigo.VerticalScrollbarHighlightOnWheel = false;
            this.Artigo.VerticalScrollbarSize = 10;
            // 
            // cbAitensAtivos
            // 
            this.cbAitensAtivos.AutoSize = true;
            this.cbAitensAtivos.Location = new System.Drawing.Point(213, 165);
            this.cbAitensAtivos.Name = "cbAitensAtivos";
            this.cbAitensAtivos.Size = new System.Drawing.Size(138, 15);
            this.cbAitensAtivos.TabIndex = 11;
            this.cbAitensAtivos.Text = "Por Nr. de itens ativos";
            this.cbAitensAtivos.UseSelectable = true;
            // 
            // cbAlocali
            // 
            this.cbAlocali.AutoSize = true;
            this.cbAlocali.Location = new System.Drawing.Point(213, 143);
            this.cbAlocali.Name = "cbAlocali";
            this.cbAlocali.Size = new System.Drawing.Size(102, 15);
            this.cbAlocali.TabIndex = 10;
            this.cbAlocali.Text = "Por localização";
            this.cbAlocali.UseSelectable = true;
            // 
            // cbAlistagemArt
            // 
            this.cbAlistagemArt.AutoSize = true;
            this.cbAlistagemArt.Location = new System.Drawing.Point(213, 121);
            this.cbAlistagemArt.Name = "cbAlistagemArt";
            this.cbAlistagemArt.Size = new System.Drawing.Size(128, 15);
            this.cbAlistagemArt.TabIndex = 9;
            this.cbAlistagemArt.Text = "Listagem de Artigos";
            this.cbAlistagemArt.UseSelectable = true;
            // 
            // cbApontoEncom
            // 
            this.cbApontoEncom.AutoSize = true;
            this.cbApontoEncom.Location = new System.Drawing.Point(213, 99);
            this.cbApontoEncom.Name = "cbApontoEncom";
            this.cbApontoEncom.Size = new System.Drawing.Size(156, 15);
            this.cbApontoEncom.TabIndex = 8;
            this.cbApontoEncom.Text = "Ponto Encom./Stock Min";
            this.cbApontoEncom.UseSelectable = true;
            // 
            // cbArotacaoStock
            // 
            this.cbArotacaoStock.AutoSize = true;
            this.cbArotacaoStock.Location = new System.Drawing.Point(213, 77);
            this.cbArotacaoStock.Name = "cbArotacaoStock";
            this.cbArotacaoStock.Size = new System.Drawing.Size(114, 15);
            this.cbArotacaoStock.TabIndex = 7;
            this.cbArotacaoStock.Text = "Rotação de Stock";
            this.cbArotacaoStock.UseSelectable = true;
            // 
            // cbAsemQuant
            // 
            this.cbAsemQuant.AutoSize = true;
            this.cbAsemQuant.Location = new System.Drawing.Point(37, 165);
            this.cbAsemQuant.Name = "cbAsemQuant";
            this.cbAsemQuant.Size = new System.Drawing.Size(109, 15);
            this.cbAsemQuant.TabIndex = 6;
            this.cbAsemQuant.Text = "Sem quantidade";
            this.cbAsemQuant.UseSelectable = true;
            // 
            // cbAhistorMov
            // 
            this.cbAhistorMov.AutoSize = true;
            this.cbAhistorMov.Location = new System.Drawing.Point(37, 143);
            this.cbAhistorMov.Name = "cbAhistorMov";
            this.cbAhistorMov.Size = new System.Drawing.Size(143, 15);
            this.cbAhistorMov.TabIndex = 5;
            this.cbAhistorMov.Text = "Histórico-Movimentos";
            this.cbAhistorMov.UseSelectable = true;
            // 
            // cbAexistencias
            // 
            this.cbAexistencias.AutoSize = true;
            this.cbAexistencias.Location = new System.Drawing.Point(37, 121);
            this.cbAexistencias.Name = "cbAexistencias";
            this.cbAexistencias.Size = new System.Drawing.Size(79, 15);
            this.cbAexistencias.TabIndex = 4;
            this.cbAexistencias.Text = "Existências";
            this.cbAexistencias.UseSelectable = true;
            // 
            // cbAetiqueta
            // 
            this.cbAetiqueta.AutoSize = true;
            this.cbAetiqueta.Location = new System.Drawing.Point(37, 99);
            this.cbAetiqueta.Name = "cbAetiqueta";
            this.cbAetiqueta.Size = new System.Drawing.Size(71, 15);
            this.cbAetiqueta.TabIndex = 3;
            this.cbAetiqueta.Text = "Etiquetas";
            this.cbAetiqueta.UseSelectable = true;
            // 
            // cbAprecario
            // 
            this.cbAprecario.AutoSize = true;
            this.cbAprecario.Location = new System.Drawing.Point(37, 77);
            this.cbAprecario.Name = "cbAprecario";
            this.cbAprecario.Size = new System.Drawing.Size(66, 15);
            this.cbAprecario.TabIndex = 2;
            this.cbAprecario.Text = "Preçário";
            this.cbAprecario.UseSelectable = true;
            // 
            // Movimentos
            // 
            this.Movimentos.Controls.Add(this.cbMsaidas);
            this.Movimentos.Controls.Add(this.cbMentradas);
            this.Movimentos.Controls.Add(this.cbMentradaFornec);
            this.Movimentos.Controls.Add(this.cbMgeralArt);
            this.Movimentos.Controls.Add(this.cbMdetalheMov);
            this.Movimentos.HorizontalScrollbarBarColor = true;
            this.Movimentos.HorizontalScrollbarHighlightOnWheel = false;
            this.Movimentos.HorizontalScrollbarSize = 10;
            this.Movimentos.Location = new System.Drawing.Point(4, 38);
            this.Movimentos.Name = "Movimentos";
            this.Movimentos.Size = new System.Drawing.Size(1205, 377);
            this.Movimentos.TabIndex = 3;
            this.Movimentos.Text = "Movimentos";
            this.Movimentos.VerticalScrollbarBarColor = true;
            this.Movimentos.VerticalScrollbarHighlightOnWheel = false;
            this.Movimentos.VerticalScrollbarSize = 10;
            // 
            // cbMsaidas
            // 
            this.cbMsaidas.AutoSize = true;
            this.cbMsaidas.Location = new System.Drawing.Point(82, 173);
            this.cbMsaidas.Name = "cbMsaidas";
            this.cbMsaidas.Size = new System.Drawing.Size(76, 15);
            this.cbMsaidas.TabIndex = 6;
            this.cbMsaidas.Text = "Por saidas";
            this.cbMsaidas.UseSelectable = true;
            // 
            // cbMentradas
            // 
            this.cbMentradas.AutoSize = true;
            this.cbMentradas.Location = new System.Drawing.Point(82, 151);
            this.cbMentradas.Name = "cbMentradas";
            this.cbMentradas.Size = new System.Drawing.Size(89, 15);
            this.cbMentradas.TabIndex = 5;
            this.cbMentradas.Text = "Por entradas";
            this.cbMentradas.UseSelectable = true;
            // 
            // cbMentradaFornec
            // 
            this.cbMentradaFornec.AutoSize = true;
            this.cbMentradaFornec.Location = new System.Drawing.Point(82, 129);
            this.cbMentradaFornec.Name = "cbMentradaFornec";
            this.cbMentradaFornec.Size = new System.Drawing.Size(156, 15);
            this.cbMentradaFornec.TabIndex = 4;
            this.cbMentradaFornec.Text = "Entrada por fornecedores";
            this.cbMentradaFornec.UseSelectable = true;
            // 
            // cbMgeralArt
            // 
            this.cbMgeralArt.AutoSize = true;
            this.cbMgeralArt.Location = new System.Drawing.Point(82, 107);
            this.cbMgeralArt.Name = "cbMgeralArt";
            this.cbMgeralArt.Size = new System.Drawing.Size(148, 15);
            this.cbMgeralArt.TabIndex = 3;
            this.cbMgeralArt.Text = "Geral dos artigos / Docs";
            this.cbMgeralArt.UseSelectable = true;
            // 
            // cbMdetalheMov
            // 
            this.cbMdetalheMov.AutoSize = true;
            this.cbMdetalheMov.Location = new System.Drawing.Point(82, 85);
            this.cbMdetalheMov.Name = "cbMdetalheMov";
            this.cbMdetalheMov.Size = new System.Drawing.Size(249, 15);
            this.cbMdetalheMov.TabIndex = 2;
            this.cbMdetalheMov.Text = "Detalhe de movimentos  por artigos / Docs";
            this.cbMdetalheMov.UseSelectable = true;
            // 
            // MapaEstatico
            // 
            this.MapaEstatico.Controls.Add(this.cbMAartigoAno);
            this.MapaEstatico.Controls.Add(this.cbMAsaidaPano);
            this.MapaEstatico.Controls.Add(this.cbMAsaidasAno);
            this.MapaEstatico.Controls.Add(this.cbMmovimPorAno);
            this.MapaEstatico.Controls.Add(this.cbMmovimento);
            this.MapaEstatico.HorizontalScrollbarBarColor = true;
            this.MapaEstatico.HorizontalScrollbarHighlightOnWheel = false;
            this.MapaEstatico.HorizontalScrollbarSize = 10;
            this.MapaEstatico.Location = new System.Drawing.Point(4, 38);
            this.MapaEstatico.Name = "MapaEstatico";
            this.MapaEstatico.Size = new System.Drawing.Size(1205, 377);
            this.MapaEstatico.TabIndex = 4;
            this.MapaEstatico.Text = "Mapa Estático";
            this.MapaEstatico.VerticalScrollbarBarColor = true;
            this.MapaEstatico.VerticalScrollbarHighlightOnWheel = false;
            this.MapaEstatico.VerticalScrollbarSize = 10;
            // 
            // cbMAartigoAno
            // 
            this.cbMAartigoAno.AutoSize = true;
            this.cbMAartigoAno.Location = new System.Drawing.Point(70, 165);
            this.cbMAartigoAno.Name = "cbMAartigoAno";
            this.cbMAartigoAno.Size = new System.Drawing.Size(122, 15);
            this.cbMAartigoAno.TabIndex = 6;
            this.cbMAartigoAno.Text = "Dos artigos no ano";
            this.cbMAartigoAno.UseSelectable = true;
            // 
            // cbMAsaidaPano
            // 
            this.cbMAsaidaPano.AutoSize = true;
            this.cbMAsaidaPano.Location = new System.Drawing.Point(70, 144);
            this.cbMAsaidaPano.Name = "cbMAsaidaPano";
            this.cbMAsaidaPano.Size = new System.Drawing.Size(121, 15);
            this.cbMAsaidaPano.TabIndex = 5;
            this.cbMAsaidaPano.Text = "Das saidas por ano";
            this.cbMAsaidaPano.UseSelectable = true;
            // 
            // cbMAsaidasAno
            // 
            this.cbMAsaidasAno.AutoSize = true;
            this.cbMAsaidasAno.Location = new System.Drawing.Point(70, 123);
            this.cbMAsaidasAno.Name = "cbMAsaidasAno";
            this.cbMAsaidasAno.Size = new System.Drawing.Size(117, 15);
            this.cbMAsaidasAno.TabIndex = 4;
            this.cbMAsaidasAno.Text = "Das saidas do ano";
            this.cbMAsaidasAno.UseSelectable = true;
            // 
            // cbMmovimPorAno
            // 
            this.cbMmovimPorAno.AutoSize = true;
            this.cbMmovimPorAno.Location = new System.Drawing.Point(70, 102);
            this.cbMmovimPorAno.Name = "cbMmovimPorAno";
            this.cbMmovimPorAno.Size = new System.Drawing.Size(157, 15);
            this.cbMmovimPorAno.TabIndex = 3;
            this.cbMmovimPorAno.Text = "Dos movimentos por ano";
            this.cbMmovimPorAno.UseSelectable = true;
            // 
            // cbMmovimento
            // 
            this.cbMmovimento.AutoSize = true;
            this.cbMmovimento.Location = new System.Drawing.Point(70, 80);
            this.cbMmovimento.Name = "cbMmovimento";
            this.cbMmovimento.Size = new System.Drawing.Size(153, 15);
            this.cbMmovimento.TabIndex = 2;
            this.cbMmovimento.Text = "Dos movimentos do ano";
            this.cbMmovimento.UseSelectable = true;
            // 
            // Tabelas
            // 
            this.Tabelas.Controls.Add(this.cbTAfamilia);
            this.Tabelas.Controls.Add(this.cbMAsubFam);
            this.Tabelas.Controls.Add(this.cbTAmerc);
            this.Tabelas.Controls.Add(this.cbTAloja);
            this.Tabelas.HorizontalScrollbarBarColor = true;
            this.Tabelas.HorizontalScrollbarHighlightOnWheel = false;
            this.Tabelas.HorizontalScrollbarSize = 10;
            this.Tabelas.Location = new System.Drawing.Point(4, 38);
            this.Tabelas.Name = "Tabelas";
            this.Tabelas.Size = new System.Drawing.Size(1205, 377);
            this.Tabelas.TabIndex = 5;
            this.Tabelas.Text = "Tabelas";
            this.Tabelas.VerticalScrollbarBarColor = true;
            this.Tabelas.VerticalScrollbarHighlightOnWheel = false;
            this.Tabelas.VerticalScrollbarSize = 10;
            // 
            // cbTAfamilia
            // 
            this.cbTAfamilia.AutoSize = true;
            this.cbTAfamilia.Location = new System.Drawing.Point(442, 98);
            this.cbTAfamilia.Name = "cbTAfamilia";
            this.cbTAfamilia.Size = new System.Drawing.Size(61, 15);
            this.cbTAfamilia.TabIndex = 5;
            this.cbTAfamilia.Text = "Familia";
            this.cbTAfamilia.UseSelectable = true;
            // 
            // cbMAsubFam
            // 
            this.cbMAsubFam.AutoSize = true;
            this.cbMAsubFam.Location = new System.Drawing.Point(442, 121);
            this.cbMAsubFam.Name = "cbMAsubFam";
            this.cbMAsubFam.Size = new System.Drawing.Size(86, 15);
            this.cbMAsubFam.TabIndex = 4;
            this.cbMAsubFam.Text = "Sub-Familia";
            this.cbMAsubFam.UseSelectable = true;
            // 
            // cbTAmerc
            // 
            this.cbTAmerc.AutoSize = true;
            this.cbTAmerc.Location = new System.Drawing.Point(273, 120);
            this.cbTAmerc.Name = "cbTAmerc";
            this.cbTAmerc.Size = new System.Drawing.Size(83, 15);
            this.cbTAmerc.TabIndex = 3;
            this.cbTAmerc.Text = "Mercadoria";
            this.cbTAmerc.UseSelectable = true;
            // 
            // cbTAloja
            // 
            this.cbTAloja.AutoSize = true;
            this.cbTAloja.Location = new System.Drawing.Point(273, 98);
            this.cbTAloja.Name = "cbTAloja";
            this.cbTAloja.Size = new System.Drawing.Size(45, 15);
            this.cbTAloja.TabIndex = 2;
            this.cbTAloja.Text = "Loja";
            this.cbTAloja.UseSelectable = true;
            // 
            // ReemissãoGuias
            // 
            this.ReemissãoGuias.Controls.Add(this.cbTAentrMerc);
            this.ReemissãoGuias.Controls.Add(this.cbTAguiasTransp);
            this.ReemissãoGuias.Controls.Add(this.cbTAentrSaidAjus);
            this.ReemissãoGuias.Controls.Add(this.cbTAsaidaReq);
            this.ReemissãoGuias.Controls.Add(this.cbTAsaidaTrans);
            this.ReemissãoGuias.Controls.Add(this.cbTAentradaMerc);
            this.ReemissãoGuias.HorizontalScrollbarBarColor = true;
            this.ReemissãoGuias.HorizontalScrollbarHighlightOnWheel = false;
            this.ReemissãoGuias.HorizontalScrollbarSize = 10;
            this.ReemissãoGuias.Location = new System.Drawing.Point(4, 38);
            this.ReemissãoGuias.Name = "ReemissãoGuias";
            this.ReemissãoGuias.Size = new System.Drawing.Size(1205, 377);
            this.ReemissãoGuias.TabIndex = 6;
            this.ReemissãoGuias.Text = "Reemissão de guias";
            this.ReemissãoGuias.VerticalScrollbarBarColor = true;
            this.ReemissãoGuias.VerticalScrollbarHighlightOnWheel = false;
            this.ReemissãoGuias.VerticalScrollbarSize = 10;
            // 
            // cbTAentrMerc
            // 
            this.cbTAentrMerc.AutoSize = true;
            this.cbTAentrMerc.Location = new System.Drawing.Point(87, 202);
            this.cbTAentrMerc.Name = "cbTAentrMerc";
            this.cbTAentrMerc.Size = new System.Drawing.Size(189, 15);
            this.cbTAentrMerc.TabIndex = 7;
            this.cbTAentrMerc.Text = "Entrada de Mercadoria por guia";
            this.cbTAentrMerc.UseSelectable = true;
            // 
            // cbTAguiasTransp
            // 
            this.cbTAguiasTransp.AutoSize = true;
            this.cbTAguiasTransp.Location = new System.Drawing.Point(87, 180);
            this.cbTAguiasTransp.Name = "cbTAguiasTransp";
            this.cbTAguiasTransp.Size = new System.Drawing.Size(125, 15);
            this.cbTAguiasTransp.TabIndex = 6;
            this.cbTAguiasTransp.Text = "Guias de transporte";
            this.cbTAguiasTransp.UseSelectable = true;
            // 
            // cbTAentrSaidAjus
            // 
            this.cbTAentrSaidAjus.AutoSize = true;
            this.cbTAentrSaidAjus.Location = new System.Drawing.Point(87, 158);
            this.cbTAentrSaidAjus.Name = "cbTAentrSaidAjus";
            this.cbTAentrSaidAjus.Size = new System.Drawing.Size(156, 15);
            this.cbTAentrSaidAjus.TabIndex = 5;
            this.cbTAentrSaidAjus.Text = "Entradas/Saidas de ajuste";
            this.cbTAentrSaidAjus.UseSelectable = true;
            // 
            // cbTAsaidaReq
            // 
            this.cbTAsaidaReq.AutoSize = true;
            this.cbTAsaidaReq.Location = new System.Drawing.Point(87, 136);
            this.cbTAsaidaReq.Name = "cbTAsaidaReq";
            this.cbTAsaidaReq.Size = new System.Drawing.Size(132, 15);
            this.cbTAsaidaReq.TabIndex = 4;
            this.cbTAsaidaReq.Text = "Saída por Requisiçao";
            this.cbTAsaidaReq.UseSelectable = true;
            // 
            // cbTAsaidaTrans
            // 
            this.cbTAsaidaTrans.AutoSize = true;
            this.cbTAsaidaTrans.Location = new System.Drawing.Point(87, 115);
            this.cbTAsaidaTrans.Name = "cbTAsaidaTrans";
            this.cbTAsaidaTrans.Size = new System.Drawing.Size(143, 15);
            this.cbTAsaidaTrans.TabIndex = 3;
            this.cbTAsaidaTrans.Text = "Saída por transferencia";
            this.cbTAsaidaTrans.UseSelectable = true;
            // 
            // cbTAentradaMerc
            // 
            this.cbTAentradaMerc.AutoSize = true;
            this.cbTAentradaMerc.Location = new System.Drawing.Point(87, 94);
            this.cbTAentradaMerc.Name = "cbTAentradaMerc";
            this.cbTAentradaMerc.Size = new System.Drawing.Size(142, 15);
            this.cbTAentradaMerc.TabIndex = 2;
            this.cbTAentradaMerc.Text = "Entrada de Mercadoria";
            this.cbTAentradaMerc.UseSelectable = true;
            // 
            // ValoresStock
            // 
            this.ValoresStock.Controls.Add(this.metroLabel15);
            this.ValoresStock.Controls.Add(this.Loja);
            this.ValoresStock.Controls.Add(this.metroButton6);
            this.ValoresStock.Controls.Add(this.cmbVmercador);
            this.ValoresStock.Controls.Add(this.cmbVloja);
            this.ValoresStock.Controls.Add(this.txtRVDataExist);
            this.ValoresStock.Controls.Add(this.metroRadioButton1);
            this.ValoresStock.Controls.Add(this.Geral);
            this.ValoresStock.Controls.Add(this.metroLabel1);
            this.ValoresStock.HorizontalScrollbarBarColor = true;
            this.ValoresStock.HorizontalScrollbarHighlightOnWheel = false;
            this.ValoresStock.HorizontalScrollbarSize = 10;
            this.ValoresStock.Location = new System.Drawing.Point(4, 38);
            this.ValoresStock.Name = "ValoresStock";
            this.ValoresStock.Size = new System.Drawing.Size(1205, 377);
            this.ValoresStock.TabIndex = 7;
            this.ValoresStock.Text = "Valores em Stock";
            this.ValoresStock.VerticalScrollbarBarColor = true;
            this.ValoresStock.VerticalScrollbarHighlightOnWheel = false;
            this.ValoresStock.VerticalScrollbarSize = 10;
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(256, 241);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(67, 19);
            this.metroLabel15.TabIndex = 10;
            this.metroLabel15.Text = "Mercador";
            // 
            // Loja
            // 
            this.Loja.Location = new System.Drawing.Point(0, 0);
            this.Loja.Name = "Loja";
            this.Loja.Size = new System.Drawing.Size(100, 23);
            this.Loja.TabIndex = 11;
            // 
            // metroButton6
            // 
            this.metroButton6.Location = new System.Drawing.Point(432, 300);
            this.metroButton6.Name = "metroButton6";
            this.metroButton6.Size = new System.Drawing.Size(75, 23);
            this.metroButton6.TabIndex = 8;
            this.metroButton6.Text = "Imprimir";
            this.metroButton6.UseSelectable = true;
            // 
            // cmbVmercador
            // 
            this.cmbVmercador.FormattingEnabled = true;
            this.cmbVmercador.Location = new System.Drawing.Point(338, 241);
            this.cmbVmercador.Name = "cmbVmercador";
            this.cmbVmercador.Size = new System.Drawing.Size(253, 21);
            this.cmbVmercador.TabIndex = 7;
            // 
            // cmbVloja
            // 
            this.cmbVloja.FormattingEnabled = true;
            this.cmbVloja.Location = new System.Drawing.Point(338, 213);
            this.cmbVloja.Name = "cmbVloja";
            this.cmbVloja.Size = new System.Drawing.Size(253, 21);
            this.cmbVloja.TabIndex = 6;
            // 
            // txtRVDataExist
            // 
            this.txtRVDataExist.Location = new System.Drawing.Point(380, 120);
            this.txtRVDataExist.Name = "txtRVDataExist";
            this.txtRVDataExist.Size = new System.Drawing.Size(114, 20);
            this.txtRVDataExist.TabIndex = 5;
            // 
            // metroRadioButton1
            // 
            this.metroRadioButton1.AutoSize = true;
            this.metroRadioButton1.Location = new System.Drawing.Point(338, 191);
            this.metroRadioButton1.Name = "metroRadioButton1";
            this.metroRadioButton1.Size = new System.Drawing.Size(58, 15);
            this.metroRadioButton1.TabIndex = 4;
            this.metroRadioButton1.Text = "Parcial";
            this.metroRadioButton1.UseSelectable = true;
            // 
            // Geral
            // 
            this.Geral.AutoSize = true;
            this.Geral.Location = new System.Drawing.Point(338, 161);
            this.Geral.Name = "Geral";
            this.Geral.Size = new System.Drawing.Size(50, 15);
            this.Geral.TabIndex = 3;
            this.Geral.Text = "Geral";
            this.Geral.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(380, 78);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(114, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Data da existência";
            // 
            // EntArtigos
            // 
            this.EntArtigos.Controls.Add(this.btEimprimir);
            this.EntArtigos.Controls.Add(this.txtEdataFinal);
            this.EntArtigos.Controls.Add(this.txtEdata);
            this.EntArtigos.Controls.Add(this.metroLabel7);
            this.EntArtigos.Controls.Add(this.metroLabel6);
            this.EntArtigos.Controls.Add(this.metroLabel5);
            this.EntArtigos.Controls.Add(this.metroLabel4);
            this.EntArtigos.Controls.Add(this.metroLabel3);
            this.EntArtigos.Controls.Add(this.metroLabel2);
            this.EntArtigos.Controls.Add(this.cmbEdescricao);
            this.EntArtigos.Controls.Add(this.cmbEreferencia);
            this.EntArtigos.Controls.Add(this.cmbEmercadoria);
            this.EntArtigos.Controls.Add(this.cmbEloja);
            this.EntArtigos.HorizontalScrollbarBarColor = true;
            this.EntArtigos.HorizontalScrollbarHighlightOnWheel = false;
            this.EntArtigos.HorizontalScrollbarSize = 10;
            this.EntArtigos.Location = new System.Drawing.Point(4, 38);
            this.EntArtigos.Name = "EntArtigos";
            this.EntArtigos.Size = new System.Drawing.Size(1205, 377);
            this.EntArtigos.TabIndex = 8;
            this.EntArtigos.Text = "Ent/Transf. Artigos";
            this.EntArtigos.VerticalScrollbarBarColor = true;
            this.EntArtigos.VerticalScrollbarHighlightOnWheel = false;
            this.EntArtigos.VerticalScrollbarSize = 10;
            // 
            // btEimprimir
            // 
            this.btEimprimir.Location = new System.Drawing.Point(386, 345);
            this.btEimprimir.Name = "btEimprimir";
            this.btEimprimir.Size = new System.Drawing.Size(75, 23);
            this.btEimprimir.TabIndex = 14;
            this.btEimprimir.Text = "Imprimir";
            this.btEimprimir.UseSelectable = true;
            // 
            // txtEdataFinal
            // 
            this.txtEdataFinal.Location = new System.Drawing.Point(490, 299);
            this.txtEdataFinal.Name = "txtEdataFinal";
            this.txtEdataFinal.Size = new System.Drawing.Size(100, 20);
            this.txtEdataFinal.TabIndex = 13;
            // 
            // txtEdata
            // 
            this.txtEdata.Location = new System.Drawing.Point(362, 298);
            this.txtEdata.Name = "txtEdata";
            this.txtEdata.Size = new System.Drawing.Size(100, 20);
            this.txtEdata.TabIndex = 12;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(369, 154);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(70, 19);
            this.metroLabel7.TabIndex = 11;
            this.metroLabel7.Text = "Referência";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(369, 215);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(65, 19);
            this.metroLabel6.TabIndex = 10;
            this.metroLabel6.Text = "Descrição";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(468, 299);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(16, 19);
            this.metroLabel5.TabIndex = 9;
            this.metroLabel5.Text = "a";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(320, 299);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(36, 19);
            this.metroLabel4.TabIndex = 8;
            this.metroLabel4.Text = "Data";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(369, 106);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(77, 19);
            this.metroLabel3.TabIndex = 7;
            this.metroLabel3.Text = "Mercadoria";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(369, 48);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(33, 19);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Loja";
            // 
            // cmbEdescricao
            // 
            this.cmbEdescricao.FormattingEnabled = true;
            this.cmbEdescricao.Location = new System.Drawing.Point(369, 237);
            this.cmbEdescricao.Name = "cmbEdescricao";
            this.cmbEdescricao.Size = new System.Drawing.Size(190, 21);
            this.cmbEdescricao.TabIndex = 5;
            // 
            // cmbEreferencia
            // 
            this.cmbEreferencia.FormattingEnabled = true;
            this.cmbEreferencia.Location = new System.Drawing.Point(369, 177);
            this.cmbEreferencia.Name = "cmbEreferencia";
            this.cmbEreferencia.Size = new System.Drawing.Size(190, 21);
            this.cmbEreferencia.TabIndex = 4;
            // 
            // cmbEmercadoria
            // 
            this.cmbEmercadoria.FormattingEnabled = true;
            this.cmbEmercadoria.Location = new System.Drawing.Point(369, 128);
            this.cmbEmercadoria.Name = "cmbEmercadoria";
            this.cmbEmercadoria.Size = new System.Drawing.Size(190, 21);
            this.cmbEmercadoria.TabIndex = 3;
            // 
            // cmbEloja
            // 
            this.cmbEloja.FormattingEnabled = true;
            this.cmbEloja.Location = new System.Drawing.Point(369, 70);
            this.cmbEloja.Name = "cmbEloja";
            this.cmbEloja.Size = new System.Drawing.Size(190, 21);
            this.cmbEloja.TabIndex = 2;
            // 
            // FolhaEspelho
            // 
            this.FolhaEspelho.Controls.Add(this.metroLabel8);
            this.FolhaEspelho.Controls.Add(this.txtEnumProf);
            this.FolhaEspelho.Controls.Add(this.btFimprimir);
            this.FolhaEspelho.HorizontalScrollbarBarColor = true;
            this.FolhaEspelho.HorizontalScrollbarHighlightOnWheel = false;
            this.FolhaEspelho.HorizontalScrollbarSize = 10;
            this.FolhaEspelho.Location = new System.Drawing.Point(4, 38);
            this.FolhaEspelho.Name = "FolhaEspelho";
            this.FolhaEspelho.Size = new System.Drawing.Size(1205, 377);
            this.FolhaEspelho.TabIndex = 9;
            this.FolhaEspelho.Text = "Folha espelho";
            this.FolhaEspelho.VerticalScrollbarBarColor = true;
            this.FolhaEspelho.VerticalScrollbarHighlightOnWheel = false;
            this.FolhaEspelho.VerticalScrollbarSize = 10;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(171, 118);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(138, 19);
            this.metroLabel8.TabIndex = 4;
            this.metroLabel8.Text = "Numero da proforma";
            // 
            // txtEnumProf
            // 
            this.txtEnumProf.Lines = new string[0];
            this.txtEnumProf.Location = new System.Drawing.Point(315, 118);
            this.txtEnumProf.MaxLength = 32767;
            this.txtEnumProf.Name = "txtEnumProf";
            this.txtEnumProf.PasswordChar = '\0';
            this.txtEnumProf.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEnumProf.SelectedText = "";
            this.txtEnumProf.Size = new System.Drawing.Size(208, 23);
            this.txtEnumProf.TabIndex = 3;
            this.txtEnumProf.UseSelectable = true;
            // 
            // btFimprimir
            // 
            this.btFimprimir.Location = new System.Drawing.Point(407, 243);
            this.btFimprimir.Name = "btFimprimir";
            this.btFimprimir.Size = new System.Drawing.Size(75, 23);
            this.btFimprimir.TabIndex = 2;
            this.btFimprimir.Text = "Imprimir";
            this.btFimprimir.UseSelectable = true;
            // 
            // TransfPendentes
            // 
            this.TransfPendentes.Controls.Add(this.btTimprimir);
            this.TransfPendentes.Controls.Add(this.cmbFlojaDest);
            this.TransfPendentes.Controls.Add(this.metroLabel9);
            this.TransfPendentes.HorizontalScrollbarBarColor = true;
            this.TransfPendentes.HorizontalScrollbarHighlightOnWheel = false;
            this.TransfPendentes.HorizontalScrollbarSize = 10;
            this.TransfPendentes.Location = new System.Drawing.Point(4, 38);
            this.TransfPendentes.Name = "TransfPendentes";
            this.TransfPendentes.Size = new System.Drawing.Size(1205, 377);
            this.TransfPendentes.TabIndex = 10;
            this.TransfPendentes.Text = "Transf. Pendentes";
            this.TransfPendentes.VerticalScrollbarBarColor = true;
            this.TransfPendentes.VerticalScrollbarHighlightOnWheel = false;
            this.TransfPendentes.VerticalScrollbarSize = 10;
            // 
            // btTimprimir
            // 
            this.btTimprimir.Location = new System.Drawing.Point(424, 187);
            this.btTimprimir.Name = "btTimprimir";
            this.btTimprimir.Size = new System.Drawing.Size(75, 23);
            this.btTimprimir.TabIndex = 4;
            this.btTimprimir.Text = "Imprimir";
            this.btTimprimir.UseSelectable = true;
            // 
            // cmbFlojaDest
            // 
            this.cmbFlojaDest.FormattingEnabled = true;
            this.cmbFlojaDest.ItemHeight = 23;
            this.cmbFlojaDest.Location = new System.Drawing.Point(296, 111);
            this.cmbFlojaDest.Name = "cmbFlojaDest";
            this.cmbFlojaDest.Size = new System.Drawing.Size(329, 29);
            this.cmbFlojaDest.TabIndex = 3;
            this.cmbFlojaDest.UseSelectable = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(424, 89);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(79, 19);
            this.metroLabel9.TabIndex = 2;
            this.metroLabel9.Text = "Loja destino";
            // 
            // CurvaAbc
            // 
            this.CurvaAbc.Controls.Add(this.cbCexpExSaid);
            this.CurvaAbc.Controls.Add(this.cbCexportEXvend);
            this.CurvaAbc.Controls.Add(this.cbCvendasArtSmov);
            this.CurvaAbc.Controls.Add(this.cbCvendas);
            this.CurvaAbc.Controls.Add(this.cbCsaidaArt);
            this.CurvaAbc.HorizontalScrollbarBarColor = true;
            this.CurvaAbc.HorizontalScrollbarHighlightOnWheel = false;
            this.CurvaAbc.HorizontalScrollbarSize = 10;
            this.CurvaAbc.Location = new System.Drawing.Point(4, 38);
            this.CurvaAbc.Name = "CurvaAbc";
            this.CurvaAbc.Size = new System.Drawing.Size(1205, 377);
            this.CurvaAbc.TabIndex = 11;
            this.CurvaAbc.Text = "Curva ABC";
            this.CurvaAbc.VerticalScrollbarBarColor = true;
            this.CurvaAbc.VerticalScrollbarHighlightOnWheel = false;
            this.CurvaAbc.VerticalScrollbarSize = 10;
            // 
            // cbCexpExSaid
            // 
            this.cbCexpExSaid.AutoSize = true;
            this.cbCexpExSaid.Location = new System.Drawing.Point(135, 196);
            this.cbCexpExSaid.Name = "cbCexpExSaid";
            this.cbCexpExSaid.Size = new System.Drawing.Size(171, 15);
            this.cbCexpExSaid.TabIndex = 6;
            this.cbCexpExSaid.Text = "Exportaçao para Excel saidas";
            this.cbCexpExSaid.UseSelectable = true;
            // 
            // cbCexportEXvend
            // 
            this.cbCexportEXvend.AutoSize = true;
            this.cbCexportEXvend.Location = new System.Drawing.Point(135, 174);
            this.cbCexportEXvend.Name = "cbCexportEXvend";
            this.cbCexportEXvend.Size = new System.Drawing.Size(176, 15);
            this.cbCexportEXvend.TabIndex = 5;
            this.cbCexportEXvend.Text = "Exportaçao para Excel vendas";
            this.cbCexportEXvend.UseSelectable = true;
            // 
            // cbCvendasArtSmov
            // 
            this.cbCvendasArtSmov.AutoSize = true;
            this.cbCvendasArtSmov.Location = new System.Drawing.Point(135, 152);
            this.cbCvendasArtSmov.Name = "cbCvendasArtSmov";
            this.cbCvendasArtSmov.Size = new System.Drawing.Size(211, 15);
            this.cbCvendasArtSmov.TabIndex = 4;
            this.cbCvendasArtSmov.Text = "Vendas do artigo e sem movimento";
            this.cbCvendasArtSmov.UseSelectable = true;
            // 
            // cbCvendas
            // 
            this.cbCvendas.AutoSize = true;
            this.cbCvendas.Location = new System.Drawing.Point(135, 130);
            this.cbCvendas.Name = "cbCvendas";
            this.cbCvendas.Size = new System.Drawing.Size(112, 15);
            this.cbCvendas.TabIndex = 3;
            this.cbCvendas.Text = "Vendas do artigo";
            this.cbCvendas.UseSelectable = true;
            // 
            // cbCsaidaArt
            // 
            this.cbCsaidaArt.AutoSize = true;
            this.cbCsaidaArt.Location = new System.Drawing.Point(135, 108);
            this.cbCsaidaArt.Name = "cbCsaidaArt";
            this.cbCsaidaArt.Size = new System.Drawing.Size(109, 15);
            this.cbCsaidaArt.TabIndex = 2;
            this.cbCsaidaArt.Text = "Saídas do Artigo";
            this.cbCsaidaArt.UseSelectable = true;
            // 
            // ApoioGestão
            // 
            this.ApoioGestão.Controls.Add(this.cbAGsituEnc);
            this.ApoioGestão.Controls.Add(this.cbAGriAteZero);
            this.ApoioGestão.Controls.Add(this.cbAGriPend);
            this.ApoioGestão.Controls.Add(this.cbAGriAtend);
            this.ApoioGestão.Controls.Add(this.cbAGriEstad);
            this.ApoioGestão.Controls.Add(this.cbAGsugestEnc);
            this.ApoioGestão.Controls.Add(this.cbAGstockTem);
            this.ApoioGestão.Controls.Add(this.cbAGstockGeral);
            this.ApoioGestão.Controls.Add(this.cbAGstockZero);
            this.ApoioGestão.HorizontalScrollbarBarColor = true;
            this.ApoioGestão.HorizontalScrollbarHighlightOnWheel = false;
            this.ApoioGestão.HorizontalScrollbarSize = 10;
            this.ApoioGestão.Location = new System.Drawing.Point(4, 38);
            this.ApoioGestão.Name = "ApoioGestão";
            this.ApoioGestão.Size = new System.Drawing.Size(1205, 377);
            this.ApoioGestão.TabIndex = 12;
            this.ApoioGestão.Text = "Apoio a Gestão";
            this.ApoioGestão.VerticalScrollbarBarColor = true;
            this.ApoioGestão.VerticalScrollbarHighlightOnWheel = false;
            this.ApoioGestão.VerticalScrollbarSize = 10;
            // 
            // cbAGsituEnc
            // 
            this.cbAGsituEnc.AutoSize = true;
            this.cbAGsituEnc.Location = new System.Drawing.Point(361, 161);
            this.cbAGsituEnc.Name = "cbAGsituEnc";
            this.cbAGsituEnc.Size = new System.Drawing.Size(160, 15);
            this.cbAGsituEnc.TabIndex = 10;
            this.cbAGsituEnc.Text = "Situação das encomendas";
            this.cbAGsituEnc.UseSelectable = true;
            // 
            // cbAGriAteZero
            // 
            this.cbAGriAteZero.AutoSize = true;
            this.cbAGriAteZero.Location = new System.Drawing.Point(361, 139);
            this.cbAGriAteZero.Name = "cbAGriAteZero";
            this.cbAGriAteZero.Size = new System.Drawing.Size(143, 15);
            this.cbAGriAteZero.TabIndex = 9;
            this.cbAGriAteZero.Text = "R.I Atendimento a zero";
            this.cbAGriAteZero.UseSelectable = true;
            // 
            // cbAGriPend
            // 
            this.cbAGriPend.AutoSize = true;
            this.cbAGriPend.Location = new System.Drawing.Point(361, 95);
            this.cbAGriPend.Name = "cbAGriPend";
            this.cbAGriPend.Size = new System.Drawing.Size(89, 15);
            this.cbAGriPend.TabIndex = 8;
            this.cbAGriPend.Text = "R.I Pendente";
            this.cbAGriPend.UseSelectable = true;
            // 
            // cbAGriAtend
            // 
            this.cbAGriAtend.AutoSize = true;
            this.cbAGriAtend.Location = new System.Drawing.Point(361, 116);
            this.cbAGriAtend.Name = "cbAGriAtend";
            this.cbAGriAtend.Size = new System.Drawing.Size(102, 15);
            this.cbAGriAtend.TabIndex = 7;
            this.cbAGriAtend.Text = "R.I Atedimento";
            this.cbAGriAtend.UseSelectable = true;
            // 
            // cbAGriEstad
            // 
            this.cbAGriEstad.AutoSize = true;
            this.cbAGriEstad.Location = new System.Drawing.Point(187, 183);
            this.cbAGriEstad.Name = "cbAGriEstad";
            this.cbAGriEstad.Size = new System.Drawing.Size(74, 15);
            this.cbAGriEstad.TabIndex = 6;
            this.cbAGriEstad.Text = "R.I Estado";
            this.cbAGriEstad.UseSelectable = true;
            // 
            // cbAGsugestEnc
            // 
            this.cbAGsugestEnc.AutoSize = true;
            this.cbAGsugestEnc.Location = new System.Drawing.Point(187, 161);
            this.cbAGsugestEnc.Name = "cbAGsugestEnc";
            this.cbAGsugestEnc.Size = new System.Drawing.Size(158, 15);
            this.cbAGsugestEnc.TabIndex = 5;
            this.cbAGsugestEnc.Text = "Sugestão de encomendas";
            this.cbAGsugestEnc.UseSelectable = true;
            // 
            // cbAGstockTem
            // 
            this.cbAGstockTem.AutoSize = true;
            this.cbAGstockTem.Location = new System.Drawing.Point(187, 139);
            this.cbAGstockTem.Name = "cbAGstockTem";
            this.cbAGstockTem.Size = new System.Drawing.Size(160, 15);
            this.cbAGstockTem.TabIndex = 4;
            this.cbAGstockTem.Text = "Stock tempo de reposiçao";
            this.cbAGstockTem.UseSelectable = true;
            // 
            // cbAGstockGeral
            // 
            this.cbAGstockGeral.AutoSize = true;
            this.cbAGstockGeral.Location = new System.Drawing.Point(187, 117);
            this.cbAGstockGeral.Name = "cbAGstockGeral";
            this.cbAGstockGeral.Size = new System.Drawing.Size(115, 15);
            this.cbAGstockGeral.TabIndex = 3;
            this.cbAGstockGeral.Text = "Stock geral a zero";
            this.cbAGstockGeral.UseSelectable = true;
            // 
            // cbAGstockZero
            // 
            this.cbAGstockZero.AutoSize = true;
            this.cbAGstockZero.Location = new System.Drawing.Point(187, 95);
            this.cbAGstockZero.Name = "cbAGstockZero";
            this.cbAGstockZero.Size = new System.Drawing.Size(86, 15);
            this.cbAGstockZero.TabIndex = 2;
            this.cbAGstockZero.Text = "Stock a zero";
            this.cbAGstockZero.UseSelectable = true;
            // 
            // MapaComparativo
            // 
            this.MapaComparativo.Controls.Add(this.metroLabel13);
            this.MapaComparativo.Controls.Add(this.metroLabel12);
            this.MapaComparativo.Controls.Add(this.metroLabel11);
            this.MapaComparativo.Controls.Add(this.metroLabel10);
            this.MapaComparativo.Controls.Add(this.cmbMloja);
            this.MapaComparativo.Controls.Add(this.cmbMlojaNexis);
            this.MapaComparativo.Controls.Add(this.btMimprimir);
            this.MapaComparativo.HorizontalScrollbarBarColor = true;
            this.MapaComparativo.HorizontalScrollbarHighlightOnWheel = false;
            this.MapaComparativo.HorizontalScrollbarSize = 10;
            this.MapaComparativo.Location = new System.Drawing.Point(4, 38);
            this.MapaComparativo.Name = "MapaComparativo";
            this.MapaComparativo.Size = new System.Drawing.Size(1205, 377);
            this.MapaComparativo.TabIndex = 13;
            this.MapaComparativo.Text = "Mapa comparativo";
            this.MapaComparativo.VerticalScrollbarBarColor = true;
            this.MapaComparativo.VerticalScrollbarHighlightOnWheel = false;
            this.MapaComparativo.VerticalScrollbarSize = 10;
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(491, 185);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(151, 19);
            this.metroLabel13.TabIndex = 8;
            this.metroLabel13.Text = "Artigos que nao existem";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(491, 81);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(125, 19);
            this.metroLabel12.TabIndex = 7;
            this.metroLabel12.Text = "Artigos que existem";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(440, 228);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(33, 19);
            this.metroLabel11.TabIndex = 6;
            this.metroLabel11.Text = "Loja";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(440, 127);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(33, 19);
            this.metroLabel10.TabIndex = 5;
            this.metroLabel10.Text = "Loja";
            // 
            // cmbMloja
            // 
            this.cmbMloja.FormattingEnabled = true;
            this.cmbMloja.ItemHeight = 23;
            this.cmbMloja.Location = new System.Drawing.Point(491, 117);
            this.cmbMloja.Name = "cmbMloja";
            this.cmbMloja.Size = new System.Drawing.Size(289, 29);
            this.cmbMloja.TabIndex = 4;
            this.cmbMloja.UseSelectable = true;
            // 
            // cmbMlojaNexis
            // 
            this.cmbMlojaNexis.FormattingEnabled = true;
            this.cmbMlojaNexis.ItemHeight = 23;
            this.cmbMlojaNexis.Location = new System.Drawing.Point(491, 218);
            this.cmbMlojaNexis.Name = "cmbMlojaNexis";
            this.cmbMlojaNexis.Size = new System.Drawing.Size(289, 29);
            this.cmbMlojaNexis.TabIndex = 3;
            this.cmbMlojaNexis.UseSelectable = true;
            // 
            // btMimprimir
            // 
            this.btMimprimir.Location = new System.Drawing.Point(601, 280);
            this.btMimprimir.Name = "btMimprimir";
            this.btMimprimir.Size = new System.Drawing.Size(75, 23);
            this.btMimprimir.TabIndex = 2;
            this.btMimprimir.Text = "Imprimir";
            this.btMimprimir.UseSelectable = true;
            // 
            // Graficos
            // 
            this.Graficos.Controls.Add(this.cmbGgraficosArm);
            this.Graficos.Controls.Add(this.cmbGgraficosLj);
            this.Graficos.Controls.Add(this.cmbGgraficosArt);
            this.Graficos.Controls.Add(this.cmbGgraficosOp);
            this.Graficos.HorizontalScrollbarBarColor = true;
            this.Graficos.HorizontalScrollbarHighlightOnWheel = false;
            this.Graficos.HorizontalScrollbarSize = 10;
            this.Graficos.Location = new System.Drawing.Point(4, 38);
            this.Graficos.Name = "Graficos";
            this.Graficos.Size = new System.Drawing.Size(1205, 377);
            this.Graficos.TabIndex = 14;
            this.Graficos.Text = "Graficos";
            this.Graficos.VerticalScrollbarBarColor = true;
            this.Graficos.VerticalScrollbarHighlightOnWheel = false;
            this.Graficos.VerticalScrollbarSize = 10;
            // 
            // cmbGgraficosArm
            // 
            this.cmbGgraficosArm.AutoSize = true;
            this.cmbGgraficosArm.Location = new System.Drawing.Point(359, 204);
            this.cmbGgraficosArm.Name = "cmbGgraficosArm";
            this.cmbGgraficosArm.Size = new System.Drawing.Size(137, 15);
            this.cmbGgraficosArm.TabIndex = 5;
            this.cmbGgraficosArm.Text = "Gráficos de Armazens";
            this.cmbGgraficosArm.UseSelectable = true;
            // 
            // cmbGgraficosLj
            // 
            this.cmbGgraficosLj.AutoSize = true;
            this.cmbGgraficosLj.Location = new System.Drawing.Point(359, 182);
            this.cmbGgraficosLj.Name = "cmbGgraficosLj";
            this.cmbGgraficosLj.Size = new System.Drawing.Size(112, 15);
            this.cmbGgraficosLj.TabIndex = 4;
            this.cmbGgraficosLj.Text = "Gráficos de Lojas";
            this.cmbGgraficosLj.UseSelectable = true;
            // 
            // cmbGgraficosArt
            // 
            this.cmbGgraficosArt.AutoSize = true;
            this.cmbGgraficosArt.Location = new System.Drawing.Point(359, 160);
            this.cmbGgraficosArt.Name = "cmbGgraficosArt";
            this.cmbGgraficosArt.Size = new System.Drawing.Size(116, 15);
            this.cmbGgraficosArt.TabIndex = 3;
            this.cmbGgraficosArt.Text = "Gráficos de artigo";
            this.cmbGgraficosArt.UseSelectable = true;
            // 
            // cmbGgraficosOp
            // 
            this.cmbGgraficosOp.AutoSize = true;
            this.cmbGgraficosOp.Location = new System.Drawing.Point(359, 139);
            this.cmbGgraficosOp.Name = "cmbGgraficosOp";
            this.cmbGgraficosOp.Size = new System.Drawing.Size(146, 15);
            this.cmbGgraficosOp.TabIndex = 2;
            this.cmbGgraficosOp.Text = "Gráficos de Operadores";
            this.cmbGgraficosOp.UseSelectable = true;
            // 
            // FormRelatorioPOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1225, 629);
            this.ControlBox = false;
            this.Controls.Add(this.metroTabControl1);
            this.Name = "FormRelatorioPOS";
            this.Text = "Relatórios P.O.S";
            this.TransparencyKey = System.Drawing.Color.Empty;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormRelatorioPOS_FormClosing);
            this.Load += new System.EventHandler(this.FormRelatorioPOS_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.Vendas.ResumeLayout(false);
            this.Vendas.PerformLayout();
            this.cbIantesFecho.ResumeLayout(false);
            this.cbIantesFecho.PerformLayout();
            this.Artigo.ResumeLayout(false);
            this.Artigo.PerformLayout();
            this.Movimentos.ResumeLayout(false);
            this.Movimentos.PerformLayout();
            this.MapaEstatico.ResumeLayout(false);
            this.MapaEstatico.PerformLayout();
            this.Tabelas.ResumeLayout(false);
            this.Tabelas.PerformLayout();
            this.ReemissãoGuias.ResumeLayout(false);
            this.ReemissãoGuias.PerformLayout();
            this.ValoresStock.ResumeLayout(false);
            this.ValoresStock.PerformLayout();
            this.EntArtigos.ResumeLayout(false);
            this.EntArtigos.PerformLayout();
            this.FolhaEspelho.ResumeLayout(false);
            this.FolhaEspelho.PerformLayout();
            this.TransfPendentes.ResumeLayout(false);
            this.TransfPendentes.PerformLayout();
            this.CurvaAbc.ResumeLayout(false);
            this.CurvaAbc.PerformLayout();
            this.ApoioGestão.ResumeLayout(false);
            this.ApoioGestão.PerformLayout();
            this.MapaComparativo.ResumeLayout(false);
            this.MapaComparativo.PerformLayout();
            this.Graficos.ResumeLayout(false);
            this.Graficos.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        //private Telerik.WinControls.Themes.TelerikMetroTheme telerikMetroTheme1;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage Vendas;
        private MetroFramework.Controls.MetroTabPage cbIantesFecho;
        private MetroFramework.Controls.MetroTabPage Artigo;
        private MetroFramework.Controls.MetroTabPage Movimentos;
        private MetroFramework.Controls.MetroTabPage MapaEstatico;
        private MetroFramework.Controls.MetroTabPage Tabelas;
        private MetroFramework.Controls.MetroTabPage ReemissãoGuias;
        private MetroFramework.Controls.MetroTabPage ValoresStock;
        private MetroFramework.Controls.MetroTabPage EntArtigos;
        private MetroFramework.Controls.MetroTabPage FolhaEspelho;
        private MetroFramework.Controls.MetroTabPage TransfPendentes;
        private MetroFramework.Controls.MetroTabPage CurvaAbc;
        private MetroFramework.Controls.MetroTabPage ApoioGestão;
        private MetroFramework.Controls.MetroTabPage MapaComparativo;
        private MetroFramework.Controls.MetroTabPage Graficos;
        private MetroFramework.Controls.MetroCheckBox cbVListagemMov;
        private MetroFramework.Controls.MetroCheckBox cbVfaturacaoSemanaDia;
        private MetroFramework.Controls.MetroCheckBox cbVfaturaDiaHora;
        private MetroFramework.Controls.MetroCheckBox cbVresumoPerVendF;
        private MetroFramework.Controls.MetroCheckBox cbVresumoPerVend;
        private MetroFramework.Controls.MetroCheckBox cbVresumoPerOP;
        private MetroFramework.Controls.MetroCheckBox cbVdocDesc;
        private MetroFramework.Controls.MetroCheckBox cbVdocOperador;
        private MetroFramework.Controls.MetroCheckBox cbVresumoDiario;
        private MetroFramework.Controls.MetroCheckBox cbVresumoDiarioDoc;
        private MetroFramework.Controls.MetroCheckBox cbVlistagem;
        private MetroFramework.Controls.MetroCheckBox cbVvendasDinhVend;
        private MetroFramework.Controls.MetroCheckBox cbVvendasDinhOp;
        private MetroFramework.Controls.MetroCheckBox cbVmovimentoArtigo;
        private MetroFramework.Controls.MetroCheckBox cbVacumuladosFamilia;
        private MetroFramework.Controls.MetroCheckBox cbVacumuladosArmazem;
        private MetroFramework.Controls.MetroCheckBox cbVacumulados;
        private MetroFramework.Controls.MetroCheckBox cbContagemAd;
        private MetroFramework.Controls.MetroCheckBox cbIdepoisFecho;
        private MetroFramework.Controls.MetroCheckBox cbIantesFe;
        private MetroFramework.Controls.MetroCheckBox cbAsemQuant;
        private MetroFramework.Controls.MetroCheckBox cbAhistorMov;
        private MetroFramework.Controls.MetroCheckBox cbAexistencias;
        private MetroFramework.Controls.MetroCheckBox cbAetiqueta;
        private MetroFramework.Controls.MetroCheckBox cbAprecario;
        private MetroFramework.Controls.MetroCheckBox cbAitensAtivos;
        private MetroFramework.Controls.MetroCheckBox cbAlocali;
        private MetroFramework.Controls.MetroCheckBox cbAlistagemArt;
        private MetroFramework.Controls.MetroCheckBox cbApontoEncom;
        private MetroFramework.Controls.MetroCheckBox cbArotacaoStock;
        private MetroFramework.Controls.MetroCheckBox cbMsaidas;
        private MetroFramework.Controls.MetroCheckBox cbMentradas;
        private MetroFramework.Controls.MetroCheckBox cbMentradaFornec;
        private MetroFramework.Controls.MetroCheckBox cbMgeralArt;
        private MetroFramework.Controls.MetroCheckBox cbMdetalheMov;
        private MetroFramework.Controls.MetroCheckBox cbMAartigoAno;
        private MetroFramework.Controls.MetroCheckBox cbMAsaidaPano;
        private MetroFramework.Controls.MetroCheckBox cbMAsaidasAno;
        private MetroFramework.Controls.MetroCheckBox cbMmovimPorAno;
        private MetroFramework.Controls.MetroCheckBox cbMmovimento;
        private MetroFramework.Controls.MetroCheckBox cbTAfamilia;
        private MetroFramework.Controls.MetroCheckBox cbMAsubFam;
        private MetroFramework.Controls.MetroCheckBox cbTAmerc;
        private MetroFramework.Controls.MetroCheckBox cbTAloja;
        private MetroFramework.Controls.MetroCheckBox cbTAentrMerc;
        private MetroFramework.Controls.MetroCheckBox cbTAguiasTransp;
        private MetroFramework.Controls.MetroCheckBox cbTAentrSaidAjus;
        private MetroFramework.Controls.MetroCheckBox cbTAsaidaReq;
        private MetroFramework.Controls.MetroCheckBox cbTAsaidaTrans;
        private MetroFramework.Controls.MetroCheckBox cbTAentradaMerc;
        private System.Windows.Forms.ComboBox cmbVmercador;
        private System.Windows.Forms.ComboBox cmbVloja;
        private System.Windows.Forms.TextBox txtRVDataExist;
        private MetroFramework.Controls.MetroRadioButton metroRadioButton1;
        private MetroFramework.Controls.MetroRadioButton Geral;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.ComboBox cmbEdescricao;
        private System.Windows.Forms.ComboBox cmbEreferencia;
        private System.Windows.Forms.ComboBox cmbEmercadoria;
        private System.Windows.Forms.ComboBox cmbEloja;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.TextBox txtEdataFinal;
        private System.Windows.Forms.TextBox txtEdata;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox txtEnumProf;
        private MetroFramework.Controls.MetroButton btFimprimir;
        private MetroFramework.Controls.MetroButton btTimprimir;
        private MetroFramework.Controls.MetroComboBox cmbFlojaDest;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroCheckBox cbCexpExSaid;
        private MetroFramework.Controls.MetroCheckBox cbCexportEXvend;
        private MetroFramework.Controls.MetroCheckBox cbCvendasArtSmov;
        private MetroFramework.Controls.MetroCheckBox cbCvendas;
        private MetroFramework.Controls.MetroCheckBox cbCsaidaArt;
        private MetroFramework.Controls.MetroCheckBox cbAGsituEnc;
        private MetroFramework.Controls.MetroCheckBox cbAGriAteZero;
        private MetroFramework.Controls.MetroCheckBox cbAGriPend;
        private MetroFramework.Controls.MetroCheckBox cbAGriAtend;
        private MetroFramework.Controls.MetroCheckBox cbAGriEstad;
        private MetroFramework.Controls.MetroCheckBox cbAGstockTem;
        private MetroFramework.Controls.MetroCheckBox cbAGstockGeral;
        private MetroFramework.Controls.MetroCheckBox cbAGstockZero;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroComboBox cmbMloja;
        private MetroFramework.Controls.MetroComboBox cmbMlojaNexis;
        private MetroFramework.Controls.MetroButton btMimprimir;
        private MetroFramework.Controls.MetroButton metroButton6;
        private MetroFramework.Controls.MetroButton btEimprimir;
        private MetroFramework.Controls.MetroCheckBox cmbGgraficosArm;
        private MetroFramework.Controls.MetroCheckBox cmbGgraficosLj;
        private MetroFramework.Controls.MetroCheckBox cmbGgraficosArt;
        private MetroFramework.Controls.MetroCheckBox cmbGgraficosOp;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroLabel Loja;
        private MetroFramework.Controls.MetroCheckBox cbAGsugestEnc;
        private MetroFramework.Controls.MetroCheckBox cbAcumuladosSubfamilia;
    }
}