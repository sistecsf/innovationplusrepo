﻿namespace SISTEC_PLUS.POS.FormsRelatoriosPOS
{
    partial class FormAcumuladosPorSubFamilia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grpdata = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpDataInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpDataFinal = new System.Windows.Forms.DateTimePicker();
            this.grpcliente = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbFamilia = new System.Windows.Forms.ComboBox();
            this.cmbSubFamilia = new System.Windows.Forms.ComboBox();
            this.cmbSubFamilia2 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.btMinimisar = new System.Windows.Forms.Button();
            this.btMinimizar = new System.Windows.Forms.Button();
            this.btAplicar = new System.Windows.Forms.Button();
            this.btPaarametro = new System.Windows.Forms.Button();
            this.VW_POSVENDAS_ResumoVendaSubFamBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SubFamiliaAcumulados = new SISTEC_PLUS.POS.FormsRelatoriosPOS.SubFamiliaAcumulados();
            this.VW_POSVENDAS_ResumoVendaSubFamTableAdapter = new SISTEC_PLUS.POS.FormsRelatoriosPOS.SubFamiliaAcumuladosTableAdapters.VW_POSVENDAS_ResumoVendaSubFamTableAdapter();
            this.panel1.SuspendLayout();
            this.grpdata.SuspendLayout();
            this.grpcliente.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VW_POSVENDAS_ResumoVendaSubFamBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubFamiliaAcumulados)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.VW_POSVENDAS_ResumoVendaSubFamBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "SISTEC_PLUS.POS.FormsRelatoriosPOS.SubFamiliaAcumulados.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(90, 73);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(1286, 606);
            this.reportViewer1.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Controls.Add(this.grpdata);
            this.panel1.Controls.Add(this.grpcliente);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.btMinimisar);
            this.panel1.Controls.Add(this.btMinimizar);
            this.panel1.Controls.Add(this.btAplicar);
            this.panel1.Location = new System.Drawing.Point(-192, 685);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1853, 247);
            this.panel1.TabIndex = 14;
            this.panel1.Visible = false;
            // 
            // grpdata
            // 
            this.grpdata.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpdata.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.grpdata.Controls.Add(this.label1);
            this.grpdata.Controls.Add(this.label2);
            this.grpdata.Controls.Add(this.dtpDataInicial);
            this.grpdata.Controls.Add(this.dtpDataFinal);
            this.grpdata.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpdata.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpdata.Location = new System.Drawing.Point(1229, 39);
            this.grpdata.Name = "grpdata";
            this.grpdata.Size = new System.Drawing.Size(339, 119);
            this.grpdata.TabIndex = 148;
            this.grpdata.TabStop = false;
            this.grpdata.Text = "Data";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(76, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(76, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "De";
            // 
            // dtpDataInicial
            // 
            this.dtpDataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataInicial.Location = new System.Drawing.Point(105, 34);
            this.dtpDataInicial.Name = "dtpDataInicial";
            this.dtpDataInicial.Size = new System.Drawing.Size(128, 21);
            this.dtpDataInicial.TabIndex = 16;
            this.dtpDataInicial.ValueChanged += new System.EventHandler(this.dtpDataInicial_ValueChanged);
            // 
            // dtpDataFinal
            // 
            this.dtpDataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataFinal.Location = new System.Drawing.Point(105, 64);
            this.dtpDataFinal.Name = "dtpDataFinal";
            this.dtpDataFinal.Size = new System.Drawing.Size(128, 21);
            this.dtpDataFinal.TabIndex = 17;
            this.dtpDataFinal.ValueChanged += new System.EventHandler(this.dtpDataFinal_ValueChanged);
            // 
            // grpcliente
            // 
            this.grpcliente.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.grpcliente.Controls.Add(this.label3);
            this.grpcliente.Controls.Add(this.label6);
            this.grpcliente.Controls.Add(this.label10);
            this.grpcliente.Controls.Add(this.label7);
            this.grpcliente.Controls.Add(this.cmbFamilia);
            this.grpcliente.Controls.Add(this.cmbSubFamilia);
            this.grpcliente.Controls.Add(this.cmbSubFamilia2);
            this.grpcliente.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpcliente.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpcliente.Location = new System.Drawing.Point(717, 39);
            this.grpcliente.Name = "grpcliente";
            this.grpcliente.Size = new System.Drawing.Size(506, 115);
            this.grpcliente.TabIndex = 147;
            this.grpcliente.TabStop = false;
            this.grpcliente.Text = "Por Família";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(6, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 16);
            this.label3.TabIndex = 142;
            this.label3.Text = "Da Sub Familia";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(282, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 16);
            this.label6.TabIndex = 140;
            this.label6.Text = "à";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(6, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 16);
            this.label10.TabIndex = 1;
            this.label10.Text = "Da Familia";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(284, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(14, 16);
            this.label7.TabIndex = 141;
            this.label7.Text = "à";
            // 
            // cmbFamilia
            // 
            this.cmbFamilia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbFamilia.FormattingEnabled = true;
            this.cmbFamilia.IntegralHeight = false;
            this.cmbFamilia.Location = new System.Drawing.Point(101, 36);
            this.cmbFamilia.Name = "cmbFamilia";
            this.cmbFamilia.Size = new System.Drawing.Size(177, 24);
            this.cmbFamilia.TabIndex = 134;
            this.cmbFamilia.TextChanged += new System.EventHandler(this.cmbFamilia_TextChanged);
            // 
            // cmbSubFamilia
            // 
            this.cmbSubFamilia.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbSubFamilia.FormattingEnabled = true;
            this.cmbSubFamilia.IntegralHeight = false;
            this.cmbSubFamilia.Location = new System.Drawing.Point(101, 70);
            this.cmbSubFamilia.Name = "cmbSubFamilia";
            this.cmbSubFamilia.Size = new System.Drawing.Size(175, 24);
            this.cmbSubFamilia.TabIndex = 132;
            this.cmbSubFamilia.TextChanged += new System.EventHandler(this.cmbSubFamilia_TextChanged);
            // 
            // cmbSubFamilia2
            // 
            this.cmbSubFamilia2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbSubFamilia2.FormattingEnabled = true;
            this.cmbSubFamilia2.IntegralHeight = false;
            this.cmbSubFamilia2.Location = new System.Drawing.Point(302, 70);
            this.cmbSubFamilia2.Name = "cmbSubFamilia2";
            this.cmbSubFamilia2.Size = new System.Drawing.Size(198, 24);
            this.cmbSubFamilia2.TabIndex = 133;
            this.cmbSubFamilia2.TextChanged += new System.EventHandler(this.cmbSubFamilia2_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.cmbLoja);
            this.groupBox1.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox1.Location = new System.Drawing.Point(275, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(436, 115);
            this.groupBox1.TabIndex = 146;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Por Loja";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(55, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 16);
            this.label9.TabIndex = 1;
            this.label9.Text = "LOJA";
            // 
            // cmbLoja
            // 
            this.cmbLoja.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.IntegralHeight = false;
            this.cmbLoja.Location = new System.Drawing.Point(98, 27);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(280, 24);
            this.cmbLoja.TabIndex = 13;
            this.cmbLoja.TextChanged += new System.EventHandler(this.cmbLoja_TextChanged);
            // 
            // btMinimisar
            // 
            this.btMinimisar.Location = new System.Drawing.Point(194, 39);
            this.btMinimisar.Name = "btMinimisar";
            this.btMinimisar.Size = new System.Drawing.Size(75, 28);
            this.btMinimisar.TabIndex = 13;
            this.btMinimisar.Text = "Minimizar";
            this.btMinimisar.UseVisualStyleBackColor = true;
            this.btMinimisar.Click += new System.EventHandler(this.btMinimisar_Click);
            // 
            // btMinimizar
            // 
            this.btMinimizar.Location = new System.Drawing.Point(35, 39);
            this.btMinimizar.Name = "btMinimizar";
            this.btMinimizar.Size = new System.Drawing.Size(75, 28);
            this.btMinimizar.TabIndex = 6;
            this.btMinimizar.Text = "Minimizar";
            this.btMinimizar.UseVisualStyleBackColor = true;
            // 
            // btAplicar
            // 
            this.btAplicar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.btAplicar.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btAplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAplicar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAplicar.ForeColor = System.Drawing.Color.White;
            this.btAplicar.Location = new System.Drawing.Point(275, 170);
            this.btAplicar.Name = "btAplicar";
            this.btAplicar.Size = new System.Drawing.Size(92, 34);
            this.btAplicar.TabIndex = 1;
            this.btAplicar.Text = "Aplicar";
            this.btAplicar.UseVisualStyleBackColor = false;
            this.btAplicar.Click += new System.EventHandler(this.btAplicar_Click);
            // 
            // btPaarametro
            // 
            this.btPaarametro.Location = new System.Drawing.Point(9, 73);
            this.btPaarametro.Name = "btPaarametro";
            this.btPaarametro.Size = new System.Drawing.Size(75, 27);
            this.btPaarametro.TabIndex = 15;
            this.btPaarametro.Text = "Parametros";
            this.btPaarametro.UseVisualStyleBackColor = true;
            this.btPaarametro.Click += new System.EventHandler(this.btPaarametro_Click);
            // 
            // VW_POSVENDAS_ResumoVendaSubFamBindingSource
            // 
            this.VW_POSVENDAS_ResumoVendaSubFamBindingSource.DataMember = "VW_POSVENDAS_ResumoVendaSubFam";
            this.VW_POSVENDAS_ResumoVendaSubFamBindingSource.DataSource = this.SubFamiliaAcumulados;
            // 
            // SubFamiliaAcumulados
            // 
            this.SubFamiliaAcumulados.DataSetName = "SubFamiliaAcumulados";
            this.SubFamiliaAcumulados.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // VW_POSVENDAS_ResumoVendaSubFamTableAdapter
            // 
            this.VW_POSVENDAS_ResumoVendaSubFamTableAdapter.ClearBeforeFill = true;
            // 
            // FormAcumuladosPorSubFamilia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1468, 912);
            this.Controls.Add(this.btPaarametro);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.reportViewer1);
            this.Name = "FormAcumuladosPorSubFamilia";
            this.Text = "Relátorios de Movimeno por Sub-familia";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormAcumuladosPorSubFamilia_Load);
            this.panel1.ResumeLayout(false);
            this.grpdata.ResumeLayout(false);
            this.grpdata.PerformLayout();
            this.grpcliente.ResumeLayout(false);
            this.grpcliente.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VW_POSVENDAS_ResumoVendaSubFamBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubFamiliaAcumulados)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbFamilia;
        private System.Windows.Forms.ComboBox cmbSubFamilia2;
        private System.Windows.Forms.Button btMinimisar;
        private System.Windows.Forms.DateTimePicker dtpDataFinal;
        private System.Windows.Forms.DateTimePicker dtpDataInicial;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.Button btMinimizar;
        private System.Windows.Forms.Button btAplicar;
        private System.Windows.Forms.Button btPaarametro;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox grpcliente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox grpdata;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbSubFamilia;
        private System.Windows.Forms.BindingSource VW_POSVENDAS_ResumoVendaSubFamBindingSource;
        private SubFamiliaAcumulados SubFamiliaAcumulados;
        private SubFamiliaAcumuladosTableAdapters.VW_POSVENDAS_ResumoVendaSubFamTableAdapter VW_POSVENDAS_ResumoVendaSubFamTableAdapter;

    }
}