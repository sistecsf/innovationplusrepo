﻿namespace SISTEC_PLUS.POS.FormsRelatoriosPOS
{
    partial class FormMovimentoPorArtigo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.btPaarametro = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdPorValor = new System.Windows.Forms.RadioButton();
            this.rdQuantidade = new System.Windows.Forms.RadioButton();
            this.btRelatVendFam = new System.Windows.Forms.Button();
            this.btRelatGlFami = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdParcial = new System.Windows.Forms.RadioButton();
            this.rdGeral = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbLoja = new System.Windows.Forms.ComboBox();
            this.grpcliente = new System.Windows.Forms.GroupBox();
            this.rdRelatVendArtFamilia = new System.Windows.Forms.RadioButton();
            this.rdRelatGeralFamilia = new System.Windows.Forms.RadioButton();
            this.rdRelatorioGeralArtigo = new System.Windows.Forms.RadioButton();
            this.rdRelatVendasArtigo = new System.Windows.Forms.RadioButton();
            this.grpdata = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpDataInicial = new System.Windows.Forms.DateTimePicker();
            this.dtpDataFinal = new System.Windows.Forms.DateTimePicker();
            this.btRelatVendas = new System.Windows.Forms.Button();
            this.btRelatGeral = new System.Windows.Forms.Button();
            this.btMinimisar = new System.Windows.Forms.Button();
            this.btMinimizar = new System.Windows.Forms.Button();
            this.btAplicar = new System.Windows.Forms.Button();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.reportViewer3 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.reportViewer4 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.GeraldeSaídasPorFamíliaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.RelatorioGeralArtigosFamilia = new SISTEC_PLUS.POS.FormsRelatoriosPOS.RelatorioGeralArtigosFamilia();
            this.RelatorioGeralArtigosDataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.RelatorioGeralArtigos = new SISTEC_PLUS.POS.FormsRelatoriosPOS.RelatorioGeralArtigos();
            this.RelatorioGeralArtigosDataTable1TableAdapter = new SISTEC_PLUS.POS.FormsRelatoriosPOS.RelatorioGeralArtigosTableAdapters.RelatorioGeralArtigosDataTable1TableAdapter();
            this.GeraldeSaídasPorFamíliaTableAdapter = new SISTEC_PLUS.POS.FormsRelatoriosPOS.RelatorioGeralArtigosFamiliaTableAdapters.GeraldeSaídasPorFamíliaTableAdapter();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpcliente.SuspendLayout();
            this.grpdata.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GeraldeSaídasPorFamíliaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioGeralArtigosFamilia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioGeralArtigosDataTable1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioGeralArtigos)).BeginInit();
            this.SuspendLayout();
            // 
            // btPaarametro
            // 
            this.btPaarametro.Location = new System.Drawing.Point(23, 75);
            this.btPaarametro.Name = "btPaarametro";
            this.btPaarametro.Size = new System.Drawing.Size(75, 27);
            this.btPaarametro.TabIndex = 17;
            this.btPaarametro.Text = "Parametros";
            this.btPaarametro.UseVisualStyleBackColor = true;
            this.btPaarametro.Click += new System.EventHandler(this.btPaarametro_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btRelatVendFam);
            this.panel1.Controls.Add(this.btRelatGlFami);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.grpcliente);
            this.panel1.Controls.Add(this.grpdata);
            this.panel1.Controls.Add(this.btRelatVendas);
            this.panel1.Controls.Add(this.btRelatGeral);
            this.panel1.Controls.Add(this.btMinimisar);
            this.panel1.Controls.Add(this.btMinimizar);
            this.panel1.Controls.Add(this.btAplicar);
            this.panel1.Location = new System.Drawing.Point(-192, 689);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1853, 243);
            this.panel1.TabIndex = 19;
            this.panel1.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.groupBox2.Controls.Add(this.rdPorValor);
            this.groupBox2.Controls.Add(this.rdQuantidade);
            this.groupBox2.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox2.Location = new System.Drawing.Point(1034, 35);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(288, 115);
            this.groupBox2.TabIndex = 41;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opções de impressão e ordenação";
            // 
            // rdPorValor
            // 
            this.rdPorValor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdPorValor.AutoSize = true;
            this.rdPorValor.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdPorValor.Location = new System.Drawing.Point(37, 51);
            this.rdPorValor.Name = "rdPorValor";
            this.rdPorValor.Size = new System.Drawing.Size(77, 20);
            this.rdPorValor.TabIndex = 141;
            this.rdPorValor.TabStop = true;
            this.rdPorValor.Text = "Por Valor";
            this.rdPorValor.UseVisualStyleBackColor = true;
            // 
            // rdQuantidade
            // 
            this.rdQuantidade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdQuantidade.AutoSize = true;
            this.rdQuantidade.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdQuantidade.Location = new System.Drawing.Point(37, 25);
            this.rdQuantidade.Name = "rdQuantidade";
            this.rdQuantidade.Size = new System.Drawing.Size(155, 20);
            this.rdQuantidade.TabIndex = 140;
            this.rdQuantidade.TabStop = true;
            this.rdQuantidade.Text = "Por Qunatidade vendida";
            this.rdQuantidade.UseVisualStyleBackColor = true;
            // 
            // btRelatVendFam
            // 
            this.btRelatVendFam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btRelatVendFam.Location = new System.Drawing.Point(1570, 132);
            this.btRelatVendFam.Name = "btRelatVendFam";
            this.btRelatVendFam.Size = new System.Drawing.Size(75, 28);
            this.btRelatVendFam.TabIndex = 147;
            this.btRelatVendFam.Text = "btReltVedFam";
            this.btRelatVendFam.UseVisualStyleBackColor = true;
            this.btRelatVendFam.Click += new System.EventHandler(this.btRelatVendFam_Click);
            // 
            // btRelatGlFami
            // 
            this.btRelatGlFami.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btRelatGlFami.Location = new System.Drawing.Point(1570, 101);
            this.btRelatGlFami.Name = "btRelatGlFami";
            this.btRelatGlFami.Size = new System.Drawing.Size(75, 28);
            this.btRelatGlFami.TabIndex = 146;
            this.btRelatGlFami.Text = "btRelatGlFami";
            this.btRelatGlFami.UseVisualStyleBackColor = true;
            this.btRelatGlFami.Click += new System.EventHandler(this.btRelatGlFami_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.groupBox1.Controls.Add(this.rdParcial);
            this.groupBox1.Controls.Add(this.rdGeral);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cmbLoja);
            this.groupBox1.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox1.Location = new System.Drawing.Point(296, 35);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(392, 115);
            this.groupBox1.TabIndex = 145;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Por Loja";
            // 
            // rdParcial
            // 
            this.rdParcial.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdParcial.AutoSize = true;
            this.rdParcial.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdParcial.Location = new System.Drawing.Point(204, 66);
            this.rdParcial.Name = "rdParcial";
            this.rdParcial.Size = new System.Drawing.Size(63, 20);
            this.rdParcial.TabIndex = 143;
            this.rdParcial.TabStop = true;
            this.rdParcial.Text = "Parcial";
            this.rdParcial.UseVisualStyleBackColor = true;
            this.rdParcial.CheckedChanged += new System.EventHandler(this.rdParcial_CheckedChanged);
            // 
            // rdGeral
            // 
            this.rdGeral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdGeral.AutoSize = true;
            this.rdGeral.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdGeral.Location = new System.Drawing.Point(70, 65);
            this.rdGeral.Name = "rdGeral";
            this.rdGeral.Size = new System.Drawing.Size(56, 20);
            this.rdGeral.TabIndex = 142;
            this.rdGeral.TabStop = true;
            this.rdGeral.Text = "Geral";
            this.rdGeral.UseVisualStyleBackColor = true;
            this.rdGeral.CheckedChanged += new System.EventHandler(this.rdGeral_CheckedChanged);
            this.rdGeral.VisibleChanged += new System.EventHandler(this.rdGeral_VisibleChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(27, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "LOJA";
            // 
            // cmbLoja
            // 
            this.cmbLoja.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.IntegralHeight = false;
            this.cmbLoja.Location = new System.Drawing.Point(70, 26);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(249, 24);
            this.cmbLoja.TabIndex = 13;
            this.cmbLoja.TextChanged += new System.EventHandler(this.cmbLoja_TextChanged);
            // 
            // grpcliente
            // 
            this.grpcliente.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.grpcliente.Controls.Add(this.rdRelatVendArtFamilia);
            this.grpcliente.Controls.Add(this.rdRelatGeralFamilia);
            this.grpcliente.Controls.Add(this.rdRelatorioGeralArtigo);
            this.grpcliente.Controls.Add(this.rdRelatVendasArtigo);
            this.grpcliente.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpcliente.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpcliente.Location = new System.Drawing.Point(694, 35);
            this.grpcliente.Name = "grpcliente";
            this.grpcliente.Size = new System.Drawing.Size(334, 115);
            this.grpcliente.TabIndex = 144;
            this.grpcliente.TabStop = false;
            this.grpcliente.Text = "Opções de Relátorios";
            // 
            // rdRelatVendArtFamilia
            // 
            this.rdRelatVendArtFamilia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdRelatVendArtFamilia.AutoSize = true;
            this.rdRelatVendArtFamilia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdRelatVendArtFamilia.Location = new System.Drawing.Point(49, 89);
            this.rdRelatVendArtFamilia.Name = "rdRelatVendArtFamilia";
            this.rdRelatVendArtFamilia.Size = new System.Drawing.Size(247, 20);
            this.rdRelatVendArtFamilia.TabIndex = 141;
            this.rdRelatVendArtFamilia.TabStop = true;
            this.rdRelatVendArtFamilia.Text = "Relatório de vendas do Artigo por Família";
            this.rdRelatVendArtFamilia.UseVisualStyleBackColor = true;
            // 
            // rdRelatGeralFamilia
            // 
            this.rdRelatGeralFamilia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdRelatGeralFamilia.AutoSize = true;
            this.rdRelatGeralFamilia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdRelatGeralFamilia.Location = new System.Drawing.Point(49, 66);
            this.rdRelatGeralFamilia.Name = "rdRelatGeralFamilia";
            this.rdRelatGeralFamilia.Size = new System.Drawing.Size(279, 20);
            this.rdRelatGeralFamilia.TabIndex = 140;
            this.rdRelatGeralFamilia.TabStop = true;
            this.rdRelatGeralFamilia.Text = "Relatório Geral de Saídas do Artigo Por Família";
            this.rdRelatGeralFamilia.UseVisualStyleBackColor = true;
            // 
            // rdRelatorioGeralArtigo
            // 
            this.rdRelatorioGeralArtigo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdRelatorioGeralArtigo.AutoSize = true;
            this.rdRelatorioGeralArtigo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdRelatorioGeralArtigo.Location = new System.Drawing.Point(49, 20);
            this.rdRelatorioGeralArtigo.Name = "rdRelatorioGeralArtigo";
            this.rdRelatorioGeralArtigo.Size = new System.Drawing.Size(215, 20);
            this.rdRelatorioGeralArtigo.TabIndex = 139;
            this.rdRelatorioGeralArtigo.TabStop = true;
            this.rdRelatorioGeralArtigo.Text = "Relatório Geral de Saídas do Artigo";
            this.rdRelatorioGeralArtigo.UseVisualStyleBackColor = true;
            // 
            // rdRelatVendasArtigo
            // 
            this.rdRelatVendasArtigo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rdRelatVendasArtigo.AutoSize = true;
            this.rdRelatVendasArtigo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.rdRelatVendasArtigo.Location = new System.Drawing.Point(49, 40);
            this.rdRelatVendasArtigo.Name = "rdRelatVendasArtigo";
            this.rdRelatVendasArtigo.Size = new System.Drawing.Size(183, 20);
            this.rdRelatVendasArtigo.TabIndex = 132;
            this.rdRelatVendasArtigo.TabStop = true;
            this.rdRelatVendasArtigo.Text = "Relatório de vendas do Artigo";
            this.rdRelatVendasArtigo.UseVisualStyleBackColor = true;
            // 
            // grpdata
            // 
            this.grpdata.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpdata.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.grpdata.Controls.Add(this.label8);
            this.grpdata.Controls.Add(this.label9);
            this.grpdata.Controls.Add(this.dtpDataInicial);
            this.grpdata.Controls.Add(this.dtpDataFinal);
            this.grpdata.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpdata.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpdata.Location = new System.Drawing.Point(1328, 35);
            this.grpdata.Name = "grpdata";
            this.grpdata.Size = new System.Drawing.Size(236, 115);
            this.grpdata.TabIndex = 40;
            this.grpdata.TabStop = false;
            this.grpdata.Text = "Data";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(50, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 16);
            this.label8.TabIndex = 2;
            this.label8.Text = "A";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(50, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 16);
            this.label9.TabIndex = 0;
            this.label9.Text = "De";
            // 
            // dtpDataInicial
            // 
            this.dtpDataInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataInicial.Location = new System.Drawing.Point(79, 29);
            this.dtpDataInicial.Name = "dtpDataInicial";
            this.dtpDataInicial.Size = new System.Drawing.Size(111, 21);
            this.dtpDataInicial.TabIndex = 16;
            // 
            // dtpDataFinal
            // 
            this.dtpDataFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataFinal.Location = new System.Drawing.Point(79, 63);
            this.dtpDataFinal.Name = "dtpDataFinal";
            this.dtpDataFinal.Size = new System.Drawing.Size(111, 21);
            this.dtpDataFinal.TabIndex = 17;
            // 
            // btRelatVendas
            // 
            this.btRelatVendas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btRelatVendas.Location = new System.Drawing.Point(1570, 72);
            this.btRelatVendas.Name = "btRelatVendas";
            this.btRelatVendas.Size = new System.Drawing.Size(75, 28);
            this.btRelatVendas.TabIndex = 143;
            this.btRelatVendas.Text = "btRelatVendas";
            this.btRelatVendas.UseVisualStyleBackColor = true;
            this.btRelatVendas.Click += new System.EventHandler(this.btRelatVendas_Click);
            // 
            // btRelatGeral
            // 
            this.btRelatGeral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btRelatGeral.Location = new System.Drawing.Point(1570, 41);
            this.btRelatGeral.Name = "btRelatGeral";
            this.btRelatGeral.Size = new System.Drawing.Size(75, 28);
            this.btRelatGeral.TabIndex = 142;
            this.btRelatGeral.Text = "btRelatGeral";
            this.btRelatGeral.UseVisualStyleBackColor = true;
            this.btRelatGeral.Click += new System.EventHandler(this.btRelatGeral_Click);
            // 
            // btMinimisar
            // 
            this.btMinimisar.Location = new System.Drawing.Point(215, 39);
            this.btMinimisar.Name = "btMinimisar";
            this.btMinimisar.Size = new System.Drawing.Size(75, 28);
            this.btMinimisar.TabIndex = 13;
            this.btMinimisar.Text = "Minimizar";
            this.btMinimisar.UseVisualStyleBackColor = true;
            this.btMinimisar.Click += new System.EventHandler(this.btMinimisar_Click);
            // 
            // btMinimizar
            // 
            this.btMinimizar.Location = new System.Drawing.Point(35, 39);
            this.btMinimizar.Name = "btMinimizar";
            this.btMinimizar.Size = new System.Drawing.Size(75, 28);
            this.btMinimizar.TabIndex = 6;
            this.btMinimizar.Text = "Minimizar";
            this.btMinimizar.UseVisualStyleBackColor = true;
            // 
            // btAplicar
            // 
            this.btAplicar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btAplicar.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btAplicar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAplicar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAplicar.ForeColor = System.Drawing.Color.White;
            this.btAplicar.Location = new System.Drawing.Point(296, 165);
            this.btAplicar.Name = "btAplicar";
            this.btAplicar.Size = new System.Drawing.Size(77, 35);
            this.btAplicar.TabIndex = 1;
            this.btAplicar.Text = "Aplicar";
            this.btAplicar.UseVisualStyleBackColor = false;
            this.btAplicar.Click += new System.EventHandler(this.btAplicar_Click);
            // 
            // reportViewer1
            // 
            this.reportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "RelatorioGeralArtigos";
            reportDataSource1.Value = this.RelatorioGeralArtigosDataTable1BindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "SISTEC_PLUS.POS.FormsRelatoriosPOS.RelatorioGeralArtigos.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(104, 75);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(1268, 608);
            this.reportViewer1.TabIndex = 20;
            // 
            // reportViewer2
            // 
            this.reportViewer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource2.Name = "RelatorioGeralArtigos";
            reportDataSource2.Value = this.RelatorioGeralArtigosDataTable1BindingSource;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "SISTEC_PLUS.POS.FormsRelatoriosPOS.RelatorioVendaArtigos.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(104, 75);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.Size = new System.Drawing.Size(1268, 608);
            this.reportViewer2.TabIndex = 21;
            // 
            // reportViewer3
            // 
            this.reportViewer3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource3.Name = "RelatorioGeralArtigosFamilia";
            reportDataSource3.Value = this.GeraldeSaídasPorFamíliaBindingSource;
            this.reportViewer3.LocalReport.DataSources.Add(reportDataSource3);
            this.reportViewer3.LocalReport.ReportEmbeddedResource = "SISTEC_PLUS.POS.FormsRelatoriosPOS.RelatorioGeralArtigosFamilia.rdlc";
            this.reportViewer3.Location = new System.Drawing.Point(104, 75);
            this.reportViewer3.Name = "reportViewer3";
            this.reportViewer3.Size = new System.Drawing.Size(1268, 608);
            this.reportViewer3.TabIndex = 22;
            // 
            // reportViewer4
            // 
            this.reportViewer4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource4.Name = "RelatorioGeralArtigosFamilia";
            reportDataSource4.Value = this.GeraldeSaídasPorFamíliaBindingSource;
            this.reportViewer4.LocalReport.DataSources.Add(reportDataSource4);
            this.reportViewer4.LocalReport.ReportEmbeddedResource = "SISTEC_PLUS.POS.FormsRelatoriosPOS.VendasArtigosFamilia.rdlc";
            this.reportViewer4.Location = new System.Drawing.Point(104, 75);
            this.reportViewer4.Name = "reportViewer4";
            this.reportViewer4.Size = new System.Drawing.Size(1268, 608);
            this.reportViewer4.TabIndex = 23;
            // 
            // GeraldeSaídasPorFamíliaBindingSource
            // 
            this.GeraldeSaídasPorFamíliaBindingSource.DataMember = "GeraldeSaídasPorFamília";
            this.GeraldeSaídasPorFamíliaBindingSource.DataSource = this.RelatorioGeralArtigosFamilia;
            // 
            // RelatorioGeralArtigosFamilia
            // 
            this.RelatorioGeralArtigosFamilia.DataSetName = "RelatorioGeralArtigosFamilia";
            this.RelatorioGeralArtigosFamilia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // RelatorioGeralArtigosDataTable1BindingSource
            // 
            this.RelatorioGeralArtigosDataTable1BindingSource.DataMember = "RelatorioGeralArtigosDataTable1";
            this.RelatorioGeralArtigosDataTable1BindingSource.DataSource = this.RelatorioGeralArtigos;
            // 
            // RelatorioGeralArtigos
            // 
            this.RelatorioGeralArtigos.DataSetName = "RelatorioGeralArtigos";
            this.RelatorioGeralArtigos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // RelatorioGeralArtigosDataTable1TableAdapter
            // 
            this.RelatorioGeralArtigosDataTable1TableAdapter.ClearBeforeFill = true;
            // 
            // GeraldeSaídasPorFamíliaTableAdapter
            // 
            this.GeraldeSaídasPorFamíliaTableAdapter.ClearBeforeFill = true;
            // 
            // FormMovimentoPorArtigo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1468, 912);
            this.Controls.Add(this.reportViewer4);
            this.Controls.Add(this.reportViewer3);
            this.Controls.Add(this.reportViewer2);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btPaarametro);
            this.ForeColor = System.Drawing.SystemColors.MenuText;
            this.Name = "FormMovimentoPorArtigo";
            this.Text = "Relátorio Movimento Por Artigo";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormMovimentoPorArtigo_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpcliente.ResumeLayout(false);
            this.grpcliente.PerformLayout();
            this.grpdata.ResumeLayout(false);
            this.grpdata.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GeraldeSaídasPorFamíliaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioGeralArtigosFamilia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioGeralArtigosDataTable1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioGeralArtigos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btPaarametro;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbLoja;
        private System.Windows.Forms.RadioButton rdRelatVendasArtigo;
        private System.Windows.Forms.GroupBox grpcliente;
        private System.Windows.Forms.GroupBox grpdata;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpDataInicial;
        private System.Windows.Forms.DateTimePicker dtpDataFinal;
        private System.Windows.Forms.Button btRelatVendas;
        private System.Windows.Forms.Button btRelatGeral;
        private System.Windows.Forms.Button btMinimisar;
        private System.Windows.Forms.Button btMinimizar;
        private System.Windows.Forms.Button btAplicar;
        private System.Windows.Forms.RadioButton rdRelatVendArtFamilia;
        private System.Windows.Forms.RadioButton rdRelatGeralFamilia;
        private System.Windows.Forms.RadioButton rdRelatorioGeralArtigo;
        private System.Windows.Forms.Button btRelatVendFam;
        private System.Windows.Forms.Button btRelatGlFami;
        private System.Windows.Forms.RadioButton rdParcial;
        private System.Windows.Forms.RadioButton rdGeral;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdPorValor;
        private System.Windows.Forms.RadioButton rdQuantidade;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource RelatorioGeralArtigosDataTable1BindingSource;
        private RelatorioGeralArtigos RelatorioGeralArtigos;
        private RelatorioGeralArtigosTableAdapters.RelatorioGeralArtigosDataTable1TableAdapter RelatorioGeralArtigosDataTable1TableAdapter;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
        private RelatorioGeralArtigosFamilia RelatorioGeralArtigosFamilia;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer3;
        private System.Windows.Forms.BindingSource GeraldeSaídasPorFamíliaBindingSource;
        private RelatorioGeralArtigosFamiliaTableAdapters.GeraldeSaídasPorFamíliaTableAdapter GeraldeSaídasPorFamíliaTableAdapter;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer4;
    }
}