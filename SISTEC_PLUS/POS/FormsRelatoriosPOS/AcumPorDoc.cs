﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
namespace SISTEC_PLUS.POS.FormsRelatoriosPOS
{
    public partial class AcumPorDoc : MetroFramework.Forms.MetroForm
    {
        public AcumPorDoc()
        {
            InitializeComponent();
        }

        private void AcumPorDoc_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }
    }
}
