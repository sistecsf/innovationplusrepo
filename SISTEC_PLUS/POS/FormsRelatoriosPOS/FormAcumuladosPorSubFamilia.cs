﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS.FormsRelatoriosPOS
{
    public partial class FormAcumuladosPorSubFamilia : MetroFramework.Forms.MetroForm
    {


        string[] Armazem, NomeLoja, Valor, Contravalor, Padrao, CodArmazem, Doc, qtd;
        string operador, sql1, sql2, sql3;


        int tamanho, i;
        string codLoja;
        string CODLOJA, codArmz, codArmz2, codArmz3;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        //SqlConnection conexao = new SqlConnection(Variavel.Conn);

        public FormAcumuladosPorSubFamilia()
        {
            InitializeComponent();
        }

        private void FormAcumuladosPorSubFamilia_Load(object sender, EventArgs e)
        {
            


           



            dtpDataInicial.Format = DateTimePickerFormat.Custom;

            dtpDataInicial.CustomFormat = "yyyy-MM-dd";

            dtpDataFinal.Format = DateTimePickerFormat.Custom;

            dtpDataFinal.CustomFormat = "yyyy-MM-dd";



            // PESQUISA  A LOJA NO BANCO DE  DADOS
            SqlCommand cmd = new SqlCommand(@"SELECT DISTINCT RTRIM (LTRIM (NOMELOJA)) FROM ASLOJA WITH (NOLOCK)");
            cmd.Connection = conexao;



            try
            {

                conexao.Open();
                cmd.Connection = conexao;
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {

                    cmbLoja.Items.Add(leitor.GetValue(0));

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }

        private void btMinimisar_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
        }

        private void btPaarametro_Click(object sender, EventArgs e)
        {
            panel1.Visible = true;
        }

        private void cmbLoja_TextChanged(object sender, EventArgs e)
        {
            // PESQUISA  O CODLOJA 
            SqlCommand cmmd = new SqlCommand(@"SELECT CodLoja from ASLOJA WITH (NOLOCK) where NomeLoja='" + cmbLoja.Text + "'");

            cmmd.Connection = conexao;

            try
            {

                conexao.Open();
                cmmd.Connection = conexao;
                SqlDataReader leitor = cmmd.ExecuteReader();
                while (leitor.Read())
                {

                    codLoja = leitor[0].ToString();

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

            MessageBox.Show(codLoja);

            // LIMPAR A COMBO ANTES DE MOSTRAR OS RESULTADOS 
            cmbFamilia.Text = "";
            cmbFamilia.Items.Clear();
            cmbSubFamilia.Text = "";
            cmbSubFamilia.Items.Clear();
            cmbSubFamilia2.Text = "";
            cmbSubFamilia2.Items.Clear();

            SqlCommand cmdd = new SqlCommand(@" SELECT DISTINCT RTRIM( FA.NOMEMERC )FROM ASFAMILIA FA WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), ASFICMOV1 F WITH (NOLOCK) WHERE M.CODLOJA = F.CODLOJA AND M.CODARMZ = F.CODARMZ AND M.REFERENC = F.REFERENC AND M.CODFAM = FA.CODFAM AND F.Codloja ='" + codLoja + "'");

            cmdd.Connection = conexao;


            try
            {
                cmdd.Connection = conexao;
                //cmdd .ExecuteNonQuery();
                conexao.Open();
                SqlDataReader leitor = cmdd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbFamilia.Items.Add(leitor.GetValue(0));
                   // cmbSubFamilia.Items.Add(leitor.GetValue(0));
                    //cmbSubFamilia2.Items.Add(leitor.GetValue(0));

                   

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }



        }

        private void dtpDataInicial_ValueChanged(object sender, EventArgs e)
        {
            dtpDataInicial.Format = DateTimePickerFormat.Custom;
            dtpDataInicial.CustomFormat = ("yyyy-MM-dd");
            dtpDataFinal.Format = DateTimePickerFormat.Custom;
            dtpDataFinal.CustomFormat = ("yyyy-MM-dd");

            DateTime de = dtpDataInicial.Value.Date;
            DateTime hoje = DateTime.Now.Date;

            int result = DateTime.Compare(hoje, de);


            if (result < 0)
            {
                MessageBox.Show("A data não pode ser menor que hoje.", "Data");
                dtpDataInicial.Text = hoje.ToShortDateString();
                de = dtpDataInicial.Value.Date;

            }

            DateTime a = dtpDataFinal.Value.Date;

            result = DateTime.Compare(a, de);


            if (result < 0)
            {
                MessageBox.Show("Alterou a data de A.", "Data");
                dtpDataFinal.Text = dtpDataInicial.Value.ToShortDateString();

            }
        }

        private void dtpDataFinal_ValueChanged(object sender, EventArgs e)
        {
            dtpDataInicial.Format = DateTimePickerFormat.Custom;
            dtpDataInicial.CustomFormat = ("yyyy-MM-dd");
            dtpDataFinal.Format = DateTimePickerFormat.Custom;
            dtpDataFinal.CustomFormat = ("yyyy-MM-dd");

            DateTime de = dtpDataInicial.Value.Date;
            DateTime a = dtpDataFinal.Value.Date;

            DateTime hoje = DateTime.Now.Date;

            int result = DateTime.Compare(hoje, a);


            if (result < 0)
            {
                MessageBox.Show("A data não pode ser maior que Hoje.", "Data");
                dtpDataFinal.Text = hoje.ToShortDateString();

            }
            else
            {

                result = DateTime.Compare(a, de);


                if (result < 0)
                {
                    MessageBox.Show("A data não pode ser menor.", "Data");
                    dtpDataFinal.Text = dtpDataInicial.Value.ToShortDateString();

                }
            }
        }

        private void cmbFamilia_TextChanged(object sender, EventArgs e)
        {
            SqlCommand cmmd = new SqlCommand(@"SELECT RTRIM( CODFAM ) FROM ASFAMILIA WITH (NOLOCK)  WHERE NOMEMERC = '" + cmbFamilia.Text + "'   ");

            cmmd.Connection = conexao;

            try
            {

                conexao.Open();
                cmmd.Connection = conexao;
                SqlDataReader leitor = cmmd.ExecuteReader();
                while (leitor.Read())
                {

                    codArmz = leitor[0].ToString();

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

            MessageBox.Show(codArmz);


            cmbSubFamilia2.Text = "";
            cmbSubFamilia2.Items.Clear();
            cmbSubFamilia.Text = "";
            cmbSubFamilia.Items.Clear();


            SqlCommand cmdd = new SqlCommand(@" SELECT DISTINCT RTRIM( SU.DESCRICAO )FROM ASSUBFAMILIA SU WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), ASFICMOV1 F WITH (NOLOCK)WHERE M.CODLOJA= F.CODLOJA AND M.CODARMZ = F.CODARMZ AND M.CODFAM = SU.CODFAM AND M.CODSUBFAM = SU.CODSUBFAM AND SU.CODFAM='" + codArmz + "' AND F.Codloja ='" + codLoja + "'");

            cmdd.Connection = conexao;


            try
            {
                cmdd.Connection = conexao;
                //cmdd .ExecuteNonQuery();
                conexao.Open();
                SqlDataReader leitor = cmdd.ExecuteReader();
                while (leitor.Read())
                {

                    cmbSubFamilia2.Items.Add(leitor.GetValue(0));
                    cmbSubFamilia.Items.Add(leitor.GetValue(0));

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }







        }

        private void cmbSubFamilia2_TextChanged(object sender, EventArgs e)
        {
           
            
            /*
            SqlCommand cmmd = new SqlCommand(@"SELECT RTRIM( CODFAM ) FROM ASFAMILIA WITH (NOLOCK)  WHERE NOMEMERC = '" + cmbFamilia.Text + "'   ");

            cmmd.Connection = conexao;

            try
            {

                conexao.Open();
                cmmd.Connection = conexao;
                SqlDataReader leitor = cmmd.ExecuteReader();
                while (leitor.Read())
                {

                    codArmz3 = leitor[0].ToString();

                }
                conexao.Close();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }

            MessageBox.Show(codArmz3);
             
             */
        }

        private void cmbSubFamilia_TextChanged(object sender, EventArgs e)
        {
            // select verdadeiro
            /*

            
        SELECT DISTINCT RTRIM( SU.DESCRICAO )FROM ASSUBFAMILIA SU WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), ASFICMOV1
         F WITH (NOLOCK)
       WHERE M.CODLOJA= F.CODLOJA AND M.CODARMZ = F.CODARMZ AND M.CODFAM =
      SU.CODFAM
          AND M.CODSUBFAM = SU.CODSUBFAM AND SU.CODFAM='BAT' AND F.Codloja ='TES'
        */ 
            /*
       SqlCommand cmmd = new SqlCommand(@"SELECT RTRIM( CODFAM ) FROM ASFAMILIA WITH (NOLOCK)  WHERE NOMEMERC = '" + cmbFamilia.Text + "'   ");

       cmmd.Connection = conexao;

       try
       {

           conexao.Open();
           cmmd.Connection = conexao;
           SqlDataReader leitor = cmmd.ExecuteReader();
           while (leitor.Read())
           {

               codArmz2 = leitor[0].ToString();

           }
           conexao.Close();

       }
       catch (SqlException ex)
       {
           MessageBox.Show(ex.Message);
       }
       finally
       {
           conexao.Close();
       }

       MessageBox.Show(codArmz2);
              
              
             */
        }

        private void btAplicar_Click(object sender, EventArgs e)
        {
            Microsoft.Reporting.WinForms.ReportParameter[] p = new Microsoft.Reporting.WinForms.ReportParameter[4];
            p[1] = new Microsoft.Reporting.WinForms.ReportParameter("Dinicial", dtpDataInicial.Text);
            p[2] = new Microsoft.Reporting.WinForms.ReportParameter("Dfinal", dtpDataFinal.Text);
            p[0] = new Microsoft.Reporting.WinForms.ReportParameter("operador", Variavel.codUtilizador);
            p[3] = new Microsoft.Reporting.WinForms.ReportParameter("loja", cmbLoja.Text);

            //  reportViewer1.LocalReport.SetParameters(p);
            reportViewer1.LocalReport.SetParameters(p);

            this.VW_POSVENDAS_ResumoVendaSubFamTableAdapter.Fill(this.SubFamiliaAcumulados.VW_POSVENDAS_ResumoVendaSubFam, codLoja, cmbFamilia.Text, cmbSubFamilia.Text, cmbSubFamilia2.Text, dtpDataInicial.Text, dtpDataFinal.Text);
            this.reportViewer1.RefreshReport();
        }
    }
}
