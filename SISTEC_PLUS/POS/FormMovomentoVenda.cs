﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using SISTEC_PLUS.POS;
using System.Data.SqlClient;

namespace SISTEC_PLUS.OPS
{
    public partial class FormMovomentoVenda : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(@"Data Source=sisdbs1b;Initial Catalog=INNOVUIGE;User ID=admin;Password=admin"); // conexão com o banco
        FormAberturaFechodoDia abertura = new FormAberturaFechodoDia();
        FormTabelas tabelas = new FormTabelas();
        FormEntradaMercadoria entrada = new FormEntradaMercadoria();
        FormRelatorioPOS relatorio = new FormRelatorioPOS();
        FormNotaDevolucao nota = new FormNotaDevolucao();
        FormVendas venda = new FormVendas();
        FormFacturaDescritiva factura = new FormFacturaDescritiva();
        FormFacturaDescritiva2 factura2 = new FormFacturaDescritiva2();
        FormProformaDescritiva proforma = new FormProformaDescritiva();
        FormPagamentoFactura pagamento = new FormPagamentoFactura();
        FormRequisicaoInterna requisicao = new FormRequisicaoInterna();
        FormMenuTransferencias transferencias = new FormMenuTransferencias();
        FormInventarioFisico inventario = new FormInventarioFisico();
        FormEntradaSaidas entradasaida = new FormEntradaSaidas();
        FormTransferenciaGuias importacao = new FormTransferenciaGuias();
        FormGestaoDescontos gestao = new FormGestaoDescontos();
        FormPagamentoVendas pagamentoV = new FormPagamentoVendas();
        FormRegistroOportunidade registro = new FormRegistroOportunidade();
        string dataA, data1;

        private void fecharFormulario()
        {
            tabelas.Visible = false;
            abertura.Visible = false;
            entrada.Visible = false;
            relatorio.Visible = false;
            nota.Visible = false;
            venda.Visible = false;
            factura.Visible = false;
            factura2.Visible = false;
            proforma.Visible = false;
            pagamento.Visible = false;
            requisicao.Visible = false;
            transferencias.Visible = false;
            inventario.Visible = false;
            entradasaida.Visible = false;
            importacao.Visible = false;
            gestao.Visible = false;
            pagamentoV.Visible = false;
            registro.Visible = false;/*
             */
        }
        public FormMovomentoVenda()
        {
            InitializeComponent();
        }
        private void FormMovomentoVenda_Load(object sender, EventArgs e)
        {
           // toolStripButton3.Enabled = false;
            toolStripButton10.Enabled = false;
            toolStripButton11.Enabled = false;
            toolStripButton12.Enabled = false;
            toolStripButton13.Enabled = false;
            toolStripButton14.Enabled = false;
            toolStripButton15.Enabled = false;


            this.Text = "Ponto de Vendas"+"    ||    "+ Variavel.nomeUtilizador;
            MdiClient ctlMDI;

            // Loop through all of the form's controls looking
            // for the control of type MdiClient.
            foreach (Control ctl in this.Controls)
            {
                try
                {
                    // Attempt to cast the control to type MdiClient.
                    ctlMDI = (MdiClient)ctl;

                    // Set the BackColor of the MdiClient control.
                    ctlMDI.BackColor = this.BackColor;
                }
                catch (InvalidCastException exc)
                {
                    // Catch and ignore the error if casting failed.
                }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            fecharFormulario();
            abertura.MdiParent = this;
            abertura.Show();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            fecharFormulario();
            tabelas.MdiParent = this;
            tabelas.Show();
        }

        private void toolStripButton17_Click(object sender, EventArgs e)
        {
            fecharFormulario();
            entrada.MdiParent = this;
            entrada.Show();
        }

        private void toolStripButton12_Click(object sender, EventArgs e)
        {
            
            fecharFormulario();
            importacao.MdiParent = this;
            importacao.Show();
        }

        private void toolStripButton11_Click(object sender, EventArgs e)
        {
           
            fecharFormulario();
            entradasaida.MdiParent = this;
            entradasaida.Show();
        }

        private void toolStripButton16_Click(object sender, EventArgs e)
        {
            fecharFormulario();
            relatorio.MdiParent = this;
            relatorio.Show();
        }

        private void toolStripButton18_Click(object sender, EventArgs e)
        {
            FormAberturaFechodoDia form = new FormAberturaFechodoDia();
            //MessageBox.Show(form.data()); 
            DateTime agora = Variavel.dataAtual;
            int dia = agora.Day;
            int mes = agora.Month;
            int ano = agora.Year;
            dataA = "" + ano + "-" + mes + "-" + dia;
            if (form.data() != "0")
            {
                if (form.fecho() == 0)
                {
                    DateTime dt;
                    DateTime dt2;
                    if (DateTime.TryParse(form.data(), out dt) && DateTime.TryParse(dataA, out dt2))
                    {
                        if (dt == dt2)
                        {
                            fecharFormulario();
                            venda.MdiParent = this;
                            venda.Show();
                        }
                        else
                            MessageBox.Show("O dia " + form.data() + " Continua aberto", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    DateTime dt;
                    DateTime dt2;
                    if (DateTime.TryParse(form.data(), out dt) && DateTime.TryParse(dataA, out dt2))
                    {
                        if (dt == dt2)
                        {
                            MessageBox.Show("O dia já foi fechado", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                            MessageBox.Show("O dia ainda não foi Aberto", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
                MessageBox.Show("Não Existe Nem uma data cadastrada ");
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
           
            fecharFormulario();
            nota.MdiParent = this;
            nota.Show();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            fecharFormulario();
            factura.MdiParent = this;
            factura.Show();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            fecharFormulario();
            factura2.MdiParent = this;
            factura2.Show();
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            fecharFormulario();
            proforma.MdiParent = this;
            proforma.Show();
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            fecharFormulario();
            pagamento.MdiParent = this;
            pagamento.Show();
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            fecharFormulario();
            requisicao.MdiParent = this;
            requisicao.Show();
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            fecharFormulario();
            transferencias.MdiParent = this;
            transferencias.Show();
        }

        private void toolStripButton19_Click(object sender, EventArgs e)
        {
            fecharFormulario();
            inventario.MdiParent = this;
            inventario.Show();
        }

        private void toolStripButton10_Click(object sender, EventArgs e)
        {
          
            fecharFormulario();
            gestao.MdiParent = this;
            gestao.Show();
        }

        private void toolStripButton15_Click(object sender, EventArgs e)
        {
           
            fecharFormulario();
            pagamentoV.MdiParent = this;
            pagamentoV.Show();
        }

        private void toolStripButton13_Click(object sender, EventArgs e)
        {
           
            
            fecharFormulario();
            registro.MdiParent = this;
            registro.Show();
        }

        private void toolStripButton14_Click(object sender, EventArgs e)
        {
           
        }
    }
}
