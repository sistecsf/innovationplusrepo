﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendaEntrega : MetroFramework.Forms.MetroForm
    {
        public FormVendaEntrega()
        {
            InitializeComponent();
        }
        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FormVendas.strEntrega_Empresa = txtEmpresa.Text;
            FormVendas.strEntrega_Morada = txtMorada.Text;
            FormVendas.strEntrega_PReferencia = txtPontoReferencia.Text;
            FormVendas.strEntrega_Contacto = txtContacto.Text;
            FormVendas.strEntrega_Telefone = txtTelefone.Text;
            FormVendas.strEntrega_Data = txtData.Text;
            FormVendas.strEntrega_Hora = txtHora.Text;

            this.DialogResult = DialogResult.OK;
        }

        private void txtData_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtData.Text); }
            catch (Exception ex)
            {
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!");
                txtData.Text = "";
            }
        }

        private void txtHora_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtHora.Text); }
            catch (Exception ex)
            {
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Hora Válida!");
                txtHora.Text = "";
            }
        }
    }
}
