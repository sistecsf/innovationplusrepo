﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS;
using SISTEC_PLUS;
using SISTEC_PLUS.OPS;
using System.Drawing.Imaging;
using System.IO;

namespace SISTEC_PLUS.POS
{
    public partial class FormTabelaLojaConsultar : MetroFramework.Forms.MetroForm
    {
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao); // conexão com o banco
        string sql;
        public FormTabelaLojaConsultar()
        {
            InitializeComponent();
        }
        private void prenxeGrid()
        {
            sql = @"select CodLoja, NomeLoja, Morada, Caixa_post, Fone, Fax, Email from ASLOJA where CodLoja like  '%" + txtCodigo.Text + "%' ";
            if (txtCodigo.Text != "")
                sql = sql + "and CodLoja =  '" + txtCodigo.Text + "' ";
            if (txtNome.Text != "")
                sql = sql + "and NomeLoja =  '" + txtNome.Text + "'";
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("Loja");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void gridDuploClick(DataGridView dataGrid)
        {
            FormTabelas.Lcodigo = dataGrid.CurrentRow.Cells[0].Value.ToString();
            FormTabelas.Lnome = dataGrid.CurrentRow.Cells[1].Value.ToString();
            FormTabelas.Lmorada = dataGrid.CurrentRow.Cells[2].Value.ToString();
            FormTabelas.Lcaixa_post = dataGrid.CurrentRow.Cells[3].Value.ToString();
            FormTabelas.Ltelefone = dataGrid.CurrentRow.Cells[4].Value.ToString();
            FormTabelas.Lfax = dataGrid.CurrentRow.Cells[5].Value.ToString();

            SqlCommand cmd2 = new SqlCommand("select LojaOnLine, Obriga_FechoDia, AGTELEC, EMITIR_RC, Visita_Definida, CodContaNAC, CodContaPDR, CodContaOUTROS, CodEmpresa from ASLOJA where CodLoja = '" + FormTabelas.Lcodigo + "' ", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd2.ExecuteReader())
            {
                while (reader.Read())
                {
                    FormTabelas.visualisar = reader["LojaOnLine"].ToString();
                    FormTabelas.agente = reader["AGTELEC"].ToString();
                    FormTabelas.emitir = reader["EMITIR_RC"].ToString();
                    FormTabelas.abertura = reader["Obriga_FechoDia"].ToString();
                    FormTabelas.visita = reader["Visita_Definida"].ToString();
                    FormTabelas.LcontaN = reader["CodContaNAC"].ToString();
                    FormTabelas.LcontaO = reader["CodContaOUTROS"].ToString();
                    FormTabelas.LcontaP = reader["CodContaPDR"].ToString();
                    FormTabelas.Lempresa = reader["CodEmpresa"].ToString();
                }
            }
            conexao.Close();

            SqlCommand cmd = new SqlCommand("select NOME from EMPRESA where CodEmpresa=@empresa ", conexao);
            cmd.Parameters.Add(new SqlParameter("@empresa", FormTabelas.Lempresa));
            conexao.Open();
            FormTabelas.empresa = (string)cmd.ExecuteScalar();
            conexao.Close();
            try
            {
                SqlCommand cmdSelect = new SqlCommand("select LOGO from ASLOJA where CodLoja =@ID ", conexao);
                cmdSelect.Parameters.Add("@ID", SqlDbType.Char, 4);
                cmdSelect.Parameters["@ID"].Value = FormTabelas.Lcodigo;

                conexao.Open();
                FormTabelas.foto = (byte[])cmdSelect.ExecuteScalar();
                FormTabelas.Limagem = Convert.ToString(DateTime.Now.ToFileTime());
                FileStream fs = new FileStream(FormTabelas.Limagem, FileMode.CreateNew, FileAccess.Write);

                fs.Write(FormTabelas.foto, 0, FormTabelas.foto.Length);
                fs.Flush();
                fs.Close();
            }
            catch (Exception) { }
            finally { conexao.Close(); }

            this.DialogResult = DialogResult.OK;
        }
        private void FormTabelaLojaConsultar_Load(object sender, EventArgs e)
        {
            metroGrid1.AutoGenerateColumns = false;
            prenxeGrid();
        }

        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    try { gridDuploClick(metroGrid1); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                Cursor.Current = Cursors.WaitCursor;
                prenxeGrid();
            }
        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try { gridDuploClick(metroGrid1); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }
    }
}
