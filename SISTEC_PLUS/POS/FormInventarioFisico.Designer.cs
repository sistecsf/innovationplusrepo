﻿namespace SISTEC_PLUS.POS
{
    partial class FormInventarioFisico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInventarioFisico));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.lbAberto = new MetroFramework.Controls.MetroLabel();
            this.lbACodLoja = new MetroFramework.Controls.MetroLabel();
            this.txtADataControle = new MetroFramework.Controls.MetroTextBox();
            this.txtAFechadoPor = new MetroFramework.Controls.MetroTextBox();
            this.txtAAbertoPor = new MetroFramework.Controls.MetroTextBox();
            this.cmbAInventarioDia = new MetroFramework.Controls.MetroComboBox();
            this.cmbAInvertarioLoja = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.cbFechoInventario = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btAGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.lbRDescricao = new MetroFramework.Controls.MetroLabel();
            this.lbRArmazem = new MetroFramework.Controls.MetroLabel();
            this.lbRLoja = new MetroFramework.Controls.MetroLabel();
            this.metroGrid1 = new MetroFramework.Controls.MetroGrid();
            this.Referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mercadoria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Localizacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdSist = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdFis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdRec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbRReferencia = new MetroFramework.Controls.MetroComboBox();
            this.cmbRMercadoria = new MetroFramework.Controls.MetroComboBox();
            this.cmbRLoja = new MetroFramework.Controls.MetroComboBox();
            this.cmbRDataInventrario = new MetroFramework.Controls.MetroComboBox();
            this.cbRAberturaLoja = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btRGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer2 = new System.Windows.Forms.ToolStripContainer();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.txtETotal3 = new MetroFramework.Controls.MetroTextBox();
            this.txtETotal2 = new MetroFramework.Controls.MetroTextBox();
            this.txtETotal1 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel26 = new MetroFramework.Controls.MetroLabel();
            this.metroGrid2 = new MetroFramework.Controls.MetroGrid();
            this.Referencia2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Un = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mercadoria2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PCA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Localizacao2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroComboBox6 = new MetroFramework.Controls.MetroComboBox();
            this.txtEPCAP = new MetroFramework.Controls.MetroTextBox();
            this.txtEFOB = new MetroFramework.Controls.MetroTextBox();
            this.txtEInvoiceN = new MetroFramework.Controls.MetroTextBox();
            this.txtETotal = new MetroFramework.Controls.MetroTextBox();
            this.txtEDesc = new MetroFramework.Controls.MetroTextBox();
            this.txtEQtd = new MetroFramework.Controls.MetroTextBox();
            this.txtELocalizacao = new MetroFramework.Controls.MetroTextBox();
            this.cmbEReferencia = new MetroFramework.Controls.MetroComboBox();
            this.txtEGuia = new MetroFramework.Controls.MetroTextBox();
            this.txtEMovStock = new MetroFramework.Controls.MetroTextBox();
            this.txtENumero = new MetroFramework.Controls.MetroTextBox();
            this.txtEFechado = new MetroFramework.Controls.MetroTextBox();
            this.txtEOrigemEntrada = new MetroFramework.Controls.MetroTextBox();
            this.metroComboBox4 = new MetroFramework.Controls.MetroComboBox();
            this.txtEFornecedor = new MetroFramework.Controls.MetroTextBox();
            this.metroComboBox3 = new MetroFramework.Controls.MetroComboBox();
            this.cmbELoja = new MetroFramework.Controls.MetroComboBox();
            this.cmbEDataInventario = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel24 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel23 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel22 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.btENovo = new System.Windows.Forms.ToolStripButton();
            this.btEGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer3 = new System.Windows.Forms.ToolStripContainer();
            this.metroTabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.metroGrid3 = new MetroFramework.Controls.MetroGrid();
            this.cmbZLoja = new MetroFramework.Controls.MetroComboBox();
            this.cmbZData = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel28 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel27 = new MetroFramework.Controls.MetroLabel();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.btZGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer4 = new System.Windows.Forms.ToolStripContainer();
            this.metroTabPage5 = new MetroFramework.Controls.MetroTabPage();
            this.metroGrid4 = new MetroFramework.Controls.MetroGrid();
            this.toolStrip5 = new System.Windows.Forms.ToolStrip();
            this.btAcImprimir = new System.Windows.Forms.ToolStripButton();
            this.btAcGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer5 = new System.Windows.Forms.ToolStripContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtDataFinal = new System.Windows.Forms.MaskedTextBox();
            this.txtDataInicial = new System.Windows.Forms.MaskedTextBox();
            this.cmbAcDataInventario = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel31 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel30 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel29 = new MetroFramework.Controls.MetroLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbAcSaelecaoLoja = new MetroFramework.Controls.MetroComboBox();
            this.metroTabPage6 = new MetroFramework.Controls.MetroTabPage();
            this.txtFDataControlo = new MetroFramework.Controls.MetroTextBox();
            this.txtFFechadopor = new MetroFramework.Controls.MetroTextBox();
            this.txtFAbertoPor = new MetroFramework.Controls.MetroTextBox();
            this.cmbFInventarioDia = new MetroFramework.Controls.MetroComboBox();
            this.cmbFInventarioLoja = new MetroFramework.Controls.MetroComboBox();
            this.cbFFecharInventario = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel36 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel35 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel34 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel33 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel32 = new MetroFramework.Controls.MetroLabel();
            this.toolStrip6 = new System.Windows.Forms.ToolStrip();
            this.btFGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer6 = new System.Windows.Forms.ToolStripContainer();
            this.metroTabPage7 = new MetroFramework.Controls.MetroTabPage();
            this.metroGrid5 = new MetroFramework.Controls.MetroGrid();
            this.Referencia4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mercadoria4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Localizacao4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdSistema = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdContada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroComboBox2 = new MetroFramework.Controls.MetroComboBox();
            this.metroComboBox1 = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel38 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel37 = new MetroFramework.Controls.MetroLabel();
            this.toolStrip7 = new System.Windows.Forms.ToolStrip();
            this.btGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer7 = new System.Windows.Forms.ToolStripContainer();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lbZLoja = new MetroFramework.Controls.MetroLabel();
            this.Mercadoria1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbACLoja = new MetroFramework.Controls.MetroLabel();
            this.Referencia3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mercadoria3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QtdMov = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoMov = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataMov = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroTabControl1.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.toolStripContainer2.SuspendLayout();
            this.metroTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid2)).BeginInit();
            this.toolStrip3.SuspendLayout();
            this.toolStripContainer3.SuspendLayout();
            this.metroTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid3)).BeginInit();
            this.toolStrip4.SuspendLayout();
            this.toolStripContainer4.SuspendLayout();
            this.metroTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid4)).BeginInit();
            this.toolStrip5.SuspendLayout();
            this.toolStripContainer5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.metroTabPage6.SuspendLayout();
            this.toolStrip6.SuspendLayout();
            this.toolStripContainer6.SuspendLayout();
            this.metroTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid5)).BeginInit();
            this.toolStrip7.SuspendLayout();
            this.toolStripContainer7.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.metroTabPage2);
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Controls.Add(this.metroTabPage3);
            this.metroTabControl1.Controls.Add(this.metroTabPage4);
            this.metroTabControl1.Controls.Add(this.metroTabPage5);
            this.metroTabControl1.Controls.Add(this.metroTabPage6);
            this.metroTabControl1.Controls.Add(this.metroTabPage7);
            this.metroTabControl1.Location = new System.Drawing.Point(5, 63);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 4;
            this.metroTabControl1.Size = new System.Drawing.Size(993, 501);
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage1.Controls.Add(this.lbAberto);
            this.metroTabPage1.Controls.Add(this.lbACodLoja);
            this.metroTabPage1.Controls.Add(this.txtADataControle);
            this.metroTabPage1.Controls.Add(this.txtAFechadoPor);
            this.metroTabPage1.Controls.Add(this.txtAAbertoPor);
            this.metroTabPage1.Controls.Add(this.cmbAInventarioDia);
            this.metroTabPage1.Controls.Add(this.cmbAInvertarioLoja);
            this.metroTabPage1.Controls.Add(this.metroLabel5);
            this.metroTabPage1.Controls.Add(this.metroLabel4);
            this.metroTabPage1.Controls.Add(this.cbFechoInventario);
            this.metroTabPage1.Controls.Add(this.metroLabel3);
            this.metroTabPage1.Controls.Add(this.metroLabel2);
            this.metroTabPage1.Controls.Add(this.metroLabel1);
            this.metroTabPage1.Controls.Add(this.toolStrip1);
            this.metroTabPage1.Controls.Add(this.toolStripContainer1);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(985, 459);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Abertura de Inventário";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // lbAberto
            // 
            this.lbAberto.AutoSize = true;
            this.lbAberto.Location = new System.Drawing.Point(432, 71);
            this.lbAberto.Name = "lbAberto";
            this.lbAberto.Size = new System.Drawing.Size(0, 0);
            this.lbAberto.TabIndex = 103;
            // 
            // lbACodLoja
            // 
            this.lbACodLoja.AutoSize = true;
            this.lbACodLoja.Location = new System.Drawing.Point(776, 127);
            this.lbACodLoja.Name = "lbACodLoja";
            this.lbACodLoja.Size = new System.Drawing.Size(16, 19);
            this.lbACodLoja.TabIndex = 102;
            this.lbACodLoja.Text = "F";
            // 
            // txtADataControle
            // 
            this.txtADataControle.Lines = new string[0];
            this.txtADataControle.Location = new System.Drawing.Point(329, 249);
            this.txtADataControle.MaxLength = 32767;
            this.txtADataControle.Name = "txtADataControle";
            this.txtADataControle.PasswordChar = '\0';
            this.txtADataControle.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtADataControle.SelectedText = "";
            this.txtADataControle.Size = new System.Drawing.Size(164, 23);
            this.txtADataControle.TabIndex = 101;
            this.txtADataControle.UseSelectable = true;
            // 
            // txtAFechadoPor
            // 
            this.txtAFechadoPor.Lines = new string[0];
            this.txtAFechadoPor.Location = new System.Drawing.Point(606, 206);
            this.txtAFechadoPor.MaxLength = 32767;
            this.txtAFechadoPor.Name = "txtAFechadoPor";
            this.txtAFechadoPor.PasswordChar = '\0';
            this.txtAFechadoPor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAFechadoPor.SelectedText = "";
            this.txtAFechadoPor.Size = new System.Drawing.Size(164, 23);
            this.txtAFechadoPor.TabIndex = 100;
            this.txtAFechadoPor.UseSelectable = true;
            // 
            // txtAAbertoPor
            // 
            this.txtAAbertoPor.Lines = new string[0];
            this.txtAAbertoPor.Location = new System.Drawing.Point(329, 206);
            this.txtAAbertoPor.MaxLength = 32767;
            this.txtAAbertoPor.Name = "txtAAbertoPor";
            this.txtAAbertoPor.PasswordChar = '\0';
            this.txtAAbertoPor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtAAbertoPor.SelectedText = "";
            this.txtAAbertoPor.Size = new System.Drawing.Size(164, 23);
            this.txtAAbertoPor.TabIndex = 99;
            this.txtAAbertoPor.UseSelectable = true;
            // 
            // cmbAInventarioDia
            // 
            this.cmbAInventarioDia.FormattingEnabled = true;
            this.cmbAInventarioDia.IntegralHeight = false;
            this.cmbAInventarioDia.ItemHeight = 23;
            this.cmbAInventarioDia.Location = new System.Drawing.Point(329, 162);
            this.cmbAInventarioDia.Name = "cmbAInventarioDia";
            this.cmbAInventarioDia.Size = new System.Drawing.Size(164, 29);
            this.cmbAInventarioDia.TabIndex = 98;
            this.cmbAInventarioDia.UseSelectable = true;
            this.cmbAInventarioDia.DropDown += new System.EventHandler(this.cmbAInventarioDia_DropDown);
            this.cmbAInventarioDia.SelectedIndexChanged += new System.EventHandler(this.cmbAInventarioDia_SelectedIndexChanged);
            // 
            // cmbAInvertarioLoja
            // 
            this.cmbAInvertarioLoja.FormattingEnabled = true;
            this.cmbAInvertarioLoja.IntegralHeight = false;
            this.cmbAInvertarioLoja.ItemHeight = 23;
            this.cmbAInvertarioLoja.Location = new System.Drawing.Point(329, 117);
            this.cmbAInvertarioLoja.Name = "cmbAInvertarioLoja";
            this.cmbAInvertarioLoja.Size = new System.Drawing.Size(441, 29);
            this.cmbAInvertarioLoja.TabIndex = 97;
            this.cmbAInvertarioLoja.UseSelectable = true;
            this.cmbAInvertarioLoja.DropDown += new System.EventHandler(this.cmbAInvertarioLoja_DropDown);
            this.cmbAInvertarioLoja.SelectedIndexChanged += new System.EventHandler(this.cmbAInvertarioLoja_SelectedIndexChanged);
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(177, 253);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(91, 19);
            this.metroLabel5.TabIndex = 96;
            this.metroLabel5.Text = "Data Controle";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(516, 210);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(84, 19);
            this.metroLabel4.TabIndex = 95;
            this.metroLabel4.Text = "Fechado Por";
            // 
            // cbFechoInventario
            // 
            this.cbFechoInventario.AutoSize = true;
            this.cbFechoInventario.Location = new System.Drawing.Point(557, 176);
            this.cbFechoInventario.Name = "cbFechoInventario";
            this.cbFechoInventario.Size = new System.Drawing.Size(114, 15);
            this.cbFechoInventario.TabIndex = 94;
            this.cbFechoInventario.Text = "Fechar Inventário";
            this.cbFechoInventario.UseSelectable = true;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(177, 210);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(75, 19);
            this.metroLabel3.TabIndex = 93;
            this.metroLabel3.Text = "Aberto Por";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(177, 172);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(109, 19);
            this.metroLabel2.TabIndex = 92;
            this.metroLabel2.Text = "Inventário do Dia";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(177, 127);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(113, 19);
            this.metroLabel1.TabIndex = 91;
            this.metroLabel1.Text = "Inventário da Loja";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btAGravar});
            this.toolStrip1.Location = new System.Drawing.Point(449, 20);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(64, 25);
            this.toolStrip1.TabIndex = 89;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btAGravar
            // 
            this.btAGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btAGravar.Image = ((System.Drawing.Image)(resources.GetObject("btAGravar.Image")));
            this.btAGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btAGravar.Name = "btAGravar";
            this.btAGravar.Size = new System.Drawing.Size(61, 22);
            this.btAGravar.Text = "Gravar";
            this.btAGravar.Click += new System.EventHandler(this.btAGravar_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(138, 2);
            this.toolStripContainer1.Location = new System.Drawing.Point(449, 20);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(138, 27);
            this.toolStripContainer1.TabIndex = 90;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer1.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer1.TopToolStripPanel.Tag = "XC";
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage2.Controls.Add(this.lbRDescricao);
            this.metroTabPage2.Controls.Add(this.lbRArmazem);
            this.metroTabPage2.Controls.Add(this.lbRLoja);
            this.metroTabPage2.Controls.Add(this.metroGrid1);
            this.metroTabPage2.Controls.Add(this.cmbRReferencia);
            this.metroTabPage2.Controls.Add(this.cmbRMercadoria);
            this.metroTabPage2.Controls.Add(this.cmbRLoja);
            this.metroTabPage2.Controls.Add(this.cmbRDataInventrario);
            this.metroTabPage2.Controls.Add(this.cbRAberturaLoja);
            this.metroTabPage2.Controls.Add(this.metroLabel9);
            this.metroTabPage2.Controls.Add(this.metroLabel8);
            this.metroTabPage2.Controls.Add(this.metroLabel7);
            this.metroTabPage2.Controls.Add(this.metroLabel6);
            this.metroTabPage2.Controls.Add(this.toolStrip2);
            this.metroTabPage2.Controls.Add(this.toolStripContainer2);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(985, 459);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Recolha do Inventário";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // lbRDescricao
            // 
            this.lbRDescricao.AutoSize = true;
            this.lbRDescricao.Location = new System.Drawing.Point(506, 183);
            this.lbRDescricao.Name = "lbRDescricao";
            this.lbRDescricao.Size = new System.Drawing.Size(15, 19);
            this.lbRDescricao.TabIndex = 103;
            this.lbRDescricao.Text = "L";
            // 
            // lbRArmazem
            // 
            this.lbRArmazem.AutoSize = true;
            this.lbRArmazem.Location = new System.Drawing.Point(858, 148);
            this.lbRArmazem.Name = "lbRArmazem";
            this.lbRArmazem.Size = new System.Drawing.Size(15, 19);
            this.lbRArmazem.TabIndex = 102;
            this.lbRArmazem.Text = "L";
            // 
            // lbRLoja
            // 
            this.lbRLoja.AutoSize = true;
            this.lbRLoja.Location = new System.Drawing.Point(858, 113);
            this.lbRLoja.Name = "lbRLoja";
            this.lbRLoja.Size = new System.Drawing.Size(15, 19);
            this.lbRLoja.TabIndex = 101;
            this.lbRLoja.Text = "L";
            // 
            // metroGrid1
            // 
            this.metroGrid1.AllowUserToResizeRows = false;
            this.metroGrid1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.metroGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Referencia,
            this.Mercadoria,
            this.Localizacao,
            this.QtdSist,
            this.QtdFis,
            this.QtdRec});
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1.DefaultCellStyle = dataGridViewCellStyle20;
            this.metroGrid1.EnableHeadersVisualStyles = false;
            this.metroGrid1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid1.Location = new System.Drawing.Point(134, 224);
            this.metroGrid1.Name = "metroGrid1";
            this.metroGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.metroGrid1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1.Size = new System.Drawing.Size(718, 150);
            this.metroGrid1.TabIndex = 100;
            // 
            // Referencia
            // 
            this.Referencia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Referencia.HeaderText = "Referência";
            this.Referencia.Name = "Referencia";
            // 
            // Mercadoria
            // 
            this.Mercadoria.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Mercadoria.HeaderText = "Mercadoria";
            this.Mercadoria.Name = "Mercadoria";
            // 
            // Localizacao
            // 
            this.Localizacao.HeaderText = "Localização";
            this.Localizacao.Name = "Localizacao";
            // 
            // QtdSist
            // 
            this.QtdSist.HeaderText = "Qtd. Sist";
            this.QtdSist.Name = "QtdSist";
            // 
            // QtdFis
            // 
            this.QtdFis.HeaderText = "Qtd. Fis";
            this.QtdFis.Name = "QtdFis";
            // 
            // QtdRec
            // 
            this.QtdRec.HeaderText = "Qtd. Rec";
            this.QtdRec.Name = "QtdRec";
            // 
            // cmbRReferencia
            // 
            this.cmbRReferencia.FormattingEnabled = true;
            this.cmbRReferencia.IntegralHeight = false;
            this.cmbRReferencia.ItemHeight = 23;
            this.cmbRReferencia.Location = new System.Drawing.Point(237, 173);
            this.cmbRReferencia.Name = "cmbRReferencia";
            this.cmbRReferencia.Size = new System.Drawing.Size(256, 29);
            this.cmbRReferencia.TabIndex = 99;
            this.cmbRReferencia.UseSelectable = true;
            this.cmbRReferencia.DropDown += new System.EventHandler(this.cmbRReferencia_DropDown);
            this.cmbRReferencia.SelectedIndexChanged += new System.EventHandler(this.cmbRReferencia_SelectedIndexChanged);
            // 
            // cmbRMercadoria
            // 
            this.cmbRMercadoria.FormattingEnabled = true;
            this.cmbRMercadoria.IntegralHeight = false;
            this.cmbRMercadoria.ItemHeight = 23;
            this.cmbRMercadoria.Location = new System.Drawing.Point(237, 138);
            this.cmbRMercadoria.Name = "cmbRMercadoria";
            this.cmbRMercadoria.Size = new System.Drawing.Size(615, 29);
            this.cmbRMercadoria.TabIndex = 98;
            this.cmbRMercadoria.UseSelectable = true;
            this.cmbRMercadoria.DropDown += new System.EventHandler(this.cmbRMercadoria_DropDown);
            this.cmbRMercadoria.SelectedIndexChanged += new System.EventHandler(this.cmbRMercadoria_SelectedIndexChanged);
            // 
            // cmbRLoja
            // 
            this.cmbRLoja.FormattingEnabled = true;
            this.cmbRLoja.IntegralHeight = false;
            this.cmbRLoja.ItemHeight = 23;
            this.cmbRLoja.Location = new System.Drawing.Point(237, 103);
            this.cmbRLoja.Name = "cmbRLoja";
            this.cmbRLoja.Size = new System.Drawing.Size(615, 29);
            this.cmbRLoja.TabIndex = 97;
            this.cmbRLoja.UseSelectable = true;
            this.cmbRLoja.DropDown += new System.EventHandler(this.cmbRLoja_DropDown);
            this.cmbRLoja.SelectedIndexChanged += new System.EventHandler(this.cmbRLoja_SelectedIndexChanged);
            // 
            // cmbRDataInventrario
            // 
            this.cmbRDataInventrario.FormattingEnabled = true;
            this.cmbRDataInventrario.IntegralHeight = false;
            this.cmbRDataInventrario.ItemHeight = 23;
            this.cmbRDataInventrario.Location = new System.Drawing.Point(237, 68);
            this.cmbRDataInventrario.Name = "cmbRDataInventrario";
            this.cmbRDataInventrario.Size = new System.Drawing.Size(256, 29);
            this.cmbRDataInventrario.TabIndex = 96;
            this.cmbRDataInventrario.UseSelectable = true;
            this.cmbRDataInventrario.DropDown += new System.EventHandler(this.cmbRDataInventrario_DropDown);
            // 
            // cbRAberturaLoja
            // 
            this.cbRAberturaLoja.AutoSize = true;
            this.cbRAberturaLoja.Location = new System.Drawing.Point(506, 82);
            this.cbRAberturaLoja.Name = "cbRAberturaLoja";
            this.cbRAberturaLoja.Size = new System.Drawing.Size(165, 15);
            this.cbRAberturaLoja.TabIndex = 95;
            this.cbRAberturaLoja.Text = "Abertura Loja / Mercadoria";
            this.cbRAberturaLoja.UseSelectable = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(134, 183);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(70, 19);
            this.metroLabel9.TabIndex = 94;
            this.metroLabel9.Text = "Referência";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(134, 148);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(77, 19);
            this.metroLabel8.TabIndex = 93;
            this.metroLabel8.Text = "Mercadoria";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(134, 113);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(33, 19);
            this.metroLabel7.TabIndex = 92;
            this.metroLabel7.Text = "Loja";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(134, 78);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(97, 19);
            this.metroLabel6.TabIndex = 91;
            this.metroLabel6.Text = "Data Inventário";
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btRGravar});
            this.toolStrip2.Location = new System.Drawing.Point(449, 19);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(64, 25);
            this.toolStrip2.TabIndex = 89;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btRGravar
            // 
            this.btRGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btRGravar.Image = ((System.Drawing.Image)(resources.GetObject("btRGravar.Image")));
            this.btRGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btRGravar.Name = "btRGravar";
            this.btRGravar.Size = new System.Drawing.Size(61, 22);
            this.btRGravar.Text = "Gravar";
            this.btRGravar.Click += new System.EventHandler(this.btRGravar_Click);
            // 
            // toolStripContainer2
            // 
            // 
            // toolStripContainer2.ContentPanel
            // 
            this.toolStripContainer2.ContentPanel.Size = new System.Drawing.Size(103, 2);
            this.toolStripContainer2.Location = new System.Drawing.Point(449, 19);
            this.toolStripContainer2.Name = "toolStripContainer2";
            this.toolStripContainer2.Size = new System.Drawing.Size(103, 27);
            this.toolStripContainer2.TabIndex = 90;
            this.toolStripContainer2.Text = "toolStripContainer2";
            // 
            // toolStripContainer2.TopToolStripPanel
            // 
            this.toolStripContainer2.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer2.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer2.TopToolStripPanel.Tag = "XC";
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage3.Controls.Add(this.txtETotal3);
            this.metroTabPage3.Controls.Add(this.txtETotal2);
            this.metroTabPage3.Controls.Add(this.txtETotal1);
            this.metroTabPage3.Controls.Add(this.metroLabel26);
            this.metroTabPage3.Controls.Add(this.metroGrid2);
            this.metroTabPage3.Controls.Add(this.metroComboBox6);
            this.metroTabPage3.Controls.Add(this.txtEPCAP);
            this.metroTabPage3.Controls.Add(this.txtEFOB);
            this.metroTabPage3.Controls.Add(this.txtEInvoiceN);
            this.metroTabPage3.Controls.Add(this.txtETotal);
            this.metroTabPage3.Controls.Add(this.txtEDesc);
            this.metroTabPage3.Controls.Add(this.txtEQtd);
            this.metroTabPage3.Controls.Add(this.txtELocalizacao);
            this.metroTabPage3.Controls.Add(this.cmbEReferencia);
            this.metroTabPage3.Controls.Add(this.txtEGuia);
            this.metroTabPage3.Controls.Add(this.txtEMovStock);
            this.metroTabPage3.Controls.Add(this.txtENumero);
            this.metroTabPage3.Controls.Add(this.txtEFechado);
            this.metroTabPage3.Controls.Add(this.txtEOrigemEntrada);
            this.metroTabPage3.Controls.Add(this.metroComboBox4);
            this.metroTabPage3.Controls.Add(this.txtEFornecedor);
            this.metroTabPage3.Controls.Add(this.metroComboBox3);
            this.metroTabPage3.Controls.Add(this.cmbELoja);
            this.metroTabPage3.Controls.Add(this.cmbEDataInventario);
            this.metroTabPage3.Controls.Add(this.metroLabel25);
            this.metroTabPage3.Controls.Add(this.metroLabel24);
            this.metroTabPage3.Controls.Add(this.metroLabel23);
            this.metroTabPage3.Controls.Add(this.metroLabel22);
            this.metroTabPage3.Controls.Add(this.metroLabel21);
            this.metroTabPage3.Controls.Add(this.metroLabel20);
            this.metroTabPage3.Controls.Add(this.metroLabel19);
            this.metroTabPage3.Controls.Add(this.metroLabel18);
            this.metroTabPage3.Controls.Add(this.metroLabel17);
            this.metroTabPage3.Controls.Add(this.metroLabel16);
            this.metroTabPage3.Controls.Add(this.metroLabel15);
            this.metroTabPage3.Controls.Add(this.metroLabel14);
            this.metroTabPage3.Controls.Add(this.metroLabel13);
            this.metroTabPage3.Controls.Add(this.metroLabel12);
            this.metroTabPage3.Controls.Add(this.metroLabel11);
            this.metroTabPage3.Controls.Add(this.metroLabel10);
            this.metroTabPage3.Controls.Add(this.toolStrip3);
            this.metroTabPage3.Controls.Add(this.toolStripContainer3);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(985, 459);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Entrada de Inventário";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // txtETotal3
            // 
            this.txtETotal3.Lines = new string[0];
            this.txtETotal3.Location = new System.Drawing.Point(319, 401);
            this.txtETotal3.MaxLength = 32767;
            this.txtETotal3.Name = "txtETotal3";
            this.txtETotal3.PasswordChar = '\0';
            this.txtETotal3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtETotal3.SelectedText = "";
            this.txtETotal3.Size = new System.Drawing.Size(92, 23);
            this.txtETotal3.TabIndex = 130;
            this.txtETotal3.UseSelectable = true;
            // 
            // txtETotal2
            // 
            this.txtETotal2.Lines = new string[0];
            this.txtETotal2.Location = new System.Drawing.Point(197, 401);
            this.txtETotal2.MaxLength = 32767;
            this.txtETotal2.Name = "txtETotal2";
            this.txtETotal2.PasswordChar = '\0';
            this.txtETotal2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtETotal2.SelectedText = "";
            this.txtETotal2.Size = new System.Drawing.Size(92, 23);
            this.txtETotal2.TabIndex = 129;
            this.txtETotal2.UseSelectable = true;
            // 
            // txtETotal1
            // 
            this.txtETotal1.Lines = new string[0];
            this.txtETotal1.Location = new System.Drawing.Point(61, 401);
            this.txtETotal1.MaxLength = 32767;
            this.txtETotal1.Name = "txtETotal1";
            this.txtETotal1.PasswordChar = '\0';
            this.txtETotal1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtETotal1.SelectedText = "";
            this.txtETotal1.Size = new System.Drawing.Size(92, 23);
            this.txtETotal1.TabIndex = 128;
            this.txtETotal1.UseSelectable = true;
            // 
            // metroLabel26
            // 
            this.metroLabel26.AutoSize = true;
            this.metroLabel26.Location = new System.Drawing.Point(-4, 405);
            this.metroLabel26.Name = "metroLabel26";
            this.metroLabel26.Size = new System.Drawing.Size(38, 19);
            this.metroLabel26.TabIndex = 127;
            this.metroLabel26.Text = "Total";
            // 
            // metroGrid2
            // 
            this.metroGrid2.AllowUserToResizeRows = false;
            this.metroGrid2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.metroGrid2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Referencia2,
            this.Un,
            this.Quantidade,
            this.Mercadoria2,
            this.FOB,
            this.PCA,
            this.Desc,
            this.Total,
            this.Localizacao2});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid2.DefaultCellStyle = dataGridViewCellStyle23;
            this.metroGrid2.EnableHeadersVisualStyles = false;
            this.metroGrid2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid2.Location = new System.Drawing.Point(0, 230);
            this.metroGrid2.Name = "metroGrid2";
            this.metroGrid2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid2.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.metroGrid2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid2.Size = new System.Drawing.Size(982, 150);
            this.metroGrid2.TabIndex = 126;
            // 
            // Referencia2
            // 
            this.Referencia2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Referencia2.HeaderText = "Referência";
            this.Referencia2.Name = "Referencia2";
            // 
            // Un
            // 
            this.Un.HeaderText = "Un";
            this.Un.Name = "Un";
            // 
            // Quantidade
            // 
            this.Quantidade.HeaderText = "Mercadoria";
            this.Quantidade.Name = "Quantidade";
            // 
            // Mercadoria2
            // 
            this.Mercadoria2.HeaderText = "Quantidade";
            this.Mercadoria2.Name = "Mercadoria2";
            // 
            // FOB
            // 
            this.FOB.HeaderText = "FOB";
            this.FOB.Name = "FOB";
            // 
            // PCA
            // 
            this.PCA.HeaderText = "PCA";
            this.PCA.Name = "PCA";
            // 
            // Desc
            // 
            this.Desc.HeaderText = "% Desc";
            this.Desc.Name = "Desc";
            // 
            // Total
            // 
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            // 
            // Localizacao2
            // 
            this.Localizacao2.HeaderText = "Column1Localização";
            this.Localizacao2.Name = "Localizacao2";
            // 
            // metroComboBox6
            // 
            this.metroComboBox6.FormattingEnabled = true;
            this.metroComboBox6.IntegralHeight = false;
            this.metroComboBox6.ItemHeight = 23;
            this.metroComboBox6.Location = new System.Drawing.Point(125, 195);
            this.metroComboBox6.Name = "metroComboBox6";
            this.metroComboBox6.Size = new System.Drawing.Size(311, 29);
            this.metroComboBox6.TabIndex = 125;
            this.metroComboBox6.UseSelectable = true;
            // 
            // txtEPCAP
            // 
            this.txtEPCAP.Lines = new string[0];
            this.txtEPCAP.Location = new System.Drawing.Point(912, 131);
            this.txtEPCAP.MaxLength = 32767;
            this.txtEPCAP.Name = "txtEPCAP";
            this.txtEPCAP.PasswordChar = '\0';
            this.txtEPCAP.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEPCAP.SelectedText = "";
            this.txtEPCAP.Size = new System.Drawing.Size(73, 23);
            this.txtEPCAP.TabIndex = 124;
            this.txtEPCAP.UseSelectable = true;
            // 
            // txtEFOB
            // 
            this.txtEFOB.Lines = new string[0];
            this.txtEFOB.Location = new System.Drawing.Point(764, 131);
            this.txtEFOB.MaxLength = 32767;
            this.txtEFOB.Name = "txtEFOB";
            this.txtEFOB.PasswordChar = '\0';
            this.txtEFOB.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEFOB.SelectedText = "";
            this.txtEFOB.Size = new System.Drawing.Size(79, 23);
            this.txtEFOB.TabIndex = 123;
            this.txtEFOB.UseSelectable = true;
            // 
            // txtEInvoiceN
            // 
            this.txtEInvoiceN.Lines = new string[0];
            this.txtEInvoiceN.Location = new System.Drawing.Point(588, 131);
            this.txtEInvoiceN.MaxLength = 32767;
            this.txtEInvoiceN.Name = "txtEInvoiceN";
            this.txtEInvoiceN.PasswordChar = '\0';
            this.txtEInvoiceN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEInvoiceN.SelectedText = "";
            this.txtEInvoiceN.Size = new System.Drawing.Size(129, 23);
            this.txtEInvoiceN.TabIndex = 122;
            this.txtEInvoiceN.UseSelectable = true;
            // 
            // txtETotal
            // 
            this.txtETotal.Lines = new string[0];
            this.txtETotal.Location = new System.Drawing.Point(902, 166);
            this.txtETotal.MaxLength = 32767;
            this.txtETotal.Name = "txtETotal";
            this.txtETotal.PasswordChar = '\0';
            this.txtETotal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtETotal.SelectedText = "";
            this.txtETotal.Size = new System.Drawing.Size(87, 23);
            this.txtETotal.TabIndex = 121;
            this.txtETotal.UseSelectable = true;
            // 
            // txtEDesc
            // 
            this.txtEDesc.Lines = new string[0];
            this.txtEDesc.Location = new System.Drawing.Point(742, 166);
            this.txtEDesc.MaxLength = 32767;
            this.txtEDesc.Name = "txtEDesc";
            this.txtEDesc.PasswordChar = '\0';
            this.txtEDesc.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEDesc.SelectedText = "";
            this.txtEDesc.Size = new System.Drawing.Size(87, 23);
            this.txtEDesc.TabIndex = 120;
            this.txtEDesc.UseSelectable = true;
            // 
            // txtEQtd
            // 
            this.txtEQtd.Lines = new string[0];
            this.txtEQtd.Location = new System.Drawing.Point(607, 166);
            this.txtEQtd.MaxLength = 32767;
            this.txtEQtd.Name = "txtEQtd";
            this.txtEQtd.PasswordChar = '\0';
            this.txtEQtd.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEQtd.SelectedText = "";
            this.txtEQtd.Size = new System.Drawing.Size(87, 23);
            this.txtEQtd.TabIndex = 119;
            this.txtEQtd.UseSelectable = true;
            // 
            // txtELocalizacao
            // 
            this.txtELocalizacao.Lines = new string[0];
            this.txtELocalizacao.Location = new System.Drawing.Point(435, 166);
            this.txtELocalizacao.MaxLength = 32767;
            this.txtELocalizacao.Name = "txtELocalizacao";
            this.txtELocalizacao.PasswordChar = '\0';
            this.txtELocalizacao.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtELocalizacao.SelectedText = "";
            this.txtELocalizacao.Size = new System.Drawing.Size(115, 23);
            this.txtELocalizacao.TabIndex = 118;
            this.txtELocalizacao.UseSelectable = true;
            // 
            // cmbEReferencia
            // 
            this.cmbEReferencia.FormattingEnabled = true;
            this.cmbEReferencia.IntegralHeight = false;
            this.cmbEReferencia.ItemHeight = 23;
            this.cmbEReferencia.Location = new System.Drawing.Point(125, 160);
            this.cmbEReferencia.Name = "cmbEReferencia";
            this.cmbEReferencia.Size = new System.Drawing.Size(213, 29);
            this.cmbEReferencia.TabIndex = 117;
            this.cmbEReferencia.UseSelectable = true;
            // 
            // txtEGuia
            // 
            this.txtEGuia.Lines = new string[0];
            this.txtEGuia.Location = new System.Drawing.Point(898, 100);
            this.txtEGuia.MaxLength = 32767;
            this.txtEGuia.Name = "txtEGuia";
            this.txtEGuia.PasswordChar = '\0';
            this.txtEGuia.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEGuia.SelectedText = "";
            this.txtEGuia.Size = new System.Drawing.Size(87, 23);
            this.txtEGuia.TabIndex = 116;
            this.txtEGuia.UseSelectable = true;
            // 
            // txtEMovStock
            // 
            this.txtEMovStock.Lines = new string[0];
            this.txtEMovStock.Location = new System.Drawing.Point(703, 100);
            this.txtEMovStock.MaxLength = 32767;
            this.txtEMovStock.Name = "txtEMovStock";
            this.txtEMovStock.PasswordChar = '\0';
            this.txtEMovStock.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEMovStock.SelectedText = "";
            this.txtEMovStock.Size = new System.Drawing.Size(87, 23);
            this.txtEMovStock.TabIndex = 115;
            this.txtEMovStock.UseSelectable = true;
            // 
            // txtENumero
            // 
            this.txtENumero.Lines = new string[0];
            this.txtENumero.Location = new System.Drawing.Point(895, 64);
            this.txtENumero.MaxLength = 32767;
            this.txtENumero.Name = "txtENumero";
            this.txtENumero.PasswordChar = '\0';
            this.txtENumero.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtENumero.SelectedText = "";
            this.txtENumero.Size = new System.Drawing.Size(87, 23);
            this.txtENumero.TabIndex = 114;
            this.txtENumero.UseSelectable = true;
            // 
            // txtEFechado
            // 
            this.txtEFechado.Lines = new string[0];
            this.txtEFechado.Location = new System.Drawing.Point(703, 64);
            this.txtEFechado.MaxLength = 32767;
            this.txtEFechado.Name = "txtEFechado";
            this.txtEFechado.PasswordChar = '\0';
            this.txtEFechado.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEFechado.SelectedText = "";
            this.txtEFechado.Size = new System.Drawing.Size(87, 23);
            this.txtEFechado.TabIndex = 113;
            this.txtEFechado.UseSelectable = true;
            // 
            // txtEOrigemEntrada
            // 
            this.txtEOrigemEntrada.Lines = new string[0];
            this.txtEOrigemEntrada.Location = new System.Drawing.Point(146, 131);
            this.txtEOrigemEntrada.MaxLength = 32767;
            this.txtEOrigemEntrada.Name = "txtEOrigemEntrada";
            this.txtEOrigemEntrada.PasswordChar = '\0';
            this.txtEOrigemEntrada.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEOrigemEntrada.SelectedText = "";
            this.txtEOrigemEntrada.Size = new System.Drawing.Size(87, 23);
            this.txtEOrigemEntrada.TabIndex = 112;
            this.txtEOrigemEntrada.UseSelectable = true;
            // 
            // metroComboBox4
            // 
            this.metroComboBox4.FormattingEnabled = true;
            this.metroComboBox4.IntegralHeight = false;
            this.metroComboBox4.ItemHeight = 23;
            this.metroComboBox4.Location = new System.Drawing.Point(269, 125);
            this.metroComboBox4.Name = "metroComboBox4";
            this.metroComboBox4.Size = new System.Drawing.Size(238, 29);
            this.metroComboBox4.TabIndex = 111;
            this.metroComboBox4.UseSelectable = true;
            // 
            // txtEFornecedor
            // 
            this.txtEFornecedor.Lines = new string[0];
            this.txtEFornecedor.Location = new System.Drawing.Point(146, 96);
            this.txtEFornecedor.MaxLength = 32767;
            this.txtEFornecedor.Name = "txtEFornecedor";
            this.txtEFornecedor.PasswordChar = '\0';
            this.txtEFornecedor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEFornecedor.SelectedText = "";
            this.txtEFornecedor.Size = new System.Drawing.Size(87, 23);
            this.txtEFornecedor.TabIndex = 110;
            this.txtEFornecedor.UseSelectable = true;
            // 
            // metroComboBox3
            // 
            this.metroComboBox3.FormattingEnabled = true;
            this.metroComboBox3.IntegralHeight = false;
            this.metroComboBox3.ItemHeight = 23;
            this.metroComboBox3.Location = new System.Drawing.Point(269, 90);
            this.metroComboBox3.Name = "metroComboBox3";
            this.metroComboBox3.Size = new System.Drawing.Size(352, 29);
            this.metroComboBox3.TabIndex = 109;
            this.metroComboBox3.UseSelectable = true;
            // 
            // cmbELoja
            // 
            this.cmbELoja.FormattingEnabled = true;
            this.cmbELoja.IntegralHeight = false;
            this.cmbELoja.ItemHeight = 23;
            this.cmbELoja.Location = new System.Drawing.Point(353, 58);
            this.cmbELoja.Name = "cmbELoja";
            this.cmbELoja.Size = new System.Drawing.Size(268, 29);
            this.cmbELoja.TabIndex = 108;
            this.cmbELoja.UseSelectable = true;
            // 
            // cmbEDataInventario
            // 
            this.cmbEDataInventario.FormattingEnabled = true;
            this.cmbEDataInventario.IntegralHeight = false;
            this.cmbEDataInventario.ItemHeight = 23;
            this.cmbEDataInventario.Location = new System.Drawing.Point(125, 58);
            this.cmbEDataInventario.Name = "cmbEDataInventario";
            this.cmbEDataInventario.Size = new System.Drawing.Size(148, 29);
            this.cmbEDataInventario.TabIndex = 107;
            this.cmbEDataInventario.UseSelectable = true;
            // 
            // metroLabel25
            // 
            this.metroLabel25.AutoSize = true;
            this.metroLabel25.Location = new System.Drawing.Point(835, 170);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(38, 19);
            this.metroLabel25.TabIndex = 106;
            this.metroLabel25.Text = "Total";
            // 
            // metroLabel24
            // 
            this.metroLabel24.AutoSize = true;
            this.metroLabel24.Location = new System.Drawing.Point(700, 170);
            this.metroLabel24.Name = "metroLabel24";
            this.metroLabel24.Size = new System.Drawing.Size(36, 19);
            this.metroLabel24.TabIndex = 105;
            this.metroLabel24.Text = "Desc";
            // 
            // metroLabel23
            // 
            this.metroLabel23.AutoSize = true;
            this.metroLabel23.Location = new System.Drawing.Point(849, 135);
            this.metroLabel23.Name = "metroLabel23";
            this.metroLabel23.Size = new System.Drawing.Size(47, 19);
            this.metroLabel23.TabIndex = 104;
            this.metroLabel23.Text = "PCA P";
            // 
            // metroLabel22
            // 
            this.metroLabel22.AutoSize = true;
            this.metroLabel22.Location = new System.Drawing.Point(723, 135);
            this.metroLabel22.Name = "metroLabel22";
            this.metroLabel22.Size = new System.Drawing.Size(35, 19);
            this.metroLabel22.TabIndex = 103;
            this.metroLabel22.Text = "FOB";
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.Location = new System.Drawing.Point(556, 170);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(32, 19);
            this.metroLabel21.TabIndex = 102;
            this.metroLabel21.Text = "Qtd";
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.Location = new System.Drawing.Point(353, 170);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(76, 19);
            this.metroLabel20.TabIndex = 101;
            this.metroLabel20.Text = "Localização";
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(819, 100);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(35, 19);
            this.metroLabel19.TabIndex = 100;
            this.metroLabel19.Text = "Guia";
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(627, 100);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(73, 19);
            this.metroLabel18.TabIndex = 99;
            this.metroLabel18.Text = "Mov. Stock";
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(819, 64);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(58, 19);
            this.metroLabel17.TabIndex = 98;
            this.metroLabel17.Text = "Número";
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(638, 68);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(59, 19);
            this.metroLabel16.TabIndex = 97;
            this.metroLabel16.Text = "Fechado";
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(3, 170);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(70, 19);
            this.metroLabel15.TabIndex = 96;
            this.metroLabel15.Text = "Referência";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(513, 135);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(69, 19);
            this.metroLabel14.TabIndex = 95;
            this.metroLabel14.Text = "Invoice Nº";
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(3, 135);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(123, 19);
            this.metroLabel13.TabIndex = 94;
            this.metroLabel13.Text = "Origem da Entrada";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(305, 68);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(33, 19);
            this.metroLabel12.TabIndex = 93;
            this.metroLabel12.Text = "Loja";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(3, 100);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(77, 19);
            this.metroLabel11.TabIndex = 92;
            this.metroLabel11.Text = "Fornecedor";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(3, 68);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(116, 19);
            this.metroLabel10.TabIndex = 91;
            this.metroLabel10.Text = "Data de Inventário";
            // 
            // toolStrip3
            // 
            this.toolStrip3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btENovo,
            this.btEGravar});
            this.toolStrip3.Location = new System.Drawing.Point(435, 11);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(120, 25);
            this.toolStrip3.TabIndex = 89;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // btENovo
            // 
            this.btENovo.Image = ((System.Drawing.Image)(resources.GetObject("btENovo.Image")));
            this.btENovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btENovo.Name = "btENovo";
            this.btENovo.Size = new System.Drawing.Size(56, 22);
            this.btENovo.Text = "Novo";
            // 
            // btEGravar
            // 
            this.btEGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btEGravar.Image = ((System.Drawing.Image)(resources.GetObject("btEGravar.Image")));
            this.btEGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btEGravar.Name = "btEGravar";
            this.btEGravar.Size = new System.Drawing.Size(61, 22);
            this.btEGravar.Text = "Gravar";
            // 
            // toolStripContainer3
            // 
            // 
            // toolStripContainer3.ContentPanel
            // 
            this.toolStripContainer3.ContentPanel.Size = new System.Drawing.Size(212, 2);
            this.toolStripContainer3.Location = new System.Drawing.Point(435, 11);
            this.toolStripContainer3.Name = "toolStripContainer3";
            this.toolStripContainer3.Size = new System.Drawing.Size(212, 27);
            this.toolStripContainer3.TabIndex = 90;
            this.toolStripContainer3.Text = "toolStripContainer3";
            // 
            // toolStripContainer3.TopToolStripPanel
            // 
            this.toolStripContainer3.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer3.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer3.TopToolStripPanel.Tag = "XC";
            // 
            // metroTabPage4
            // 
            this.metroTabPage4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage4.Controls.Add(this.metroGrid3);
            this.metroTabPage4.Controls.Add(this.cmbZLoja);
            this.metroTabPage4.Controls.Add(this.cmbZData);
            this.metroTabPage4.Controls.Add(this.metroLabel28);
            this.metroTabPage4.Controls.Add(this.metroLabel27);
            this.metroTabPage4.Controls.Add(this.toolStrip4);
            this.metroTabPage4.Controls.Add(this.toolStripContainer4);
            this.metroTabPage4.Controls.Add(this.groupBox3);
            this.metroTabPage4.HorizontalScrollbarBarColor = true;
            this.metroTabPage4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.HorizontalScrollbarSize = 10;
            this.metroTabPage4.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage4.Name = "metroTabPage4";
            this.metroTabPage4.Size = new System.Drawing.Size(985, 459);
            this.metroTabPage4.TabIndex = 3;
            this.metroTabPage4.Text = "Zerar Inventário";
            this.metroTabPage4.VerticalScrollbarBarColor = true;
            this.metroTabPage4.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.VerticalScrollbarSize = 10;
            // 
            // metroGrid3
            // 
            this.metroGrid3.AllowUserToResizeRows = false;
            this.metroGrid3.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.metroGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Mercadoria1,
            this.Codigo});
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid3.DefaultCellStyle = dataGridViewCellStyle26;
            this.metroGrid3.EnableHeadersVisualStyles = false;
            this.metroGrid3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid3.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid3.Location = new System.Drawing.Point(197, 175);
            this.metroGrid3.Name = "metroGrid3";
            this.metroGrid3.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid3.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.metroGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid3.Size = new System.Drawing.Size(629, 238);
            this.metroGrid3.TabIndex = 96;
            // 
            // cmbZLoja
            // 
            this.cmbZLoja.FormattingEnabled = true;
            this.cmbZLoja.IntegralHeight = false;
            this.cmbZLoja.ItemHeight = 23;
            this.cmbZLoja.Location = new System.Drawing.Point(383, 114);
            this.cmbZLoja.Name = "cmbZLoja";
            this.cmbZLoja.Size = new System.Drawing.Size(386, 29);
            this.cmbZLoja.TabIndex = 95;
            this.cmbZLoja.UseSelectable = true;
            this.cmbZLoja.DropDown += new System.EventHandler(this.cmbZLoja_DropDown);
            this.cmbZLoja.SelectedIndexChanged += new System.EventHandler(this.cmbZLoja_SelectedIndexChanged);
            // 
            // cmbZData
            // 
            this.cmbZData.FormattingEnabled = true;
            this.cmbZData.IntegralHeight = false;
            this.cmbZData.ItemHeight = 23;
            this.cmbZData.Location = new System.Drawing.Point(383, 77);
            this.cmbZData.Name = "cmbZData";
            this.cmbZData.Size = new System.Drawing.Size(144, 29);
            this.cmbZData.TabIndex = 94;
            this.cmbZData.UseSelectable = true;
            this.cmbZData.DropDown += new System.EventHandler(this.cmbZData_DropDown);
            // 
            // metroLabel28
            // 
            this.metroLabel28.AutoSize = true;
            this.metroLabel28.Location = new System.Drawing.Point(321, 124);
            this.metroLabel28.Name = "metroLabel28";
            this.metroLabel28.Size = new System.Drawing.Size(33, 19);
            this.metroLabel28.TabIndex = 93;
            this.metroLabel28.Text = "Loja";
            // 
            // metroLabel27
            // 
            this.metroLabel27.AutoSize = true;
            this.metroLabel27.Location = new System.Drawing.Point(321, 87);
            this.metroLabel27.Name = "metroLabel27";
            this.metroLabel27.Size = new System.Drawing.Size(36, 19);
            this.metroLabel27.TabIndex = 92;
            this.metroLabel27.Text = "Data";
            // 
            // toolStrip4
            // 
            this.toolStrip4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip4.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btZGravar});
            this.toolStrip4.Location = new System.Drawing.Point(456, 11);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.Size = new System.Drawing.Size(64, 25);
            this.toolStrip4.TabIndex = 89;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // btZGravar
            // 
            this.btZGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btZGravar.Image = ((System.Drawing.Image)(resources.GetObject("btZGravar.Image")));
            this.btZGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btZGravar.Name = "btZGravar";
            this.btZGravar.Size = new System.Drawing.Size(61, 22);
            this.btZGravar.Text = "Gravar";
            this.btZGravar.Click += new System.EventHandler(this.btZGravar_Click);
            // 
            // toolStripContainer4
            // 
            // 
            // toolStripContainer4.ContentPanel
            // 
            this.toolStripContainer4.ContentPanel.Size = new System.Drawing.Size(149, 2);
            this.toolStripContainer4.Location = new System.Drawing.Point(456, 11);
            this.toolStripContainer4.Name = "toolStripContainer4";
            this.toolStripContainer4.Size = new System.Drawing.Size(149, 27);
            this.toolStripContainer4.TabIndex = 90;
            this.toolStripContainer4.Text = "toolStripContainer4";
            // 
            // toolStripContainer4.TopToolStripPanel
            // 
            this.toolStripContainer4.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer4.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer4.TopToolStripPanel.Tag = "XC";
            // 
            // metroTabPage5
            // 
            this.metroTabPage5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage5.Controls.Add(this.metroGrid4);
            this.metroTabPage5.Controls.Add(this.toolStrip5);
            this.metroTabPage5.Controls.Add(this.toolStripContainer5);
            this.metroTabPage5.Controls.Add(this.groupBox1);
            this.metroTabPage5.Controls.Add(this.groupBox2);
            this.metroTabPage5.HorizontalScrollbarBarColor = true;
            this.metroTabPage5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.HorizontalScrollbarSize = 10;
            this.metroTabPage5.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage5.Name = "metroTabPage5";
            this.metroTabPage5.Size = new System.Drawing.Size(985, 459);
            this.metroTabPage5.TabIndex = 4;
            this.metroTabPage5.Text = "Acerto Inventário";
            this.metroTabPage5.VerticalScrollbarBarColor = true;
            this.metroTabPage5.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.VerticalScrollbarSize = 10;
            this.metroTabPage5.Leave += new System.EventHandler(this.metroTabPage5_Leave);
            // 
            // metroGrid4
            // 
            this.metroGrid4.AllowUserToResizeRows = false;
            this.metroGrid4.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid4.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid4.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid4.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.metroGrid4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid4.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Referencia3,
            this.Mercadoria3,
            this.QtdMov,
            this.TipoMov,
            this.DataMov,
            this.Column1,
            this.Column2,
            this.Column3});
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid4.DefaultCellStyle = dataGridViewCellStyle17;
            this.metroGrid4.EnableHeadersVisualStyles = false;
            this.metroGrid4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid4.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid4.Location = new System.Drawing.Point(154, 187);
            this.metroGrid4.Name = "metroGrid4";
            this.metroGrid4.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid4.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.metroGrid4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid4.Size = new System.Drawing.Size(723, 205);
            this.metroGrid4.TabIndex = 100;
            // 
            // toolStrip5
            // 
            this.toolStrip5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip5.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip5.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btAcImprimir,
            this.btAcGravar});
            this.toolStrip5.Location = new System.Drawing.Point(424, 11);
            this.toolStrip5.Name = "toolStrip5";
            this.toolStrip5.Size = new System.Drawing.Size(168, 25);
            this.toolStrip5.TabIndex = 89;
            this.toolStrip5.Text = "toolStrip5";
            // 
            // btAcImprimir
            // 
            this.btAcImprimir.Image = ((System.Drawing.Image)(resources.GetObject("btAcImprimir.Image")));
            this.btAcImprimir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btAcImprimir.Name = "btAcImprimir";
            this.btAcImprimir.Size = new System.Drawing.Size(73, 22);
            this.btAcImprimir.Text = "Imprimir";
            // 
            // btAcGravar
            // 
            this.btAcGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btAcGravar.Image = ((System.Drawing.Image)(resources.GetObject("btAcGravar.Image")));
            this.btAcGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btAcGravar.Name = "btAcGravar";
            this.btAcGravar.Size = new System.Drawing.Size(61, 22);
            this.btAcGravar.Text = "Gravar";
            this.btAcGravar.Click += new System.EventHandler(this.btAcGravar_Click);
            // 
            // toolStripContainer5
            // 
            // 
            // toolStripContainer5.ContentPanel
            // 
            this.toolStripContainer5.ContentPanel.Size = new System.Drawing.Size(217, 2);
            this.toolStripContainer5.Location = new System.Drawing.Point(424, 11);
            this.toolStripContainer5.Name = "toolStripContainer5";
            this.toolStripContainer5.Size = new System.Drawing.Size(217, 27);
            this.toolStripContainer5.TabIndex = 90;
            this.toolStripContainer5.Text = "toolStripContainer5";
            // 
            // toolStripContainer5.TopToolStripPanel
            // 
            this.toolStripContainer5.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer5.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer5.TopToolStripPanel.Tag = "XC";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDataFinal);
            this.groupBox1.Controls.Add(this.txtDataInicial);
            this.groupBox1.Controls.Add(this.cmbAcDataInventario);
            this.groupBox1.Controls.Add(this.metroLabel31);
            this.groupBox1.Controls.Add(this.metroLabel30);
            this.groupBox1.Controls.Add(this.metroLabel29);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(154, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(723, 52);
            this.groupBox1.TabIndex = 101;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data do Acerto";
            // 
            // txtDataFinal
            // 
            this.txtDataFinal.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtDataFinal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDataFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDataFinal.Location = new System.Drawing.Point(327, 26);
            this.txtDataFinal.Mask = "00/00/0000";
            this.txtDataFinal.Name = "txtDataFinal";
            this.txtDataFinal.Size = new System.Drawing.Size(80, 20);
            this.txtDataFinal.TabIndex = 104;
            this.txtDataFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDataFinal.ValidatingType = typeof(System.DateTime);
            this.txtDataFinal.Leave += new System.EventHandler(this.txtDataFinal_Leave);
            // 
            // txtDataInicial
            // 
            this.txtDataInicial.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtDataInicial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDataInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDataInicial.Location = new System.Drawing.Point(147, 26);
            this.txtDataInicial.Mask = "00/00/0000";
            this.txtDataInicial.Name = "txtDataInicial";
            this.txtDataInicial.Size = new System.Drawing.Size(80, 20);
            this.txtDataInicial.TabIndex = 103;
            this.txtDataInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDataInicial.ValidatingType = typeof(System.DateTime);
            this.txtDataInicial.Leave += new System.EventHandler(this.txtDataInicial_Leave);
            // 
            // cmbAcDataInventario
            // 
            this.cmbAcDataInventario.FormattingEnabled = true;
            this.cmbAcDataInventario.IntegralHeight = false;
            this.cmbAcDataInventario.ItemHeight = 23;
            this.cmbAcDataInventario.Location = new System.Drawing.Point(566, 15);
            this.cmbAcDataInventario.Name = "cmbAcDataInventario";
            this.cmbAcDataInventario.Size = new System.Drawing.Size(151, 29);
            this.cmbAcDataInventario.TabIndex = 96;
            this.cmbAcDataInventario.UseSelectable = true;
            this.cmbAcDataInventario.DropDown += new System.EventHandler(this.cmbAcDataInventario_DropDown);
            // 
            // metroLabel31
            // 
            this.metroLabel31.AutoSize = true;
            this.metroLabel31.Location = new System.Drawing.Point(436, 25);
            this.metroLabel31.Name = "metroLabel31";
            this.metroLabel31.Size = new System.Drawing.Size(117, 19);
            this.metroLabel31.TabIndex = 95;
            this.metroLabel31.Text = "Data do Inventário";
            // 
            // metroLabel30
            // 
            this.metroLabel30.AutoSize = true;
            this.metroLabel30.Location = new System.Drawing.Point(270, 25);
            this.metroLabel30.Name = "metroLabel30";
            this.metroLabel30.Size = new System.Drawing.Size(31, 19);
            this.metroLabel30.TabIndex = 94;
            this.metroLabel30.Text = "Fim";
            // 
            // metroLabel29
            // 
            this.metroLabel29.AutoSize = true;
            this.metroLabel29.Location = new System.Drawing.Point(88, 25);
            this.metroLabel29.Name = "metroLabel29";
            this.metroLabel29.Size = new System.Drawing.Size(39, 19);
            this.metroLabel29.TabIndex = 93;
            this.metroLabel29.Text = "Inicio";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbACLoja);
            this.groupBox2.Controls.Add(this.cmbAcSaelecaoLoja);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(154, 120);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(723, 50);
            this.groupBox2.TabIndex = 102;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Selecção da Loja";
            // 
            // cmbAcSaelecaoLoja
            // 
            this.cmbAcSaelecaoLoja.FormattingEnabled = true;
            this.cmbAcSaelecaoLoja.IntegralHeight = false;
            this.cmbAcSaelecaoLoja.ItemHeight = 23;
            this.cmbAcSaelecaoLoja.Location = new System.Drawing.Point(147, 15);
            this.cmbAcSaelecaoLoja.Name = "cmbAcSaelecaoLoja";
            this.cmbAcSaelecaoLoja.Size = new System.Drawing.Size(406, 29);
            this.cmbAcSaelecaoLoja.TabIndex = 99;
            this.cmbAcSaelecaoLoja.UseSelectable = true;
            this.cmbAcSaelecaoLoja.DropDown += new System.EventHandler(this.cmbAcSaelecaoLoja_DropDown);
            this.cmbAcSaelecaoLoja.SelectedIndexChanged += new System.EventHandler(this.cmbAcSaelecaoLoja_SelectedIndexChanged);
            // 
            // metroTabPage6
            // 
            this.metroTabPage6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage6.Controls.Add(this.txtFDataControlo);
            this.metroTabPage6.Controls.Add(this.txtFFechadopor);
            this.metroTabPage6.Controls.Add(this.txtFAbertoPor);
            this.metroTabPage6.Controls.Add(this.cmbFInventarioDia);
            this.metroTabPage6.Controls.Add(this.cmbFInventarioLoja);
            this.metroTabPage6.Controls.Add(this.cbFFecharInventario);
            this.metroTabPage6.Controls.Add(this.metroLabel36);
            this.metroTabPage6.Controls.Add(this.metroLabel35);
            this.metroTabPage6.Controls.Add(this.metroLabel34);
            this.metroTabPage6.Controls.Add(this.metroLabel33);
            this.metroTabPage6.Controls.Add(this.metroLabel32);
            this.metroTabPage6.Controls.Add(this.toolStrip6);
            this.metroTabPage6.Controls.Add(this.toolStripContainer6);
            this.metroTabPage6.HorizontalScrollbarBarColor = true;
            this.metroTabPage6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.HorizontalScrollbarSize = 10;
            this.metroTabPage6.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage6.Name = "metroTabPage6";
            this.metroTabPage6.Size = new System.Drawing.Size(985, 459);
            this.metroTabPage6.TabIndex = 5;
            this.metroTabPage6.Text = "Abertura / Fecho do Inventário";
            this.metroTabPage6.VerticalScrollbarBarColor = true;
            this.metroTabPage6.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.VerticalScrollbarSize = 10;
            // 
            // txtFDataControlo
            // 
            this.txtFDataControlo.Lines = new string[0];
            this.txtFDataControlo.Location = new System.Drawing.Point(359, 214);
            this.txtFDataControlo.MaxLength = 32767;
            this.txtFDataControlo.Name = "txtFDataControlo";
            this.txtFDataControlo.PasswordChar = '\0';
            this.txtFDataControlo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFDataControlo.SelectedText = "";
            this.txtFDataControlo.Size = new System.Drawing.Size(198, 23);
            this.txtFDataControlo.TabIndex = 101;
            this.txtFDataControlo.UseSelectable = true;
            // 
            // txtFFechadopor
            // 
            this.txtFFechadopor.Lines = new string[0];
            this.txtFFechadopor.Location = new System.Drawing.Point(359, 185);
            this.txtFFechadopor.MaxLength = 32767;
            this.txtFFechadopor.Name = "txtFFechadopor";
            this.txtFFechadopor.PasswordChar = '\0';
            this.txtFFechadopor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFFechadopor.SelectedText = "";
            this.txtFFechadopor.Size = new System.Drawing.Size(198, 23);
            this.txtFFechadopor.TabIndex = 100;
            this.txtFFechadopor.UseSelectable = true;
            // 
            // txtFAbertoPor
            // 
            this.txtFAbertoPor.Lines = new string[0];
            this.txtFAbertoPor.Location = new System.Drawing.Point(359, 156);
            this.txtFAbertoPor.MaxLength = 32767;
            this.txtFAbertoPor.Name = "txtFAbertoPor";
            this.txtFAbertoPor.PasswordChar = '\0';
            this.txtFAbertoPor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFAbertoPor.SelectedText = "";
            this.txtFAbertoPor.Size = new System.Drawing.Size(198, 23);
            this.txtFAbertoPor.TabIndex = 99;
            this.txtFAbertoPor.UseSelectable = true;
            // 
            // cmbFInventarioDia
            // 
            this.cmbFInventarioDia.FormattingEnabled = true;
            this.cmbFInventarioDia.IntegralHeight = false;
            this.cmbFInventarioDia.ItemHeight = 23;
            this.cmbFInventarioDia.Location = new System.Drawing.Point(359, 120);
            this.cmbFInventarioDia.Name = "cmbFInventarioDia";
            this.cmbFInventarioDia.Size = new System.Drawing.Size(198, 29);
            this.cmbFInventarioDia.TabIndex = 98;
            this.cmbFInventarioDia.UseSelectable = true;
            // 
            // cmbFInventarioLoja
            // 
            this.cmbFInventarioLoja.FormattingEnabled = true;
            this.cmbFInventarioLoja.IntegralHeight = false;
            this.cmbFInventarioLoja.ItemHeight = 23;
            this.cmbFInventarioLoja.Location = new System.Drawing.Point(359, 84);
            this.cmbFInventarioLoja.Name = "cmbFInventarioLoja";
            this.cmbFInventarioLoja.Size = new System.Drawing.Size(347, 29);
            this.cmbFInventarioLoja.TabIndex = 97;
            this.cmbFInventarioLoja.UseSelectable = true;
            // 
            // cbFFecharInventario
            // 
            this.cbFFecharInventario.AutoSize = true;
            this.cbFFecharInventario.Location = new System.Drawing.Point(592, 134);
            this.cbFFecharInventario.Name = "cbFFecharInventario";
            this.cbFFecharInventario.Size = new System.Drawing.Size(114, 15);
            this.cbFFecharInventario.TabIndex = 96;
            this.cbFFecharInventario.Text = "Fechar Inventário";
            this.cbFFecharInventario.UseSelectable = true;
            // 
            // metroLabel36
            // 
            this.metroLabel36.AutoSize = true;
            this.metroLabel36.Location = new System.Drawing.Point(215, 218);
            this.metroLabel36.Name = "metroLabel36";
            this.metroLabel36.Size = new System.Drawing.Size(92, 19);
            this.metroLabel36.TabIndex = 95;
            this.metroLabel36.Text = "Data Controlo";
            // 
            // metroLabel35
            // 
            this.metroLabel35.AutoSize = true;
            this.metroLabel35.Location = new System.Drawing.Point(215, 189);
            this.metroLabel35.Name = "metroLabel35";
            this.metroLabel35.Size = new System.Drawing.Size(84, 19);
            this.metroLabel35.TabIndex = 94;
            this.metroLabel35.Text = "Fechado Por";
            // 
            // metroLabel34
            // 
            this.metroLabel34.AutoSize = true;
            this.metroLabel34.Location = new System.Drawing.Point(215, 160);
            this.metroLabel34.Name = "metroLabel34";
            this.metroLabel34.Size = new System.Drawing.Size(75, 19);
            this.metroLabel34.TabIndex = 93;
            this.metroLabel34.Text = "Aberto Por";
            // 
            // metroLabel33
            // 
            this.metroLabel33.AutoSize = true;
            this.metroLabel33.Location = new System.Drawing.Point(215, 130);
            this.metroLabel33.Name = "metroLabel33";
            this.metroLabel33.Size = new System.Drawing.Size(109, 19);
            this.metroLabel33.TabIndex = 92;
            this.metroLabel33.Text = "Inventario do Dia";
            // 
            // metroLabel32
            // 
            this.metroLabel32.AutoSize = true;
            this.metroLabel32.Location = new System.Drawing.Point(215, 94);
            this.metroLabel32.Name = "metroLabel32";
            this.metroLabel32.Size = new System.Drawing.Size(113, 19);
            this.metroLabel32.TabIndex = 91;
            this.metroLabel32.Text = "Inventario da Loja";
            // 
            // toolStrip6
            // 
            this.toolStrip6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip6.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip6.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btFGravar});
            this.toolStrip6.Location = new System.Drawing.Point(432, 17);
            this.toolStrip6.Name = "toolStrip6";
            this.toolStrip6.Size = new System.Drawing.Size(64, 25);
            this.toolStrip6.TabIndex = 89;
            this.toolStrip6.Text = "toolStrip6";
            // 
            // btFGravar
            // 
            this.btFGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btFGravar.Image = ((System.Drawing.Image)(resources.GetObject("btFGravar.Image")));
            this.btFGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btFGravar.Name = "btFGravar";
            this.btFGravar.Size = new System.Drawing.Size(61, 22);
            this.btFGravar.Text = "Gravar";
            // 
            // toolStripContainer6
            // 
            // 
            // toolStripContainer6.ContentPanel
            // 
            this.toolStripContainer6.ContentPanel.Size = new System.Drawing.Size(172, 2);
            this.toolStripContainer6.Location = new System.Drawing.Point(432, 17);
            this.toolStripContainer6.Name = "toolStripContainer6";
            this.toolStripContainer6.Size = new System.Drawing.Size(172, 27);
            this.toolStripContainer6.TabIndex = 90;
            this.toolStripContainer6.Text = "toolStripContainer6";
            // 
            // toolStripContainer6.TopToolStripPanel
            // 
            this.toolStripContainer6.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer6.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer6.TopToolStripPanel.Tag = "XC";
            // 
            // metroTabPage7
            // 
            this.metroTabPage7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage7.Controls.Add(this.metroGrid5);
            this.metroTabPage7.Controls.Add(this.metroComboBox2);
            this.metroTabPage7.Controls.Add(this.metroComboBox1);
            this.metroTabPage7.Controls.Add(this.metroLabel38);
            this.metroTabPage7.Controls.Add(this.metroLabel37);
            this.metroTabPage7.Controls.Add(this.toolStrip7);
            this.metroTabPage7.Controls.Add(this.toolStripContainer7);
            this.metroTabPage7.HorizontalScrollbarBarColor = true;
            this.metroTabPage7.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage7.HorizontalScrollbarSize = 10;
            this.metroTabPage7.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage7.Name = "metroTabPage7";
            this.metroTabPage7.Size = new System.Drawing.Size(985, 459);
            this.metroTabPage7.TabIndex = 6;
            this.metroTabPage7.Text = "Artigos - Contagem Ad-hoc";
            this.metroTabPage7.VerticalScrollbarBarColor = true;
            this.metroTabPage7.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage7.VerticalScrollbarSize = 10;
            // 
            // metroGrid5
            // 
            this.metroGrid5.AllowUserToResizeRows = false;
            this.metroGrid5.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid5.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid5.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid5.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.metroGrid5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Referencia4,
            this.Mercadoria4,
            this.Localizacao4,
            this.QtdSistema,
            this.QtdContada});
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid5.DefaultCellStyle = dataGridViewCellStyle29;
            this.metroGrid5.EnableHeadersVisualStyles = false;
            this.metroGrid5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid5.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid5.Location = new System.Drawing.Point(121, 137);
            this.metroGrid5.Name = "metroGrid5";
            this.metroGrid5.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid5.RowHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.metroGrid5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid5.Size = new System.Drawing.Size(704, 200);
            this.metroGrid5.TabIndex = 95;
            // 
            // Referencia4
            // 
            this.Referencia4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Referencia4.HeaderText = "Referência";
            this.Referencia4.Name = "Referencia4";
            // 
            // Mercadoria4
            // 
            this.Mercadoria4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Mercadoria4.HeaderText = "Mercadoria";
            this.Mercadoria4.Name = "Mercadoria4";
            // 
            // Localizacao4
            // 
            this.Localizacao4.HeaderText = "Localização";
            this.Localizacao4.Name = "Localizacao4";
            // 
            // QtdSistema
            // 
            this.QtdSistema.HeaderText = "Qtd Sistema";
            this.QtdSistema.Name = "QtdSistema";
            // 
            // QtdContada
            // 
            this.QtdContada.HeaderText = "Qtd Contada";
            this.QtdContada.Name = "QtdContada";
            // 
            // metroComboBox2
            // 
            this.metroComboBox2.FormattingEnabled = true;
            this.metroComboBox2.IntegralHeight = false;
            this.metroComboBox2.ItemHeight = 23;
            this.metroComboBox2.Location = new System.Drawing.Point(197, 92);
            this.metroComboBox2.Name = "metroComboBox2";
            this.metroComboBox2.Size = new System.Drawing.Size(628, 29);
            this.metroComboBox2.TabIndex = 94;
            this.metroComboBox2.UseSelectable = true;
            // 
            // metroComboBox1
            // 
            this.metroComboBox1.FormattingEnabled = true;
            this.metroComboBox1.IntegralHeight = false;
            this.metroComboBox1.ItemHeight = 23;
            this.metroComboBox1.Location = new System.Drawing.Point(197, 57);
            this.metroComboBox1.Name = "metroComboBox1";
            this.metroComboBox1.Size = new System.Drawing.Size(628, 29);
            this.metroComboBox1.TabIndex = 93;
            this.metroComboBox1.UseSelectable = true;
            // 
            // metroLabel38
            // 
            this.metroLabel38.AutoSize = true;
            this.metroLabel38.Location = new System.Drawing.Point(121, 102);
            this.metroLabel38.Name = "metroLabel38";
            this.metroLabel38.Size = new System.Drawing.Size(70, 19);
            this.metroLabel38.TabIndex = 92;
            this.metroLabel38.Text = "Referência";
            // 
            // metroLabel37
            // 
            this.metroLabel37.AutoSize = true;
            this.metroLabel37.Location = new System.Drawing.Point(121, 67);
            this.metroLabel37.Name = "metroLabel37";
            this.metroLabel37.Size = new System.Drawing.Size(33, 19);
            this.metroLabel37.TabIndex = 91;
            this.metroLabel37.Text = "Loja";
            // 
            // toolStrip7
            // 
            this.toolStrip7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip7.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip7.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btGravar,
            this.toolStripButton2});
            this.toolStrip7.Location = new System.Drawing.Point(425, 10);
            this.toolStrip7.Name = "toolStrip7";
            this.toolStrip7.Size = new System.Drawing.Size(137, 25);
            this.toolStrip7.TabIndex = 89;
            this.toolStrip7.Text = "toolStrip7";
            // 
            // btGravar
            // 
            this.btGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btGravar.Image = ((System.Drawing.Image)(resources.GetObject("btGravar.Image")));
            this.btGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(61, 22);
            this.btGravar.Text = "Gravar";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(73, 22);
            this.toolStripButton2.Text = "Imprimir";
            // 
            // toolStripContainer7
            // 
            // 
            // toolStripContainer7.ContentPanel
            // 
            this.toolStripContainer7.ContentPanel.Size = new System.Drawing.Size(219, 2);
            this.toolStripContainer7.Location = new System.Drawing.Point(425, 10);
            this.toolStripContainer7.Name = "toolStripContainer7";
            this.toolStripContainer7.Size = new System.Drawing.Size(219, 27);
            this.toolStripContainer7.TabIndex = 90;
            this.toolStripContainer7.Text = "toolStripContainer7";
            // 
            // toolStripContainer7.TopToolStripPanel
            // 
            this.toolStripContainer7.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer7.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer7.TopToolStripPanel.Tag = "XC";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lbZLoja);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(197, 58);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(629, 100);
            this.groupBox3.TabIndex = 97;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Data e Loja do Inventário";
            // 
            // lbZLoja
            // 
            this.lbZLoja.AutoSize = true;
            this.lbZLoja.Location = new System.Drawing.Point(578, 66);
            this.lbZLoja.Name = "lbZLoja";
            this.lbZLoja.Size = new System.Drawing.Size(15, 19);
            this.lbZLoja.TabIndex = 94;
            this.lbZLoja.Text = "L";
            // 
            // Mercadoria1
            // 
            this.Mercadoria1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Mercadoria1.DataPropertyName = "Column1";
            this.Mercadoria1.HeaderText = "Mercadoria";
            this.Mercadoria1.Name = "Mercadoria1";
            // 
            // Codigo
            // 
            this.Codigo.DataPropertyName = "Column2";
            this.Codigo.HeaderText = "Código";
            this.Codigo.Name = "Codigo";
            // 
            // lbACLoja
            // 
            this.lbACLoja.AutoSize = true;
            this.lbACLoja.Location = new System.Drawing.Point(559, 25);
            this.lbACLoja.Name = "lbACLoja";
            this.lbACLoja.Size = new System.Drawing.Size(15, 19);
            this.lbACLoja.TabIndex = 100;
            this.lbACLoja.Text = "L";
            // 
            // Referencia3
            // 
            this.Referencia3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Referencia3.DataPropertyName = "Column4";
            this.Referencia3.HeaderText = "Referência";
            this.Referencia3.Name = "Referencia3";
            // 
            // Mercadoria3
            // 
            this.Mercadoria3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Mercadoria3.DataPropertyName = "Column1";
            this.Mercadoria3.HeaderText = "Mercadoria";
            this.Mercadoria3.Name = "Mercadoria3";
            // 
            // QtdMov
            // 
            this.QtdMov.DataPropertyName = "QTD";
            this.QtdMov.HeaderText = "Quantidade Mov";
            this.QtdMov.Name = "QtdMov";
            // 
            // TipoMov
            // 
            this.TipoMov.DataPropertyName = "Column6";
            this.TipoMov.HeaderText = "Tipo Mov";
            this.TipoMov.Name = "TipoMov";
            // 
            // DataMov
            // 
            this.DataMov.DataPropertyName = "Column5";
            this.DataMov.HeaderText = "Data Mov";
            this.DataMov.Name = "DataMov";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Column2";
            this.Column1.HeaderText = "CodLoja";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "Column3";
            this.Column2.HeaderText = "CodArmazem";
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "CodDoc";
            this.Column3.HeaderText = "CodDoc";
            this.Column3.Name = "Column3";
            this.Column3.Visible = false;
            // 
            // FormInventarioFisico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 587);
            this.ControlBox = false;
            this.Controls.Add(this.metroTabControl1);
            this.Name = "FormInventarioFisico";
            this.Text = "Inventário Físico";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormInventarioFisico_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.toolStripContainer2.ResumeLayout(false);
            this.toolStripContainer2.PerformLayout();
            this.metroTabPage3.ResumeLayout(false);
            this.metroTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid2)).EndInit();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.toolStripContainer3.ResumeLayout(false);
            this.toolStripContainer3.PerformLayout();
            this.metroTabPage4.ResumeLayout(false);
            this.metroTabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid3)).EndInit();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            this.toolStripContainer4.ResumeLayout(false);
            this.toolStripContainer4.PerformLayout();
            this.metroTabPage5.ResumeLayout(false);
            this.metroTabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid4)).EndInit();
            this.toolStrip5.ResumeLayout(false);
            this.toolStrip5.PerformLayout();
            this.toolStripContainer5.ResumeLayout(false);
            this.toolStripContainer5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.metroTabPage6.ResumeLayout(false);
            this.metroTabPage6.PerformLayout();
            this.toolStrip6.ResumeLayout(false);
            this.toolStrip6.PerformLayout();
            this.toolStripContainer6.ResumeLayout(false);
            this.toolStripContainer6.PerformLayout();
            this.metroTabPage7.ResumeLayout(false);
            this.metroTabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid5)).EndInit();
            this.toolStrip7.ResumeLayout(false);
            this.toolStrip7.PerformLayout();
            this.toolStripContainer7.ResumeLayout(false);
            this.toolStripContainer7.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroTabPage metroTabPage4;
        private MetroFramework.Controls.MetroTabPage metroTabPage5;
        private MetroFramework.Controls.MetroTabPage metroTabPage6;
        private MetroFramework.Controls.MetroTabPage metroTabPage7;
        private MetroFramework.Controls.MetroTextBox txtADataControle;
        private MetroFramework.Controls.MetroTextBox txtAFechadoPor;
        private MetroFramework.Controls.MetroTextBox txtAAbertoPor;
        private MetroFramework.Controls.MetroComboBox cmbAInventarioDia;
        private MetroFramework.Controls.MetroComboBox cmbAInvertarioLoja;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroCheckBox cbFechoInventario;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btAGravar;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private MetroFramework.Controls.MetroCheckBox cbRAberturaLoja;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btRGravar;
        private System.Windows.Forms.ToolStripContainer toolStripContainer2;
        private MetroFramework.Controls.MetroGrid metroGrid1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mercadoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn Localizacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdSist;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdFis;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdRec;
        private MetroFramework.Controls.MetroComboBox cmbRReferencia;
        private MetroFramework.Controls.MetroComboBox cmbRMercadoria;
        private MetroFramework.Controls.MetroComboBox cmbRLoja;
        private MetroFramework.Controls.MetroComboBox cmbRDataInventrario;
        private MetroFramework.Controls.MetroTextBox txtETotal3;
        private MetroFramework.Controls.MetroTextBox txtETotal2;
        private MetroFramework.Controls.MetroTextBox txtETotal1;
        private MetroFramework.Controls.MetroLabel metroLabel26;
        private MetroFramework.Controls.MetroGrid metroGrid2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Un;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantidade;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mercadoria2;
        private System.Windows.Forms.DataGridViewTextBoxColumn FOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn PCA;
        private System.Windows.Forms.DataGridViewTextBoxColumn Desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn Localizacao2;
        private MetroFramework.Controls.MetroComboBox metroComboBox6;
        private MetroFramework.Controls.MetroTextBox txtEPCAP;
        private MetroFramework.Controls.MetroTextBox txtEFOB;
        private MetroFramework.Controls.MetroTextBox txtEInvoiceN;
        private MetroFramework.Controls.MetroTextBox txtETotal;
        private MetroFramework.Controls.MetroTextBox txtEDesc;
        private MetroFramework.Controls.MetroTextBox txtEQtd;
        private MetroFramework.Controls.MetroTextBox txtELocalizacao;
        private MetroFramework.Controls.MetroComboBox cmbEReferencia;
        private MetroFramework.Controls.MetroTextBox txtEGuia;
        private MetroFramework.Controls.MetroTextBox txtEMovStock;
        private MetroFramework.Controls.MetroTextBox txtENumero;
        private MetroFramework.Controls.MetroTextBox txtEFechado;
        private MetroFramework.Controls.MetroTextBox txtEOrigemEntrada;
        private MetroFramework.Controls.MetroComboBox metroComboBox4;
        private MetroFramework.Controls.MetroTextBox txtEFornecedor;
        private MetroFramework.Controls.MetroComboBox metroComboBox3;
        private MetroFramework.Controls.MetroComboBox cmbELoja;
        private MetroFramework.Controls.MetroComboBox cmbEDataInventario;
        private MetroFramework.Controls.MetroLabel metroLabel25;
        private MetroFramework.Controls.MetroLabel metroLabel24;
        private MetroFramework.Controls.MetroLabel metroLabel23;
        private MetroFramework.Controls.MetroLabel metroLabel22;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton btENovo;
        private System.Windows.Forms.ToolStripButton btEGravar;
        private System.Windows.Forms.ToolStripContainer toolStripContainer3;
        private MetroFramework.Controls.MetroGrid metroGrid3;
        private MetroFramework.Controls.MetroComboBox cmbZLoja;
        private MetroFramework.Controls.MetroComboBox cmbZData;
        private MetroFramework.Controls.MetroLabel metroLabel28;
        private MetroFramework.Controls.MetroLabel metroLabel27;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripButton btZGravar;
        private System.Windows.Forms.ToolStripContainer toolStripContainer4;
        private System.Windows.Forms.ToolStrip toolStrip5;
        private System.Windows.Forms.ToolStripButton btAcImprimir;
        private System.Windows.Forms.ToolStripButton btAcGravar;
        private System.Windows.Forms.ToolStripContainer toolStripContainer5;
        private MetroFramework.Controls.MetroGrid metroGrid4;
        private MetroFramework.Controls.MetroComboBox cmbAcSaelecaoLoja;
        private MetroFramework.Controls.MetroComboBox cmbAcDataInventario;
        private MetroFramework.Controls.MetroLabel metroLabel31;
        private MetroFramework.Controls.MetroLabel metroLabel30;
        private MetroFramework.Controls.MetroLabel metroLabel29;
        private MetroFramework.Controls.MetroTextBox txtFDataControlo;
        private MetroFramework.Controls.MetroTextBox txtFFechadopor;
        private MetroFramework.Controls.MetroTextBox txtFAbertoPor;
        private MetroFramework.Controls.MetroComboBox cmbFInventarioDia;
        private MetroFramework.Controls.MetroComboBox cmbFInventarioLoja;
        private MetroFramework.Controls.MetroCheckBox cbFFecharInventario;
        private MetroFramework.Controls.MetroLabel metroLabel36;
        private MetroFramework.Controls.MetroLabel metroLabel35;
        private MetroFramework.Controls.MetroLabel metroLabel34;
        private MetroFramework.Controls.MetroLabel metroLabel33;
        private MetroFramework.Controls.MetroLabel metroLabel32;
        private System.Windows.Forms.ToolStrip toolStrip6;
        private System.Windows.Forms.ToolStripButton btFGravar;
        private System.Windows.Forms.ToolStripContainer toolStripContainer6;
        private MetroFramework.Controls.MetroGrid metroGrid5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mercadoria4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Localizacao4;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdSistema;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdContada;
        private MetroFramework.Controls.MetroComboBox metroComboBox2;
        private MetroFramework.Controls.MetroComboBox metroComboBox1;
        private MetroFramework.Controls.MetroLabel metroLabel38;
        private MetroFramework.Controls.MetroLabel metroLabel37;
        private System.Windows.Forms.ToolStrip toolStrip7;
        private System.Windows.Forms.ToolStripButton btGravar;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripContainer toolStripContainer7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MaskedTextBox txtDataFinal;
        private System.Windows.Forms.MaskedTextBox txtDataInicial;
        private MetroFramework.Controls.MetroLabel lbACodLoja;
        private MetroFramework.Controls.MetroLabel lbAberto;
        private MetroFramework.Controls.MetroLabel lbRLoja;
        private MetroFramework.Controls.MetroLabel lbRArmazem;
        private MetroFramework.Controls.MetroLabel lbRDescricao;
        private System.Windows.Forms.GroupBox groupBox3;
        private MetroFramework.Controls.MetroLabel lbZLoja;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mercadoria1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private MetroFramework.Controls.MetroLabel lbACLoja;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mercadoria3;
        private System.Windows.Forms.DataGridViewTextBoxColumn QtdMov;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoMov;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataMov;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}