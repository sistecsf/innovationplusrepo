﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendaHotelBooking : MetroFramework.Forms.MetroForm
    {
        DateTime dataInicio, dataFim;
        string horaInicio, horaFim;
        public FormVendaHotelBooking()
        {
            InitializeComponent();
        }

        private void prenxarDataGrid()
        {
            metroGrid1.Rows.Add(FormVendas.referencia, dataInicio, dataFim, horaInicio, horaFim);
        }
        private void btAdicionar_Click(object sender, EventArgs e)
        {
            if (txtDataInicial.Text == "")
            {
                MessageBox.Show("O Campo data Inicio não foi preenxido");
            }
            else
            {
                if (txtHoraInicio.Text == "")
                {
                    MessageBox.Show("O campo Hora inicio não foi preenxido");
                }
                else
                {
                    try
                    {
                        dataInicio = Convert.ToDateTime(txtDataInicial.Text + " " + txtHoraInicio.Text);
                        horaInicio = txtHoraInicio.Text;
                        dataFim = dataInicio.AddHours(Convert.ToInt32(FormVendas.Qtd));
                        horaFim = "" + dataFim.Hour + ":" + dataFim.Minute;
                        lbDataFim.Text = "" + dataFim;
                        lbHoraFim.Text = horaFim;
                        prenxarDataGrid();
                    }
                    catch (Exception ex) 
                    { 
                        MessageBox.Show("Data ou hora incorreta"); 
                    }
                }
            }
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btGravar_Click(object sender, EventArgs e)
        {
            try
            {
                FormVendas.dataInicio = Convert.ToString(dataInicio);
                FormVendas.dataFim = Convert.ToString(dataFim);
                FormVendas.horaInicio = horaInicio;
                FormVendas.horaFim = horaFim;
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception)
            {
                this.DialogResult = DialogResult.Cancel;
            }
        }

        private void txtDataInicial_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception ex)
            {
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!");
                txtDataInicial.Text = "";
            }
        }

        private void txtHoraInicio_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtHoraInicio.Text); }
            catch (Exception ex)
            {
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Hora Válida!");
                txtHoraInicio.Text = "";
            }
        }
    }
}
