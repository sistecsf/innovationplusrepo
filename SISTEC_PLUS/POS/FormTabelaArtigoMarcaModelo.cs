﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS;
using SISTEC_PLUS;
using SISTEC_PLUS.OPS;
using System.Drawing.Imaging;
using System.IO;

namespace SISTEC_PLUS.POS
{
    public partial class FormTabelaArtigoMarcaModelo : MetroFramework.Forms.MetroForm
    {
        string marca, modelo;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao); // conexão com o banco
        public FormTabelaArtigoMarcaModelo()
        {
            InitializeComponent();
        }
        private void CmbMarca()
        {
            SqlCommand cmd = new SqlCommand(@"select DescrMarca from AEMarca WITH (NOLOCK)", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbMarca.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            { MessageBox.Show(ex.Message); }
            finally
            { conexao.Close(); }
        }
        private void CmbModelo()
        {
            SqlCommand cmd = new SqlCommand(@"select DescrModelo from AEMODELO where CodMarca =@modelo", conexao);
            cmd.Parameters.Add(new SqlParameter("@modelo", FormTabelas.Amarca));
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbModelo.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            { MessageBox.Show(ex.Message); }
            finally
            { conexao.Close(); }
        }
        private void FormTabelaArtigoMarcaModelo_Load(object sender, EventArgs e)
        {
            CmbMarca();
        }

        private void cmbMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            conexao.Close();
            SqlCommand cmd = new SqlCommand(@"select CodMarca from AEMarca where DescrMarca= '" + cmbMarca.Text + "' ", conexao);
            try
            {
                cmd.Connection = conexao;
                conexao.Open();
                FormTabelas.Amarca = (string)cmd.ExecuteScalar();
                lbMarca.Text = FormTabelas.Amarca;
                CmbModelo();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }

        private void cmbModelo_SelectedIndexChanged(object sender, EventArgs e)
        {
            conexao.Close();
            SqlCommand cmd = new SqlCommand(@"select CodModelo from AEMODELO where DescrModelo= '" + cmbModelo.Text + "'", conexao);
            try
            {
                cmd.Connection = conexao;
                conexao.Open();
                FormTabelas.Amodelo = (string)cmd.ExecuteScalar();
                lbModelo.Text = FormTabelas.Amodelo;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }

        private void btOk_Click(object sender, EventArgs e)
        {
            
        }

        private void btSair_Click(object sender, EventArgs e)
        {
           
        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void btAtualizar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
