﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS;

namespace SISTEC_PLUS.POS
{
    public partial class FormMenuTransferencias : MetroFramework.Forms.MetroForm
    {
        SqlConnection Conexao = new SqlConnection(Variavel.Conexao);
        public FormMenuTransferencias()
        {
            InitializeComponent();
        }

        /*
         * 
         * METODOS DO NEGOCIO 
         * 
         * 
         *
         */

        //FUNCAO - getNomeDasLojas
        public List<string> getNomeDasLojas()
        {
            List<string> nomes = new List<string>();
            SqlCommand cmd = new SqlCommand("Select Distinct RTRIM(LTRIM(NomeLoja)) as nomeDaLoja From AsLOJA WITH (NOLOCK)", Conexao);
            Conexao.Open();
            SqlDataReader nomesDasLojas = cmd.ExecuteReader();

            while (nomesDasLojas.Read())
            {
                nomes.Add(nomesDasLojas["nomeDaLoja"].ToString());
            }
            Conexao.Close();
            return nomes;
        }

        private void FormTransferencias_Load(object sender, EventArgs e)
        {
            //pupula a combobox
            if (comboBoxNomesDasLojas.Items.Count == 0)
            {
                foreach (string nome in getNomeDasLojas())
                {
                    comboBoxNomesDasLojas.Items.Add(nome);
                }
            }
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (comboBoxNomesDasLojas.SelectedIndex == -1)
            {
                MessageBox.Show("Escolha uma loja!", "Innovation - Aviso");
            }
            
            if (comboBoxNomesDasLojas.SelectedIndex != -1)
            {
                FormTranferencias janelaTransfererencias = new FormTranferencias(comboBoxNomesDasLojas.SelectedItem.ToString());
                
                this.Hide();
                janelaTransfererencias.ShowDialog();
            }
        }
        private void btSair_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
