﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using System.IO;
using System.Drawing.Imaging;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendaAnexo : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        SqlCommand comd;
        MemoryStream ms;
        public static Byte[] foto;
        string idUnico;
        int count = 0, i = 0;
        string[] codigo, numero, descricao, codCliente, nomeCliente, emitidoPor, status, codLoja, linha;
        public FormVendaAnexo()
        {
            InitializeComponent();
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        private void txtID_KeyPress(object sender, KeyPressEventArgs e)
        {
            byte imag;
            if (e.KeyChar == (char)Keys.Enter || e.KeyChar == (char)Keys.Tab)
            {
                int i = 0, a = 0;
                try
                {
                    SqlCommand comd = new SqlCommand("SELECT IDunico From ASANEXO With (NoLock) Where IDunico = '" + txtID.Text + "'", conexao);
                    conexao.Open();
                    using (SqlDataReader reader = comd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            idUnico = reader[0].ToString();
                            count = count + 1;
                        }
                    }
                }
                catch (Exception) { }
                finally { conexao.Close(); }

                codigo = new string[count];
                numero = new string[count];
                descricao = new string[count];
                codCliente = new string[count];
                nomeCliente = new string[count];
                emitidoPor = new string[count];
                status = new string[count];
                codLoja = new string[count];
                linha = new string[count];

                SqlCommand cmd = new SqlCommand("Select LTRIM(RTRIM(R.CodEntidade)), NumDoc, R.CodDoc, LTRIM(RTRIM(DescrDoc)), R.STATUS, R.CodLoja,"+
                                                "LTRIM(RTRIM(C.NomeEntidade)), NomeUtilizador From AREGDOC R, AFENTIDADE C, UTILIZADORES U, ASDOCS D"+
                                                " Where R.IDunico = '"+ txtID.Text +"' And R.CODENTIDADE = C.CODENTIDADE And R.CODUTILIZADOR = U.CODUTILIZADOR And R.CodDoc = D.CodDoc", conexao);
                try
                {
                    conexao.Close();
                    conexao.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            codCliente[i] = reader[0].ToString();
                            numero[i] = reader[1].ToString();
                            codigo[i] = reader[2].ToString();
                            descricao[i] = reader[3].ToString();
                            status[i] = reader[4].ToString();
                            codLoja[i] = reader[5].ToString();
                            nomeCliente[i] = reader[6].ToString();
                            emitidoPor[i] = reader[7].ToString();
                            i++;
                        }
                    }
                }
                catch (Exception) { }
                finally { conexao.Close(); }

                try
                {
                    SqlCommand comd1 = new SqlCommand("SELECT ROWID From ASANEXO With (NoLock) Where IDunico = '" + txtID.Text + "'", conexao);
                    conexao.Open();
                    using (SqlDataReader reader = comd1.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            linha[a] = reader[0].ToString();
                            a++;
                        }
                    }

                    if (idUnico != "" || idUnico != null)
                    {
                        foto = new byte[100];
                        SqlCommand cmdSelect = new SqlCommand("SELECT Anexo From ASANEXO With (NoLock) Where ROWID = '" + linha[0] + "'", conexao);

                        //conexao.Open();
                        foto = (byte[])cmdSelect.ExecuteScalar();
                        if (foto != null)
                            pbFoto.Image = byteArrayToImage(foto);
                    }
                }
                catch (Exception) { }
                finally { conexao.Close(); }

                lbCodigo.Text = codigo[0];
                lbNumero.Text = numero[0];
                lbDescricao.Text = descricao[0];
                lbCliente.Text = nomeCliente[0];
                lbEmitidoPor.Text = emitidoPor[0];
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //Exibir a imagem no formulário
                string nomeArquivo = openFileDialog1.FileName;
                Bitmap bmp = new Bitmap(nomeArquivo);
                pbFoto.Image = bmp;

                //salva a imagem no banco de dados
                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, ImageFormat.Bmp);
                foto = ms.ToArray();
            }
        }

        private void btGravar_Click(object sender, EventArgs e)
        {
            SqlCommand comand;
            SqlParameter paramFoto;
            try
            {
                if (txtID.Text != "")
                {
                    conexao.Open();
                    if (pbFoto.Image != null)
                    {
                        comand = new SqlCommand("insert into ASANEXO (IDUnico, DataCria, CodUtilizador, Anexo)" +
                        "values ('" + txtID.Text + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + Variavel.codUtilizador + "',@foto)");
                        paramFoto = new SqlParameter("@foto", SqlDbType.Binary);
                        paramFoto.Value = foto;
                        comand.Connection = conexao;
                        comand.Parameters.Add(paramFoto);
                        comand.ExecuteNonQuery();
                    }
                    conexao.Close();
                    MessageBox.Show("Inserido Com Sucesso");
                }
            }
            catch (Exception) { }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btProximo_Click(object sender, EventArgs e)
        {
            if (count > 1)
            {
                i++;
                if (i == count)
                    i = 0;
                if (idUnico != "" || idUnico != null)
                {
                    try
                    {
                        foto = new byte[100];
                        SqlCommand cmdSelect = new SqlCommand("SELECT Anexo From ASANEXO With (NoLock) Where ROWID = '" + linha[i] + "'", conexao);

                        conexao.Open();
                        foto = (byte[])cmdSelect.ExecuteScalar();
                        if (foto != null)
                            pbFoto.Image = byteArrayToImage(foto);
                    }
                    catch (Exception) { }
                    finally { conexao.Close(); }

                }
            }
        }

        private void btAnterior_Click(object sender, EventArgs e)
        {
            if (count > 1)
            {
                i --;
                if (i < 0)
                    i = count - 1;
                if (idUnico != "" || idUnico != null)
                {
                    try
                    {
                        foto = new byte[100];
                        SqlCommand cmdSelect = new SqlCommand("SELECT Anexo From ASANEXO With (NoLock) Where ROWID = '" + linha[i] + "'", conexao);

                        conexao.Open();
                        foto = (byte[])cmdSelect.ExecuteScalar();
                        if (foto != null)
                            pbFoto.Image = byteArrayToImage(foto);
                    }
                    catch (Exception) { }
                    finally { conexao.Close(); }
                }
            }
        }
    }
}
