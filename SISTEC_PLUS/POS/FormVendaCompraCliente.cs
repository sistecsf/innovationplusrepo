﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendaCompraCliente : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        int tamanho = 0, i = 0;
        string sql, codCliente, nomecliente;
        string datafim, datainicio;
        DateTime data;
        public FormVendaCompraCliente()
        {
            InitializeComponent();
        }
        private void CmbNomeCliente()
        {
            int tipo = 0;
            SqlCommand cmd = new SqlCommand("SP_ComboEntidade", conexao);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new SqlParameter("@p_iTipoSeleccao", tipo));
            cmd.Parameters.Add(new SqlParameter("@p_sNivelUSER", Permissoes.nPrgClie));
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbNomeCliente1.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void imprimir()
        {
            nomecliente = cmbNomeCliente1.Text;
            codCliente = txtNumeroCliente.Text;
            i = 0;
            sql = "Select Distinct R.NUMDOC, STATUS = Case R.STATUS WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO' WHEN 'F' THEN 'PAG. PARCIAL' WHEN 'J' THEN 'ADJUDICADO' WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END, PVP= CASE F.MOEDA WHEN '"+ Gerais.PARAM_MOEDA_NAC+"' THEN F.PVP*F.QTD ELSE '0' END, F.PVPD*F.QTD, Cast(R.DataCria As DateTime), R.CodEntidade, F.Referenc,  F.QTD,  R.IDUNICO, LTrim(RTrim(DescrDoc)), M.DescrArtigo From ASFICMOV1 F  WITH (NOLOCK),  ASDOCS S  WITH (NOLOCK), ASMESTRE M  WITH (NOLOCK), AREGDOC R WITH(NOLOCK)" +
                "Where   F.CodEntidade='" + txtNumeroCliente.Text + "' And R.STATUS!='A' And R.IDunico=F.IDunico And R.NUMDOC=F.NUMDOC And R.CodEntidade=F.CodEntidade And F.CodEntidade IS NOT NULL  And R.CodDoc = S.CodDoc And R.CodDoc=F.CodDoc And R.CodLoja = F.CodLoja And F.referenc = M.referenc And F.codloja = M.codloja And F.CodArmz = M.CodArmz  And R.IDUNICO=F.IDUNICO";

            if (txtDataInicio.Text != "")
                sql = sql + " And CONVERT(DATETIME, R.DataCria, 120) > CONVERT(DATETIME, '" + datainicio + "', 120)";
            else
                sql = sql + " And LEFT( R.DataCria, 10) >= ''";
            if (txtDataFim.Text != "")
                sql = sql + " And CONVERT(DATETIME, R.DataCria, 120) <= CONVERT(DATETIME, '" + datafim + " 18:59" + "', 120) Order By Cast(R.DataCria As DateTime), LTrim(RTrim(DescrDoc)), R.NUMDOC";
            else
                sql = sql + " And LEFT(R.DataCria, 10) <= '' Order By Cast(R.DataCria As DateTime), LTrim(RTrim(DescrDoc)), R.NUMDOC";

            try
            {
                SqlCommand cmd2 = new SqlCommand(sql, conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tamanho++;
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
            
            string[] refrenc = new string[tamanho];
            string[] descr = new string[tamanho];
            string[] qtd = new string[tamanho];
            string[] valor = new string[tamanho];
            string[] docum = new string[tamanho];
            string[] id = new string[tamanho];
            string[] nDoc = new string[tamanho];
            string[] dat = new string[tamanho];
            string[] valorDoc = new string[tamanho];

            try
            {
                SqlCommand cmd = new SqlCommand(sql, conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        nDoc[i] = reader[0].ToString();
                        valorDoc[i] = reader[2].ToString();
                        valor[i] = reader[3].ToString();
                        dat[i] = reader[4].ToString();
                        refrenc[i] = reader[6].ToString();
                        qtd[i] = reader[7].ToString();
                        id[i] = reader[8].ToString();
                        docum[i] = reader[9].ToString();
                        descr[i] = reader[10].ToString();
                        
                        i++;
                    }
                }

                VendaCliente form = new VendaCliente(refrenc, descr, qtd, valor, docum, id, nDoc, dat, valorDoc, tamanho, codCliente, nomecliente, txtDataInicio.Text + " A " + txtDataFim.Text);
                form.Show();
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }
        private void FormVendaCompraCliente_Load(object sender, EventArgs e)
        {
            txtDataInicio.Text = txtDataFim.Text = DateTime.Now.ToShortDateString();
            CmbNomeCliente();
        }
        private void txtNumeroCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                try
                {
                    SqlCommand cmd = new SqlCommand("SELECT RTRIM(NomeEntidade) FROM AFENTIDADE WITH (NOLOCK) where CODENTIDADE = '" + txtNumeroCliente.Text + "'", conexao);
                    conexao.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cmbNomeCliente1.Text = reader[0].ToString();
                            cmbNomeCliente1.PromptText = reader[0].ToString();
                        }
                    }
                }
                catch (Exception) { }
                finally { conexao.Close(); }
            }
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            try { data = Convert.ToDateTime(txtDataInicio.Text); }
            catch (Exception) { data = Convert.ToDateTime("1500-1-1"); }
            datainicio = "" + data.Year + "-" + data.Month + "-" + data.Day;
            try { data = Convert.ToDateTime(txtDataFim.Text); }
            catch (Exception) { data = Convert.ToDateTime("1500-1-1"); }
            datafim = "" + data.Year + "-" + data.Month + "-" + data.Day;

            if (txtNumeroCliente.Text != "")
            {
                Cursor.Current = Cursors.WaitCursor;
                imprimir();
            }
            else
                MessageBox.Show("O prenximento do campo [Número do Cliente] é obrigatório!");
        }
        private void btSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbNomeCliente1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("SELECT CODENTIDADE, ISNULL(DESC1,0), RTRIM(CODTIPOCLI), RTRIM(LTRIM(Display_ContraValor)), Morada, FONE FROM AFENTIDADE WITH (NOLOCK) WHERE RTRIM(NomeEntidade) = '" + cmbNomeCliente1.Text + "' ", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        txtNumeroCliente.Text = reader[0].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }
        }

        private void txtDataInicio_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataInicio.Text); }
            catch (Exception ex) 
            { 
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); 
                txtDataInicio.Text = ""; 
            }
        }

        private void txtDataFim_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtDataFim.Text); }
            catch (Exception ex)
            {
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!");
                txtDataFim.Text = "";
            }
        }
    }
}
