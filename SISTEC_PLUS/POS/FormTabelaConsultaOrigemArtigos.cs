﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS;
using SISTEC_PLUS;
using SISTEC_PLUS.OPS;
using System.Drawing.Imaging;
using System.IO;

namespace SISTEC_PLUS.POS
{
    public partial class FormTabelaConsultaOrigemArtigos : MetroFramework.Forms.MetroForm
    {
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao); // conexão com o banco
        public FormTabelaConsultaOrigemArtigos()
        {
            InitializeComponent();
        }

        private void prenxeGrid()
        {
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(@"select CodOrigem, Descricao from ASORIGEM where CodOrigem like  '" + txtCodigo.Text + "' AND Descricao like '" + txtNome.Text + "%'", conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("Origem");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void FormTabelaConsultaOrigemArtigos_Load(object sender, EventArgs e)
        {
            metroGrid1.AutoGenerateColumns = false;
            prenxeGrid();
        }

        private void metroGrid1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                {
                    FormTabelas.Ocodigo = metroGrid1.CurrentRow.Cells[0].Value.ToString();
                    FormTabelas.Odesignacao = metroGrid1.CurrentRow.Cells[1].Value.ToString();

                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            prenxeGrid();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            prenxeGrid();
        }

        private void metroGrid1_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try 
                {
                    FormTabelas.Ocodigo = metroGrid1.CurrentRow.Cells[0].Value.ToString();
                    FormTabelas.Odesignacao = metroGrid1.CurrentRow.Cells[1].Value.ToString();

                    this.DialogResult = DialogResult.OK;
                }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }
    }
}
