﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using SISTEC_PLUS.POS;
using System.Data.SqlClient;
using System.IO;
using System.Drawing.Imaging;

namespace SISTEC_PLUS.OPS
{   
    public partial class FormTabelas : MetroFramework.Forms.MetroForm
    {
        public static string Lcodigo, Lnome, Lmorada, Lcaixa_post, Ltelefone, Lfax, empresa, Lconta = "", Lconta1, visualisar, agente, emitir, abertura, visita, LcontaN, LcontaP, LcontaO, Lempresa, Limagem;
        public static string Mloja, MareaO, Mcodigo, Mdesignacao, McontaV, McontaC, MmercadoriaV, MmercadoriaR, Mativo, Memitir;
        public static string Fcodigo, Fnome, Fdesconto, FcontabilizacaoVenda, FcontabilizacaoStock, FcontabilizacaoCusto, Fimagem;
        public static string AcodLoja, AcodMerc, AcodFam, AcodSubFam, AcodGrupo, ALoja, AalteradoP, Amercadoria, Afamilia, AsubFamilia, Areferc, Adesig, Adesig2, Agrupo, Adescr, Alucro, Autilizador, AdataC, AdataA, AdataF, Acambio, ApontoEncomenda, Apercquebra, Adesconto, Aunid, AdesgE, Aqtd, Anorma, Apermite, Aativo, Amostrar, Acontabilizar, Apermitir, Atermo, Aregisto, Apromocao, Abooking, Afob, Apca, Apvp, Apmc, Alocal, AstockMin, AstockMax, AregE, Aloja, AdataEx, AcontStock, AdesiSupl, Aimagem, Anegativo, Aobsoleto, Acalcular, Aimposto = "0", Amarca = "", Amodelo = "", AcodAnucio = "";
        public static int Fpraso, foto1 = 0;
        public static string Scodigo, Sfamilia, designacao, SnomeFamilia;
        public static string ALcodigo, ALdesignacaoA, ALdesignacaoL, ALareaOrganica, ALcodF, ALareaO;
        public static string Ocodigo, Odesignacao, retorno;
        public static Byte[] foto;
        public static Bitmap bmp;
        public static MemoryStream ms;
        SqlCommand comd;
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        SqlCommand cmd = new SqlCommand();
        public FormTabelas()
        {
            InitializeComponent();
        }
        private void CmbSFamilia1()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeMerc from ASFamilia WITH (NOLOCK)", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbSFamilia2.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            { MessageBox.Show(ex.Message); }
            finally
            { conexao.Close(); }
        }
        private void CmbMAreaOrganica()
        {
            SqlCommand cmd = new SqlCommand(@"SELECT NomeAO FROM AIAREAORGANICA WITH (NOLOCK)", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbMAreaOrganica.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            {MessageBox.Show(ex.Message);}
            finally{conexao.Close();}
        }
        private void CmbALoja()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeLoja from ASLOJA WITH (NOLOCK)", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbALoja1.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }
        private void CmbAMercadoria()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeArz from ASARMAZ WITH (NOLOCK) where CodLoja= '"+AcodLoja+"'", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbAMercadoria1.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }
        private void CmbAFamilia()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeMerc from ASFamilia WITH (NOLOCK)", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbAFamilia1.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }
        private void CmbASubFamilia()
        {
            SqlCommand cmd = new SqlCommand(@"select Descricao from ASSUBFAMILIA WITH (NOLOCK) where CodFam= '"+AcodFam+"'", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbASubFamilia1.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }
        private void CmbAGrupoWeb()
        {
            SqlCommand cmd = new SqlCommand(@"select nome from GRUPOANUNCIO WITH (NOLOCK)", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbAGrupoWeb1.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }
        private void CmbSFamilia()
        {
            SqlCommand cmd = new SqlCommand(@"select NomeMerc from ASFamilia WITH (NOLOCK)", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbSFamilia2.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }
        private void CmbMDesignacaoLoja()
        {
            SqlCommand cmd = new SqlCommand(@"SELECT NomeLoja FROM ASLOJA WITH (NOLOCK)", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbMDesignacaoLoja.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }
        private void CmbLEmpresa()
        {
            SqlCommand cmd = new SqlCommand(@"SELECT NOME FROM EMPRESA WITH (NOLOCK)", conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    cmbLEmpresa.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }
        private string coverte(string valor)
        {
            double num = 0;
            try{num = Convert.ToDouble(valor);}
            catch (Exception) { num = 0; }
            return "" + num;
        }
        private int inteiro(string valor)
        {
            int num = 0;
            try { num = Convert.ToInt32(valor); }
            catch (Exception) { num = 0; }
            return num;
        }
        private void limparCampos(Control.ControlCollection controles)
        {
            //Faz um laço para todos os controles passados no parâmetro
            foreach (Control ctrl in controles)
            {
                //Se o contorle for um TextBox...
                if (ctrl is MetroFramework.Controls.MetroTextBox)
                {
                    ((MetroFramework.Controls.MetroTextBox)(ctrl)).Text = String.Empty;
                }
                //Se o contorle for um ComboBox...
                if (ctrl is MetroFramework.Controls.MetroComboBox)
                {
                    ((MetroFramework.Controls.MetroComboBox)(ctrl)).Text = null;
                    ((MetroFramework.Controls.MetroComboBox)(ctrl)).PromptText = "";
                }
                //Se o contorle for um ComboBox...
                if (ctrl is MetroFramework.Controls.MetroCheckBox)
                {
                    ((MetroFramework.Controls.MetroCheckBox)(ctrl)).Checked = false;
                }
                //Se o contorle for um RadioButton...
                if (ctrl is MetroFramework.Controls.MetroRadioButton)
                {
                    ((MetroFramework.Controls.MetroRadioButton)(ctrl)).Checked = false;
                }
                //Se o contorle for um PictureBox...
                if (ctrl is PictureBox)
                {
                    ((PictureBox)(ctrl)).Image = null;
                }
                if (ctrl is TextBox)
                {
                    ((TextBox)(ctrl)).Text = String.Empty;
                }
            }
        }
        private void limparLoja()
        {
            limparCampos(metroTabPage1.Controls);
            limparCampos(groupBox3.Controls);
            txtLCodigo.Enabled = true;
            txtLCodigo.Select();
        }
        private void limparMercadoria()
        {
            limparCampos(metroTabPage2.Controls);
            txtMCodigo.Enabled = true;
            txtMCodigo.Select();
        }
        private void limparFamilia()
        {
            limparCampos(metroTabPage3.Controls);
            txtFCodigo.Enabled = true;
            txtFCodigo.Select();
        }
        private void limparSubFamilia()
        {
            limparCampos(metroTabPage4.Controls);
            txtSCodigo.Enabled = true;
            txtSCodigo.Select();
        }
        private void limparArtigos()
        {
            limparCampos(metroTabPage5.Controls);
            limparCampos(groupBox2.Controls);
            limparCampos(groupBox1.Controls);
            limparCampos(metroTabControl2.Controls);
            txtAPVP.Text = "0";
            metroTextBox21.Text = "0";
            txtAFOB.Text = "0";
            txtAPCP.Text = "0";
            metroTextBox10.Text = "0";
            metroTextBox12.Text = "0";
            txtAPCM.Text = "0";
            metroCheckBox15.Checked = false;
            metroCheckBox16.Checked = false;
            metroCheckBox17.Checked = false;
        }
        private void limparOrigemArtigos()
        {
            limparCampos(metroTabPage6.Controls);
            txtOCodigo.Enabled = true;
            txtOCodigo.Select();
        }
        private void FormTabelas_Load(object sender, EventArgs e)
        {
            Limagem = "";
            Fimagem = "";
            TabTabelas.SelectedTab = metroTabPage1;
            cmd.Connection = conexao;
            txtLCodigo.Enabled = false;
            txtMCodigo.Enabled = false;
            txtFCodigo.Enabled = false;
            txtOCodigo.Enabled = false;
            txtALCodigo.Enabled = false;
            txtLContaNacional.ReadOnly = true;
            txtLContaOutros.ReadOnly = true;
            txtLContaPa.ReadOnly = true;
            txtMContaVendas.ReadOnly = true;
            txtMContaCustos.ReadOnly = true;
            cmbLEmpresa.Select();
        }
        private void btNovo_Click(object sender, EventArgs e)
        {
            if (TabTabelas.SelectedTab == metroTabPage1)
            {
                limparLoja(); 
            }
            else if (TabTabelas.SelectedTab == metroTabPage2)
            {
                limparMercadoria();
            }
            else if (TabTabelas.SelectedTab == metroTabPage3)
            {
                limparFamilia();
            }
            else if (TabTabelas.SelectedTab == metroTabPage4)
            {
                limparSubFamilia();
            }
            else if (TabTabelas.SelectedTab == metroTabPage5)
            {
                limparArtigos();
            }
            else if (TabTabelas.SelectedTab == metroTabPage6)
            {
                limparOrigemArtigos();
            }
            else if (TabTabelas.SelectedTab == metroTabPage8)
            {
                txtALCodigo.Enabled = true;
                txtALCodigo.Text = "";
                txtALCodigo.Select();
            }
        }
        private void btGravar_Click(object sender, EventArgs e)
        {
            if (TabTabelas.SelectedTab == metroTabPage1)
            {
                comd = new SqlCommand("select CodLoja from ASLOJA WHERE CodLoja = '" + txtLCodigo.Text + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = comd.ExecuteReader())
                { while (reader.Read()) { retorno = reader[0].ToString(); } }
                conexao.Close();
                if (metroCheckBox1.Checked) visualisar = "S";
                else visualisar = "N";
                if (metroCheckBox2.Checked) agente = "S";
                else agente = "N";
                if (metroCheckBox3.Checked) emitir = "S";
                else emitir = "N";
                if (metroCheckBox4.Checked) abertura = "S";
                else abertura = "N";
                if (metroCheckBox5.Checked) visita = "S";
                else visita = "N";

                SqlCommand comand;
                SqlParameter paramFoto;
                if (txtLCodigo.Text != "")
                {
                    if (txtLDesignacao.Text != "")
                    {
                        if (cmbLEmpresa.Text != "" || cmbLEmpresa.PromptText != "")
                        {
                            if (retorno == "" || retorno == null)
                            {
                                try
                                {
                                    conexao.Open();
                                    if (pbLImagem.Image != null)
                                    {
                                        comand = new SqlCommand("insert into ASLOJA (CodLoja,DataCria, NomeLoja, CodUtilizador, Morada, Caixa_post, Fone, Data_Alteracao, Alterado_Por, LojaOnLine, Obriga_FechoDia, AGTELEC, EMITIR_RC, Visita_Definida,LOGO, CodContaNAC,CodContaOUTROS, CodContaPDR,CodEmpresa)" +
                                        "values ('" + txtLCodigo.Text + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + txtLDesignacao.Text + "','" + Variavel.codUtilizador + "','" + txtLMarada.Text + "','" + txtLCodigoPostal.Text + "','" + txtLTelefone.Text + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + Variavel.codUtilizador + "','" + visualisar + "','" + abertura + "','" + agente + "','" + emitir + "','" + visita + "',@foto,'" + txtLContaNacional.Text + "','" + txtLContaOutros.Text + "','" + txtLContaPa.Text + "','" + empresa + "')");
                                        paramFoto = new SqlParameter("@foto", SqlDbType.Binary);
                                        paramFoto.Value = foto;
                                        comand.Connection = conexao;
                                        comand.Parameters.Add(paramFoto);
                                        comand.ExecuteNonQuery();
                                    }
                                    else
                                    {
                                        comand = new SqlCommand("insert into ASLOJA (CodLoja,DataCria, NomeLoja, CodUtilizador, Morada, Caixa_post, Fone, Data_Alteracao, Alterado_Por, LojaOnLine, Obriga_FechoDia, AGTELEC, EMITIR_RC, Visita_Definida, CodContaNAC,CodContaOUTROS, CodContaPDR,CodEmpresa)" +
                                        "values ('" + txtLCodigo.Text + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + txtLDesignacao.Text + "','" + Variavel.codUtilizador + "','" + txtLMarada.Text + "','" + txtLCodigoPostal.Text + "','" + txtLTelefone.Text + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + Variavel.codUtilizador + "','" + visualisar + "','" + abertura + "','" + agente + "','" + emitir + "','" + visita + "','" + txtLContaNacional.Text + "','" + txtLContaOutros.Text + "','" + txtLContaPa.Text + "','" + empresa + "')");
                                        comand.Connection = conexao;
                                        comand.ExecuteNonQuery();
                                    }

                                    MessageBox.Show("Gravado com sucesso", "Gravar");
                                    txtLContaPa.Text = ""; txtLCodigo.Text = ""; txtLCodigoPostal.Text = ""; txtLContaOutros.Text = ""; txtLDesignacao.Text = "";
                                    txtLFax.Text = ""; txtLMarada.Text = ""; txtLContaNacional.Text = ""; txtLCodigoPostal.Text = "";
                                    txtLTelefone.Text = ""; txtLFax.Text = ""; txtLPraso2.Text = ""; txtLPraso.Text = ""; pbLImagem.Image = null;

                                }
                                catch (SqlException ex) { MessageBox.Show("Não foi possivel Cadastrar essa Loja"); }
                                finally { conexao.Close(); }
                            }
                            else
                            {
                                try
                                {
                                    conexao.Open();
                                    const string message = "Essa Loja já existe. Pretendes Alterar os dados dessa Loja?";
                                    const string caption = "CONFIRMAÇÃO";
                                    var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                    if (result == DialogResult.Yes)
                                    {
                                        if (pbLImagem.Image != null && foto1 == 1)
                                        {

                                            comand = new SqlCommand("update ASLOJA set Morada = '" + txtLMarada.Text + "', NomeLoja = '" + txtLDesignacao.Text + "', Caixa_post ='" + txtLCodigoPostal.Text + "',Fone ='" + txtLTelefone.Text + "',Data_Alteracao =CONVERT(VARCHAR(20),GETDATE(),120), Alterado_Por ='" + Variavel.codUtilizador + "', LojaOnLine ='" + visualisar + "', Obriga_FechoDia ='" + abertura + "', AGTELEC = '" + agente + "', EMITIR_RC ='" + emitir + "', Visita_Definida = '" + visita + "',CodEmpresa = '" + Lempresa + "',CodContaNAC ='" + txtLContaNacional.Text + "',CodContaOUTROS = '" + txtLContaOutros.Text + "', CodContaPDR = '" + txtLContaPa.Text + "',LOGO =@foto where CodLoja = '" + txtLCodigo.Text + "'");
                                            paramFoto = new SqlParameter("@foto", SqlDbType.Binary);
                                            paramFoto.Value = foto;
                                            comand.Connection = conexao;
                                            comand.Parameters.Add(paramFoto);
                                            comand.ExecuteNonQuery();
                                        }
                                        else
                                        {
                                            comand = new SqlCommand("update ASLOJA set Morada = '" + txtLMarada.Text + "', NomeLoja = '" + txtLDesignacao.Text + "', Caixa_post ='" + txtLCodigoPostal.Text + "',Fone ='" + txtLTelefone.Text + "',Data_Alteracao =CONVERT(VARCHAR(20),GETDATE(),120), Alterado_Por ='" + Variavel.codUtilizador + "', LojaOnLine ='" + visualisar + "', Obriga_FechoDia ='" + abertura + "', AGTELEC = '" + agente + "', EMITIR_RC ='" + emitir + "', Visita_Definida = '" + visita + "',CodEmpresa = '" + Lempresa + "',CodContaNAC ='" + txtLContaNacional.Text + "',CodContaOUTROS = '" + txtLContaOutros.Text + "', CodContaPDR = '" + txtLContaPa.Text + "' where CodLoja = '" + txtLCodigo.Text + "'");
                                            comand.Connection = conexao;
                                            comand.ExecuteNonQuery();
                                        }

                                        MessageBox.Show("Alterado com sucesso", "Editar");
                                        txtLContaPa.Text = ""; txtLCodigo.Text = ""; txtLCodigoPostal.Text = ""; txtLContaOutros.Text = "";
                                        txtLDesignacao.Text = "";
                                        txtLFax.Text = ""; txtLMarada.Text = ""; txtLContaNacional.Text = ""; txtLCodigoPostal.Text = "";
                                        txtLTelefone.Text = ""; txtLFax.Text = ""; txtLPraso2.Text = ""; txtLPraso.Text = ""; pbLImagem.Image = null;
                                    }
                                }
                                catch (SqlException ex) { MessageBox.Show("Não foi possivel Alterar os dados dessa Loja"); }
                                finally { conexao.Close(); }
                            }
                        }
                        else { MessageBox.Show("O Campo [Empreza] Tem de ser Preenchido!"); }
                    }
                    else { MessageBox.Show("O Campo [Designação] Tem de ser Preenchido!"); }
                }
                else { MessageBox.Show("O Campo [Código] Tem de ser Preenchido!"); }
            }
            else if (TabTabelas.SelectedTab == metroTabPage2) {
                retorno = "";
                comd = new SqlCommand("select CodArmz from ASARMAZ WHERE CodArmz = '" + txtMCodigo.Text + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = comd.ExecuteReader())
                { while (reader.Read()) { retorno = reader[0].ToString(); } }
                conexao.Close();
                if (cbMMercadoriaVenda.Checked) MmercadoriaV = "S";
                else MmercadoriaV = "N";
                if (cbMMercadoriaVenda.Checked) MmercadoriaR = "S";
                else MmercadoriaR = "N";
                if (cbMActivo.Checked) Mativo = "S";
                else Mativo = "N";
                if (cbMEmitirRI.Checked) Memitir = "S";
                else Memitir = "N";

                conexao.Open();
                SqlCommand comand;
                if (txtMCodigo.Text != "")
                {
                    if (txtMDesignacao.Text != "")
                    {
                        if (cmbMDesignacaoLoja.Text != "" || cmbMDesignacaoLoja.PromptText != "")
                        {
                            if (cmbMAreaOrganica.Text != "" || cmbMAreaOrganica.PromptText != "")
                            {
                                if (retorno == "" || retorno == null)
                                {
                                    try
                                    {
                                        comand = new SqlCommand("insert into ASARMAZ (CodArmz, DATACRIA, NomeArz, CodUtilizador, CodLoja, Morada, ContVend, Data_Alteracao,                                                                         ContCusto,ARMA_VENDA,ARMA_REMOTO,Ativo,EMITIR_RI)" +
                                                "values ('" + txtMCodigo.Text + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + txtMDesignacao.Text + "','" + Variavel.codUtilizador + "','" + Mloja + "','" + MareaO + "','" + txtMContaVendas.Text + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + txtMContaCustos.Text + "','" + MmercadoriaV + "','" + MmercadoriaR + "','" + Mativo + "','" + Memitir + "')");
                                        comand.Connection = conexao;
                                        comand.ExecuteNonQuery();
                                        MessageBox.Show("Gravado com sucesso", "Gravar");
                                        txtMCodigo.Text = ""; txtMDesignacao.Text = ""; txtMContaCustos.Text = ""; txtMContaVendas.Text = ""; cmbMAreaOrganica.PromptText = ""; cmbMDesignacaoLoja.PromptText = ""; cmbMAreaOrganica.Text = ""; cmbMDesignacaoLoja.Text = "";
                                        cbMMercadoriaVenda.Checked = false; cbMMercadoriaVenda.Checked = false; cbMActivo.Checked = false; cbMEmitirRI.Checked = false;
                                    }
                                    catch (SqlException ex) { MessageBox.Show("Não foi posivel Cadastrar esse Armazém"); }
                                    finally { conexao.Close(); }
                                }
                                else
                                {
                                    try
                                    {
                                        //conexao.Open();
                                        const string message = "Esse Armazém já existe. Pretendes Alterar os dados dessa Armazém?";
                                        const string caption = "CONFIRMAÇÃO";
                                        var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                        if (result == DialogResult.Yes)
                                        {
                                            comand = new SqlCommand("update ASARMAZ set NomeArz = '" + txtMDesignacao.Text + "', Alterado_Por = '" + Variavel.codUtilizador + "', CodLoja ='" + Mloja + "',Morada ='" + MareaO + "',ContVend ='" + txtMContaVendas.Text + "', Data_Alteracao =CONVERT(VARCHAR(20),GETDATE(),120), ContCusto ='" + txtMContaCustos.Text + "', ARMA_VENDA ='" + MmercadoriaV + "', ARMA_REMOTO = '" + MmercadoriaR + "', Ativo ='" + Mativo + "', EMITIR_RI = '" + Memitir + "' where CodArmz = '" + txtMCodigo.Text + "'");
                                            comand.Connection = conexao;
                                            comand.ExecuteNonQuery();
                                            MessageBox.Show("Alterado com sucesso", "Editar");
                                            txtMCodigo.Text = ""; txtMDesignacao.Text = ""; txtMContaCustos.Text = ""; txtMContaVendas.Text = ""; cmbMAreaOrganica.PromptText = ""; cmbMDesignacaoLoja.PromptText = ""; cmbMAreaOrganica.Text = ""; cmbMDesignacaoLoja.Text = "";
                                            lbArea.Text = ""; lbLoja.Text = ""; cbMMercadoriaVenda.Checked = false; cbMMercadoriaRemoto.Checked = false; cbMActivo.Checked = false; cbMEmitirRI.Checked = false;
                                        }
                                    }
                                    catch (SqlException ex) { MessageBox.Show("Não foi posivel Alterar os dados desse Armazém"); }
                                    finally { conexao.Close(); }
                                }
                            }
                            else { MessageBox.Show("O Campo [Área Organica] Tem de ser Preenchido!"); }
                        }
                        else { MessageBox.Show("O Campo [Designação Loja] Tem de ser Preenchido!"); }
                    }
                    else { MessageBox.Show("O Campo [Designação] Tem de ser Preenchido!"); }
                }
                else { MessageBox.Show("O Campo [Código] Tem de ser Preenchido!"); }
            }
            else if (TabTabelas.SelectedTab == metroTabPage3)
            {
                retorno = "";
                comd = new SqlCommand("SELECT CodFam FROM ASFamilia where CodFam = '" + txtFCodigo.Text + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = comd.ExecuteReader())
                { while (reader.Read()) { retorno = reader[0].ToString(); } }
                conexao.Close();
                if (cbFContabilizacaoVenda.Checked) FcontabilizacaoVenda = "S";
                else FcontabilizacaoVenda = "N";
                if (cbFContabilizacaoStock.Checked) FcontabilizacaoStock = "S";
                else FcontabilizacaoStock = "N";
                if (cbFContabilizacaoCusto.Checked) FcontabilizacaoCusto = "S";
                else FcontabilizacaoCusto = "N";
                
                conexao.Open();
                SqlCommand comand;
                SqlParameter paramFoto;
                if (txtFCodigo.Text != "")
                {
                    if (txtFDesignacao.Text != "")
                    {
                        if (retorno == "" || retorno == null)
                        {
                            string desconto = "0", prazo = "0";
                            if (txtFDesconto.Text != "")
                                desconto = txtFDesconto.Text;
                            if (txtFPrazo.Text != "")
                                prazo = txtFPrazo.Text;
                            try
                            {
                                if (pbFImagem.Image != null)
                                {
                                    comand = new SqlCommand("insert into ASFamilia (CodFam, DataCria, NomeMerc, CodUtilizador, ContVend, ContStok,ContCusto, Desconto, Prazo, IMAGEM)" +
                                    "values ('" + txtFCodigo.Text + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + txtFDesignacao.Text + "','" + Variavel.codUtilizador + "','" + FcontabilizacaoVenda + "','" + FcontabilizacaoStock + "','" + FcontabilizacaoCusto + "','" + desconto + "','" + prazo + "',@foto)");
                                    paramFoto = new SqlParameter("@foto", SqlDbType.Binary);
                                    paramFoto.Value = foto;
                                    comand.Connection = conexao;
                                    comand.Parameters.Add(paramFoto);
                                    comand.ExecuteNonQuery();
                                }
                                else
                                {
                                    comand = new SqlCommand("insert into ASFamilia (CodFam, DataCria, NomeMerc, CodUtilizador, ContVend, ContStok,ContCusto, Desconto, Prazo)" +
                                    "values ('" + txtFCodigo.Text + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + txtFDesignacao.Text + "','" + Variavel.codUtilizador + "','" + FcontabilizacaoVenda + "','" + FcontabilizacaoStock + "','" + FcontabilizacaoCusto + "','" + desconto + "','" + prazo + "')");
                                    comand.Connection = conexao;
                                    comand.ExecuteNonQuery();
                                }

                                MessageBox.Show("Gravado com sucesso", "Gravar");
                                txtFCodigo.Text = ""; txtFDesignacao.Text = ""; txtFDesconto.Text = ""; txtFPrazo.Text = ""; pbFImagem.Image = null;
                                cbFContabilizacaoVenda.Checked = false; cbFContabilizacaoStock.Checked = false; cbFContabilizacaoCusto.Checked = false;

                            }
                            catch (SqlException ex) { MessageBox.Show("Não foi possivel Guardar Essa Família"); }
                            finally { conexao.Close(); }
                        }
                        else
                        {
                            try
                            {
                                const string message = "Essa Família já existe. Pretendes Alterar os dados dessa Família?";
                                const string caption = "CONFIRMAÇÃO";
                                var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (result == DialogResult.Yes)
                                {
                                    if (pbFImagem.Image != null && foto1 == 1)
                                    {
                                        comand = new SqlCommand("update ASFamilia set NomeMerc = '" + txtFDesignacao.Text + "', ContVend = '" + FcontabilizacaoVenda + "', ContStok ='" + FcontabilizacaoStock + "',ContCusto ='" + FcontabilizacaoCusto + "',Data_Alteracao =CONVERT(VARCHAR(20),GETDATE(),120), Alterado_Por ='" + Variavel.codUtilizador + "', Desconto ='" + Convert.ToDouble(txtFDesconto.Text) + "', Prazo ='" + txtFPrazo.Text + "',IMAGEM = '" + SqlDbType.Binary + "' where CodFam = '" + txtFCodigo.Text + "'");
                                        comand.Connection = conexao;
                                        comand.ExecuteNonQuery();
                                    }
                                    else
                                    {
                                        comand = new SqlCommand("update ASFamilia set NomeMerc = '" + txtFDesignacao.Text + "', ContVend = '" + FcontabilizacaoVenda + "', ContStok ='" + FcontabilizacaoStock + "',ContCusto ='" + FcontabilizacaoCusto + "',Data_Alteracao =CONVERT(VARCHAR(20),GETDATE(),120), Alterado_Por ='" + Variavel.codUtilizador + "', Desconto ='" + Convert.ToDouble(txtFDesconto.Text) + "', Prazo ='" + txtFPrazo.Text + "' where CodFam = '" + txtFCodigo.Text + "'");
                                        comand.Connection = conexao;
                                        comand.ExecuteNonQuery();
                                    }

                                    MessageBox.Show("Alterado com sucesso", "Editar");
                                    txtFCodigo.Text = ""; txtFDesignacao.Text = ""; txtFDesconto.Text = ""; txtFPrazo.Text = ""; pbFImagem.Image = null;
                                    cbFContabilizacaoVenda.Checked = false; cbFContabilizacaoStock.Checked = false; cbFContabilizacaoCusto.Checked = false;
                                }
                            }
                            catch (SqlException ex) { MessageBox.Show("Não foi possivel Alterar os dados dessa Família"); }
                            finally { conexao.Close(); }
                        }
                    }
                    else { MessageBox.Show("O Campo [Designação] Tem de ser Preenchido!"); }
                }
                else { MessageBox.Show("O Campo [Código] Tem de ser Preenchido!"); }
            }
            else if (TabTabelas.SelectedTab == metroTabPage4)
            {
                retorno = "";
                comd = new SqlCommand("SELECT CodSubFam FROM ASSUBFAMILIA where CodSubFam = '" + txtSCodigo.Text + "' AND CodFam = '" + Sfamilia + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = comd.ExecuteReader())
                { while (reader.Read()) { retorno = reader[0].ToString(); } }
                conexao.Close();
                
                SqlCommand comand;
                if (txtSCodigo.Text != "")
                {
                    if (txtSDesignacao.Text != "")
                    {
                        if (cmbSFamilia2.PromptText != "" || cmbSFamilia2.Text != "")
                        {
                            if (retorno == "" || retorno == null)
                            {
                                try
                                {
                                    conexao.Open();
                                    comand = new SqlCommand("insert into ASSUBFAMILIA (CodSubFam, DataCria, Descricao, CodUtilizador, CodFam)" +
                                            "values ('" + txtSCodigo.Text + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + txtSDesignacao.Text + "','" + Variavel.codUtilizador + "','" + Sfamilia + "')");
                                    comand.Connection = conexao;
                                    comand.ExecuteNonQuery();
                                    MessageBox.Show("Gravado com sucesso", "Gravar");
                                    txtSCodigo.Text = ""; txtSDesignacao.Text = ""; cmbSFamilia2.PromptText = ""; cmbSFamilia2.Text = "";
                                }
                                catch (SqlException ex) { MessageBox.Show("Náo foi possivel guardar essa sub-família!"); }
                                finally { conexao.Close(); }
                            }
                            else
                            {
                                try
                                {
                                    conexao.Open();
                                    const string message = "Essa SubFamília já existe. Pretendes Alterar os dados dessa SubFamília?";
                                    const string caption = "CONFIRMAÇÃO";
                                    var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                    if (result == DialogResult.Yes)
                                    {
                                        comand = new SqlCommand("update ASSUBFAMILIA set Descricao = '" + txtSDesignacao.Text + "', CodFam = '" + Sfamilia + "' where CodSubFam = '" + txtSCodigo.Text + "'");
                                        comand.Connection = conexao;
                                        comand.ExecuteNonQuery();
                                        MessageBox.Show("Alterado com sucesso", "Editar");
                                        txtSCodigo.Text = ""; txtSDesignacao.Text = ""; cmbSFamilia2.PromptText = ""; cmbSFamilia2.Text = "";
                                    }
                                }
                                catch (SqlException ex) { MessageBox.Show("Náo foi possivel alterar os dados dessa sub-família!"); }
                                finally { conexao.Close(); }
                            }
                        }
                        else { MessageBox.Show("O Campo [Família] Tem de ser Preenchido!"); }
                    }
                    else { MessageBox.Show("O Campo [Designação] Tem de ser Preenchido!"); }
                }
                else { MessageBox.Show("O Campo [Código] Tem de ser Preenchido!"); }
            }
            else if (TabTabelas.SelectedTab == metroTabPage5)
            {
                retorno = "";
                comd = new SqlCommand("SELECT Referenc FROM ASMESTRE where Referenc = '" + txtAReferencia.Text + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = comd.ExecuteReader())
                { while (reader.Read()) { retorno = reader[0].ToString(); } }
                conexao.Close();

                if (cbAPermiteAlterar.Checked) Apermite = "S";
                else Apermite = "N";
                if (cbAActivo.Checked) Aativo = "S";
                else Aativo = "N";
                if (cbAMostraImagem.Checked) Amostrar = "S";
                else Amostrar = "N";
                if (cbAContabilizarStock.Checked) Acontabilizar = "S";
                else Acontabilizar = "N";
                if (cbAPermiteOBS.Checked) Apermitir = "S";
                else Apermitir = "N";
                if (cbATermoGarantia.Checked) Atermo = "S";
                else Atermo = "N";
                if (cbARegistoEquipamento.Checked) Aregisto = "S";
                else Aregisto = "N";
                if (cbAPromocao.Checked) Apromocao = "S";
                else Apromocao = "N";
                if (cbABookingHotel.Checked) Abooking = "S";
                else Abooking = "N";
                if (metroCheckBox17.Checked) Anegativo = "S";
                else Anegativo = "N";
                if (metroCheckBox15.Checked) Acalcular = "S";
                else Acalcular = "N";
                if (rbAInsento.Checked) Aimposto = "0";
                else if (rbAImposto5.Checked) Aimposto = "5";
                else if (rbAImposto10.Checked) Aimposto = "10";
                if (txtAReferencia.Text != "")
                {
                    conexao.Open();
                    SqlCommand comand;
                    SqlParameter paramFoto;
                    if (retorno == "" || retorno == null)
                    {
                        try
                        {
                            if (pictureBox1.Image != null)
                            {
                                comand = new SqlCommand("insert into ASMESTRE (CodArmz,CodLoja,referenc,                   DescrArtigo,                 CodFam,          CodSubFam,                       QTD,                                     fob,                                               pca,                                             pvp,                                    datacria,                        data_fabrico,                  CodUtilizador,                 unidade,                   desembalagem,                         DESCR_SUP,                                   STOCKMAX,                                 STOCKMIN,                             STOCKTOTAL,                    LOCALI,                     QUAEMBALAGEM,             RegEquip,                                 pmc,                                 Norma,              Alterapreco,        Contastocks,                                 percquebra,                                  Data_Expira,                                 Desconto,                          ImpostoConsumo,            Lucro,               Activo,           Permitir_Obs,            PONTOENCOMENDA,          STOCKNEGATIVO,         PROMOCAO,          CODMARCA,        CODMODELO,          CALCULA_PCL,        HotelBooKing, IMAGEM)" +
                                       "values ('" + AcodMerc + "','" + AcodLoja + "','" + txtAReferencia.Text + "','" + txtADesignacao.Text + "','" + AcodFam + "','" + AcodSubFam + "','" + inteiro(txtAStockReal.Text) + "','" + coverte(txtAFOB.Text).Replace(',', '.') + "','" + coverte(txtAPCP.Text).Replace(',', '.') + "','" + coverte(txtAPVP.Text).Replace(',', '.') + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + txtADataFabrico.Text + "','" + Variavel.codUtilizador + "','" + cmbAUnidade.Text + "','" + cmbADesigEmbalagem.Text + "','" + txtADesignacapSuplementar.Text + "','" + inteiro(txtAStockMaximo.Text) + "','" + inteiro(txtAStockMimimo.Text) + "','" + inteiro(txtAStockReal.Text) + "','" + txtALocal.Text + "','" + txtQdtEmbalagem.Text + "','" + Aregisto + "','" + coverte(metroTextBox13.Text).Replace(',', '.') + "','" + txtANorme.Text + "','" + Apermite + "','" + AcontStock + "','" + coverte(txtAPercetagemQeuebra.Text).Replace(',', '.') + "','" + txtADataExpiracao.Text + "','" + coverte(txtADesconto.Text).Replace(',', '.') + "','" + Aimposto + "','" + txtALucro.Text + "','" + Aativo + "','" + Apermitir + "','" + txtAPontoEncomenda.Text + "','" + Anegativo + "','" + Apromocao + "','" + Amarca + "','" + Amodelo + "', '" + Acalcular + "', '" + Abooking + "',@foto)");
                                paramFoto = new SqlParameter("@foto", SqlDbType.Binary);
                                paramFoto.Value = foto;
                                comand.Connection = conexao;
                                comand.Parameters.Add(paramFoto);
                                comand.ExecuteNonQuery();
                            }
                            else
                            {
                                comand = new SqlCommand("Insert Into ASMESTRE (CodArmz,CodLoja,                   referenc,       DescrArtigo,         CodFam,              CodSubFam,                          fob,                                          QTD,                                    pca,                                               pvp,                                      datacria,                        data_fabrico,                 CodUtilizador,                 unidade,                 desembalagem,                         DESCR_SUP,                                   STOCKMAX,                             STOCKMIN,                               STOCKTOTAL,                     LOCALI,                   QUAEMBALAGEM,               RegEquip,                                 pmc,                                 Norma,              Alterapreco,         Contastocks,                               percquebra,                                   Data_Expira,                                   Desconto,                         ImpostoConsumo,           Lucro,               Activo,          Permitir_Obs,            PONTOENCOMENDA,           STOCKNEGATIVO,         PROMOCAO,          CODMARCA,       CODMODELO,        CALCULA_PCL,       HotelBooKing)" +
                                       "values ('" + AcodMerc + "','" + AcodLoja + "','" + txtAReferencia.Text + "','" + txtADesignacao.Text + "','" + AcodFam + "','" + AcodSubFam + "','" + coverte(txtAFOB.Text).Replace(',', '.') + "','" + inteiro(txtAStockReal.Text) + "','" + coverte(txtAPCP.Text).Replace(',', '.') + "','" + coverte(txtAPVP.Text).Replace(',', '.') + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + txtADataFabrico.Text + "','" + Variavel.codUtilizador + "','" + cmbAUnidade.Text + "','" + cmbADesigEmbalagem.Text + "','" + txtADesignacapSuplementar.Text + "','" + inteiro(txtAStockMaximo.Text) + "','" + inteiro(txtAStockMimimo.Text) + "','" + inteiro(txtAStockReal.Text) + "','" + txtALocal.Text + "','" + txtQdtEmbalagem.Text + "','" + Aregisto + "','" + coverte(metroTextBox13.Text).Replace(',', '.') + "','" + txtANorme.Text + "','" + Apermite + "','" + AcontStock + "','" + coverte(txtAPercetagemQeuebra.Text).Replace(',', '.') + "','" + txtADataExpiracao.Text + "','" + coverte(txtADesconto.Text).Replace(',', '.') + "','" + Aimposto + "','" + txtALucro.Text + "','" + Aativo + "','" + Apermitir + "','" + txtAPontoEncomenda.Text + "','" + Anegativo + "','" + Apromocao + "','" + Amarca + "','" + Amodelo + "','" + Acalcular + "','" + Abooking + "')");
                                comand.Connection = conexao;
                                comand.ExecuteNonQuery();
                            }
                            MessageBox.Show("Gravado com sucesso", "Gravar");
                        }
                        catch (SqlException ex) { MessageBox.Show("Não foi possivel guradar esse Artigo"); }
                        finally { conexao.Close(); }
                    }
                    else
                    {
                        const string message = "Esse Artigo já existe. Pretendes Alterar os dados desse Artigo?";
                        const string caption = "CONFIRMAÇÃO";
                        var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            try
                            {
                                if (pictureBox1.Image != null && foto1 == 1)
                                {
                                    if (Aregisto == "S")
                                    {
                                        comand = new SqlCommand("UPDATE ASMESTRE SET CodFam='" + AcodFam + "', CodSubFam='" + AcodSubFam + "', DescrArtigo='" + txtADesignacao.Text + "', data_fabrico='" + txtADataFabrico.Text + "', unidade='" + cmbAUnidade.Text + "', desembalagem='" + cmbADesigEmbalagem.Text + "', DESCR_SUP='" + txtADesignacapSuplementar.Text + "', LOCALI='" + txtALocal.Text + "', QUAEMBALAGEM='" + txtQdtEmbalagem.Text + "', RegEquip='" + Aregisto + "', pmc='" + coverte(metroTextBox13.Text).Replace(',', '.') + "', Norma='" + txtANorme.Text + "', Alterapreco='" + Apermite + "', Contastocks='" + AcontStock + "', QTD='" + inteiro(txtAStockReal.Text) + "', percquebra='" + coverte(txtADesconto.Text).Replace(',', '.') + "', data_expira='" + txtADataExpiracao.Text + "', STOCKMIN='" + inteiro(txtAStockMimimo.Text) + "', PONTOENCOMENDA='" + inteiro(txtAPontoEncomenda.Text) + "', Lucro='" + coverte(txtALucro.Text).Replace(',', '.') + "', IMAGEM='" + SqlDbType.Binary + "', FOB='" + coverte(txtAFOB.Text).Replace(',', '.') + "', PCA='" + coverte(txtAPCP.Text).Replace(',', '.') + "', PVP='" + coverte(txtAPVP.Text).Replace(',', '.') + "', ImpostoConsumo='" + Aimposto + "', Desconto='" + coverte(txtADesconto.Text).Replace(',', '.') + "', Activo='" + Aativo + "', Permitir_Obs='" + Apermitir + "', IMAGEM_WEB='" + Amostrar + "', IdAnuncio='" + AcodAnucio + "', PROMOCAO='" + Apromocao + "', STOCKNEGATIVO='" + Anegativo + "', Data_Alteracao=CONVERT(VARCHAR(20),GETDATE(),120), Alterado_Por='" + Variavel.codUtilizador + "', CODMARCA='" + Amarca + "', CODMODELO='" + Amodelo + "', Calcula_PCL='" + Acalcular + "', HotelBooKing='" + Abooking + "' WHERE CodLoja='" + AcodLoja + "' AND CodArmz='" + AcodMerc + "' AND Referenc='" + Areferc + "'");
                                        comand.Connection = conexao;
                                        comand.ExecuteNonQuery();
                                    }
                                    else
                                    {
                                        comand = new SqlCommand("UPDATE ASMESTRE SET CodFam='" + AcodFam + "', CodSubFam='" + AcodSubFam + "', DescrArtigo='" + txtADesignacao.Text + "', data_fabrico='" + txtADataFabrico.Text + "', unidade='" + cmbAUnidade.Text + "', desembalagem='" + cmbADesigEmbalagem.Text + "', DESCR_SUP='" + txtADesignacapSuplementar.Text + "', LOCALI='" + txtALocal.Text + "', QUAEMBALAGEM='" + txtQdtEmbalagem.Text + "',pmc='" + coverte(metroTextBox13.Text).Replace(',', '.') + "', Norma='" + txtANorme.Text + "', Alterapreco='" + Apermite + "', Contastocks='" + AcontStock + "', QTD='" + inteiro(txtAStockReal.Text) + "', percquebra='" + coverte(txtADesconto.Text).Replace(',', '.') + "', data_expira='" + txtADataExpiracao.Text + "', STOCKMIN='" + inteiro(txtAStockMimimo.Text) + "', PONTOENCOMENDA='" + inteiro(txtAPontoEncomenda.Text) + "', Lucro='" + coverte(txtALucro.Text).Replace(',', '.') + "', IMAGEM='" + SqlDbType.Binary + "', FOB='" + coverte(txtAFOB.Text).Replace(',', '.') + "', PCA='" + coverte(txtAPCP.Text).Replace(',', '.') + "', PVP='" + coverte(txtAPVP.Text).Replace(',', '.') + "', Desconto='" + coverte(txtADesconto.Text).Replace(',', '.') + "', ImpostoConsumo='" + Aimposto + "', Activo='" + Aativo + "', Permitir_Obs='" + Apermitir + "', IMAGEM_WEB='" + Amostrar + "', IdAnuncio='" + AcodAnucio + "', PROMOCAO='" + Apromocao + "', STOCKNEGATIVO='" + Anegativo + "', Data_Alteracao=CONVERT(VARCHAR(20),GETDATE(),120), Alterado_Por='" + Variavel.codUtilizador + "', Calcula_PCL='" + Acalcular + "', HotelBooKing='" + Abooking + "' WHERE CodLoja='" + AcodLoja + "' AND CodArmz='" + AcodMerc + "' AND Referenc='" + Areferc + "'");
                                        comand.Connection = conexao;
                                        comand.ExecuteNonQuery();
                                    }
                                }
                                else
                                {
                                    if (Aregisto == "S")
                                    {
                                        comand = new SqlCommand("UPDATE ASMESTRE SET CodFam='" + AcodFam + "', CodSubFam='" + AcodSubFam + "', DescrArtigo='" + txtADesignacao.Text + "', data_fabrico='" + txtADataFabrico.Text + "', unidade='" + cmbAUnidade.Text + "', desembalagem='" + cmbADesigEmbalagem.Text + "', DESCR_SUP='" + txtADesignacapSuplementar.Text + "', LOCALI='" + txtALocal.Text + "', QUAEMBALAGEM='" + txtQdtEmbalagem.Text + "', RegEquip='" + Aregisto + "', pmc='" + coverte(metroTextBox13.Text).Replace(',', '.') + "', Norma='" + txtANorme.Text + "', Alterapreco='" + Apermite + "', Contastocks='" + AcontStock + "', QTD='" + inteiro(txtAStockReal.Text) + "', percquebra='" + coverte(txtADesconto.Text).Replace(',', '.') + "', data_expira='" + txtADataExpiracao.Text + "', STOCKMIN='" + inteiro(txtAStockMimimo.Text) + "', PONTOENCOMENDA='" + inteiro(txtAPontoEncomenda.Text) + "', Lucro='" + coverte(txtALucro.Text).Replace(',', '.') + "', FOB='" + coverte(txtAFOB.Text).Replace(',', '.') + "', PCA='" + coverte(txtAPCP.Text).Replace(',', '.') + "', PVP='" + coverte(txtAPVP.Text).Replace(',', '.') + "', ImpostoConsumo='" + Aimposto + "', Desconto='" + coverte(txtADesconto.Text).Replace(',', '.') + "', Activo='" + Aativo + "', Permitir_Obs='" + Apermitir + "', IMAGEM_WEB='" + Amostrar + "', IdAnuncio='" + AcodAnucio + "', PROMOCAO='" + Apromocao + "', STOCKNEGATIVO='" + Anegativo + "', Data_Alteracao=CONVERT(VARCHAR(20),GETDATE(),120), Alterado_Por='" + Variavel.codUtilizador + "', CODMARCA='" + Amarca + "', CODMODELO='" + Amodelo + "', Calcula_PCL='" + Acalcular + "', HotelBooKing='" + Abooking + "' WHERE CodLoja='" + AcodLoja + "' AND CodArmz='" + AcodMerc + "' AND Referenc='" + Areferc + "'");
                                        comand.Connection = conexao;
                                        comand.ExecuteNonQuery();
                                    }
                                    else
                                    {
                                        comand = new SqlCommand("UPDATE ASMESTRE SET CodFam='" + AcodFam + "', CodSubFam='" + AcodSubFam + "', DescrArtigo='" + txtADesignacao.Text + "', data_fabrico='" + txtADataFabrico.Text + "', unidade='" + cmbAUnidade.Text + "', desembalagem='" + cmbADesigEmbalagem.Text + "', DESCR_SUP='" + txtADesignacapSuplementar.Text + "', LOCALI='" + txtALocal.Text + "', QUAEMBALAGEM='" + txtQdtEmbalagem.Text + "',pmc='" + coverte(metroTextBox13.Text).Replace(',', '.') + "', Norma='" + txtANorme.Text + "', Alterapreco='" + Apermite + "', Contastocks='" + AcontStock + "', QTD='" + inteiro(txtAStockReal.Text) + "', percquebra='" + coverte(txtADesconto.Text).Replace(',', '.') + "', data_expira='" + txtADataExpiracao.Text + "', STOCKMIN='" + inteiro(txtAStockMimimo.Text) + "', PONTOENCOMENDA='" + inteiro(txtAPontoEncomenda.Text) + "', Lucro='" + coverte(txtALucro.Text).Replace(',', '.') + "', FOB='" + coverte(txtAFOB.Text).Replace(',', '.') + "', PCA='" + coverte(txtAPCP.Text).Replace(',', '.') + "', PVP='" + coverte(txtAPVP.Text).Replace(',', '.') + "', Desconto='" + coverte(txtADesconto.Text).Replace(',', '.') + "', ImpostoConsumo='" + Aimposto + "', Activo='" + Aativo + "', Permitir_Obs='" + Apermitir + "', IMAGEM_WEB='" + Amostrar + "', IdAnuncio='" + AcodAnucio + "', PROMOCAO='" + Apromocao + "', STOCKNEGATIVO='" + Anegativo + "', Data_Alteracao=CONVERT(VARCHAR(20),GETDATE(),120), Alterado_Por='" + Variavel.codUtilizador + "', Calcula_PCL='" + Acalcular + "', HotelBooKing='" + Abooking + "' WHERE CodLoja='" + AcodLoja + "' AND CodArmz='" + AcodMerc + "' AND Referenc='" + Areferc + "'");
                                        comand.Connection = conexao;
                                        comand.ExecuteNonQuery();
                                    }
                                }
                                MessageBox.Show("Alterado com sucesso", "Editar");
                            }
                            catch (SqlException ex) { MessageBox.Show("Não foi possível Alterar od Dados desse Artigo"); }
                            finally { conexao.Close(); }
                        }
                    }
                }
                else { MessageBox.Show("O Campo [Referência] Tem de ser Preenchido!"); }
            }
            else if (TabTabelas.SelectedTab == metroTabPage6)
            {
                retorno = "";
                comd = new SqlCommand("SELECT CodOrigem FROM ASORIGEM where CodOrigem = '" + txtOCodigo.Text + "'", conexao);
                conexao.Open();
                using (SqlDataReader reader = comd.ExecuteReader())
                { while (reader.Read()) { retorno = reader[0].ToString(); } }
                conexao.Close();

                conexao.Open();
                SqlCommand comand;
                if (txtOCodigo.Text != "")
                {
                    if (txtODesignacao.Text != "")
                    {
                        if (retorno == "" || retorno == null)
                        {
                            try
                            {
                                comand = new SqlCommand("insert into ASORIGEM (CodOrigem, DataCria, Descricao, CodUtilizador)" +
                                        "values ('" + txtOCodigo.Text + "',CONVERT(VARCHAR(20),GETDATE(),120),'" + txtODesignacao.Text + "','" + Variavel.codUtilizador + "')");

                                comand.Connection = conexao;
                                comand.ExecuteNonQuery();
                                MessageBox.Show("Gravado com sucesso", "Gravar");
                                txtOCodigo.Text = ""; txtODesignacao.Text = "";
                            }
                            catch (SqlException ex) { MessageBox.Show("Não foi possível gravar essa Origem!"); }
                            finally { conexao.Close(); }
                        }
                        else
                        {
                            const string message = "Essa Origem já existe. Pretendes Alterar os dados dessa Origem?";
                            const string caption = "CONFIRMAÇÃO";
                            var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                try
                                {
                                    comand = new SqlCommand("update ASORIGEM set Descricao = '" + txtODesignacao.Text + "' where CodOrigem = '" + txtOCodigo.Text + "'");
                                    comand.Connection = conexao;
                                    comand.ExecuteNonQuery();
                                    MessageBox.Show("Alterado com sucesso", "Editar");
                                    txtOCodigo.Text = ""; txtODesignacao.Text = "";
                                }
                                catch (SqlException ex) { MessageBox.Show("Não foi possível Alterar os dados dessa Origem!"); }
                                finally { conexao.Close(); }
                            }
                        }
                    }
                    else { MessageBox.Show("O Campo [Dedignação] Tem de ser Preenchido!"); }
                }
                else { MessageBox.Show("O Campo [Código] Tem de ser Preenchido!"); }
            }
            else if (TabTabelas.SelectedTab == metroTabPage8)
            {

            }
        }

        private void btConsultar_Click(object sender, EventArgs e)
        {
            foto1 = 0;
            if (TabTabelas.SelectedTab == metroTabPage1)
            {
                FormTabelaLojaConsultar form = new FormTabelaLojaConsultar();
                if (form.ShowDialog() == DialogResult.OK)
                {
                    txtLCodigo.Text = Lcodigo;
                    txtLDesignacao.Text = Lnome;
                    txtLMarada.Text = Lmorada;
                    txtLCodigoPostal.Text = Lcaixa_post;
                    txtLTelefone.Text = Ltelefone;
                    txtLFax.Text = Lfax;
                    txtLContaNacional.Text = LcontaN;
                    txtLContaOutros.Text = LcontaO;
                    txtLContaPa.Text = LcontaP;
                    cmbLEmpresa.Text = empresa;
                    cmbLEmpresa.PromptText = empresa;
                    if (Limagem != "" && Limagem != null)
                    {
                        try { pbLImagem.Image = Image.FromFile(Limagem); }
                        catch (Exception) { }
                    }

                    if (visualisar == "S") metroCheckBox1.Checked = true;
                    if (agente == "S") metroCheckBox2.Checked = true;
                    if (emitir == "S") metroCheckBox3.Checked =true;
                    if (abertura == "S") metroCheckBox4.Checked = true;
                    if (visita == "S") metroCheckBox5.Checked = true;
                }
            }
            else if (TabTabelas.SelectedTab == metroTabPage2)
            {
                string loja = "", area = "";
                FormTabelaMercadoriaConseltar form = new FormTabelaMercadoriaConseltar();
                if (form.ShowDialog() == DialogResult.OK)
                {
                    SqlCommand cmd2 = new SqlCommand("SELECT NomeLoja, NomeAO FROM ASLOJA, AIAREAORGANICA where CodLoja = '" + Mloja + "' AND CodAO = '" + MareaO + "'", conexao);
                    conexao.Open();
                    using (SqlDataReader reader = cmd2.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            loja = reader["NomeLoja"].ToString();
                            area = reader["NomeAO"].ToString();

                        }
                    }
                    conexao.Close();

                    txtMCodigo.Text = Mcodigo;
                    txtMDesignacao.Text = Mdesignacao;
                    cmbMDesignacaoLoja.Text = loja;
                    lbLoja.Text = Mloja;
                    cmbMDesignacaoLoja.PromptText = loja;
                    txtMContaVendas.Text = McontaV;
                    txtMContaCustos.Text = McontaC;
                    cmbMAreaOrganica.Text = area;
                    cmbMAreaOrganica.PromptText = area;
                    lbArea.Text = MareaO;
                    if (MmercadoriaV == "S") cbMMercadoriaVenda.Checked = true;
                    if (MmercadoriaR == "S") cbMMercadoriaRemoto.Checked = true;
                    if (Mativo == "S") cbMActivo.Checked = true;
                    if (Memitir == "S") cbMEmitirRI.Checked = true;
                }
            }
            else if (TabTabelas.SelectedTab == metroTabPage3)
            {
                FormTabelaFamiliaConsultar form = new FormTabelaFamiliaConsultar();
                if (form.ShowDialog() == DialogResult.OK)
                {
                    txtFCodigo.Text = Fcodigo;
                    txtFDesignacao.Text = Fnome;
                    txtFDesconto.Text = Fdesconto;
                    txtFPrazo.Text = ""+Fpraso;
                    if (Fimagem != "" && Fimagem != null)
                    {
                        try { 
                            pbFImagem.Image = Image.FromFile(Fimagem); }
                        catch (Exception ex) { MessageBox.Show(ex.Message);}
                    }

                    if(FcontabilizacaoVenda == "S") cbFContabilizacaoVenda.Checked = true;
                    if(FcontabilizacaoStock == "S") cbFContabilizacaoStock.Checked = true;
                    if(FcontabilizacaoCusto == "S") cbFContabilizacaoCusto.Checked = true;
                }
            }
            else if (TabTabelas.SelectedTab == metroTabPage4)
            {
                FormTabelasSubFamiliaConsultar form = new FormTabelasSubFamiliaConsultar();
                if (form.ShowDialog() == DialogResult.OK)
                {
                    cmbSFamilia2.PromptText = SnomeFamilia;
                    cmbSFamilia2.Text = SnomeFamilia;
                    lbSFamilia.Text = Sfamilia;
                    txtSCodigo.Text = Scodigo;
                    txtSDesignacao.Text = designacao;
                }
            }
            else if (TabTabelas.SelectedTab == metroTabPage5)
            {
                FormTabelaArtigoConsulta form = new FormTabelaArtigoConsulta();
                if (form.ShowDialog() == DialogResult.OK)
                {
                    cmbALoja1.Text = ALoja;
                    cmbALoja1.PromptText = ALoja;
                    cmbAMercadoria1.Text = Amercadoria;
                    cmbAMercadoria1.PromptText = Amercadoria;
                    txtAReferencia.Text = Areferc;
                    cmbAFamilia1.Text = Afamilia;
                    cmbAFamilia1.PromptText = Afamilia;
                    cmbASubFamilia1.Text = AsubFamilia;
                    cmbASubFamilia1.PromptText = AsubFamilia;
                    txtADesignacao.Text = Adesig;
                    txtADesignacaoII.Text = Adesig2;
                    cmbAGrupoWeb1.Text = Agrupo;
                    cmbAGrupoWeb1.PromptText = Agrupo;
                    cmbAUnidade.Text = Aunid;
                    cmbADesigEmbalagem.Text = AdesgE;
                    txtAStockReal.Text = Aqtd;
                    txtALocal.Text = Alocal;
                    txtANorme.Text = Anorma;
                    txtADesignacapSuplementar.Text = AdesiSupl;
                    txtAStockMimimo.Text = AstockMin;
                    txtAFOB.Text = Afob;
                    txtAPCP.Text = Apca;
                    txtAPCM.Text = Apmc;
                    txtAPVP.Text = Apvp;

                    txtAStockMaximo.Text = AstockMax;
                    txtALucro.Text = Alucro;
                    metroTextBox16.Text = Autilizador;
                    metroTextBox17.Text = AdataC;
                    metroTextBox18.Text = AdataA;
                    metroTextBox19.Text = AalteradoP;
                    metroTextBox23.Text = Acambio;
                    txtADataFabrico.Text = AdataF;
                    txtADataExpiracao.Text = AdataEx;
                    txtAPontoEncomenda.Text = ApontoEncomenda;
                    txtAPercetagemQeuebra.Text = Apercquebra;
                    txtADesconto.Text = Adesconto;
                    
                    if (Aimagem != "" && Aimagem != null)
                        pictureBox1.Image = Image.FromFile(Aimagem);

                    if (Aativo == "S") cbAActivo.Checked = true;
                    if (AcontStock == "S") cbAContabilizarStock.Checked = true;
                    if (AcontStock == "S") cbAContabilizarStock.Checked = true;
                    if (Abooking == "S") cbABookingHotel.Checked = true;
                    if (Amostrar == "S") cbAMostraImagem.Checked = true;
                    if (Apermitir == "S") cbAPermiteAlterar.Checked = true;
                    if (Aregisto == "S") cbARegistoEquipamento.Checked = true;
                    if (Apromocao == "S") cbAPromocao.Checked = true;
                    if (Anegativo == "S") metroCheckBox17.Checked = true;
                    if (Aimposto == "5") rbAImposto5.Checked = true;
                    else if (Aimposto == "10") rbAImposto10.Checked = true;
                    else rbAInsento.Checked = true;
                }
            }
            else if (TabTabelas.SelectedTab == metroTabPage6)
            {
                FormTabelaConsultaOrigemArtigos form = new FormTabelaConsultaOrigemArtigos();
                if (form.ShowDialog() == DialogResult.OK)
                {
                    txtOCodigo.Text = Ocodigo;
                    txtODesignacao.Text = Odesignacao;
                }
            }
            else if (TabTabelas.SelectedTab == metroTabPage8)
            {

            }
        }
        private void cmbLEmpresa_MouseClick(object sender, MouseEventArgs e)
        {
            cmbLEmpresa.Items.Clear();
            CmbLEmpresa();
        }
        private void cmbLEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("SELECT CodEmpresa FROM EMPRESA where NOME = @nome", conexao);
            cmd2.Parameters.Add(new SqlParameter("@nome", cmbLEmpresa.Text));
            conexao.Open();
            empresa = "" + cmd2.ExecuteScalar();
            conexao.Close();
            cbCodEmpresa.Text = empresa;
        }

        private void pbLImagem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //Exibir a imagem no formulário
                string nomeArquivo = openFileDialog1.FileName;
                bmp = new Bitmap(nomeArquivo);
                pbLImagem.Image = bmp;

                //salva a imagem no banco de dados
                ms = new MemoryStream();
                bmp.Save(ms, ImageFormat.Bmp);
                foto = ms.ToArray();
                foto1 = 1;
            }
        }

        private void txtLContaNacional_Click(object sender, EventArgs e)
        {
            Lconta = "N";
        }

        private void txtLContaOutros_Click(object sender, EventArgs e)
        {
            Lconta = "O";
        }

        private void txtLContaPa_Click(object sender, EventArgs e)
        {
            Lconta = "P";
        }

        private void cmbMAreaOrganica_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("SELECT CodAO FROM AIAREAORGANICA where NomeAO = @nome", conexao);
            cmd2.Parameters.Add(new SqlParameter("@nome", cmbMAreaOrganica.Text));
            conexao.Open();
            MareaO = "" + cmd2.ExecuteScalar();
            conexao.Close();
            lbArea.Text = MareaO;
        }

        private void cmbMDesignacaoLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("SELECT CodLoja FROM ASLOJA where NomeLoja = @nome", conexao);
            cmd2.Parameters.Add(new SqlParameter("@nome", cmbMDesignacaoLoja.Text));
            conexao.Open();
            Mloja = "" + cmd2.ExecuteScalar();
            conexao.Close();
            lbLoja.Text = Mloja;
        }

        private void cmbMDesignacaoLoja_MouseClick(object sender, MouseEventArgs e)
        {
            cmbMDesignacaoLoja.Items.Clear();
            CmbMDesignacaoLoja();
        }

        private void cmbMAreaOrganica_MouseClick(object sender, MouseEventArgs e)
        {
            cmbMAreaOrganica.Items.Clear();
            CmbMAreaOrganica();
        }

        private void txtMContaVendas_Click(object sender, EventArgs e)
        {
            Lconta = "CV";
        }

        private void txtMContaCustos_Click(object sender, EventArgs e)
        {
            Lconta = "CC";
        }
        private void btSair_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void pbFImagem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //Exibir a imagem no formulário
                string nomeArquivo = openFileDialog1.FileName;
                bmp = new Bitmap(nomeArquivo);
                pbFImagem.Image = bmp;

                //salva a imagem no banco de dados
                ms = new MemoryStream();
                bmp.Save(ms, ImageFormat.Bmp);
                foto = ms.ToArray();
                foto1 = 1;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //Exibir a imagem no formulário
                string nomeArquivo = openFileDialog1.FileName;
                Bitmap bmp = new Bitmap(nomeArquivo);
                pictureBox1.Image = bmp;

                //salva a imagem no banco de dados
                MemoryStream ms = new MemoryStream();
                bmp.Save(ms, ImageFormat.Bmp);
                foto = ms.ToArray();
                foto1 = 1;
            }
        }
        private void cmbSFamilia_MouseClick(object sender, MouseEventArgs e)
        {
            CmbSFamilia();
        }
        private void cbARegistoEquipamento_CheckedChanged(object sender, EventArgs e)
        {
            FormTabelaArtigoMarcaModelo form = new FormTabelaArtigoMarcaModelo();
            if (form.ShowDialog() == DialogResult.OK)
                cbARegistoEquipamento.Checked = true;
            else
                cbARegistoEquipamento.Checked = false;
        }
        private void btAtualizar_Click(object sender, EventArgs e)
        {
            FormTabelasLojaConta form = new FormTabelasLojaConta();
            if (form.ShowDialog() == DialogResult.OK)
            {
                if (Lconta == "N")
                    txtLContaNacional.Text = Lconta1;
                else if (Lconta == "O")
                    txtLContaOutros.Text = Lconta1;
                else if (Lconta == "P")
                    txtLContaPa.Text = Lconta1;
            }
        }
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            FormTabelasLojaConta form = new FormTabelasLojaConta();
            if (form.ShowDialog() == DialogResult.OK)
            {
                if (Lconta == "CV")
                    txtMContaVendas.Text = Lconta1;
                else if (Lconta == "CC")
                    txtMContaCustos.Text = Lconta1;
                conexao.Close();
            }
        }

        private void cmbSFamilia2_SelectedIndexChanged(object sender, EventArgs e)
        {
            conexao.Close(); 
            SqlCommand cmd2 = new SqlCommand("select CodFam from ASFamilia where NomeMerc=@loja ", conexao);
            cmd2.Parameters.Add(new SqlParameter("@loja", cmbSFamilia2.Text));
            conexao.Open();
            try { Sfamilia = (string)cmd2.ExecuteScalar(); }
            catch (Exception) { }
            finally { conexao.Close(); }
            lbSFamilia.Text = Sfamilia;
        }
        private void cmbSFamilia2_DropDown(object sender, EventArgs e)
        {
            cmbSFamilia2.Items.Clear();
            CmbSFamilia1();
        }

        private void txtADataFabrico_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtADataFabrico.Text); }
            catch (Exception ex) { MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); txtADataFabrico.Text = ""; }
        }

        private void txtADataExpiracao_Leave(object sender, EventArgs e)
        {
            try { Convert.ToDateTime(txtADataExpiracao.Text); }
            catch (Exception ex) 
            { 
                MessageBox.Show("A cadeia de caracteres não foi reconhecida como uma Data Válida!"); 
                txtADataExpiracao.Text = ""; 
            }
        }

        private void metroTabPage1_Leave(object sender, EventArgs e)
        {
            limparLoja();
        }

        private void metroTabPage2_Leave(object sender, EventArgs e)
        {
            limparMercadoria();
        }

        private void metroTabPage3_Leave(object sender, EventArgs e)
        {
            limparFamilia();
        }

        private void metroTabPage4_Leave(object sender, EventArgs e)
        {
            limparSubFamilia();
        }

        private void metroTabPage5_Leave(object sender, EventArgs e)
        {
            limparArtigos();
        }

        private void metroTabPage6_Leave(object sender, EventArgs e)
        {
            limparOrigemArtigos();
        }

        private void cmbALoja1_DropDown(object sender, EventArgs e)
        {
            cmbALoja1.Items.Clear();
            CmbALoja();
        }

        private void cmbALoja1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("seleCt CodLoja from ASLOJA WHERE NomeLoja= '" + cmbALoja1.Text + "'", conexao);
            conexao.Open();
            try { AcodLoja = (string)cmd2.ExecuteScalar(); lbALoj.Text = AcodLoja; }
            catch (Exception) { }
            finally { conexao.Close(); cmbAMercadoria1.Items.Clear(); lbAmerc.Text = ""; }
            
        }

        private void cmbAMercadoria1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("seleCt CodArmz from ASARMAZ where NomeArz= '" + cmbAMercadoria1.Text + "'", conexao);
            conexao.Open();
            try { AcodMerc = (string)cmd2.ExecuteScalar(); }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
            lbAmerc.Text = AcodMerc;
        }

        private void cmbAMercadoria1_DropDown(object sender, EventArgs e)
        {
            cmbAMercadoria1.Items.Clear();
            CmbAMercadoria();
        }

        private void cmbAFamilia1_DropDown(object sender, EventArgs e)
        {
            cmbAFamilia1.Items.Clear();
            CmbAFamilia();
        }

        private void cmbAFamilia1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("select CodFam from ASFamilia where NomeMerc= '" + cmbAFamilia1.Text + "'", conexao);
            conexao.Open();
            try { AcodFam = (string)cmd2.ExecuteScalar(); lbAFam.Text = AcodFam; }
            catch (Exception) { }
            finally { conexao.Close(); cmbASubFamilia1.Items.Clear(); lbASubFam.Text = ""; }
        }

        private void cmbASubFamilia1_DropDown(object sender, EventArgs e)
        {
            cmbASubFamilia1.Items.Clear();
            CmbASubFamilia();
        }

        private void cmbASubFamilia1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("seleCt CodSubFam from ASSUBFAMILIA where Descricao= '" + cmbASubFamilia1.Text + "'", conexao);
            conexao.Open();
            try { AcodSubFam = (string)cmd2.ExecuteScalar(); }
            catch (Exception) { }
            finally { conexao.Close(); }
            lbASubFam.Text = AcodSubFam;
        }

        private void cmbAGrupoWeb1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd2 = new SqlCommand("select id from GRUPOANUNCIO with (NOLOCK) where nome = '" + cmbAGrupoWeb1.Text + "'", conexao);
            conexao.Open();
            try { AcodAnucio = "" + cmd2.ExecuteScalar(); lbAGrupo.Text = AcodAnucio; }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }

        private void cmbAGrupoWeb1_DropDown(object sender, EventArgs e)
        {
            cmbAGrupoWeb1.Items.Clear();
            CmbAGrupoWeb();
        }
    }
}
