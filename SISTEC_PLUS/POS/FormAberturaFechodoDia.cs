﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS;

namespace SISTEC_PLUS.OPS
{
    public partial class FormAberturaFechodoDia : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao); // conexão com o banco
        string dataA, codLoj;// = DateTime.Now.ToShortDateString();
        string codutilizador;
        string dia1, mes1, dat;
        DateTime Data;
        int dia, mes, ano;
        SqlCommand cmd3 = new SqlCommand();
        public FormAberturaFechodoDia()
        {
            InitializeComponent();
        }
        public int fecho()
        {
            int codigo;
            if (data() != "0")
            {
                SqlCommand cmd = new SqlCommand("select RTRIM(LTRIM(Fecho)) from AFLANCAMENTO where data_lancamento=@data", conexao);
                cmd.Parameters.Add(new SqlParameter("@data", data()));

                try
                {
                    conexao.Open();
                    codigo = Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (FormatException) { codigo = 0; } //Se der erro na conversão, código recebe 0
                catch (Exception) { codigo = 0; }
                finally { conexao.Close(); }
                return codigo;
            } else
                return 2;
        }
        public String data()
        {
            SqlCommand cmd2 = new SqlCommand("select RTRIM(LTRIM(MAX(data_lancamento))) from AFLANCAMENTO where CodLoja=@loja ", conexao);
            cmd2.Parameters.Add(new SqlParameter("@loja", Variavel.codLoja));
            string data = "";
            try
            {
                conexao.Open();

                data = (string)cmd2.ExecuteScalar();
            }
            catch (Exception ex) { data = "0"; }
            finally { conexao.Close(); }

            return data;
        }
        private int comparaData()
        {
            DateTime agora = Variavel.dataAtual;
            int dia = agora.Day;
            if (dia < 10)
                dia1 = "0" + dia;
            else dia1 = ""+dia;
            int mes = agora.Month;
            int ano = agora.Year;
            dataA = "" + ano + "-" + mes + "-" + dia1;
            DateTime dt;
            DateTime dt2;
            DateTime.TryParse(data(), out dt);
            DateTime.TryParse(dataA, out dt2);
            if (dt < dt2)
            {
                return 0;
            }
            else
                return 1;
        }
        private void FormAberturaFechodoDia_Load(object sender, EventArgs e)
        {
            cmd3.Connection = conexao;
           
            if (fecho() == 0)
            {
                if (comparaData() == 0)
                {
                    MessageBox.Show("O dia " + data() + " Continua aberto");
                }
                lbAbertura.Text = "Dia Aberto";
                txtAberturoPor.Text = Variavel.Login;
                Data = Convert.ToDateTime(data());
                if(Data.Day < 10)
                    dat = "0"+Data.Day;
                else
                    dat = "" + Data.Day;

                if (Data.Month < 10)
                    dat = dat + " - 0" + Data.Month;
                else
                    dat = dat + " - " + Data.Month;
                dat = dat + " - " + Data.Year;

                txtLancamentoDia.Text = dat;
            }
        }
 
        private void btGravar_Click(object sender, EventArgs e)
        {
            
        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            DateTime agora = DateTime.Now;
            int dia = agora.Day;
            if (dia < 10)
                dia1 = "0" + dia;
            else dia1 = "" + dia;
            int mes = agora.Month;
            if (mes < 10)
                mes1 = "0" + mes;
            else
                mes1 = "" + mes;
            int ano = agora.Year;
            dataA = "" + ano + "-" + mes1 + "-" + dia1;

            string tipo;
            if (cbFecharDia.Checked)
            {
                if (fecho() == 0)
                {
                    tipo = "1";
                    SqlCommand cmd = new SqlCommand("SP_ABRE_FECHA_DIA", conexao);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@p_iTipo", tipo));
                    cmd.Parameters.Add(new SqlParameter("@p_sUserCode", Variavel.codUtilizador));
                    cmd.Parameters.Add(new SqlParameter("@p_sCodLoja", Variavel.codLoja));
                    try
                    {
                        conexao.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex) { }
                    finally
                    {
                        conexao.Close();
                        MessageBox.Show("O dia foi fechado com sucesso");
                        txtAberturoPor.Text = "";
                        txtLancamentoDia.Text = "";
                        lbAbertura.Text = "Dia Fechado";
                    }
                }
                else
                {
                    MessageBox.Show("O dia não foi aberto ou já foi fechado");
                }

                cbFecharDia.Checked = false;
            }
            else
            {
                if (comparaData() == 0)
                {
                    tipo = "0";
                    Cursor.Current = Cursors.WaitCursor;
                    
                    SqlCommand cmd = new SqlCommand("SP_ABRE_FECHA_DIA", conexao);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@p_iTipo", tipo));
                    cmd.Parameters.Add(new SqlParameter("@p_sUserCode", Variavel.codUtilizador));
                    cmd.Parameters.Add(new SqlParameter("@p_sCodLoja", Variavel.codLoja));
                    try
                    {
                        conexao.Open();
                        cmd.CommandTimeout = 900000;
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Dia Aberto com sucesso");
                        txtAberturoPor.Text = Variavel.Login;
                        txtLancamentoDia.Text = "" + dia1 + " - " + mes1 + " - " + ano;
                        lbAbertura.Text = "Dia Aberto";
                    }
                    catch (Exception ex) { }
                    finally
                    {
                        conexao.Close();
                        
                    }
                }
                else
                {
                    if (fecho() == 1)
                    {
                        MessageBox.Show("O Dia já foi fechado", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("O Dia já foi aberto", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
