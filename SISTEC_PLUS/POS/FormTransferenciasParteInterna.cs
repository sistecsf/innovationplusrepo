﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormTranferencias : MetroFramework.Forms.MetroForm
    {
        string nomeDaLoja; //valor passado no construtor do form
        string codigoDaLoja;//valor passado no construtor do form
        private string codigoDoArmazem;
        SqlConnection Conn = new SqlConnection(Variavel.Conn);
        private string nomeDoArmazem;
        
        public FormTranferencias(string paramNomeDaloja)
        {
            InitializeComponent();
            this.nomeDaLoja = paramNomeDaloja;
        }

        /*
         * 
         * METODOS DO NEGOCIO 
         * 
         * 
         *
         */
        
        //FUNCAO - getNomeDasLojas
        public List<string> getNomeDasLojas()
        {
            List<string> nomes = new List<string>();
            SqlCommand cmd = new SqlCommand("Select Distinct RTRIM(LTRIM(NomeLoja)) as nomeDaLoja From AsLOJA WHERE NomeLoja not in( '"+this.nomeDaLoja+"')", Conn);
            Conn.Open();
            SqlDataReader res = cmd.ExecuteReader();
            
            if (res.HasRows)
            {
                while (res.Read())
                {
                    nomes.Add(res["nomeDaLoja"].ToString());
                }
            }
            Conn.Close();
            return nomes; 
        }

        //FUNCAO - getCodigoDaLoja
        public string getCodigoDaLoja(string nomeDaLoja)
        {
            SqlCommand cmd = new SqlCommand("Select Distinct RTRIM(LTRIM(CodLoja)) as codigoDaLoja From AsLOJA WITH (NOLOCK) where nomeLoja = @nomeDaLoja", Conn);
            cmd.Parameters.Add("@nomeDaLoja", nomeDaLoja);
            Conn.Open();
            string codLoja = cmd.ExecuteScalar().ToString();
            Conn.Close();

            return codLoja;
        }

        //FUNCAO getArmazensDestino
        public List<string> getArmazens(bool E_ArmazemDestino = false)
        {
            //Lista para albergar o resultado da consulta
            List<string> nomesDosArmazens = new List<string>();
            string sql;
            sql = "SELECT DISTINCT RTRIM(LTRIM(NomeArz)) as nomeDoArmazem From ASARMAZ With (NoLock) Where CodLoja='"+this.codigoDaLoja+"' And Ativo='S' ";
            

            //Verifica se é para retornar so armazens de venda
            if (cbSoArmazensVenda.Checked == true)
            {
                sql += " And ARMA_VENDA='S' ";
            }

            //Se for armazem destino
            if (E_ArmazemDestino)
            {
                sql += "  And RTRIM(LTRIM(CodArmz)) Not In ( ' comboBoxArmazemDestino')";
            }

            SqlCommand cmd = new SqlCommand(sql, Conn);
            
            //Abre a conexao e executa a consulta
            Conn.Open();
            SqlDataReader res = cmd.ExecuteReader();

            if (res.HasRows)
            {
                while (res.Read())
                {
                    nomesDosArmazens.Add(res["nomeDoArmazem"].ToString());
                }
                Conn.Close();
                return nomesDosArmazens;
            }
            else
            {
                return null;
            }   
        }

        //FUNCAO getReferencias
        public List<string> getReferencias(string codigoDoArmazem)
        {
            //Lista para albergar o resultado da consulta
            List<string> referencias = new List<string>();

            SqlCommand cmd = new SqlCommand("SELECT RTRIM(LTRIM(Referenc)) as referencias FROM ASMESTRE WITH (NOLOCK) WHERE CodLoja='" + this.codigoDaLoja + "' AND  CodArmz = '" + codigoDoArmazem + "' AND QTD > 0 AND CONTASTOCKS='S' AND ACTIVO = 'S' ORDER BY RTRIM(LTRIM(Referenc)) ", Conn);
          
            //Abre a conexao e executa a consulta
            Conn.Open();
            SqlDataReader res = cmd.ExecuteReader();

            if (res.HasRows)
            {
                while (res.Read())
                {
                    referencias.Add(res["referencias"].ToString());
                }
                Conn.Close();
                return referencias;
            }
            else
            {
                return null;
            }
        }

        //FUNCAO getCodigoDoArmazem
        public string getCodigoDoArmazem(string nomeDoArmazem)
        {
            string codigoDoArmazem;
            SqlCommand cmd = new SqlCommand(" SELECT RTRIM(LTRIM(CodArmz)) as codAo FROM ASARMAZ WITH (NOLOCK) WHERE CodLoja=@codigoDaLoja AND NomeArz=@nomeDoArmazem",Conn);
            cmd.Parameters.Add("@codigoDaLoja", this.codigoDaLoja);
            cmd.Parameters.Add("@nomeDoArmazem", nomeDoArmazem);
            Conn.Open();
            string res = cmd.ExecuteScalar().ToString();
            Conn.Close();
            codigoDoArmazem = res;

            return codigoDoArmazem;
        }

        //FUNCAO getCodigoDaAreaOrganica
        public string getCodigoDaAreaOrganica(string codigoDoArmazem, string codigoDaLoja)
        {
            string codigoDaAreaOrganica;
            SqlCommand cmd = new SqlCommand(" SELECT LTRIM(RTRIM(CodAO)) as codAo FROM ASARMAZ WITH (NOLOCK) WHERE CODARMZ =@codigoDoArmazem AND CODLOJA=@codigoDaLoja", Conn);
            Conn.Open();
            string res = cmd.ExecuteScalar().ToString();
            Conn.Close();

            codigoDaAreaOrganica = res;

            return codigoDaAreaOrganica;
        }

        //FUNCAO getDescricaoDoArtigo
        public string getDescricaoDoArtigo(string codigoDoArmazem)
        {
            string descricaoDoArtigo;
            SqlCommand cmd = new SqlCommand(" SELECT RTRIM(LTRIM(DESCRARTIGO)) as descricaoDoArtigo From ASMESTRE WITH (NOLOCK) Where CodLoja=@codigoDaloja And CodArmz=@codigoDoArmazem And QTD > 0 And CONTASTOCKS='S' And ACTIVO = 'S' Order By RTRIM(LTRIM(Referenc))", Conn);
            cmd.Parameters.Add("@codigoDaLoja", this.codigoDaLoja);
            cmd.Parameters.Add("@codigoDoArmazem", codigoDoArmazem);

            Conn.Open();
            string res = cmd.ExecuteScalar().ToString();
            Conn.Close();

            descricaoDoArtigo = res;

            return descricaoDoArtigo;
        }

        private void FormInventarioFisico_Load(object sender, EventArgs e)
        {

        }

        private void toolStripContainer1_TopToolStripPanel_Click(object sender, EventArgs e)
        {

        }

        private void metroTabPage1_Click(object sender, EventArgs e)
        {
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void FormTranferencias_Loaf(object sender, EventArgs e)
        {
            //precisa da data actual na tela das transferencias
            labelDataActualTab1.Text = DateTime.Now.ToShortDateString();
            labelDataActualTab2.Text = labelDataActualTab1.Text;
            labelDataActualTab3.Text = labelDataActualTab1.Text;
            
            //carrega usuario logado para mostrar na tela
            labelOperadorTab1.Text = Variavel.codUtilizador;
            labelOperadorTab2.Text = labelOperadorTab1.Text;
            labelOperadorTab3.Text = labelOperadorTab1.Text;
  
            //carrega o nome da loja nas labels
            labelLojaCorrente.Text = this.nomeDaLoja;
            labelNomeLojaTab1.Text = labelLojaCorrente.Text;
            labelNomeLojaTab2.Text = labelNomeLojaTab1.Text;
            labelNomeLojaTab3.Text = labelNomeLojaTab1.Text;

            //carrega o codigo da loja
            try
            {
                this.codigoDaLoja = getCodigoDaLoja(this.nomeDaLoja);
            }
            catch (SqlException erro)
            {
                MessageBox.Show("Erro no carregamento do codigo da loja: "+erro.Message,"Innovation - Erro");
            }
            

            //carrega os armazens na combo nas combos de origem
            try
            {
                List<string> armazens = new List<string>();
                armazens = getArmazens();
                if (armazens != null)
                {
                    foreach (string armazem in armazens)
                    {
                        dataGridViewComboBoxTab1Mercadoria.Items.Add(armazem);
                        dataGridViewComboBoxTab2Mercadoria.Items.Add(armazem);
                    } 
                }
            }
            catch(SqlException erro)
            {
                MessageBox.Show("Erro de consulta " + erro.Message, "Innovation - Erro");
            }
            catch(Exception erro)
            {
                MessageBox.Show("Erro na aplicação "+erro.Message, "Innovation - Erro");
            }
        }

        private void metroGrid2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            MessageBoxButtons botoes = MessageBoxButtons.YesNo;
            DialogResult mensagem = MessageBox.Show("Tem certeza que deseja eliminar linha?", "Innovation - Atenção!", botoes);

            if(mensagem == DialogResult.Yes)
            {
               
            }
            else
            {
                return;
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void labelNomeLojaTab1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void labelNomeLojaTab1_TabIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void labelNomeLojaTab1_TabStopChanged(object sender, EventArgs e)
        {
            
        }

        private void bntTransferir_Click(object sender, EventArgs e)
        {
           
        }

        private void comboBoxArmazensTab2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxLojasTab2_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void comboBoxLojasTab2_DropDown(object sender, EventArgs e)
        {
            try
            {
                foreach (string loja in this.getNomeDasLojas())
                {
                    this.comboBoxLojasTab2.Items.Add(loja);
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show("Erro ao carregar as lojas" + erro.Message + erro.StackTrace, "Innovatio - Erro");
            }
        }

        private void comboBoxMercadoria_dropDown(object sender, EventArgs e)
        {
            for (int i = 0; i <= metroGridTransferenciaTab1.Rows.Count - 1; i++ )
            {
                
            }
            
        }

        private void metroGridTransferenciaTab2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void metroGridTransferenciaTab2_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            ComboBox comboBox = e.Control as ComboBox;

            if (comboBox != null)
            {
                comboBox.DropDown -= new EventHandler(comboBoxMercadoria_dropDown);
                comboBox.DropDown += new EventHandler(comboBoxMercadoria_dropDown);
            }
        }

        private void metroGridTransferenciaTab1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(metroGridTransferenciaTab1.CurrentRow != null)
            {
                if (metroGridTransferenciaTab1.CurrentRow.Cells[0].Value != null)
                {
                    this.nomeDoArmazem = metroGridTransferenciaTab1.CurrentRow.Cells[0].Value.ToString();
                    
                    //Try to fill the comboBox with the references
                    try
                    {
                        this.codigoDoArmazem = getCodigoDoArmazem(this.nomeDoArmazem);

                        //Return all references from the database
                        if (this.getReferencias(this.codigoDoArmazem) != null)
                        {
                            dataGridViewComboBoxTab1Referencia.DataSource = getReferencias(this.codigoDoArmazem);
                        }
                    }
                    catch(Exception erro)
                    {
                        MessageBox.Show("Erro ao carregar referencias do armazem "+this.nomeDoArmazem +" Originou o seguinte erro: "+erro.Message +erro.StackTrace);
                    }
                }
            }
        }
    }
}
