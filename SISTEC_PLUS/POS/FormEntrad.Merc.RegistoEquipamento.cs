﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Odbc;
using System.IO;
using System.Reflection;
using System.Web;
using Excel = Microsoft.Office.Interop.Excel; 




namespace SISTEC_PLUS.POS
{
    public partial class FormEntrad : MetroFramework.Forms.MetroForm
    {


        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        //SqlConnection conexao = new SqlConnection(@"Data Source=sisdbs1b;Initial Catalog=INNOVUIGE;User ID=admin;Password=admin"); // conexão com o banco

        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        // SqlConnection conexao = new SqlConnection(Variavel.Conn);
        int a = 0, count;

        string NumeroSerie, imei, marca,modelo, referencia, telefone,cartaoSim;


        public FormEntrad()
        {
            InitializeComponent();
        }


        // FUNCAO PARA PEGAR O CODIGO DA MARCA
       private string codigoMarca()
        {
           string marca = "";
           SqlCommand cmd = new SqlCommand(@"SELECT CodMarca FROM ASMESTRE WITH (NOLOCK)  WHERE Referenc = '" + FormEntradaMercadoria.REFERENC + "' AND CodArmz='" + FormEntradaMercadoria.CodArmz + "' AND CodLoja='" + Variavel.codLoja + "'", conexao);
           
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
               
                using (SqlDataReader reader = cmd.ExecuteReader())
                { 
                while (reader.Read())
                {
                    marca = reader[0].ToString(); ; //Buscando dados do campo NOME

                    
                }

                }
            
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
            return marca;
        }


       // FUNCAO PARA PEGAR O CODIGO Do MODELO
       private string codigoModelo()
       {
           string modelo ="";
           SqlCommand cmd = new SqlCommand(@"SELECT CodModelo FROM ASMESTRE WITH (NOLOCK)  WHERE Referenc = '" + FormEntradaMercadoria.REFERENC + "' AND CodArmz='" + FormEntradaMercadoria.CodArmz + "' AND CodLoja='" + Variavel.codLoja + "'", conexao);

           cmd.Connection = conexao;
           try
           {
               conexao.Open();
             
               using (SqlDataReader reader = cmd.ExecuteReader())
               {
                   while (reader.Read())
                   {
                       modelo = reader[0].ToString(); ; //Buscando dados do campo NOME


                   }

               }

           }
           catch (SqlException ex)
           {
               MessageBox.Show(ex.Message);
           }
           finally
           {
               conexao.Close();
           }
           return modelo;

       }

       // FUNCAO PARA PEGAR A DESCRIÇAO DA MARCA
       private string descricaoMarca()
       {
           string descMarca = "";
           SqlCommand cmd = new SqlCommand(@"SELECT DescrMarca FROM AEMarca WHERE CodMarca  = '" + codigoMarca() + "'", conexao);

           cmd.Connection = conexao;
           try
           {
               conexao.Open();

               using (SqlDataReader reader = cmd.ExecuteReader())
               {
                   while (reader.Read())
                   {
                       descMarca = reader[0].ToString(); ; //Buscando dados do campo NOME


                   }

               }

           }
           catch (SqlException ex)
           {
               MessageBox.Show(ex.Message);
           }
           finally
           {
               conexao.Close();
           }
           return descMarca;

       }


       // FUNCAO PARA PEGAR A DESCRIÇAO DO MODELO
       private string descricaoModelo()
       {
           string descModelo = "";
           SqlCommand cmd = new SqlCommand(@"SELECT DescrModelo FROM AEMODELO WHERE CodModelo  = '" + codigoModelo() + "'", conexao);

           cmd.Connection = conexao;
           try
           {
               conexao.Open();

               using (SqlDataReader reader = cmd.ExecuteReader())
               {
                   while (reader.Read())
                   {
                       descModelo = reader[0].ToString(); ; //Buscando dados do campo NOME


                   }

               }

           }
           catch (SqlException ex)
           {
               MessageBox.Show(ex.Message);
           }
           finally
           {
               conexao.Close();
           }
           return descModelo;

       }










        // FORMULARIO LOAD DE ENTRADA E MERCADORIA 


        private void FormEntrad_Load(object sender, EventArgs e)
        {
            txtMarca.ReadOnly = true;
            txtQtdRE.ReadOnly = true;
            txtCustoFob.ReadOnly = true;
            txtCodMarca.ReadOnly = true;
            txtCodigoModelo.ReadOnly = true;
            txtDescricao.ReadOnly = true;

            txtMarca.Text = descricaoMarca();
            txtDescricao.Text = descricaoModelo();
            txtQtdRE.Text = FormEntradaMercadoria.Qtd;
            txtCustoFob.Text = FormEntradaMercadoria.FOB1;
            txtCodMarca.Text = codigoMarca();
            txtCodigoModelo.Text =codigoModelo();

         //   MessageBox.Show(codigoMarca());
          //  MessageBox.Show(codigoModelo());

          //  MessageBox.Show(descricaoMarca());
          // MessageBox.Show(descricaoModelo());
           metroGrid1.AutoGenerateColumns = false;
        }

        // FUNCAO PARA PREENCHER O DATAGRID
        private void prenxeGrid()
        {
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(@"select distinct M.CodFam,  M.CodArmz,Referenc,QTD,NomeMerc,NOMEARZ from ASMESTRE M WITH (NOLOCK), ASARMAZ AR WITH (NOLOCK) ,ASFamilia F WITH (NOLOCK) " +
                                                "WHERE Referenc = '" + FormEntradaMercadoria.REFERENC + "'  AND  M.CodLoja='" + Variavel.codLoja + "' AND F.CodFam=M.CODFAM AND m.CodArmz=AR.CodArmz", conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("Armazem");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }

        // MUDAR O TIPO DE TELEFONE OU CAARTAO SIM
        string tipoTel_Sim ="";

      
        private void RadioButtonTelefone_CheckedChanged(object sender, EventArgs e)
        {
            tipoTel_Sim = "T";

        }

        private void RadioButtonCartaoSim_CheckedChanged(object sender, EventArgs e)
        {
            tipoTel_Sim = "C";
        }


        // FUNCAO PARA PREENCHER OS DADOS DENTRO DO DATA GRID
        private void EntradaDataGrid()
        {

            metroGrid1.Rows.Add(txtNumSer.Text, txtImei.Text, txtCodMarca.Text, txtCodigoModelo.Text, FormEntradaMercadoria.REFERENC, tipoTel_Sim);
            a++;

           // NumeroSerie, imei, marca,modelo, referencia, telefone,cartaoSim
            //txtTotalItens = 0;
            

            // for para somar  as linhas dentro do grid e posteriormente mostrar nos text box em baixo
            for (int i = 0; i < a; i++)
            {
             //  txtTotalItens = txtTotalItens + Convert.ToDouble(metroGrid1[0, i].Value);
              
            }

            txtTotalItens.Text = "" + txtTotalItens;
         
        }


        // BOTAO IMPORTAR IMEI ATRAVES DA TABELA EXCEL
        private void metroButton1_Click(object sender, EventArgs e)
        {

             OpenFileDialog vAbreArq = new OpenFileDialog();
            vAbreArq.Filter = "Excel Files |*.xls";
            vAbreArq.Title = "Selecione o Arquivo";
            if (vAbreArq.ShowDialog() == DialogResult.OK)
            {
                DataSet ds = new DataSet();




                OleDbConnection conexao = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" +
                       "Data Source=" + vAbreArq.FileName + ";" +
                         "Extended Properties='Excel 12.0 Xml;HDR=NO;IMEX=1;ImportMixedTypes=Text;TypeGuessRows=0';");
                OleDbDataAdapter da = new OleDbDataAdapter("Select F1 , F2 AS DOC  from [Folha1$A1:B" + txtQtdRE.Text + "]", conexao);


                metroGrid1.DataSource = ds.DefaultViewManager;

                da.Fill(ds);
                metroGrid1.DataSource = ds.Tables[0];


                metroGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;

                conexao.Close();
        
            }


            for (int i = 0; i < Convert.ToInt32(txtQtdRE.Text); i++)
            {

                this.metroGrid1[2, i].Value = txtCodMarca.Text;
                this.metroGrid1[3, i].Value = txtCodigoModelo.Text; ;
                this.metroGrid1[4, i].Value = FormEntradaMercadoria.REFERENC;
                this.metroGrid1[5, i].Value = tipoTel_Sim;
            }


        }

        // BOTÃO IMPORTAR NUMERO DE SERIE ATRAVES DA TABELA EXCEL
        private void metroButton2_Click(object sender, EventArgs e)
        {
            OpenFileDialog vAbreArq = new OpenFileDialog();
            vAbreArq.Filter = "Excel Files |*.xlsx";
            vAbreArq.Title = "Selecione o Arquivo";
            if (vAbreArq.ShowDialog() == DialogResult.OK)
            {
                DataSet ds = new DataSet();




                OleDbConnection conexao = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" +
                       "Data Source=" + vAbreArq.FileName + ";" +
                          "Extended Properties='Excel 12.0 Xml;HDR=YES;IMEX=1;ImportMixedTypes=Text;TypeGuessRows=0';");
                OleDbDataAdapter da = new OleDbDataAdapter("Select F1 from [Folha1$A1:A" + txtQtdRE.Text + "]", conexao);



                metroGrid1.DataSource = ds.DefaultViewManager;
                da.Fill(ds);
                metroGrid1.DataSource = ds.Tables[0];


                metroGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
                conexao.Close();
            }

        }

        // BOTAO ADICIONAR
        private void metroButton3_Click(object sender, EventArgs e)
        {
            EntradaDataGrid();
        }


        // BOTAO OK PARA ENVIAR OS DADOS NA GRID DO FORMULARI PRINCIPAL
        private void btOk_Click(object sender, EventArgs e)
        {

            FormEntradaMercadoria.NSERIE = new string[metroGrid1.RowCount - 1];
            FormEntradaMercadoria.IMEI = new string[metroGrid1.RowCount - 1];
            FormEntradaMercadoria.TEL_SIM = new string[metroGrid1.RowCount - 1];

            for (int a = 0; a < metroGrid1.RowCount - 1; a++)
            {
                FormEntradaMercadoria.NSERIE[a] = "" + this.metroGrid1[0, a].Value;
                FormEntradaMercadoria.IMEI[a] = ""+this.metroGrid1[1, a].Value;
                FormEntradaMercadoria.TEL_SIM[a] = this.metroGrid1[5, a].Value.ToString();
            }

            FormEntradaMercadoria.MARCA = codigoMarca();
            FormEntradaMercadoria.MODELO = codigoModelo();

            FormEntradaMercadoria.countNS = metroGrid1.RowCount - 1;
            this.DialogResult = DialogResult.OK;

        }

        // BOTAO PARA IMPORTAR OS DADOS DO EXCEL
        private void importar_Click(object sender, EventArgs e)
        {

            OpenFileDialog vAbreArq = new OpenFileDialog();
            vAbreArq.Filter = "Excel Files |*.xls";
            vAbreArq.Title = "Selecione o Arquivo";
            if (vAbreArq.ShowDialog() == DialogResult.OK)
            {
                DataSet ds = new DataSet();




                OleDbConnection conexao = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;" +
                       "Data Source=" + vAbreArq.FileName + ";" +
                         "Extended Properties='Excel 12.0 Xml;HDR=NO;IMEX=1;ImportMixedTypes=Text;TypeGuessRows=0';");
                OleDbDataAdapter da = new OleDbDataAdapter("Select F1  from [Folha1$A1:A" + txtQtdRE.Text + "]", conexao);


                metroGrid1.DataSource = ds.DefaultViewManager;

                da.Fill(ds);
                metroGrid1.DataSource = ds.Tables[0];


                metroGrid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;

                conexao.Close();
            }

            for (int i = 0; i < Convert.ToInt32(txtQtdRE.Text); i++)
            {

                this.metroGrid1[2, i].Value = txtCodMarca.Text;
                this.metroGrid1[3, i].Value = txtCodigoModelo.Text; ;
                this.metroGrid1[4, i].Value = FormEntradaMercadoria.REFERENC;
                this.metroGrid1[5, i].Value = tipoTel_Sim;
            }




        }

        private void txtImei_Click(object sender, EventArgs e)
        {

        }

      

        



    }
}
