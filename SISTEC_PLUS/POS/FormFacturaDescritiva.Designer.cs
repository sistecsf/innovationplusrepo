﻿namespace SISTEC_PLUS.POS
{
    partial class FormFacturaDescritiva
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFacturaDescritiva));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btNovo = new System.Windows.Forms.ToolStripButton();
            this.btGravar = new System.Windows.Forms.ToolStripButton();
            this.btApagar = new System.Windows.Forms.ToolStripButton();
            this.btConsultar = new System.Windows.Forms.ToolStripButton();
            this.btSair = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.txtCliente = new MetroFramework.Controls.MetroTextBox();
            this.txtValor = new MetroFramework.Controls.MetroTextBox();
            this.txtDesconto = new MetroFramework.Controls.MetroTextBox();
            this.txtFacturaN = new MetroFramework.Controls.MetroTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.rbInsentoImposto = new MetroFramework.Controls.MetroRadioButton();
            this.cbImpostoConsumo = new MetroFramework.Controls.MetroRadioButton();
            this.rbImpostoConsumo2 = new MetroFramework.Controls.MetroRadioButton();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDescreveDescricao = new System.Windows.Forms.RichTextBox();
            this.lbCambio = new MetroFramework.Controls.MetroLabel();
            this.lbDescricao = new MetroFramework.Controls.MetroLabel();
            this.lbDesconto = new MetroFramework.Controls.MetroLabel();
            this.lbVendedor = new MetroFramework.Controls.MetroLabel();
            this.cmbNome = new MetroFramework.Controls.MetroComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbArea1 = new MetroFramework.Controls.MetroComboBox();
            this.cmbVendador = new System.Windows.Forms.GroupBox();
            this.cmbVendedor1 = new MetroFramework.Controls.MetroComboBox();
            this.cmbMoeda = new MetroFramework.Controls.MetroComboBox();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.cmbVendador.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btNovo,
            this.btGravar,
            this.btApagar,
            this.btConsultar,
            this.btSair});
            this.toolStrip1.Location = new System.Drawing.Point(233, 60);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(340, 25);
            this.toolStrip1.TabIndex = 85;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btNovo
            // 
            this.btNovo.Image = ((System.Drawing.Image)(resources.GetObject("btNovo.Image")));
            this.btNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(56, 22);
            this.btNovo.Text = "Novo";
            this.btNovo.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // btGravar
            // 
            this.btGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btGravar.Image = ((System.Drawing.Image)(resources.GetObject("btGravar.Image")));
            this.btGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(61, 22);
            this.btGravar.Text = "Gravar";
            this.btGravar.Click += new System.EventHandler(this.btGravar_Click_1);
            // 
            // btApagar
            // 
            this.btApagar.Image = ((System.Drawing.Image)(resources.GetObject("btApagar.Image")));
            this.btApagar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btApagar.Name = "btApagar";
            this.btApagar.Size = new System.Drawing.Size(65, 22);
            this.btApagar.Text = "Apagar";
            this.btApagar.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // btConsultar
            // 
            this.btConsultar.Image = ((System.Drawing.Image)(resources.GetObject("btConsultar.Image")));
            this.btConsultar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btConsultar.Name = "btConsultar";
            this.btConsultar.Size = new System.Drawing.Size(78, 22);
            this.btConsultar.Text = "Consultar";
            this.btConsultar.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // btSair
            // 
            this.btSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btSair.Image = ((System.Drawing.Image)(resources.GetObject("btSair.Image")));
            this.btSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(46, 22);
            this.btSair.Text = "Sair";
            this.btSair.Click += new System.EventHandler(this.btSair_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(276, 2);
            this.toolStripContainer1.Location = new System.Drawing.Point(233, 60);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(276, 27);
            this.toolStripContainer1.TabIndex = 86;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer1.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer1.TopToolStripPanel.Tag = "XC";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(28, 127);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(49, 19);
            this.metroLabel1.TabIndex = 87;
            this.metroLabel1.Text = "Cliente";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(29, 197);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(37, 19);
            this.metroLabel2.TabIndex = 88;
            this.metroLabel2.Text = "Área";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(27, 226);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(65, 19);
            this.metroLabel3.TabIndex = 89;
            this.metroLabel3.Text = "Descrição";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(214, 226);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(72, 19);
            this.metroLabel4.TabIndex = 90;
            this.metroLabel4.Text = "Factura Nº";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(408, 127);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(67, 19);
            this.metroLabel5.TabIndex = 91;
            this.metroLabel5.Text = "Vendador";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(408, 162);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(51, 19);
            this.metroLabel6.TabIndex = 92;
            this.metroLabel6.Text = "Moeda";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(408, 197);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(40, 19);
            this.metroLabel7.TabIndex = 93;
            this.metroLabel7.Text = "Valor";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(587, 162);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(56, 19);
            this.metroLabel8.TabIndex = 94;
            this.metroLabel8.Text = "Câmbio";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(582, 197);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(63, 19);
            this.metroLabel9.TabIndex = 95;
            this.metroLabel9.Text = "Desconto";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(412, 226);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(63, 19);
            this.metroLabel10.TabIndex = 96;
            this.metroLabel10.Text = "Desconto";
            // 
            // txtCliente
            // 
            this.txtCliente.Lines = new string[0];
            this.txtCliente.Location = new System.Drawing.Point(110, 123);
            this.txtCliente.MaxLength = 32767;
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.PasswordChar = '\0';
            this.txtCliente.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCliente.SelectedText = "";
            this.txtCliente.Size = new System.Drawing.Size(75, 23);
            this.txtCliente.TabIndex = 97;
            this.txtCliente.UseSelectable = true;
            this.txtCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCliente_KeyPress);
            // 
            // txtValor
            // 
            this.txtValor.Lines = new string[0];
            this.txtValor.Location = new System.Drawing.Point(476, 193);
            this.txtValor.MaxLength = 32767;
            this.txtValor.Name = "txtValor";
            this.txtValor.PasswordChar = '\0';
            this.txtValor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtValor.SelectedText = "";
            this.txtValor.Size = new System.Drawing.Size(97, 23);
            this.txtValor.TabIndex = 102;
            this.txtValor.UseSelectable = true;
            // 
            // txtDesconto
            // 
            this.txtDesconto.Lines = new string[0];
            this.txtDesconto.Location = new System.Drawing.Point(648, 193);
            this.txtDesconto.MaxLength = 32767;
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.PasswordChar = '\0';
            this.txtDesconto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDesconto.SelectedText = "";
            this.txtDesconto.Size = new System.Drawing.Size(82, 23);
            this.txtDesconto.TabIndex = 105;
            this.txtDesconto.UseSelectable = true;
            // 
            // txtFacturaN
            // 
            this.txtFacturaN.Lines = new string[0];
            this.txtFacturaN.Location = new System.Drawing.Point(307, 222);
            this.txtFacturaN.MaxLength = 32767;
            this.txtFacturaN.Name = "txtFacturaN";
            this.txtFacturaN.PasswordChar = '\0';
            this.txtFacturaN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtFacturaN.SelectedText = "";
            this.txtFacturaN.Size = new System.Drawing.Size(75, 23);
            this.txtFacturaN.TabIndex = 107;
            this.txtFacturaN.UseSelectable = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(93, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 13);
            this.label1.TabIndex = 109;
            this.label1.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(93, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 13);
            this.label2.TabIndex = 110;
            this.label2.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(93, 187);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 13);
            this.label3.TabIndex = 111;
            this.label3.Text = "*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(459, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 13);
            this.label4.TabIndex = 112;
            this.label4.Text = "*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(459, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 13);
            this.label5.TabIndex = 113;
            this.label5.Text = "*";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(730, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(15, 13);
            this.label6.TabIndex = 114;
            this.label6.Text = "%";
            // 
            // rbInsentoImposto
            // 
            this.rbInsentoImposto.AutoSize = true;
            this.rbInsentoImposto.Location = new System.Drawing.Point(6, 19);
            this.rbInsentoImposto.Name = "rbInsentoImposto";
            this.rbInsentoImposto.Size = new System.Drawing.Size(173, 15);
            this.rbInsentoImposto.TabIndex = 115;
            this.rbInsentoImposto.Text = "Isento Imposto de Consumo";
            this.rbInsentoImposto.UseSelectable = true;
            // 
            // cbImpostoConsumo
            // 
            this.cbImpostoConsumo.AutoSize = true;
            this.cbImpostoConsumo.Location = new System.Drawing.Point(295, 19);
            this.cbImpostoConsumo.Name = "cbImpostoConsumo";
            this.cbImpostoConsumo.Size = new System.Drawing.Size(157, 15);
            this.cbImpostoConsumo.TabIndex = 116;
            this.cbImpostoConsumo.Text = "Imposto de Consumo 5%";
            this.cbImpostoConsumo.UseSelectable = true;
            // 
            // rbImpostoConsumo2
            // 
            this.rbImpostoConsumo2.AutoSize = true;
            this.rbImpostoConsumo2.Location = new System.Drawing.Point(556, 19);
            this.rbImpostoConsumo2.Name = "rbImpostoConsumo2";
            this.rbImpostoConsumo2.Size = new System.Drawing.Size(163, 15);
            this.rbImpostoConsumo2.TabIndex = 117;
            this.rbImpostoConsumo2.Text = "Imposto de Consumo 10%";
            this.rbImpostoConsumo2.UseSelectable = true;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(20, 338);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(310, 19);
            this.metroLabel11.TabIndex = 118;
            this.metroLabel11.Text = "De uma boa descrição sobre o objectivo da factura";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(371, 344);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 13);
            this.label7.TabIndex = 119;
            this.label7.Text = "*";
            // 
            // txtDescreveDescricao
            // 
            this.txtDescreveDescricao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(32)))), ((int)(((byte)(32)))));
            this.txtDescreveDescricao.Location = new System.Drawing.Point(20, 360);
            this.txtDescreveDescricao.Name = "txtDescreveDescricao";
            this.txtDescreveDescricao.Size = new System.Drawing.Size(725, 200);
            this.txtDescreveDescricao.TabIndex = 120;
            this.txtDescreveDescricao.Text = "";
            // 
            // lbCambio
            // 
            this.lbCambio.AutoSize = true;
            this.lbCambio.ForeColor = System.Drawing.Color.Red;
            this.lbCambio.Location = new System.Drawing.Point(664, 161);
            this.lbCambio.Name = "lbCambio";
            this.lbCambio.Size = new System.Drawing.Size(0, 0);
            this.lbCambio.TabIndex = 122;
            // 
            // lbDescricao
            // 
            this.lbDescricao.AutoSize = true;
            this.lbDescricao.ForeColor = System.Drawing.Color.Red;
            this.lbDescricao.Location = new System.Drawing.Point(110, 226);
            this.lbDescricao.Name = "lbDescricao";
            this.lbDescricao.Size = new System.Drawing.Size(25, 19);
            this.lbDescricao.TabIndex = 123;
            this.lbDescricao.Text = "DF";
            // 
            // lbDesconto
            // 
            this.lbDesconto.AutoSize = true;
            this.lbDesconto.Location = new System.Drawing.Point(476, 226);
            this.lbDesconto.Name = "lbDesconto";
            this.lbDesconto.Size = new System.Drawing.Size(0, 0);
            this.lbDesconto.TabIndex = 124;
            // 
            // lbVendedor
            // 
            this.lbVendedor.AutoSize = true;
            this.lbVendedor.Location = new System.Drawing.Point(727, 123);
            this.lbVendedor.Name = "lbVendedor";
            this.lbVendedor.Size = new System.Drawing.Size(0, 0);
            this.lbVendedor.TabIndex = 128;
            // 
            // cmbNome
            // 
            this.cmbNome.FormattingEnabled = true;
            this.cmbNome.IntegralHeight = false;
            this.cmbNome.ItemHeight = 23;
            this.cmbNome.Location = new System.Drawing.Point(110, 152);
            this.cmbNome.Name = "cmbNome";
            this.cmbNome.Size = new System.Drawing.Size(272, 29);
            this.cmbNome.TabIndex = 159;
            this.cmbNome.UseSelectable = true;
            this.cmbNome.SelectedIndexChanged += new System.EventHandler(this.cmbNome_SelectedIndexChanged_1);
            this.cmbNome.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbNome_MouseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbInsentoImposto);
            this.groupBox1.Controls.Add(this.rbImpostoConsumo2);
            this.groupBox1.Controls.Add(this.cbImpostoConsumo);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(23, 281);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(725, 46);
            this.groupBox1.TabIndex = 160;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Imposto";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbArea1);
            this.groupBox2.Location = new System.Drawing.Point(26, 107);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(363, 149);
            this.groupBox2.TabIndex = 161;
            this.groupBox2.TabStop = false;
            // 
            // cmbArea1
            // 
            this.cmbArea1.FormattingEnabled = true;
            this.cmbArea1.IntegralHeight = false;
            this.cmbArea1.ItemHeight = 23;
            this.cmbArea1.Location = new System.Drawing.Point(84, 80);
            this.cmbArea1.Name = "cmbArea1";
            this.cmbArea1.Size = new System.Drawing.Size(272, 29);
            this.cmbArea1.TabIndex = 163;
            this.cmbArea1.UseSelectable = true;
            this.cmbArea1.SelectedIndexChanged += new System.EventHandler(this.cmbArea1_SelectedIndexChanged_1);
            this.cmbArea1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbArea1_MouseClick_1);
            // 
            // cmbVendador
            // 
            this.cmbVendador.Controls.Add(this.cmbVendedor1);
            this.cmbVendador.Controls.Add(this.cmbMoeda);
            this.cmbVendador.Location = new System.Drawing.Point(395, 107);
            this.cmbVendador.Name = "cmbVendador";
            this.cmbVendador.Size = new System.Drawing.Size(353, 149);
            this.cmbVendador.TabIndex = 162;
            this.cmbVendador.TabStop = false;
            // 
            // cmbVendedor1
            // 
            this.cmbVendedor1.FormattingEnabled = true;
            this.cmbVendedor1.IntegralHeight = false;
            this.cmbVendedor1.ItemHeight = 23;
            this.cmbVendedor1.Location = new System.Drawing.Point(81, 16);
            this.cmbVendedor1.Name = "cmbVendedor1";
            this.cmbVendedor1.Size = new System.Drawing.Size(228, 29);
            this.cmbVendedor1.TabIndex = 163;
            this.cmbVendedor1.UseSelectable = true;
            this.cmbVendedor1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.metroComboBox2_MouseClick);
            // 
            // cmbMoeda
            // 
            this.cmbMoeda.FormattingEnabled = true;
            this.cmbMoeda.IntegralHeight = false;
            this.cmbMoeda.ItemHeight = 23;
            this.cmbMoeda.Items.AddRange(new object[] {
            "KZ",
            "USD"});
            this.cmbMoeda.Location = new System.Drawing.Point(81, 51);
            this.cmbMoeda.Name = "cmbMoeda";
            this.cmbMoeda.PromptText = "KZ";
            this.cmbMoeda.Size = new System.Drawing.Size(97, 29);
            this.cmbMoeda.TabIndex = 163;
            this.cmbMoeda.UseSelectable = true;
            // 
            // FormFacturaDescritiva
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 574);
            this.ControlBox = false;
            this.Controls.Add(this.cmbNome);
            this.Controls.Add(this.lbVendedor);
            this.Controls.Add(this.lbDesconto);
            this.Controls.Add(this.lbDescricao);
            this.Controls.Add(this.lbCambio);
            this.Controls.Add(this.txtDescreveDescricao);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFacturaN);
            this.Controls.Add(this.txtDesconto);
            this.Controls.Add(this.txtValor);
            this.Controls.Add(this.txtCliente);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.cmbVendador);
            this.Name = "FormFacturaDescritiva";
            this.Text = "Factura Descritiva";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormFacturaDescritiva_Load);
            this.Leave += new System.EventHandler(this.FormFacturaDescritiva_Leave);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.cmbVendador.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btNovo;
        private System.Windows.Forms.ToolStripButton btGravar;
        private System.Windows.Forms.ToolStripButton btConsultar;
        private System.Windows.Forms.ToolStripButton btSair;
        private System.Windows.Forms.ToolStripButton btApagar;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroTextBox txtCliente;
        private MetroFramework.Controls.MetroTextBox txtValor;
        private MetroFramework.Controls.MetroTextBox txtDesconto;
        private MetroFramework.Controls.MetroTextBox txtFacturaN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private MetroFramework.Controls.MetroRadioButton rbInsentoImposto;
        private MetroFramework.Controls.MetroRadioButton cbImpostoConsumo;
        private MetroFramework.Controls.MetroRadioButton rbImpostoConsumo2;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox txtDescreveDescricao;
        private MetroFramework.Controls.MetroLabel lbCambio;
        private MetroFramework.Controls.MetroLabel lbDescricao;
        private MetroFramework.Controls.MetroLabel lbDesconto;
        private MetroFramework.Controls.MetroLabel lbVendedor;
        private MetroFramework.Controls.MetroComboBox cmbNome;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox cmbVendador;
        private MetroFramework.Controls.MetroComboBox cmbMoeda;
        private MetroFramework.Controls.MetroComboBox cmbVendedor1;
        private MetroFramework.Controls.MetroComboBox cmbArea1;
    }
}