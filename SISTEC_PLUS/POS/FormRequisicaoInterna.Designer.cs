﻿namespace SISTEC_PLUS.POS
{
    partial class FormRequisicaoInterna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRequisicaoInterna));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btC = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.cmbLoja = new MetroFramework.Controls.MetroComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.btROk = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer2 = new System.Windows.Forms.ToolStripContainer();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.cmbLoja2 = new MetroFramework.Controls.MetroComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.btAOk = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer3 = new System.Windows.Forms.ToolStripContainer();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.b = new System.Windows.Forms.ToolStrip();
            this.btReGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.cmbLoja3 = new MetroFramework.Controls.MetroComboBox();
            this.metroTabPage4 = new MetroFramework.Controls.MetroTabPage();
            this.dgFicha = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn97 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn98 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn99 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgModelo = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn95 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn96 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgMarca = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn93 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn94 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgSubFam = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn88 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn89 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn90 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn91 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn92 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgFam = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn78 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn79 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn80 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn81 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn82 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn83 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn84 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn85 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn86 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn87 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgArma = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn65 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn66 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn67 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn68 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn69 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn70 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn71 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn72 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn73 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn74 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn75 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn76 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn77 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgLoja = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgArt = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgDetail = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgL1 = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.btEExportacao = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer4 = new System.Windows.Forms.ToolStripContainer();
            this.dgEntradaGuia = new MetroFramework.Controls.MetroGrid();
            this.Mercadoria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Referencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Descricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FOB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PCL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmbExIDGI = new MetroFramework.Controls.MetroComboBox();
            this.cmbExEntidadeFilial = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtExDataFinal = new System.Windows.Forms.MaskedTextBox();
            this.txtExDataInicial = new System.Windows.Forms.MaskedTextBox();
            this.lbfuncao = new MetroFramework.Controls.MetroLabel();
            this.lbNumero = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage5 = new MetroFramework.Controls.MetroTabPage();
            this.label2 = new MetroFramework.Controls.MetroLabel();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.dgEncomendas2 = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn111 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn114 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn122 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn124 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn125 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn126 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn127 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn128 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn129 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn130 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn131 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn132 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn133 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn134 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn135 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn136 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn137 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn138 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn139 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn140 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn141 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn142 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn143 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn144 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgEncomendas1 = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewTextBoxColumn100 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn101 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn102 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn103 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn104 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn105 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn106 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn107 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn108 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn109 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn110 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn112 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn113 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn115 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn116 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn117 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn118 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn119 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn120 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn121 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn123 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbItens = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.lbTotalSelecionado = new MetroFramework.Controls.MetroLabel();
            this.lbTotalEncomendar = new MetroFramework.Controls.MetroLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btAtualizar = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer7 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip5 = new System.Windows.Forms.ToolStrip();
            this.btEnCalcular = new System.Windows.Forms.ToolStripButton();
            this.btEnCriar = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer5 = new System.Windows.Forms.ToolStripContainer();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.dgEncomendas = new MetroFramework.Controls.MetroGrid();
            this.Column48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LOJA2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Col6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.F = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PVP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ValorPCL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.STK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEnDiasRepor = new MetroFramework.Controls.MetroTextBox();
            this.txtEnPercentualSaida = new MetroFramework.Controls.MetroTextBox();
            this.txtEnNDias = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lbELoja = new MetroFramework.Controls.MetroLabel();
            this.cmbELoja = new MetroFramework.Controls.MetroComboBox();
            this.lbECusto = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage6 = new MetroFramework.Controls.MetroTabPage();
            this.toolStrip6 = new System.Windows.Forms.ToolStrip();
            this.btCOk = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer6 = new System.Windows.Forms.ToolStripContainer();
            this.metroGrid3 = new MetroFramework.Controls.MetroGrid();
            this.txtCRIN = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel21 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel20 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel18 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel16 = new MetroFramework.Controls.MetroLabel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtCDataFinal = new System.Windows.Forms.MaskedTextBox();
            this.txtCDataInicial = new System.Windows.Forms.MaskedTextBox();
            this.cmbCLoja = new MetroFramework.Controls.MetroComboBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.IDRI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NrRI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DataRI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OperadorRI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EstadoRI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IDSaida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NSaida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Doc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Operador = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btC.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.toolStripContainer2.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.toolStripContainer3.SuspendLayout();
            this.metroTabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.b.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.metroTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFicha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgModelo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgMarca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSubFam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgArma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgLoja)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgArt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgL1)).BeginInit();
            this.toolStrip4.SuspendLayout();
            this.toolStripContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEntradaGuia)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.metroTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEncomendas2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgEncomendas1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer7.SuspendLayout();
            this.toolStrip5.SuspendLayout();
            this.toolStripContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEncomendas)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.metroTabPage6.SuspendLayout();
            this.toolStrip6.SuspendLayout();
            this.toolStripContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid3)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // btC
            // 
            this.btC.Controls.Add(this.metroTabPage1);
            this.btC.Controls.Add(this.metroTabPage2);
            this.btC.Controls.Add(this.metroTabPage3);
            this.btC.Controls.Add(this.metroTabPage4);
            this.btC.Controls.Add(this.metroTabPage5);
            this.btC.Controls.Add(this.metroTabPage6);
            this.btC.Location = new System.Drawing.Point(23, 63);
            this.btC.Multiline = true;
            this.btC.Name = "btC";
            this.btC.SelectedIndex = 5;
            this.btC.Size = new System.Drawing.Size(1083, 518);
            this.btC.TabIndex = 0;
            this.btC.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage1.Controls.Add(this.cmbLoja);
            this.metroTabPage1.Controls.Add(this.groupBox1);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(1075, 476);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Requisição";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // cmbLoja
            // 
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.IntegralHeight = false;
            this.cmbLoja.ItemHeight = 23;
            this.cmbLoja.Location = new System.Drawing.Point(243, 280);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(418, 29);
            this.cmbLoja.TabIndex = 4;
            this.cmbLoja.UseSelectable = true;
            this.cmbLoja.SelectedIndexChanged += new System.EventHandler(this.cmbLoja_SelectedIndexChanged);
            this.cmbLoja.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbLoja_MouseClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.toolStrip2);
            this.groupBox1.Controls.Add(this.toolStripContainer2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(225, 264);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(608, 56);
            this.groupBox1.TabIndex = 93;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Selecione a Loja com os Artigos ";
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btROk});
            this.toolStrip2.Location = new System.Drawing.Point(482, 16);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(45, 25);
            this.toolStrip2.TabIndex = 91;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // btROk
            // 
            this.btROk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btROk.Image = ((System.Drawing.Image)(resources.GetObject("btROk.Image")));
            this.btROk.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btROk.Name = "btROk";
            this.btROk.Size = new System.Drawing.Size(42, 22);
            this.btROk.Text = "Ok";
            this.btROk.Click += new System.EventHandler(this.btROk_Click);
            // 
            // toolStripContainer2
            // 
            // 
            // toolStripContainer2.ContentPanel
            // 
            this.toolStripContainer2.ContentPanel.Size = new System.Drawing.Size(82, 2);
            this.toolStripContainer2.Location = new System.Drawing.Point(482, 16);
            this.toolStripContainer2.Name = "toolStripContainer2";
            this.toolStripContainer2.Size = new System.Drawing.Size(82, 27);
            this.toolStripContainer2.TabIndex = 92;
            this.toolStripContainer2.Text = "toolStripContainer2";
            // 
            // toolStripContainer2.TopToolStripPanel
            // 
            this.toolStripContainer2.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer2.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer2.TopToolStripPanel.Tag = "XC";
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage2.Controls.Add(this.cmbLoja2);
            this.metroTabPage2.Controls.Add(this.groupBox2);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(1075, 476);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Atendimento";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // cmbLoja2
            // 
            this.cmbLoja2.FormattingEnabled = true;
            this.cmbLoja2.IntegralHeight = false;
            this.cmbLoja2.ItemHeight = 23;
            this.cmbLoja2.Location = new System.Drawing.Point(241, 271);
            this.cmbLoja2.Name = "cmbLoja2";
            this.cmbLoja2.Size = new System.Drawing.Size(419, 29);
            this.cmbLoja2.TabIndex = 8;
            this.cmbLoja2.UseSelectable = true;
            this.cmbLoja2.SelectedIndexChanged += new System.EventHandler(this.cmbLoja2_SelectedIndexChanged);
            this.cmbLoja2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbLoja2_MouseClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.toolStrip3);
            this.groupBox2.Controls.Add(this.toolStripContainer3);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(222, 256);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(603, 55);
            this.groupBox2.TabIndex = 93;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Selecione a Loja que Emitiu a Requisição";
            // 
            // toolStrip3
            // 
            this.toolStrip3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btAOk});
            this.toolStrip3.Location = new System.Drawing.Point(469, 15);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(45, 25);
            this.toolStrip3.TabIndex = 91;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // btAOk
            // 
            this.btAOk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btAOk.Image = ((System.Drawing.Image)(resources.GetObject("btAOk.Image")));
            this.btAOk.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btAOk.Name = "btAOk";
            this.btAOk.Size = new System.Drawing.Size(42, 22);
            this.btAOk.Text = "Ok";
            this.btAOk.Click += new System.EventHandler(this.btAOk_Click);
            // 
            // toolStripContainer3
            // 
            // 
            // toolStripContainer3.ContentPanel
            // 
            this.toolStripContainer3.ContentPanel.Size = new System.Drawing.Size(79, 2);
            this.toolStripContainer3.Location = new System.Drawing.Point(469, 15);
            this.toolStripContainer3.Name = "toolStripContainer3";
            this.toolStripContainer3.Size = new System.Drawing.Size(79, 27);
            this.toolStripContainer3.TabIndex = 92;
            this.toolStripContainer3.Text = "toolStripContainer3";
            // 
            // toolStripContainer3.TopToolStripPanel
            // 
            this.toolStripContainer3.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer3.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer3.TopToolStripPanel.Tag = "XC";
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage3.Controls.Add(this.groupBox3);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(1075, 476);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Recepção";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.b);
            this.groupBox3.Controls.Add(this.toolStripContainer1);
            this.groupBox3.Controls.Add(this.cmbLoja3);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(216, 272);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(622, 59);
            this.groupBox3.TabIndex = 93;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Selecione a Loja do Destino da Requisição";
            // 
            // b
            // 
            this.b.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.b.Dock = System.Windows.Forms.DockStyle.None;
            this.b.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.b.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btReGravar});
            this.b.Location = new System.Drawing.Point(492, 19);
            this.b.Name = "b";
            this.b.Size = new System.Drawing.Size(45, 25);
            this.b.TabIndex = 91;
            this.b.Text = "toolStrip1";
            // 
            // btReGravar
            // 
            this.btReGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btReGravar.Image = ((System.Drawing.Image)(resources.GetObject("btReGravar.Image")));
            this.btReGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btReGravar.Name = "btReGravar";
            this.btReGravar.Size = new System.Drawing.Size(42, 22);
            this.btReGravar.Text = "Ok";
            this.btReGravar.Click += new System.EventHandler(this.btReGravar_Click);
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(78, 2);
            this.toolStripContainer1.Location = new System.Drawing.Point(492, 19);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(78, 27);
            this.toolStripContainer1.TabIndex = 92;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer1.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer1.TopToolStripPanel.Tag = "XC";
            // 
            // cmbLoja3
            // 
            this.cmbLoja3.FormattingEnabled = true;
            this.cmbLoja3.IntegralHeight = false;
            this.cmbLoja3.ItemHeight = 23;
            this.cmbLoja3.Location = new System.Drawing.Point(19, 19);
            this.cmbLoja3.Name = "cmbLoja3";
            this.cmbLoja3.Size = new System.Drawing.Size(452, 29);
            this.cmbLoja3.TabIndex = 8;
            this.cmbLoja3.UseSelectable = true;
            this.cmbLoja3.SelectedIndexChanged += new System.EventHandler(this.cmbLoja3_SelectedIndexChanged);
            this.cmbLoja3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbLoja3_MouseClick);
            // 
            // metroTabPage4
            // 
            this.metroTabPage4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage4.Controls.Add(this.dgFicha);
            this.metroTabPage4.Controls.Add(this.dgModelo);
            this.metroTabPage4.Controls.Add(this.dgMarca);
            this.metroTabPage4.Controls.Add(this.dgSubFam);
            this.metroTabPage4.Controls.Add(this.dgFam);
            this.metroTabPage4.Controls.Add(this.dgArma);
            this.metroTabPage4.Controls.Add(this.dgLoja);
            this.metroTabPage4.Controls.Add(this.dgArt);
            this.metroTabPage4.Controls.Add(this.dgDetail);
            this.metroTabPage4.Controls.Add(this.dgL1);
            this.metroTabPage4.Controls.Add(this.toolStrip4);
            this.metroTabPage4.Controls.Add(this.toolStripContainer4);
            this.metroTabPage4.Controls.Add(this.dgEntradaGuia);
            this.metroTabPage4.Controls.Add(this.cmbExIDGI);
            this.metroTabPage4.Controls.Add(this.cmbExEntidadeFilial);
            this.metroTabPage4.Controls.Add(this.metroLabel8);
            this.metroTabPage4.Controls.Add(this.metroLabel7);
            this.metroTabPage4.Controls.Add(this.metroLabel6);
            this.metroTabPage4.Controls.Add(this.metroLabel5);
            this.metroTabPage4.Controls.Add(this.metroLabel4);
            this.metroTabPage4.Controls.Add(this.groupBox4);
            this.metroTabPage4.HorizontalScrollbarBarColor = true;
            this.metroTabPage4.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.HorizontalScrollbarSize = 10;
            this.metroTabPage4.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage4.Name = "metroTabPage4";
            this.metroTabPage4.Size = new System.Drawing.Size(1075, 476);
            this.metroTabPage4.TabIndex = 3;
            this.metroTabPage4.Text = "Exportação";
            this.metroTabPage4.VerticalScrollbarBarColor = true;
            this.metroTabPage4.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage4.VerticalScrollbarSize = 10;
            // 
            // dgFicha
            // 
            this.dgFicha.AllowUserToResizeRows = false;
            this.dgFicha.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgFicha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgFicha.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgFicha.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFicha.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgFicha.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFicha.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn97,
            this.dataGridViewTextBoxColumn98,
            this.dataGridViewTextBoxColumn99,
            this.Column35,
            this.Column36,
            this.Column37,
            this.Column38,
            this.Column39,
            this.Column40,
            this.Column41,
            this.Column42,
            this.Column43,
            this.Column44});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgFicha.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgFicha.EnableHeadersVisualStyles = false;
            this.dgFicha.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgFicha.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgFicha.Location = new System.Drawing.Point(74, 79);
            this.dgFicha.Name = "dgFicha";
            this.dgFicha.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFicha.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgFicha.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgFicha.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFicha.Size = new System.Drawing.Size(10, 23);
            this.dgFicha.TabIndex = 101;
            this.dgFicha.Visible = false;
            // 
            // dataGridViewTextBoxColumn97
            // 
            this.dataGridViewTextBoxColumn97.DataPropertyName = "CODMARCA";
            this.dataGridViewTextBoxColumn97.HeaderText = "CODMARCA";
            this.dataGridViewTextBoxColumn97.Name = "dataGridViewTextBoxColumn97";
            // 
            // dataGridViewTextBoxColumn98
            // 
            this.dataGridViewTextBoxColumn98.DataPropertyName = "CODMODELO";
            this.dataGridViewTextBoxColumn98.HeaderText = "CODMODELO";
            this.dataGridViewTextBoxColumn98.Name = "dataGridViewTextBoxColumn98";
            // 
            // dataGridViewTextBoxColumn99
            // 
            this.dataGridViewTextBoxColumn99.DataPropertyName = "NSERIE";
            this.dataGridViewTextBoxColumn99.HeaderText = "NSERIE";
            this.dataGridViewTextBoxColumn99.Name = "dataGridViewTextBoxColumn99";
            // 
            // Column35
            // 
            this.Column35.DataPropertyName = "REFERENC";
            this.Column35.HeaderText = "REFERENC";
            this.Column35.Name = "Column35";
            // 
            // Column36
            // 
            this.Column36.DataPropertyName = "CODUTILIZADOR";
            this.Column36.HeaderText = "CODUTILIZADOR";
            this.Column36.Name = "Column36";
            // 
            // Column37
            // 
            this.Column37.DataPropertyName = "Column1";
            this.Column37.HeaderText = "DataCria";
            this.Column37.Name = "Column37";
            // 
            // Column38
            // 
            this.Column38.DataPropertyName = "FOB";
            this.Column38.HeaderText = "FOB";
            this.Column38.Name = "Column38";
            // 
            // Column39
            // 
            this.Column39.DataPropertyName = "CODENTIDADE";
            this.Column39.HeaderText = "CODENTIDADE";
            this.Column39.Name = "Column39";
            // 
            // Column40
            // 
            this.Column40.DataPropertyName = "IDUNICO_SAIDA";
            this.Column40.HeaderText = "IDUNICO_SAIDA";
            this.Column40.Name = "Column40";
            // 
            // Column41
            // 
            this.Column41.DataPropertyName = "DATAGARANTIA";
            this.Column41.HeaderText = "DATAGARANTIA";
            this.Column41.Name = "Column41";
            // 
            // Column42
            // 
            this.Column42.DataPropertyName = "CODLOJA";
            this.Column42.HeaderText = "CODLOJA";
            this.Column42.Name = "Column42";
            // 
            // Column43
            // 
            this.Column43.DataPropertyName = "CODARMZ";
            this.Column43.HeaderText = "CODARMZ";
            this.Column43.Name = "Column43";
            this.Column43.ReadOnly = true;
            // 
            // Column44
            // 
            this.Column44.DataPropertyName = "TEL_SIM";
            this.Column44.HeaderText = "TEL_SIM";
            this.Column44.Name = "Column44";
            // 
            // dgModelo
            // 
            this.dgModelo.AllowUserToResizeRows = false;
            this.dgModelo.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgModelo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgModelo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgModelo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgModelo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgModelo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgModelo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn95,
            this.dataGridViewTextBoxColumn96,
            this.Column34});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgModelo.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgModelo.EnableHeadersVisualStyles = false;
            this.dgModelo.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgModelo.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgModelo.Location = new System.Drawing.Point(58, 138);
            this.dgModelo.Name = "dgModelo";
            this.dgModelo.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgModelo.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgModelo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgModelo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgModelo.Size = new System.Drawing.Size(10, 28);
            this.dgModelo.TabIndex = 100;
            this.dgModelo.Visible = false;
            // 
            // dataGridViewTextBoxColumn95
            // 
            this.dataGridViewTextBoxColumn95.DataPropertyName = "CODMARCA";
            this.dataGridViewTextBoxColumn95.HeaderText = "CODMARCA";
            this.dataGridViewTextBoxColumn95.Name = "dataGridViewTextBoxColumn95";
            // 
            // dataGridViewTextBoxColumn96
            // 
            this.dataGridViewTextBoxColumn96.DataPropertyName = "CODMODELO";
            this.dataGridViewTextBoxColumn96.HeaderText = "CODMODELO";
            this.dataGridViewTextBoxColumn96.Name = "dataGridViewTextBoxColumn96";
            // 
            // Column34
            // 
            this.Column34.DataPropertyName = "DESCRMODELO";
            this.Column34.HeaderText = "DESCRMODELO";
            this.Column34.Name = "Column34";
            // 
            // dgMarca
            // 
            this.dgMarca.AllowUserToResizeRows = false;
            this.dgMarca.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgMarca.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgMarca.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgMarca.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMarca.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgMarca.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMarca.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn93,
            this.dataGridViewTextBoxColumn94});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgMarca.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgMarca.EnableHeadersVisualStyles = false;
            this.dgMarca.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgMarca.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgMarca.Location = new System.Drawing.Point(58, 110);
            this.dgMarca.Name = "dgMarca";
            this.dgMarca.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgMarca.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgMarca.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgMarca.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMarca.Size = new System.Drawing.Size(10, 26);
            this.dgMarca.TabIndex = 99;
            this.dgMarca.Visible = false;
            // 
            // dataGridViewTextBoxColumn93
            // 
            this.dataGridViewTextBoxColumn93.DataPropertyName = "CODMARCA";
            this.dataGridViewTextBoxColumn93.HeaderText = "CODMARCA";
            this.dataGridViewTextBoxColumn93.Name = "dataGridViewTextBoxColumn93";
            // 
            // dataGridViewTextBoxColumn94
            // 
            this.dataGridViewTextBoxColumn94.DataPropertyName = "DESCRMARCA";
            this.dataGridViewTextBoxColumn94.HeaderText = "DESCRMARCA";
            this.dataGridViewTextBoxColumn94.Name = "dataGridViewTextBoxColumn94";
            // 
            // dgSubFam
            // 
            this.dgSubFam.AllowUserToResizeRows = false;
            this.dgSubFam.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgSubFam.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgSubFam.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgSubFam.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSubFam.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgSubFam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSubFam.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn88,
            this.dataGridViewTextBoxColumn89,
            this.dataGridViewTextBoxColumn90,
            this.dataGridViewTextBoxColumn91,
            this.dataGridViewTextBoxColumn92});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgSubFam.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgSubFam.EnableHeadersVisualStyles = false;
            this.dgSubFam.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgSubFam.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgSubFam.Location = new System.Drawing.Point(58, 79);
            this.dgSubFam.Name = "dgSubFam";
            this.dgSubFam.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSubFam.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgSubFam.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgSubFam.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSubFam.Size = new System.Drawing.Size(10, 29);
            this.dgSubFam.TabIndex = 98;
            this.dgSubFam.Visible = false;
            // 
            // dataGridViewTextBoxColumn88
            // 
            this.dataGridViewTextBoxColumn88.DataPropertyName = "CodSubFam";
            this.dataGridViewTextBoxColumn88.HeaderText = "CodSubFam";
            this.dataGridViewTextBoxColumn88.Name = "dataGridViewTextBoxColumn88";
            // 
            // dataGridViewTextBoxColumn89
            // 
            this.dataGridViewTextBoxColumn89.DataPropertyName = "CodFam";
            this.dataGridViewTextBoxColumn89.HeaderText = "CodFam";
            this.dataGridViewTextBoxColumn89.Name = "dataGridViewTextBoxColumn89";
            // 
            // dataGridViewTextBoxColumn90
            // 
            this.dataGridViewTextBoxColumn90.DataPropertyName = "Descricao";
            this.dataGridViewTextBoxColumn90.HeaderText = "Descricao";
            this.dataGridViewTextBoxColumn90.Name = "dataGridViewTextBoxColumn90";
            // 
            // dataGridViewTextBoxColumn91
            // 
            this.dataGridViewTextBoxColumn91.DataPropertyName = "DataCria";
            this.dataGridViewTextBoxColumn91.HeaderText = "DataCria";
            this.dataGridViewTextBoxColumn91.Name = "dataGridViewTextBoxColumn91";
            // 
            // dataGridViewTextBoxColumn92
            // 
            this.dataGridViewTextBoxColumn92.DataPropertyName = "CodUtilizador";
            this.dataGridViewTextBoxColumn92.HeaderText = "CodUtilizador";
            this.dataGridViewTextBoxColumn92.Name = "dataGridViewTextBoxColumn92";
            // 
            // dgFam
            // 
            this.dgFam.AllowUserToResizeRows = false;
            this.dgFam.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgFam.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgFam.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgFam.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFam.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgFam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFam.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn78,
            this.dataGridViewTextBoxColumn79,
            this.dataGridViewTextBoxColumn80,
            this.dataGridViewTextBoxColumn81,
            this.dataGridViewTextBoxColumn82,
            this.dataGridViewTextBoxColumn83,
            this.dataGridViewTextBoxColumn84,
            this.dataGridViewTextBoxColumn85,
            this.dataGridViewTextBoxColumn86,
            this.dataGridViewTextBoxColumn87});
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgFam.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgFam.EnableHeadersVisualStyles = false;
            this.dgFam.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgFam.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgFam.Location = new System.Drawing.Point(42, 131);
            this.dgFam.Name = "dgFam";
            this.dgFam.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgFam.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgFam.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgFam.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgFam.Size = new System.Drawing.Size(10, 24);
            this.dgFam.TabIndex = 97;
            this.dgFam.Visible = false;
            // 
            // dataGridViewTextBoxColumn78
            // 
            this.dataGridViewTextBoxColumn78.DataPropertyName = "Codfam";
            this.dataGridViewTextBoxColumn78.HeaderText = "Codfam";
            this.dataGridViewTextBoxColumn78.Name = "dataGridViewTextBoxColumn78";
            // 
            // dataGridViewTextBoxColumn79
            // 
            this.dataGridViewTextBoxColumn79.DataPropertyName = "ContVend";
            this.dataGridViewTextBoxColumn79.HeaderText = "ContVend";
            this.dataGridViewTextBoxColumn79.Name = "dataGridViewTextBoxColumn79";
            // 
            // dataGridViewTextBoxColumn80
            // 
            this.dataGridViewTextBoxColumn80.DataPropertyName = "ContStok";
            this.dataGridViewTextBoxColumn80.HeaderText = "ContStok";
            this.dataGridViewTextBoxColumn80.Name = "dataGridViewTextBoxColumn80";
            // 
            // dataGridViewTextBoxColumn81
            // 
            this.dataGridViewTextBoxColumn81.DataPropertyName = "ContCusto";
            this.dataGridViewTextBoxColumn81.HeaderText = "ContCusto";
            this.dataGridViewTextBoxColumn81.Name = "dataGridViewTextBoxColumn81";
            // 
            // dataGridViewTextBoxColumn82
            // 
            this.dataGridViewTextBoxColumn82.DataPropertyName = "Nomemerc";
            this.dataGridViewTextBoxColumn82.HeaderText = "Nomemerc";
            this.dataGridViewTextBoxColumn82.Name = "dataGridViewTextBoxColumn82";
            // 
            // dataGridViewTextBoxColumn83
            // 
            this.dataGridViewTextBoxColumn83.DataPropertyName = "Datacria";
            this.dataGridViewTextBoxColumn83.HeaderText = "Datacria";
            this.dataGridViewTextBoxColumn83.Name = "dataGridViewTextBoxColumn83";
            // 
            // dataGridViewTextBoxColumn84
            // 
            this.dataGridViewTextBoxColumn84.DataPropertyName = "Codutilizador";
            this.dataGridViewTextBoxColumn84.HeaderText = "Codutilizador";
            this.dataGridViewTextBoxColumn84.Name = "dataGridViewTextBoxColumn84";
            // 
            // dataGridViewTextBoxColumn85
            // 
            this.dataGridViewTextBoxColumn85.DataPropertyName = "Desconto";
            this.dataGridViewTextBoxColumn85.HeaderText = "Desconto";
            this.dataGridViewTextBoxColumn85.Name = "dataGridViewTextBoxColumn85";
            // 
            // dataGridViewTextBoxColumn86
            // 
            this.dataGridViewTextBoxColumn86.DataPropertyName = "Data_alteracao";
            this.dataGridViewTextBoxColumn86.HeaderText = "Data_alteracao";
            this.dataGridViewTextBoxColumn86.Name = "dataGridViewTextBoxColumn86";
            // 
            // dataGridViewTextBoxColumn87
            // 
            this.dataGridViewTextBoxColumn87.DataPropertyName = "Alterado_por";
            this.dataGridViewTextBoxColumn87.HeaderText = "Alterado_por";
            this.dataGridViewTextBoxColumn87.Name = "dataGridViewTextBoxColumn87";
            // 
            // dgArma
            // 
            this.dgArma.AllowUserToResizeRows = false;
            this.dgArma.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgArma.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgArma.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgArma.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgArma.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgArma.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgArma.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64,
            this.dataGridViewTextBoxColumn65,
            this.dataGridViewTextBoxColumn66,
            this.dataGridViewTextBoxColumn67,
            this.dataGridViewTextBoxColumn68,
            this.dataGridViewTextBoxColumn69,
            this.dataGridViewTextBoxColumn70,
            this.dataGridViewTextBoxColumn71,
            this.dataGridViewTextBoxColumn72,
            this.dataGridViewTextBoxColumn73,
            this.dataGridViewTextBoxColumn74,
            this.dataGridViewTextBoxColumn75,
            this.dataGridViewTextBoxColumn76,
            this.dataGridViewTextBoxColumn77});
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgArma.DefaultCellStyle = dataGridViewCellStyle17;
            this.dgArma.EnableHeadersVisualStyles = false;
            this.dgArma.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgArma.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgArma.Location = new System.Drawing.Point(42, 110);
            this.dgArma.Name = "dgArma";
            this.dgArma.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgArma.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.dgArma.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgArma.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgArma.Size = new System.Drawing.Size(10, 19);
            this.dgArma.TabIndex = 96;
            this.dgArma.Visible = false;
            // 
            // dataGridViewTextBoxColumn62
            // 
            this.dataGridViewTextBoxColumn62.DataPropertyName = "Codloja";
            this.dataGridViewTextBoxColumn62.HeaderText = "Codloja";
            this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
            // 
            // dataGridViewTextBoxColumn63
            // 
            this.dataGridViewTextBoxColumn63.DataPropertyName = "Codarmz";
            this.dataGridViewTextBoxColumn63.HeaderText = "Codarmz";
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            // 
            // dataGridViewTextBoxColumn64
            // 
            this.dataGridViewTextBoxColumn64.DataPropertyName = "Nomearz";
            this.dataGridViewTextBoxColumn64.HeaderText = "Nomearz";
            this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
            // 
            // dataGridViewTextBoxColumn65
            // 
            this.dataGridViewTextBoxColumn65.DataPropertyName = "Cabecalho";
            this.dataGridViewTextBoxColumn65.HeaderText = "Cabecalho";
            this.dataGridViewTextBoxColumn65.Name = "dataGridViewTextBoxColumn65";
            // 
            // dataGridViewTextBoxColumn66
            // 
            this.dataGridViewTextBoxColumn66.DataPropertyName = "Rodape";
            this.dataGridViewTextBoxColumn66.HeaderText = "Rodape";
            this.dataGridViewTextBoxColumn66.Name = "dataGridViewTextBoxColumn66";
            // 
            // dataGridViewTextBoxColumn67
            // 
            this.dataGridViewTextBoxColumn67.DataPropertyName = "Morada";
            this.dataGridViewTextBoxColumn67.HeaderText = "Morada";
            this.dataGridViewTextBoxColumn67.Name = "dataGridViewTextBoxColumn67";
            // 
            // dataGridViewTextBoxColumn68
            // 
            this.dataGridViewTextBoxColumn68.DataPropertyName = "Codutilizador";
            this.dataGridViewTextBoxColumn68.HeaderText = "Codutilizador";
            this.dataGridViewTextBoxColumn68.Name = "dataGridViewTextBoxColumn68";
            // 
            // dataGridViewTextBoxColumn69
            // 
            this.dataGridViewTextBoxColumn69.DataPropertyName = "ContVend";
            this.dataGridViewTextBoxColumn69.HeaderText = "ContVend";
            this.dataGridViewTextBoxColumn69.Name = "dataGridViewTextBoxColumn69";
            // 
            // dataGridViewTextBoxColumn70
            // 
            this.dataGridViewTextBoxColumn70.DataPropertyName = "ContStock";
            this.dataGridViewTextBoxColumn70.HeaderText = "ContStock";
            this.dataGridViewTextBoxColumn70.Name = "dataGridViewTextBoxColumn70";
            // 
            // dataGridViewTextBoxColumn71
            // 
            this.dataGridViewTextBoxColumn71.DataPropertyName = "ContCusto";
            this.dataGridViewTextBoxColumn71.HeaderText = "ContCusto";
            this.dataGridViewTextBoxColumn71.Name = "dataGridViewTextBoxColumn71";
            // 
            // dataGridViewTextBoxColumn72
            // 
            this.dataGridViewTextBoxColumn72.DataPropertyName = "Datacria";
            this.dataGridViewTextBoxColumn72.HeaderText = "Datacria";
            this.dataGridViewTextBoxColumn72.Name = "dataGridViewTextBoxColumn72";
            // 
            // dataGridViewTextBoxColumn73
            // 
            this.dataGridViewTextBoxColumn73.DataPropertyName = "Data_Alteracao";
            this.dataGridViewTextBoxColumn73.HeaderText = "Data_Alteracao";
            this.dataGridViewTextBoxColumn73.Name = "dataGridViewTextBoxColumn73";
            // 
            // dataGridViewTextBoxColumn74
            // 
            this.dataGridViewTextBoxColumn74.DataPropertyName = "Alterado_por";
            this.dataGridViewTextBoxColumn74.HeaderText = "Alterado_por";
            this.dataGridViewTextBoxColumn74.Name = "dataGridViewTextBoxColumn74";
            // 
            // dataGridViewTextBoxColumn75
            // 
            this.dataGridViewTextBoxColumn75.DataPropertyName = "Codao";
            this.dataGridViewTextBoxColumn75.HeaderText = "Codao";
            this.dataGridViewTextBoxColumn75.Name = "dataGridViewTextBoxColumn75";
            // 
            // dataGridViewTextBoxColumn76
            // 
            this.dataGridViewTextBoxColumn76.DataPropertyName = "Arma_venda";
            this.dataGridViewTextBoxColumn76.HeaderText = "Arma_venda";
            this.dataGridViewTextBoxColumn76.Name = "dataGridViewTextBoxColumn76";
            // 
            // dataGridViewTextBoxColumn77
            // 
            this.dataGridViewTextBoxColumn77.DataPropertyName = "Arma_remoto";
            this.dataGridViewTextBoxColumn77.HeaderText = "Arma_remoto";
            this.dataGridViewTextBoxColumn77.Name = "dataGridViewTextBoxColumn77";
            // 
            // dgLoja
            // 
            this.dgLoja.AllowUserToResizeRows = false;
            this.dgLoja.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgLoja.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgLoja.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgLoja.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgLoja.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgLoja.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLoja.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48,
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewTextBoxColumn61});
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgLoja.DefaultCellStyle = dataGridViewCellStyle20;
            this.dgLoja.EnableHeadersVisualStyles = false;
            this.dgLoja.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgLoja.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgLoja.Location = new System.Drawing.Point(42, 79);
            this.dgLoja.Name = "dgLoja";
            this.dgLoja.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgLoja.RowHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.dgLoja.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgLoja.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgLoja.Size = new System.Drawing.Size(10, 27);
            this.dgLoja.TabIndex = 95;
            this.dgLoja.Visible = false;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.DataPropertyName = "Codloja";
            this.dataGridViewTextBoxColumn42.HeaderText = "Codloja";
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.DataPropertyName = "Datacria";
            this.dataGridViewTextBoxColumn43.HeaderText = "Datacria";
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.DataPropertyName = "Nomeloja";
            this.dataGridViewTextBoxColumn44.HeaderText = "Nomeloja";
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.DataPropertyName = "Datamov";
            this.dataGridViewTextBoxColumn45.HeaderText = "Datamov";
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.DataPropertyName = "Flag";
            this.dataGridViewTextBoxColumn46.HeaderText = "Flag";
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.DataPropertyName = "Codutilizador";
            this.dataGridViewTextBoxColumn47.HeaderText = "Codutilizador";
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.DataPropertyName = "Morada";
            this.dataGridViewTextBoxColumn48.HeaderText = "Morada";
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.DataPropertyName = "Caixa_Post";
            this.dataGridViewTextBoxColumn49.HeaderText = "Caixa_Post";
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.DataPropertyName = "Fone";
            this.dataGridViewTextBoxColumn50.HeaderText = "Fone";
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.DataPropertyName = "Fax";
            this.dataGridViewTextBoxColumn51.HeaderText = "Fax";
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.DataPropertyName = "Email";
            this.dataGridViewTextBoxColumn52.HeaderText = "Email";
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.DataPropertyName = "Lojaonline";
            this.dataGridViewTextBoxColumn53.HeaderText = "Lojaonline";
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.DataPropertyName = "Codconta";
            this.dataGridViewTextBoxColumn54.HeaderText = "Codconta";
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.DataPropertyName = "CodcontaPdr";
            this.dataGridViewTextBoxColumn55.HeaderText = "CodcontaPdr";
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.DataPropertyName = "CodcontaNac";
            this.dataGridViewTextBoxColumn56.HeaderText = "CodcontaNac";
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.DataPropertyName = "CodcontaOutros";
            this.dataGridViewTextBoxColumn57.HeaderText = "CodcontaOutros";
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.DataPropertyName = "Data_alteracao";
            this.dataGridViewTextBoxColumn58.HeaderText = "Data_alteracao";
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.DataPropertyName = "Alterado_por";
            this.dataGridViewTextBoxColumn59.HeaderText = "Alterado_por";
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.DataPropertyName = "Display_conta";
            this.dataGridViewTextBoxColumn60.HeaderText = "Display_conta";
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            // 
            // dataGridViewTextBoxColumn61
            // 
            this.dataGridViewTextBoxColumn61.DataPropertyName = "Agtelec";
            this.dataGridViewTextBoxColumn61.HeaderText = "Agtelec";
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            // 
            // dgArt
            // 
            this.dgArt.AllowUserToResizeRows = false;
            this.dgArt.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgArt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgArt.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgArt.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgArt.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dgArt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgArt.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24,
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn41,
            this.Column30,
            this.Column31,
            this.Column32,
            this.Column33});
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgArt.DefaultCellStyle = dataGridViewCellStyle23;
            this.dgArt.EnableHeadersVisualStyles = false;
            this.dgArt.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgArt.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgArt.Location = new System.Drawing.Point(26, 131);
            this.dgArt.Name = "dgArt";
            this.dgArt.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgArt.RowHeadersDefaultCellStyle = dataGridViewCellStyle24;
            this.dgArt.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgArt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgArt.Size = new System.Drawing.Size(10, 30);
            this.dgArt.TabIndex = 94;
            this.dgArt.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "IDunico";
            this.dataGridViewTextBoxColumn9.HeaderText = "IDunico";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "Column1";
            this.dataGridViewTextBoxColumn13.HeaderText = "Referenc";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "CodLoja";
            this.dataGridViewTextBoxColumn11.HeaderText = "CodLoja";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "CodArmz";
            this.dataGridViewTextBoxColumn12.HeaderText = "CodArmz";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "Codfam";
            this.dataGridViewTextBoxColumn10.HeaderText = "Codfam";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "Codsubfam";
            this.dataGridViewTextBoxColumn14.HeaderText = "Codsubfam";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "Descrartigo";
            this.dataGridViewTextBoxColumn15.HeaderText = "Descrartigo";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "Descr_sup";
            this.dataGridViewTextBoxColumn20.HeaderText = "Descr_sup";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "Codbar";
            this.dataGridViewTextBoxColumn22.HeaderText = "Codbar";
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "QTD";
            this.dataGridViewTextBoxColumn21.HeaderText = "QTD";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "FOB";
            this.dataGridViewTextBoxColumn17.HeaderText = "FOB";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "PCA";
            this.dataGridViewTextBoxColumn18.HeaderText = "PCA";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "PVP";
            this.dataGridViewTextBoxColumn19.HeaderText = "PVP";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "Column2";
            this.dataGridViewTextBoxColumn16.HeaderText = "DataCria";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "Unidade";
            this.dataGridViewTextBoxColumn23.HeaderText = "Unidade";
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "Desembalagem";
            this.dataGridViewTextBoxColumn24.HeaderText = "Desembalagem";
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "Alterapreco";
            this.dataGridViewTextBoxColumn25.HeaderText = "Alterapreco";
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "Contastocks";
            this.dataGridViewTextBoxColumn26.HeaderText = "Contastocks";
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "Stocknegativo";
            this.dataGridViewTextBoxColumn27.HeaderText = "Stocknegativo";
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "Devolver";
            this.dataGridViewTextBoxColumn28.HeaderText = "Devolver";
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "Stockmax";
            this.dataGridViewTextBoxColumn29.HeaderText = "Stockmax";
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "Stockmin";
            this.dataGridViewTextBoxColumn30.HeaderText = "Stockmin";
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "Stocktotal";
            this.dataGridViewTextBoxColumn31.HeaderText = "Stocktotal";
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "Obs";
            this.dataGridViewTextBoxColumn32.HeaderText = "Obs";
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "Data_fabrico";
            this.dataGridViewTextBoxColumn33.HeaderText = "Data_fabrico";
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "Pmc";
            this.dataGridViewTextBoxColumn34.HeaderText = "Pmc";
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "Regequip";
            this.dataGridViewTextBoxColumn35.HeaderText = "Regequip";
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "Codutilizador";
            this.dataGridViewTextBoxColumn36.HeaderText = "Codutilizador";
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "Column3";
            this.dataGridViewTextBoxColumn37.HeaderText = "Data_Expira";
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "Desconto";
            this.dataGridViewTextBoxColumn38.HeaderText = "Desconto";
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "Lucro";
            this.dataGridViewTextBoxColumn39.HeaderText = "Lucro";
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.DataPropertyName = "Codmarca";
            this.dataGridViewTextBoxColumn40.HeaderText = "Codmarca";
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.DataPropertyName = "Codmodelo";
            this.dataGridViewTextBoxColumn41.HeaderText = "Codmodelo";
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            // 
            // Column30
            // 
            this.Column30.DataPropertyName = "Tiposervico";
            this.Column30.HeaderText = "Tiposervico";
            this.Column30.Name = "Column30";
            // 
            // Column31
            // 
            this.Column31.DataPropertyName = "Permitir_obs";
            this.Column31.HeaderText = "Permitir_obs";
            this.Column31.Name = "Column31";
            // 
            // Column32
            // 
            this.Column32.DataPropertyName = "Activo";
            this.Column32.HeaderText = "Activo";
            this.Column32.Name = "Column32";
            // 
            // Column33
            // 
            this.Column33.DataPropertyName = "Pontoencomenda";
            this.Column33.HeaderText = "Pontoencomenda";
            this.Column33.Name = "Column33";
            // 
            // dgDetail
            // 
            this.dgDetail.AllowUserToResizeRows = false;
            this.dgDetail.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgDetail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgDetail.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgDetail.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDetail.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26,
            this.Column27,
            this.Column28,
            this.Column29});
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgDetail.DefaultCellStyle = dataGridViewCellStyle26;
            this.dgDetail.EnableHeadersVisualStyles = false;
            this.dgDetail.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgDetail.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgDetail.Location = new System.Drawing.Point(26, 106);
            this.dgDetail.Name = "dgDetail";
            this.dgDetail.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgDetail.RowHeadersDefaultCellStyle = dataGridViewCellStyle27;
            this.dgDetail.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDetail.Size = new System.Drawing.Size(10, 23);
            this.dgDetail.TabIndex = 93;
            this.dgDetail.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "IDunico";
            this.dataGridViewTextBoxColumn3.HeaderText = "IDunico";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "NUMDOC";
            this.dataGridViewTextBoxColumn6.HeaderText = "NUMDOC";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "CodLoja";
            this.dataGridViewTextBoxColumn7.HeaderText = "CodLoja";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "CodArmz";
            this.dataGridViewTextBoxColumn8.HeaderText = "CodArmz";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Column1";
            this.Column1.HeaderText = "Referenc";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "GUIA";
            this.Column2.HeaderText = "GUIA";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "AltStok";
            this.Column3.HeaderText = "AltStok";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Column2";
            this.Column4.HeaderText = "DataCria";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "FOB";
            this.Column5.HeaderText = "FOB";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "PCL";
            this.Column6.HeaderText = "PCL";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "PVP";
            this.Column7.HeaderText = "PVP";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "Descont";
            this.Column8.HeaderText = "Descont";
            this.Column8.Name = "Column8";
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "QTD";
            this.Column9.HeaderText = "QTD";
            this.Column9.Name = "Column9";
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "CodEntidade";
            this.Column10.HeaderText = "CodEntidade";
            this.Column10.Name = "Column10";
            // 
            // Column11
            // 
            this.Column11.DataPropertyName = "TIPMOV";
            this.Column11.HeaderText = "TIPMOV";
            this.Column11.Name = "Column11";
            // 
            // Column12
            // 
            this.Column12.DataPropertyName = "POSTO";
            this.Column12.HeaderText = "POSTO";
            this.Column12.Name = "Column12";
            // 
            // Column13
            // 
            this.Column13.DataPropertyName = "Moeda";
            this.Column13.HeaderText = "Moeda";
            this.Column13.Name = "Column13";
            // 
            // Column14
            // 
            this.Column14.DataPropertyName = "ANULADOR";
            this.Column14.HeaderText = "ANULADOR";
            this.Column14.Name = "Column14";
            // 
            // Column15
            // 
            this.Column15.DataPropertyName = "CodDoc";
            this.Column15.HeaderText = "CodDoc";
            this.Column15.Name = "Column15";
            // 
            // Column16
            // 
            this.Column16.DataPropertyName = "Anulado";
            this.Column16.HeaderText = "Anulado";
            this.Column16.Name = "Column16";
            // 
            // Column17
            // 
            this.Column17.DataPropertyName = "CodUtilizador";
            this.Column17.HeaderText = "CodUtilizador";
            this.Column17.Name = "Column17";
            // 
            // Column18
            // 
            this.Column18.DataPropertyName = "CodVend";
            this.Column18.HeaderText = "CodVend";
            this.Column18.Name = "Column18";
            // 
            // Column19
            // 
            this.Column19.DataPropertyName = "LojaDestino";
            this.Column19.HeaderText = "LojaDestino";
            this.Column19.Name = "Column19";
            // 
            // Column20
            // 
            this.Column20.DataPropertyName = "PCLD";
            this.Column20.HeaderText = "PCLD";
            this.Column20.Name = "Column20";
            // 
            // Column21
            // 
            this.Column21.DataPropertyName = "NumDocFRN";
            this.Column21.HeaderText = "NumDocFRN";
            this.Column21.Name = "Column21";
            // 
            // Column22
            // 
            this.Column22.DataPropertyName = "CodFornec";
            this.Column22.HeaderText = "CodFornec";
            this.Column22.Name = "Column22";
            // 
            // Column23
            // 
            this.Column23.DataPropertyName = "PVPD";
            this.Column23.HeaderText = "PVPD";
            this.Column23.Name = "Column23";
            // 
            // Column24
            // 
            this.Column24.DataPropertyName = "ARMZDESTINO";
            this.Column24.HeaderText = "ARMZDESTINO";
            this.Column24.Name = "Column24";
            // 
            // Column25
            // 
            this.Column25.DataPropertyName = "Obs";
            this.Column25.HeaderText = "Obs";
            this.Column25.Name = "Column25";
            // 
            // Column26
            // 
            this.Column26.DataPropertyName = "CodAO";
            this.Column26.HeaderText = "CodAO";
            this.Column26.Name = "Column26";
            // 
            // Column27
            // 
            this.Column27.DataPropertyName = "Data_Lancamento";
            this.Column27.HeaderText = "Data_Lancamento";
            this.Column27.Name = "Column27";
            // 
            // Column28
            // 
            this.Column28.DataPropertyName = "DATA_INV";
            this.Column28.HeaderText = "DATA_INV";
            this.Column28.Name = "Column28";
            // 
            // Column29
            // 
            this.Column29.DataPropertyName = "QTD_INV";
            this.Column29.HeaderText = "QTD_INV";
            this.Column29.Name = "Column29";
            // 
            // dgL1
            // 
            this.dgL1.AllowUserToResizeRows = false;
            this.dgL1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgL1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgL1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgL1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgL1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.dgL1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgL1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgL1.DefaultCellStyle = dataGridViewCellStyle29;
            this.dgL1.EnableHeadersVisualStyles = false;
            this.dgL1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgL1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgL1.Location = new System.Drawing.Point(26, 79);
            this.dgL1.Name = "dgL1";
            this.dgL1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgL1.RowHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.dgL1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgL1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgL1.Size = new System.Drawing.Size(10, 25);
            this.dgL1.TabIndex = 92;
            this.dgL1.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "NUMDOC";
            this.dataGridViewTextBoxColumn1.HeaderText = "NUMERO";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "NOMEENTIDADE";
            this.dataGridViewTextBoxColumn2.HeaderText = "CLIENTE";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Column1";
            this.dataGridViewTextBoxColumn4.HeaderText = "LOJA";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "IDUNICO";
            this.dataGridViewTextBoxColumn5.HeaderText = "ID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // toolStrip4
            // 
            this.toolStrip4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip4.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btEExportacao});
            this.toolStrip4.Location = new System.Drawing.Point(479, 79);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.Size = new System.Drawing.Size(88, 25);
            this.toolStrip4.TabIndex = 89;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // btEExportacao
            // 
            this.btEExportacao.Image = ((System.Drawing.Image)(resources.GetObject("btEExportacao.Image")));
            this.btEExportacao.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btEExportacao.Name = "btEExportacao";
            this.btEExportacao.Size = new System.Drawing.Size(85, 22);
            this.btEExportacao.Text = "Exportação";
            this.btEExportacao.Click += new System.EventHandler(this.btEExportacao_Click);
            // 
            // toolStripContainer4
            // 
            // 
            // toolStripContainer4.ContentPanel
            // 
            this.toolStripContainer4.ContentPanel.Size = new System.Drawing.Size(122, 2);
            this.toolStripContainer4.Location = new System.Drawing.Point(479, 79);
            this.toolStripContainer4.Name = "toolStripContainer4";
            this.toolStripContainer4.Size = new System.Drawing.Size(122, 27);
            this.toolStripContainer4.TabIndex = 90;
            this.toolStripContainer4.Text = "toolStripContainer4";
            // 
            // toolStripContainer4.TopToolStripPanel
            // 
            this.toolStripContainer4.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer4.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer4.TopToolStripPanel.Tag = "XC";
            // 
            // dgEntradaGuia
            // 
            this.dgEntradaGuia.AllowUserToResizeRows = false;
            this.dgEntradaGuia.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgEntradaGuia.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgEntradaGuia.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgEntradaGuia.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEntradaGuia.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.dgEntradaGuia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEntradaGuia.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Mercadoria,
            this.Referencia,
            this.Descricao,
            this.qtd,
            this.FOB,
            this.PCL,
            this.Total});
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEntradaGuia.DefaultCellStyle = dataGridViewCellStyle32;
            this.dgEntradaGuia.EnableHeadersVisualStyles = false;
            this.dgEntradaGuia.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgEntradaGuia.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgEntradaGuia.Location = new System.Drawing.Point(116, 284);
            this.dgEntradaGuia.Name = "dgEntradaGuia";
            this.dgEntradaGuia.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEntradaGuia.RowHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.dgEntradaGuia.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgEntradaGuia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEntradaGuia.Size = new System.Drawing.Size(839, 150);
            this.dgEntradaGuia.TabIndex = 19;
            // 
            // Mercadoria
            // 
            this.Mercadoria.DataPropertyName = "Column1";
            this.Mercadoria.HeaderText = "Mercadoria";
            this.Mercadoria.Name = "Mercadoria";
            // 
            // Referencia
            // 
            this.Referencia.DataPropertyName = "Column2";
            this.Referencia.HeaderText = "Referência";
            this.Referencia.Name = "Referencia";
            // 
            // Descricao
            // 
            this.Descricao.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Descricao.DataPropertyName = "Column3";
            this.Descricao.HeaderText = "Descrição";
            this.Descricao.Name = "Descricao";
            // 
            // qtd
            // 
            this.qtd.DataPropertyName = "QTD";
            this.qtd.HeaderText = "Quantidade";
            this.qtd.Name = "qtd";
            // 
            // FOB
            // 
            this.FOB.DataPropertyName = "FOB";
            this.FOB.HeaderText = "FOB";
            this.FOB.Name = "FOB";
            // 
            // PCL
            // 
            this.PCL.DataPropertyName = "PCLD";
            this.PCL.HeaderText = "PCL";
            this.PCL.Name = "PCL";
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Column4";
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            // 
            // cmbExIDGI
            // 
            this.cmbExIDGI.FormattingEnabled = true;
            this.cmbExIDGI.IntegralHeight = false;
            this.cmbExIDGI.ItemHeight = 23;
            this.cmbExIDGI.Location = new System.Drawing.Point(361, 193);
            this.cmbExIDGI.Name = "cmbExIDGI";
            this.cmbExIDGI.Size = new System.Drawing.Size(237, 29);
            this.cmbExIDGI.TabIndex = 18;
            this.cmbExIDGI.UseSelectable = true;
            this.cmbExIDGI.SelectedIndexChanged += new System.EventHandler(this.cmbExIDGI_SelectedIndexChanged);
            this.cmbExIDGI.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbExIDGI_MouseClick);
            // 
            // cmbExEntidadeFilial
            // 
            this.cmbExEntidadeFilial.FormattingEnabled = true;
            this.cmbExEntidadeFilial.IntegralHeight = false;
            this.cmbExEntidadeFilial.ItemHeight = 23;
            this.cmbExEntidadeFilial.Location = new System.Drawing.Point(361, 155);
            this.cmbExEntidadeFilial.Name = "cmbExEntidadeFilial";
            this.cmbExEntidadeFilial.Size = new System.Drawing.Size(434, 29);
            this.cmbExEntidadeFilial.TabIndex = 17;
            this.cmbExEntidadeFilial.UseSelectable = true;
            this.cmbExEntidadeFilial.SelectedIndexChanged += new System.EventHandler(this.cmbExEntidadeFilial_SelectedIndexChanged);
            this.cmbExEntidadeFilial.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbExEntidadeFilial_MouseClick);
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(242, 203);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(42, 19);
            this.metroLabel8.TabIndex = 13;
            this.metroLabel8.Text = "ID.G.I.";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(242, 165);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(99, 19);
            this.metroLabel7.TabIndex = 12;
            this.metroLabel7.Text = "Entidade / Filial";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(242, 131);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(72, 19);
            this.metroLabel6.TabIndex = 10;
            this.metroLabel6.Text = "Data Inicial";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(603, 131);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(67, 19);
            this.metroLabel5.TabIndex = 9;
            this.metroLabel5.Text = "Data Final";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel4.Location = new System.Drawing.Point(418, 34);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(291, 25);
            this.metroLabel4.TabIndex = 8;
            this.metroLabel4.Text = "Exportação dos Dados de Requisição";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtExDataFinal);
            this.groupBox4.Controls.Add(this.txtExDataInicial);
            this.groupBox4.Controls.Add(this.lbfuncao);
            this.groupBox4.Controls.Add(this.lbNumero);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(116, 112);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(839, 147);
            this.groupBox4.TabIndex = 91;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dados da Requisição";
            // 
            // txtExDataFinal
            // 
            this.txtExDataFinal.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtExDataFinal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtExDataFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExDataFinal.Location = new System.Drawing.Point(601, 19);
            this.txtExDataFinal.Mask = "00/00/0000";
            this.txtExDataFinal.Name = "txtExDataFinal";
            this.txtExDataFinal.Size = new System.Drawing.Size(78, 20);
            this.txtExDataFinal.TabIndex = 65;
            this.txtExDataFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtExDataFinal.ValidatingType = typeof(System.DateTime);
            this.txtExDataFinal.Leave += new System.EventHandler(this.txtExDataFinal_Leave);
            // 
            // txtExDataInicial
            // 
            this.txtExDataInicial.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtExDataInicial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtExDataInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExDataInicial.Location = new System.Drawing.Point(245, 19);
            this.txtExDataInicial.Mask = "00/00/0000";
            this.txtExDataInicial.Name = "txtExDataInicial";
            this.txtExDataInicial.Size = new System.Drawing.Size(78, 20);
            this.txtExDataInicial.TabIndex = 64;
            this.txtExDataInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtExDataInicial.ValidatingType = typeof(System.DateTime);
            this.txtExDataInicial.Leave += new System.EventHandler(this.txtExDataInicial_Leave);
            // 
            // lbfuncao
            // 
            this.lbfuncao.AutoSize = true;
            this.lbfuncao.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbfuncao.Location = new System.Drawing.Point(283, 113);
            this.lbfuncao.Name = "lbfuncao";
            this.lbfuncao.Size = new System.Drawing.Size(0, 0);
            this.lbfuncao.TabIndex = 12;
            // 
            // lbNumero
            // 
            this.lbNumero.AutoSize = true;
            this.lbNumero.Location = new System.Drawing.Point(521, 91);
            this.lbNumero.Name = "lbNumero";
            this.lbNumero.Size = new System.Drawing.Size(0, 0);
            this.lbNumero.TabIndex = 11;
            // 
            // metroTabPage5
            // 
            this.metroTabPage5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage5.Controls.Add(this.label2);
            this.metroTabPage5.Controls.Add(this.progressBar1);
            this.metroTabPage5.Controls.Add(this.dgEncomendas2);
            this.metroTabPage5.Controls.Add(this.dgEncomendas1);
            this.metroTabPage5.Controls.Add(this.lbItens);
            this.metroTabPage5.Controls.Add(this.metroLabel2);
            this.metroTabPage5.Controls.Add(this.lbTotalSelecionado);
            this.metroTabPage5.Controls.Add(this.lbTotalEncomendar);
            this.metroTabPage5.Controls.Add(this.toolStrip1);
            this.metroTabPage5.Controls.Add(this.toolStripContainer7);
            this.metroTabPage5.Controls.Add(this.toolStrip5);
            this.metroTabPage5.Controls.Add(this.toolStripContainer5);
            this.metroTabPage5.Controls.Add(this.metroLabel15);
            this.metroTabPage5.Controls.Add(this.metroLabel14);
            this.metroTabPage5.Controls.Add(this.dgEncomendas);
            this.metroTabPage5.Controls.Add(this.metroLabel13);
            this.metroTabPage5.Controls.Add(this.label3);
            this.metroTabPage5.Controls.Add(this.txtEnDiasRepor);
            this.metroTabPage5.Controls.Add(this.txtEnPercentualSaida);
            this.metroTabPage5.Controls.Add(this.txtEnNDias);
            this.metroTabPage5.Controls.Add(this.metroLabel12);
            this.metroTabPage5.Controls.Add(this.metroLabel11);
            this.metroTabPage5.Controls.Add(this.metroLabel10);
            this.metroTabPage5.Controls.Add(this.metroLabel9);
            this.metroTabPage5.Controls.Add(this.groupBox5);
            this.metroTabPage5.HorizontalScrollbarBarColor = true;
            this.metroTabPage5.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.HorizontalScrollbarSize = 10;
            this.metroTabPage5.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage5.Name = "metroTabPage5";
            this.metroTabPage5.Size = new System.Drawing.Size(1075, 476);
            this.metroTabPage5.TabIndex = 4;
            this.metroTabPage5.Text = "Encomendas";
            this.metroTabPage5.VerticalScrollbarBarColor = true;
            this.metroTabPage5.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage5.VerticalScrollbarSize = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(782, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(12, 19);
            this.label2.TabIndex = 101;
            this.label2.Text = "l";
            this.label2.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(361, 212);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(406, 23);
            this.progressBar1.TabIndex = 100;
            this.progressBar1.Visible = false;
            // 
            // dgEncomendas2
            // 
            this.dgEncomendas2.AllowUserToOrderColumns = true;
            this.dgEncomendas2.AllowUserToResizeRows = false;
            this.dgEncomendas2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgEncomendas2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgEncomendas2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgEncomendas2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEncomendas2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.dgEncomendas2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEncomendas2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn111,
            this.dataGridViewTextBoxColumn114,
            this.dataGridViewTextBoxColumn122,
            this.dataGridViewTextBoxColumn124,
            this.dataGridViewTextBoxColumn125,
            this.dataGridViewTextBoxColumn126,
            this.dataGridViewTextBoxColumn127,
            this.dataGridViewTextBoxColumn128,
            this.dataGridViewTextBoxColumn129,
            this.dataGridViewTextBoxColumn130,
            this.dataGridViewTextBoxColumn131,
            this.dataGridViewTextBoxColumn132,
            this.dataGridViewTextBoxColumn133,
            this.dataGridViewTextBoxColumn134,
            this.dataGridViewTextBoxColumn135,
            this.dataGridViewTextBoxColumn136,
            this.dataGridViewTextBoxColumn137,
            this.dataGridViewTextBoxColumn138,
            this.dataGridViewTextBoxColumn139,
            this.dataGridViewTextBoxColumn140,
            this.dataGridViewTextBoxColumn141,
            this.dataGridViewTextBoxColumn142,
            this.dataGridViewTextBoxColumn143,
            this.dataGridViewTextBoxColumn144});
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEncomendas2.DefaultCellStyle = dataGridViewCellStyle35;
            this.dgEncomendas2.EnableHeadersVisualStyles = false;
            this.dgEncomendas2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgEncomendas2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgEncomendas2.Location = new System.Drawing.Point(84, 238);
            this.dgEncomendas2.Name = "dgEncomendas2";
            this.dgEncomendas2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEncomendas2.RowHeadersDefaultCellStyle = dataGridViewCellStyle36;
            this.dgEncomendas2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgEncomendas2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEncomendas2.Size = new System.Drawing.Size(843, 150);
            this.dgEncomendas2.TabIndex = 99;
            this.dgEncomendas2.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEncomendas2_CellContentDoubleClick);
            // 
            // dataGridViewTextBoxColumn111
            // 
            this.dataGridViewTextBoxColumn111.DataPropertyName = "CodLoja";
            this.dataGridViewTextBoxColumn111.HeaderText = "LOJA";
            this.dataGridViewTextBoxColumn111.Name = "dataGridViewTextBoxColumn111";
            this.dataGridViewTextBoxColumn111.Visible = false;
            // 
            // dataGridViewTextBoxColumn114
            // 
            this.dataGridViewTextBoxColumn114.DataPropertyName = "CODIGOLOJA_GE";
            this.dataGridViewTextBoxColumn114.HeaderText = "LOJAX";
            this.dataGridViewTextBoxColumn114.Name = "dataGridViewTextBoxColumn114";
            this.dataGridViewTextBoxColumn114.Visible = false;
            // 
            // dataGridViewTextBoxColumn122
            // 
            this.dataGridViewTextBoxColumn122.DataPropertyName = "CodArmz";
            this.dataGridViewTextBoxColumn122.HeaderText = "Armazém";
            this.dataGridViewTextBoxColumn122.Name = "dataGridViewTextBoxColumn122";
            this.dataGridViewTextBoxColumn122.Visible = false;
            // 
            // dataGridViewTextBoxColumn124
            // 
            this.dataGridViewTextBoxColumn124.DataPropertyName = "REFERENC";
            this.dataGridViewTextBoxColumn124.HeaderText = "Código";
            this.dataGridViewTextBoxColumn124.Name = "dataGridViewTextBoxColumn124";
            // 
            // dataGridViewTextBoxColumn125
            // 
            this.dataGridViewTextBoxColumn125.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn125.DataPropertyName = "DESCRARTIGO";
            this.dataGridViewTextBoxColumn125.HeaderText = "Designação";
            this.dataGridViewTextBoxColumn125.Name = "dataGridViewTextBoxColumn125";
            // 
            // dataGridViewTextBoxColumn126
            // 
            this.dataGridViewTextBoxColumn126.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn126.DataPropertyName = "QTD_STK";
            this.dataGridViewTextBoxColumn126.HeaderText = "Stock";
            this.dataGridViewTextBoxColumn126.Name = "dataGridViewTextBoxColumn126";
            this.dataGridViewTextBoxColumn126.Width = 58;
            // 
            // dataGridViewTextBoxColumn127
            // 
            this.dataGridViewTextBoxColumn127.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn127.DataPropertyName = "QTD_SUGESTAO";
            this.dataGridViewTextBoxColumn127.HeaderText = "Sugestão";
            this.dataGridViewTextBoxColumn127.Name = "dataGridViewTextBoxColumn127";
            this.dataGridViewTextBoxColumn127.Width = 78;
            // 
            // dataGridViewTextBoxColumn128
            // 
            this.dataGridViewTextBoxColumn128.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn128.DataPropertyName = "QTD_SAIDA";
            this.dataGridViewTextBoxColumn128.HeaderText = "Qtd Saída";
            this.dataGridViewTextBoxColumn128.Name = "dataGridViewTextBoxColumn128";
            this.dataGridViewTextBoxColumn128.Width = 81;
            // 
            // dataGridViewTextBoxColumn129
            // 
            this.dataGridViewTextBoxColumn129.DataPropertyName = "Column2";
            this.dataGridViewTextBoxColumn129.HeaderText = "PCA";
            this.dataGridViewTextBoxColumn129.Name = "dataGridViewTextBoxColumn129";
            this.dataGridViewTextBoxColumn129.Visible = false;
            // 
            // dataGridViewTextBoxColumn130
            // 
            this.dataGridViewTextBoxColumn130.DataPropertyName = "FOB";
            this.dataGridViewTextBoxColumn130.HeaderText = "FOB";
            this.dataGridViewTextBoxColumn130.Name = "dataGridViewTextBoxColumn130";
            this.dataGridViewTextBoxColumn130.Visible = false;
            // 
            // dataGridViewTextBoxColumn131
            // 
            this.dataGridViewTextBoxColumn131.DataPropertyName = "PVPD";
            this.dataGridViewTextBoxColumn131.HeaderText = "PVP";
            this.dataGridViewTextBoxColumn131.Name = "dataGridViewTextBoxColumn131";
            this.dataGridViewTextBoxColumn131.Visible = false;
            // 
            // dataGridViewTextBoxColumn132
            // 
            this.dataGridViewTextBoxColumn132.DataPropertyName = "LOCALI";
            this.dataGridViewTextBoxColumn132.HeaderText = "Locali";
            this.dataGridViewTextBoxColumn132.Name = "dataGridViewTextBoxColumn132";
            this.dataGridViewTextBoxColumn132.Visible = false;
            // 
            // dataGridViewTextBoxColumn133
            // 
            this.dataGridViewTextBoxColumn133.DataPropertyName = "Column1";
            this.dataGridViewTextBoxColumn133.HeaderText = "qtdEncomenda";
            this.dataGridViewTextBoxColumn133.Name = "dataGridViewTextBoxColumn133";
            this.dataGridViewTextBoxColumn133.Visible = false;
            // 
            // dataGridViewTextBoxColumn134
            // 
            this.dataGridViewTextBoxColumn134.DataPropertyName = "PCLD";
            this.dataGridViewTextBoxColumn134.HeaderText = "Valor PCL";
            this.dataGridViewTextBoxColumn134.Name = "dataGridViewTextBoxColumn134";
            // 
            // dataGridViewTextBoxColumn135
            // 
            this.dataGridViewTextBoxColumn135.DataPropertyName = "QTDGE";
            this.dataGridViewTextBoxColumn135.HeaderText = "STK";
            this.dataGridViewTextBoxColumn135.Name = "dataGridViewTextBoxColumn135";
            // 
            // dataGridViewTextBoxColumn136
            // 
            this.dataGridViewTextBoxColumn136.HeaderText = "CodDoc";
            this.dataGridViewTextBoxColumn136.Name = "dataGridViewTextBoxColumn136";
            this.dataGridViewTextBoxColumn136.Visible = false;
            // 
            // dataGridViewTextBoxColumn137
            // 
            this.dataGridViewTextBoxColumn137.HeaderText = "Conta Stock";
            this.dataGridViewTextBoxColumn137.Name = "dataGridViewTextBoxColumn137";
            this.dataGridViewTextBoxColumn137.Visible = false;
            // 
            // dataGridViewTextBoxColumn138
            // 
            this.dataGridViewTextBoxColumn138.HeaderText = "CodAo";
            this.dataGridViewTextBoxColumn138.Name = "dataGridViewTextBoxColumn138";
            this.dataGridViewTextBoxColumn138.Visible = false;
            // 
            // dataGridViewTextBoxColumn139
            // 
            this.dataGridViewTextBoxColumn139.HeaderText = "Opção";
            this.dataGridViewTextBoxColumn139.Name = "dataGridViewTextBoxColumn139";
            this.dataGridViewTextBoxColumn139.Visible = false;
            // 
            // dataGridViewTextBoxColumn140
            // 
            this.dataGridViewTextBoxColumn140.HeaderText = "Índice";
            this.dataGridViewTextBoxColumn140.Name = "dataGridViewTextBoxColumn140";
            this.dataGridViewTextBoxColumn140.Visible = false;
            // 
            // dataGridViewTextBoxColumn141
            // 
            this.dataGridViewTextBoxColumn141.HeaderText = "ÍndiceX";
            this.dataGridViewTextBoxColumn141.Name = "dataGridViewTextBoxColumn141";
            this.dataGridViewTextBoxColumn141.Visible = false;
            // 
            // dataGridViewTextBoxColumn142
            // 
            this.dataGridViewTextBoxColumn142.HeaderText = "IDúnicoGE";
            this.dataGridViewTextBoxColumn142.Name = "dataGridViewTextBoxColumn142";
            this.dataGridViewTextBoxColumn142.Visible = false;
            // 
            // dataGridViewTextBoxColumn143
            // 
            this.dataGridViewTextBoxColumn143.DataPropertyName = "Column3";
            this.dataGridViewTextBoxColumn143.HeaderText = "Linhas";
            this.dataGridViewTextBoxColumn143.Name = "dataGridViewTextBoxColumn143";
            this.dataGridViewTextBoxColumn143.Visible = false;
            // 
            // dataGridViewTextBoxColumn144
            // 
            this.dataGridViewTextBoxColumn144.DataPropertyName = "CodigoArmzGE";
            this.dataGridViewTextBoxColumn144.HeaderText = "ArmazémX";
            this.dataGridViewTextBoxColumn144.Name = "dataGridViewTextBoxColumn144";
            // 
            // dgEncomendas1
            // 
            this.dgEncomendas1.AllowUserToResizeRows = false;
            this.dgEncomendas1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgEncomendas1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgEncomendas1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgEncomendas1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEncomendas1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle37;
            this.dgEncomendas1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEncomendas1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn100,
            this.dataGridViewTextBoxColumn101,
            this.dataGridViewTextBoxColumn102,
            this.dataGridViewTextBoxColumn103,
            this.dataGridViewTextBoxColumn104,
            this.dataGridViewTextBoxColumn105,
            this.dataGridViewTextBoxColumn106,
            this.dataGridViewTextBoxColumn107,
            this.dataGridViewTextBoxColumn108,
            this.dataGridViewTextBoxColumn109,
            this.dataGridViewTextBoxColumn110,
            this.dataGridViewTextBoxColumn112,
            this.dataGridViewTextBoxColumn113,
            this.dataGridViewTextBoxColumn115,
            this.dataGridViewTextBoxColumn116,
            this.dataGridViewTextBoxColumn117,
            this.dataGridViewTextBoxColumn118,
            this.dataGridViewTextBoxColumn119,
            this.dataGridViewTextBoxColumn120,
            this.dataGridViewTextBoxColumn121,
            this.dataGridViewTextBoxColumn123});
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEncomendas1.DefaultCellStyle = dataGridViewCellStyle38;
            this.dgEncomendas1.EnableHeadersVisualStyles = false;
            this.dgEncomendas1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgEncomendas1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgEncomendas1.Location = new System.Drawing.Point(5, 241);
            this.dgEncomendas1.Name = "dgEncomendas1";
            this.dgEncomendas1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle39.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle39.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEncomendas1.RowHeadersDefaultCellStyle = dataGridViewCellStyle39;
            this.dgEncomendas1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgEncomendas1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEncomendas1.Size = new System.Drawing.Size(73, 79);
            this.dgEncomendas1.TabIndex = 98;
            this.dgEncomendas1.Visible = false;
            this.dgEncomendas1.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEncomendas1_CellContentDoubleClick);
            // 
            // dataGridViewTextBoxColumn100
            // 
            this.dataGridViewTextBoxColumn100.DataPropertyName = "Column1";
            this.dataGridViewTextBoxColumn100.HeaderText = "LOJA1";
            this.dataGridViewTextBoxColumn100.Name = "dataGridViewTextBoxColumn100";
            // 
            // dataGridViewTextBoxColumn101
            // 
            this.dataGridViewTextBoxColumn101.DataPropertyName = "Column2";
            this.dataGridViewTextBoxColumn101.HeaderText = "LOJA2";
            this.dataGridViewTextBoxColumn101.Name = "dataGridViewTextBoxColumn101";
            // 
            // dataGridViewTextBoxColumn102
            // 
            this.dataGridViewTextBoxColumn102.DataPropertyName = "CodArmz";
            this.dataGridViewTextBoxColumn102.HeaderText = "Armazém";
            this.dataGridViewTextBoxColumn102.Name = "dataGridViewTextBoxColumn102";
            // 
            // dataGridViewTextBoxColumn103
            // 
            this.dataGridViewTextBoxColumn103.DataPropertyName = "REFERENC";
            this.dataGridViewTextBoxColumn103.HeaderText = "Código";
            this.dataGridViewTextBoxColumn103.Name = "dataGridViewTextBoxColumn103";
            // 
            // dataGridViewTextBoxColumn104
            // 
            this.dataGridViewTextBoxColumn104.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewTextBoxColumn104.DataPropertyName = "DESCRARTIGO";
            this.dataGridViewTextBoxColumn104.HeaderText = "Designação";
            this.dataGridViewTextBoxColumn104.Name = "dataGridViewTextBoxColumn104";
            this.dataGridViewTextBoxColumn104.Width = 90;
            // 
            // dataGridViewTextBoxColumn105
            // 
            this.dataGridViewTextBoxColumn105.DataPropertyName = "STOCK";
            this.dataGridViewTextBoxColumn105.HeaderText = "Stock";
            this.dataGridViewTextBoxColumn105.Name = "dataGridViewTextBoxColumn105";
            // 
            // dataGridViewTextBoxColumn106
            // 
            this.dataGridViewTextBoxColumn106.DataPropertyName = "PERCENTAGEM";
            this.dataGridViewTextBoxColumn106.HeaderText = "Sugestão";
            this.dataGridViewTextBoxColumn106.Name = "dataGridViewTextBoxColumn106";
            // 
            // dataGridViewTextBoxColumn107
            // 
            this.dataGridViewTextBoxColumn107.DataPropertyName = "QTDSAIDA";
            this.dataGridViewTextBoxColumn107.HeaderText = "Qtd Saída";
            this.dataGridViewTextBoxColumn107.Name = "dataGridViewTextBoxColumn107";
            // 
            // dataGridViewTextBoxColumn108
            // 
            this.dataGridViewTextBoxColumn108.DataPropertyName = "PCA";
            this.dataGridViewTextBoxColumn108.HeaderText = "PCA";
            this.dataGridViewTextBoxColumn108.Name = "dataGridViewTextBoxColumn108";
            // 
            // dataGridViewTextBoxColumn109
            // 
            this.dataGridViewTextBoxColumn109.DataPropertyName = "FOB";
            this.dataGridViewTextBoxColumn109.HeaderText = "FOB";
            this.dataGridViewTextBoxColumn109.Name = "dataGridViewTextBoxColumn109";
            // 
            // dataGridViewTextBoxColumn110
            // 
            this.dataGridViewTextBoxColumn110.DataPropertyName = "PVP";
            this.dataGridViewTextBoxColumn110.HeaderText = "PVP";
            this.dataGridViewTextBoxColumn110.Name = "dataGridViewTextBoxColumn110";
            // 
            // dataGridViewTextBoxColumn112
            // 
            this.dataGridViewTextBoxColumn112.DataPropertyName = "Column3";
            this.dataGridViewTextBoxColumn112.HeaderText = "qtdEncomenda";
            this.dataGridViewTextBoxColumn112.Name = "dataGridViewTextBoxColumn112";
            // 
            // dataGridViewTextBoxColumn113
            // 
            this.dataGridViewTextBoxColumn113.HeaderText = "Valor PCL";
            this.dataGridViewTextBoxColumn113.Name = "dataGridViewTextBoxColumn113";
            // 
            // dataGridViewTextBoxColumn115
            // 
            this.dataGridViewTextBoxColumn115.HeaderText = "CodDoc";
            this.dataGridViewTextBoxColumn115.Name = "dataGridViewTextBoxColumn115";
            // 
            // dataGridViewTextBoxColumn116
            // 
            this.dataGridViewTextBoxColumn116.HeaderText = "Conta Stock";
            this.dataGridViewTextBoxColumn116.Name = "dataGridViewTextBoxColumn116";
            // 
            // dataGridViewTextBoxColumn117
            // 
            this.dataGridViewTextBoxColumn117.HeaderText = "CodAo";
            this.dataGridViewTextBoxColumn117.Name = "dataGridViewTextBoxColumn117";
            // 
            // dataGridViewTextBoxColumn118
            // 
            this.dataGridViewTextBoxColumn118.HeaderText = "Opção";
            this.dataGridViewTextBoxColumn118.Name = "dataGridViewTextBoxColumn118";
            // 
            // dataGridViewTextBoxColumn119
            // 
            this.dataGridViewTextBoxColumn119.HeaderText = "Índice";
            this.dataGridViewTextBoxColumn119.Name = "dataGridViewTextBoxColumn119";
            // 
            // dataGridViewTextBoxColumn120
            // 
            this.dataGridViewTextBoxColumn120.HeaderText = "ÍndiceX";
            this.dataGridViewTextBoxColumn120.Name = "dataGridViewTextBoxColumn120";
            // 
            // dataGridViewTextBoxColumn121
            // 
            this.dataGridViewTextBoxColumn121.HeaderText = "IDúnicoGE";
            this.dataGridViewTextBoxColumn121.Name = "dataGridViewTextBoxColumn121";
            // 
            // dataGridViewTextBoxColumn123
            // 
            this.dataGridViewTextBoxColumn123.HeaderText = "ArmazémX";
            this.dataGridViewTextBoxColumn123.Name = "dataGridViewTextBoxColumn123";
            // 
            // lbItens
            // 
            this.lbItens.AutoSize = true;
            this.lbItens.Location = new System.Drawing.Point(216, 444);
            this.lbItens.Name = "lbItens";
            this.lbItens.Size = new System.Drawing.Size(16, 19);
            this.lbItens.TabIndex = 97;
            this.lbItens.Text = "0";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(78, 444);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(35, 19);
            this.metroLabel2.TabIndex = 96;
            this.metroLabel2.Text = "Itens";
            // 
            // lbTotalSelecionado
            // 
            this.lbTotalSelecionado.AutoSize = true;
            this.lbTotalSelecionado.Location = new System.Drawing.Point(216, 421);
            this.lbTotalSelecionado.Name = "lbTotalSelecionado";
            this.lbTotalSelecionado.Size = new System.Drawing.Size(16, 19);
            this.lbTotalSelecionado.TabIndex = 95;
            this.lbTotalSelecionado.Text = "0";
            // 
            // lbTotalEncomendar
            // 
            this.lbTotalEncomendar.AutoSize = true;
            this.lbTotalEncomendar.Location = new System.Drawing.Point(217, 400);
            this.lbTotalEncomendar.Name = "lbTotalEncomendar";
            this.lbTotalEncomendar.Size = new System.Drawing.Size(16, 19);
            this.lbTotalEncomendar.TabIndex = 94;
            this.lbTotalEncomendar.Text = "0";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btAtualizar});
            this.toolStrip1.Location = new System.Drawing.Point(379, 432);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(82, 25);
            this.toolStrip1.TabIndex = 92;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btAtualizar
            // 
            this.btAtualizar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("btAtualizar.Image")));
            this.btAtualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btAtualizar.Name = "btAtualizar";
            this.btAtualizar.Size = new System.Drawing.Size(79, 22);
            this.btAtualizar.Text = "Actualizar";
            this.btAtualizar.Click += new System.EventHandler(this.btAtualizar_Click);
            // 
            // toolStripContainer7
            // 
            // 
            // toolStripContainer7.ContentPanel
            // 
            this.toolStripContainer7.ContentPanel.Size = new System.Drawing.Size(115, 2);
            this.toolStripContainer7.Location = new System.Drawing.Point(379, 432);
            this.toolStripContainer7.Name = "toolStripContainer7";
            this.toolStripContainer7.Size = new System.Drawing.Size(115, 27);
            this.toolStripContainer7.TabIndex = 93;
            this.toolStripContainer7.Text = "toolStripContainer7";
            // 
            // toolStripContainer7.TopToolStripPanel
            // 
            this.toolStripContainer7.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer7.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer7.TopToolStripPanel.Tag = "XC";
            // 
            // toolStrip5
            // 
            this.toolStrip5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip5.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip5.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btEnCalcular,
            this.btEnCriar});
            this.toolStrip5.Location = new System.Drawing.Point(481, 56);
            this.toolStrip5.Name = "toolStrip5";
            this.toolStrip5.Size = new System.Drawing.Size(109, 25);
            this.toolStrip5.TabIndex = 89;
            this.toolStrip5.Text = "toolStrip5";
            // 
            // btEnCalcular
            // 
            this.btEnCalcular.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btEnCalcular.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btEnCalcular.Name = "btEnCalcular";
            this.btEnCalcular.Size = new System.Drawing.Size(54, 22);
            this.btEnCalcular.Text = "Calcular";
            this.btEnCalcular.Click += new System.EventHandler(this.btEnCalcular_Click);
            // 
            // btEnCriar
            // 
            this.btEnCriar.Image = ((System.Drawing.Image)(resources.GetObject("btEnCriar.Image")));
            this.btEnCriar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btEnCriar.Name = "btEnCriar";
            this.btEnCriar.Size = new System.Drawing.Size(52, 22);
            this.btEnCriar.Text = "Criar";
            this.btEnCriar.Click += new System.EventHandler(this.btEnCriar_Click);
            // 
            // toolStripContainer5
            // 
            // 
            // toolStripContainer5.ContentPanel
            // 
            this.toolStripContainer5.ContentPanel.Size = new System.Drawing.Size(148, 2);
            this.toolStripContainer5.Location = new System.Drawing.Point(481, 56);
            this.toolStripContainer5.Name = "toolStripContainer5";
            this.toolStripContainer5.Size = new System.Drawing.Size(148, 27);
            this.toolStripContainer5.TabIndex = 90;
            this.toolStripContainer5.Text = "toolStripContainer5";
            // 
            // toolStripContainer5.TopToolStripPanel
            // 
            this.toolStripContainer5.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer5.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer5.TopToolStripPanel.Tag = "XC";
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(78, 421);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(113, 19);
            this.metroLabel15.TabIndex = 26;
            this.metroLabel15.Text = "Total Selecionado";
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(78, 400);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(127, 19);
            this.metroLabel14.TabIndex = 25;
            this.metroLabel14.Text = "Total a Encomendar";
            // 
            // dgEncomendas
            // 
            this.dgEncomendas.AllowUserToResizeRows = false;
            this.dgEncomendas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgEncomendas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgEncomendas.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgEncomendas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEncomendas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle40;
            this.dgEncomendas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEncomendas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column48,
            this.LOJA2,
            this.Column46,
            this.Column49,
            this.Column50,
            this.Column51,
            this.Column52,
            this.Col5,
            this.Col6,
            this.F,
            this.PVP,
            this.Column45,
            this.Column53,
            this.ValorPCL,
            this.STK,
            this.EP,
            this.Column47,
            this.Column54,
            this.Column55,
            this.Column56,
            this.Column57,
            this.Column58,
            this.Column59,
            this.Column60});
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEncomendas.DefaultCellStyle = dataGridViewCellStyle41;
            this.dgEncomendas.EnableHeadersVisualStyles = false;
            this.dgEncomendas.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.dgEncomendas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgEncomendas.Location = new System.Drawing.Point(525, 400);
            this.dgEncomendas.Name = "dgEncomendas";
            this.dgEncomendas.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEncomendas.RowHeadersDefaultCellStyle = dataGridViewCellStyle42;
            this.dgEncomendas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgEncomendas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEncomendas.Size = new System.Drawing.Size(453, 80);
            this.dgEncomendas.TabIndex = 24;
            this.dgEncomendas.Visible = false;
            this.dgEncomendas.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEncomendas_CellContentDoubleClick);
            // 
            // Column48
            // 
            this.Column48.DataPropertyName = "Column1";
            this.Column48.HeaderText = "LOJA1";
            this.Column48.Name = "Column48";
            // 
            // LOJA2
            // 
            this.LOJA2.DataPropertyName = "Column2";
            this.LOJA2.HeaderText = "LOJA2";
            this.LOJA2.Name = "LOJA2";
            // 
            // Column46
            // 
            this.Column46.DataPropertyName = "CodArmz";
            this.Column46.HeaderText = "Armazém";
            this.Column46.Name = "Column46";
            this.Column46.Visible = false;
            // 
            // Column49
            // 
            this.Column49.DataPropertyName = "REFERENC";
            this.Column49.HeaderText = "Código";
            this.Column49.Name = "Column49";
            // 
            // Column50
            // 
            this.Column50.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column50.DataPropertyName = "DESCRARTIGO";
            this.Column50.HeaderText = "Designação";
            this.Column50.Name = "Column50";
            // 
            // Column51
            // 
            this.Column51.DataPropertyName = "STOCK";
            this.Column51.HeaderText = "Stock";
            this.Column51.Name = "Column51";
            // 
            // Column52
            // 
            this.Column52.DataPropertyName = "PERCENTAGEM";
            this.Column52.HeaderText = "Sugestão";
            this.Column52.Name = "Column52";
            // 
            // Col5
            // 
            this.Col5.DataPropertyName = "QTDSAIDA";
            this.Col5.HeaderText = "Qtd Saída";
            this.Col5.Name = "Col5";
            // 
            // Col6
            // 
            this.Col6.DataPropertyName = "PCA";
            this.Col6.HeaderText = "PCA";
            this.Col6.Name = "Col6";
            this.Col6.Visible = false;
            // 
            // F
            // 
            this.F.DataPropertyName = "FOB";
            this.F.HeaderText = "FOB";
            this.F.Name = "F";
            this.F.Visible = false;
            // 
            // PVP
            // 
            this.PVP.DataPropertyName = "PVP";
            this.PVP.HeaderText = "PVP";
            this.PVP.Name = "PVP";
            this.PVP.Visible = false;
            // 
            // Column45
            // 
            this.Column45.DataPropertyName = "LOCALI";
            this.Column45.HeaderText = "Locali";
            this.Column45.Name = "Column45";
            this.Column45.Visible = false;
            // 
            // Column53
            // 
            this.Column53.DataPropertyName = "Column3";
            this.Column53.HeaderText = "qtdEncomenda";
            this.Column53.Name = "Column53";
            this.Column53.Visible = false;
            // 
            // ValorPCL
            // 
            this.ValorPCL.HeaderText = "Valor PCL";
            this.ValorPCL.Name = "ValorPCL";
            // 
            // STK
            // 
            this.STK.HeaderText = "STK";
            this.STK.Name = "STK";
            // 
            // EP
            // 
            this.EP.HeaderText = "CodDoc";
            this.EP.Name = "EP";
            // 
            // Column47
            // 
            this.Column47.HeaderText = "Conta Stock";
            this.Column47.Name = "Column47";
            this.Column47.Visible = false;
            // 
            // Column54
            // 
            this.Column54.HeaderText = "CodAo";
            this.Column54.Name = "Column54";
            this.Column54.Visible = false;
            // 
            // Column55
            // 
            this.Column55.HeaderText = "Opção";
            this.Column55.Name = "Column55";
            this.Column55.Visible = false;
            // 
            // Column56
            // 
            this.Column56.HeaderText = "Índice";
            this.Column56.Name = "Column56";
            this.Column56.Visible = false;
            // 
            // Column57
            // 
            this.Column57.HeaderText = "ÍndiceX";
            this.Column57.Name = "Column57";
            this.Column57.Visible = false;
            // 
            // Column58
            // 
            this.Column58.HeaderText = "IDúnicoGE";
            this.Column58.Name = "Column58";
            this.Column58.Visible = false;
            // 
            // Column59
            // 
            this.Column59.HeaderText = "Linhas";
            this.Column59.Name = "Column59";
            this.Column59.Visible = false;
            // 
            // Column60
            // 
            this.Column60.HeaderText = "ArmazémX";
            this.Column60.Name = "Column60";
            this.Column60.Visible = false;
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(84, 216);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(260, 19);
            this.metroLabel13.TabIndex = 23;
            this.metroLabel13.Text = "Sugestão de Encomendas dos Movimentos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(666, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "%";
            // 
            // txtEnDiasRepor
            // 
            this.txtEnDiasRepor.Lines = new string[] {
        "15"};
            this.txtEnDiasRepor.Location = new System.Drawing.Point(836, 167);
            this.txtEnDiasRepor.MaxLength = 32767;
            this.txtEnDiasRepor.Name = "txtEnDiasRepor";
            this.txtEnDiasRepor.PasswordChar = '\0';
            this.txtEnDiasRepor.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEnDiasRepor.SelectedText = "";
            this.txtEnDiasRepor.Size = new System.Drawing.Size(75, 23);
            this.txtEnDiasRepor.TabIndex = 21;
            this.txtEnDiasRepor.Text = "15";
            this.txtEnDiasRepor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtEnDiasRepor.UseSelectable = true;
            // 
            // txtEnPercentualSaida
            // 
            this.txtEnPercentualSaida.Lines = new string[] {
        "70"};
            this.txtEnPercentualSaida.Location = new System.Drawing.Point(585, 167);
            this.txtEnPercentualSaida.MaxLength = 32767;
            this.txtEnPercentualSaida.Name = "txtEnPercentualSaida";
            this.txtEnPercentualSaida.PasswordChar = '\0';
            this.txtEnPercentualSaida.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEnPercentualSaida.SelectedText = "";
            this.txtEnPercentualSaida.Size = new System.Drawing.Size(75, 23);
            this.txtEnPercentualSaida.TabIndex = 20;
            this.txtEnPercentualSaida.Text = "70";
            this.txtEnPercentualSaida.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtEnPercentualSaida.UseSelectable = true;
            // 
            // txtEnNDias
            // 
            this.txtEnNDias.Lines = new string[] {
        "60"};
            this.txtEnNDias.Location = new System.Drawing.Point(316, 167);
            this.txtEnNDias.MaxLength = 32767;
            this.txtEnNDias.Name = "txtEnNDias";
            this.txtEnNDias.PasswordChar = '\0';
            this.txtEnNDias.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEnNDias.SelectedText = "";
            this.txtEnNDias.Size = new System.Drawing.Size(75, 23);
            this.txtEnNDias.TabIndex = 19;
            this.txtEnNDias.Text = "60";
            this.txtEnNDias.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtEnNDias.UseSelectable = true;
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(724, 171);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(84, 19);
            this.metroLabel12.TabIndex = 18;
            this.metroLabel12.Text = "Dias a Repor";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(452, 171);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(106, 19);
            this.metroLabel11.TabIndex = 17;
            this.metroLabel11.Text = "Percentual Saida";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(216, 171);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(72, 19);
            this.metroLabel10.TabIndex = 16;
            this.metroLabel10.Text = "Nº de Dias";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel9.Location = new System.Drawing.Point(414, 18);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(253, 25);
            this.metroLabel9.TabIndex = 12;
            this.metroLabel9.Text = "Processamento de Encomendas";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lbELoja);
            this.groupBox5.Controls.Add(this.cmbELoja);
            this.groupBox5.Controls.Add(this.lbECusto);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(84, 105);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(843, 98);
            this.groupBox5.TabIndex = 91;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Operações de Sugestão";
            // 
            // lbELoja
            // 
            this.lbELoja.AutoSize = true;
            this.lbELoja.Location = new System.Drawing.Point(465, 34);
            this.lbELoja.Name = "lbELoja";
            this.lbELoja.Size = new System.Drawing.Size(0, 0);
            this.lbELoja.TabIndex = 17;
            // 
            // cmbELoja
            // 
            this.cmbELoja.FormattingEnabled = true;
            this.cmbELoja.IntegralHeight = false;
            this.cmbELoja.ItemHeight = 23;
            this.cmbELoja.Location = new System.Drawing.Point(16, 24);
            this.cmbELoja.Name = "cmbELoja";
            this.cmbELoja.Size = new System.Drawing.Size(443, 29);
            this.cmbELoja.TabIndex = 15;
            this.cmbELoja.UseSelectable = true;
            this.cmbELoja.DropDown += new System.EventHandler(this.cmbELoja_DropDown);
            this.cmbELoja.SelectedIndexChanged += new System.EventHandler(this.cmbELoja_SelectedIndexChanged);
            // 
            // lbECusto
            // 
            this.lbECusto.AutoSize = true;
            this.lbECusto.Location = new System.Drawing.Point(566, 34);
            this.lbECusto.Name = "lbECusto";
            this.lbECusto.Size = new System.Drawing.Size(16, 19);
            this.lbECusto.TabIndex = 98;
            this.lbECusto.Text = "0";
            // 
            // metroTabPage6
            // 
            this.metroTabPage6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage6.Controls.Add(this.toolStrip6);
            this.metroTabPage6.Controls.Add(this.toolStripContainer6);
            this.metroTabPage6.Controls.Add(this.metroGrid3);
            this.metroTabPage6.Controls.Add(this.txtCRIN);
            this.metroTabPage6.Controls.Add(this.metroLabel21);
            this.metroTabPage6.Controls.Add(this.metroLabel20);
            this.metroTabPage6.Controls.Add(this.metroLabel19);
            this.metroTabPage6.Controls.Add(this.metroLabel18);
            this.metroTabPage6.Controls.Add(this.metroLabel16);
            this.metroTabPage6.Controls.Add(this.groupBox6);
            this.metroTabPage6.HorizontalScrollbarBarColor = true;
            this.metroTabPage6.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.HorizontalScrollbarSize = 10;
            this.metroTabPage6.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage6.Name = "metroTabPage6";
            this.metroTabPage6.Size = new System.Drawing.Size(1075, 476);
            this.metroTabPage6.TabIndex = 5;
            this.metroTabPage6.Text = "Consultas";
            this.metroTabPage6.VerticalScrollbarBarColor = true;
            this.metroTabPage6.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage6.VerticalScrollbarSize = 10;
            // 
            // toolStrip6
            // 
            this.toolStrip6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip6.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip6.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btCOk});
            this.toolStrip6.Location = new System.Drawing.Point(507, 55);
            this.toolStrip6.Name = "toolStrip6";
            this.toolStrip6.Size = new System.Drawing.Size(45, 25);
            this.toolStrip6.TabIndex = 89;
            this.toolStrip6.Text = "toolStrip6";
            // 
            // btCOk
            // 
            this.btCOk.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btCOk.Image = ((System.Drawing.Image)(resources.GetObject("btCOk.Image")));
            this.btCOk.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btCOk.Name = "btCOk";
            this.btCOk.Size = new System.Drawing.Size(42, 22);
            this.btCOk.Text = "Ok";
            this.btCOk.Click += new System.EventHandler(this.btCOk_Click);
            // 
            // toolStripContainer6
            // 
            // 
            // toolStripContainer6.ContentPanel
            // 
            this.toolStripContainer6.ContentPanel.Size = new System.Drawing.Size(81, 2);
            this.toolStripContainer6.Location = new System.Drawing.Point(507, 55);
            this.toolStripContainer6.Name = "toolStripContainer6";
            this.toolStripContainer6.Size = new System.Drawing.Size(81, 27);
            this.toolStripContainer6.TabIndex = 90;
            this.toolStripContainer6.Text = "toolStripContainer6";
            // 
            // toolStripContainer6.TopToolStripPanel
            // 
            this.toolStripContainer6.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer6.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer6.TopToolStripPanel.Tag = "XC";
            // 
            // metroGrid3
            // 
            this.metroGrid3.AllowUserToResizeRows = false;
            this.metroGrid3.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle43;
            this.metroGrid3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IDRI,
            this.NrRI,
            this.DataRI,
            this.OperadorRI,
            this.EstadoRI,
            this.IDSaida,
            this.NSaida,
            this.Doc,
            this.Operador,
            this.Estado});
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid3.DefaultCellStyle = dataGridViewCellStyle44;
            this.metroGrid3.EnableHeadersVisualStyles = false;
            this.metroGrid3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid3.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGrid3.Location = new System.Drawing.Point(3, 226);
            this.metroGrid3.Name = "metroGrid3";
            this.metroGrid3.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle45.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid3.RowHeadersDefaultCellStyle = dataGridViewCellStyle45;
            this.metroGrid3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid3.Size = new System.Drawing.Size(1069, 199);
            this.metroGrid3.TabIndex = 24;
            // 
            // txtCRIN
            // 
            this.txtCRIN.Lines = new string[0];
            this.txtCRIN.Location = new System.Drawing.Point(927, 149);
            this.txtCRIN.MaxLength = 32767;
            this.txtCRIN.Name = "txtCRIN";
            this.txtCRIN.PasswordChar = '\0';
            this.txtCRIN.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCRIN.SelectedText = "";
            this.txtCRIN.Size = new System.Drawing.Size(112, 23);
            this.txtCRIN.TabIndex = 22;
            this.txtCRIN.UseSelectable = true;
            // 
            // metroLabel21
            // 
            this.metroLabel21.AutoSize = true;
            this.metroLabel21.Location = new System.Drawing.Point(863, 153);
            this.metroLabel21.Name = "metroLabel21";
            this.metroLabel21.Size = new System.Drawing.Size(43, 19);
            this.metroLabel21.TabIndex = 19;
            this.metroLabel21.Text = "R.I Nº";
            // 
            // metroLabel20
            // 
            this.metroLabel20.AutoSize = true;
            this.metroLabel20.Location = new System.Drawing.Point(454, 149);
            this.metroLabel20.Name = "metroLabel20";
            this.metroLabel20.Size = new System.Drawing.Size(33, 19);
            this.metroLabel20.TabIndex = 18;
            this.metroLabel20.Text = "Loja";
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(251, 153);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(67, 19);
            this.metroLabel19.TabIndex = 17;
            this.metroLabel19.Text = "Data Final";
            // 
            // metroLabel18
            // 
            this.metroLabel18.AutoSize = true;
            this.metroLabel18.Location = new System.Drawing.Point(23, 153);
            this.metroLabel18.Name = "metroLabel18";
            this.metroLabel18.Size = new System.Drawing.Size(72, 19);
            this.metroLabel18.TabIndex = 16;
            this.metroLabel18.Text = "Data Inicial";
            // 
            // metroLabel16
            // 
            this.metroLabel16.AutoSize = true;
            this.metroLabel16.Location = new System.Drawing.Point(3, 204);
            this.metroLabel16.Name = "metroLabel16";
            this.metroLabel16.Size = new System.Drawing.Size(191, 19);
            this.metroLabel16.TabIndex = 14;
            this.metroLabel16.Text = "Dados da RI e do Atendimento";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtCDataFinal);
            this.groupBox6.Controls.Add(this.txtCDataInicial);
            this.groupBox6.Controls.Add(this.cmbCLoja);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(3, 133);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1054, 49);
            this.groupBox6.TabIndex = 91;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Dados da Requisição";
            // 
            // txtCDataFinal
            // 
            this.txtCDataFinal.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtCDataFinal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCDataFinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCDataFinal.Location = new System.Drawing.Point(330, 19);
            this.txtCDataFinal.Mask = "00/00/0000";
            this.txtCDataFinal.Name = "txtCDataFinal";
            this.txtCDataFinal.Size = new System.Drawing.Size(82, 20);
            this.txtCDataFinal.TabIndex = 92;
            this.txtCDataFinal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCDataFinal.ValidatingType = typeof(System.DateTime);
            this.txtCDataFinal.Leave += new System.EventHandler(this.txtCDataFinal_Leave);
            // 
            // txtCDataInicial
            // 
            this.txtCDataInicial.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtCDataInicial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCDataInicial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCDataInicial.Location = new System.Drawing.Point(118, 20);
            this.txtCDataInicial.Mask = "00/00/0000";
            this.txtCDataInicial.Name = "txtCDataInicial";
            this.txtCDataInicial.Size = new System.Drawing.Size(82, 20);
            this.txtCDataInicial.TabIndex = 64;
            this.txtCDataInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCDataInicial.ValidatingType = typeof(System.DateTime);
            this.txtCDataInicial.Leave += new System.EventHandler(this.txtCDataInicial_Leave);
            // 
            // cmbCLoja
            // 
            this.cmbCLoja.FormattingEnabled = true;
            this.cmbCLoja.IntegralHeight = false;
            this.cmbCLoja.ItemHeight = 23;
            this.cmbCLoja.Location = new System.Drawing.Point(490, 14);
            this.cmbCLoja.Name = "cmbCLoja";
            this.cmbCLoja.Size = new System.Drawing.Size(364, 29);
            this.cmbCLoja.TabIndex = 23;
            this.cmbCLoja.UseSelectable = true;
            this.cmbCLoja.SelectedIndexChanged += new System.EventHandler(this.cmbCLoja_SelectedIndexChanged);
            this.cmbCLoja.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cmbCLoja_MouseClick);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // IDRI
            // 
            this.IDRI.DataPropertyName = "IDUnico";
            this.IDRI.HeaderText = "ID RI";
            this.IDRI.Name = "IDRI";
            // 
            // NrRI
            // 
            this.NrRI.DataPropertyName = "NumDoc";
            this.NrRI.HeaderText = "Nr. RI";
            this.NrRI.Name = "NrRI";
            // 
            // DataRI
            // 
            this.DataRI.DataPropertyName = "Column1";
            this.DataRI.HeaderText = "Data RI";
            this.DataRI.Name = "DataRI";
            // 
            // OperadorRI
            // 
            this.OperadorRI.DataPropertyName = "NOMEUTILIZADOR";
            this.OperadorRI.HeaderText = "Operador RI";
            this.OperadorRI.Name = "OperadorRI";
            // 
            // EstadoRI
            // 
            this.EstadoRI.DataPropertyName = "STATUS";
            this.EstadoRI.HeaderText = "Estado RI";
            this.EstadoRI.Name = "EstadoRI";
            // 
            // IDSaida
            // 
            this.IDSaida.DataPropertyName = "Column2";
            this.IDSaida.HeaderText = "ID Saida";
            this.IDSaida.Name = "IDSaida";
            // 
            // NSaida
            // 
            this.NSaida.DataPropertyName = "Column3";
            this.NSaida.HeaderText = "Nº Saida";
            this.NSaida.Name = "NSaida";
            // 
            // Doc
            // 
            this.Doc.DataPropertyName = "CODDOC";
            this.Doc.HeaderText = "Doc";
            this.Doc.Name = "Doc";
            // 
            // Operador
            // 
            this.Operador.DataPropertyName = "Column5";
            this.Operador.HeaderText = "Operador";
            this.Operador.Name = "Operador";
            // 
            // Estado
            // 
            this.Estado.DataPropertyName = "ESTADO";
            this.Estado.HeaderText = "Estado";
            this.Estado.Name = "Estado";
            // 
            // FormRequisicaoInterna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1107, 597);
            this.ControlBox = false;
            this.Controls.Add(this.btC);
            this.Name = "FormRequisicaoInterna";
            this.Text = "Requisição Interna";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormRequisicaoInterna_Load);
            this.btC.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.toolStripContainer2.ResumeLayout(false);
            this.toolStripContainer2.PerformLayout();
            this.metroTabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.toolStripContainer3.ResumeLayout(false);
            this.toolStripContainer3.PerformLayout();
            this.metroTabPage3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.b.ResumeLayout(false);
            this.b.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.metroTabPage4.ResumeLayout(false);
            this.metroTabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgFicha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgModelo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgMarca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgSubFam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgArma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgLoja)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgArt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgL1)).EndInit();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            this.toolStripContainer4.ResumeLayout(false);
            this.toolStripContainer4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEntradaGuia)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.metroTabPage5.ResumeLayout(false);
            this.metroTabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEncomendas2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgEncomendas1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer7.ResumeLayout(false);
            this.toolStripContainer7.PerformLayout();
            this.toolStrip5.ResumeLayout(false);
            this.toolStrip5.PerformLayout();
            this.toolStripContainer5.ResumeLayout(false);
            this.toolStripContainer5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEncomendas)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.metroTabPage6.ResumeLayout(false);
            this.metroTabPage6.PerformLayout();
            this.toolStrip6.ResumeLayout(false);
            this.toolStrip6.PerformLayout();
            this.toolStripContainer6.ResumeLayout(false);
            this.toolStripContainer6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid3)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl btC;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroTabPage metroTabPage4;
        private MetroFramework.Controls.MetroTabPage metroTabPage5;
        private MetroFramework.Controls.MetroTabPage metroTabPage6;
        private MetroFramework.Controls.MetroComboBox cmbLoja;
        private MetroFramework.Controls.MetroComboBox cmbLoja2;
        private MetroFramework.Controls.MetroComboBox cmbLoja3;
        private MetroFramework.Controls.MetroGrid dgEntradaGuia;
        private MetroFramework.Controls.MetroComboBox cmbExIDGI;
        private MetroFramework.Controls.MetroComboBox cmbExEntidadeFilial;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroGrid dgEncomendas;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private System.Windows.Forms.Label label3;
        private MetroFramework.Controls.MetroTextBox txtEnDiasRepor;
        private MetroFramework.Controls.MetroTextBox txtEnPercentualSaida;
        private MetroFramework.Controls.MetroTextBox txtEnNDias;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroComboBox cmbELoja;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroGrid metroGrid3;
        private MetroFramework.Controls.MetroComboBox cmbCLoja;
        private MetroFramework.Controls.MetroTextBox txtCRIN;
        private MetroFramework.Controls.MetroLabel metroLabel21;
        private MetroFramework.Controls.MetroLabel metroLabel20;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel18;
        private MetroFramework.Controls.MetroLabel metroLabel16;
        private System.Windows.Forms.ToolStrip b;
        private System.Windows.Forms.ToolStripButton btReGravar;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton btROk;
        private System.Windows.Forms.ToolStripContainer toolStripContainer2;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton btAOk;
        private System.Windows.Forms.ToolStripContainer toolStripContainer3;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripButton btEExportacao;
        private System.Windows.Forms.ToolStripContainer toolStripContainer4;
        private System.Windows.Forms.ToolStrip toolStrip5;
        private System.Windows.Forms.ToolStripButton btEnCalcular;
        private System.Windows.Forms.ToolStripButton btEnCriar;
        private System.Windows.Forms.ToolStripContainer toolStripContainer5;
        private System.Windows.Forms.ToolStrip toolStrip6;
        private System.Windows.Forms.ToolStripButton btCOk;
        private System.Windows.Forms.ToolStripContainer toolStripContainer6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btAtualizar;
        private System.Windows.Forms.ToolStripContainer toolStripContainer7;
        private MetroFramework.Controls.MetroLabel lbfuncao;
        private MetroFramework.Controls.MetroLabel lbNumero;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mercadoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn Referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn Descricao;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtd;
        private System.Windows.Forms.DataGridViewTextBoxColumn FOB;
        private System.Windows.Forms.DataGridViewTextBoxColumn PCL;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private MetroFramework.Controls.MetroGrid dgL1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private MetroFramework.Controls.MetroGrid dgDetail;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column27;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column28;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column29;
        private MetroFramework.Controls.MetroGrid dgArt;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column30;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column32;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column33;
        private MetroFramework.Controls.MetroGrid dgLoja;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private MetroFramework.Controls.MetroGrid dgArma;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn65;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn66;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn67;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn68;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn69;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn70;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn71;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn72;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn73;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn74;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn75;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn76;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn77;
        private MetroFramework.Controls.MetroGrid dgFam;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn78;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn79;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn80;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn81;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn82;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn83;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn84;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn85;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn86;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn87;
        private MetroFramework.Controls.MetroGrid dgSubFam;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn88;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn89;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn90;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn91;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn92;
        private MetroFramework.Controls.MetroGrid dgMarca;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn93;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn94;
        private MetroFramework.Controls.MetroGrid dgModelo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn95;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn96;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column34;
        private MetroFramework.Controls.MetroGrid dgFicha;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn97;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn98;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn99;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column35;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column36;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column37;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column38;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column39;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column40;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column41;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column42;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column43;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column44;
        private MetroFramework.Controls.MetroLabel lbTotalSelecionado;
        private MetroFramework.Controls.MetroLabel lbTotalEncomendar;
        private MetroFramework.Controls.MetroLabel lbItens;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel lbELoja;
        private MetroFramework.Controls.MetroLabel lbECusto;
        private MetroFramework.Controls.MetroGrid dgEncomendas2;
        private MetroFramework.Controls.MetroGrid dgEncomendas1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private MetroFramework.Controls.MetroLabel label2;
        private System.Windows.Forms.MaskedTextBox txtExDataFinal;
        private System.Windows.Forms.MaskedTextBox txtExDataInicial;
        private System.Windows.Forms.MaskedTextBox txtCDataFinal;
        private System.Windows.Forms.MaskedTextBox txtCDataInicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column48;
        private System.Windows.Forms.DataGridViewTextBoxColumn LOJA2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column46;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column49;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column50;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column51;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column52;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Col6;
        private System.Windows.Forms.DataGridViewTextBoxColumn F;
        private System.Windows.Forms.DataGridViewTextBoxColumn PVP;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column45;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column53;
        private System.Windows.Forms.DataGridViewTextBoxColumn ValorPCL;
        private System.Windows.Forms.DataGridViewTextBoxColumn STK;
        private System.Windows.Forms.DataGridViewTextBoxColumn EP;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column47;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column54;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column55;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column56;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column57;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column58;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column59;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn100;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn101;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn102;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn103;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn104;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn105;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn106;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn107;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn108;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn109;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn110;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn112;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn113;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn115;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn116;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn117;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn118;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn119;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn120;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn121;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn123;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn111;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn114;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn122;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn124;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn125;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn126;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn127;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn128;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn129;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn130;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn131;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn132;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn133;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn134;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn135;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn136;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn137;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn138;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn139;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn140;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn141;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn142;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn143;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn144;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDRI;
        private System.Windows.Forms.DataGridViewTextBoxColumn NrRI;
        private System.Windows.Forms.DataGridViewTextBoxColumn DataRI;
        private System.Windows.Forms.DataGridViewTextBoxColumn OperadorRI;
        private System.Windows.Forms.DataGridViewTextBoxColumn EstadoRI;
        private System.Windows.Forms.DataGridViewTextBoxColumn IDSaida;
        private System.Windows.Forms.DataGridViewTextBoxColumn NSaida;
        private System.Windows.Forms.DataGridViewTextBoxColumn Doc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Operador;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estado;
    }
}