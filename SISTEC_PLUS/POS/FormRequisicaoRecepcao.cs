﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormRequisicaoRecepcao : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        DateTime data;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand comand, comandUp;  // definicao do comando sql command
        int a = 0;
        string nomeLoja, sql, codArmaz, codAO, sCodArmzDestino, nPrgSTK;
        string sTName, id, existe, sStatus, sMovimentoET, nIDUnicoET, x1;
        int nFactura, nNumdocET, numero;
        double nPreco;
        string pcl, pvp, fob, pclD, pvpD,a1,d;
        public FormRequisicaoRecepcao()
        {
            InitializeComponent();
        }
        private void gravar(int tamanho)
        {
            sTName = "T_INS_RECP_LOJA";
            nFactura = 0;
            nPreco = 0;
            //nUnico = "";
            nNumdocET = numeroDoc();
            nIDUnicoET = idUnico();
            
            //apagarLinhasNoGrid();
            if (sCodArmzDestino == null || sCodArmzDestino == "")
                gravarIf(tamanho);
            else
                gravarElse(tamanho);
        }
        private void gravarIf(int tamanho)
        {
            SqlCommand[] comd = new SqlCommand[tamanho];
            SqlCommand[] comd1 = new SqlCommand[tamanho];
            SqlCommand comd2 = new SqlCommand();
            SqlCommand[] comd3 = new SqlCommand[tamanho];
            SqlCommand[] comd4 = new SqlCommand[tamanho];
            SqlCommand[] comd5 = new SqlCommand[tamanho];
            SqlCommand[] comd6 = new SqlCommand[tamanho];

              for (int i = 0; i < tamanho; i++)
                {
                    pcl = "" + Convert.ToDouble(metroGrid1[12, i].Value);
                    pvp = "" + Convert.ToDouble(metroGrid1[13, i].Value);
                    fob = "" + Convert.ToDouble(metroGrid1[9, i].Value);
                    pclD = "" + Convert.ToDouble(metroGrid1[10, i].Value);
                    pvpD = "" + Convert.ToDouble(metroGrid1[11, i].Value);
                  string nPMC;
                    if (metroGrid1[6, i].Value == null)
                        x1 = "0";
                    else
                        x1 = metroGrid1[6, i].Value.ToString();
                    //MessageBox.Show(lbLoj.Text + " - " + metroGrid1[7, i].Value + " - " + metroGrid1[2, i].Value);
                    SqlCommand comand1 = new SqlCommand("SELECT CODLOJA, CODARMZ, REFERENC FROM ASMESTRE WITH (NOLOCK) Where CodLoja = '" + lbLoj.Text + "' and CodArmz = '" + metroGrid1[7, i].Value + "' and Referenc = '" + metroGrid1[2, i].Value + "'", conexao);
                    conexao.Open();
                    try
                    {
                        using (SqlDataReader reader = comand1.ExecuteReader())
                        { while (reader.Read()) { a1 = reader[0].ToString(); d = reader[1].ToString(); existe = reader[2].ToString(); } } 
                    }
                    catch (Exception) { existe = ""; }
                    finally { conexao.Close(); }
                        //MessageBox.Show(nIDUnicoET);
                    if (existe != "" && existe != null)
                    {
                        nPMC = ""+calculaPMC(metroGrid1[7, i].Value.ToString(), metroGrid1[2, i].Value.ToString(), Convert.ToDouble(pclD), Convert.ToInt32(metroGrid1[4, i].Value));
                        //MessageBox.Show("" + nPMC);
                        //***CASO A REFERÊNCIA JÁ EXISTA NO ARMAZÉM RECEPTOR, PROCEDER O CÁLCULO DO PMC ********* BEM COMO O UPDATE DA QTE, PCA, FOB E PMC - ASMESTRE********
                        comd[i] = conexao.CreateCommand();
                        comd[i].CommandText = "UPDATE ASMESTRE SET PMC = '" + nPMC.Replace(',', '.') + "', LOCALI= '" + metroGrid1[1, i].Value + "', FOB = '" + fob.Replace(',', '.') + "', PCA = '" + pclD.Replace(',', '.') + "', PVP = '" + pvpD.Replace(',', '.') + "' " +
                            "Where CODLOJA = '" + lbLoj.Text + "' AND CODARMZ = '" + metroGrid1[7, i].Value + "' AND REFERENC = '" + metroGrid1[2, i].Value + "'";
                        //MessageBox.Show("if");
                    }
                    else
                    {
                        //MessageBox.Show("else");
                        //***CASO A REFERÊNCIA NÃO EXISTA NO ARMAZÉM RECEPTOR, PROCEDER A CRIAÇÃO DA MESMA, ********* À IMAGEM E SEMELHANÇA DO QUE SE ENCONTRA NO ARMAZÉM RECEPTOR ********
                        comd1[i] = conexao.CreateCommand();
                        comd1[i].CommandText = "INSERT INTO ASMESTRE (REFERENC,           CODLOJA,                                   CODARMZ,                          CODFAM,                            CODSUBFAM,                       DESCRARTIGO,         DESCR_SUP, CODBAR, QTD,     FOB,                            PCA,                               PVP,                                       LOCALI,            DATACRIA, TEMPO, MTBF, OBS2, CAMBIO, FORNEC2, CARA, PONENC, UNIDADE, DESEMBALAGEM, QUAEMBALAGEM, TARA, DESCMAX, DESCMIN, PERCQUEBRA, GUARDAR, ALTERAPRECO, CONTASTOCKS, STOCKNEGATIVO, DEVOLVER, NIVELDESC, STOCKCONSIG, STOCKMAX, STOCKMIN, ImpostoConsumo, STOCKTOTAL, VAL_ENT, VAL_SAIDO, FORNEC, OBS, data_fabrico, PROMOCAO_I, PROMOCAO_F,                  PMC,             IMAGEM, ANULADO, NORMA, COD_GRUPO, REGEQUIP,          CODUTILIZADOR,           CODSECCAO, Data_Expira, Lucro, Permitir_Obs, Activo, PONTOENCOMENDA, CODMARCA, CODMODELO) " +
                                 "SELECT '" + metroGrid1[2, i].Value + "', '" + lbLoj.Text + "', '" + metroGrid1[7, i].Value + "', '" + metroGrid1[18, i].Value + "', '" + metroGrid1[25, i].Value + "', '" + metroGrid1[3, i].Value + "', DESCR_SUP, CODBAR, 0, '" + fob.Replace(',', '.') + "', '" + pclD.Replace(',', '.') + "', '" + pvpD.Replace(',', '.') + "', '" + metroGrid1[1, i].Value + "', DataCria, TEMPO, MTBF, OBS2, CAMBIO, FORNEC2, CARA, PONENC, UNIDADE, DESEMBALAGEM, QUAEMBALAGEM, TARA, DESCMAX, DESCMIN, PERCQUEBRA, GUARDAR, ALTERAPRECO, CONTASTOCKS, STOCKNEGATIVO, DEVOLVER, NIVELDESC, STOCKCONSIG, STOCKMAX, STOCKMIN, ImpostoConsumo, STOCKTOTAL, VAL_ENT, VAL_SAIDO, FORNEC, OBS, Data_Fabrico, PROMOCAO_I, PROMOCAO_F, '" + metroGrid1[5, i].Value + "', IMAGEM, ANULADO, NORMA, COD_GRUPO, REGEQUIP, '" + Variavel.codUtilizador + "', CODSECCAO, Data_Expira, Lucro, Permitir_Obs, Activo, PONTOENCOMENDA, CODMARCA, CODMODELO " +
                                 "FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA = '" + metroGrid1[15, i].Value + "' AND CODARMZ = '" + metroGrid1[17, i].Value + "' AND REFERENC = '" + metroGrid1[2, i].Value + "'";
                        
                        sStatus = "E";
                        sMovimentoET = "+";
                        comd2 = conexao.CreateCommand();
                        comd2.CommandText = "INSERT INTO AREGDOC (IDUNICO, NUMDOC,                           NUMFACT,                      CODDOC,                        CodEntidade,                         CODUTILIZADOR,                      DATACRIA,             PRECO, PRECOD,      CODLOJA,                       IDORIG,                        DATA_LANCAMENTO,             STATUS) " +
                            "Values ('" + nIDUnicoET + "', '" + nNumdocET + "', '" + nNumdocET + "', '" + Gerais.PARAM_CODDOC_ER + "', '" + Gerais.PARAM_CODCLI_GERAL + "', '" + Variavel.codUtilizador + "', CONVERT(VARCHAR(20),GETDATE(),120), 0,      0, '" + lbLoj.Text + "', '" + metroGrid1[26, i].Value + "', CONVERT(VARCHAR(10),GETDATE(),120), '" + sStatus + "')";

                        comd3[i] = conexao.CreateCommand();
                        comd3[i].CommandText = "INSERT INTO ASFICMOV1 (IDUNICO, NUMDOC,              GUIA,                          DATACRIA,                            DATA_LANCAMENTO,             CODLOJA,                      CODARMZ,                          REFERENC,                FOB,                             PCLD,                             PVPD,                             PCL,                             PVP,                                        QTD,           ANULADO,             CodEntidade,                        CODDOC,                     TIPMOV,                    CODUTILIZADOR,                          CODAO,                 X1)" +
                                 "Values ('" + nIDUnicoET + "', '" + nNumdocET + "', '" + metroGrid1[27, i].Value + "', CONVERT(VARCHAR(20),GETDATE(),120), CONVERT(VARCHAR(10),GETDATE(),120), '" + lbLoj.Text + "', '" + metroGrid1[7, i].Value + "', '" + metroGrid1[2, i].Value + "', '" + fob.Replace(',', '.') + "', '" + pclD.Replace(',', '.') + "', '" + pvpD.Replace(',', '.') + "', '" + pcl.Replace(',', '.') + "', '" + pvp.Replace(',', '.') + "', '" + metroGrid1[4, i].Value + "','Z', '" + Gerais.PARAM_CODCLI_GERAL + "', '" + Gerais.PARAM_CODDOC_ER + "', '" + sMovimentoET + "', '" + Variavel.codUtilizador + "', '" + metroGrid1[8, i].Value + "', '" + x1 + "')";

                        comd4[i] = conexao.CreateCommand();
                        comd4[i].CommandText = "UPDATE ASFICMOV1 SET ANULADO = 'W' WHERE LTRIM(RTRIM(IDUNICO)) = '" + metroGrid1[26, i].Value + "' AND LTRIM(RTRIM(CODLOJA)) = '" + metroGrid1[15, i].Value + "' AND LTRIM(RTRIM(CODARMZ)) = '" + metroGrid1[7, i].Value + "' AND LTRIM(RTRIM(REFERENC)) = '" + metroGrid1[2, i].Value + "' AND CODDOC= '" + Gerais.PARAM_CODDOC_SR + "'";
                        
                        if (metroGrid1[22, i].Value == "S")
                        {
                            comd5[i] = conexao.CreateCommand();
                            comd5[i].CommandText = "UPDATE AEFICHAS SET CODARMZ = '" + metroGrid1[7, i].Value + "', Localizacao = '" + metroGrid1[7, i].Value + "', Data_Alteracao = Convert(Varchar(20),GetDate(),120), Alterado_Por = '" + Variavel.codUtilizador + "' Where IDUNICO = '" + metroGrid1[26, i].Value + "'";

                            comd6[i] = conexao.CreateCommand();
                            comd6[i].CommandText = "INSERT Into AEFICMOV (IDUNICO, CODDOC,           CODMARCA, CODMODELO, NSERIE, CODLOJA, REFERENC, CODARMZ, CODENTIDADE,                 DataCria,                    CodUtilizador)" +
                                     "Select '" + nIDUnicoET + "', '" + Gerais.PARAM_CODDOC_ER + "', CODMARCA, CODMODELO, NSERIE, CODLOJA, REFERENC, CODARMZ, CODENTIDADE, Convert(Varchar(20),GetDate(),120), '" + Variavel.codUtilizador + "' From Aefichas Where IDunico = '" + metroGrid1[26, i].Value + "'";
                        }
                    }
            }
            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try{
                for (int i = 0; i < tamanho; i++) 
                {
                    comand = new SqlCommand("SELECT CODLOJA, CODARMZ, REFERENC FROM ASMESTRE WITH (NOLOCK) Where CodLoja = '" + lbLoj.Text + "' and CodArmz = '" + metroGrid1[7, i].Value + "' and Referenc = '" + metroGrid1[2, i].Value + "'", conexao);
                    try
                    {
                        comand.Transaction = tran;
                        using (SqlDataReader reader = comand.ExecuteReader())
                        { while (reader.Read()) { existe = reader[2].ToString(); } }
                    }
                    catch (Exception) { existe = ""; }
                        //MessageBox.Show(existe);
                    if (existe != "" && existe != null)
                    {
                        comd[i].Transaction = tran;
                        comd[i].ExecuteNonQuery();
                    }
                    else
                    {
                        comd1[i].Transaction = tran;
                        comd1[i].ExecuteNonQuery();
                        try
                        {
                            comd2.Transaction = tran;
                            comd2.ExecuteNonQuery();
                        }
                        catch (SqlException) { }
                        try
                        {
                            comd3[i].Transaction = tran;
                            comd3[i].ExecuteNonQuery();
                        }
                        catch (SqlException) { }
                        try
                        {
                            comd4[i].Transaction = tran;
                            comd4[i].ExecuteNonQuery();
                        }
                        catch (SqlException) { }
                        if (metroGrid1[22, i].Value == "S")
                        {
                            comd5[i].Transaction = tran;
                            comd5[i].ExecuteNonQuery();
                            comd6[i].Transaction = tran;
                            comd6[i].ExecuteNonQuery();
                        }
                    }
                }
                tran.Commit();
                MessageBox.Show("Operacão realizada com sucesso!");
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void gravarElse(int tamanho)
        {
            SqlCommand[] comd = new SqlCommand[tamanho];
            SqlCommand[] comd1 = new SqlCommand[tamanho];
            SqlCommand comd2 = new SqlCommand();
            SqlCommand[] comd3 = new SqlCommand[tamanho];
            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            for (int i = 0; i < tamanho; i++)
            {
                pcl = "" + Convert.ToDouble(metroGrid1[12, i].Value);
                pvp = "" + Convert.ToDouble(metroGrid1[13, i].Value);
                fob = "" + Convert.ToDouble(metroGrid1[9, i].Value);
                pclD = "" + Convert.ToDouble(metroGrid1[10, i].Value);
                pvpD = "" + Convert.ToDouble(metroGrid1[11, i].Value);
                string nPMC;
                if (metroGrid1[6, i].Value == null)
                    x1 = "0";
                else
                    x1 = metroGrid1[6, i].Value.ToString();
                //MessageBox.Show(lbLoj.Text + " - " + metroGrid1[7, i].Value + " - " + metroGrid1[2, i].Value);
                SqlCommand comand1 = new SqlCommand("SELECT CODLOJA, CODARMZ, REFERENC FROM ASMESTRE WITH (NOLOCK) Where CodLoja = '" + lbLoj.Text + "' and CodArmz = '" + metroGrid1[7, i].Value + "' and Referenc = '" + metroGrid1[2, i].Value + "'", conexao);
                conexao.Open();
                try
                {
                    using (SqlDataReader reader = comand1.ExecuteReader())
                    { while (reader.Read()) { a1 = reader[0].ToString(); d = reader[1].ToString(); existe = reader[2].ToString(); } }
                }
                catch (Exception) { existe = ""; }
                finally { conexao.Close(); }
                //MessageBox.Show(nIDUnicoET);

                if (existe != "" && existe != null)
                {
                    nPMC = "" + calculaPMC(metroGrid1[7, i].Value.ToString(), metroGrid1[2, i].Value.ToString(), Convert.ToDouble(pclD), Convert.ToInt32(metroGrid1[4, i].Value));
                    //MessageBox.Show("" + nPMC);
                    //***CASO A REFERÊNCIA JÁ EXISTA NO ARMAZÉM RECEPTOR, PROCEDER O CÁLCULO DO PMC ********* BEM COMO O UPDATE DA QTE, PCA, FOB E PMC - ASMESTRE********
                    comd[i] = conexao.CreateCommand();
                    comd[i].CommandText = "UPDATE ASMESTRE SET PMC = '" + nPMC.Replace(',', '.') + "', LOCALI= '" + metroGrid1[1, i].Value + "', FOB = '" + fob.Replace(',', '.') + "', PCA = '" + pclD.Replace(',', '.') + "', PVP = '" + pvpD.Replace(',', '.') + "' " +
                        "Where CODLOJA = '" + lbLoj.Text + "' AND CODARMZ = '" + metroGrid1[7, i].Value + "' AND REFERENC = '" + metroGrid1[2, i].Value + "'";
                    //MessageBox.Show("if");
                }
                else
                {
                    //MessageBox.Show("else");
                    //***CASO A REFERÊNCIA NÃO EXISTA NO ARMAZÉM RECEPTOR, PROCEDER A CRIAÇÃO DA MESMA, ********* À IMAGEM E SEMELHANÇA DO QUE SE ENCONTRA NO ARMAZÉM RECEPTOR ********
                    comd1[i] = conexao.CreateCommand();
                    comd1[i].CommandText = "INSERT INTO ASMESTRE (REFERENC,           CODLOJA,                                   CODARMZ,                          CODFAM,                            CODSUBFAM,                       DESCRARTIGO,         DESCR_SUP, CODBAR, QTD,     FOB,                            PCA,                               PVP,                                       LOCALI,            DATACRIA, TEMPO, MTBF, OBS2, CAMBIO, FORNEC2, CARA, PONENC, UNIDADE, DESEMBALAGEM, QUAEMBALAGEM, TARA, DESCMAX, DESCMIN, PERCQUEBRA, GUARDAR, ALTERAPRECO, CONTASTOCKS, STOCKNEGATIVO, DEVOLVER, NIVELDESC, STOCKCONSIG, STOCKMAX, STOCKMIN, ImpostoConsumo, STOCKTOTAL, VAL_ENT, VAL_SAIDO, FORNEC, OBS, data_fabrico, PROMOCAO_I, PROMOCAO_F,                  PMC,             IMAGEM, ANULADO, NORMA, COD_GRUPO, REGEQUIP,          CODUTILIZADOR,           CODSECCAO, Data_Expira, Lucro, Permitir_Obs, Activo, PONTOENCOMENDA, CODMARCA, CODMODELO) " +
                             "SELECT '" + metroGrid1[2, i].Value + "', '" + lbLoj.Text + "', '" + metroGrid1[7, i].Value + "', '" + metroGrid1[18, i].Value + "', '" + metroGrid1[25, i].Value + "', '" + metroGrid1[3, i].Value + "', DESCR_SUP, CODBAR, 0, '" + fob.Replace(',', '.') + "', '" + pclD.Replace(',', '.') + "', '" + pvpD.Replace(',', '.') + "', '" + metroGrid1[1, i].Value + "', DataCria, TEMPO, MTBF, OBS2, CAMBIO, FORNEC2, CARA, PONENC, UNIDADE, DESEMBALAGEM, QUAEMBALAGEM, TARA, DESCMAX, DESCMIN, PERCQUEBRA, GUARDAR, ALTERAPRECO, CONTASTOCKS, STOCKNEGATIVO, DEVOLVER, NIVELDESC, STOCKCONSIG, STOCKMAX, STOCKMIN, ImpostoConsumo, STOCKTOTAL, VAL_ENT, VAL_SAIDO, FORNEC, OBS, Data_Fabrico, PROMOCAO_I, PROMOCAO_F, '" + metroGrid1[5, i].Value + "', IMAGEM, ANULADO, NORMA, COD_GRUPO, REGEQUIP, '" + Variavel.codUtilizador + "', CODSECCAO, Data_Expira, Lucro, Permitir_Obs, Activo, PONTOENCOMENDA, CODMARCA, CODMODELO " +
                             "FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA = '" + metroGrid1[15, i].Value + "' AND CODARMZ = '" + metroGrid1[17, i].Value + "' AND REFERENC = '" + metroGrid1[2, i].Value + "'";

                    sStatus = "E";
                    sMovimentoET = "+";
                    comd2 = conexao.CreateCommand();
                    comd2.CommandText = "INSERT INTO AREGDOC (IDUNICO, NUMDOC,                           NUMFACT,                      CODDOC,                        CodEntidade,                         CODUTILIZADOR,                      DATACRIA,             PRECO, PRECOD,      CODLOJA,                       IDORIG,                        DATA_LANCAMENTO,             STATUS) " +
                        "Values ('" + nIDUnicoET + "', '" + nNumdocET + "', '" + nNumdocET + "', '" + Gerais.PARAM_CODDOC_ER + "', '" + Gerais.PARAM_CODCLI_GERAL + "', '" + Variavel.codUtilizador + "', CONVERT(VARCHAR(20),GETDATE(),120), 0,      0, '" + lbLoj.Text + "', '" + metroGrid1[26, i].Value + "', CONVERT(VARCHAR(10),GETDATE(),120), '" + sStatus + "')";

                    comd3[i] = conexao.CreateCommand();
                    comd3[i].CommandText = "INSERT INTO ASFICMOV1 (IDUNICO, NUMDOC,              GUIA,                          DATACRIA,                            DATA_LANCAMENTO,             CODLOJA,                      CODARMZ,                          REFERENC,                FOB,                             PCLD,                             PVPD,                             PCL,                             PVP,                                        QTD,           ANULADO,             CodEntidade,                        CODDOC,                     TIPMOV,                    CODUTILIZADOR,                          CODAO,                 X1)" +
                             "Values ('" + nIDUnicoET + "', '" + nNumdocET + "', '" + metroGrid1[27, i].Value + "', CONVERT(VARCHAR(20),GETDATE(),120), CONVERT(VARCHAR(10),GETDATE(),120), '" + lbLoj.Text + "', '" + metroGrid1[7, i].Value + "', '" + metroGrid1[2, i].Value + "', '" + fob.Replace(',', '.') + "', '" + pclD.Replace(',', '.') + "', '" + pvpD.Replace(',', '.') + "', '" + pcl.Replace(',', '.') + "', '" + pvp.Replace(',', '.') + "', '" + metroGrid1[4, i].Value + "','Z', '" + Gerais.PARAM_CODCLI_GERAL + "', '" + Gerais.PARAM_CODDOC_ER + "', '" + sMovimentoET + "', '" + Variavel.codUtilizador + "', '" + metroGrid1[8, i].Value + "', '" + x1 + "')";
                }
            }
            conexao.Open();
            SqlTransaction transac = conexao.BeginTransaction();
            try
            {
                for (int i = 0; i < tamanho; i++)
                {
                    comand = new SqlCommand("SELECT CODLOJA, CODARMZ, REFERENC FROM ASMESTRE WITH (NOLOCK) Where CodLoja = '" + lbLoj.Text + "' and CodArmz = '" + metroGrid1[7, i].Value + "' and Referenc = '" + metroGrid1[2, i].Value + "'", conexao);
                    try
                    {
                        comand.Transaction = transac;
                        using (SqlDataReader reader = comand.ExecuteReader())
                        { while (reader.Read()) { existe = reader[2].ToString(); } }
                    }
                    catch (Exception) { existe = ""; }
                    //MessageBox.Show(existe);
                    if (existe != "" && existe != null)
                    {
                        comd[i].Transaction = transac;
                        comd[i].ExecuteNonQuery();
                    }
                    else
                    {
                        comd1[i].Transaction = transac;
                        comd1[i].ExecuteNonQuery();
                        try
                        {
                            comd2.Transaction = transac;
                            comd2.ExecuteNonQuery();
                        }
                        catch (SqlException) { }
                        try
                        {
                            comd3[i].Transaction = transac;
                            comd3[i].ExecuteNonQuery();
                        }
                        catch (SqlException) { }
                    }
                }
                transac.Commit();
                MessageBox.Show("Operacão realizada com sucesso!");
            }
            catch (SqlException ex) { transac.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private double calculaPMC(string p_CodArmz, string p_Ref, double p_Pca, int p_Qtd)
        {
            int nQTDPMC = 0, nQTDTMP = 0, nQTDKeep = 0;
            double nPMC = 0, nNovo_PMC = 0;
            //======================= SELECT DO QTD E PREÇO MÉDIO =======================
            comand = new SqlCommand("SELECT M.QTD, M.PMC From ASMESTRE M WITH (NOLOCK)Where M.CODLOJA = '" + lbLoj.Text + "' And M.CODARMZ= '" + p_CodArmz + "' And M.REFERENC= '" + p_Ref + "'", conexao);
            conexao.Open();
            try
            {
                using (SqlDataReader reader = comand.ExecuteReader())
                { while (reader.Read()) { nQTDTMP = Convert.ToInt32(reader[0].ToString()); nPMC = Convert.ToDouble(reader[0].ToString()); } }
            }
            catch (Exception) { nQTDTMP = 0; nPMC = 0; }
            finally { conexao.Close(); }
            //======================== CALCULO PARA PREÇO MÉDIO ========================
            if(nQTDPMC < 0){
                nQTDKeep = nQTDTMP;
                nQTDTMP = 0;
            }
            else if(nPMC == 0 && nQTDPMC == 0)
                nNovo_PMC = p_Pca;
            else{
                nNovo_PMC = (nPMC * nQTDPMC) + (p_Pca * p_Qtd );
                nQTDTMP = nQTDTMP + p_Qtd;
                nNovo_PMC = nNovo_PMC / nQTDTMP;
            }
            return nNovo_PMC;
        }
        //================= FUNÇÃO QUE DETERMINA O ID ÚNICO ==================================
        private string idUnico()
        {
            //string id;
            comand = new SqlCommand("SELECT RTRIM(ISNULL(CODLOJA, '" + lbLoj.Text + "')) + CAST (ISNULL( MAX( CAST( RIGHT (RTRIM(IDUNICO), LEN(RTRIM(IDUNICO)) - ( LEN(ISNULL(CODLOJA, '" + lbLoj.Text + "')) ) )AS INT ) ) , 1) + 1 AS VARCHAR(23)) ASIDUNICO " +
                                    "FROM AREGDOC WITH (XLOCK) WHERE CODLOJA = '" + lbLoj.Text + "' AND LEFT(IDUNICO, LEN(CODLOJA) ) = '" + lbLoj.Text + "' GROUP BY CODLOJA", conexao);
            conexao.Open();
            try
            {
                using (SqlDataReader reader = comand.ExecuteReader())
                { while (reader.Read()) { id = reader[0].ToString(); } }
            }
            catch (Exception) { id = Variavel.codLoja + "1"; }
            finally{conexao.Close();}

            return id;
        }
        //================= FUNÇÃO QUE DETERMINA O NÚMERO DO DOCUMENTO ==================================
        private int numeroDoc()
        {
            //int numero;
            comand = new SqlCommand("SELECT ISNULL(MAX(NUMDOC),0) + 1 FROM AregDoc WITH (XLOCK) WHERE CodDoc = 'SR' AND CodLoja= '" + Variavel.codLoja + "'", conexao);
            conexao.Open();
            try
            {
                using (SqlDataReader reader = comand.ExecuteReader())
                { while (reader.Read()) { numero = Convert.ToInt32(reader[0].ToString()); } }
            }
            catch (Exception) { numero = 1; }
            finally{conexao.Close();}

            return numero;
        }
        //==================== FUNÇÃO QUE VERIFICA AS LINHAS INCOMPLETAS E AS ELIMINA =======================
        private void apagarLinhasNoGrid()
        {
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                if (metroGrid1[0, i].Value == null || metroGrid1[7, i].Value == null)
                {
                    metroGrid1.Rows.RemoveAt(i);
                }
            }
            metroGrid1.Refresh();
        }
        private void prenxerGrid()
        {
            if (sCodArmzDestino == null || sCodArmzDestino == "")
                nivelif();
            else
                nivelelse();
        }
        private void cmbGrid()
        {
            comand = new SqlCommand(@"Select DISTINCT RTRIM(LTRIM(NomeArz)) From ASARMAZ With (NoLock) Where CodLoja= 'TIT' And Ativo = 'S' ", conexao);
            comand.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = comand.ExecuteReader();
                while (leitor.Read()) { Mercadoria.Items.Add(leitor.GetValue(0)); }
            }
            catch (SqlException ex){ MessageBox.Show(ex.Message);}
            finally{ conexao.Close();}
        }
        private void CmbALoja()
        {
            comand = new SqlCommand(@"SELECT DISTINCT RTRIM(LTRIM(J.NOMELOJA)) FROM ASLOJA J WITH (NOLOCK), ASFICMOV1 V WITH (NOLOCK) " +
                                           "Where J.CODLOJA = V.CODLOJA AND V.ANULADO = 'Y' AND V.CODDOC = '" + Gerais.PARAM_CODDOC_SR + "' AND (V.LOJADESTINO = '" + FormRequisicaoInterna.codLojaRI + "' Or V.CodLoja= '" + FormRequisicaoInterna.codLojaRI + "')", conexao);
            comand.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = comand.ExecuteReader();
                while (leitor.Read()) { cmbLoja.Items.Add(leitor.GetValue(0)); }
            }
            catch (SqlException ex){ MessageBox.Show(ex.Message);}
            finally{ conexao.Close();}
        }
        private void CmbID()
        {
            comand = new SqlCommand(@"SELECT DISTINCT RTRIM(LTRIM(F.IDUNICO)) From ASFICMOV1 F With (NoLock), AREGDOC R With (NoLock) " +
                                           "Where F.IDUNICO=R.IDUNICO And STATUS<>'A' And F.QTD>0 And ANULADO='Y' And F.CODDOC= '" + Gerais.PARAM_CODDOC_SR + "' And F.CODLOJA= '" + lbLoj.Text + "' And (LOJADESTINO= '" + FormRequisicaoInterna.codLojaRI + "' Or ARMZDESTINO Is Not Null) Order By RTrim(LTrim(F.IDUNICO))", conexao);
            comand.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = comand.ExecuteReader();
                while (leitor.Read()) { metroComboBox1.Items.Add(leitor.GetValue(0)); }
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void nivelif()
        {
            if (Permissoes.nPrgSTK == null || Permissoes.nPrgSTK == "")
                nPrgSTK = "0";
            else
                nPrgSTK = Permissoes.nPrgSTK;
            if (Convert.ToInt32(nPrgSTK) == 5)
                if_nivelif();
            else
                else_nivelif();
        }
        private void nivelelse()
        {
            if (Permissoes.nPrgSTK == null || Permissoes.nPrgSTK == "")
                nPrgSTK = "0";
            else
                nPrgSTK = Permissoes.nPrgSTK;
            if (Convert.ToInt32(nPrgSTK) == 5)
                if_nivelelse();
            else
                else_nivelelse();
        }
        private void if_nivelif()
        {
            sql = @"SELECT DISTINCT RTRIM(LTRIM(V.Referenc)), RTRIM(LTRIM(M.DESCRARTIGO)) , V.QTD, RTRIM(LTRIM(F.NOMEMERC)), RTRIM(LTRIM(V.CODLOJA)), RTRIM(LTRIM(V.CODARMZ)), RTRIM(LTRIM(F.CODFAM)), M.FOB, V.PCLD, V.PVPD, V.PCLD * '" + Convert.ToDouble(Variavel.cambio) + "', V.PVPD * '" + Convert.ToDouble(Variavel.cambio) + "', " +
                "RTRIM(LTRIM(DESCR_SUP)), RTRIM(LTRIM(ISNULL(DATA_FABRICO, NULL))), RTRIM(LTRIM(ISNULL(DATA_EXPIRA, NULL ))), RTRIM(LTRIM(REGEQUIP)), RTRIM(LTRIM(CODMARCA)), RTRIM(LTRIM(CODMODELO)), RTRIM(LTRIM(CODSUBFAM)), LTRIM(RTRIM(V.IDUNICO)), V.NUMDOC, RTRIM(LTRIM(V.GUIA)), RTRIM(LTRIM(FMRI.QTD)) " +
                "From AREGDOC RSR WITH (NOLOCK), ASFICMOV1 V WITH (NOLOCK), AREGDOC RRI WITH (NOLOCK), ASFICMOV1 FMRI WITH (NOLOCK), ASARMAZ Z WITH (NOLOCK), ASLOJA J WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), ASFAMILIA F WITH (NOLOCK) " +
                "Where LTRIM(RTRIM(V.IDUNICO))= '"+ metroComboBox1.Text +"' AND Z.CODLOJA=J.CODLOJA AND J.CODLOJA =V.CODLOJA AND FMRI.CODLOJA =V.CODLOJA AND FMRI.CODARMZ =V.CODARMZ AND Z.CODARMZ =V.CODARMZ AND V.CODLOJA = '"+ lbLoj.Text +"' AND (V.LOJADESTINO= '"+ FormRequisicaoInterna.codLojaRI +"' Or V.ARMZDESTINO Is Not Null) " +
                "AND V.CODDOC = 'SR' AND V.ANULADO ='Y' AND M.CODFAM=F.CODFAM AND M.CODLOJA=V.CODLOJA AND M.CODARMZ=V.CODARMZ AND M.REFERENC=V.REFERENC AND RSR.IDUNICO=V.IDUNICO And RSR.CODDOC='SR' And RSR.IDOrig=RRI.IDUnico And V.Referenc=FMRI.Referenc AND RRI.IDUNICO=FMRI.IDUNICO";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataAdapter.SelectCommand.CommandTimeout = 90000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void else_nivelif()
        {
            sql = @"SELECT DISTINCT RTRIM(LTRIM(V.Referenc)), RTRIM(LTRIM(M.DESCRARTIGO)) , V.QTD, RTRIM(LTRIM(F.NOMEMERC)), RTRIM(LTRIM(V.CODLOJA)), RTRIM(LTRIM(V.CODARMZ)), RTRIM(LTRIM(F.CODFAM)), M.FOB, V.PCLD, V.PVPD, V.PCLD * '" + Convert.ToDouble(Variavel.cambio) + "', V.PVPD * '" + Convert.ToDouble(Variavel.cambio) + "', " +
                "RTRIM(LTRIM(DESCR_SUP)), RTRIM(LTRIM(ISNULL(DATA_FABRICO, NULL))), RTRIM(LTRIM(ISNULL(DATA_EXPIRA, NULL ))), RTRIM(LTRIM(REGEQUIP)), RTRIM(LTRIM(CODMARCA)), RTRIM(LTRIM(CODMODELO)), RTRIM(LTRIM(CODSUBFAM)), LTRIM(RTRIM(V.IDUNICO)), V.NUMDOC, RTRIM(LTRIM(V.GUIA)), RTRIM(LTRIM(FMRI.QTD)) " +
                "From AREGDOC RSR WITH (NOLOCK), ASFICMOV1 V WITH (NOLOCK), AREGDOC RRI WITH (NOLOCK), ASFICMOV1 FMRI WITH (NOLOCK), ASARMAZ Z WITH (NOLOCK), ASLOJA J WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), ASFAMILIA F WITH (NOLOCK) " +
                "Where LTRIM(RTRIM(V.IDUNICO))= '" + metroComboBox1.Text + "' AND Z.CODLOJA=J.CODLOJA AND J.CODLOJA =V.CODLOJA AND FMRI.CODLOJA =V.CODLOJA AND FMRI.CODARMZ =V.CODARMZ AND Z.CODARMZ =V.CODARMZ AND V.CODLOJA = '" + lbLoj.Text + "' AND (V.LOJADESTINO= '" + FormRequisicaoInterna.codLojaRI + "' Or V.ARMZDESTINO Is Not Null) " +
                "AND V.CODDOC = 'SR' AND V.ANULADO ='Y' AND M.CODFAM=F.CODFAM AND M.CODLOJA=V.CODLOJA AND M.CODARMZ=V.CODARMZ AND M.REFERENC=V.REFERENC AND RSR.IDUNICO=V.IDUNICO And RSR.CODDOC='SR' And RSR.IDOrig=RRI.IDUnico And RRI.CodUtilizador!= '"+ Variavel.codUtilizador +"' And V.Referenc=FMRI.Referenc AND RRI.IDUNICO=FMRI.IDUNICO";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataAdapter.SelectCommand.CommandTimeout = 90000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void if_nivelelse()
        {
            sql = @"SELECT DISTINCT RTRIM(LTRIM(V.Referenc)), RTRIM(LTRIM(M.DESCRARTIGO)) , V.QTD, RTRIM(LTRIM(F.NOMEMERC)), RTRIM(LTRIM(V.CODLOJA)), RTRIM(LTRIM(V.CODARMZ)), RTRIM(LTRIM(F.CODFAM)), M.FOB, V.PCLD, V.PVPD, V.PCLD * '" + Convert.ToDouble(Variavel.cambio) + "', V.PVPD * '" + Convert.ToDouble(Variavel.cambio) + "', " +
                "RTRIM(LTRIM(DESCR_SUP)), RTRIM(LTRIM(ISNULL(DATA_FABRICO, NULL))), RTRIM(LTRIM(ISNULL(DATA_EXPIRA, NULL ))), RTRIM(LTRIM(REGEQUIP)), RTRIM(LTRIM(CODMARCA)),RTRIM(LTRIM(CODMODELO)), RTRIM(LTRIM(CODSUBFAM)), LTRIM(RTRIM(V.IDUNICO)), V.NUMDOC, RTRIM(LTRIM(V.GUIA)), FMRI.QTD, RTRIM(LTRIM(AZ.NomeArz)), RTrim(LTrim(V.ArmzDestino)) " +
                "From AREGDOC RSR WITH (NOLOCK), ASFICMOV1 V WITH (NOLOCK), AREGDOC RRI WITH (NOLOCK), ASFICMOV1 FMRI WITH (NOLOCK), ASARMAZ Z WITH (NOLOCK), ASLOJA J WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), ASFAMILIA F WITH (NOLOCK) " +
                "Where LTRIM(RTRIM(V.IDUNICO))='" + metroComboBox1.Text + "' AND Z.CODLOJA=J.CODLOJA AND J.CODLOJA =V.CODLOJA AND FMRI.CODLOJA =V.CODLOJA AND FMRI.CODARMZ =V.CODARMZ AND Z.CODARMZ =V.CODARMZ And AZ.CODARMZ =V.ARMZDESTINO AND V.CODLOJA = '" + lbLoj.Text + "' AND (V.LOJADESTINO= '" + FormRequisicaoInterna.codLojaRI + "' Or V.ARMZDESTINO Is Not Null) " +
                "AND V.CODDOC = 'SR' AND V.ANULADO ='Y' AND M.CODFAM=F.CODFAM AND M.CODLOJA=V.CODLOJA AND M.CODARMZ=V.CODARMZ AND M.REFERENC=V.REFERENC AND RSR.IDUNICO=V.IDUNICO And RSR.CODDOC='SR' And RSR.IDOrig=RRI.IDUnico And V.Referenc=FMRI.Referenc AND RRI.IDUNICO=FMRI.IDUNICO";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataAdapter.SelectCommand.CommandTimeout = 90000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void else_nivelelse()
        {
            sql = @"SELECT DISTINCT RTRIM(LTRIM(V.Referenc)), RTRIM(LTRIM(M.DESCRARTIGO)) , V.QTD, RTRIM(LTRIM(F.NOMEMERC)), RTRIM(LTRIM(V.CODLOJA)), RTRIM(LTRIM(V.CODARMZ)), RTRIM(LTRIM(F.CODFAM)), M.FOB, V.PCLD, V.PVPD, V.PCLD * '" + Convert.ToDouble(Variavel.cambio) + "', V.PVPD * '" + Convert.ToDouble(Variavel.cambio) + "', " +
                "RTRIM(LTRIM(DESCR_SUP)), RTRIM(LTRIM(ISNULL(DATA_FABRICO, NULL))), RTRIM(LTRIM(ISNULL(DATA_EXPIRA, NULL ))), RTRIM(LTRIM(REGEQUIP)), RTRIM(LTRIM(CODMARCA)),RTRIM(LTRIM(CODMODELO)), RTRIM(LTRIM(CODSUBFAM)), LTRIM(RTRIM(V.IDUNICO)), V.NUMDOC, RTRIM(LTRIM(V.GUIA)), FMRI.QTD, RTRIM(LTRIM(AZ.NomeArz)), RTrim(LTrim(V.ArmzDestino)) " +
                "From AREGDOC RSR WITH (NOLOCK), ASFICMOV1 V WITH (NOLOCK), AREGDOC RRI WITH (NOLOCK), ASFICMOV1 FMRI WITH (NOLOCK), ASARMAZ Z WITH (NOLOCK), ASLOJA J WITH (NOLOCK), ASMESTRE M WITH (NOLOCK), ASFAMILIA F WITH (NOLOCK) " +
                "Where LTRIM(RTRIM(V.IDUNICO))='" + metroComboBox1.Text + "' AND Z.CODLOJA=J.CODLOJA AND J.CODLOJA =V.CODLOJA AND FMRI.CODLOJA =V.CODLOJA AND FMRI.CODARMZ =V.CODARMZ AND Z.CODARMZ =V.CODARMZ And AZ.CODARMZ =V.ARMZDESTINO AND V.CODLOJA = '" + lbLoj.Text + "' AND (V.LOJADESTINO= '" + FormRequisicaoInterna.codLojaRI + "' Or V.ARMZDESTINO Is Not Null) " +
                "AND V.CODDOC = 'SR' AND V.ANULADO ='Y' AND M.CODFAM=F.CODFAM AND M.CODLOJA=V.CODLOJA AND M.CODARMZ=V.CODARMZ AND M.REFERENC=V.REFERENC AND RSR.IDUNICO=V.IDUNICO And RSR.CODDOC='SR' And RSR.IDOrig=RRI.IDUnico And V.Referenc=FMRI.Referenc AND RRI.IDUNICO=FMRI.IDUNICO And RRI.CodUtilizador!= '"+ Variavel.codUtilizador +"'";

            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataAdapter.SelectCommand.CommandTimeout = 90000;
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        private void FormRequisicaoRecepcao_Load(object sender, EventArgs e)
        {
            comand = new SqlCommand("select RTRIM(LTRIM(NomeLoja)) from ASLOJA WITH (NOLOCK) where CodLoja = '" + FormRequisicaoInterna.codLojaRI + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = comand.ExecuteReader())
            { while (reader.Read()) { nomeLoja = reader[0].ToString(); } }   
            conexao.Close();

            lbLoja.Text = FormRequisicaoInterna.codLojaRI + "  -  " + nomeLoja;

            metroGrid1.AutoGenerateColumns = false;
            //prenxerGrid();
            cmbGrid();
        }

        private void cmbLoja_MouseClick(object sender, MouseEventArgs e)
        {
            cmbLoja.Items.Clear();
            Cursor.Current = Cursors.WaitCursor;
            CmbALoja();
        }

        private void cmbLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            comand = new SqlCommand("select RTRIM(LTRIM(CodLoja)) from ASLOJA WITH (NOLOCK) where NomeLoja = '" + cmbLoja.Text + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = comand.ExecuteReader())
            { while (reader.Read()) { lbLoj.Text = reader[0].ToString(); } } 
            conexao.Close();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid1.CurrentRow != null)
                    //MessageBox.Show("Sim");
                if (e.ColumnIndex == 0)
                {
                    //cmbGrid();
                }
            }
            //if(metroComboBox1 == metroComboBox1.Click())
        }

        private void metroComboBox1_MouseClick(object sender, MouseEventArgs e)
        {
            metroComboBox1.Items.Clear();
            Cursor.Current = Cursors.WaitCursor;
            CmbID();
        }
        private void metroGrid1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (metroGrid1.Columns[metroGrid1.CurrentCell.ColumnIndex].Name.Equals("Mercadoria"))
            {
                ComboBox cmbprocess = e.Control as ComboBox;
                cmbprocess.SelectedIndexChanged += new EventHandler(Mercadoria_SelectedIndexChanged);
            }
        }
        private void Mercadoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmbprocess = (ComboBox)sender;
            if (cmbprocess.SelectedValue != null)
            {
                //MessageBox.Show("Sim 1");
            }
            
            for(int i = 0; i < metroGrid1.RowCount - 1; i++) 
                metroGrid1.CurrentRow.Cells[0].ReadOnly = true;
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                metroGrid1.CurrentRow.Cells[0].ReadOnly = false;
            //metroGrid1.CurrentRow.Cells[0].Value = DataGridViewComboBoxCell.;
            comand = new SqlCommand("SELECT LTRIM(RTRIM(CodArmz)) FROM ASARMAZ WITH (NOLOCK) WHERE CodLoja = '" + lbLoj.Text + "' AND NomeArz= '" + metroGrid1.CurrentCell.Value + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = comand.ExecuteReader())
            { while (reader.Read()) { codArmaz = reader[0].ToString(); } }

            comand = new SqlCommand("SELECT LTRIM(RTRIM(CodAO)) FROM ASARMAZ WITH (NOLOCK) WHERE CODARMZ = '" + codArmaz + "' AND CODLOJA= '" + lbLoj.Text + "'", conexao);
            using (SqlDataReader reader = comand.ExecuteReader())
            { while (reader.Read()) { codAO = reader[0].ToString(); } }
            conexao.Close();
            //MessageBox.Show(codArmaz + " - " + codAO);
            //MessageBox.Show("" + metroGrid1.CurrentCell.Value);
            metroGrid1.CurrentRow.Cells[7].Value = codArmaz;
            metroGrid1.CurrentRow.Cells[8].Value = codAO;
        }

        private void metroComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;
            comand = new SqlCommand("Select RTRIM(LTRIM(ARMZDESTINO)) From ASFICMOV1 With (NoLock) Where LTrim(RTrim(IDunico)) = '" + metroComboBox1.Text + "'", conexao);
            conexao.Open();
            try
            {
                using (SqlDataReader reader = comand.ExecuteReader())
                { while (reader.Read()) { sCodArmzDestino = reader[0].ToString(); } }
            }
            catch (Exception) { sCodArmzDestino = ""; }
            finally { conexao.Close(); }
            //MessageBox.Show(sCodArmzDestino);
            Cursor.Current = Cursors.WaitCursor;
            prenxerGrid();
        }

        private void btGravar_Click(object sender, EventArgs e)
        {
            if (lbLoj.Text == "" || lbLoj.Text == null)
                MessageBox.Show("Por favor especifique a [ Loja Emissora ]");
            else if (metroComboBox1.Text == "" || metroComboBox1.Text == null)
                MessageBox.Show("Por favor especifique o [ ID do Documento ]");
            else
            {
                for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                    apagarLinhasNoGrid();
                apagarLinhasNoGrid();
                metroGrid1.Refresh();
                Cursor.Current = Cursors.WaitCursor;
                gravar(metroGrid1.RowCount - 1);

                this.Close();
            }
        }
    }
}
