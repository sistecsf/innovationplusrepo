﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormVendasNSerie : MetroFramework.Forms.MetroForm
    {
        string garantia;
        string[] numeroSerie;
        int c = 0;
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        SqlCommand cmd = new SqlCommand();  // definicao do comando sql command
        SqlConnection conexao = new SqlConnection(Variavel.Conexao); // conexão com o banco
        public FormVendasNSerie()
        {
            InitializeComponent();
        }
        private void prenxeGrid2()
        {
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(@"SELECT NSERIE FROM AEFICHAS where CodMarca = '" + FormVendas.codMarca + "' and CodModelo = '" + FormVendas.codModelo + "' and Referenc = '" + FormVendas.referencia + "' and CodLoja = '" + Variavel.codLoja + "' and CODARMZ = '" + FormVendas.CodArmz + "' AND Saiu is null", conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("Loja");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid2.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void gridDuploClick(DataGridView dataGrid, DataGridView dataGrid1)
        {
            if (dataGrid1.RowCount - 1 < Convert.ToInt32(lbQuantidade.Text))
            {
                dataGrid1.Rows.Add(dataGrid.CurrentRow.Cells[0].Value.ToString());
                dataGrid.Rows.Remove(dataGrid.CurrentRow);
                dataGrid.Refresh();
                c++;
            }
            else
                MessageBox.Show("Já foi seleccionado a quantidade desejada");
        }
        private void FormVendasNSerie_Load(object sender, EventArgs e)
        {
            lbQuantidade.Text = FormVendas.Qtd;
            metroGrid2.AutoGenerateColumns = false;
            prenxeGrid2();
            SqlCommand cmd = new SqlCommand("select Garantia from AEMODELO with (NOLOCK) where CodMarca = '" + FormVendas.codMarca + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    garantia = reader[0].ToString();
                }
            }
        }
        private void metroGrid2_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (metroGrid2.CurrentRow != null)
                {
                    try { gridDuploClick(metroGrid2, metroGrid1); }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btGravar_Click(object sender, EventArgs e)
        {
            FormVendas.numeroS = new string[metroGrid1.RowCount - 1];

            try { FormVendas.garantia = Convert.ToInt32(garantia);}
            catch { FormVendas.garantia = 0; }

            for (int a = 0; a < metroGrid1.RowCount - 1; a++)
                FormVendas.numeroS[a] = this.metroGrid1[0, a].Value.ToString();

            FormVendas.countNS = metroGrid1.RowCount - 1;
            this.DialogResult = DialogResult.OK;
        }

        private void metroGrid2_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < metroGrid1.RowCount - 1)
            {
                try { gridDuploClick(metroGrid2, metroGrid1); }
                catch (Exception ex) { MessageBox.Show(ex.Message); }
            }
        }
    }
}
