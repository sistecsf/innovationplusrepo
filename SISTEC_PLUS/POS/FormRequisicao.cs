﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;
using SISTEC_PLUS.POS.Recibos;

namespace SISTEC_PLUS.POS
{
    public partial class FormRequisicao : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        string codCliente, codArmazem = "", codArea = "", cliente, armazem;
        string referencia, descricao, preco, stock, sql, select, PCA, FOB, obs, precoUnico, Qtd, Total, totalKz, totalUsd, CONTASTOCKS;
        int nCliDesc;
        string sTipoCli, sDisplay_ContraValor, sLocalizacao_Fisica;
        string numero;
        string dat, codD, codL, nomeU, nomeL, codLojaD, nomeLojaD, status;
        int tamanho, i;
        public static string idunico, data, nome, num, codLoja;
        public FormRequisicao()
        {
            InitializeComponent();
        }
        /*Função com os comandoa sql de armazenamentos de dados*/
        private void gravar()
        {
            string pcld, pvpd, fob, pvp, pcl;
            if (cmbArea.Text == "" || cmbArea.Visible == false) codArea = "";
            SqlCommand comand = conexao.CreateCommand();
            SqlCommand[] comand1 = new SqlCommand[metroGrid1.RowCount - 1];
            //comand1 = conexao.CreateCommand();
            if (codCliente == Gerais.PARAM_CODCLI_GERAL)
                cliente = cmbCliente1.Text;
            else cliente = "";
            //============================ INSERT AREGDOC ============================
            comand.CommandText = "INSERT INTO AREGDOC (IDunico,            DataCria,                       Codutilizador,                    CodDoc,            CodEntidade,                    CodLoja,                         CodAO,             NumDoc,          NumFact,            Descr,                            Preco,                                PrecoD,            Status,       Data_Lancamento,                 Moeda,                     Idorig,            OBS,         NomeEntidade)" +
                                "values ('" + idunico + "', CONVERT(VARCHAR(20), GETDATE(), 120), '" + Variavel.codUtilizador + "', '" + lbTipo.Text + "', '" + codCliente + "', '" + FormRequisicaoInterna.codLojaRI + "', '" + codArea + "', '" + numero + "', '" + numero + "', '" + txtTipo.Text + "', '" + totalKz.Replace(',', '.') + "', '" + totalUsd.Replace(',', '.') + "', 'E', '" + dataLancamente() + "', '" + Gerais.PARAM_MOEDA_PDR + "', '" + idunico + "', '" + obs + "', '" + cliente + "')";
            
            //============================ INSERT ASFICMOV1 ============================
            if (cmbLoja.Enabled == false && cmbArmaze.Text != "")
            {
                for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                {
                    pcl = "" + Convert.ToDouble(metroGrid1[6, i].Value) * Convert.ToDouble(lbCambio.Text);
                    pvpd = "" + Convert.ToDouble(metroGrid1[3, i].Value) / Convert.ToDouble(lbCambio.Text);
                    fob = "" + Convert.ToDouble(metroGrid1[7, i].Value);
                    pvp = "" + Convert.ToDouble(metroGrid1[3, i].Value);
                    pcld = "" + Convert.ToDouble(metroGrid1[6, i].Value);
                    comand1[i] = conexao.CreateCommand();
                    comand1[i].CommandText = "INSERT INTO ASFICMOV1 (IDUNICO, NUMDOC,           CODARMZ,                          REFERENC,                              PVP,                            PVPD,                             PCL,                             PCLD,                          FOB,                            ALTSTOK,                      DATACRIA,                                  QTD,                         CODENTIDADE,                      CODDOC,     TIPMOV, POSTO,          CODUTILIZADOR,                           CODLOJA,                                MOEDA,                             CODAO,            ESTADO,    DATA_LANCAMENTO,             ARMZDESTINO)" +
                                    "values ('" + idunico + "', '" + numero + "', '" + metroGrid1[5, i].Value + "', '" + metroGrid1[0, i].Value + "', '" + pvp.Replace(',', '.') + "', '" + pvpd.Replace(',', '.') + "', '" + pcl.Replace(',', '.') + "', '" + pcld.Replace(',', '.') + "', '" + fob.Replace(',', '.') + "', '" + metroGrid1[8, i].Value + "', Convert(varchar(20),GetDate(),120), '" + metroGrid1[2, i].Value + "', '" + Gerais.PARAM_CODCLI_GERAL + "', '" + lbTipo.Text + "', '=',    0 , '" + Variavel.codUtilizador + "', '" + FormRequisicaoInterna.codLojaRI + "', '" + Gerais.PARAM_MOEDA_PDR + "', '" + metroGrid1[9, i].Value + "', 'E', '" + dataLancamente() + "', '" + codArmazem + "')";
                }
            }
            else
            {
                for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                {
                    pcl = "" + Convert.ToDouble(metroGrid1[6, i].Value) * Convert.ToDouble(lbCambio.Text);
                    pvpd = "" + Convert.ToDouble(metroGrid1[3, i].Value) / Convert.ToDouble(lbCambio.Text);
                    fob = "" + Convert.ToDouble(metroGrid1[7, i].Value);
                    pvp = "" + Convert.ToDouble(metroGrid1[3, i].Value);
                    pcld = "" + Convert.ToDouble(metroGrid1[6, i].Value);
                    comand1[i] = conexao.CreateCommand();
                    comand1[i].CommandText = "INSERT INTO ASFICMOV1 (IDUNICO, NUMDOC,              CODARMZ,                       REFERENC,                             PVP,                           PVPD,                              PCL,                             PCLD,                              FOB,                          ALTSTOK,                      DATACRIA,                                   QTD,                  CODENTIDADE,             CODDOC,      TIPMOV, POSTO,         CODUTILIZADOR,                              CODLOJA,                             MOEDA,                              CODAO,            ESTADO,    DATA_LANCAMENTO,         LOJADESTINO,                      X1,                                 X2)" +
                                    "values ('" + idunico + "', '" + numero + "', '" + metroGrid1[5, i].Value + "', '" + metroGrid1[0, i].Value + "', '" + pvp.Replace(',', '.') + "', '" + pvpd.Replace(',', '.') + "', '" + pcld.Replace(',', '.') + "', '" + pcld.Replace(',', '.') + "', '" + fob.Replace(',', '.') + "', '" + metroGrid1[8, i].Value + "', CONVERT(VARCHAR(20),GETDATE(), 120), '" + metroGrid1[2, i].Value + "', '" + codCliente + "', '" + lbTipo.Text + "', '=',    0 , '" + Variavel.codUtilizador + "', '" + FormRequisicaoInterna.codLojaRI + "', '" + Gerais.PARAM_MOEDA_PDR + "', '" + metroGrid1[9, i].Value + "', 'E', '" + dataLancamente() + "', '" + codLoja + "', '" + metroGrid1[10, i].Value + "', '" + metroGrid1[11, i].Value + "')";
                }
            }

            conexao.Open();
            SqlTransaction tran = conexao.BeginTransaction();
            try
            {
                comand.Transaction = tran;
                comand.ExecuteNonQuery();

                for (int i = 0; i < metroGrid1.RowCount - 1; i++)
                {
                    comand1[i].Transaction = tran;
                    comand1[i].ExecuteNonQuery();
                }

                tran.Commit();
                MessageBox.Show("Gravado com sucesso!");
            }
            catch (SqlException ex) { tran.Rollback(); MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        /*Função que imprime a factira*/
        private void imprimir(string id, string remitido)
        {
            try
            {
                SqlCommand cmd2 = new SqlCommand("select distinct F.CodLoja, L.NomeLoja from ASFICMOV1 F, ASLOJA L where IDunico = '" + id + "' and F.CodLoja = L.CodLoja", conexao);
                conexao.Open();
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        codLojaD = reader["CodLoja"].ToString();
                        nomeLojaD = reader["NomeLoja"].ToString();
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }

            try
            {
                SqlCommand comd = new SqlCommand("select DISTINCT CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105), R.NumDoc, R.CodLoja, U.NomeUtilizador, L.NomeLoja from ARegDoc R WITH (NOLOCK), UTILIZADORES U WITH (NOLOCK), ASLOJA L WITH (NOLOCK) where IDUnico = '" + id + "' AND R.CodUtilizador = U.CodUtilizador AND R.CodLoja = L.CodLoja", conexao);
                conexao.Open();
                using (SqlDataReader reader = comd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        dat = reader[0].ToString();
                        codD = reader[1].ToString();
                        codL = reader[2].ToString();
                        nomeU = reader[3].ToString();
                        nomeL = reader[4].ToString();
                    }
                }
            }
            catch (Exception) { }
            finally { conexao.Close(); }

            Requisicao form = new Requisicao(tamanho, dat, codD + "/" + codL, id, nomeL, nomeU, remitido, status, nomeLojaD + " - " + codLojaD);
            form.ShowDialog();
        }
        private void prenxerGrid()
        {
            double total = 0;
            bool b = false;
            int c = -1;
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                if (cmbReferencia1.Text == metroGrid1[0, i].Value.ToString() && cmbDesignacao1.Text == metroGrid1[1, i].Value.ToString())
                {
                    b = true;
                    c = i;
                }
            }

            if (b == true)
            {
                    Qtd = "" + (Convert.ToInt32(txtQtd.Text) + Convert.ToInt32(metroGrid1[2, c].Value));
                    Total = "" + (Convert.ToDouble(lbTotal.Text) + Convert.ToDouble(metroGrid1[4, c].Value));
                    if (Convert.ToInt32(Qtd) <= Convert.ToInt32(lbStock.Text))
                    {
                        this.metroGrid1[2, c].Value = Qtd;
                        this.metroGrid1[4, c].Value = Total;
                    }
                    else
                        MessageBox.Show("A Quantidade total do artigo tem de ser menor ou igual ao que tem no Stock");
            }
            else
            {
                metroGrid1.Rows.Add(referencia, descricao, Qtd, lbPrUnitario.Text, Total, armazem, PCA, FOB, CONTASTOCKS, codArea, lbStock.Text, "0");
            }
            totalKz = "0";
            for (int i = 0; i < metroGrid1.RowCount - 1; i++)
            {
                totalKz = "" + (Convert.ToDouble(totalKz) + Convert.ToDouble(metroGrid1[4, i].Value));
            }
            totalUsd = "" + ((Convert.ToDouble(totalKz) / Convert.ToDouble(lbCambio.Text)));
        }
        private void prenxerGrid3(string id)
        {
            sql = @"select F.IDunico, F.NUMDOC, CONVERT(VARCHAR(10), CAST(F.DataCria AS DATETIME), 105), U.NomeUtilizador, "+
                              "STATUS = CASE R.Status WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO' WHEN 'F' THEN 'PAG. PARCIAL' WHEN 'J' THEN 'ADJUDICADO' WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END "+
                                "from ASFICMOV1 F with (nolock), ARegDoc R with (nolock), UTILIZADORES U with (nolock) " +
                                "WHERE R.IDOrig = '"+ id +"' AND F.IDunico = R.IDUnico AND R.CodUtilizador = U.CodUtilizador and R.STATUS != 'W' AND R.STATUS != 'C' order by R.DataCria, F.NUMDOC";
            
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(sql, conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid3.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        private void prenxerGrid2(string id)
        {
            try
            {
                conexao.Open();
                dataAdapter = new SqlDataAdapter(@"select DISTINCT F.Referenc, M.DescrArtigo, F.QTD, F.PVP, F.PVP * F.QTD, F.CodArmz from ASFICMOV1 F WITH (NOLOCK), ASMESTRE M WITH (NOLOCK) where IDUnico = '" + id + "' AND F.Referenc = M.Referenc", conexao);
                dataSet = new System.Data.DataSet();
                dataTable = new DataTable("pessoa");
                dataView = new DataView(dataTable);
                dataAdapter.Fill(dataTable);
                metroGrid1.DataSource = dataView;
            }
            catch (Exception ex) { }
            finally { conexao.Close(); }
        }
        /*Função que calcula o ID Único*/
        private string idUnico()
        {
            string unico;
            SqlCommand cmd2 = new SqlCommand("SELECT RTRIM(ISNULL(CodLoja, '" + FormRequisicaoInterna.codLojaRI + "')) + CAST (ISNULL( MAX( CAST( RIGHT (RTRIM(IDUNICO), LEN(RTRIM(IDUNICO)) - " +
                                        "(LEN(ISNULL(CodLoja,'" + FormRequisicaoInterna.codLojaRI + "')) ) )AS INT ) ) , 1) + 1 AS VARCHAR(23)) AS IDUNICO FROM AREGDOC WITH (XLOCK) WHERE CODLOJA= '" + FormRequisicaoInterna.codLojaRI + "' AND LEFT(IDUNICO, LEN(CODLOJA) ) = '" + FormRequisicaoInterna.codLojaRI + "' GROUP BY CODLOJA", conexao);
            conexao.Open();
            try
            {
                unico = (string)cmd2.ExecuteScalar();
                if (unico == null || unico == "")
                    unico = FormRequisicaoInterna.codLojaRI + 1;
            }
            catch (Exception) { unico = FormRequisicaoInterna.codLojaRI + 1; }
            finally { conexao.Close(); }
            return unico;
        }
        /*Função que calcula o Número do Documento*/
        private string numeroDoc()
        {
            string numero;
            SqlCommand cmd = new SqlCommand("SP_GET_NUMDOC", conexao);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@p_sCodLoja", FormRequisicaoInterna.codLojaRI));
            cmd.Parameters.Add(new SqlParameter("@p_sCodDoc", Gerais.PARAM_CODDOC_RI));

            try
            {
                conexao.Open();
                numero = "" + cmd.ExecuteScalar();
                if (numero == null || numero == "")
                    numero = Variavel.codUtilizador + 1;
            }
            catch (Exception) { numero = Variavel.codUtilizador + 1; }
            finally { conexao.Close(); }
            return numero;
        }
        private string dataLancamente()
        {
            SqlCommand cmd2 = new SqlCommand("select MAX(data_lancamento) from AFLANCAMENTO where CodLoja=@loja ", conexao);
            cmd2.Parameters.Add(new SqlParameter("@loja", Variavel.codLoja));
            string data = "";

            try
            {
                conexao.Open();
                data = (string)cmd2.ExecuteScalar();
            }
            catch (Exception ex) { data = "2010-01-01"; }
            finally { conexao.Close(); }
            return data;
        }
        /*Função que preenxe comboBox*/
        private void preemxeCombo(string sql, ComboBox combo)
        {
            SqlCommand cmd = new SqlCommand(sql);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read())
                {
                    combo.Items.Add(leitor.GetValue(0)); //Buscando dados do campo NOME
                }
                conexao.Close();
            }
            catch (SqlException ex) { MessageBox.Show(ex.Message); }
            finally { conexao.Close(); }
        }
        /*Função que preenxe comboBox das Referências dos produtos*/
        private void CmbReferencia(int condicao, ComboBox combo)
        {
            if (condicao == 1)
            {
                sql = @"SELECT DISTINCT RTRIM(LTRIM(REFERENC)) FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA= '" + FormRequisicaoInterna.codLojaRI + "' and PVP > 0 " +
                                                    "AND ((CONTASTOCKS = 'S' AND QTD > 0 ) OR (ISNULL(CONTASTOCKS,'N')='N')) AND ISNULL(Data_Expira,CONVERT(VARCHAR(10),GETDATE(), 120)) >= CONVERT(VARCHAR(10),GETDATE(), 120) " +
                                                    "And ACTIVO = 'S' And CODARMZ In (Select CODARMZ From ASARMAZ Where CODLOJA= '" + FormRequisicaoInterna.codLojaRI + "' And UPPER(ARMA_VENDA)='S' And UPPER(IsNull(EMITIR_RI,'N'))='S') ORDER BY RTRIM(LTRIM(REFERENC))";
            }
            else
            {
                sql = "SELECT DISTINCT RTRIM(LTRIM(REFERENC)) FROM ASMESTRE WITH (NOLOCK) Where CODLOJA= '" + FormRequisicaoInterna.codLojaRI + "' and PVP > 0 " +
                        "And CodArmz<> '"+ codArmazem +"' AND ((CONTASTOCKS = 'S' AND QTD > 0 ) OR (ISNULL(CONTASTOCKS,'N')='N')) AND ISNULL(Data_Expira,Convert(varchar(10),GetDate(), 120)) >= "+
                        "Convert(varchar(10),GetDate(), 120) And ACTIVO = 'S' And CODARMZ In (Select CODARMZ From ASARMAZ Where CODLOJA= '" + FormRequisicaoInterna.codLojaRI + "' And UPPER(ARMA_VENDA)='S' And UPPER(IsNull(EMITIR_RI,'N'))='S') ORDER BY RTRIM(LTRIM(REFERENC))";
            }
            preemxeCombo(sql, combo);
        }
        /*Função que preenxe comboBox da descrição dos produtos*/
        private void CmbDesignacao(int condicao, ComboBox combo)
        {
            if (condicao == 1)
            {
                sql = @"SELECT DISTINCT RTRIM(LTRIM(DESCRARTIGO)) FROM ASMESTRE WITH (NOLOCK) WHERE CODLOJA= '" + FormRequisicaoInterna.codLojaRI + "' and PVP > 0 " +
                                            "And ((CONTASTOCKS = 'S' AND QTD > 0 ) OR (ISNULL(CONTASTOCKS,'N')='N')) And ISNULL(Data_Expira,CONVERT(VARCHAR(10),GETDATE(), 120)) >= CONVERT(VARCHAR(10),GETDATE(), 120) And ACTIVO = 'S' " +
                                            "And CODARMZ In (Select CODARMZ From ASARMAZ Where CODLOJA= '" + FormRequisicaoInterna.codLojaRI + "' And UPPER(ARMA_VENDA)='S' And UPPER(IsNull(EMITIR_RI,'N'))='S') ORDER BY RTRIM(LTRIM(DESCRARTIGO))";
            }
            else
            {
                sql = "SELECT DISTINCT RTRIM(LTRIM(DESCRARTIGO)) FROM ASMESTRE WITH (NOLOCK) Where CODLOJA= '" + FormRequisicaoInterna.codLojaRI + "' And PVP > 0 And CodArmz<> '" + codArmazem + "' " +
                    "AND ((CONTASTOCKS = 'S' AND QTD > 0 ) OR (ISNULL(CONTASTOCKS,'N')='N')) AND ISNULL(Data_Expira,Convert(varchar(10),GetDate(), 120)) >= Convert(varchar(10),GetDate(), 120) And ACTIVO = 'S' "+
                    "And CODARMZ In (Select CODARMZ From ASARMAZ Where CODLOJA= '" + FormRequisicaoInterna.codLojaRI + "' And UPPER(ARMA_VENDA)='S' And UPPER(IsNull(EMITIR_RI,'N'))='S') ORDER BY RTRIM(LTRIM(DESCRARTIGO))";
            }
            preemxeCombo(sql, combo);
        }
        /*Função que preenxe comboBox com a lista das lojas*/
        private void CmbALoja(ComboBox combo)
        {
            string sql = @"Select distinct RTRIM(LTRIM(NomeLoja)) From ASLOJA With (NoLock) Where CodLoja != '" + FormRequisicaoInterna.codLojaRI + "' And CodigoEntidade Is Null";
            preemxeCombo(sql, combo);
        }
        /*Função que preenxe comboBox com a lista dos Armazens*/
        private void CmbArmazem(ComboBox combo)
        {
            string sql = @"Select Distinct RTrim(LTrim(NomeArz)) From ASARMAZ With (NoLock) Where CodLoja = '" + FormRequisicaoInterna.codLojaRI + "' And Ativo='S'";
            preemxeCombo(sql, combo);
        }
        /*Função que preenxe comboBox com a lista dos clientes*/
        private void CmbCliente(ComboBox combo)
        {
            string sql = @"Select Distinct LTRIM(RTRIM(C.NOMEENTIDADE)) From AFENTIDADE C WITH (NOLOCK) Where CodTipoCli = 'INT' Or CodTipoCli = 'FIL' Order By LTRIM(RTRIM(C.NOMEENTIDADE))";
            preemxeCombo(sql, combo);
        }
        private void CmbArea(ComboBox combo)
        {
            string sql = @"Select NomeAO from AIAREAORGANICA WITH (NOLOCK) WHERE ISNULL(TO_DISPLAY,'S') ='S'";
            preemxeCombo(sql, combo);
        }
        private void FormRequisicao_Load(object sender, EventArgs e)
        {
            metroGrid1.AutoGenerateColumns = false;
            metroGrid3.AutoGenerateColumns = false;
            lbCambio.Text = Variavel.cambio;
            btImprimir.Enabled = false;
            TabTabelas.SelectedTab = metroTabPage1;
        }
        private void TabTabelas_MouseClick(object sender, MouseEventArgs e)
        {
            if (TabTabelas.SelectedTab == metroTabPage1)
            {
                btConsultar.Enabled = true;
                FormRequisicao.ActiveForm.Text = FormRequisicao.ActiveForm.Text + "Requisição";
            }
            else if (TabTabelas.SelectedTab == metroTabPage2)
            {
                btConsultar.Enabled = true;
                FormRequisicao.ActiveForm.Text = FormRequisicao.ActiveForm.Text + "Estado da Requisição";
            }
            else if (TabTabelas.SelectedTab == metroTabPage3)
            {
                btConsultar.Enabled = false;
                FormRequisicao.ActiveForm.Text = FormRequisicao.ActiveForm.Text + "Detalhe do Atendimento";
            }
        }

        private void metroTabPage1_MouseClick(object sender, MouseEventArgs e)
        {
            FormRequisicao form = new FormRequisicao();
            form.Text = "Requisição";
        }
        private void btConsultar_Click(object sender, EventArgs e)
        {
            FormRequisicaoPesquisar form = new FormRequisicaoPesquisar();
            if (form.ShowDialog() == DialogResult.OK)
            {
                btGravar.Enabled = false;
                btImprimir.Enabled = true;
                
                lbRIN.Text = num;
                lbData.Text = data;
                lbOperador.Text = nome;
                lbNumero.Text = num;
                cmbCliente1.Text = "EXMO SR(A)";
                cmbCliente1.PromptText = "EXMO SR(A)";
                metroGrid1.DataSource = null;
                metroGrid1.Rows.Clear();
                prenxerGrid2(idunico);

                SqlCommand cmd = new SqlCommand("SELECT RTRIM(LTRIM(STATUS)) From AREGDOC R where R.CODLOJA = '" + codLoja + "' And R.CODDOC = 'RI' And R.NUMDOC = '" + num + "' And LTRIM(RTRIM(R.IDunico)) = '" + idunico + "'", conexao);
                cmd.Connection = conexao;
                conexao.Open();
                try { status = (string)cmd.ExecuteScalar(); }
                catch (Exception) { }
                finally { conexao.Close(); }
                if (status == "W" || status == "C")
                {
                    metroGrid3.DataSource = null;
                    metroGrid3.Rows.Clear();
                    prenxerGrid3(idunico);
                }
            }
        }
        private void cmbArmaze_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand("SELECT RTRIM(LTRIM(CodArmz)) From ASARMAZ With (NoLock) Where CodLoja= '" + FormRequisicaoInterna.codLojaRI + "' And Ativo='S' And NomeArz= '" + cmbArmaze.Text + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd1.ExecuteReader())
            {
                while (reader.Read())
                {
                    codArmazem = reader[0].ToString();
                }
            }
            if (codArmazem == "" || codArmazem == null)
                cmbArmaze.Text = "";
            codCliente = Gerais.PARAM_CODCLI_GERAL;
            SqlCommand cmd = new SqlCommand("SELECT RTRIM(LTRIM(NomeEntidade)) From Afentidade With (NoLock) Where CodEntidade= '" + codCliente + "'", conexao);
            
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    cliente = reader[0].ToString();
                    
                }
            }
            conexao.Close();
            cmbCliente1.Text = cliente;
            cmbCliente1.PromptText = cliente;
            cmbCliente1.Enabled = false;
            cmbLoja.Enabled = false;
        }
        private void cmbLoja_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand("SELECT RTRIM(LTRIM(CodLoja)) From ASLOJA WITH (NOLOCK) Where NomeLoja= '"+ cmbLoja.Text +"'", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd1.ExecuteReader())
            {
                while (reader.Read())
                {
                    codLoja = reader[0].ToString();
                }
            }
            if (codLoja == "" || codLoja == null)
                cmbLoja.Text = "";
            codCliente = Gerais.PARAM_CODCLI_GERAL;
            SqlCommand cmd = new SqlCommand("SELECT RTRIM(LTRIM(NomeEntidade)) From Afentidade With (NoLock) Where CodEntidade= '" + codCliente + "'", conexao);

            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    cliente = reader[0].ToString();
                }
            }
            conexao.Close();
            cmbCliente1.Text = cliente;
            cmbCliente1.PromptText = cliente;
            cmbCliente1.Enabled = false;
            cmbArmaze.Enabled = false;
        }
        private void btNovo_Click(object sender, EventArgs e)
        {
            cmbCliente1.Enabled = true;
            btImprimir.Enabled = false;
            if (!cmbLoja.Enabled)
            {
                cmbArmaze.Text = null;
                cmbLoja.Enabled = true;
            }
            else if (!cmbArmaze.Enabled)
            {
                cmbLoja.Text = null;
                cmbArmaze.Enabled = true;
            }
            cmbCliente1.Text = null;
            cmbCliente1.PromptText = "";
            metroGrid1.DataSource = null;
            metroGrid1.Rows.Clear();
        }

        private void cmbArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("Select CodAO from AIAREAORGANICA WITH (NOLOCK) where NomeAO ='" + cmbArea.Text + "'", conexao);
            cmd.Connection = conexao;
            conexao.Open();
            try { codArea = (string)cmd.ExecuteScalar(); }
            catch (Exception) { }
            finally { conexao.Close(); }
        }
        private void txtQtd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (cmbReferencia1.Text == "")
                    MessageBox.Show("Artigo precisa estar seleccionado!");
                else
                {
                    try
                    {
                        lbTotal.Text = "" + Convert.ToInt32(txtQtd.Text) * Convert.ToDouble(lbPrUnitario.Text);
                        Qtd = txtQtd.Text;
                    }
                    catch
                    {
                        lbTotal.Text = "" + 0 * Convert.ToDouble(lbPrUnitario.Text);
                        Qtd = "0";
                    }
                    Total = lbTotal.Text;
                    prenxerGrid();

                    cmbReferencia1.Text = null;
                    cmbReferencia1.PromptText = "";
                    cmbDesignacao1.Text = null;
                    cmbDesignacao1.PromptText = "";
                    lbPrUnitario.Text = "0";
                    lbStock.Text = "0";
                    lbTotal.Text = "0";
                }
            }
        }

        private void txtQtd_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(txtQtd.Text) <= Convert.ToInt32(stock))
                {
                    try
                    {
                        lbTotal.Text = "" + Convert.ToInt32(txtQtd.Text) * Convert.ToDouble(lbPrUnitario.Text);
                    }
                    catch
                    {
                        lbTotal.Text = "" + 0 * Convert.ToDouble(lbPrUnitario.Text);
                    }
                    Total = lbTotal.Text;
                    Qtd = txtQtd.Text;
                }
                else
                {
                    MessageBox.Show("A quantidade tem que ser menor ou igual ao que tem disponivel no Stock");
                    Qtd = txtQtd.Text = "0";
                }
            }
            catch (Exception) { Qtd = "0"; Total = "0"; }
        }

        private void btGravar_Click(object sender, EventArgs e)
        {
            if (cmbCliente1.Text == "" && cmbCliente1.PromptText == "")
                MessageBox.Show("O campo [ Cliente ] deve ser preenshido!");
            else if (metroGrid1.RowCount <= 1)
                MessageBox.Show("A tabela deve ser preenshido!");
            else
            {
                idunico = idUnico();
                numero = numeroDoc();
                lbNumero.Text = numero;
                gravar();
                status = "EMISSÃO";
                imprimir(idunico, ".");
            }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            status = "REEMISSÃO";
            imprimir(idunico, Variavel.nomeUtilizador);
        }

        private void btApagar_Click(object sender, EventArgs e)
        {
            const string message = "Confirma a ANULAÇÃO do documento?";
            const string caption = "CONFIRMAÇÃO";
            var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            SqlCommand cmd = new SqlCommand("SELECT RTRIM(LTRIM(STATUS)) From AREGDOC R where R.CODLOJA = '" + codLoja + "' And R.CODDOC = 'RI' And R.NUMDOC = '" + num + "' And LTRIM(RTRIM(R.IDunico)) = '" + idunico + "'", conexao);
            cmd.Connection = conexao;
            conexao.Open();
            try { status = (string)cmd.ExecuteScalar(); }
            catch (Exception) { }
            finally { conexao.Close(); }
            if (status == "A")
            {
                MessageBox.Show("Este Documento já esta Anulado!");
            }
            else if (status == "W" || status == "C")
            {
                MessageBox.Show("Este Documento já foi Atendido!");
            }
            else
            {
                if (Convert.ToInt32(Permissoes.nPrgPos) < 4)
                {
                    MessageBox.Show("Não tem permissão para executar a acção seleccionada!");
                }
                else if (lbNumero.Text == "" || lbNumero.Text == "0") //VERIFICAR SE O DOCUMENTO EXISTE OU TEM DADOS
                {
                    MessageBox.Show("O documento não existe ou não tem dados!");
                }
                else
                {
                    if (result == DialogResult.Yes)
                    {
                        try
                        {
                            conexao.Open();
                            SqlCommand cmad = new SqlCommand("SP_ANULADOCS_RI", conexao);
                            cmad.CommandType = CommandType.StoredProcedure;

                            cmad.Parameters.Add(new SqlParameter("@IDUNICO", idunico));
                            cmad.Parameters.Add(new SqlParameter("@CODUTIL_ANUL", Variavel.codUtilizador));

                            cmad.ExecuteNonQuery();
                        }
                        catch (Exception) { }
                        finally{conexao.Close();}
                        MessageBox.Show("Documento Anulado com Sucesso");

                        cmbCliente1.Text = "";
                        lbNumero.Text = "";
                        metroGrid1.DataSource = null;
                        metroGrid1.Rows.Clear();
                    }
                }
            }
        }

        private void cmbCliente1_SelectedIndexChanged(object sender, EventArgs e)
        {
            nCliDesc = 0;
            SqlCommand cmd = new SqlCommand("SELECT RTRIM(CODENTIDADE), RTRIM(CODTIPOCLI), RTRIM(LTRIM(Display_ContraValor)), RTrim(LTrim(Localizacao_Fisica)) FROM AFENTIDADE WITH (NOLOCK) WHERE RTRIM(NomeEntidade)= '" + cmbCliente1.Text + "'", conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    codCliente = reader[0].ToString();
                    sTipoCli = reader[1].ToString();
                    sDisplay_ContraValor = reader[2].ToString();
                    sLocalizacao_Fisica = reader[3].ToString();
                }
            }
            conexao.Close();
        }
        private void cmbCliente1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                if (cmbCliente1.Text != null && cmbCliente1.Text != "")
                {
                    SqlCommand cmd = new SqlCommand("SELECT RTRIM(LTRIM(NomeEntidade)), RTRIM(CodEntidade) From Afentidade With (NoLock) Where NomeEntidade= '" + cmbCliente1.Text + "'", conexao);
                    conexao.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cmbCliente1.Text = reader[0].ToString();
                            cmbCliente1.PromptText = reader[0].ToString();
                            codCliente = reader[1].ToString();
                        }
                    }
                    conexao.Close();
                }
                if (cmbCliente1.Text == "" || codCliente != Gerais.PARAM_CODCLI_GERAL)
                    MessageBox.Show("O Cliente digitado não existe ou está anulado!");
            }
        }

        private void cmbReferencia1_SelectedIndexChanged(object sender, EventArgs e)
        {
            referencia = cmbReferencia1.Text;
            select = "SELECT FOB, PCA, PVP, QTD, OBS, RTRIM(LTRIM(DescrArtigo)), CONTASTOCKS, CodArmz FROM ASMESTRE WITH (NOLOCK) where REFERENC = '" + cmbReferencia1.Text + "'";
            SqlCommand cmd = new SqlCommand(select, conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    FOB = reader[0].ToString();
                    PCA = reader[1].ToString();
                    preco = reader[2].ToString();
                    stock = reader[3].ToString();
                    obs = reader[4].ToString();
                    descricao = reader[5].ToString();
                    CONTASTOCKS = reader[6].ToString();
                    armazem = reader[7].ToString();
                }
            }
            conexao.Close();
            cmbDesignacao1.Text = descricao;
            cmbDesignacao1.PromptText = descricao;
            precoUnico = "" + Convert.ToDouble(Variavel.cambio) * Convert.ToDouble(preco);
            lbTotal.Text = lbPrUnitario.Text = precoUnico;
            lbStock.Text = stock;
            txtQtd.Text = "1";
        }
        private void cmbDesignacao1_SelectedIndexChanged(object sender, EventArgs e)
        {
            descricao = cmbDesignacao1.Text;
            select = "SELECT FOB, PCA, PVP, QTD, OBS, RTRIM(LTRIM(REFERENC)), CONTASTOCKS, CodArmz FROM ASMESTRE WITH (NOLOCK) where DescrArtigo = '" + cmbDesignacao1.Text + "'";
            SqlCommand cmd = new SqlCommand(select, conexao);
            conexao.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    FOB = reader[0].ToString();
                    PCA = reader[1].ToString();
                    preco = reader[2].ToString();
                    stock = reader[3].ToString();
                    obs = reader[4].ToString();
                    referencia = reader[5].ToString();
                    CONTASTOCKS = reader[6].ToString();
                    armazem = reader[7].ToString();
                }
            }
            conexao.Close();
            cmbReferencia1.Text = referencia;
            precoUnico = "" + Convert.ToDouble(Variavel.cambio) * Convert.ToDouble(preco);
            lbTotal.Text = lbPrUnitario.Text = precoUnico;
            lbStock.Text = stock;
            txtQtd.Text = "1";
        }
        private void cmbReferencia1_DropDown(object sender, EventArgs e)
        {
            cmbReferencia1.Items.Clear();
            if (codArmazem == "")
                CmbReferencia(1, cmbReferencia1);
            else
                CmbReferencia(2, cmbReferencia1);
        }

        private void cmbDesignacao1_DropDown(object sender, EventArgs e)
        {
            cmbDesignacao1.Items.Clear();
            if (codArmazem == "")
                CmbDesignacao(1, cmbDesignacao1);
            else
                CmbDesignacao(2, cmbDesignacao1);
        }

        private void cmbLoja_DropDown(object sender, EventArgs e)
        {
            cmbLoja.Items.Clear();
            CmbALoja(cmbLoja);
        }

        private void cmbArmaze_DropDown(object sender, EventArgs e)
        {
            cmbArmaze.Items.Clear();
            CmbArmazem(cmbArmaze);
        }

        private void cmbCliente1_DropDown(object sender, EventArgs e)
        {
            cmbCliente1.Items.Clear();
            CmbCliente(cmbCliente1);
        }

        private void cmbArea_DropDown(object sender, EventArgs e)
        {
            cmbArea.Items.Clear();
            CmbArea(cmbArea);
        }
    }
}
