﻿namespace SISTEC_PLUS.POS
{
    partial class FormTranferencias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelNomeLojaTab1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.labelLojaCorrente = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.labelOperadorTab1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelDataActualTab1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.comboBoxArmazemDestinoTab1 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.cbSoArmazensVenda = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNumeroDaGuiaTab1 = new System.Windows.Forms.TextBox();
            this.metroGridTransferenciaTab1 = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewComboBoxTab1Mercadoria = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxTab1Referencia = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.labelNomeLojaTab2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelOperadorTab2 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.labelDataActualTab2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.comboBoxLojasTab2 = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtNumeroDaGuiaTab2 = new System.Windows.Forms.TextBox();
            this.metroGridTransferenciaTab2 = new MetroFramework.Controls.MetroGrid();
            this.dataGridViewComboBoxTab2Mercadoria = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewComboBoxTab2Referencia = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.label23 = new System.Windows.Forms.Label();
            this.labelNomeLojaTab3 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.labelOperadorTab3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelDataActualTab3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.metroGridTransferenciaTab3 = new MetroFramework.Controls.MetroGrid();
            this.fieldMercadoriaDestino = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fieldLocalizacao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fieldReferencia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fieldDescricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fieldQuantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelDataDeEmissao = new System.Windows.Forms.Label();
            this.labelUsuarioQueEmitiu = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.labelGuia = new System.Windows.Forms.Label();
            this.txtETotal3 = new MetroFramework.Controls.MetroTextBox();
            this.txtETotal2 = new MetroFramework.Controls.MetroTextBox();
            this.txtETotal1 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel26 = new MetroFramework.Controls.MetroLabel();
            this.txtEPCAP = new MetroFramework.Controls.MetroTextBox();
            this.txtETotal = new MetroFramework.Controls.MetroTextBox();
            this.txtEGuia = new MetroFramework.Controls.MetroTextBox();
            this.txtENumero = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel25 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel23 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel19 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel17 = new MetroFramework.Controls.MetroLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bntSair = new System.Windows.Forms.Button();
            this.bntImprimir = new System.Windows.Forms.Button();
            this.bntReceber = new System.Windows.Forms.Button();
            this.bntTransferir = new System.Windows.Forms.Button();
            this.labelNomeLojaTab1.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridTransferenciaTab1)).BeginInit();
            this.metroTabPage1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridTransferenciaTab2)).BeginInit();
            this.metroTabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridTransferenciaTab3)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelNomeLojaTab1
            // 
            this.labelNomeLojaTab1.Controls.Add(this.metroTabPage2);
            this.labelNomeLojaTab1.Controls.Add(this.metroTabPage1);
            this.labelNomeLojaTab1.Controls.Add(this.metroTabPage3);
            this.labelNomeLojaTab1.Location = new System.Drawing.Point(54, 116);
            this.labelNomeLojaTab1.Name = "labelNomeLojaTab1";
            this.labelNomeLojaTab1.SelectedIndex = 0;
            this.labelNomeLojaTab1.Size = new System.Drawing.Size(752, 440);
            this.labelNomeLojaTab1.TabIndex = 0;
            this.labelNomeLojaTab1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelNomeLojaTab1.UseSelectable = true;
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage2.Controls.Add(this.labelLojaCorrente);
            this.metroTabPage2.Controls.Add(this.label10);
            this.metroTabPage2.Controls.Add(this.labelOperadorTab1);
            this.metroTabPage2.Controls.Add(this.label5);
            this.metroTabPage2.Controls.Add(this.labelDataActualTab1);
            this.metroTabPage2.Controls.Add(this.label7);
            this.metroTabPage2.Controls.Add(this.groupBox5);
            this.metroTabPage2.Controls.Add(this.groupBox6);
            this.metroTabPage2.Controls.Add(this.groupBox7);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(744, 398);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Tranferência Inter-Mercadorias";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // labelLojaCorrente
            // 
            this.labelLojaCorrente.AutoSize = true;
            this.labelLojaCorrente.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelLojaCorrente.Location = new System.Drawing.Point(36, 6);
            this.labelLojaCorrente.Name = "labelLojaCorrente";
            this.labelLojaCorrente.Size = new System.Drawing.Size(0, 13);
            this.labelLojaCorrente.TabIndex = 145;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label10.Location = new System.Drawing.Point(0, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 144;
            this.label10.Text = "Loja:";
            // 
            // labelOperadorTab1
            // 
            this.labelOperadorTab1.AutoSize = true;
            this.labelOperadorTab1.Location = new System.Drawing.Point(228, 316);
            this.labelOperadorTab1.Name = "labelOperadorTab1";
            this.labelOperadorTab1.Size = new System.Drawing.Size(0, 13);
            this.labelOperadorTab1.TabIndex = 143;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(160, 316);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 142;
            this.label5.Text = "Operador:";
            // 
            // labelDataActualTab1
            // 
            this.labelDataActualTab1.AutoSize = true;
            this.labelDataActualTab1.Location = new System.Drawing.Point(73, 316);
            this.labelDataActualTab1.Name = "labelDataActualTab1";
            this.labelDataActualTab1.Size = new System.Drawing.Size(0, 13);
            this.labelDataActualTab1.TabIndex = 141;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 316);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 137;
            this.label7.Text = "Data actual:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radioButton1);
            this.groupBox5.Controls.Add(this.radioButton2);
            this.groupBox5.Location = new System.Drawing.Point(653, 248);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(91, 59);
            this.groupBox5.TabIndex = 140;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Total Sobre:";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 35);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(77, 17);
            this.radioButton1.TabIndex = 1;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Preço PCA";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(7, 14);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(77, 17);
            this.radioButton2.TabIndex = 0;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Preço FOB";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.comboBoxArmazemDestinoTab1);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Location = new System.Drawing.Point(3, 248);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(644, 59);
            this.groupBox6.TabIndex = 139;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Destino";
            // 
            // comboBoxArmazemDestinoTab1
            // 
            this.comboBoxArmazemDestinoTab1.FormattingEnabled = true;
            this.comboBoxArmazemDestinoTab1.Location = new System.Drawing.Point(70, 20);
            this.comboBoxArmazemDestinoTab1.Name = "comboBoxArmazemDestinoTab1";
            this.comboBoxArmazemDestinoTab1.Size = new System.Drawing.Size(486, 21);
            this.comboBoxArmazemDestinoTab1.TabIndex = 131;
            this.comboBoxArmazemDestinoTab1.DropDown += new System.EventHandler(this.comboBoxArmazemDestinoTab1_DropDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 130;
            this.label8.Text = "Armazém";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.checkBox4);
            this.groupBox7.Controls.Add(this.checkBox5);
            this.groupBox7.Controls.Add(this.cbSoArmazensVenda);
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Controls.Add(this.txtNumeroDaGuiaTab1);
            this.groupBox7.Controls.Add(this.metroGridTransferenciaTab1);
            this.groupBox7.Location = new System.Drawing.Point(3, 28);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(741, 199);
            this.groupBox7.TabIndex = 138;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Origem";
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(495, 25);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(61, 17);
            this.checkBox4.TabIndex = 131;
            this.checkBox4.Text = "Imprimir";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(337, 25);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(138, 17);
            this.checkBox5.TabIndex = 130;
            this.checkBox5.Text = "Selecção por descrição";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // cbSoArmazensVenda
            // 
            this.cbSoArmazensVenda.AutoSize = true;
            this.cbSoArmazensVenda.Location = new System.Drawing.Point(183, 25);
            this.cbSoArmazensVenda.Name = "cbSoArmazensVenda";
            this.cbSoArmazensVenda.Size = new System.Drawing.Size(135, 17);
            this.cbSoArmazensVenda.TabIndex = 129;
            this.cbSoArmazensVenda.Text = "Só armazéns de venda";
            this.cbSoArmazensVenda.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 128;
            this.label9.Text = "Guia";
            // 
            // txtNumeroDaGuiaTab1
            // 
            this.txtNumeroDaGuiaTab1.Location = new System.Drawing.Point(53, 23);
            this.txtNumeroDaGuiaTab1.Name = "txtNumeroDaGuiaTab1";
            this.txtNumeroDaGuiaTab1.Size = new System.Drawing.Size(105, 20);
            this.txtNumeroDaGuiaTab1.TabIndex = 127;
            // 
            // metroGridTransferenciaTab1
            // 
            this.metroGridTransferenciaTab1.AllowUserToResizeRows = false;
            this.metroGridTransferenciaTab1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridTransferenciaTab1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGridTransferenciaTab1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGridTransferenciaTab1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridTransferenciaTab1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGridTransferenciaTab1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGridTransferenciaTab1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewComboBoxTab1Mercadoria,
            this.dataGridViewComboBoxTab1Referencia,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGridTransferenciaTab1.DefaultCellStyle = dataGridViewCellStyle2;
            this.metroGridTransferenciaTab1.EnableHeadersVisualStyles = false;
            this.metroGridTransferenciaTab1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGridTransferenciaTab1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridTransferenciaTab1.Location = new System.Drawing.Point(21, 60);
            this.metroGridTransferenciaTab1.Name = "metroGridTransferenciaTab1";
            this.metroGridTransferenciaTab1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridTransferenciaTab1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.metroGridTransferenciaTab1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGridTransferenciaTab1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGridTransferenciaTab1.Size = new System.Drawing.Size(700, 117);
            this.metroGridTransferenciaTab1.TabIndex = 126;
            this.metroGridTransferenciaTab1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGridTransferenciaTab1_CellClick);
            this.metroGridTransferenciaTab1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGridTransferenciaTab1_CellEndEdit);
            // 
            // dataGridViewComboBoxTab1Mercadoria
            // 
            this.dataGridViewComboBoxTab1Mercadoria.HeaderText = "Mercadoria";
            this.dataGridViewComboBoxTab1Mercadoria.Name = "dataGridViewComboBoxTab1Mercadoria";
            // 
            // dataGridViewComboBoxTab1Referencia
            // 
            this.dataGridViewComboBoxTab1Referencia.HeaderText = "Referência";
            this.dataGridViewComboBoxTab1Referencia.Name = "dataGridViewComboBoxTab1Referencia";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Stock minimo";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "fieldStockReal";
            this.dataGridViewTextBoxColumn2.HeaderText = "Stock Real";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "fieldQuantidadeATransferir";
            this.dataGridViewTextBoxColumn3.HeaderText = "Quantidade a transferir";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage1.Controls.Add(this.labelNomeLojaTab2);
            this.metroTabPage1.Controls.Add(this.label12);
            this.metroTabPage1.Controls.Add(this.labelOperadorTab2);
            this.metroTabPage1.Controls.Add(this.label14);
            this.metroTabPage1.Controls.Add(this.labelDataActualTab2);
            this.metroTabPage1.Controls.Add(this.label16);
            this.metroTabPage1.Controls.Add(this.groupBox8);
            this.metroTabPage1.Controls.Add(this.groupBox9);
            this.metroTabPage1.Controls.Add(this.groupBox10);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(744, 398);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Tranferência Inter-Loja";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // labelNomeLojaTab2
            // 
            this.labelNomeLojaTab2.AutoSize = true;
            this.labelNomeLojaTab2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelNomeLojaTab2.Location = new System.Drawing.Point(36, 4);
            this.labelNomeLojaTab2.Name = "labelNomeLojaTab2";
            this.labelNomeLojaTab2.Size = new System.Drawing.Size(0, 13);
            this.labelNomeLojaTab2.TabIndex = 154;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label12.Location = new System.Drawing.Point(0, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 153;
            this.label12.Text = "Loja:";
            // 
            // labelOperadorTab2
            // 
            this.labelOperadorTab2.AutoSize = true;
            this.labelOperadorTab2.Location = new System.Drawing.Point(228, 313);
            this.labelOperadorTab2.Name = "labelOperadorTab2";
            this.labelOperadorTab2.Size = new System.Drawing.Size(0, 13);
            this.labelOperadorTab2.TabIndex = 152;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(160, 313);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 151;
            this.label14.Text = "Operador:";
            // 
            // labelDataActualTab2
            // 
            this.labelDataActualTab2.AutoSize = true;
            this.labelDataActualTab2.Location = new System.Drawing.Point(73, 313);
            this.labelDataActualTab2.Name = "labelDataActualTab2";
            this.labelDataActualTab2.Size = new System.Drawing.Size(0, 13);
            this.labelDataActualTab2.TabIndex = 150;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 313);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 13);
            this.label16.TabIndex = 146;
            this.label16.Text = "Data actual:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.radioButton3);
            this.groupBox8.Controls.Add(this.radioButton4);
            this.groupBox8.Location = new System.Drawing.Point(653, 245);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(91, 59);
            this.groupBox8.TabIndex = 149;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Total Sobre:";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(6, 35);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(77, 17);
            this.radioButton3.TabIndex = 1;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Preço PCA";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(7, 14);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(77, 17);
            this.radioButton4.TabIndex = 0;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Preço FOB";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.comboBoxLojasTab2);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Location = new System.Drawing.Point(3, 245);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(641, 59);
            this.groupBox9.TabIndex = 148;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Destino";
            // 
            // comboBoxLojasTab2
            // 
            this.comboBoxLojasTab2.FormattingEnabled = true;
            this.comboBoxLojasTab2.Location = new System.Drawing.Point(70, 20);
            this.comboBoxLojasTab2.Name = "comboBoxLojasTab2";
            this.comboBoxLojasTab2.Size = new System.Drawing.Size(486, 21);
            this.comboBoxLojasTab2.TabIndex = 131;
            this.comboBoxLojasTab2.DropDown += new System.EventHandler(this.comboBoxLojasTab2_DropDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(32, 13);
            this.label17.TabIndex = 130;
            this.label17.Text = "Lojas";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.checkBox7);
            this.groupBox10.Controls.Add(this.checkBox8);
            this.groupBox10.Controls.Add(this.checkBox9);
            this.groupBox10.Controls.Add(this.label18);
            this.groupBox10.Controls.Add(this.txtNumeroDaGuiaTab2);
            this.groupBox10.Controls.Add(this.metroGridTransferenciaTab2);
            this.groupBox10.Location = new System.Drawing.Point(3, 25);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(741, 199);
            this.groupBox10.TabIndex = 147;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Origem";
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(401, 25);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(138, 17);
            this.checkBox7.TabIndex = 131;
            this.checkBox7.Text = "Selecção por descrição";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(243, 25);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(147, 17);
            this.checkBox8.TabIndex = 130;
            this.checkBox8.Text = "Só mercadorias de venda";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(183, 25);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(48, 17);
            this.checkBox9.TabIndex = 129;
            this.checkBox9.Text = "Auto";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(18, 27);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 128;
            this.label18.Text = "Guia";
            // 
            // txtNumeroDaGuiaTab2
            // 
            this.txtNumeroDaGuiaTab2.Location = new System.Drawing.Point(53, 23);
            this.txtNumeroDaGuiaTab2.Name = "txtNumeroDaGuiaTab2";
            this.txtNumeroDaGuiaTab2.Size = new System.Drawing.Size(105, 20);
            this.txtNumeroDaGuiaTab2.TabIndex = 127;
            // 
            // metroGridTransferenciaTab2
            // 
            this.metroGridTransferenciaTab2.AllowUserToResizeRows = false;
            this.metroGridTransferenciaTab2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridTransferenciaTab2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGridTransferenciaTab2.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGridTransferenciaTab2.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridTransferenciaTab2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.metroGridTransferenciaTab2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGridTransferenciaTab2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewComboBoxTab2Mercadoria,
            this.dataGridViewComboBoxTab2Referencia,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGridTransferenciaTab2.DefaultCellStyle = dataGridViewCellStyle5;
            this.metroGridTransferenciaTab2.EnableHeadersVisualStyles = false;
            this.metroGridTransferenciaTab2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGridTransferenciaTab2.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridTransferenciaTab2.Location = new System.Drawing.Point(21, 60);
            this.metroGridTransferenciaTab2.Name = "metroGridTransferenciaTab2";
            this.metroGridTransferenciaTab2.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridTransferenciaTab2.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.metroGridTransferenciaTab2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGridTransferenciaTab2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGridTransferenciaTab2.Size = new System.Drawing.Size(699, 117);
            this.metroGridTransferenciaTab2.TabIndex = 126;
            this.metroGridTransferenciaTab2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGridTransferenciaTab2_CellClick);
            this.metroGridTransferenciaTab2.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGridTransferenciaTab2_CellEndEdit);
            this.metroGridTransferenciaTab2.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.metroGridTransferenciaTab2_EditingControlShowing);
            // 
            // dataGridViewComboBoxTab2Mercadoria
            // 
            this.dataGridViewComboBoxTab2Mercadoria.HeaderText = "Mercadoria";
            this.dataGridViewComboBoxTab2Mercadoria.Name = "dataGridViewComboBoxTab2Mercadoria";
            // 
            // dataGridViewComboBoxTab2Referencia
            // 
            this.dataGridViewComboBoxTab2Referencia.HeaderText = "Referência";
            this.dataGridViewComboBoxTab2Referencia.Name = "dataGridViewComboBoxTab2Referencia";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Stock minimo";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "fieldStokReal";
            this.dataGridViewTextBoxColumn5.HeaderText = "Stock Real";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "fieldQuantidadeASerTransferida";
            this.dataGridViewTextBoxColumn6.HeaderText = "Quantidade a transferir";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metroTabPage3.Controls.Add(this.label23);
            this.metroTabPage3.Controls.Add(this.labelNomeLojaTab3);
            this.metroTabPage3.Controls.Add(this.label20);
            this.metroTabPage3.Controls.Add(this.labelOperadorTab3);
            this.metroTabPage3.Controls.Add(this.label4);
            this.metroTabPage3.Controls.Add(this.labelDataActualTab3);
            this.metroTabPage3.Controls.Add(this.label2);
            this.metroTabPage3.Controls.Add(this.groupBox3);
            this.metroTabPage3.Controls.Add(this.groupBox1);
            this.metroTabPage3.Controls.Add(this.txtETotal3);
            this.metroTabPage3.Controls.Add(this.txtETotal2);
            this.metroTabPage3.Controls.Add(this.txtETotal1);
            this.metroTabPage3.Controls.Add(this.metroLabel26);
            this.metroTabPage3.Controls.Add(this.txtEPCAP);
            this.metroTabPage3.Controls.Add(this.txtETotal);
            this.metroTabPage3.Controls.Add(this.txtEGuia);
            this.metroTabPage3.Controls.Add(this.txtENumero);
            this.metroTabPage3.Controls.Add(this.metroLabel25);
            this.metroTabPage3.Controls.Add(this.metroLabel23);
            this.metroTabPage3.Controls.Add(this.metroLabel19);
            this.metroTabPage3.Controls.Add(this.metroLabel17);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(744, 398);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Recepção de tranferências";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(3, 358);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(365, 13);
            this.label23.TabIndex = 157;
            this.label23.Text = "Atenção: é OBRIGATÓRIO escrever a localização de cada item a transferir.";
            // 
            // labelNomeLojaTab3
            // 
            this.labelNomeLojaTab3.AutoSize = true;
            this.labelNomeLojaTab3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelNomeLojaTab3.Location = new System.Drawing.Point(33, 6);
            this.labelNomeLojaTab3.Name = "labelNomeLojaTab3";
            this.labelNomeLojaTab3.Size = new System.Drawing.Size(0, 13);
            this.labelNomeLojaTab3.TabIndex = 156;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label20.Location = new System.Drawing.Point(-3, 6);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 13);
            this.label20.TabIndex = 155;
            this.label20.Text = "Loja:";
            // 
            // labelOperadorTab3
            // 
            this.labelOperadorTab3.AutoSize = true;
            this.labelOperadorTab3.Location = new System.Drawing.Point(225, 337);
            this.labelOperadorTab3.Name = "labelOperadorTab3";
            this.labelOperadorTab3.Size = new System.Drawing.Size(0, 13);
            this.labelOperadorTab3.TabIndex = 136;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(157, 337);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 135;
            this.label4.Text = "Operador:";
            // 
            // labelDataActualTab3
            // 
            this.labelDataActualTab3.AutoSize = true;
            this.labelDataActualTab3.Location = new System.Drawing.Point(70, 337);
            this.labelDataActualTab3.Name = "labelDataActualTab3";
            this.labelDataActualTab3.Size = new System.Drawing.Size(0, 13);
            this.labelDataActualTab3.TabIndex = 134;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 337);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Data actual:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.metroGridTransferenciaTab3);
            this.groupBox3.Location = new System.Drawing.Point(0, 147);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(741, 179);
            this.groupBox3.TabIndex = 132;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Destino";
            // 
            // metroGridTransferenciaTab3
            // 
            this.metroGridTransferenciaTab3.AllowUserToResizeRows = false;
            this.metroGridTransferenciaTab3.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridTransferenciaTab3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGridTransferenciaTab3.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGridTransferenciaTab3.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridTransferenciaTab3.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.metroGridTransferenciaTab3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGridTransferenciaTab3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.fieldMercadoriaDestino,
            this.fieldLocalizacao,
            this.fieldReferencia,
            this.fieldDescricao,
            this.fieldQuantidade});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGridTransferenciaTab3.DefaultCellStyle = dataGridViewCellStyle8;
            this.metroGridTransferenciaTab3.EnableHeadersVisualStyles = false;
            this.metroGridTransferenciaTab3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGridTransferenciaTab3.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metroGridTransferenciaTab3.Location = new System.Drawing.Point(21, 21);
            this.metroGridTransferenciaTab3.Name = "metroGridTransferenciaTab3";
            this.metroGridTransferenciaTab3.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGridTransferenciaTab3.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.metroGridTransferenciaTab3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGridTransferenciaTab3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGridTransferenciaTab3.Size = new System.Drawing.Size(702, 152);
            this.metroGridTransferenciaTab3.TabIndex = 127;
            // 
            // fieldMercadoriaDestino
            // 
            this.fieldMercadoriaDestino.HeaderText = "Mercadoria destino";
            this.fieldMercadoriaDestino.Name = "fieldMercadoriaDestino";
            this.fieldMercadoriaDestino.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fieldMercadoriaDestino.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // fieldLocalizacao
            // 
            this.fieldLocalizacao.HeaderText = "Localização";
            this.fieldLocalizacao.Name = "fieldLocalizacao";
            this.fieldLocalizacao.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fieldLocalizacao.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // fieldReferencia
            // 
            this.fieldReferencia.HeaderText = "Referência";
            this.fieldReferencia.Name = "fieldReferencia";
            this.fieldReferencia.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.fieldReferencia.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // fieldDescricao
            // 
            this.fieldDescricao.HeaderText = "Descrição";
            this.fieldDescricao.Name = "fieldDescricao";
            // 
            // fieldQuantidade
            // 
            this.fieldQuantidade.HeaderText = "Quantidade";
            this.fieldQuantidade.Name = "fieldQuantidade";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelDataDeEmissao);
            this.groupBox1.Controls.Add(this.labelUsuarioQueEmitiu);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBox3);
            this.groupBox1.Controls.Add(this.labelGuia);
            this.groupBox1.Location = new System.Drawing.Point(0, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(741, 94);
            this.groupBox1.TabIndex = 131;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Origem";
            // 
            // labelDataDeEmissao
            // 
            this.labelDataDeEmissao.AutoSize = true;
            this.labelDataDeEmissao.Location = new System.Drawing.Point(291, 60);
            this.labelDataDeEmissao.Name = "labelDataDeEmissao";
            this.labelDataDeEmissao.Size = new System.Drawing.Size(0, 13);
            this.labelDataDeEmissao.TabIndex = 163;
            // 
            // labelUsuarioQueEmitiu
            // 
            this.labelUsuarioQueEmitiu.AutoSize = true;
            this.labelUsuarioQueEmitiu.Location = new System.Drawing.Point(560, 64);
            this.labelUsuarioQueEmitiu.Name = "labelUsuarioQueEmitiu";
            this.labelUsuarioQueEmitiu.Size = new System.Drawing.Size(0, 13);
            this.labelUsuarioQueEmitiu.TabIndex = 162;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(492, 64);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(62, 13);
            this.label22.TabIndex = 161;
            this.label22.Text = "Emitido por:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(194, 60);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 13);
            this.label21.TabIndex = 160;
            this.label21.Text = "Data de emissão: ";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(68, 57);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 159;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 158;
            this.label1.Text = "ID Doc.";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(68, 23);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(486, 21);
            this.comboBox3.TabIndex = 157;
            // 
            // labelGuia
            // 
            this.labelGuia.AutoSize = true;
            this.labelGuia.Location = new System.Drawing.Point(18, 27);
            this.labelGuia.Name = "labelGuia";
            this.labelGuia.Size = new System.Drawing.Size(30, 13);
            this.labelGuia.TabIndex = 128;
            this.labelGuia.Text = "Loja:";
            // 
            // txtETotal3
            // 
            this.txtETotal3.Lines = new string[0];
            this.txtETotal3.Location = new System.Drawing.Point(319, 401);
            this.txtETotal3.MaxLength = 32767;
            this.txtETotal3.Name = "txtETotal3";
            this.txtETotal3.PasswordChar = '\0';
            this.txtETotal3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtETotal3.SelectedText = "";
            this.txtETotal3.Size = new System.Drawing.Size(92, 23);
            this.txtETotal3.TabIndex = 130;
            this.txtETotal3.UseSelectable = true;
            // 
            // txtETotal2
            // 
            this.txtETotal2.Lines = new string[0];
            this.txtETotal2.Location = new System.Drawing.Point(197, 401);
            this.txtETotal2.MaxLength = 32767;
            this.txtETotal2.Name = "txtETotal2";
            this.txtETotal2.PasswordChar = '\0';
            this.txtETotal2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtETotal2.SelectedText = "";
            this.txtETotal2.Size = new System.Drawing.Size(92, 23);
            this.txtETotal2.TabIndex = 129;
            this.txtETotal2.UseSelectable = true;
            // 
            // txtETotal1
            // 
            this.txtETotal1.Lines = new string[0];
            this.txtETotal1.Location = new System.Drawing.Point(61, 401);
            this.txtETotal1.MaxLength = 32767;
            this.txtETotal1.Name = "txtETotal1";
            this.txtETotal1.PasswordChar = '\0';
            this.txtETotal1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtETotal1.SelectedText = "";
            this.txtETotal1.Size = new System.Drawing.Size(92, 23);
            this.txtETotal1.TabIndex = 128;
            this.txtETotal1.UseSelectable = true;
            // 
            // metroLabel26
            // 
            this.metroLabel26.AutoSize = true;
            this.metroLabel26.Location = new System.Drawing.Point(-4, 405);
            this.metroLabel26.Name = "metroLabel26";
            this.metroLabel26.Size = new System.Drawing.Size(36, 19);
            this.metroLabel26.TabIndex = 127;
            this.metroLabel26.Text = "Total";
            // 
            // txtEPCAP
            // 
            this.txtEPCAP.Lines = new string[0];
            this.txtEPCAP.Location = new System.Drawing.Point(912, 131);
            this.txtEPCAP.MaxLength = 32767;
            this.txtEPCAP.Name = "txtEPCAP";
            this.txtEPCAP.PasswordChar = '\0';
            this.txtEPCAP.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEPCAP.SelectedText = "";
            this.txtEPCAP.Size = new System.Drawing.Size(73, 23);
            this.txtEPCAP.TabIndex = 124;
            this.txtEPCAP.UseSelectable = true;
            // 
            // txtETotal
            // 
            this.txtETotal.Lines = new string[0];
            this.txtETotal.Location = new System.Drawing.Point(902, 166);
            this.txtETotal.MaxLength = 32767;
            this.txtETotal.Name = "txtETotal";
            this.txtETotal.PasswordChar = '\0';
            this.txtETotal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtETotal.SelectedText = "";
            this.txtETotal.Size = new System.Drawing.Size(87, 23);
            this.txtETotal.TabIndex = 121;
            this.txtETotal.UseSelectable = true;
            // 
            // txtEGuia
            // 
            this.txtEGuia.Lines = new string[0];
            this.txtEGuia.Location = new System.Drawing.Point(898, 100);
            this.txtEGuia.MaxLength = 32767;
            this.txtEGuia.Name = "txtEGuia";
            this.txtEGuia.PasswordChar = '\0';
            this.txtEGuia.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtEGuia.SelectedText = "";
            this.txtEGuia.Size = new System.Drawing.Size(87, 23);
            this.txtEGuia.TabIndex = 116;
            this.txtEGuia.UseSelectable = true;
            // 
            // txtENumero
            // 
            this.txtENumero.Lines = new string[0];
            this.txtENumero.Location = new System.Drawing.Point(895, 64);
            this.txtENumero.MaxLength = 32767;
            this.txtENumero.Name = "txtENumero";
            this.txtENumero.PasswordChar = '\0';
            this.txtENumero.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtENumero.SelectedText = "";
            this.txtENumero.Size = new System.Drawing.Size(87, 23);
            this.txtENumero.TabIndex = 114;
            this.txtENumero.UseSelectable = true;
            // 
            // metroLabel25
            // 
            this.metroLabel25.AutoSize = true;
            this.metroLabel25.Location = new System.Drawing.Point(835, 170);
            this.metroLabel25.Name = "metroLabel25";
            this.metroLabel25.Size = new System.Drawing.Size(36, 19);
            this.metroLabel25.TabIndex = 106;
            this.metroLabel25.Text = "Total";
            // 
            // metroLabel23
            // 
            this.metroLabel23.AutoSize = true;
            this.metroLabel23.Location = new System.Drawing.Point(849, 135);
            this.metroLabel23.Name = "metroLabel23";
            this.metroLabel23.Size = new System.Drawing.Size(47, 19);
            this.metroLabel23.TabIndex = 104;
            this.metroLabel23.Text = "PCA P";
            // 
            // metroLabel19
            // 
            this.metroLabel19.AutoSize = true;
            this.metroLabel19.Location = new System.Drawing.Point(819, 100);
            this.metroLabel19.Name = "metroLabel19";
            this.metroLabel19.Size = new System.Drawing.Size(35, 19);
            this.metroLabel19.TabIndex = 100;
            this.metroLabel19.Text = "Guia";
            // 
            // metroLabel17
            // 
            this.metroLabel17.AutoSize = true;
            this.metroLabel17.Location = new System.Drawing.Point(819, 64);
            this.metroLabel17.Name = "metroLabel17";
            this.metroLabel17.Size = new System.Drawing.Size(58, 19);
            this.metroLabel17.TabIndex = 98;
            this.metroLabel17.Text = "Número";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bntSair);
            this.groupBox2.Controls.Add(this.bntImprimir);
            this.groupBox2.Controls.Add(this.bntReceber);
            this.groupBox2.Controls.Add(this.bntTransferir);
            this.groupBox2.Location = new System.Drawing.Point(54, 63);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(745, 47);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // bntSair
            // 
            this.bntSair.Location = new System.Drawing.Point(516, 16);
            this.bntSair.Name = "bntSair";
            this.bntSair.Size = new System.Drawing.Size(132, 23);
            this.bntSair.TabIndex = 3;
            this.bntSair.Text = "Sair";
            this.bntSair.UseVisualStyleBackColor = true;
            this.bntSair.Click += new System.EventHandler(this.button4_Click);
            // 
            // bntImprimir
            // 
            this.bntImprimir.Location = new System.Drawing.Point(378, 16);
            this.bntImprimir.Name = "bntImprimir";
            this.bntImprimir.Size = new System.Drawing.Size(132, 23);
            this.bntImprimir.TabIndex = 2;
            this.bntImprimir.Text = "Imprimir";
            this.bntImprimir.UseVisualStyleBackColor = true;
            // 
            // bntReceber
            // 
            this.bntReceber.Location = new System.Drawing.Point(233, 16);
            this.bntReceber.Name = "bntReceber";
            this.bntReceber.Size = new System.Drawing.Size(132, 23);
            this.bntReceber.TabIndex = 1;
            this.bntReceber.Text = "Receber";
            this.bntReceber.UseVisualStyleBackColor = true;
            // 
            // bntTransferir
            // 
            this.bntTransferir.Location = new System.Drawing.Point(83, 16);
            this.bntTransferir.Name = "bntTransferir";
            this.bntTransferir.Size = new System.Drawing.Size(132, 23);
            this.bntTransferir.TabIndex = 0;
            this.bntTransferir.Text = "Transferir";
            this.bntTransferir.UseVisualStyleBackColor = true;
            this.bntTransferir.Click += new System.EventHandler(this.bntTransferir_Click);
            // 
            // FormTranferencias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 537);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.labelNomeLojaTab1);
            this.Name = "FormTranferencias";
            this.Text = "Tranferências";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormTranferencias_Loaf);
            this.labelNomeLojaTab1.ResumeLayout(false);
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridTransferenciaTab1)).EndInit();
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metroGridTransferenciaTab2)).EndInit();
            this.metroTabPage3.ResumeLayout(false);
            this.metroTabPage3.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metroGridTransferenciaTab3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl labelNomeLojaTab1;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroTextBox txtETotal3;
        private MetroFramework.Controls.MetroTextBox txtETotal2;
        private MetroFramework.Controls.MetroTextBox txtETotal1;
        private MetroFramework.Controls.MetroLabel metroLabel26;
        private MetroFramework.Controls.MetroTextBox txtEPCAP;
        private MetroFramework.Controls.MetroTextBox txtETotal;
        private MetroFramework.Controls.MetroTextBox txtEGuia;
        private MetroFramework.Controls.MetroTextBox txtENumero;
        private MetroFramework.Controls.MetroLabel metroLabel25;
        private MetroFramework.Controls.MetroLabel metroLabel23;
        private MetroFramework.Controls.MetroLabel metroLabel19;
        private MetroFramework.Controls.MetroLabel metroLabel17;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelGuia;
        private System.Windows.Forms.Label labelOperadorTab3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelDataActualTab3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bntSair;
        private System.Windows.Forms.Button bntImprimir;
        private System.Windows.Forms.Button bntReceber;
        private System.Windows.Forms.Button bntTransferir;
        private System.Windows.Forms.Label labelLojaCorrente;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelOperadorTab1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelDataActualTab1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox comboBoxArmazemDestinoTab1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox cbSoArmazensVenda;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNumeroDaGuiaTab1;
        private MetroFramework.Controls.MetroGrid metroGridTransferenciaTab1;
        private System.Windows.Forms.Label labelNomeLojaTab2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label labelOperadorTab2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label labelDataActualTab2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox comboBoxLojasTab2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtNumeroDaGuiaTab2;
        private MetroFramework.Controls.MetroGrid metroGridTransferenciaTab2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label labelNomeLojaTab3;
        private System.Windows.Forms.Label label20;
        private MetroFramework.Controls.MetroGrid metroGridTransferenciaTab3;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldMercadoriaDestino;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldLocalizacao;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldReferencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldDescricao;
        private System.Windows.Forms.DataGridViewTextBoxColumn fieldQuantidade;
        private System.Windows.Forms.Label labelDataDeEmissao;
        private System.Windows.Forms.Label labelUsuarioQueEmitiu;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxTab1Mercadoria;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxTab1Referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxTab2Mercadoria;
        private System.Windows.Forms.DataGridViewComboBoxColumn dataGridViewComboBoxTab2Referencia;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    }
}