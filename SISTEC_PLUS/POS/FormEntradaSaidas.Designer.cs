﻿namespace SISTEC_PLUS.POS
{
    partial class FormEntradaSaidas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEntradaSaidas));
            this.cmbLoja = new MetroFramework.Controls.MetroComboBox();
            this.cmbMercadoria = new MetroFramework.Controls.MetroComboBox();
            this.cmbReferencia = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel13 = new MetroFramework.Controls.MetroLabel();
            this.txtPrecoFob = new MetroFramework.Controls.MetroTextBox();
            this.txtPadraoCusto = new MetroFramework.Controls.MetroTextBox();
            this.txtPrecoVenda = new MetroFramework.Controls.MetroTextBox();
            this.txtDataFabrico = new MetroFramework.Controls.MetroTextBox();
            this.txtDataExpiracao = new MetroFramework.Controls.MetroTextBox();
            this.txtQtdTotal = new MetroFramework.Controls.MetroTextBox();
            this.txtQtdAcerto = new MetroFramework.Controls.MetroTextBox();
            this.txtStock = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel14 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel15 = new MetroFramework.Controls.MetroLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btGravar = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.btSair = new System.Windows.Forms.ToolStripButton();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStrip1.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbLoja
            // 
            this.cmbLoja.FormattingEnabled = true;
            this.cmbLoja.ItemHeight = 23;
            this.cmbLoja.Location = new System.Drawing.Point(135, 130);
            this.cmbLoja.Name = "cmbLoja";
            this.cmbLoja.Size = new System.Drawing.Size(348, 29);
            this.cmbLoja.TabIndex = 0;
            this.cmbLoja.UseSelectable = true;
            // 
            // cmbMercadoria
            // 
            this.cmbMercadoria.FormattingEnabled = true;
            this.cmbMercadoria.ItemHeight = 23;
            this.cmbMercadoria.Location = new System.Drawing.Point(135, 165);
            this.cmbMercadoria.Name = "cmbMercadoria";
            this.cmbMercadoria.Size = new System.Drawing.Size(348, 29);
            this.cmbMercadoria.TabIndex = 1;
            this.cmbMercadoria.UseSelectable = true;
            // 
            // cmbReferencia
            // 
            this.cmbReferencia.FormattingEnabled = true;
            this.cmbReferencia.ItemHeight = 23;
            this.cmbReferencia.Location = new System.Drawing.Point(135, 200);
            this.cmbReferencia.Name = "cmbReferencia";
            this.cmbReferencia.Size = new System.Drawing.Size(348, 29);
            this.cmbReferencia.TabIndex = 2;
            this.cmbReferencia.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(31, 175);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(77, 19);
            this.metroLabel1.TabIndex = 3;
            this.metroLabel1.Text = "Mercadoria";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(31, 210);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(70, 19);
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "Referência";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(31, 258);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(51, 19);
            this.metroLabel3.TabIndex = 5;
            this.metroLabel3.Text = "Familia";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(31, 277);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(79, 19);
            this.metroLabel4.TabIndex = 6;
            this.metroLabel4.Text = "Sub-Familia";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(29, 321);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(73, 19);
            this.metroLabel5.TabIndex = 7;
            this.metroLabel5.Text = "Preço FOB";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(27, 354);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(106, 19);
            this.metroLabel6.TabIndex = 8;
            this.metroLabel6.Text = "Pr. Padrão custo";
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(27, 382);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(101, 19);
            this.metroLabel7.TabIndex = 9;
            this.metroLabel7.Text = "Preço de venda";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(29, 411);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(81, 19);
            this.metroLabel8.TabIndex = 10;
            this.metroLabel8.Text = "Data fabrico";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(27, 440);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(116, 19);
            this.metroLabel9.TabIndex = 11;
            this.metroLabel9.Text = "Data de expiração";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(31, 133);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(33, 19);
            this.metroLabel10.TabIndex = 12;
            this.metroLabel10.Text = "Loja";
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(308, 321);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(94, 19);
            this.metroLabel11.TabIndex = 13;
            this.metroLabel11.Text = "Stock Existente";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(308, 354);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(79, 19);
            this.metroLabel12.TabIndex = 14;
            this.metroLabel12.Text = "QTD Acerto";
            // 
            // metroLabel13
            // 
            this.metroLabel13.AutoSize = true;
            this.metroLabel13.Location = new System.Drawing.Point(308, 382);
            this.metroLabel13.Name = "metroLabel13";
            this.metroLabel13.Size = new System.Drawing.Size(66, 19);
            this.metroLabel13.TabIndex = 15;
            this.metroLabel13.Text = "QTD total";
            // 
            // txtPrecoFob
            // 
            this.txtPrecoFob.Lines = new string[0];
            this.txtPrecoFob.Location = new System.Drawing.Point(149, 321);
            this.txtPrecoFob.MaxLength = 32767;
            this.txtPrecoFob.Name = "txtPrecoFob";
            this.txtPrecoFob.PasswordChar = '\0';
            this.txtPrecoFob.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrecoFob.SelectedText = "";
            this.txtPrecoFob.Size = new System.Drawing.Size(75, 23);
            this.txtPrecoFob.TabIndex = 16;
            this.txtPrecoFob.UseSelectable = true;
            // 
            // txtPadraoCusto
            // 
            this.txtPadraoCusto.Lines = new string[0];
            this.txtPadraoCusto.Location = new System.Drawing.Point(149, 354);
            this.txtPadraoCusto.MaxLength = 32767;
            this.txtPadraoCusto.Name = "txtPadraoCusto";
            this.txtPadraoCusto.PasswordChar = '\0';
            this.txtPadraoCusto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPadraoCusto.SelectedText = "";
            this.txtPadraoCusto.Size = new System.Drawing.Size(75, 23);
            this.txtPadraoCusto.TabIndex = 17;
            this.txtPadraoCusto.UseSelectable = true;
            // 
            // txtPrecoVenda
            // 
            this.txtPrecoVenda.Lines = new string[0];
            this.txtPrecoVenda.Location = new System.Drawing.Point(149, 382);
            this.txtPrecoVenda.MaxLength = 32767;
            this.txtPrecoVenda.Name = "txtPrecoVenda";
            this.txtPrecoVenda.PasswordChar = '\0';
            this.txtPrecoVenda.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPrecoVenda.SelectedText = "";
            this.txtPrecoVenda.Size = new System.Drawing.Size(75, 23);
            this.txtPrecoVenda.TabIndex = 18;
            this.txtPrecoVenda.UseSelectable = true;
            // 
            // txtDataFabrico
            // 
            this.txtDataFabrico.Lines = new string[0];
            this.txtDataFabrico.Location = new System.Drawing.Point(149, 411);
            this.txtDataFabrico.MaxLength = 32767;
            this.txtDataFabrico.Name = "txtDataFabrico";
            this.txtDataFabrico.PasswordChar = '\0';
            this.txtDataFabrico.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDataFabrico.SelectedText = "";
            this.txtDataFabrico.Size = new System.Drawing.Size(75, 23);
            this.txtDataFabrico.TabIndex = 19;
            this.txtDataFabrico.UseSelectable = true;
            // 
            // txtDataExpiracao
            // 
            this.txtDataExpiracao.Lines = new string[0];
            this.txtDataExpiracao.Location = new System.Drawing.Point(149, 440);
            this.txtDataExpiracao.MaxLength = 32767;
            this.txtDataExpiracao.Name = "txtDataExpiracao";
            this.txtDataExpiracao.PasswordChar = '\0';
            this.txtDataExpiracao.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDataExpiracao.SelectedText = "";
            this.txtDataExpiracao.Size = new System.Drawing.Size(75, 23);
            this.txtDataExpiracao.TabIndex = 20;
            this.txtDataExpiracao.UseSelectable = true;
            // 
            // txtQtdTotal
            // 
            this.txtQtdTotal.Lines = new string[0];
            this.txtQtdTotal.Location = new System.Drawing.Point(408, 382);
            this.txtQtdTotal.MaxLength = 32767;
            this.txtQtdTotal.Name = "txtQtdTotal";
            this.txtQtdTotal.PasswordChar = '\0';
            this.txtQtdTotal.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtQtdTotal.SelectedText = "";
            this.txtQtdTotal.Size = new System.Drawing.Size(75, 23);
            this.txtQtdTotal.TabIndex = 23;
            this.txtQtdTotal.UseSelectable = true;
            // 
            // txtQtdAcerto
            // 
            this.txtQtdAcerto.Lines = new string[0];
            this.txtQtdAcerto.Location = new System.Drawing.Point(408, 354);
            this.txtQtdAcerto.MaxLength = 32767;
            this.txtQtdAcerto.Name = "txtQtdAcerto";
            this.txtQtdAcerto.PasswordChar = '\0';
            this.txtQtdAcerto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtQtdAcerto.SelectedText = "";
            this.txtQtdAcerto.Size = new System.Drawing.Size(75, 23);
            this.txtQtdAcerto.TabIndex = 22;
            this.txtQtdAcerto.UseSelectable = true;
            // 
            // txtStock
            // 
            this.txtStock.Lines = new string[0];
            this.txtStock.Location = new System.Drawing.Point(408, 321);
            this.txtStock.MaxLength = 32767;
            this.txtStock.Name = "txtStock";
            this.txtStock.PasswordChar = '\0';
            this.txtStock.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtStock.SelectedText = "";
            this.txtStock.Size = new System.Drawing.Size(75, 23);
            this.txtStock.TabIndex = 21;
            this.txtStock.UseSelectable = true;
            // 
            // metroLabel14
            // 
            this.metroLabel14.AutoSize = true;
            this.metroLabel14.Location = new System.Drawing.Point(308, 440);
            this.metroLabel14.Name = "metroLabel14";
            this.metroLabel14.Size = new System.Drawing.Size(78, 19);
            this.metroLabel14.TabIndex = 24;
            this.metroLabel14.Text = "(-) Redução";
            // 
            // metroLabel15
            // 
            this.metroLabel15.AutoSize = true;
            this.metroLabel15.Location = new System.Drawing.Point(408, 440);
            this.metroLabel15.Name = "metroLabel15";
            this.metroLabel15.Size = new System.Drawing.Size(67, 19);
            this.metroLabel15.TabIndex = 25;
            this.metroLabel15.Text = "(+)Adição";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btGravar,
            this.toolStripButton2,
            this.btSair});
            this.toolStrip1.Location = new System.Drawing.Point(171, 82);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(188, 25);
            this.toolStrip1.TabIndex = 89;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btGravar
            // 
            this.btGravar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btGravar.Image = ((System.Drawing.Image)(resources.GetObject("btGravar.Image")));
            this.btGravar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btGravar.Name = "btGravar";
            this.btGravar.Size = new System.Drawing.Size(61, 22);
            this.btGravar.Text = "Gravar";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(78, 22);
            this.toolStripButton2.Text = "Consultar";
            // 
            // btSair
            // 
            this.btSair.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btSair.Image = ((System.Drawing.Image)(resources.GetObject("btSair.Image")));
            this.btSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btSair.Name = "btSair";
            this.btSair.Size = new System.Drawing.Size(46, 22);
            this.btSair.Text = "Sair";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(301, 2);
            this.toolStripContainer1.Location = new System.Drawing.Point(171, 82);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(301, 27);
            this.toolStripContainer1.TabIndex = 90;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.toolStripContainer1.TopToolStripPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStripContainer1.TopToolStripPanel.Tag = "XC";
            // 
            // FormEntradaSaidas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 483);
            this.ControlBox = false;
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.metroLabel15);
            this.Controls.Add(this.metroLabel14);
            this.Controls.Add(this.txtQtdTotal);
            this.Controls.Add(this.txtQtdAcerto);
            this.Controls.Add(this.txtStock);
            this.Controls.Add(this.txtDataExpiracao);
            this.Controls.Add(this.txtDataFabrico);
            this.Controls.Add(this.txtPrecoVenda);
            this.Controls.Add(this.txtPadraoCusto);
            this.Controls.Add(this.txtPrecoFob);
            this.Controls.Add(this.metroLabel13);
            this.Controls.Add(this.metroLabel12);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.cmbReferencia);
            this.Controls.Add(this.cmbMercadoria);
            this.Controls.Add(this.cmbLoja);
            this.Name = "FormEntradaSaidas";
            this.Text = "Entradas/Saidas/Ajustes";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.FormEntradaSaidas_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox cmbLoja;
        private MetroFramework.Controls.MetroComboBox cmbMercadoria;
        private MetroFramework.Controls.MetroComboBox cmbReferencia;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel metroLabel13;
        private MetroFramework.Controls.MetroTextBox txtPrecoFob;
        private MetroFramework.Controls.MetroTextBox txtPadraoCusto;
        private MetroFramework.Controls.MetroTextBox txtPrecoVenda;
        private MetroFramework.Controls.MetroTextBox txtDataFabrico;
        private MetroFramework.Controls.MetroTextBox txtDataExpiracao;
        private MetroFramework.Controls.MetroTextBox txtQtdTotal;
        private MetroFramework.Controls.MetroTextBox txtQtdAcerto;
        private MetroFramework.Controls.MetroTextBox txtStock;
        private MetroFramework.Controls.MetroLabel metroLabel14;
        private MetroFramework.Controls.MetroLabel metroLabel15;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btGravar;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton btSair;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
    }
}