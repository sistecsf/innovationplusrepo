﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using System.Data.SqlClient;

namespace SISTEC_PLUS.POS
{
    public partial class FormNotaDevolucao : MetroFramework.Forms.MetroForm
    {
        SqlConnection conexao = new SqlConnection(Variavel.Conexao);
        DataTable dataTable; // deficnicao do dataable
        DataView dataView;  // definicao do data view
        SqlDataAdapter dataAdapter;  // definicao do comando dataadapter
        DataSet dataSet;             // definicao de um dataset
        public FormNotaDevolucao()
        {
            InitializeComponent();
        }
        private void limparCampos()
        {
            txtID.Text = "";
            lbNumero.Text = "";
            lbCodigo.Text = "";
            lbDescricao.Text = "";
            lbUtilizador.Text = "";
            lbCliente.Text = "";
            cmbCaixaDevolucao.Text = "";
            cmbCaixaDevolucao.PromptText = "";
            metroGrid1.DataSource = null;
            metroGrid1.Rows.Clear();
            lbMoedaNacional.Text = "0";
            lbValorPadrao.Text = "0";
        }
        private void gravar()
        {

        }
        private void consultar()
        {

        }
        private void preenxerComboBox(ComboBox combo, string sql)
        {
            SqlCommand cmd = new SqlCommand(sql, conexao);
            cmd.Connection = conexao;
            try
            {
                conexao.Open();
                SqlDataReader leitor = cmd.ExecuteReader();
                while (leitor.Read()) { combo.Items.Add(leitor.GetValue(0)); }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conexao.Close();
            }
        }
        private void FormNotaDevolucao_Load(object sender, EventArgs e)
        {
            limparCampos();
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }
    }
}
