﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;

namespace SISTEC_PLUS
{
    public partial class FormMessagem2 : MetroFramework.Forms.MetroForm
    {
        public FormMessagem2(string messagem)
        {
            InitializeComponent();
            txtMessagem.Text = messagem;
        }

        private void FormMessagem2_Load(object sender, EventArgs e)
        {

        }

        private void btSim_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
        }

        private void btNao_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
        }
    }
}
