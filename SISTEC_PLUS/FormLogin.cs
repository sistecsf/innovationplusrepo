﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework;
using SISTEC_PLUS;
using System.Data.SqlClient;

namespace SISTEC_PLUS
{
    public partial class FormLogin : MetroFramework.Forms.MetroForm
    {
        int count1 = 0, count2 = 0;
        string count;
        SqlConnection conexao = new SqlConnection(Variavel.Conexao); // conexão co o banco
        public FormLogin()
        {
            InitializeComponent();
        }
        private void Variaveis()
        {
            SqlCommand cmd = new SqlCommand("select Convert(nvarchar(100),PasswordEncrypted,1)  from Utilizadores where Login=@login AND Anulado IS NULL", conexao); // buscar a string criptografada
            cmd.Parameters.Add(new SqlParameter("@login", txtUtilizador.Text)); //deu se valor ao parametro 
            conexao.Open();
            try
            {
                count = (string)cmd.ExecuteScalar(); //passar na variavel count o valor retornado
            }
            catch { count = ""; }
            if (count != "")
            {
                string resultado = "select pwdCompare(@password, convert(varbinary(100),@passwordEncrypted,1),0)"; // buscar a string o password
                SqlCommand cmd1 = new SqlCommand(resultado, conexao);
                cmd1.Parameters.Add(new SqlParameter("@passwordEncrypted", count)); //deu se valor ao parametro
                cmd1.Parameters.Add(new SqlParameter("@password", txtPalavraPasse.Text)); //deu se valor ao parametro

                try
                {
                    count1 = (int)cmd1.ExecuteScalar(); //passar na variavel count1 o valor retornado
                }
                catch { count1 = 0; }

                Variavel.Login = txtUtilizador.Text;

                SqlCommand cmd2 = new SqlCommand("select LTrim(RTrim(CodLoja)), LTrim(RTrim(CodUtilizador)), RTRIM(Nomeutilizador), Email, Contakz, Contausd, Contaval, Opercx, Criador, Desconto, MultiLoja  from Utilizadores (NOLOCK) where Login='" + Variavel.Login + "'", conexao);
                using (SqlDataReader reader = cmd2.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Variavel.codLoja = reader[0].ToString();
                        Variavel.codUtilizador = reader[1].ToString();
                        Variavel.nomeUtilizador = reader[2].ToString();
                        Variavel.email = reader[3].ToString();
                        Variavel.contakz = reader[4].ToString();
                        Variavel.contausd = reader[5].ToString();
                        Variavel.contaval = reader[6].ToString();
                        Variavel.opercx = reader[7].ToString();
                        Variavel.criador = reader[8].ToString();
                        Variavel.desconto = reader[9].ToString();
                        Variavel.multiLoja = reader[10].ToString();
                    }
                }
                ///*
                SqlCommand cmd10 = new SqlCommand(@"Select CONVERT(VARCHAR(20),GETDATE(),120)", conexao);
                cmd10.Connection = conexao;
                Variavel.dataAtual = Convert.ToDateTime(cmd10.ExecuteScalar());

                SqlCommand cmd3 = new SqlCommand("SELECT A.CODAO, A.NOMEAO FROM AIAREAORGANICA A, UTILIZADORES U WITH (NOLOCK) where Login='" + txtUtilizador.Text + "' and A.CODAO=U.CODAO", conexao);
                using (SqlDataReader reader = cmd3.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Variavel.codArea = reader[0].ToString();
                        Variavel.nomeArea = reader[1].ToString();
                    }
                }

                SqlCommand comd = new SqlCommand("select CODEMPRESA, NOME, IsNull(OWNER,'N'), EMAIL, NOME_SUP, NIF, CONTAKZ, CONTAUSD, IBANKZ, IBANUSD, IsNull(CodigoInterno,0)  from EMPRESA (NOLOCK)", conexao);
                using (SqlDataReader reader = comd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Empreza.sCodEmp = reader[0].ToString();
                        Empreza.sNomeEmp = reader[1].ToString();
                        Empreza.sOwner_Empresa = reader[2].ToString();
                        Empreza.sEmpMail = reader[3].ToString();
                        Empreza.sNome_Sup = reader[4].ToString();
                        Empreza.sEmpresaNIF = reader[5].ToString();
                        Empreza.sEmpresaContaKZ = reader[6].ToString();
                        Empreza.sEmpresaContaUSD = reader[7].ToString();
                        Empreza.sEmpresaIBANAKZ = reader[8].ToString();
                        Empreza.sEmpresaIBANUSD = reader[9].ToString();
                        Empreza.nCodigoInterno = reader[10].ToString();
                    }
                }

                SqlCommand cmd4 = new SqlCommand("SELECT RTRIM(NOMELOJA), CodContaNAC, CodContaPDR, CodContaOUTROS,LTrim(RTrim(CodigoEntidade)), Morada, fone, Caixa_Post From ASLOJA WITH (NOLOCK) where CodLoja = '" + Variavel.codLoja + "'", conexao);
                using (SqlDataReader reader = cmd4.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Variavel.nomeLoja = reader[0].ToString();
                        Variavel.contaNacional = reader[1].ToString();
                        Variavel.contaPadao = reader[2].ToString();
                        Variavel.contaOutros = reader[3].ToString();
                        Variavel.codigoEntidade = reader[4].ToString();
                        Variavel.enderecoLoja = reader[5].ToString();
                        Variavel.telefoneLoja = reader[6].ToString();
                        Variavel.postalLoja = reader[7].ToString();
                    }
                }

                SqlCommand comand = new SqlCommand("SELECT PrgOrc, Prgcont, Prgcx, Prgcli, PrgEquip, Prggt, Prgman, PrgPos, PrgSTK, PrgUtil, PrgAudit, PrgContratos, PrgSalar, Distrib, Facturou, AltCambio, GrupoLogin, PrgHotel From Utilizadores (NoLock) Where Login= '" + Variavel.Login + "'", conexao);
                using (SqlDataReader reader = comand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Permissoes.nPrgOrc = reader[0].ToString();
                        Permissoes.nPrgCont = reader[1].ToString();
                        Permissoes.nPrgCx = reader[2].ToString();
                        Permissoes.nPrgClie = reader[3].ToString();
                        Permissoes.nPrgEquip = reader[4].ToString();
                        Permissoes.nPrgGt = reader[5].ToString();
                        Permissoes.nPrgMan = reader[6].ToString();
                        Permissoes.nPrgPos = reader[7].ToString();
                        Permissoes.nPrgSTK = reader[8].ToString();
                        Permissoes.nPrgUtil = reader[9].ToString();
                        Permissoes.nPrgAudit = reader[10].ToString();
                        Permissoes.nPrgCtrto = reader[11].ToString();
                        Permissoes.nPrgSalar = reader[12].ToString();
                        Permissoes.sDistrib = reader[13].ToString();
                        Permissoes.sFact = reader[14].ToString();
                        Permissoes.sAltCambio = reader[15].ToString();
                        Permissoes.sGrupoLogin = reader[16].ToString();
                        Permissoes.nPrgHotel = reader[17].ToString();
                    }
                }
                conexao.Close();
            }//fim do if (count != "")
        }
        private void validacao()
        {
            // aqui fez-se a autenticação do login a da senha
            try
            {
                Variaveis();
                count2 += 1;
                if (count2 < 3)
                {
                    if (count1 == 1)
                    {
                        Variavel.cambio = ""+Cambio();
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    else
                        panel1.Visible = true;
                }
                else
                {
                    this.DialogResult = DialogResult.Cancel;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                panel1.Visible = true;
            }
        }
        private double Cambio()
        {
            double cambio;
            conexao.Close();
            SqlCommand cmd = new SqlCommand("SELECT RTRIM(LTRIM(MAX( datacria))) FROM AFCAMBIO WITH (NOLOCK) Where Sigla='" + "KZ" + "'", conexao);
            cmd.Connection = conexao;
            conexao.Open();
            string data = (string)cmd.ExecuteScalar();
            conexao.Close();
            SqlCommand comd = new SqlCommand("select cambio from AFCAMBIO WHERE datacria ='" + data + "'", conexao);
            comd.Connection = conexao;
            conexao.Open();
            cambio = Convert.ToDouble(comd.ExecuteScalar());
            conexao.Close();
            return cambio;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            txtUtilizador.Select();
            panel1.Visible = false;
            Gerais gr = new Gerais();
            gr.lerParametro();
        }
        private void metroButton5_Click(object sender, EventArgs e)
        {
            if (txtPalavraPasse.Text != "" && txtUtilizador.Text != "")
            {
                validacao();
            }
            else
                MessageBox.Show("Penchimento Obrigatorio");
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        // botao e evento do text box da palavra passe ,para entrar na aplicacao depois de clique na tecla enter
        private void txtPalavraPasse_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                metroButton5_Click(sender, e);
            }
        }
    }
}
