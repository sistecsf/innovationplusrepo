﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Reporting.WinForms;
using Sistec_v2.Innov.BL.CRM.Reports;

namespace Sistec_v2.Innov.UI.CRM.Reports.Compras
{
    public partial class frmCompras : Form
    {
        public frmCompras()
        {
            InitializeComponent();
            txtUsername.Text = frmLogin.UserCode;
            
          

        }

        private void frmCompras_Load(object sender, EventArgs e)
        {
       
            //conversão formato da data para o sql
            dtpickerde.Format = DateTimePickerFormat.Custom;
            dtpickerde.CustomFormat = ("yyyy-MM-dd");
            dtpickera.Format = DateTimePickerFormat.Custom;
            dtpickera.CustomFormat = ("yyyy-MM-dd");

            txtcodigocliente.Enabled = true;
            cbcliente.Enabled = true;
            cbdocumento.Enabled = false;

           

        }

        private void btexecutar_Click(object sender, EventArgs e)
        {
            
            string codigo = textBox1.Text;

            //passagem de parâmetros (do form para o report)
            ReportParameter[] param = new ReportParameter[4];
            param[0] = new ReportParameter("content_de", dtpickerde.Text);
            param[1] = new ReportParameter("content_a", dtpickera.Text);
            param[2] = new ReportParameter("content", codigo);
            param[3] = new ReportParameter("content_user", txtUsername.Text);
            reportViewer.LocalReport.SetParameters(param);

            if (rbdetalhe.Checked)
            {
                reportViewer.LocalReport.ReportEmbeddedResource = "Sistec_v2.Innov.UI.CRM.Reports.Compras.comprasdetalherep.rdlc";
            }
            else
            {
                reportViewer.LocalReport.ReportEmbeddedResource = "Sistec_v2.Innov.UI.CRM.Reports.Compras.comprasresumorep.rdlc";
            }    
            
            if (codigo != "" && chktodos.Checked)
            {
                //passagem de parâmetros (do form para o report)
                param = new ReportParameter[4];
                param[0] = new ReportParameter("content_de", dtpickerde.Text);
                param[1] = new ReportParameter("content_a", dtpickera.Text);
                param[2] = new ReportParameter("content", codigo);
                param[3] = new ReportParameter("content_user", txtUsername.Text);
                reportViewer.LocalReport.SetParameters(param);
              
                var objCli =
                new CRepCompras(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString);

                var rds = new ReportDataSource("DataSet1", objCli.GetClientesDetalhe(dtpickerde.Text, dtpickera.Text, codigo));
                reportViewer.LocalReport.DataSources.Clear();
                reportViewer.LocalReport.DataSources.Add(rds);
                
                reportViewer.LocalReport.Refresh();

            
            }
           
            if (codigo != "" && cbdocumento.Text != "")
            {
               string doc = cbdocumento.SelectedValue.ToString();

                var objdoc =
                    new CRepCompras(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString);

                var rds = new ReportDataSource("DataSet1",
                    objdoc.GetClientesDocumento(dtpickerde.Text, dtpickera.Text, codigo, doc));
                reportViewer.LocalReport.DataSources.Clear();
                reportViewer.LocalReport.DataSources.Add(rds);

                reportViewer.LocalReport.Refresh();

            }

            if (codigo != "" && rbresumo.Checked)
            {
                //passagem de parâmetros (do form para o report)
                param = new ReportParameter[4];
                param[0] = new ReportParameter("content_de", dtpickerde.Text);
                param[1] = new ReportParameter("content_a", dtpickera.Text);
                param[2] = new ReportParameter("content", codigo);
                param[3] = new ReportParameter("content_user", txtUsername.Text);
                reportViewer.LocalReport.SetParameters(param);

                
                var objcli =
                   new CRepCompras(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString);

               var rds = new ReportDataSource("DataSet1",
                    objcli.GetClientesResumo(dtpickerde.Text, dtpickera.Text, codigo));

                reportViewer.LocalReport.DataSources.Clear();
                reportViewer.LocalReport.DataSources.Add(rds);
                
                reportViewer.LocalReport.Refresh();
            }

            else if (codigo == "")
            {
                MessageBox.Show("Deve preencher o Código do Cliente.", "Código Cliente");

            }

          
            reportViewer.RefreshReport();


        }


        private void dtpickerde_ValueChanged_1(object sender, EventArgs e)
        {
            dtpickerde.Format = DateTimePickerFormat.Custom;
            dtpickerde.CustomFormat = ("yyyy-MM-dd");
            dtpickera.Format = DateTimePickerFormat.Custom;
            dtpickera.CustomFormat = ("yyyy-MM-dd");

            DateTime de = dtpickerde.Value.Date;
            DateTime hoje = DateTime.Now.Date;

            int result = DateTime.Compare(hoje, de);


            if (result < 0)
            {
                MessageBox.Show("A data não pode ser menor que hoje.", "Data");
                dtpickerde.Text = hoje.ToShortDateString();
                de = dtpickerde.Value.Date;

            }

            DateTime a = dtpickera.Value.Date;

            result = DateTime.Compare(a, de);


            if (result < 0)
            {
                MessageBox.Show("Alterou a data de A.", "Data");
                dtpickera.Text = dtpickerde.Value.ToShortDateString();

            }
        }

        private void dtpickera_ValueChanged_1(object sender, EventArgs e)
        {
            dtpickerde.Format = DateTimePickerFormat.Custom;
            dtpickerde.CustomFormat = ("yyyy-MM-dd");
            dtpickera.Format = DateTimePickerFormat.Custom;
            dtpickera.CustomFormat = ("yyyy-MM-dd");

            DateTime de = dtpickerde.Value.Date;
            DateTime a = dtpickera.Value.Date;

            DateTime hoje = DateTime.Now.Date;

            int result = DateTime.Compare(hoje, a);


            if (result < 0)
            {
                MessageBox.Show("A data não pode ser maior que Hoje.", "Data");
                dtpickera.Text = hoje.ToShortDateString();

            }
            else
            {

                result = DateTime.Compare(a, de);


                if (result < 0)
                {
                    MessageBox.Show("A data não pode ser menor.", "Data");
                    dtpickera.Text = dtpickerde.Value.ToShortDateString();

                }
            }

        }


        private void chktodos_CheckedChanged_1(object sender, EventArgs e)
        {

            var objCli =
            new CRepCompras(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString);

            cbdocumento.DataSource = objCli.GetDocumento();
            cbdocumento.ValueMember = "DESCRDOC";

            if (chktodos.Checked)
            {

                cbdocumento.Enabled = false;
                cbdocumento.Text = null;

            }
            else
            {
                cbdocumento.Enabled = true;
 
            }
        }

        private void rbresumo_CheckedChanged(object sender, EventArgs e)
        {
            grpdocumento.Enabled = false;
            
        }

        private void rbdetalhe_CheckedChanged(object sender, EventArgs e)
        {
            grpdocumento.Enabled = true;
        }

        private void txtcodigocliente_TextChanged(object sender, EventArgs e)
        {

            string codigo = txtcodigocliente.Text;
            int countWords = codigo.Length;

            if (countWords >= 3)
            {
                cbcliente.SelectedText = "";

                var objCli =
                    new CRepCompras(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString);

                cbcliente.DataSource = objCli.GetClientesCodigo(codigo);
                cbcliente.ValueMember = "CodEntidade";
                cbcliente.DisplayMember = "NomeEntidade";

            }
        }

        private void cbcliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            

            DataRow row = ((DataTable)cbcliente.DataSource).Rows[cbcliente.SelectedIndex];
            string Id = (string)row["CodEntidade"];
            string Name = (string)row["NomeEntidade"];

            textBox1.Text = Id;
            textBox2.Text = Name;

           
        }

 

       
      
    }
}
