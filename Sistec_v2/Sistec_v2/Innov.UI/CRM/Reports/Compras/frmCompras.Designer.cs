﻿namespace Sistec_v2.Innov.UI.CRM.Reports.Compras
{
    partial class frmCompras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCompras));
            this.DataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btexecutar = new System.Windows.Forms.Button();
            this.grpcliente = new System.Windows.Forms.GroupBox();
            this.cbcliente = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtcodigocliente = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chktodos = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grpdata = new System.Windows.Forms.GroupBox();
            this.dtpickera = new System.Windows.Forms.DateTimePicker();
            this.dtpickerde = new System.Windows.Forms.DateTimePicker();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.reportViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.grpdocumento = new System.Windows.Forms.GroupBox();
            this.cbdocumento = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rbresumo = new System.Windows.Forms.RadioButton();
            this.comprasds = new Sistec_v2.Innov.UI.CRM.Reports.Compras.comprasds();
            this.rbdetalhe = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).BeginInit();
            this.grpcliente.SuspendLayout();
            this.grpdata.SuspendLayout();
            this.grpdocumento.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comprasds)).BeginInit();
            this.SuspendLayout();
            // 
            // btexecutar
            // 
            this.btexecutar.Font = new System.Drawing.Font("Book Antiqua", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btexecutar.ForeColor = System.Drawing.Color.SteelBlue;
            this.btexecutar.Location = new System.Drawing.Point(33, 132);
            this.btexecutar.Name = "btexecutar";
            this.btexecutar.Size = new System.Drawing.Size(101, 31);
            this.btexecutar.TabIndex = 33;
            this.btexecutar.Text = "Aplicar";
            this.btexecutar.UseVisualStyleBackColor = true;
            this.btexecutar.Click += new System.EventHandler(this.btexecutar_Click);
            // 
            // grpcliente
            // 
            this.grpcliente.BackColor = System.Drawing.Color.White;
            this.grpcliente.Controls.Add(this.textBox2);
            this.grpcliente.Controls.Add(this.textBox1);
            this.grpcliente.Controls.Add(this.cbcliente);
            this.grpcliente.Controls.Add(this.label5);
            this.grpcliente.Controls.Add(this.txtcodigocliente);
            this.grpcliente.Controls.Add(this.label4);
            this.grpcliente.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpcliente.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpcliente.Location = new System.Drawing.Point(33, 11);
            this.grpcliente.Name = "grpcliente";
            this.grpcliente.Size = new System.Drawing.Size(391, 111);
            this.grpcliente.TabIndex = 31;
            this.grpcliente.TabStop = false;
            this.grpcliente.Text = "Por Cliente";
            // 
            // cbcliente
            // 
            this.cbcliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbcliente.Font = new System.Drawing.Font("Book Antiqua", 8.25F);
            this.cbcliente.FormattingEnabled = true;
            this.cbcliente.ItemHeight = 15;
            this.cbcliente.Location = new System.Drawing.Point(73, 51);
            this.cbcliente.MaxDropDownItems = 5;
            this.cbcliente.Name = "cbcliente";
            this.cbcliente.Size = new System.Drawing.Size(311, 23);
            this.cbcliente.TabIndex = 4;
            this.cbcliente.SelectedIndexChanged += new System.EventHandler(this.cbcliente_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 16);
            this.label5.TabIndex = 3;
            this.label5.Text = "Nome";
            // 
            // txtcodigocliente
            // 
            this.txtcodigocliente.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcodigocliente.Location = new System.Drawing.Point(73, 23);
            this.txtcodigocliente.Name = "txtcodigocliente";
            this.txtcodigocliente.Size = new System.Drawing.Size(201, 21);
            this.txtcodigocliente.TabIndex = 5;
            this.txtcodigocliente.TextChanged += new System.EventHandler(this.txtcodigocliente_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "Código";
            // 
            // chktodos
            // 
            this.chktodos.AutoSize = true;
            this.chktodos.Checked = true;
            this.chktodos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chktodos.Location = new System.Drawing.Point(175, 30);
            this.chktodos.Name = "chktodos";
            this.chktodos.Size = new System.Drawing.Size(58, 20);
            this.chktodos.TabIndex = 2;
            this.chktodos.Text = "Todos";
            this.chktodos.UseVisualStyleBackColor = true;
            this.chktodos.CheckedChanged += new System.EventHandler(this.chktodos_CheckedChanged_1);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "A";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "De";
            // 
            // grpdata
            // 
            this.grpdata.BackColor = System.Drawing.Color.White;
            this.grpdata.Controls.Add(this.dtpickera);
            this.grpdata.Controls.Add(this.dtpickerde);
            this.grpdata.Controls.Add(this.label2);
            this.grpdata.Controls.Add(this.label1);
            this.grpdata.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpdata.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpdata.Location = new System.Drawing.Point(685, 11);
            this.grpdata.Name = "grpdata";
            this.grpdata.Size = new System.Drawing.Size(246, 111);
            this.grpdata.TabIndex = 32;
            this.grpdata.TabStop = false;
            this.grpdata.Text = "Data";
            // 
            // dtpickera
            // 
            this.dtpickera.CustomFormat = "";
            this.dtpickera.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpickera.Location = new System.Drawing.Point(73, 59);
            this.dtpickera.Name = "dtpickera";
            this.dtpickera.Size = new System.Drawing.Size(139, 21);
            this.dtpickera.TabIndex = 6;
            this.dtpickera.ValueChanged += new System.EventHandler(this.dtpickera_ValueChanged_1);
            // 
            // dtpickerde
            // 
            this.dtpickerde.CustomFormat = "";
            this.dtpickerde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpickerde.Location = new System.Drawing.Point(73, 29);
            this.dtpickerde.Name = "dtpickerde";
            this.dtpickerde.Size = new System.Drawing.Size(139, 21);
            this.dtpickerde.TabIndex = 5;
            this.dtpickerde.ValueChanged += new System.EventHandler(this.dtpickerde_ValueChanged_1);
            // 
            // txtUsername
            // 
            this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUsername.Enabled = false;
            this.txtUsername.Font = new System.Drawing.Font("Book Antiqua", 9F);
            this.txtUsername.Location = new System.Drawing.Point(812, 107);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.ReadOnly = true;
            this.txtUsername.Size = new System.Drawing.Size(119, 15);
            this.txtUsername.TabIndex = 34;
            this.txtUsername.Visible = false;
            // 
            // reportViewer
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.DataTable1BindingSource;
            this.reportViewer.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer.LocalReport.ReportEmbeddedResource = "Sistec_v2.Innov.UI.CRM.Reports.Compras.comprasdetalherep.rdlc";
            this.reportViewer.Location = new System.Drawing.Point(33, 178);
            this.reportViewer.Name = "reportViewer";
            this.reportViewer.Size = new System.Drawing.Size(898, 389);
            this.reportViewer.TabIndex = 30;
            // 
            // grpdocumento
            // 
            this.grpdocumento.BackColor = System.Drawing.Color.White;
            this.grpdocumento.Controls.Add(this.cbdocumento);
            this.grpdocumento.Controls.Add(this.label3);
            this.grpdocumento.Controls.Add(this.chktodos);
            this.grpdocumento.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpdocumento.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpdocumento.Location = new System.Drawing.Point(430, 11);
            this.grpdocumento.Name = "grpdocumento";
            this.grpdocumento.Size = new System.Drawing.Size(249, 111);
            this.grpdocumento.TabIndex = 37;
            this.grpdocumento.TabStop = false;
            this.grpdocumento.Text = "Por Tipo Documento";
            // 
            // cbdocumento
            // 
            this.cbdocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbdocumento.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbdocumento.FormattingEnabled = true;
            this.cbdocumento.Location = new System.Drawing.Point(23, 56);
            this.cbdocumento.Name = "cbdocumento";
            this.cbdocumento.Size = new System.Drawing.Size(207, 23);
            this.cbdocumento.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Documento";
            // 
            // rbresumo
            // 
            this.rbresumo.AutoSize = true;
            this.rbresumo.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbresumo.ForeColor = System.Drawing.SystemColors.Highlight;
            this.rbresumo.Location = new System.Drawing.Point(300, 138);
            this.rbresumo.Name = "rbresumo";
            this.rbresumo.Size = new System.Drawing.Size(118, 20);
            this.rbresumo.TabIndex = 38;
            this.rbresumo.Text = "Resumo Compras";
            this.rbresumo.UseVisualStyleBackColor = true;
            this.rbresumo.CheckedChanged += new System.EventHandler(this.rbresumo_CheckedChanged);
            // 
            // comprasds
            // 
            this.comprasds.DataSetName = "comprasds";
            this.comprasds.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // rbdetalhe
            // 
            this.rbdetalhe.AutoSize = true;
            this.rbdetalhe.Checked = true;
            this.rbdetalhe.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbdetalhe.ForeColor = System.Drawing.SystemColors.Highlight;
            this.rbdetalhe.Location = new System.Drawing.Point(152, 138);
            this.rbdetalhe.Name = "rbdetalhe";
            this.rbdetalhe.Size = new System.Drawing.Size(118, 20);
            this.rbdetalhe.TabIndex = 39;
            this.rbdetalhe.TabStop = true;
            this.rbdetalhe.Text = "Detalhe Compras";
            this.rbdetalhe.UseVisualStyleBackColor = true;
            this.rbdetalhe.CheckedChanged += new System.EventHandler(this.rbdetalhe_CheckedChanged);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(6, 84);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(115, 21);
            this.textBox1.TabIndex = 40;
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(127, 84);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(257, 21);
            this.textBox2.TabIndex = 41;
            // 
            // frmCompras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 602);
            this.Controls.Add(this.rbdetalhe);
            this.Controls.Add(this.rbresumo);
            this.Controls.Add(this.grpdocumento);
            this.Controls.Add(this.btexecutar);
            this.Controls.Add(this.grpcliente);
            this.Controls.Add(this.grpdata);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.reportViewer);
            this.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(974, 641);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(974, 641);
            this.Name = "frmCompras";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Compras";
            this.Load += new System.EventHandler(this.frmCompras_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).EndInit();
            this.grpcliente.ResumeLayout(false);
            this.grpcliente.PerformLayout();
            this.grpdata.ResumeLayout(false);
            this.grpdata.PerformLayout();
            this.grpdocumento.ResumeLayout(false);
            this.grpdocumento.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comprasds)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btexecutar;
        private System.Windows.Forms.GroupBox grpcliente;
        private System.Windows.Forms.ComboBox cbcliente;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chktodos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpdata;
        private System.Windows.Forms.DateTimePicker dtpickera;
        private System.Windows.Forms.DateTimePicker dtpickerde;
        private System.Windows.Forms.TextBox txtUsername;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer;
        private System.Windows.Forms.GroupBox grpdocumento;
        private System.Windows.Forms.ComboBox cbdocumento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.BindingSource DataTable1BindingSource;
        private System.Windows.Forms.TextBox txtcodigocliente;
        private System.Windows.Forms.RadioButton rbresumo;
        private comprasds comprasds;
        private System.Windows.Forms.RadioButton rbdetalhe;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
       
    }
}