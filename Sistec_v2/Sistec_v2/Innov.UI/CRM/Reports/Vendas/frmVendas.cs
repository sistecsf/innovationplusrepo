﻿using System;
using System.Configuration;
using System.Data;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using Sistec_v2.Innov.BL.CRM.Reports;

namespace Sistec_v2.Innov.UI.CRM.Reports.Vendas
{
    public partial class frmVendas : Form
    {
        public frmVendas()
        {
            InitializeComponent();
            txtUsername.Text = frmLogin.UserCode;
        }

        private void frmVendas_Load(object sender, EventArgs e)
        {
            //conversão formato da data para o sql
            dtpickerde.Format = DateTimePickerFormat.Custom;
            dtpickerde.CustomFormat = ("yyyy-MM-dd");
            dtpickera.Format = DateTimePickerFormat.Custom;
            dtpickera.CustomFormat = ("yyyy-MM-dd");

        }


        private void btexecutar_Click(object sender, EventArgs e)
        {

            string codigo = txtcodigocliente.Text;

            //passagem de parâmetros (do form para o report)
            ReportParameter[] param = new ReportParameter[4];
            param[0] = new ReportParameter("content_de", dtpickerde.Text);
            param[1] = new ReportParameter("content_a", dtpickera.Text);
            param[2] = new ReportParameter("content", codigo);
            param[3] = new ReportParameter("content_user", txtUsername.Text);
            reportViewer1.LocalReport.SetParameters(param);

            if (codigo != "")
            {
                var objCli =
               new CRepVendas(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString);

                var rds = new ReportDataSource("DataSet1", objCli.GetClientesVendas(dtpickerde.Text, dtpickera.Text, codigo));
                reportViewer1.LocalReport.DataSources.Clear();
                reportViewer1.LocalReport.DataSources.Add(rds);

                reportViewer1.LocalReport.Refresh();

            }

            else

            {
                 
                MessageBox.Show("Deve preencher o Código do Cliente.", "Código Cliente");

            }

          
            reportViewer1.RefreshReport();

            
        }

        private void dtpickerde_ValueChanged(object sender, EventArgs e)
        {
            dtpickerde.Format = DateTimePickerFormat.Custom;
            dtpickerde.CustomFormat = ("yyyy-MM-dd");
            dtpickera.Format = DateTimePickerFormat.Custom;
            dtpickera.CustomFormat = ("yyyy-MM-dd");

            DateTime de = dtpickerde.Value.Date;
            DateTime hoje = DateTime.Now.Date;

            int result = DateTime.Compare(hoje, de);


            if (result < 0)
            {
                MessageBox.Show("A data não pode ser menor que hoje.", "Data");
                dtpickerde.Text = hoje.ToShortDateString();
                de = dtpickerde.Value.Date;

            }

            DateTime a = dtpickera.Value.Date;

            result = DateTime.Compare(a, de);


            if (result < 0)
            {
                MessageBox.Show("Alterou a data de A.", "Data");
                dtpickera.Text = dtpickerde.Value.ToShortDateString();

            }
        }

        private void dtpickera_ValueChanged(object sender, EventArgs e)
        {
            dtpickerde.Format = DateTimePickerFormat.Custom;
            dtpickerde.CustomFormat = ("yyyy-MM-dd");
            dtpickera.Format = DateTimePickerFormat.Custom;
            dtpickera.CustomFormat = ("yyyy-MM-dd");

            DateTime de = dtpickerde.Value.Date;
            DateTime a = dtpickera.Value.Date;

            DateTime hoje = DateTime.Now.Date;

            int result = DateTime.Compare(hoje, a);


            if (result < 0)
            {
                MessageBox.Show("A data não pode ser maior que Hoje.", "Data");
                dtpickera.Text = hoje.ToShortDateString();

            }
            else
            {

                result = DateTime.Compare(a, de);


                if (result < 0)
                {
                    MessageBox.Show("A data não pode ser menor.", "Data");
                    dtpickera.Text = dtpickerde.Value.ToShortDateString();

                }
            }


        }

     
        private void cbcliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow row = ((DataTable)cbcliente.DataSource).Rows[cbcliente.SelectedIndex];
            string Id = (string)row["CodEntidade"];
            string Name = (string)row["NomeEntidade"];

            textBox1.Text = Id;
            textBox2.Text = Name;
        }

        private void txtcodigocliente_TextChanged(object sender, EventArgs e)
        {
            string codigo = txtcodigocliente.Text;
            int countWords = codigo.Length;

            if (countWords >= 3)
            {
                cbcliente.SelectedText = "";

                var objCli =
                    new CRepCompras(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString);

                cbcliente.DataSource = objCli.GetClientesCodigo(codigo);
                cbcliente.ValueMember = "NomeEntidade";
            }
        }

       
     
    }
}
