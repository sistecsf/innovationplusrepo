﻿namespace Sistec_v2.Innov.UI.CRM.Reports.Vendas
{
    partial class frmVendas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVendas));
            this.DataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vendasds = new Sistec_v2.Innov.UI.CRM.Reports.Vendas.vendasds();
            this.btexecutar = new System.Windows.Forms.Button();
            this.grpdata = new System.Windows.Forms.GroupBox();
            this.dtpickera = new System.Windows.Forms.DateTimePicker();
            this.dtpickerde = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.cbcliente = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtcodigocliente = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vendasds)).BeginInit();
            this.grpdata.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataTable1BindingSource
            // 
            this.DataTable1BindingSource.DataMember = "DataTable1";
            this.DataTable1BindingSource.DataSource = this.vendasds;
            // 
            // vendasds
            // 
            this.vendasds.DataSetName = "vendasds";
            this.vendasds.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btexecutar
            // 
            this.btexecutar.Font = new System.Drawing.Font("Book Antiqua", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btexecutar.ForeColor = System.Drawing.Color.SteelBlue;
            this.btexecutar.Location = new System.Drawing.Point(35, 129);
            this.btexecutar.Name = "btexecutar";
            this.btexecutar.Size = new System.Drawing.Size(101, 31);
            this.btexecutar.TabIndex = 5;
            this.btexecutar.Text = "Aplicar";
            this.btexecutar.UseVisualStyleBackColor = true;
            this.btexecutar.Click += new System.EventHandler(this.btexecutar_Click);
            // 
            // grpdata
            // 
            this.grpdata.BackColor = System.Drawing.Color.White;
            this.grpdata.Controls.Add(this.dtpickera);
            this.grpdata.Controls.Add(this.dtpickerde);
            this.grpdata.Controls.Add(this.label2);
            this.grpdata.Controls.Add(this.label1);
            this.grpdata.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpdata.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpdata.Location = new System.Drawing.Point(432, 12);
            this.grpdata.Name = "grpdata";
            this.grpdata.Size = new System.Drawing.Size(280, 111);
            this.grpdata.TabIndex = 36;
            this.grpdata.TabStop = false;
            this.grpdata.Text = "Data";
            // 
            // dtpickera
            // 
            this.dtpickera.CustomFormat = "";
            this.dtpickera.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpickera.Location = new System.Drawing.Point(87, 57);
            this.dtpickera.Name = "dtpickera";
            this.dtpickera.Size = new System.Drawing.Size(139, 21);
            this.dtpickera.TabIndex = 4;
            this.dtpickera.ValueChanged += new System.EventHandler(this.dtpickera_ValueChanged);
            // 
            // dtpickerde
            // 
            this.dtpickerde.CustomFormat = "";
            this.dtpickerde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpickerde.Location = new System.Drawing.Point(87, 27);
            this.dtpickerde.Name = "dtpickerde";
            this.dtpickerde.Size = new System.Drawing.Size(139, 21);
            this.dtpickerde.TabIndex = 3;
            this.dtpickerde.ValueChanged += new System.EventHandler(this.dtpickerde_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "A";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "De";
            // 
            // txtUsername
            // 
            this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUsername.Enabled = false;
            this.txtUsername.Font = new System.Drawing.Font("Book Antiqua", 9F);
            this.txtUsername.Location = new System.Drawing.Point(822, 108);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.ReadOnly = true;
            this.txtUsername.Size = new System.Drawing.Size(119, 15);
            this.txtUsername.TabIndex = 38;
            this.txtUsername.Visible = false;
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.DataTable1BindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Sistec_v2.Innov.UI.CRM.Reports.Vendas.vendasrep.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(35, 167);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(886, 411);
            this.reportViewer1.TabIndex = 39;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.cbcliente);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtcodigocliente);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.groupBox1.Location = new System.Drawing.Point(35, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(391, 111);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Por Cliente";
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(127, 84);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(257, 21);
            this.textBox2.TabIndex = 41;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(6, 84);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(115, 21);
            this.textBox1.TabIndex = 40;
            // 
            // cbcliente
            // 
            this.cbcliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbcliente.Font = new System.Drawing.Font("Book Antiqua", 8.25F);
            this.cbcliente.FormattingEnabled = true;
            this.cbcliente.ItemHeight = 15;
            this.cbcliente.Location = new System.Drawing.Point(73, 51);
            this.cbcliente.MaxDropDownItems = 5;
            this.cbcliente.Name = "cbcliente";
            this.cbcliente.Size = new System.Drawing.Size(311, 23);
            this.cbcliente.TabIndex = 4;
            this.cbcliente.SelectedIndexChanged += new System.EventHandler(this.cbcliente_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nome";
            // 
            // txtcodigocliente
            // 
            this.txtcodigocliente.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcodigocliente.Location = new System.Drawing.Point(73, 23);
            this.txtcodigocliente.Name = "txtcodigocliente";
            this.txtcodigocliente.Size = new System.Drawing.Size(201, 21);
            this.txtcodigocliente.TabIndex = 1;
            this.txtcodigocliente.TextChanged += new System.EventHandler(this.txtcodigocliente_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 16);
            this.label6.TabIndex = 1;
            this.label6.Text = "Código";
            // 
            // frmVendas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 602);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.btexecutar);
            this.Controls.Add(this.grpdata);
            this.Controls.Add(this.txtUsername);
            this.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(974, 641);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(974, 641);
            this.Name = "frmVendas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vendas";
            this.Load += new System.EventHandler(this.frmVendas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vendasds)).EndInit();
            this.grpdata.ResumeLayout(false);
            this.grpdata.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btexecutar;
        private System.Windows.Forms.GroupBox grpdata;
        private System.Windows.Forms.DateTimePicker dtpickera;
        private System.Windows.Forms.DateTimePicker dtpickerde;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUsername;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource DataTable1BindingSource;
        private vendasds vendasds;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ComboBox cbcliente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtcodigocliente;
        private System.Windows.Forms.Label label6;
    }
}