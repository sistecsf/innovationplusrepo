﻿using System;

namespace Sistec_v2.Innov.UI.CRM.Reports.Estado
{
    partial class frmEstado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEstado));
            this.DataTable1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.novods = new Sistec_v2.Innov.UI.CRM.Reports.Estado.novods();
            this.grpdata = new System.Windows.Forms.GroupBox();
            this.dtpickera = new System.Windows.Forms.DateTimePicker();
            this.dtpickerde = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grpestado = new System.Windows.Forms.GroupBox();
            this.cbestado = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grpoperador = new System.Windows.Forms.GroupBox();
            this.cbnomeoperador = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtcodigooperador = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chktodos = new System.Windows.Forms.CheckBox();
            this.btexecutar = new System.Windows.Forms.Button();
            this.reportViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.DataTable1TableAdapter = new Sistec_v2.Innov.UI.CRM.Reports.Estado.novodsTableAdapters.DataTable1TableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.novods)).BeginInit();
            this.grpdata.SuspendLayout();
            this.grpestado.SuspendLayout();
            this.grpoperador.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataTable1BindingSource
            // 
            this.DataTable1BindingSource.DataMember = "DataTable1";
            this.DataTable1BindingSource.DataSource = this.novods;
            // 
            // novods
            // 
            this.novods.DataSetName = "novods";
            this.novods.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // grpdata
            // 
            this.grpdata.BackColor = System.Drawing.Color.White;
            this.grpdata.Controls.Add(this.dtpickera);
            this.grpdata.Controls.Add(this.dtpickerde);
            this.grpdata.Controls.Add(this.label2);
            this.grpdata.Controls.Add(this.label1);
            this.grpdata.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpdata.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpdata.Location = new System.Drawing.Point(668, 10);
            this.grpdata.Name = "grpdata";
            this.grpdata.Size = new System.Drawing.Size(259, 85);
            this.grpdata.TabIndex = 3;
            this.grpdata.TabStop = false;
            this.grpdata.Text = "Data";
            // 
            // dtpickera
            // 
            this.dtpickera.CustomFormat = "";
            this.dtpickera.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpickera.Location = new System.Drawing.Point(77, 52);
            this.dtpickera.Name = "dtpickera";
            this.dtpickera.Size = new System.Drawing.Size(139, 21);
            this.dtpickera.TabIndex = 6;
            this.dtpickera.ValueChanged += new System.EventHandler(this.dtpickera_ValueChanged);
            // 
            // dtpickerde
            // 
            this.dtpickerde.CustomFormat = "";
            this.dtpickerde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpickerde.Location = new System.Drawing.Point(77, 22);
            this.dtpickerde.Name = "dtpickerde";
            this.dtpickerde.Size = new System.Drawing.Size(139, 21);
            this.dtpickerde.TabIndex = 5;
            this.dtpickerde.ValueChanged += new System.EventHandler(this.dtpickerde_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "A";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "De";
            // 
            // grpestado
            // 
            this.grpestado.BackColor = System.Drawing.Color.White;
            this.grpestado.Controls.Add(this.cbestado);
            this.grpestado.Controls.Add(this.label3);
            this.grpestado.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpestado.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpestado.Location = new System.Drawing.Point(29, 10);
            this.grpestado.Name = "grpestado";
            this.grpestado.Size = new System.Drawing.Size(260, 85);
            this.grpestado.TabIndex = 1;
            this.grpestado.TabStop = false;
            this.grpestado.Text = "Por Estado";
            // 
            // cbestado
            // 
            this.cbestado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbestado.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbestado.FormattingEnabled = true;
            this.cbestado.Items.AddRange(new object[] {
            "ADJUDICADO",
            "ANULADO",
            "PAGO",
            "PAGO/PARCIAL",
            "DEVOLVIDO",
            "EMITIDO"});
            this.cbestado.Location = new System.Drawing.Point(79, 30);
            this.cbestado.Name = "cbestado";
            this.cbestado.Size = new System.Drawing.Size(151, 23);
            this.cbestado.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Estado";
            // 
            // grpoperador
            // 
            this.grpoperador.BackColor = System.Drawing.Color.White;
            this.grpoperador.Controls.Add(this.cbnomeoperador);
            this.grpoperador.Controls.Add(this.label5);
            this.grpoperador.Controls.Add(this.txtcodigooperador);
            this.grpoperador.Controls.Add(this.label4);
            this.grpoperador.Controls.Add(this.chktodos);
            this.grpoperador.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpoperador.ForeColor = System.Drawing.SystemColors.Highlight;
            this.grpoperador.Location = new System.Drawing.Point(295, 10);
            this.grpoperador.Name = "grpoperador";
            this.grpoperador.Size = new System.Drawing.Size(367, 85);
            this.grpoperador.TabIndex = 2;
            this.grpoperador.TabStop = false;
            this.grpoperador.Text = "Por Operador";
            // 
            // cbnomeoperador
            // 
            this.cbnomeoperador.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbnomeoperador.ItemHeight = 16;
            this.cbnomeoperador.Location = new System.Drawing.Point(73, 51);
            this.cbnomeoperador.MaxDropDownItems = 5;
            this.cbnomeoperador.Name = "cbnomeoperador";
            this.cbnomeoperador.Size = new System.Drawing.Size(268, 24);
            this.cbnomeoperador.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 16);
            this.label5.TabIndex = 3;
            this.label5.Text = "Nome";
            // 
            // txtcodigooperador
            // 
            this.txtcodigooperador.Location = new System.Drawing.Point(73, 23);
            this.txtcodigooperador.Name = "txtcodigooperador";
            this.txtcodigooperador.Size = new System.Drawing.Size(116, 21);
            this.txtcodigooperador.TabIndex = 5;
            this.txtcodigooperador.TextChanged += new System.EventHandler(this.txtcodigooperador_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "Código";
            // 
            // chktodos
            // 
            this.chktodos.AutoSize = true;
            this.chktodos.Checked = true;
            this.chktodos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chktodos.Location = new System.Drawing.Point(283, 24);
            this.chktodos.Name = "chktodos";
            this.chktodos.Size = new System.Drawing.Size(58, 20);
            this.chktodos.TabIndex = 2;
            this.chktodos.Text = "Todos";
            this.chktodos.UseVisualStyleBackColor = true;
            this.chktodos.CheckedChanged += new System.EventHandler(this.chktodos_CheckedChanged);
            // 
            // btexecutar
            // 
            this.btexecutar.Font = new System.Drawing.Font("Book Antiqua", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btexecutar.ForeColor = System.Drawing.Color.SteelBlue;
            this.btexecutar.Location = new System.Drawing.Point(29, 106);
            this.btexecutar.Name = "btexecutar";
            this.btexecutar.Size = new System.Drawing.Size(101, 31);
            this.btexecutar.TabIndex = 7;
            this.btexecutar.Text = "Aplicar";
            this.btexecutar.UseVisualStyleBackColor = true;
            this.btexecutar.Click += new System.EventHandler(this.btexecutar_Click);
            // 
            // reportViewer
            // 
            reportDataSource1.Name = "novatabela";
            reportDataSource1.Value = this.DataTable1BindingSource;
            this.reportViewer.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer.LocalReport.ReportEmbeddedResource = "Sistec_v2.Innov.UI.CRM.Reports.Estado.novorep.rdlc";
            this.reportViewer.Location = new System.Drawing.Point(29, 161);
            this.reportViewer.Name = "reportViewer";
            this.reportViewer.Size = new System.Drawing.Size(898, 400);
            this.reportViewer.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(815, 709);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 15);
            this.label6.TabIndex = 26;
            this.label6.Text = "@ Copyright Sistec - (v 1.0)";
            // 
            // txtUsername
            // 
            this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUsername.Enabled = false;
            this.txtUsername.Font = new System.Drawing.Font("Book Antiqua", 9F);
            this.txtUsername.Location = new System.Drawing.Point(836, 123);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.ReadOnly = true;
            this.txtUsername.Size = new System.Drawing.Size(119, 15);
            this.txtUsername.TabIndex = 27;
            this.txtUsername.Visible = false;
            // 
            // DataTable1TableAdapter
            // 
            this.DataTable1TableAdapter.ClearBeforeFill = true;
            // 
            // frmEstado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 602);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.reportViewer);
            this.Controls.Add(this.btexecutar);
            this.Controls.Add(this.grpoperador);
            this.Controls.Add(this.grpestado);
            this.Controls.Add(this.grpdata);
            this.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(974, 641);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(974, 641);
            this.Name = "frmEstado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estado";
            this.Load += new System.EventHandler(this.frmEstado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataTable1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.novods)).EndInit();
            this.grpdata.ResumeLayout(false);
            this.grpdata.PerformLayout();
            this.grpestado.ResumeLayout(false);
            this.grpestado.PerformLayout();
            this.grpoperador.ResumeLayout(false);
            this.grpoperador.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpdata;
        private System.Windows.Forms.DateTimePicker dtpickera;
        private System.Windows.Forms.DateTimePicker dtpickerde;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox grpestado;
        private System.Windows.Forms.Label label3;
       private System.Windows.Forms.ComboBox cbestado;
        private System.Windows.Forms.GroupBox grpoperador;
        private System.Windows.Forms.ComboBox cbnomeoperador;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtcodigooperador;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chktodos;
        private System.Windows.Forms.Button btexecutar;


        private Microsoft.Reporting.WinForms.ReportViewer reportViewer;
        private System.Windows.Forms.BindingSource DataTable1BindingSource;
        private novods novods;
        private novodsTableAdapters.DataTable1TableAdapter DataTable1TableAdapter;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUsername;
    }
}