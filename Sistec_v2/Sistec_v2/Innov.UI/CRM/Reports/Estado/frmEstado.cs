﻿using System;
using System.Configuration;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using Sistec_v2.Innov.BL.CRM.Reports;


namespace Sistec_v2.Innov.UI.CRM.Reports.Estado
{
    public partial class frmEstado : Form
    {
        public frmEstado()
        {
            InitializeComponent();
            txtUsername.Text = frmLogin.UserCode;
           
        }

        private void frmEstado_Load(object sender, EventArgs e)
        {
            //conversão formato da data para o sql
            dtpickerde.Format = DateTimePickerFormat.Custom;
            dtpickerde.CustomFormat = ("yyyy-MM-dd");
            dtpickera.Format = DateTimePickerFormat.Custom;
            dtpickera.CustomFormat = ("yyyy-MM-dd");
          
            txtcodigooperador.Enabled = false;
            cbnomeoperador.Enabled = false;

        }

      
       
        private void btexecutar_Click(object sender, EventArgs e)
        {
           
            var cmb = cbestado.Text;
            var status = "-1";

            switch (cmb)
            {
                case "ADJUDICADO":
                    {
                        status = "J";
                        break;
                    }
                case "ANULADO":
                    {
                        status = "A";
                        break;
                    }
                case "PAGO":
                    {
                        status = "P";
                        break;
                    }
                case "PAGO/PARCIAL":
                    {
                        status = "F";
                        break;
                    }
                case "DEVOLVIDO":
                    {
                        status = "D";
                        break;
                    }
                case "EMITIDO":
                    {
                        status = "";
                        break;
                     }
                 }

       
            if (status == "-1")
            {
                MessageBox.Show("Deve preencher o Estado.","Status");

            }
             else
            {

                //passagem de parâmetros (do form para o report)
                ReportParameter[] param = new ReportParameter[4];
                param[0] = new ReportParameter("content", cbestado.Text);
                param[1] = new ReportParameter("content_de", dtpickerde.Text);
                param[2] = new ReportParameter("content_a", dtpickera.Text);
                param[3] = new ReportParameter("content_user", txtUsername.Text);
                reportViewer.LocalReport.SetParameters(param);
                DataTable1TableAdapter.Fill(novods.DataTable1, cbestado.Text, dtpickerde.Text, dtpickera.Text);
                reportViewer.RefreshReport();

                if (chktodos.Checked)
                {

                    DataTable1TableAdapter.Fill(novods.DataTable1, status, dtpickerde.Text, dtpickera.Text);
                   
                    reportViewer.RefreshReport();

                }
                else
                {
                    if (cbnomeoperador.Text == "")
                        MessageBox.Show("Deve preencher o Utilizador.", "Utilizador");
                    else
                     DataTable1TableAdapter.FillBy(novods.DataTable1, status, dtpickerde.Text, dtpickera.Text, cbnomeoperador.Text);
                     reportViewer.RefreshReport();
                }

                reportViewer.RefreshReport();
               

            }
        
               
        }


        private void txtcodigooperador_TextChanged(object sender, EventArgs e)
        {
            

            string codigo = txtcodigooperador.Text;
            cbnomeoperador.SelectedText = "";

            var objCli =
            new CRepEstado(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString);


            cbnomeoperador.DataSource = objCli.GetOperadoresCodigo(codigo);
            cbnomeoperador.ValueMember = "nomeutilizador";

           
        }

     
        private void dtpickerde_ValueChanged(object sender, EventArgs e)
        {
            dtpickerde.Format = DateTimePickerFormat.Custom;
            dtpickerde.CustomFormat = ("yyyy-MM-dd");
            dtpickera.Format = DateTimePickerFormat.Custom;
            dtpickera.CustomFormat = ("yyyy-MM-dd");

            DateTime de = dtpickerde.Value.Date;
            DateTime hoje = DateTime.Now.Date;
            
            int result = DateTime.Compare(hoje,de);


            if (result < 0)
            {
                MessageBox.Show("A data não pode ser menor que hoje.", "Data");
                dtpickerde.Text = hoje.ToShortDateString();
                de = dtpickerde.Value.Date;

            }

            DateTime a = dtpickera.Value.Date;

            result = DateTime.Compare(a, de);


            if (result < 0)
            {
                MessageBox.Show("Alterou a data de A.", "Data");
                dtpickera.Text = dtpickerde.Value.ToShortDateString();

            }
        }


        private void dtpickera_ValueChanged(object sender, EventArgs e)
        {
            dtpickerde.Format = DateTimePickerFormat.Custom;
            dtpickerde.CustomFormat = ("yyyy-MM-dd");
            dtpickera.Format = DateTimePickerFormat.Custom;
            dtpickera.CustomFormat = ("yyyy-MM-dd");
      
            DateTime de = dtpickerde.Value.Date;
            DateTime a = dtpickera.Value.Date;

            DateTime hoje = DateTime.Now.Date;

            int result = DateTime.Compare(hoje, a);


            if (result < 0)
            {
                MessageBox.Show("A data não pode ser maior que Hoje.", "Data");
                dtpickera.Text = hoje.ToShortDateString();

            }
            else
            {

                result = DateTime.Compare(a, de);


                if (result < 0)
                {
                    MessageBox.Show("A data não pode ser menor.", "Data");
                    dtpickera.Text = dtpickerde.Value.ToShortDateString();

                }
            }


        }

        private void chktodos_CheckedChanged(object sender, EventArgs e)
        {
            if (chktodos.Checked)
            {
                txtcodigooperador.Enabled = false;
                cbnomeoperador.Enabled = false;
                txtcodigooperador.Text = null;
                cbnomeoperador.Text = null;
            }
            else
            {
                txtcodigooperador.Enabled = true;
                cbnomeoperador.Enabled = true;
                
            }
        }


    }
}
