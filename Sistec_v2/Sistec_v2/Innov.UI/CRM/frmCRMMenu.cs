﻿using System;
using System.Windows.Forms;
using Sistec_v2.Innov.UI.CRM.Reports.Compras;
using Sistec_v2.Innov.UI.CRM.Reports.Estado;
using Sistec_v2.Innov.UI.CRM.Reports.Vendas;
using MetroFramework;


namespace Sistec_v2.Innov.UI.CRM
{
    public partial class frmCRMMenu : MetroFramework.Forms.MetroForm
    {
        public frmCRMMenu()
        {
            InitializeComponent();
        }

        private void frmCRMMenu_Load(object sender, EventArgs e)
        {
            // mostra a data e o user
            toolStripLabel3.Text = DateTime.Now.ToString();
            toolStripLabel5.Text = frmLogin.UserCode;

        }

        private void estadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fest = new frmEstado();
            fest.Show();
        }



        private void label2_Click(object sender, EventArgs e)
        {

            this.Close();
            Application.Exit();
        }

        private void principalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comprasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fcomp = new frmCompras();
            fcomp.Show();
        }

        private void vendasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var fvend = new frmVendas();
            fvend.Show();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

       



    }
}
