﻿using System;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;
using Sistec_v2.Innov.BL;


namespace Sistec_v2
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            txtUsername.Text = "aalberto";
            txtPassword.Text = "password";

            //Font = new Font("Book Antiqua", 8.25F, FontStyle.Regular, GraphicsUnit.Point, ((byte) (0)));
            //Font = SystemFonts.MessageBoxFont;
            
            
        }

        public static string UserCode = "";
       

        private void btnLogin_Click_Click(object sender, EventArgs e)
        {
             
            try
            {

            var objLogin = new CUsers(ConfigurationManager.ConnectionStrings["MyDB"].ConnectionString);
            
             //Vai validar o user

            string resultado = objLogin.makeLogin(txtUsername.Text, txtPassword.Text);
                
            if (resultado == "ok")
             {

                //***Validação feita com sucesso***//

                //Guarda o User
                UserCode = txtUsername.Text;
                
                var fm = new frmMain();
                fm.Show();

                Hide();

                
            }
            else
            {
                //Validação sem sucesso

                MessageBox.Show(resultado, "Autenticação");
                txtUsername.Text = "";
                txtPassword.Text = "";
            }

            objLogin = null;

            }

            catch (Exception err)
            {
                //MessageBox.Show(err.Message, "Contacte o Administrador de Sistema");
                //this.Close();
                Console.WriteLine("{0} Exception caught.", err);
                throw;
            }
    

        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {

        }

        private void logo_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {

        }

        private void logo_Click_1(object sender, EventArgs e)
        {

        }

        private void btsair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        }
    }

