﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace Sistec_v2.Innov.BL
{
    class CNotification
    {
        private string _cnn;

        public enum MsgType
        {
            User,
            Adm,
            All
        };

        public CNotification(string connectionString)
        {
            _cnn = connectionString;
        }

        public void Add(string Message, MsgType mType, string UserLogin)
        {
            try
            {
                int msgType;

                switch (mType)
                {
                    case MsgType.User:
                        msgType = 1;
                        break;

                    case MsgType.Adm:
                        msgType = 2;
                        break;

                    case MsgType.All:
                        msgType = 3;
                        break;

                    default:
                        msgType = 1;
                        break;
                }

                using (var conn = new SqlConnection(_cnn))
                {
                    using (var cmd = new SqlCommand("dbo.Notification_Add"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        //Valores de input
                        cmd.Parameters.Add(new SqlParameter("@Message", Message));
                        cmd.Parameters.Add(new SqlParameter("@MsgType", msgType));
                        cmd.Parameters.Add(new SqlParameter("@UserLogin", UserLogin));

                        conn.Open();
                        cmd.Connection = conn;

                        cmd.ExecuteNonQuery();
                        //Fecha base de dados
                        conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CNotification", "Add", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }
        }

        public DataTable GetList(string UserCode)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    using (var cmd = new SqlCommand("dbo.Notification_GetList"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        //Valores de input
                        cmd.Parameters.Add(new SqlParameter("@UserLogin", UserCode));

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();

                        return (myTable);

                    }
                }
            }
            catch (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CNotification", "GetList", err.Message, err.Source);
                var myTable = new DataTable();
                return (myTable);
                throw;
            }
        }

        public DataTable GetListAdm()
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    using (var cmd = new SqlCommand("dbo.Notification_GetListAdm"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();

                        return (myTable);

                    }
                }
            }
            catch (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CNotification", "GetListAdm", err.Message, err.Source);
                var myTable = new DataTable();
                return (myTable);
                throw;
            }
        }
    }
}

