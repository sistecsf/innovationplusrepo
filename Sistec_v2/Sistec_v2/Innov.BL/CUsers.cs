﻿using System;
using System.Data;
using System.Data.SqlClient;



namespace Sistec_v2.Innov.BL
{
    public class CUsers
    {
        private string _cnn;

        public CUsers(string connectionString)
        {
          _cnn = connectionString;
        }

        #region "Get Values"

        public string GetUserCode(string UserID)
        {
            try
            {
                string sUserCode;

                using (var conn = new SqlConnection(_cnn))
                {
                    using (
                        var cmd =
                            new SqlCommand("Select CodUtilizador from Utilizadores (NOLOCK) where Login='" + UserID +
                                           "'"))
                    {
                        cmd.CommandType = CommandType.Text;
                        
                        conn.Open();
                        cmd.Connection = conn;

                        sUserCode = (string) cmd.ExecuteScalar();

                        //Fecha base de dados
                        conn.Close();
                        return (sUserCode);
                    }
                }

            }
            catch (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CUsers", "GetUserCOde", err.Message, err.Source);
                return ("Erro lógico na aplicação");
                throw;
            }
        }

        public string GetLogin(string Login)
        {
            try
            {
                string sLogin;

                using (var conn = new SqlConnection(_cnn))
                {
                    using (
                        var cmd =
                            new SqlCommand("Select Login from Utilizadores (NOLOCK) where Login='" + Login +
                                           "'"))
                    {
                        cmd.CommandType = CommandType.Text;

                        conn.Open();
                        cmd.Connection = conn;

                        sLogin = (string)cmd.ExecuteScalar();

                        //Fecha base de dados
                        conn.Close();
                        return (sLogin);
                    }
                }

            }
            catch (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CUsers", "GetLogin", err.Message, err.Source);
                return ("Erro lógico na aplicação");
                throw;
            }
        }

        #endregion

        #region "Validate User"

        public string makeLogin(string UserID, string Psw)
        {
            try
            {
                //valida se campos vem em branco
                if (UserID =="" || Psw == "")
                {
                    return ("Deve preencher Utilizador e Password.");
                }

                var resultado = doLogin(UserID, Psw);


                switch (resultado)
                {
                    case 0:
                       
                        return ("Utilizador ou Password incorrecto.");

                    case 1:
                        return ("ok");

                    case 2:
                        return ("Introduza um Nome de usuário com permissão p/ aceder ao Sistema.");

                    default:
                        return ("Erro lógico na aplicação");
                }

            }
            catch (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CUsers", "makeLogin", err.Message, err.Source);
                return ("Erro lógico na aplicação");
                throw;
            }
        }

        private int doLogin(string UserID, string Psw)
        {
            try
            {
                string PasswordEncrypted;

                //Vai verificar se exise user e se está activo
                using (var conn = new SqlConnection(_cnn))
                {
                   
                    using (
                        var cmd =
                            new SqlCommand("Select newPassword Psw from Utilizadores (NOLOCK) where Login='" + UserID +
                                           "' AND Anulado IS NULL"))
                    {


                        cmd.CommandType = CommandType.Text;
                        conn.Open();
                        cmd.Connection = conn;

                        PasswordEncrypted = (string) cmd.ExecuteScalar();

                        //Fecha base de dados
                        conn.Close();
                    }
                }

                if (string.IsNullOrEmpty(PasswordEncrypted))
                {
                    //Não se econtra activo ou não existe
                    return (2);
                }

                //
                //Encontra-se activo, vai-se validar a password
                //

                if (Psw.Trim() != PasswordEncrypted.ToString().Trim())
                {
                    //Password incorrecta
                    return (0);
                }
                else
                {
                    //As validações correram bem, retorna positivo
                    return (1);
                }

            }
            catch (Exception err)
            {

                var objLog = new CLog(_cnn);
                objLog.LogException("CUsers", "doLogin", err.Message, err.Source);
                return (3);
                throw;
            }
        }

        #endregion

        
}

}
