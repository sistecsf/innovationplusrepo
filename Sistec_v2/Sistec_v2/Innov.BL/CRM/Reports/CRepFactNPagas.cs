﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace Sistec_v2.Innov.BL.CRM.Reports
{
    class CFactNPagas
    {

        public string _cnn;

        public CFactNPagas(string connectionString)
        {
            _cnn = connectionString;
        }



        public DataTable GetFactNaoPagas(string de, string a)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =



                        "Select Cast(F.DATA_LANCAMENTO As DateTime) as DATACRIA, R.NUMFACT, RTRIM(A.DESCRICAO) as Descrição,  F.CodEntidade, U.NomeUtilizador, " +
                        "STATUS= CASE F.STATUS WHEN 'F' THEN 'X'  END,  SUM(ISNULL(C.DEBITOD,0)) - SUM(ISNULL(C.CREDITOD,0)) as Valor, " +
                        "A.CREDITOD  FROM AREGDOC R, AREGDOC F, AFCONCLI C, AFCONCLI A, UTILIZADORES U  Where F.IDUNICO=R.IDORIG  AND F.STATUS ='E'  " +
                        "AND R.CODENTIDADE=C.CODENTIDADE  AND F.CODENTIDADE=A.CODENTIDADE  AND R.IDUNICO=C.IDUNICO AND U.CodUtilizador = R.CodUtilizador " +
                        "AND F.IDUNICO=A.IDUNICO    AND (C.DebitoD Is Not Null OR (R.CodDoc!='CC' And C.DebitoD Is Null))  " +
                        "And (LEFT(F.DATA_LANCAMENTO,10)>='" + de + "' AND LEFT(F.DATA_LANCAMENTO,10)<='" + a + "'        ) " +
                        "GROUP BY F.DATA_LANCAMENTO,R.NUMFACT,  F.STATUS, F.CodEntidade,U.NomeUtilizador,A.DESCRICAO,F.PRECOD,A.CREDITOD    " +
                        "UNION ALL    " +
                        "Select Cast(F.DATA_LANCAMENTO As DateTime), R.NUMFACT,  RTRIM(A.DESCRICAO) + '('+ CONVERT(VARCHAR(10),F.PRECOD,105)+')', F.CodEntidade, U.NomeUtilizador," +
                        " STATUS= CASE F.STATUS WHEN 'F' THEN 'X'  END,  SUM(ISNULL(C.DEBITOD,0)) - SUM(ISNULL(C.CREDITOD,0))," +
                        " A.CREDITOD  FROM AREGDOC R, AREGDOC F, AFCONCLI C, AFCONCLI A, UTILIZADORES U   " +
                        "  WHERE F.IDUNICO=R.IDORIG  AND F.STATUS ='F' AND R.CODENTIDADE=C.CODENTIDADE  AND F.CODENTIDADE=A.CODENTIDADE  " +
                        " AND R.IDUNICO=C.IDUNICO  AND F.IDUNICO=A.IDUNICO AND U.CodUtilizador = R.CodUtilizador " +
                        " AND (C.DebitoD Is Not Null OR (R.CodDoc!='CC' And C.DebitoD Is Null))  " +
                        "   AND (LEFT(C.DATA_LANCAMENTO,10)>='" + de + "' AND LEFT(F.DATA_LANCAMENTO,10)<='" + a + "'       )" +
                        "   GROUP BY Cast(F.DATA_LANCAMENTO As DateTime),R.NUMFACT,  F.CodEntidade, U.NomeUtilizador, F.STATUS,A.DESCRICAO,F.PRECOD,A.CREDITOD";


                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);



                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CFactNPagas.cs", "GetFactNaoPagas", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }

        public DataTable GetFactNaoPagasCliente(string de, string a, string codigo)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =



                        "Select Cast(F.DATA_LANCAMENTO As DateTime) as DATACRIA, R.NUMFACT, RTRIM(A.DESCRICAO) as Descrição,  F.CodEntidade, U.NomeUtilizador, " +
                        "STATUS= CASE F.STATUS WHEN 'F' THEN 'X'  END,  SUM(ISNULL(C.DEBITOD,0)) - SUM(ISNULL(C.CREDITOD,0)) as Valor, " +
                        "A.CREDITOD  FROM AREGDOC R, AREGDOC F, AFCONCLI C, AFCONCLI A, UTILIZADORES U  Where F.IDUNICO=R.IDORIG  AND F.STATUS ='E'  " +
                        "AND F.CODENTIDADE='" + codigo + "' AND R.CODENTIDADE=C.CODENTIDADE  AND F.CODENTIDADE=A.CODENTIDADE  AND R.IDUNICO=C.IDUNICO AND U.CodUtilizador = R.CodUtilizador " +
                        "AND F.IDUNICO=A.IDUNICO    AND (C.DebitoD Is Not Null OR (R.CodDoc!='CC' And C.DebitoD Is Null))  " +
                        "And (LEFT(F.DATA_LANCAMENTO,10)>='" + de + "'  AND LEFT(F.DATA_LANCAMENTO,10)<='" + a + "'        ) " +
                        "GROUP BY F.DATA_LANCAMENTO,R.NUMFACT,  F.STATUS, F.CodEntidade,U.NomeUtilizador,A.DESCRICAO,F.PRECOD,A.CREDITOD    " +
                        "UNION ALL    " +
                        "Select Cast(F.DATA_LANCAMENTO As DateTime), R.NUMFACT,  RTRIM(A.DESCRICAO) + '('+ CONVERT(VARCHAR(10),F.PRECOD,105)+')', F.CodEntidade, U.NomeUtilizador," +
                        " STATUS= CASE F.STATUS WHEN 'F' THEN 'X'  END,  SUM(ISNULL(C.DEBITOD,0)) - SUM(ISNULL(C.CREDITOD,0))," +
                        " A.CREDITOD  FROM AREGDOC R, AREGDOC F, AFCONCLI C, AFCONCLI A, UTILIZADORES U   " +
                        "  WHERE F.IDUNICO=R.IDORIG  AND F.STATUS ='F'  AND F.CODENTIDADE='" + codigo + "' AND R.CODENTIDADE=C.CODENTIDADE  AND F.CODENTIDADE=A.CODENTIDADE  " +
                        " AND R.IDUNICO=C.IDUNICO  AND F.IDUNICO=A.IDUNICO AND U.CodUtilizador = R.CodUtilizador " +
                        " AND (C.DebitoD Is Not Null OR (R.CodDoc!='CC' And C.DebitoD Is Null))  " +
                        "   AND (LEFT(C.DATA_LANCAMENTO,10)>='" + de + "'   AND LEFT(F.DATA_LANCAMENTO,10)<='" + a + "'       )" +
                        "   GROUP BY Cast(F.DATA_LANCAMENTO As DateTime),R.NUMFACT,  F.CodEntidade, U.NomeUtilizador, F.STATUS,A.DESCRICAO,F.PRECOD,A.CREDITOD";


                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);



                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CFactNPagas.cs", "GetFactNaoPagasCliente", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }

        public DataTable GetClientesCodigo(string codigo)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =

                        "SELECT DISTINCT CodEntidade, NomeEntidade  from AFENTIDADE   WITH (NOLOCK)  " +
                       "WHERE CodEntidade Like '%" + codigo + "%'  " +
                        "Order By NomeEntidade";

                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);



                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CFactNPagas.cs", "GetClientesCodigo", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }

        public DataTable GetClientesNome(string nome)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =

                        "SELECT DISTINCT CodEntidade, NomeEntidade from AFENTIDADE  WITH (NOLOCK) " +
                        "WHERE NomeEntidade Like '%" + nome + "%'  " +
                        "Order By NomeEntidade";

                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);

                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CFactNPagas.cs", "GetClientesNome", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }


    }
}
