﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace Sistec_v2.Innov.BL.CRM.Reports
{
    class CRepEstado
    {
        public string _cnn;

        public CRepEstado(string connectionString)
        {
            _cnn = connectionString;
        }

        //public DataTable GetOperadoresNome (string nome = "")
        //{
        //    try
        //    {
        //        using (var conn = new SqlConnection(_cnn))
        //        {
        //            var sSql =
        //                "select distinct nomeutilizador, codutilizador from utilizadores WITH (NOLOCK) where nomeutilizador Like '%" + nome + "%'  ";

        //            using (var cmd = new SqlCommand(sSql))
        //            {
        //                cmd.CommandType = CommandType.Text;
        //                cmd.CommandTimeout = 9000;

        //                conn.Open();
        //                cmd.Connection = conn;

        //                // Get the reader
        //                var reader = cmd.ExecuteReader();

        //                var myTable = new DataTable();
        //                myTable.Load(reader);
        //                //Fecha base de dados
        //                conn.Close();
        //                return (myTable);



        //            }
        //        }
        //    }
        //    catch
        //        (Exception err)
        //    {
        //        var objLog = new CLog(_cnn);
        //        objLog.LogException("CRepEstado.cs", "GetOperadoresNome", err.Message, err.Source);
        //        var myTable = new DataTable();
        //        throw;
        //    }

        //}

        public DataTable GetOperadoresCodigo(string codigo)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =
                        "select distinct nomeutilizador from utilizadores WITH (NOLOCK) where codutilizador Like '%" + codigo + "%'  ";

                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);



                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CRepEstado.cs", "GetOperadoresCodigo", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }


        //    public DataTable GetEstadoPorOperador(string nome = "", string codutilizador = " ", string status = " ", string De = "", string A = "")
        //    {

        //        try
        //        {
        //            using (var conn = new SqlConnection(_cnn))
        //            {

        //                var sSql =
        //     "Select distinct A.idunico, A.numdoc,  D.descrdoc, A.preco, A.precod, L.nomeloja, U.NomeUtilizador, A.codutilizador" +  
        //    " From  AREGDOC A  WITH (NOLOCK),  ASDOCS D  WITH (NOLOCK), UTILIZADORES U  WITH (NOLOCK),  ASLOJA L  WITH (NOLOCK) " +
        //    " Where  A.coddoc=D.coddoc and A.codloja=L.codloja and  U.NomeUtilizador = "+ nome + " and " +
        //     " A.codutilizador = "+ codutilizador +" and A.status= '"+ status +"' and A.codloja='TIT' and " +
        //     " LEFT (A.DataCria, 10)  >= " + De +"  And LEFT (A.DataCria, 10)  <=  "+ A +"";


        //                using (var cmd = new SqlCommand(sSql))
        //                {
        //                    cmd.CommandType = CommandType.Text;
        //                    cmd.CommandTimeout = 9000;


        //                    conn.Open();
        //                    cmd.Connection = conn;

        //                    // Get the reader
        //                    var reader = cmd.ExecuteReader();

        //                    var myTable = new DataTable();
        //                    myTable.Load(reader);
        //                    // Fecha base de dados
        //                    conn.Close();

        //                    return (myTable);
        //                    //  cmd.ExecuteNonQuery();


        //                }
        //            }
        //        }
        //        catch
        //           (Exception err)
        //        {
        //            var objLog = new CLog(_cnn);
        //            objLog.LogException("CRepEstado.cs", "GetEstadoPorOperador", err.Message,
        //                err.Source);
        //            var myTable = new DataTable();
        //            throw;
        //        }

        //    }



        //    public DataTable GetEstadoTodos(string status, DateTime De, DateTime A)
        //    {

        //        try
        //        {
        //            using (var conn = new SqlConnection(_cnn))
        //            {

        //                var sSql = "Select distinct A.idunico, A.numdoc,  D.descrdoc, A.preco, A.precod, L.nomeloja, " +
        //                              "U.NomeUtilizador, A.codutilizador " +
        //                              "From  AREGDOC A  WITH (NOLOCK),  ASDOCS D  WITH (NOLOCK), UTILIZADORES U  WITH (NOLOCK)," +
        //                              "ASLOJA L  WITH (NOLOCK) " +
        //                           "Where  A.coddoc=D.coddoc and A.codloja=L.codloja and   " +
        //                              "A.status = '"+ status +"'  and A.codloja='TIT' and U.codutilizador=A.codutilizador  " +
        //                              "And LEFT (A.DataCria, 10)  >= "+ De.ToShortDateString() +"    And LEFT (A.DataCria, 10)   <=  "+ A.ToShortDateString() +"   " +
        //                              "Order By U.NomeUtilizador";


        //                using (var cmd = new SqlCommand(sSql))
        //                {
        //                    cmd.CommandType = CommandType.Text;
        //                    cmd.CommandTimeout = 9000;


        //                    conn.Open();
        //                    cmd.Connection = conn;

        //                    // Get the reader
        //                    var reader = cmd.ExecuteReader();

        //                    var myTable = new DataTable();
        //                    myTable.Load(reader);
        //                    // Fecha base de dados
        //                    conn.Close();

        //                    return (myTable);
        //                    //  cmd.ExecuteNonQuery();


        //                }
        //            }
        //        }
        //        catch
        //           (Exception err)
        //        {
        //            var objLog = new CLog(_cnn);
        //            objLog.LogException("CRepEstado.cs", "GetEstadoTodos", err.Message,
        //                err.Source);
        //            var myTable = new DataTable();
        //            throw;
        //        }

        //    }


    }
}


