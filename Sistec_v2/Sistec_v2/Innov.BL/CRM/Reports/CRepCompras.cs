﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace Sistec_v2.Innov.BL.CRM.Reports
{
    class CRepCompras
    {

        public string _cnn;

        public CRepCompras(string connectionString)
        {
            _cnn = connectionString;
        }



        public DataTable GetClientesCodigo(string codigo)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =

                        "SELECT DISTINCT CodEntidade, NomeEntidade  from AFENTIDADE  WITH (NOLOCK) " +
                       "WHERE CodEntidade Like '%" + codigo + "%' Order By NomeEntidade";

                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);



                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CRepCompras.cs", "GetClientesCodigo", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }

     
        public DataTable GetClientesNome(string nome)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =

                        "SELECT DISTINCT CodEntidade, NomeEntidade from AFENTIDADE  WITH (NOLOCK) " +
                        "WHERE NomeEntidade Like '%" + nome + "%'  " +
                        "Order By NomeEntidade";

                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);

                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CRepCompras.cs", "GetClientesNome", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }


        public DataTable GetDocumento()
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =

                       "SELECT DESCRDOC FROM ASDOCS  WITH (NOLOCK) " +
                       "WHERE UPPER(TipoPos) = 'S'" +
                       " ORDER BY coddoc";

                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);



                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CRepCompras.cs", "GetDocumento", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }


         public DataTable GetClientesDetalhe(string de, string a, string codigo)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =

                     "SELECT DISTINCT F.NUMDOC, CASE R.STATUS WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO' WHEN 'F' THEN 'PAG. PARCIAL' WHEN 'J' THEN 'ADJUDICADO' WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END AS STATUS, " +
                         "CASE F.MOEDA WHEN 'KZ' THEN F.PCL * F.QTD ELSE '0' END AS PVP, F.PCLD * F.QTD AS Contravalor, CAST(F.DataCria AS DATETIME) AS DATACRIA, F.CodEntidade, F.Referenc, F.QTD, R.IDUnico, S.DescrDoc, " + 
                        " M.DescrArtigo, U.NomeUtilizador, C.NomeEntidade "+
                    "FROM  ASFICMOV1 AS F WITH (NOLOCK) INNER JOIN "+
                        " ASDOCS AS S WITH (NOLOCK) ON F.CodDoc = S.CodDoc INNER JOIN "+
                        " ASMESTRE AS M WITH (NOLOCK) ON F.Referenc = M.Referenc AND F.CodLoja = M.CodLoja AND F.CodArmz = M.CodArmz INNER JOIN"+
                        " AREGDOC AS R WITH (NOLOCK) ON F.IDunico = R.IDUnico INNER JOIN "+
                        " UTILIZADORES AS U ON F.CodUtilizador = U.CodUtilizador INNER JOIN "+
                       "  AFENTIDADE AS C ON F.CodEntidade = C.CodEntidade "+
                    "WHERE  (LEFT(F.DataCria, 10) >= '" + de + "') AND (LEFT(F.DataCria, 10) <= '" + a + "') "+
                    "AND (F.CodEntidade = '" + codigo + "') AND " +
                     "(F.CodEntidade IS NOT NULL)";

                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);



                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CRepCompras.cs", "GetClientesDetalhe", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }

         public DataTable GetClientesDocumento(string de, string a, string codigo, string documento)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =


                    "SELECT DISTINCT F.NUMDOC, CASE R.STATUS WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO' WHEN 'F' THEN 'PAG. PARCIAL' WHEN 'J' THEN 'ADJUDICADO' WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END AS STATUS, " +
                         "CASE F.MOEDA WHEN 'KZ' THEN F.PCL * F.QTD ELSE '0' END AS PVP, F.PCLD * F.QTD AS Contravalor, CAST(F.DataCria AS DATETIME) AS DATACRIA, F.CodEntidade, F.Referenc, F.QTD, R.IDUnico, S.DescrDoc, " +
                        " M.DescrArtigo, U.NomeUtilizador, C.NomeEntidade " +
                    "FROM  ASFICMOV1 AS F WITH (NOLOCK) INNER JOIN " +
                        " ASDOCS AS S WITH (NOLOCK) ON F.CodDoc = S.CodDoc INNER JOIN " +
                        " ASMESTRE AS M WITH (NOLOCK) ON F.Referenc = M.Referenc AND F.CodLoja = M.CodLoja AND F.CodArmz = M.CodArmz INNER JOIN" +
                        " AREGDOC AS R WITH (NOLOCK) ON F.IDunico = R.IDUnico INNER JOIN " +
                        " UTILIZADORES AS U ON F.CodUtilizador = U.CodUtilizador INNER JOIN " +
                       "  AFENTIDADE AS C ON F.CodEntidade = C.CodEntidade " +
                    "WHERE  (LEFT(F.DataCria, 10) >= '" + de + "') AND (LEFT(F.DataCria, 10) <= '" + a + "') " +
                    "AND (F.CodEntidade = '" + codigo + "') AND " +
                     "(F.CodEntidade IS NOT NULL) AND (F.coddoc = S.coddoc) and (S.DescrDoc= '" + documento + "')  " +
                     " Order By CAST(F.DataCria As DateTime) Desc ";	


                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);



                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CRepCompras.cs", "GetClientesDocumento", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }


         public DataTable GetClientesResumo(string de, string a, string codigo)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =

              
                    "Select distinct F.numdoc, STATUS = CASE R.STATUS   WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO' WHEN 'F' THEN 'PAG. PARCIAL' "+
                    "WHEN 'J' THEN 'ADJUDICADO' WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END ,PVP= CASE F.MOEDA WHEN 'KZ' THEN F.PVP*F.QTD ELSE '0' END "+
                    ",F.PVPD*F.QTD as Contravalor,CAST(F.datacria AS DATETIME) as Datacria, F.CodEntidade, F.referenc,  F.qtd,  R.IDUNICO, S.descrdoc, M.descrartigo,  U.NOMEUTILIZADOR, C.NomeEntidade  " +      
                    "FROM ASFICMOV1 F  WITH (NOLOCK),  ASDOCS S  WITH (NOLOCK), ASMESTRE M  WITH (NOLOCK), AREGDOC R WITH(NOLOCK),UTILIZADORES U,   AFENTIDADE AS C   "+
                    "WHERE  (LEFT(F.DataCria, 10) >= '" + de + "')  and (LEFT(F.datacria, 10) <='" + a + "') and F.CodEntidade='" + codigo + "' "+
                    "and F.CodEntidade = C.CodEntidade and  F.CodEntidade IS NOT NULL  AND F.coddoc=S.coddoc and F.referenc=M.referenc and F.codloja=M.codloja  and F.codarmz=M.codarmz  " +
                    "and F.coddoc= 'FA' and R.IDUNICO=F.IDUNICO And UPPER(TipoPos)='s'  And UPPER(TIPOUTIL)='s' and   F.CODUTILIZADOR = U.CODUTILIZADOR "+
                    " Order by descrdoc,  CAST(F.datacria AS DATETIME) DESC";

                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);



                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CRepCompras.cs", "GetClientesResumo", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }


    }
}
