﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace Sistec_v2.Innov.BL.CRM.Reports
{
    class CRepVendas
    {

        public string _cnn;

        public CRepVendas(string connectionString)
        {
            _cnn = connectionString;
        }



        public DataTable GetClientesVendas(string de, string a, string codigo)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =


                    "SELECT DISTINCT CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105) as DATACRIA, R.coddoc, D.Descrdoc AS TIPODOC, " +
                    "CAST(R.numdoc AS INT) AS NUMDOC, LTrim(RTrim(C.CodEntidade)) + ' - ' + LTrim(RTrim(C.NomeEntidade)) AS CLIENTE, "+
                    "C.NomeEntidade, C.CodEntidade, STATUS = CASE R.Status  WHEN 'A' THEN 'ANULADO' WHEN 'D' THEN 'DEVOLVIDO'  WHEN 'F' THEN 'PAG. PARCIAL' "+     
                    "WHEN 'J' THEN 'ADJUDICADO'  WHEN 'P' THEN 'PAGO' ELSE 'EMITIDO' END, "+
                    "RTRIM(R.IDunico) AS IDUNICO, U.NOMEUTILIZADOR, PRECOD "+
                    "FROM AFENTIDADE C   WITH  (NOLOCK) , ASDOCS D  WITH  (NOLOCK), "+
                    "AREGDOC R  WITH  (NOLOCK),  UTILIZADORES U  WITH  (NOLOCK) "+
                    "WHERE R.CodEntidade = C.CodEntidade and D.Coddoc = R.Coddoc "+
                    "And ISNULL(D.CUSTO_PCA, 'N')='N'"+
                    "And (D.TIPODOC = 'R' AND D.ALTSTOCK = 'S' AND D.TIPOUTIL = 'S' AND D.TIPOPOS = 'S' OR  D.TIPODOC = 'F') "+
                    "AND R.CODUTILIZADOR = U.CODUTILIZADOR " +
                    "AND (LEFT(R.DataCria, 10) >=  '" + de + "') and (LEFT(R.DataCria, 10) <= '" + a + "') " +
                    "AND C.CodEntidade='" + codigo + "' AND R.codloja = 'INF'          " +
                    "ORDER BY CONVERT(VARCHAR(10), CAST(R.DataCria AS DATETIME), 105), R.coddoc,  NUMDOC, CLIENTE, TIPODOC";


                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);



                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CRepVendas.cs", "GetClientesVendas", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }

        public DataTable GetClientesCodigo(string codigo)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =

                        "SELECT DISTINCT CodEntidade, NomeEntidade  from AFENTIDADE   WITH (NOLOCK)  " +
                       "WHERE CodEntidade Like '%" + codigo + "%'  " +
                        "Order By NomeEntidade";

                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);



                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CRepVendas.cs", "GetClientesCodigo", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }

        public DataTable GetClientesNome(string nome)
        {
            try
            {
                using (var conn = new SqlConnection(_cnn))
                {
                    var sSql =

                        "SELECT DISTINCT CodEntidade, NomeEntidade from AFENTIDADE  WITH (NOLOCK) " +
                        "WHERE NomeEntidade Like '%" + nome + "%'  " +
                        "Order By NomeEntidade";

                    using (var cmd = new SqlCommand(sSql))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 9000;

                        conn.Open();
                        cmd.Connection = conn;

                        // Get the reader
                        var reader = cmd.ExecuteReader();

                        var myTable = new DataTable();
                        myTable.Load(reader);
                        //Fecha base de dados
                        conn.Close();
                        return (myTable);

                    }
                }
            }
            catch
                (Exception err)
            {
                var objLog = new CLog(_cnn);
                objLog.LogException("CRepVendas.cs", "GetClientesNome", err.Message, err.Source);
                var myTable = new DataTable();
                throw;
            }

        }


    }
}
