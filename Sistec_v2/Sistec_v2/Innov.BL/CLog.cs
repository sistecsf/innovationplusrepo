﻿using System;
using System.Data;
using System.Data.SqlClient;


namespace Sistec_v2.Innov.BL
{
    class CLog
    {
        private string _cnn;

        public CLog(string connectionString)
        {
            _cnn = connectionString;
        }

        public void LogException(string Class, string Metodo, string ErrMessage, string ErrSource)
        {
            try 
	        {	        
		        //Vai verificar user, password e se está activo no sistema
                using (var conn = new SqlConnection(_cnn))
                {
                    using (var cmd = new SqlCommand("dbo.ErrorMap_Add"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        //Valores de input
                        cmd.Parameters.Add(new SqlParameter("@ErrorClass", Class));
                        cmd.Parameters.Add(new SqlParameter("@ErrorFunction", Metodo));
                        cmd.Parameters.Add(new SqlParameter("@ErrorDesc", ErrMessage));
                        cmd.Parameters.Add(new SqlParameter("@ErrorSource", ErrSource));

                        conn.Open();
                        cmd.Connection = conn;

                        cmd.ExecuteNonQuery();
                        //Fecha base de dados
                        conn.Close();

                    }
                }
	        }
	        catch (Exception)
	        {
		        throw;
	        }
        }
    }
}

    

