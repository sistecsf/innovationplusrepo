﻿using System;
using System.Windows.Forms;
using Sistec_v2.Innov.UI.CRM;



namespace Sistec_v2
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            
            // Carrega dados dos report viewer dashboards main
            ESTATISTICATableAdapter.Fill(DataSet1.ESTATISTICA);
            ESTATISTICA02TableAdapter.Fill(nrtrabalhadoresds.ESTATISTICA02);
            ESTATISTICA03TableAdapter.Fill(this.nrfobmifds.ESTATISTICA03);

        }

        private void CRM_Click(object sender, EventArgs e)
        {
            var fcm = new frmCRMMenu();
            fcm.Show();
        }

        private void logout_Click(object sender, EventArgs e)
        {
           
            Close();
            var fl = new frmLogin();
            fl.Show();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            // mostra a data
            toolStripLabel3.Text = DateTime.Now.ToString();

            // mostra dashboards main
            reportViewer1.RefreshReport();
            reportViewer2.RefreshReport();
            reportViewer3.RefreshReport();

            toolStripLabel5.Text = frmLogin.UserCode;

        }

   


    

        
    }
}
