﻿namespace Sistec_v2
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.ESTATISTICABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DataSet1 = new Sistec_v2.DataSet1();
            this.ESTATISTICA03BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nrfobmifds = new Sistec_v2.nrfobmifds();
            this.ESTATISTICA02BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nrtrabalhadoresds = new Sistec_v2.nrtrabalhadoresds();
            this.logout = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.CRM = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ESTATISTICA02TableAdapter = new Sistec_v2.nrtrabalhadoresdsTableAdapters.ESTATISTICA02TableAdapter();
            this.reportViewer2 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ESTATISTICATableAdapter = new Sistec_v2.DataSet1TableAdapters.ESTATISTICATableAdapter();
            this.reportViewer3 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ESTATISTICA03TableAdapter = new Sistec_v2.nrfobmifdsTableAdapters.ESTATISTICA03TableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.ESTATISTICABindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ESTATISTICA03BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nrfobmifds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ESTATISTICA02BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nrtrabalhadoresds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CRM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ESTATISTICABindingSource
            // 
            this.ESTATISTICABindingSource.DataMember = "ESTATISTICA";
            this.ESTATISTICABindingSource.DataSource = this.DataSet1;
            // 
            // DataSet1
            // 
            this.DataSet1.DataSetName = "DataSet1";
            this.DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ESTATISTICA03BindingSource
            // 
            this.ESTATISTICA03BindingSource.DataMember = "ESTATISTICA03";
            this.ESTATISTICA03BindingSource.DataSource = this.nrfobmifds;
            // 
            // nrfobmifds
            // 
            this.nrfobmifds.DataSetName = "nrfobmifds";
            this.nrfobmifds.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ESTATISTICA02BindingSource
            // 
            this.ESTATISTICA02BindingSource.DataMember = "ESTATISTICA02";
            this.ESTATISTICA02BindingSource.DataSource = this.nrtrabalhadoresds;
            // 
            // nrtrabalhadoresds
            // 
            this.nrtrabalhadoresds.DataSetName = "nrtrabalhadoresds";
            this.nrtrabalhadoresds.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // logout
            // 
            this.logout.AutoSize = true;
            this.logout.Font = new System.Drawing.Font("Book Antiqua", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logout.ForeColor = System.Drawing.Color.Red;
            this.logout.Location = new System.Drawing.Point(12, 21);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(47, 16);
            this.logout.TabIndex = 25;
            this.logout.Text = "Logout";
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Sistec_v2.Properties.Resources.modulo_caixa;
            this.pictureBox10.Location = new System.Drawing.Point(164, 341);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(130, 131);
            this.pictureBox10.TabIndex = 38;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Sistec_v2.Properties.Resources.modulo_utilitarios;
            this.pictureBox11.Location = new System.Drawing.Point(300, 341);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(130, 131);
            this.pictureBox11.TabIndex = 37;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Sistec_v2.Properties.Resources.modulo_pos;
            this.pictureBox13.Location = new System.Drawing.Point(28, 341);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(130, 131);
            this.pictureBox13.TabIndex = 35;
            this.pictureBox13.TabStop = false;
            // 
            // CRM
            // 
            this.CRM.Image = global::Sistec_v2.Properties.Resources.modulo_crm;
            this.CRM.Location = new System.Drawing.Point(164, 204);
            this.CRM.Name = "CRM";
            this.CRM.Size = new System.Drawing.Size(130, 131);
            this.CRM.TabIndex = 34;
            this.CRM.TabStop = false;
            this.CRM.Click += new System.EventHandler(this.CRM_Click);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Sistec_v2.Properties.Resources.modulo_equipamentos;
            this.pictureBox6.Location = new System.Drawing.Point(300, 204);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(130, 131);
            this.pictureBox6.TabIndex = 33;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Sistec_v2.Properties.Resources.modulo_salarios;
            this.pictureBox8.Location = new System.Drawing.Point(28, 478);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(130, 131);
            this.pictureBox8.TabIndex = 32;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Sistec_v2.Properties.Resources.modulo_orcamentos;
            this.pictureBox9.Location = new System.Drawing.Point(28, 204);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(130, 131);
            this.pictureBox9.TabIndex = 31;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Sistec_v2.Properties.Resources.modulo_contas;
            this.pictureBox4.Location = new System.Drawing.Point(164, 67);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(130, 131);
            this.pictureBox4.TabIndex = 30;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Sistec_v2.Properties.Resources.modulo_administracao;
            this.pictureBox3.Location = new System.Drawing.Point(300, 67);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(130, 131);
            this.pictureBox3.TabIndex = 29;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Sistec_v2.Properties.Resources.modulo_auditoria;
            this.pictureBox2.Location = new System.Drawing.Point(164, 478);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(130, 131);
            this.pictureBox2.TabIndex = 28;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Sistec_v2.Properties.Resources.modulo_contabilidade;
            this.pictureBox1.Location = new System.Drawing.Point(28, 67);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(130, 131);
            this.pictureBox1.TabIndex = 27;
            this.pictureBox1.TabStop = false;
            // 
            // ESTATISTICA02TableAdapter
            // 
            this.ESTATISTICA02TableAdapter.ClearBeforeFill = true;
            // 
            // reportViewer2
            // 
            this.reportViewer2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.reportViewer2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reportViewer2.ForeColor = System.Drawing.SystemColors.ControlText;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.ESTATISTICABindingSource;
            this.reportViewer2.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer2.LocalReport.ReportEmbeddedResource = "Sistec_v2.Report1.rdlc";
            this.reportViewer2.Location = new System.Drawing.Point(800, 206);
            this.reportViewer2.Name = "reportViewer2";
            this.reportViewer2.ShowPrintButton = false;
            this.reportViewer2.Size = new System.Drawing.Size(544, 176);
            this.reportViewer2.TabIndex = 40;
            // 
            // ESTATISTICATableAdapter
            // 
            this.ESTATISTICATableAdapter.ClearBeforeFill = true;
            // 
            // reportViewer3
            // 
            this.reportViewer3.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.reportViewer3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reportViewer3.ForeColor = System.Drawing.SystemColors.ControlText;
            reportDataSource2.Name = "nrfobmifds";
            reportDataSource2.Value = this.ESTATISTICA03BindingSource;
            this.reportViewer3.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer3.LocalReport.ReportEmbeddedResource = "Sistec_v2.nrfobmifrep.rdlc";
            this.reportViewer3.Location = new System.Drawing.Point(800, 42);
            this.reportViewer3.Name = "reportViewer3";
            this.reportViewer3.Size = new System.Drawing.Size(544, 165);
            this.reportViewer3.TabIndex = 41;
            // 
            // ESTATISTICA03TableAdapter
            // 
            this.ESTATISTICA03TableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Book Antiqua", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(1224, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 15);
            this.label1.TabIndex = 42;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.Font = new System.Drawing.Font("Book Antiqua", 9F, System.Drawing.FontStyle.Bold);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripLabel2,
            this.toolStripSeparator1,
            this.toolStripLabel3,
            this.toolStripButton1,
            this.toolStripLabel5});
            this.toolStrip1.Location = new System.Drawing.Point(0, 661);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1350, 25);
            this.toolStrip1.TabIndex = 43;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(0, 22);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel2.ForeColor = System.Drawing.SystemColors.Highlight;
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(154, 22);
            this.toolStripLabel2.Text = "@ Copyright Sistec - (v 1.0)";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripLabel3.ForeColor = System.Drawing.SystemColors.Highlight;
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(34, 22);
            this.toolStripLabel3.Text = "Data";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::Sistec_v2.Properties.Resources.loguser1;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "Utilizador";
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.ForeColor = System.Drawing.SystemColors.Highlight;
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(93, 22);
            this.toolStripLabel5.Text = "toolStripLabel5";
            // 
            // reportViewer1
            // 
            this.reportViewer1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.reportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reportViewer1.ForeColor = System.Drawing.SystemColors.ControlText;
            reportDataSource3.Name = "DataSet1";
            reportDataSource3.Value = this.ESTATISTICA02BindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource3);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Sistec_v2.nrtrabalhadoresrep.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(846, 377);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ShowPrintButton = false;
            this.reportViewer1.Size = new System.Drawing.Size(501, 284);
            this.reportViewer1.TabIndex = 39;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 686);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.reportViewer3);
            this.Controls.Add(this.reportViewer2);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.CRM);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.logout);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1364, 726);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Innovation +";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ESTATISTICABindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ESTATISTICA03BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nrfobmifds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ESTATISTICA02BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nrtrabalhadoresds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CRM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label logout;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox CRM;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.BindingSource ESTATISTICA02BindingSource;
        private nrtrabalhadoresds nrtrabalhadoresds;
        private nrtrabalhadoresdsTableAdapters.ESTATISTICA02TableAdapter ESTATISTICA02TableAdapter;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer2;
        private System.Windows.Forms.BindingSource ESTATISTICABindingSource;
        private DataSet1 DataSet1;
        private DataSet1TableAdapters.ESTATISTICATableAdapter ESTATISTICATableAdapter;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer3;
        private System.Windows.Forms.BindingSource ESTATISTICA03BindingSource;
        private nrfobmifds nrfobmifds;
        private nrfobmifdsTableAdapters.ESTATISTICA03TableAdapter ESTATISTICA03TableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
    }
}