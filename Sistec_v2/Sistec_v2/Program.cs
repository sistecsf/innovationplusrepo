﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sistec_v2.Innov.UI.CRM.Reports.Estado;
using Sistec_v2.Innov.UI.CRM.Reports.Compras;
using Sistec_v2.Innov.UI.CRM;
namespace Sistec_v2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
           // Application.Run(new frmLogin());
           Application.Run(new frmCRMMenu());

            }
        }
    }

