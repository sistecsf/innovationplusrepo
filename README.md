### INNOVATION PLUS  ###

* Sobre o Innovation Plus

Sistema de gestão integrado com mais de 10 módulos da empresa SISTEC.

* Versão

Innovation Plus V.0.1

### Pre-requisitos ###

* Crie uma conta no bitbucket - é Grátis
* Peça ao administrador para enviar-lhe o convite para juntar-se ao Team SISTEC
* Aceite o convite no enviado no seu e-mail

### Como juntar-se ao projecto? ###

* Abra o seu terminal (cmd)
* Acesse o seu directório de trabalho - /Documents/Visual Studio2013/projects
* Digite: git clone https://sousadgaspar@bitbucket.org/sistecsf/innovationplusrepo.git
* Digite as sua senha do bitbucket
* Abra o projecto na pasta  /Documents/Visual Studio2013/projects/innovationplusrepo clonada do bitbucket


### Convenções ###

* Padrão para nomenclatura durante a codificação

Aberto para discução... 
Exixtem vários padrões para nomenclatura de variáveis, funções e classes. As mais comuns são: camelCase e under_line_case. Visto que a linguagem padrão do projecto é C# vale seguir o padrão de nomenclatura da mesma linguagem - A camelCase. Para uma compreensão mais clara sobre sobre o assunto leia o artigo http://www.codeproject.com/Articles/69786/Underscore-vs-Camelcase-Naming-Convention



* Padrão para mensagens e alertas

... Sugerem algum????
